<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);
$tipoJogo = mysqli_real_escape_string($con, $_POST['tipoJogo']);

$query = "select v.*, cast(v.numero as char) numero_char,
	case when v.tipo_jogo != 'B' then '' else b.codigo end as grupo,
	case when v.tipo_jogo != 'B' then '' else b.descricao end as bicho
	from (
	select cod_jogo cod_jogo_desc, cod_jogo,
	cast(case
				when jogo.tipo_jogo in ('2','B') then
					concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'),
			   		case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
			   when jogo.tipo_jogo = 'R' then jogo.DESCRICAO
			   else concat(jogo.concurso, '-', DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y')) end as char) as concurso ,
	tipo_jogo,
	data_jogo,
	hora_extracao,
	case when tipo_jogo in ('S', 'Q', 'L', 'U') then LPAD(NUMERO_1,2,'0') 
		 when tipo_jogo = 'R' then LPAD(NUMERO_1,3,'0')	 
		 else LPAD(NUMERO_1,4,'0') end numero, '1º' AS valor,
	1 as valor_int
	from jogo
	where cod_site = '$site'
  UNION";

  switch ($tipoJogo) {
	  case "2":
		$numeroPremios = 2;
	  break;
	  case "Q":
		$numeroPremios = 5;
	  break;
	  case "S":
	  case "U":
		$numeroPremios = 6;
	  break;
	  case "L":
		$numeroPremios = 15;
	  break;
	  case "B":
		$numeroPremios = 10;
	  break;
	  case "R":
		$numeroPremios = 2;
	  break;
	  default:
	    $numeroPremios = 15;
	  break;
  }
  
for ($i = 2; $i <= $numeroPremios; $i++) {
    $query = $query .
        " select cod_jogo cod_jogo_desc, '' cod_jogo,
			'' concurso,
			tipo_jogo,
			data_jogo,
			hora_extracao,
			case when tipo_jogo != 'B' then LPAD(NUMERO_" . $i . ",2,'0') else LPAD(NUMERO_" . $i . ",4,'0') end numero, '$i º' AS valor,
			$i as valor_int			
			from jogo
			where cod_site = '$site'
			and tipo_jogo != '2' ";

    if ($i != $numeroPremios) {
        $query = $query . " UNION ";
    }
}

$query = $query .
" ) v
	inner join bicho b on (b.numero_1 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) or
						b.numero_2 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) or
						b.numero_3 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) or
						b.numero_4 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) )
	where v.numero is not null ";
	

	if ($tipoJogo != '') {
        $query = $query . " and tipo_jogo = '$tipoJogo' ";
    }
	
	if ($dataDe != '0' && $dataDe != '') {
        $query = $query . " AND data_jogo >= '$dataDe' ";
    }

    if ($dataAte != '0' && $dataAte != '') {
        $query = $query . " AND data_jogo <= '$dataAte' ";
    }
	
	$query = $query . " order by cod_jogo_desc,tipo_jogo, data_jogo, hora_extracao, valor_int ";

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
	$row_array['cod_jogo'] = $row['cod_jogo'];
    $row_array['concurso'] = $row['concurso'];
    $row_array['tipo_jogo'] = $row['tipo_jogo'];
    $row_array['data_jogo'] = $row['data_jogo'];
    $row_array['numero'] = $row['numero_char'];
    $row_array['valor'] = $row['valor'];
    $row_array['grupo'] = $row['grupo'];
	$row_array['bicho'] = $row['bicho'];    

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}

$con->close();
echo json_encode($return_arr);
