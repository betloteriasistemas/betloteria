angular.module('main').controller('clienteCtrl', 
function ($rootScope, $scope, user, $http, $route) {

    $scope.isCadastroCliente = false;
    $scope.isEdicaoCliente = false;

    $scope.cod_cliente;
    $scope.nome;
    $scope.telefone;

    $scope.itensPorPagina = 10;
    
    $scope.cadastrar = function () {
        $scope.isCadastroCliente = true;
    }

    $scope.alterar = function (cliente) {
        topoScroll();
        $scope.isEdicaoCliente = true;
        $scope.cod_cliente = cliente.cod_cliente;
        $scope.nome = cliente.nome;
        $scope.telefone = cliente.telefone;
    }

    $scope.cancelar = function () {
        $scope.isCadastroCliente = false;
        $scope.isEdicaoCliente = false;    
    }

    $scope.listarClientes = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/clientes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo()
                + '&operacao=listar'
        }).then(function (response) {
            $scope.records = response.data;
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.salvar = function (codigo) {
        var nome = $scope.nome;
        var telefone = $scope.telefone;

        if (typeof codigo === "undefined") {
            codigo = 0;
        }

        if (typeof nome === "undefined" || nome === "") {
            $.alert('Informe o NOME!');
        } else if (typeof telefone === "undefined" || telefone === "") {
            $.alert('Informe o TELEFONE!')
        } else {
            $rootScope.loading = true;
            $http({

                url: 'angularjs-mysql/clientes.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo() +
                      '&codigo=' + codigo + 
                      '&nome=' + nome + 
                      '&telefone=' + telefone + 
                      '&operacao=salvar'
            }).then(function (response) {
                $rootScope.loading = false;
                if (response.data.status == 'OK') {
                    $route.reload();
                } else {
                    $.alert(response.data.mensagem);
                }
            })
        }
    }

    $scope.excluir = function(codigo, nome) {
        $.confirm({
            title: '',
            content:"Confirma a exclusão do cliente: " + nome + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $rootScope.loading = true;
                    $http({
                        url: 'angularjs-mysql/clientes.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 
                            'cod_cliente=' + codigo +
                            '&nome_cliente=' + nome + 
                            '&operacao=excluir'
                    }).then(function (response) {
                        $rootScope.loading = false;
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    }

    function topoScroll() {
        $('html, body').animate({ scrollTop: $('.container-fluid').offset().top - 100 }, 'slow');
    }

});