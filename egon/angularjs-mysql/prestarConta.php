<?php

include "conexao.php";
require_once 'vendor/autoload.php';

require_once('auditoria.php');

if (!isset($_GET)) {
    die();
}

$site = mysqli_real_escape_string($con, $_GET['site']);
$perfil = mysqli_real_escape_string($con, $_GET['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$nome_usuario = mysqli_real_escape_string($con, $_GET['nome_usuario']);
$exibeCambista = mysqli_real_escape_string($con, $_GET['exibeCambista']);
$prestarConta = mysqli_real_escape_string($con, $_GET['prestarConta']);


if ($perfil == "G") {
    $query = "select g.*, g.saldo_par - (IFNULL(g.comissao_gerente,0)) as saldo from (
        select v.*, (v.entradas - v.saidas - v.comissao) saldo_par,
        round(((select (CASE WHEN v.Tipo_Jogo = 'S' THEN pct_comissao_seninha 
        WHEN v.Tipo_Jogo = 'Q' THEN  pct_comissao_quininha 
        WHEN v.Tipo_Jogo = 'L' THEN  pct_comissao_lotinha
        WHEN v.Tipo_Jogo in ('B','G') THEN  pct_comissao_bicho 
        WHEN v.Tipo_Jogo = '2' THEN  pct_comissao_2PRA500
        WHEN v.Tipo_Jogo = 'R' THEN  pct_comissao_rifa 
        WHEN v.Tipo_Jogo = '6' THEN  pct_comissao_6DASORTE ELSE 0 END) from usuario where cod_usuario = '$cod_usuario') * (v.entradas - v.saidas - v.comissao)) / 100 ,2) comissao_gerente
        from (
            select usu.cod_usuario, usu.nome, null as gerente,
            sum(apo.valor_aposta) entradas,
            count(apo.cod_aposta) qtd_apostas,
            sum(apo.comissao) as comissao,
            sum(apo.valor_ganho) as saidas
            from jogo
            inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
            inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
            inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
            where jogo.cod_site = '$site'
            and usu.cod_gerente = '$cod_usuario'
              and bil.status_bilhete not in ('C','F')
              and apo.status not in ('C') 
            AND jogo.TP_STATUS = 'P'
            group by usu.cod_usuario, usu.nome) v where 1 =1 order by nome desc) g; ";
    } else if ($perfil == "A") {
        if($exibeCambista == 'false')
        {
            $query = "
            select g.*, g.saldo_par - (IFNULL(g.comissao_gerente,0)) as saldo  from (
                select v.*, (v.entradas - v.saidas - v.comissao)  saldo_par,
                round((v.pct_comissao * (v.entradas - v.saidas - v.comissao)) /100,2) comissao_gerente
                from (
                 select geren.cod_usuario, geren.nome,  null as gerente,
                 (CASE WHEN jogo.Tipo_Jogo = 'S' THEN geren.pct_comissao_seninha 
                    WHEN jogo.Tipo_Jogo = 'Q' THEN  geren.pct_comissao_quininha 
                    WHEN jogo.Tipo_Jogo = 'L' THEN  geren.pct_comissao_lotinha
                    WHEN jogo.Tipo_Jogo in ('B','G') THEN  geren.pct_comissao_bicho 
                    WHEN jogo.Tipo_Jogo = '2' THEN  geren.pct_comissao_2PRA500
                    WHEN jogo.Tipo_Jogo = 'R' THEN  geren.pct_comissao_rifa
                    WHEN jogo.Tipo_Jogo = '6' THEN  geren.pct_comissao_6DASORTE ELSE 0 END) as pct_comissao,
                 sum(apo.valor_aposta) entradas,
                 count(apo.cod_aposta) qtd_apostas,
                 sum(apo.comissao) as comissao,
                
                 sum(apo.valor_ganho) as saidas
                 from jogo
                 inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                 inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                 inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                 inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                 left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                 where jogo.cod_site = '$site'
                 and geren.cod_gerente = '$cod_usuario'
                 and bil.status_bilhete not in ('C','F')
                 and apo.status not in ('C') 
                 AND jogo.TP_STATUS = 'P'
                 group by geren.cod_usuario, geren.nome) v where 1 =1 order by nome desc) g";
        } else{
           /* $query = "select cod_usuario,nome, ID_GERENTE,gerente,sum(entradas) as entradas, sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
            round((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))) /100,2) as comissao_gerente,
            (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
            ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - ((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)))/100)) as saldo
                from (
                    select  usu.cod_usuario,usu.nome, geren.cod_usuario as ID_GERENTE, geren.nome as gerente,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
                            when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
                            else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status,  jogo.tipo_jogo, geren.pct_comissao,
                    sum(apo.valor_aposta) entradas,
                    count(apo.cod_aposta) qtd_apostas,
                    (sum(apo.valor_aposta) * usu.pct_comissao) / 100 as comissao,
                    sum(case when apo.status = 'G' then apo.possivel_retorno else 0 end) as saidas
                    from jogo
                    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                    inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                    where jogo.cod_site = '$site'
                    and geren.cod_gerente = '$cod_usuario'
                    and bil.status_bilhete not in ('C','F')
                    and apo.status not in ('C') 
                    AND jogo.TP_STATUS = 'P'
                    group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status, jogo.tipo_jogo) 
                    v where 1 =1 group by cod_usuario,nome, ID_GERENTE,gerente order by gerente,nome desc "; */
                    
                    $query = "select v.cod_usuario, 
                                     v.nome, 
                                     v.ID_GERENTE, 
                                     v.gerente, 
                                     SUM(v.entradas) as entradas, 
                                     SUM(v.qtd_apostas) as qtd_apostas, 
                                     SUM(v.comissao) as comissao, 
                                     SUM(v.comissao_gerente) as comissao_gerente,
                                     SUM(v.saidas) as saidas, 
                                     SUM(v.saldo_lancamento_gerente) as saldo_lancamento_gerente, 
                                     SUM(v.saldo_lancamento_cambista) as saldo_lancamento_cambista
                              from (
                                (select  usu.cod_usuario,
                                            usu.nome, 
                                            geren.cod_usuario as ID_GERENTE, 
                                            geren.nome as gerente, 
                                            sum(apo.valor_aposta) entradas,
                                            count(apo.cod_aposta) qtd_apostas,
                                            sum(apo.comissao) as comissao,
                                            sum(apo.comissao_gerente) as comissao_gerente,
                                            sum(apo.valor_ganho) as saidas,
                                            0 as saldo_lancamento_gerente,
                                            0 as saldo_lancamento_cambista
                                    from jogo
                                    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                                    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                                    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                                    inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                                    where jogo.cod_site = '$site'
                                    and geren.cod_gerente = '$cod_usuario'
                                    and bil.status_bilhete not in ('C','F')
                                    and apo.status != 'C' 
                                    AND jogo.TP_STATUS = 'P'
                                    group by usu.cod_usuario, geren.cod_usuario)
                                    union all
                                (select U.cod_usuario, 
                                        '-' as nome, 
                                        U.cod_usuario as ID_GERENTE, 
                                        U.nome as gerente, 
                                        0 as entradas, 
                                        0 as qtd_apostas, 
                                        0 as comissao, 
                                        0 as comissao_gerente, 
                                        0 as saidas,
                                        sum(case when L.tipo_lancamento = 'E' then (L.valor) else (L.valor*-1) end) as saldo_lancamento_gerente,
                                        0 as saldo_lancamento_cambista
                                    from lancamento L
                                        inner join usuario U on (L.COD_USUARIO_lancamento = U.cod_usuario)                                        
                                    where L.cod_site = '$site' 
                                        and L.COD_USUARIO_lancamento in (select cod_usuario 
                                                                        from usuario 
                                                                        where cod_gerente = '$cod_usuario') 
                                        and L.ajuda_custo = 'N' and L.status='A'
                                    group by u.cod_usuario)
                                union all 
                                (select U.cod_usuario, 
                                        U.nome, 
                                        U.cod_gerente as ID_GERENTE, 
                                        geren.nome as gerente, 
                                        0 as entradas, 
                                        0 as qtd_apostas, 
                                        0 as comissao, 
                                        0 as comissao_gerente, 
                                        0 as saidas,
                                        0 as saldo_lancamento_gerente,
                                        sum(case when L.tipo_lancamento = 'E' then (L.valor) else (L.valor*-1) end) as saldo_lancamento_cambista
                                    from lancamento L
                                        inner join usuario U on (L.COD_USUARIO_lancamento = U.cod_usuario)
                                        inner join usuario geren on (U.cod_gerente = geren.cod_usuario)
                                    where L.cod_site = '$site' 
                                        and L.COD_USUARIO_lancamento in (select U1.cod_usuario 
                                                                        from usuario U1, usuario U2 
                                                                        where U1.cod_gerente = U2.cod_usuario 
                                                                            and U2.cod_gerente = '$cod_usuario') 
                                        and L.ajuda_custo = 'N' 
                                        and L.status='A'
                                    group by u.cod_usuario)) v
                                    group by v.cod_usuario, v.cod_usuario
                                    order by v.gerente, v.nome desc";
        }   
        
}

// CONTROLE PARA VERIFICAR SE AS COMISSOES DOS GERENTES POR MODALIDADE SAO IGUAIS - INICIO
$query_gerentes = "SELECT NOME, FLG_SENINHA, PCT_COMISSAO_SENINHA, 
                                FLG_QUININHA, PCT_COMISSAO_QUININHA, 
                                FLG_LOTINHA, PCT_COMISSAO_LOTINHA,
                                FLG_SUPER_SENA, PCT_COMISSAO_SUPERSENA,
                                FLG_BICHO, PCT_COMISSAO_BICHO,
                                FLG_RIFA, PCT_COMISSAO_RIFA,
                                FLG_2PRA500, PCT_COMISSAO_2PRA500
                    FROM USUARIO 
                    WHERE COD_SITE = '$site' 
                    AND perfil = 'G' 
                    AND STATUS = 'A'";
$result_gerentes = mysqli_query($con, $query_gerentes);
$ctrl_comissoes_gerentes_arr = array(); 
$contador = 0;
while ($row = mysqli_fetch_array($result_gerentes, MYSQLI_ASSOC)) {
    $contador = $contador + 1;

    $comissoes_arr = array();
    if ($row['FLG_SENINHA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_SENINHA']);
    }

    if ($row['FLG_QUININHA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_QUININHA']);
    }

    if ($row['FLG_LOTINHA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_LOTINHA']);
    }

    if ($row['FLG_SUPER_SENA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_SUPERSENA']);
    }

    if ($row['FLG_BICHO'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_BICHO']);
    }

    if ($row['FLG_RIFA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_RIFA']);
    }

    if ($row['FLG_2PRA500'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_2PRA500']);
    }
    $valor_comissao = 0;
    $comissoes_iguais = 1;
    $contador = 0;
    foreach ($comissoes_arr as $comissao) {
        if ($contador == 0) {
            $valor_comissao = $comissao;
        }
        $contador++;
        if ($comissao != $valor_comissao) {
            $comissoes_iguais = 0;
            break;
        }
    }

    if ($valor_comissao > 0) {
        $valor_comissao = $valor_comissao / 100;
    }

    $row_array['gerente'] = $row['NOME'];
    $row_array['comissoes_iguais'] = $comissoes_iguais;
    $row_array['comissao'] = $valor_comissao;
    array_push($ctrl_comissoes_gerentes_arr, $row_array);

    if ($contador == mysqli_num_rows($result_gerentes)) {
        break;
    }
}
// CONTROLE PARA VERIFICAR SE AS COMISSOES DOS GERENTES POR MODALIDADE SAO IGUAIS - FIM

$result = mysqli_query($con, $query);

$return_arr = array();
$comissoes_gerentes_arr = array(); 

$contador = 0;
$gerenteControle = "";
$comissaoGerente = 0;
$saldoGerente = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    
    if ($row['cod_usuario'] != null) {
        $row_array['cod_usuario'] = $row['cod_usuario'];
        $row_array['nome'] = $row['nome'];
        $saldo = $row['entradas'] - $row['saidas'] - $row['comissao'];
        $row_array['saldo'] = $saldo;
        $row_array['entradas'] = $row['entradas'];
        $row_array['qtd_apostas'] = $row['qtd_apostas'];
        $row_array['comissao'] = $row['comissao'];
        $row_array['saidas'] = $row['saidas'];
        $row_array['gerente'] = $row['gerente'];
        $row_array['saldo_par'] = ($row['entradas'] - $row['saidas'] - $row['comissao']);
        $row_array['saldo_lancamento_gerente'] = $row['saldo_lancamento_gerente'];
        if ($row['saldo_lancamento_gerente'] == null) {
            $row_array['saldo_lancamento_gerente'] = 0;
        }
        $row_array['saldo_lancamento_cambista'] = $row['saldo_lancamento_cambista'];
        if ($row['saldo_lancamento_cambista'] == null) {
            $row_array['saldo_lancamento_cambista'] = 0;
        }    

        if ($gerenteControle != $row['gerente'] && $gerenteControle != "") {
            if ($saldoGerente < 0) {
                $comissaoGerente = 0;
            }

            $comissoes_iguais = 0;
            $valor_comissao = 0;
            foreach ($ctrl_comissoes_gerentes_arr as $ctrl_comissao) {
                if ($ctrl_comissao['gerente'] == $gerenteControle) {
                    $comissoes_iguais = $ctrl_comissao['comissoes_iguais'];
                    $valor_comissao = $ctrl_comissao['comissao'];
                    break;
                }
            }

            if ($comissoes_iguais == 1 && $comissaoGerente != 0) {
                // CASO TODAS AS COMISSOES POR MODALIDADE SEJAM IGUAIS, EH REALIZADO O CALCULO DA COMISSAO
                $comissoes_gerentes_arr[$gerenteControle] = $saldoGerente * $valor_comissao;
            } else {
                $comissoes_gerentes_arr[$gerenteControle] = $comissaoGerente;
            }
            
            $comissaoGerente = 0;
            $saldoGerente = 0;
        }
        $gerenteControle = $row['gerente'];
        $comissaoGerente = $comissaoGerente + $row['comissao_gerente'];
        $saldoGerente = $saldoGerente + $saldo;

        array_push($return_arr, $row_array);
    
    }    

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
};

if ($saldoGerente < 0) {
    $comissaoGerente = 0;
}
$comissoes_iguais = 0;
$valor_comissao = 0;
foreach ($ctrl_comissoes_gerentes_arr as $ctrl_comissao) {
    if ($ctrl_comissao['gerente'] == $gerenteControle) {
        $comissoes_iguais = $ctrl_comissao['comissoes_iguais'];
        $valor_comissao = $ctrl_comissao['comissao'];
        break;
    }
}

if ($comissoes_iguais == 1 && $comissaoGerente != 0) {
    // CASO TODAS AS COMISSOES POR MODALIDADE SEJAM IGUAIS, EH REALIZADO O CALCULO DA COMISSAO
    $comissoes_gerentes_arr[$gerenteControle] = $saldoGerente * $valor_comissao;
} else {
    $comissoes_gerentes_arr[$gerenteControle] = $comissaoGerente;
}

$sumQtdApostas = 0;
$sumEntradas= 0;
$sumSaidas = 0;
$sumComissao = 0;
$sumComissaoGerente = 0;
$sumSaldo = 0;
$sumCambistaSaldo =0;
$dataAtual = date('d/m/Y', time());
$dataPrestacao = date('Y-m-d', time());
$sumLancCambista = 0;
$sumLancGerente = 0;

$gerenteControle = "";

$sumGerenteQtdApostas = 0;
$sumGerenteEntradas= 0;
$sumGerenteSaidas = 0;
$sumGerenteComissao = 0;
$sumGerenteComissaoGerente = 0;
$sumGerenteSaldo = 0;
$sumGerenteCambistaSaldo =0;
$sumGerenteLancCambista = 0;
$sumGerenteLancGerente = 0;


$saldo_final_linha = 0;
$saldo_total_gerente = 0;
$saldo_total_geral = 0;

if (count($return_arr) == 0) {

    $html = "
   <html>
<head>
   <meta charset='utf-8'>
</head>
<body>
<hr>
  <h1> Nenhuma prestação de conta </h1>
<hr>
</body>
</html> ";
} else {
    $html = "
    <html>
 <head>
    <meta charset='utf-8'>
    <style>
    table, th, td {
        border-collapse: collapse;
        font-family: Arial, Helvetica, sans-serif;
    }
    th, td {
        border: 1px solid black;
        padding: 3px;
    }
    </style>
 </head>
 <body>
 <div class='col-md-12 col-ms-12 col-xs-12'>
                 <div class='table-responsive betL_CaixaList' style='padding-top: 30px'>
 
                 <h1> PRESTAÇÃO DE CONTAS - " . $dataAtual .  " </h1>
                 
                 <div>&nbsp;</div>
            
                 
                     <table class='table table-hover' style='width: 100%;'>
                         <tbody class='container'>";
                         $odd = 0;           
                         foreach ($return_arr as $e) {
                             $odd = $odd + 1;                                                          

                             if (($gerenteControle != $e['gerente']) && ($gerenteControle != "")) {
                                $sumLancGerente = $sumLancGerente + $sumGerenteLancGerente;
                                $html = $html . "<tr style='background-color: #A9F5BC;'>

                                <th scope='row'> TOTAL </th>";
                                 $html = $html . "<td align='center'  style='font-weight: bold;'> " .  $sumGerenteQtdApostas . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteEntradas, 2, ',', '.') . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteSaidas, 2, ',', '.') . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteComissao, 2, ',', '.') . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteCambistaSaldo, 2, ',', '.') . " </td>";
                               
                                $sumGerenteComissaoGerente = $comissoes_gerentes_arr[$gerenteControle];

                                if($sumGerenteComissaoGerente > 0) {
                                      $sumComissaoGerente = $sumComissaoGerente + $sumGerenteComissaoGerente;
                                      $saldo_total_gerente = $sumGerenteSaldo - $sumGerenteComissaoGerente + $sumGerenteLancCambista + $sumGerenteLancGerente;
                                      $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                      $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                      if ($saldo_total_gerente > 0) {
                                          $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                      } else{
                                          $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                      }
                                } else {
                                      $saldo_total_gerente = $sumGerenteCambistaSaldo + $sumGerenteLancCambista + $sumGerenteLancGerente;
                                      $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                      $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                      if ($saldo_total_gerente > 0) {
                                          $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                      } else {
                                          $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                      }
                                }
                                $html = $html . "<tr style='background-color: white; color: white; border: 0 !important;'> 
                                                    <td colspan='9' style='border: 0 !important; height: 26px;'></td> 
                                                 </tr>";
                                $saldo_total_gerente = 0;
                             }

                             if ($gerenteControle != $e['gerente']) {                                
                                $html = $html . "</tr>";

                                $html = $html . "<tr style='background-color: #2E64FE; color: white;'> 
                                                    <td colspan='9' style='text-align: center; color: white; font-weight: bold;'>" 
                                                        . strtoupper($e['gerente']) 
                                                        . " / Comissão: R$ " . number_format($comissoes_gerentes_arr[$e['gerente']], 2, ',', '.') 
                                                    . "</td> 
                                                 </tr>
                                <tr style='background-color: #2E64FE; color: white;'>
                                    <td style='width: 16%; text-align: center; color: white; font-weight: bold;'>Cambista</td> 
                                    <td style='width: 6%; text-align: center; color: white; font-weight: bold;'>Qtd</td>
                                    <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Entradas</td>
                                    <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Saídas</td>
                                    <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Comissão</td>
                                    <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Saldo Camb.</td>
                                    <td style='width: 9%; text-align: center; color: white; font-weight: bold;'>Lanc. Cambista</td>
                                    <td style='width: 9%; text-align: center; color: white; font-weight: bold;'>Lanc. Gerente</td>
                                    <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Saldo</td>
                                </tr>";
                                $sumGerenteQtdApostas = 0;
                                $sumGerenteEntradas= 0;
                                $sumGerenteSaidas = 0;
                                $sumGerenteComissao = 0;
                                $sumGerenteComissaoGerente = 0;
                                $sumGerenteSaldo = 0;
                                $sumGerenteCambistaSaldo = 0;
                                $sumGerenteLancCambista = 0;
                             }

                             if($odd % 2 == 0)
                             {
                                 $html = $html . "<tr style='background-color: #E6E6E6;'>";
                             } else {
                                 $html = $html . "<tr>";
                             }

                                $sumQtdApostas = $sumQtdApostas + $e['qtd_apostas'];
                                $sumEntradas= $sumEntradas + $e['entradas'];
                                $sumSaidas = $sumSaidas + $e['saidas'];
                                $sumComissao = $sumComissao + $e['comissao'];
                                //$sumComissaoGerente = $sumComissaoGerente + $e['comissao_gerente'];                             
                                //$sumSaldo = $sumSaldo + $e['saldo'];
                                $sumCambistaSaldo = $sumCambistaSaldo + $e['saldo_par'];
                                $sumLancCambista = $sumLancCambista + $e['saldo_lancamento_cambista'];

                                $sumGerenteQtdApostas = $sumGerenteQtdApostas + $e['qtd_apostas'];
                                $sumGerenteEntradas= $sumGerenteEntradas + $e['entradas'];
                                $sumGerenteSaidas = $sumGerenteSaidas  + $e['saidas'] ;
                                $sumGerenteComissao = $sumGerenteComissao + $e['comissao'];
                                $sumGerenteSaldo = $sumGerenteSaldo + $e['saldo'];
                                $sumGerenteCambistaSaldo = $sumGerenteCambistaSaldo + $e['saldo_par'];
                                $sumGerenteLancCambista = $sumGerenteLancCambista + $e['saldo_lancamento_cambista'];
                                
                                $html = $html . "<td style='font-weight: bold; '> " . strtoupper($e['nome']) . " </td> ";
                               $html = $html ."<td align='center'> " . $e['qtd_apostas'] . " </td>
                                      <td align='right'> R$ " . number_format($e['entradas'], 2, ',', '.') . " </td>
                                      <td align='right'> R$ " . number_format($e['saidas'], 2, ',', '.') . " </td>
                                      <td align='right'> R$ " . number_format($e['comissao'], 2, ',', '.') . " </td>
                                      <td align='right'> R$ " . number_format($e['saldo_par'], 2, ',', '.') . " </td>";

                                        $html = $html . "<td align='right'> R$ " . number_format($e['saldo_lancamento_cambista'], 2, ',', '.') . " </td>";
                                        if ($e['saldo_lancamento_gerente'] > 0) {
                                            $html = $html . "<td align='right'> R$ " . number_format($e['saldo_lancamento_gerente'], 2, ',', '.') . " </td>";
                                        } else {
                                            $html = $html . "<td align='center'> - </td>";
                                        }
                                        $saldo_final_linha = $e['saldo'] + $e['saldo_lancamento_cambista'] + $e['saldo_lancamento_gerente'];
                                        if ($saldo_final_linha >= 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_final_linha, 2, ',', '.') . " </td>";
                                        } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_final_linha, 2, ',', '.') . " </td>";
                                        }

                                    $saldo_final_linha = 0;                                                                          
                                    $html = $html . "</tr>";     

                                    $gerenteControle = $e['gerente'];
                                    $sumGerenteLancGerente = $e['saldo_lancamento_gerente'];
                                   
                         }

                         if ($exibeCambista == 'true' && $perfil == "A") 
                             {

                                $sumLancGerente = $sumLancGerente + $sumGerenteLancGerente;
                                $html = $html . "<tr style='background-color: #A9F5BC;'>

                                <th scope='row'> TOTAL </th>";
                                 $html = $html . "<td align='center' style='font-weight: bold;'> " .  $sumGerenteQtdApostas . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteEntradas, 2, ',', '.') . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteSaidas, 2, ',', '.') . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteComissao, 2, ',', '.') . " </td>
                                <td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteCambistaSaldo, 2, ',', '.') . " </td>";
                                                               
                                $sumGerenteComissaoGerente = $comissoes_gerentes_arr[$gerenteControle];

                                if($sumGerenteComissaoGerente > 0)
                                      {
                                        $sumComissaoGerente = $sumComissaoGerente + $sumGerenteComissaoGerente;
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_gerente = $sumGerenteSaldo - $sumGerenteComissaoGerente + $sumGerenteLancCambista + $sumGerenteLancGerente;

                                        if ($saldo_total_gerente >= 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            }
                                      }else
                                      {
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_gerente = $sumGerenteCambistaSaldo + $sumGerenteLancCambista + $sumGerenteLancGerente;
                                        if ($saldo_total_gerente >= 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            }
                                      }

                                      $saldo_total_gerente = 0;
                                
                               
                                $html = $html . "</tr>";

                                $sumGerenteQtdApostas = 0;
                                $sumGerenteEntradas= 0;
                                $sumGerenteSaidas = 0;
                                $sumGerenteComissao = 0;
                                $sumGerenteComissaoGerente = 0;
                                $sumGerenteSaldo = 0;
                                $sumGerenteCambistaSaldo = 0;
                                //$sumGerenteLancGerente = 0;
                                $sumGerenteLancCambista = 0;
                             }
                           
                         $html = $html . "</tbody> 
                            <tr style='background-color: white; color: white; border: 0 !important;'> 
                                <td colspan='9' style='border: 0 !important; height: 26px;'></td> 
                            </tr> 
                            <tr style='background-color: #008800; color: white;'> 
                                <td colspan='9' style='text-align: center; color: white; font-weight: bold;'>
                                    TOTAL GERAL DA BANCA
                                </td> 
                            </tr>
                            <tr style='background-color: #008800; color: white;'>
                                <td style='width: 16%; text-align: center; color: white; font-weight: bold;'></td> 
                                <td style='width: 6%; text-align: center; color: white; font-weight: bold;'>Qtd</td>
                                <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Entradas</td>
                                <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Saídas</td>
                                <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Comissão</td>
                                <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Saldo Camb.</td>
                                <td style='width: 9%; text-align: center; color: white; font-weight: bold;'>Lanc. Cambista</td>
                                <td style='width: 9%; text-align: center; color: white; font-weight: bold;'>Lanc. Gerente</td>
                                <td style='width: 10%; text-align: center; color: white; font-weight: bold;'>Saldo</td>
                            </tr>                                   
                            <tr style='font-weight: bold; background-color: #E6E6E6;'>
                                   <th scope='row'>TOTAL</th>";
                                    $html = $html . "<td align='center' style='font-weight: bold;'> " .  $sumQtdApostas . " </td>
                                   <td align='right' style='font-weight: bold;'> R$ " . number_format($sumEntradas, 2, ',', '.') . " </td>
                                   <td align='right' style='font-weight: bold;'> R$ " . number_format($sumSaidas, 2, ',', '.') . " </td>
                                   <td align='right' style='font-weight: bold;'> R$ " . number_format($sumComissao, 2, ',', '.') . " </td>
                                   <td align='right' style='font-weight: bold;'> R$ " . number_format($sumCambistaSaldo, 2, ',', '.') . " </td>";
                                   

                                   if($sumComissaoGerente > 0)
                                      {
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_geral = $sumEntradas - $sumSaidas - $sumComissao - $sumComissaoGerente + $sumLancCambista + $sumLancGerente;
                                        if ($saldo_total_geral >= 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            }
                                      }else
                                      {
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right' style='font-weight: bold;'> R$ " . number_format($sumLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_geral = $sumEntradas - $sumSaidas - $sumComissao + $sumLancCambista + $sumLancGerente;
                                        if ($saldo_total_geral >= 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            }
                                      }
                                   
                                  
                                   
                                   $html = $html . "</tr>
                     </table>
 
                     
                 </div>
             </div>
     </body> 
 </html> ";

}

if ($prestarConta == 'false') {

    $tipoRelatorio = mysqli_real_escape_string($con, $_GET['tipoRelatorio']);

    $hoje = date('YmdHis');

    if ($tipoRelatorio == 'xlsx') {
        $arquivo = $hoje . "_Relatorio_Prestacao_de_Contas.xlsx";
        header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-type: application/x-msexcel");
        header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        // Imprime o conteúdo da nossa tabela no arquivo que será gerado
        echo $html;    
    } else if ($tipoRelatorio == 'pdf') {
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 
                                'orientation' => 'L', 
                                'margin_left' => '5',
                                'margin_right' => '5',
                                'margin_bottom' => '1',
                                'margin_top' => '2']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output($hoje . "_Relatorio_Prestacao_de_Contas.pdf", "D");
    }    

} else {
    try {
        $con->begin_transaction();
        $sqlUpdate = "UPDATE usuario U set U.saldo = U.limite where U.cod_usuario in (
                        select distinct usu.cod_usuario
                        from jogo
                            inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                            inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                            inner join (select * from usuario) usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                            inner join (select * from usuario) geren on (geren.cod_usuario = usu.cod_gerente)
                        where jogo.cod_site = '$site'
                        and geren.cod_gerente = '$cod_usuario'
                        and bil.status_bilhete not in ('C','F')
                        and apo.status not in ('C') 
                        AND jogo.TP_STATUS = 'P')";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao atualizar o saldo dos cambistas do site ' . $site);
        }

        $sqlUpdate = "UPDATE usuario U set U.saldo = U.limite where U.cod_usuario in (
                        select distinct usu.cod_gerente
                        from jogo
                        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                        inner join (select * from usuario) usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                        inner join (select * from usuario) geren on (geren.cod_usuario = usu.cod_gerente)
                        where jogo.cod_site = '$site'
                        and geren.cod_gerente = '$cod_usuario'
                        and bil.status_bilhete not in ('C','F')
                        and apo.status not in ('C') 
                        AND jogo.TP_STATUS = 'P')";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao atualizar o saldo dos gerentes do site ' . $site);
        }

        $sqlUpdate = "UPDATE bilhete bil
                        set status_bilhete = 'F', DATA_PRESTACAO_CONTA = '$dataPrestacao'
                        where status_bilhete not in ('C','F') 
                            and cod_site = '$site'
                            and cod_bilhete in (select apo.cod_bilhete from jogo
                                                    inner join aposta apo on (jogo.cod_jogo = apo.cod_jogo)
                                                where jogo.cod_site = bil.cod_site
                                                    and apo.status not in ('C') 
                                                    AND jogo.TP_STATUS = 'P') ";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao finalizar os bilhetes do site ' . $site);
        }

        $sqlUpdate = "UPDATE jogo J set TP_status = 'F' where cod_site = '$site' and tp_status = 'P' 
                        and not exists (select 1 from bilhete B,aposta A 
                        where B.cod_bilhete = A.cod_bilhete 
                        and A.cod_jogo = J.COD_JOGO
                        and status_bilhete = 'P')";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao finalizar os jogos do site ' . $site);
        }                    

        $sqlUpdate = "UPDATE lancamento set status = 'I' where cod_site = '$site' and status = 'A'";
        
        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao inativar os lançamentos do site ' . $site);
        }
        
        // printf($html);

        $hoje = date('YmdHis');
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 
                                'orientation' => 'L', 
                                'margin_left' => '5',
                                'margin_right' => '5',
                                'margin_bottom' => '1',
                                'margin_top' => '2']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output($hoje . "_Relatorio_Prestacao_de_Contas.pdf", "D");

        inserir_auditoria(
            $con, 
            $cod_usuario, 
            $site, 
            AUD_ADMIN_PREST_CONTA,
            $html
        );
        
        $con->commit();

    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }
}

/*

$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtpout.secureserver.net';                 // Specify main and backup server
$mail->Port = 465;                                    // Set the SMTP port
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'contato@betloteria';                // SMTP username
$mail->Password = 'Betloteria$4';                  // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'contato@betloteria';
$mail->FromName = 'BET LOTERIA';
$mail->AddAddress('danylocampos@gmail.com', 'Danylo Campos');  // Add a recipient
//$mail->AddAddress('ellen@example.com');               // Name is optional

$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Here is the subject';
$mail->Body    = 'This is the HTML message body <strong>in bold!</strong>';
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->Send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}
*/

$con->close();





?>
