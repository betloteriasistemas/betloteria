<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

try {

    $query = 'SET @@session.time_zone = "-02:00"';

    $result = mysqli_query($con, $query);

    //$cod_jogo = mysqli_real_escape_string($con, $_POST['cod_jogo']);
    $cod_jogo = mysqli_real_escape_string($con, $_GET['cod_jogo']);

    $query = "SELECT TP_STATUS, HORA_EXTRACAO, DATA_JOGO, TIPO_JOGO, CURRENT_TIME AGORA, current_date HOJE from jogo
               WHERE cod_jogo = '$cod_jogo'";

    $result = mysqli_query($con, $query);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if (($row['TP_STATUS'] == 'B') || (($row['TIPO_JOGO'] == 'B' || $row['TIPO_JOGO'] == '2') &&
        $row['HORA_EXTRACAO'] < $row['AGORA'] &&
        $row['DATA_JOGO'] == $row['HOJE'])) {
        throw new Exception('Concurso Bloqueado! Atualize a página e selecione um concurso válido!');
    }

    $tipo_jogo = $row['TIPO_JOGO'];

    if ($tipo_jogo == 'B' || $tipo_jogo == '2') {
        $query = "select (data_jogo < current_date) data_passou,
                     (data_jogo <= current_date) data_retorno,
                    (CURRENT_TIME >= hora_extracao) hora_retorno
                from jogo
                where cod_jogo = '$cod_jogo'";
    } else {
        $query = "select (data_jogo < current_date) data_passou,
                     (data_jogo <= current_date) data_retorno,
                    (CURRENT_TIME >= '19:30') hora_retorno
                from jogo
                where cod_jogo = '$cod_jogo'";
    }

    $result = mysqli_query($con, $query);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
        throw new Exception('Aposta não pode ser feita para esse concurso! Selecione o próximo concurso!');
    }

    $qtdBilhetes = 1;
    if ($tipo_jogo === '2') {
        //$qtdBilhetes = mysqli_real_escape_string($con, $_POST['qtd_bilhetes']);
        $qtdBilhetes = mysqli_real_escape_string($con, $_GET['qtd_bilhetes']);
    }

    for ($i = 1; $i <= $qtdBilhetes; $i++) {

        if ($i > 1 || $tipo_jogo != 'B') {
            $query = "SELECT AUTO_INCREMENT
	            FROM information_schema.tables
               WHERE table_name = 'bilhete'
				 AND table_schema = 'betloteria' ";

            $result = mysqli_query($con, $query);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            $cod_bilhete = $row['AUTO_INCREMENT'];
        } else {
            $cod_bilhete = mysqli_real_escape_string($con, $_POST['numero_bilhete']);
        }

        if ($i == 1) {
            $stmt = $con->prepare("insert into bilhete(cod_bilhete, cod_site, cod_usuario, nome_apostador, telefone_apostador, data_bilhete, txt_pule, numero_bilhete)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?) ");
            $stmt->bind_param("iiissssi", $cod_bilhete, $cod_site, $cod_usuario, $nome_apostador, $telefone_apostador, $data_atual, $pule, $numero_bilhete);
        } else {
            $stmt = $con->prepare("insert into bilhete(cod_bilhete, cod_site, cod_usuario, nome_apostador, telefone_apostador, data_bilhete, txt_pule, numero_bilhete, txt_pule_pai)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ");
            $stmt->bind_param("iiissssis", $cod_bilhete, $cod_site, $cod_usuario, $nome_apostador, $telefone_apostador, $data_atual, $cod_bilhete, $numero_bilhete, $pule);
        }

        //$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
        //$cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        $cod_site = mysqli_real_escape_string($con, $_GET['cod_site']);

        if ($tipo_jogo != '2') {
            $nome_apostador = mysqli_real_escape_string($con, $_POST['nome']);
            $telefone_apostador = mysqli_real_escape_string($con, $_POST['telefone']);
            $numero_bilhete = null;
        }
        //$pule = mysqli_real_escape_string($con, $_POST['pule']);
        $pule = mysqli_real_escape_string($con, $_GET['pule']);
        //$data_atual = mysqli_real_escape_string($con, $_POST['data_atual']);
        $data_atual = mysqli_real_escape_string($con, $_GET['data_atual']);

        if ($tipo_jogo == '2') {
            try {
                if (isset($_POST['numero_bilhete'])) {
                    $numero_bilhete = mysqli_real_escape_string($con, $_POST['numero_bilhete']);
                } else {
                    $numero_bilhete = $i;
                }
            } catch (Exception $e) {

            }
        }

        $stmt->execute();

        if ($tipo_jogo != '2') {
            $apostas = json_decode($_POST['apostas']);
        } else {

            $query = " select distinct a.txt_aposta from aposta a
            where a.cod_jogo = '$cod_jogo' ";

            $result = mysqli_query($con, $query);

            $return_arr = array();

            $contador = 0;

            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $contador = $contador + 1;

                $array = explode('-', str_replace(' ', '', $row['txt_aposta']));
                foreach ($array as $value) {
                    // $row_array['numero'] = $value;
                    array_push($return_arr, $value);

                }

                if ($contador == mysqli_num_rows($result)) {
                    break;
                }
            }

            $contador = 0;
            $numero1 = 0;
            $numero2 = 0;
            $numero3 = 0;
            $numero4 = 0;

            if (count($return_arr) == 10000) {
                $query = "Delete from bilhete where cod_bilhete = '$cod_bilhete' ";
                $stmt = $con->prepare($query);
                $stmt->execute();

                throw new Exception("Número máximo de milhares atingidas nesse concurso!");
                //break;
            }

            if (count($return_arr) == 0) {
                $numero1 = rand(0, 9999);
                $numero2 = rand(0, 9999);
                $numero3 = rand(0, 9999);
                $numero4 = rand(0, 9999);

            } else {
                $contador = 0;
                while ($contador < 4) {

                    $n = str_pad(rand(0, 9999), 4, "0", STR_PAD_LEFT);

                    //echo "ARRAY GERAL: " . "\n";
                    //print_r($return_arr);

                    /*$booDisponivel = false;
                    while ($booDisponivel == false) {
                    
                        if (in_array($n, $return_arr)) {
                            echo "Numero: " . $n . "\n";
                            $n = str_pad(rand(0, 9999), 4, "0", STR_PAD_LEFT);
                        }else{
                            $booDisponivel = true;
                        }
                    }*/
                    while( in_array( ($n = str_pad(rand(0, 9999),4, "0", STR_PAD_LEFT)), $return_arr ) || $n == $numero1 || $n == $numero2 || $n == $numero3 || $n == $numero4  );
                    echo "Numero: " . $n . "\n";

                    $numero = $n;
                    //$numero = str_pad($numero, 4, "0", STR_PAD_LEFT) ;
                    /*$achou = false;
                    foreach ($return_arr as $e) {
                    if ($numero == $e['numero']) {
                    $achou = true;
                    break;
                    }
                    }*/
                    //if ($achou == false) {
                    $contador = $contador + 1;
                    if ($contador == 1) {
                        $numero1 = $numero;
                    } else if ($contador == 2) {
                        $numero2 = $numero;
                    } else if ($contador == 3) {
                        $numero3 = $numero;
                    } else if ($contador == 4) {
                        $numero4 = $numero;
                    }
                    /*}else {
                    $contador = $contador + 1;
                    }*/
                    if ($contador == 4) {
                        break;
                    }
                }
            }

            //throw new Exception("RETURN NU: " . $numero1 . "-". $numero2 . "-". $numero3 . "-". $numero4 );

            //throw new Exception($numero1 . '-' . $numero2 . '-' . $numero3 . '-' . $numero4);

            $apostas = [];
            $numerosRandom['numeros'] = str_pad($numero1, 4, "0", STR_PAD_LEFT) . "-" .
            str_pad($numero2, 4, "0", STR_PAD_LEFT) . "-" .
            str_pad($numero3, 4, "0", STR_PAD_LEFT) . "-" .
            str_pad($numero4, 4, "0", STR_PAD_LEFT);
            array_push($apostas, $numerosRandom);

            print_r($numerosRandom);
            return;
        }

        $contador = 0;
        $valorReal = 0;
        foreach ($apostas as $e) {

            if ($tipo_jogo == 'B') {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, txt_aposta, valor_aposta, possivel_retorno, do_premio, ao_premio, tipo_jogo, inversoes )
            values(?,?,?,?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiisddsssi", $cod_site, $cod_jogo, $cod_bilhete, $e->numeros, $e->valorReal, $e->possivelRetornoReal, $e->premioDoReal, $e->premioAoReal, $e->tipoJogo, $e->inversoes);
            } else if ($tipo_jogo == '2') {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno )
                values(?,?,?,4,?,2,500) ");
                $stmt->bind_param("iiis", $cod_site, $cod_jogo, $cod_bilhete, $e['numeros']);
            } else {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno )
            values(?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiiisdd", $cod_site, $cod_jogo, $cod_bilhete, $e->qtd, $e->numeros, $e->valorReal, $e->possivelRetornoReal);
            }

            if ($tipo_jogo == '2') {
                $valorReal = 2;
            } else {
                $valorReal = $valorReal + $e->valorReal;
            }

            $stmt->execute();

            /*$response['teste' . $contador] = $e->possivelRetorno;
        $contador = $contador + 1;*/
        }

        $stmt = $con->prepare("UPDATE usuario SET SALDO = SALDO - ?
		                    WHERE COD_USUARIO = ? AND COD_SITE = ?");
        $stmt->bind_param("dii", $valorReal, $cod_usuario, $cod_site);
        $stmt->execute();

    }

    $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_SITE = '$cod_site'
              and COD_USUARIO = '$cod_usuario' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $response['saldo'] = $row['SALDO'];
    $response['status'] = "OK";
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

//$stmt->close();
//$con->close();
