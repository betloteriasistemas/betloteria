<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$codigo = mysqli_real_escape_string($con, $_POST['codigo']);
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
$gerente_transferir = mysqli_real_escape_string($con, $_POST['gerente_transferir']);

$query = " SELECT PERFIL FROM usuario
        WHERE COD_USUARIO = '$codigo' ";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$perfil  = $row['PERFIL'];

try {
    if ($perfil == "A") {
        throw new Exception('Não é possível excluir este usuário pois ele tem perfil ADMIN.');
    }

    $query = " SELECT COUNT(*) AS CONT FROM bilhete WHERE COD_USUARIO = '$codigo' AND STATUS_BILHETE IN ('A') ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if ($row['CONT'] > 0) {
        throw new Exception('Não é possível excluir este usuário pois ele ainda possui bilhetes em aberto.');
    }    

    $con->begin_transaction();

    if ($gerente_transferir != '') {
        $sqlUpdate = "UPDATE usuario SET cod_gerente = '$gerente_transferir' WHERE cod_gerente = '$codigo'";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao transferir os cambistas do gerente.');
        }
    }

    $sqlDelete = "DELETE FROM areas_gerencia WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os registros de mapeamento com as áreas.');
    }

    $sqlDelete = "DELETE FROM auditoria WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os registros de auditoria do usuário.');
    }

    $sqlDelete = "DELETE FROM aposta WHERE cod_bilhete in (SELECT cod_bilhete FROM bilhete WHERE cod_usuario = '$codigo')";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover as apostas criadas pelo usuário.');
    }

    $sqlDelete = "DELETE FROM bilhete WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os bilhetes do usuário.');
    }

    $sqlDelete = "DELETE FROM configuracao_comissao_bicho WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os registros das comissões do usuário.');
    }

    $sqlDelete = "DELETE FROM lancamento WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os registros dos lancamentos do usuário.');
    }

    $sqlDelete = "DELETE FROM permissao_concessao WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os registros das permissões do usuário.');
    }

    $sqlDelete = "DELETE FROM cliente_cambista WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover os registros dos clientes dos cambistas.');
    }

    $sqlDelete = "DELETE FROM usuario WHERE cod_usuario = '$codigo'";

    if ($con->query($sqlDelete) != true) {
        throw new Exception('Erro ao remover o registro do usuário.');
    }

    $con->commit();
    $response['status'] = "OK";
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

$con->close();
