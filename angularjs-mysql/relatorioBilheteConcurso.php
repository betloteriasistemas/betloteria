<?php

include "conexao.php";
require_once 'vendor/autoload.php';
require_once('funcoes_auxiliares.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
ini_set('memory_limit','256M');
set_time_limit(120);
if (!isset($_GET)) {
    die();
}

$site = mysqli_real_escape_string($con, $_GET['site']);
$concurso = mysqli_real_escape_string($con, $_GET['concurso']);

$stmtJogo = $con->prepare("SELECT TIPO_JOGO, DATE_FORMAT(DATA_JOGO, '%d/%m/%Y') AS DATA_JOGO, CONCURSO, TIME_FORMAT(HORA_EXTRACAO, '%H:%i') AS HORA_EXTRACAO, DESC_HORA 
                           FROM jogo WHERE COD_SITE = ? AND COD_JOGO = ?");
$stmtJogo->bind_param("ii", $site, $concurso);
$stmtJogo->execute();
$jogo = $stmtJogo->get_result();
$row_jogo = $jogo->fetch_array(MYSQLI_ASSOC);
$stmtJogo->close();

$stmt = $con->prepare("SELECT b.COD_BILHETE, DATE_FORMAT(DATA_BILHETE, '%d/%m/%Y %H:%i:%S') AS DATA_BILHETE, a.TXT_APOSTA, a.VALOR_APOSTA, a.POSSIVEL_RETORNO 
                       FROM bilhete b 
                       INNER JOIN aposta a ON (b.COD_BILHETE = a.COD_BILHETE)
                       WHERE b.COD_SITE = ? AND b.STATUS_BILHETE != 'C' AND a.COD_JOGO = ?
                       ORDER BY b.COD_BILHETE"
                    );
$stmt->bind_param("ii", $site, $concurso);
$stmt->execute();
$result = $stmt->get_result();
$stmt->close();
$html = gerarHtml($row_jogo, $result);

$con->close();
$result->close();
$jogo->close();
$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
$mpdf->SetDisplayMode('fullpage');
$long_html = strlen($html);
$long_int  = intval($long_html/1000000);

try {
    if($long_int > 0) {
        for($i = 0; $i<$long_int; $i++) {
            $temp_html = substr($html, ($i*1000000),999999);
            $mpdf->WriteHTML($temp_html);
        }
        $temp_html = substr($html, ($i*1000000),($long_html-($i*1000000)));
        $mpdf->WriteHTML($temp_html);
    } else {
        $mpdf->WriteHTML($html);
    }
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

$hoje = date('YmdHis');
$mpdf->Output($hoje . "_" . $row_jogo['CONCURSO'] . "_Relatorio_Bilhetes_Concurso.pdf", "D");

function gerarHtml($row, $result) {
    $cabecalho = "<div> RELATÓRIO DE BILHETES POR CONCURSO </div>";
    $cabecalho = $cabecalho . "<div>" . converterTipoJogo($row['TIPO_JOGO']) .
    " - " . converterDescricaoJogo($row['TIPO_JOGO'], $row['DATA_JOGO'], $row['CONCURSO'], $row['HORA_EXTRACAO'], $row['DESC_HORA']) . "</div>";
    $html =  "
                <html>
                    <head>
                        <style>
                            table, th, td {
                                border: 1px solid black;
                                border-collapse: collapse;
                                height: 50px;
                            }
                            th, td {
                                padding: 10px;
                            }
                        </style>
                    </head>
                    <body>
                        <div class='col-md-12 col-ms-12 col-xs-12'>
                            <div class='table-responsive betL_CaixaList'> 
                                <h1>" . $cabecalho . "</h1>
                            <div>
                            &nbsp;
                        </div>
                        <table class='table table-hover' style='width: 100%;'>
                            <thead>
                                <tr style='background-color: #2E64FE; color: white;'>
                                    <th style='width: 15%; text-align: center; color: white; font-weight: bold;'><a>BILHETE</a></th>
                                    <th style='width: 15%; text-align: center; color: white; font-weight: bold;'><a>DATA</a></th>
                                    <th style='width: 50%; text-align: center; color: white; font-weight: bold;'><a>APOSTA</a></th>
                                    <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>VALOR APOSTA</th>
                                    <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>POSSÍVEL RETORNO</th>
                                </tr>
                            </thead>
                            <tbody class='container'>";
                            while ($row = $result->fetch_array(MYSQLI_ASSOC)){
                                $html = $html . "<tr>";
                                $html = $html . "<td style='text-align: center;'><span>" . $row['COD_BILHETE'] . "</span></td>";
                                $html = $html . "<td style='text-align: center;'><span>" . $row['DATA_BILHETE'] . "</span></td>";
                                $html = $html . "<td><span>" . $row['TXT_APOSTA'] . "</span></td>";
                                $html = $html . "<td style='text-align: center;'><span>" . number_format ($row['VALOR_APOSTA'], 2, ",", ".") . "</span></td>";
                                $html = $html . "<td style='text-align: center;'><span>" . number_format ($row['POSSIVEL_RETORNO'], 2, ",", ".") . "</span></td>";
                                $html = $html . "</tr>";
                            }
                            $html = $html . 
                            "</tbody>
                        </table>
                        </div>
                    </div>
                </body>
            </html>";
    return $html;
}
