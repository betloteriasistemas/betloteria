angular.module('main').controller('dashboardCtrl', function ($scope, user, $http) {
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.resultado = [];
    $scope.edtNumero = "";
    $scope.dataHoje = new Date();
    $scope.dia = $scope.dataHoje.getDay();

    $scope.buscarResultadoAtual = function () {
        $http({
            url: 'angularjs-mysql/resultados.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'numero=' + $scope.edtNumero
        }).then(function (response) {
            $scope.resultado = response.data;
            console.log($scope.resultado);
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        })
    }
});