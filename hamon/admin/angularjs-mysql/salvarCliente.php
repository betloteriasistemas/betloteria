<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

//$operacao = mysqli_real_escape_string($con, $_GET['operacao']);

$codigo = mysqli_real_escape_string($con, $_POST['codigo']);
$nome = mysqli_real_escape_string($con, $_POST['nome']);
$responsavel = mysqli_real_escape_string($con, $_POST['responsavel']);
$telefone = mysqli_real_escape_string($con, $_POST['telefone']);
$banca = mysqli_real_escape_string($con, $_POST['banca']);
$status = mysqli_real_escape_string($con, $_POST['status']);
$obs = mysqli_real_escape_string($con, $_POST['obs']);
$mensalidade = mysqli_real_escape_string($con, $_POST['mensalidade']);
$vencimento = mysqli_real_escape_string($con, $_POST['vencimento']);
$promocao = mysqli_real_escape_string($con, $_POST['promocao']);
$booInsere = false;
$nomeSchema = "b" . $codigo;

try {

    $query = " SELECT count(*) qtd FROM site
                WHERE COD_SITE = '$codigo'";

    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if ($row['qtd'] == 0) {
        $stmt = $con->prepare("INSERT INTO site(COD_SITE, TXT_EMPRESA, TXT_RESPONSAVEL, TXT_TELEFONE, NOME_BANCA, STATUS, OBS, MENSALIDADE, VENCIMENTO, PROMOCAO)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
        $stmt->bind_param("isssssssss", $codigo, $nome, $responsavel, $telefone, $banca, $status, $obs, $mensalidade, $vencimento, $promocao);

        $booInsere = true;
    } else {
        $stmt = $con->prepare("UPDATE site SET TXT_EMPRESA = ?, TXT_RESPONSAVEL = ?, TXT_TELEFONE = ?,NOME_BANCA = ?,STATUS = ?,OBS = ?,MENSALIDADE = ?, VENCIMENTO = ?, PROMOCAO = ?
		                       WHERE COD_SITE = ? ");
        $stmt->bind_param("sssssssssi", $nome, $responsavel, $telefone, $banca, $status, $obs, $mensalidade, $vencimento, $promocao, $codigo);
    }

    $stmt->execute();


    if ($booInsere) {
        if (!$con->query("CALL criar_cliente('" . $nomeSchema . "', " . $codigo . ")")) {
            echo "CALL failed: (" . $con->error . ") " . $con->error;
        }       
    }


    $response['status'] = "OK";
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

$stmt->close();
$con->close();
