angular.module('main').controller('prestacaoContaCtrl', function ($rootScope, $scope, user, $http, $filter, $log) {
    $rootScope.loading = false;
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.chkExibirCambistas = false;

    $scope.records = [];
    $scope.ordenacao = false;
    $scope.campoOrdenacao = "";

    // PAGINAçÃO    

    $scope.currentPage = 1;
    $scope.itemsPerPage = 8;

    $scope.tipo_relatorio = 'pdf';
    $scope.eh_pdf = true;
    
    $scope.alteraTipoRelatorio = function() {
        if ($scope.eh_pdf) {
            $scope.eh_pdf = true;
            $scope.tipo_relatorio = 'pdf';
        } else {
            $scope.eh_pdf = false;
            $scope.tipo_relatorio = 'xlsx';
        }
    }


    $scope.orderFilter = function (campo) {
        if ($scope.campoOrdenacao == "") {
            $scope.campoOrdenacao = campo;
        }

        if ($scope.campoOrdenacao == campo) {
            $scope.ordenacao = !$scope.ordenacao;
        } else {
            $scope.campoOrdenacao = campo;
            $scope.ordenacao = false;
        }

        var sAux = "";
        if (!$scope.ordenacao) {
            sAux = "-";
        }

        $scope.records = $filter('orderBy')($scope.records, sAux + campo);
    };


    $scope.listarCaixa = function () {


        $http({
            method: "POST",
            url: "angularjs-mysql/prestacaoConta.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&perfil=' + $scope.perfil + 
                  '&cod_usuario=' + user.getCodigo() + 
                  '&exibeCambista=' + $scope.chkExibirCambistas +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;
            $scope.totalItems = $scope.records.length;
            $scope.totalItems = Math.ceil($scope.totalItems / 8) * 10;
            $scope.ordenacao = true;


        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.exibirCambistas = function () {
        $scope.chkExibirCambistas = !$scope.chkExibirCambistas;
        $scope.records = [];
        $scope.listarCaixa();
    }

    $scope.prestarConta = function () {
        $.confirm({
            title: '',
            content:"Ao confirmar a prestação de contas todos os usuários serão bloqueados, os bilhetes serão finalizados, um documento para controle será gerado, esse procedimento não poderá ser revertido. TEM CERTEZA ? ",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    window.open('angularjs-mysql/prestarConta.php?nome_usuario=' + user.getNome() +
                    '&site=' + user.getSite() + 
                    '&perfil=' + $scope.perfil + 
                    '&cod_usuario=' + user.getCodigo() + 
                    '&schema=' + user.getSchema() +
                    '&exibeCambista=true' +
                    '&prestarConta=true');
                }
            }
        })
    }

    $scope.gerarRelatorio = function () {
        window.open('angularjs-mysql/prestarConta.php?nome_usuario=' + user.getNome() +
                        '&site=' + user.getSite() + 
                        '&perfil=' + $scope.perfil + 
                        '&cod_usuario=' + user.getCodigo() + 
                        '&schema=' + user.getSchema() +
                        '&exibeCambista=true' +
                        '&tipoRelatorio=' + $scope.tipo_relatorio +
                        '&prestarConta=false');
    }    

    $scope.getTotalizador = function (campo) {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")
                &&
                (typeof $scope.comboTipoJogo === "undefined" ||
                    $scope.comboTipoJogo == $scope.records[i].tipo_jogo ||
                    $scope.comboTipoJogo == "")

            ) {
                if (campo == "entradas") {
                    total += parseFloat($scope.records[i].entradas);
                } else if (campo == "saidas") {
                    total += parseFloat($scope.records[i].saidas);
                } else if (campo == "saldo") {
                    total += parseFloat($scope.records[i].saldo);
                } else if (campo == "comissao") {
                    total += parseFloat($scope.records[i].comissao);
                } else if (campo == "comissao_gerente") {
                    total += parseFloat($scope.records[i].comissao_gerente);
                } else if (campo == "qtd_apostas") {
                    total += parseFloat($scope.records[i].qtd_apostas);
                } else if (campo == "saldo_par") {
                    total += parseFloat($scope.records[i].saldo_par);
                } else if (campo == "saldo_lancamento_cambista") {
                    total += parseFloat($scope.records[i].saldo_lancamento_cambista);
                } else if (campo == "saldo_lancamento_gerente") {
                    total += parseFloat($scope.records[i].saldo_lancamento_gerente);
                }
            }
        }
        if (campo != "qtd_apostas") {
            return total.toFixed(2);
        } else {
            return total;
        }

    }

    $scope.getTotalEntradas = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")

            ) {
                total += parseFloat($scope.records[i].entradas);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalSaidas = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].saidas);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalSaldo = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].saldo);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalComissao = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].comissao);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalComissaoGerente = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].comissao_gerente);
            }
        }
        return total.toFixed(2);
    }

});