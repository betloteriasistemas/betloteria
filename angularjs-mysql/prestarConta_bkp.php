<?php

include "conexao.php";
require_once 'dompdf/autoload.inc.php';
require_once('auditoria.php');
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_GET)) {
    die();
}


use Dompdf\Dompdf;


$dompdf = new Dompdf();



$site = mysqli_real_escape_string($con, $_GET['site']);
$perfil = mysqli_real_escape_string($con, $_GET['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$nome_usuario = mysqli_real_escape_string($con, $_GET['nome_usuario']);
$exibeCambista = mysqli_real_escape_string($con, $_GET['exibeCambista']);


if ($perfil == "G") {
    $query = "select g.*, g.saldo_par - (IFNULL(g.comissao_gerente,0)) as saldo from (
        select v.*, (v.entradas - v.saidas - v.comissao) saldo_par,
        round(((select (CASE WHEN v.Tipo_Jogo = 'S' THEN pct_comissao_seninha 
        WHEN v.Tipo_Jogo = 'Q' THEN  pct_comissao_quininha 
        WHEN v.Tipo_Jogo in ('B','G') THEN  pct_comissao_bicho 
        WHEN v.Tipo_Jogo = '2' THEN  pct_comissao_2PRA500 
        WHEN v.Tipo_Jogo = '6' THEN  pct_comissao_6DASORTE ELSE 0 END) from usuario where cod_usuario = '$cod_usuario') * (v.entradas - v.saidas - v.comissao)) / 100 ,2) comissao_gerente
        from (
            select usu.cod_usuario, usu.nome, null as gerente,
            sum(apo.valor_aposta) entradas,
            count(apo.cod_aposta) qtd_apostas,
            sum(apo.comissao) as comissao,
            sum(apo.valor_ganho) as saidas
            from jogo
            inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
            inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
            inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
            where jogo.cod_site = '$site'
            and usu.cod_gerente = '$cod_usuario'
              and bil.status_bilhete not in ('C','F')
              and apo.status not in ('C') 
            AND jogo.TP_STATUS = 'P'
            group by usu.cod_usuario, usu.nome) v where 1 =1 order by nome desc) g; ";
    } else if ($perfil == "A") {
        if($exibeCambista == 'false')
        {
            $query = "
            select g.*, g.saldo_par - (IFNULL(g.comissao_gerente,0)) as saldo  from (
                select v.*, (v.entradas - v.saidas - v.comissao)  saldo_par,
                round((v.pct_comissao * (v.entradas - v.saidas - v.comissao)) /100,2) comissao_gerente
                from (
                 select geren.cod_usuario, geren.nome,  null as gerente,
                 (CASE WHEN jogo.Tipo_Jogo = 'S' THEN geren.pct_comissao_seninha 
                    WHEN jogo.Tipo_Jogo = 'Q' THEN  geren.pct_comissao_quininha 
                    WHEN jogo.Tipo_Jogo in ('B','G') THEN  geren.pct_comissao_bicho 
                    WHEN jogo.Tipo_Jogo = '2' THEN  geren.pct_comissao_2PRA500 
                    WHEN jogo.Tipo_Jogo = '6' THEN  geren.pct_comissao_6DASORTE ELSE 0 END) as pct_comissao,
                 sum(apo.valor_aposta) entradas,
                 count(apo.cod_aposta) qtd_apostas,
                 sum(apo.comissao) as comissao,
                
                 sum(apo.valor_ganho) as saidas
                 from jogo
                 inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                 inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                 inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                 inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                 left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                 where jogo.cod_site = '$site'
                 and geren.cod_gerente = '$cod_usuario'
                 and bil.status_bilhete not in ('C','F')
                 and apo.status not in ('C') 
                 AND jogo.TP_STATUS = 'P'
                 group by geren.cod_usuario, geren.nome) v where 1 =1 order by nome desc) g";
        } else{
           /* $query = "select cod_usuario,nome, ID_GERENTE,gerente,sum(entradas) as entradas, sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
            round((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))) /100,2) as comissao_gerente,
            (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
            ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - ((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)))/100)) as saldo
                from (
                    select  usu.cod_usuario,usu.nome, geren.cod_usuario as ID_GERENTE, geren.nome as gerente,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
                            when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
                            else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status,  jogo.tipo_jogo, geren.pct_comissao,
                    sum(apo.valor_aposta) entradas,
                    count(apo.cod_aposta) qtd_apostas,
                    (sum(apo.valor_aposta) * usu.pct_comissao) / 100 as comissao,
                    sum(case when apo.status = 'G' then apo.possivel_retorno else 0 end) as saidas
                    from jogo
                    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                    inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                    where jogo.cod_site = '$site'
                    and geren.cod_gerente = '$cod_usuario'
                    and bil.status_bilhete not in ('C','F')
                    and apo.status not in ('C') 
                    AND jogo.TP_STATUS = 'P'
                    group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status, jogo.tipo_jogo) 
                    v where 1 =1 group by cod_usuario,nome, ID_GERENTE,gerente order by gerente,nome desc "; */
                    
                    $query = "select cod_usuario,nome, ID_GERENTE,gerente,sum(entradas) as entradas, sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
                    round((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))) /100,2) as comissao_gerente,
                    (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
                    ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - ((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)))/100)) as saldo,
                    COALESCE(saldo_lancamento_cambista,0) as saldo_lancamento_cambista,
                    COALESCE(saldo_lancamento_gerente,0) as saldo_lancamento_gerente,
                    (((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - ((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)))/100))+saldo_lancamento_cambista) as saldo_pos_lancamento
                        from (
                            select  usu.cod_usuario,usu.nome, geren.cod_usuario as ID_GERENTE, geren.nome as gerente,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
                                    when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
                                    else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status,  jogo.tipo_jogo, 
                                    (CASE WHEN jogo.Tipo_Jogo = 'S' THEN geren.pct_comissao_seninha 
                                    WHEN jogo.Tipo_Jogo = 'Q' THEN  geren.pct_comissao_quininha 
                                    WHEN jogo.Tipo_Jogo in ('B','G') THEN  geren.pct_comissao_bicho 
                                    WHEN jogo.Tipo_Jogo = '2' THEN  geren.pct_comissao_2PRA500 
                                    WHEN jogo.Tipo_Jogo = '6' THEN  geren.pct_comissao_6DASORTE ELSE 0 END) as pct_comissao,
                            sum(apo.valor_aposta) entradas,
                            count(apo.cod_aposta) qtd_apostas,
                            sum(apo.comissao) as comissao,
                            sum(apo.valor_ganho) as saidas,
                            (select sum(case when tipo_lancamento = 'E' then (valor) else (valor*-1) end)  from lancamento where cod_site = '$site' and COD_USUARIO_lancamento = geren.cod_usuario and status = 'A' and ajuda_custo = 'N') saldo_lancamento_gerente,
                            (select sum(case when tipo_lancamento = 'E' then (valor) else (valor*-1) end)  from lancamento where cod_site = '$site' and COD_USUARIO_lancamento = usu.cod_usuario and status = 'A' and ajuda_custo = 'N') saldo_lancamento_cambista
                            from jogo
                            inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                            inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                            inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                            inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                            left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                            where jogo.cod_site = '$site'
                            and geren.cod_gerente = '$cod_usuario'
                            and bil.status_bilhete not in ('C','F')
                            and apo.status not in ('C') 
                            AND jogo.TP_STATUS = 'P'
                            group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status, jogo.tipo_jogo, apo.cod_aposta) 
                            v where 1 =1 group by cod_usuario,nome, ID_GERENTE,gerente order by gerente,nome desc;";
        }
    
        
}



$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    
    $row_array['cod_usuario'] = $row['cod_usuario'];
    $row_array['nome'] = $row['nome'];
    $row_array['saldo'] = $row['saldo'];
    $row_array['entradas'] = $row['entradas'];
    $row_array['qtd_apostas'] = $row['qtd_apostas'];
    $row_array['comissao'] = $row['comissao'];
    $row_array['saidas'] = $row['saidas'];
    $row_array['comissao_gerente'] = $row['comissao_gerente'];
    $row_array['gerente'] = $row['gerente'];
    $row_array['saldo_par'] = $row['saldo_par'];

    $row_array['saldo_lancamento_gerente'] = $row['saldo_lancamento_gerente'];
    $row_array['saldo_lancamento_cambista'] = $row['saldo_lancamento_cambista'];


    array_push($return_arr, $row_array);

    

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
};

$sumQtdApostas = 0;
$sumEntradas= 0;
$sumSaidas = 0;
$sumComissao = 0;
$sumComissaoGerente = 0;
$sumSaldo = 0;
$sumCambistaSaldo =0;
$dataAtual = date('d/m/Y', time());
$dataPrestacao = date('Y-m-d', time());
$sumLancCambista = 0;
$sumLancGerente = 0;

$gerenteControle = "";

$sumGerenteQtdApostas = 0;
$sumGerenteEntradas= 0;
$sumGerenteSaidas = 0;
$sumGerenteComissao = 0;
$sumGerenteComissaoGerente = 0;
$sumGerenteSaldo = 0;
$sumGerenteCambistaSaldo =0;
$sumGerenteLancCambista = 0;
$sumGerenteLancGerente = 0;


$saldo_final_linha = 0;
$saldo_total_gerente = 0;
$saldo_total_geral = 0;

if (count($return_arr) == 0) {

    $html = "
   <html>
<body>
<hr>
  <h1> Nenhuma prestação de conta </h1>
<hr>
</body>
</html> ";
} else {
    $html = "
    <html>
 <head>
 <style>
 table, th, td {
     border: 1px solid black;
     border-collapse: collapse;
 }
 th, td {
     padding: 10px;
 }
 </style>
 </head>
 <body>
 <div class='col-md-12 col-ms-12 col-xs-12'>
                 <div class='table-responsive betL_CaixaList'>
 
                 <h1> PRESTAÇÃO DE CONTAS - " . $dataAtual .  " </h1>
                 
                 <div>&nbsp;</div>
            
                 
                     <table class='table table-hover' style='width: 980px;'>
                         <thead>
                             <tr style='background-color: #2E64FE; color: white;'>";
                                 
                                 if ($perfil == "A") {
                                     if($exibeCambista == 'true')
                                     {
                                        $html = $html . "<th style='width: 80px; text-align: center;'>Cambista</th>";
                                     }
                                     $html = $html . "<th style='width: 80px; text-align: center;'>Gerente</th>";
                                 } else {
                                     $html = $html . "<th style='width: 80px; text-align: center;'>Cambista</th>";
                                 }
                                 $html = $html . "<th style='width: 30px; text-align: center;'>Qtd</th>
                                 <th style='width: 60px; text-align: center;'><a>Entradas</a></th>
                                 <th style='width: 60px; text-align: center;'><a>Saídas </a></th>
                                 <th style='width: 60px; text-align: center;'>Comissão</th>
                                 <th style='width: 60px; text-align: center;'>Saldo Camb.</th>
                                 <th style='width: 60px; text-align: center;'>Com. Gerente</th>
                                 <th style='width: 60px; text-align: center;'>Lanc. Cambista</th>
                                 <th style='width: 60px; text-align: center;'>Lanc. Gerente</th>
                                 <th style='width: 90px; text-align: center;'>Saldo</th>
                             </tr>
                         </thead>
                         <tbody class='container'>";
                         $odd = 0;           
                         foreach ($return_arr as $e) {
                             $odd = $odd + 1;
                             
                             
                            if (($gerenteControle != $e['gerente']) && ($gerenteControle != "")) 
                             {
                                $sumLancGerente = $sumLancGerente + $sumGerenteLancGerente;
                                $html = $html . "<tr style='background-color: #A9F5BC;'>

                                <th scope='row'> TOTAL </th>";
                                if($exibeCambista == 'true' & $perfil == "A")
                                {
                                 $html = $html . "<td> - </td> ";
                                 } 
                                 $html = $html . "<td align='center'> " .  $sumGerenteQtdApostas . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteEntradas, 2, ',', '.') . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteSaidas, 2, ',', '.') . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteComissao, 2, ',', '.') . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteCambistaSaldo, 2, ',', '.') . " </td>";
                               
                                

                                if($sumGerenteComissaoGerente > 0)
                                      {
                                        $saldo_total_gerente = $sumGerenteSaldo + $sumGerenteLancCambista + $sumGerenteLancGerente;
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteComissaoGerente, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                        if ($saldo_total_gerente > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            }
                                      }else
                                      {
                                        $saldo_total_gerente = $sumGerenteCambistaSaldo + $sumGerenteLancCambista + $sumGerenteLancGerente;
                                        $html = $html . "<td align='right'> R$ " . number_format('0', 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                        if ($saldo_total_gerente > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            }
                                      }
                                
                                      $saldo_total_gerente = 0;
                                $html = $html . "</tr>";

                                $html = $html . "<tr style='background-color: #002db3;'> 
                                <td></td> 
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>";

                                $sumGerenteQtdApostas = 0;
                                $sumGerenteEntradas= 0;
                                $sumGerenteSaidas = 0;
                                $sumGerenteComissao = 0;
                                $sumGerenteComissaoGerente = 0;
                                $sumGerenteSaldo = 0;
                                $sumGerenteCambistaSaldo = 0;
                                $sumGerenteLancCambista = 0;
                             }

                             if($odd % 2 == 0)
                             {
                                 $html = $html . "<tr style='background-color: #E6E6E6;'>";
                             } else {
                                 $html = $html . "<tr>";
                             }

                                $sumQtdApostas = $sumQtdApostas + $e['qtd_apostas'];
                                $sumEntradas= $sumEntradas + $e['entradas'];
                                $sumSaidas = $sumSaidas + $e['saidas'];
                                $sumComissao = $sumComissao + $e['comissao'];
                                $sumComissaoGerente = $sumComissaoGerente + $e['comissao_gerente'];
                                $sumSaldo = $sumSaldo + $e['saldo'];
                                $sumCambistaSaldo = $sumCambistaSaldo + $e['saldo_par'];
                                $sumLancCambista = $sumLancCambista + $e['saldo_lancamento_cambista'];

                                $sumGerenteQtdApostas = $sumGerenteQtdApostas + $e['qtd_apostas'];
                                $sumGerenteEntradas= $sumGerenteEntradas + $e['entradas'];
                                $sumGerenteSaidas = $sumGerenteSaidas  + $e['saidas'] ;
                                $sumGerenteComissao = $sumGerenteComissao + $e['comissao'];
                                $sumGerenteComissaoGerente = $sumGerenteComissaoGerente + $e['comissao_gerente'];
                                $sumGerenteSaldo = $sumGerenteSaldo + $e['saldo'];
                                $sumGerenteCambistaSaldo = $sumGerenteCambistaSaldo + $e['saldo_par'];
                                $sumGerenteLancCambista = $sumGerenteLancCambista + $e['saldo_lancamento_cambista'];
                                
                                $html = $html . "<td style='font-weight: bold;'> " . $e['nome'] . " </td> ";
                                       if($exibeCambista == 'true' && $perfil == "A")
                                        {
                                           $html = $html . "<td> " . $e['gerente'] . " </td> ";
                                        }
                               $html = $html ."<td align='center'> " . $e['qtd_apostas'] . " </td>
                                      <td align='right'> R$ " . number_format($e['entradas'], 2, ',', '.') . " </td>
                                      <td align='right'> R$ " . number_format($e['saidas'], 2, ',', '.') . " </td>
                                      <td align='right'> R$ " . number_format($e['comissao'], 2, ',', '.') . " </td>
                                      <td align='right'> R$ " . number_format($e['saldo_par'], 2, ',', '.') . " </td>";

                                      if($e['comissao_gerente'] > 0)
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format($e['comissao_gerente'], 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($e['saldo_lancamento_cambista'], 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='center'> - </td>";
                                        $saldo_final_linha = $e['saldo'] + $e['saldo_lancamento_cambista'];
                                        if ($saldo_final_linha > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_final_linha, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_final_linha, 2, ',', '.') . " </td>";
                                            }
                                      }else
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format('0', 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($e['saldo_lancamento_cambista'], 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='center'> - </td>";
                                        $saldo_final_linha = $e['saldo_par'] + $e['saldo_lancamento_cambista'];
                                        if ($saldo_final_linha > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_final_linha, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_final_linha, 2, ',', '.') . " </td>";
                                            }
                                      }

                                      $saldo_final_linha = 0;
                                      
                                      
                                      
                                      $html = $html . "</tr>";
                            
                             
                             
                             

                                   $gerenteControle = $e['gerente'];
                                   $sumGerenteLancGerente = $e['saldo_lancamento_gerente'];
                                   
                         }

                         if ($exibeCambista == 'true' && $perfil == "A") 
                             {

                                $sumLancGerente = $sumLancGerente + $sumGerenteLancGerente;
                                $html = $html . "<tr style='background-color: #A9F5BC;'>

                                <th scope='row'> TOTAL </th>";
                                if($exibeCambista == 'true' & $perfil == "A")
                                {
                                 $html = $html . "<td> - </td> ";
                                 } 
                                 $html = $html . "<td align='center'> " .  $sumGerenteQtdApostas . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteEntradas, 2, ',', '.') . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteSaidas, 2, ',', '.') . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteComissao, 2, ',', '.') . " </td>
                                <td align='right'> R$ " . number_format($sumGerenteCambistaSaldo, 2, ',', '.') . " </td>";
                               


                                if($sumGerenteComissaoGerente > 0)
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteComissaoGerente, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_gerente = $sumGerenteSaldo + $sumGerenteLancCambista + $sumGerenteLancGerente;

                                        if ($saldo_total_gerente > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            }
                                      }else
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format('0', 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumGerenteLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_gerente = $sumGerenteCambistaSaldo + $sumGerenteLancCambista + $sumGerenteLancGerente;
                                        if ($saldo_total_gerente > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_gerente, 2, ',', '.') . " </td>";
                                            }
                                      }

                                      $saldo_total_gerente = 0;
                                
                               
                                $html = $html . "</tr>";

                                $html = $html . "<tr style='background-color: #002db3;'> 
                                <td></td> 
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>";

                                $sumGerenteQtdApostas = 0;
                                $sumGerenteEntradas= 0;
                                $sumGerenteSaidas = 0;
                                $sumGerenteComissao = 0;
                                $sumGerenteComissaoGerente = 0;
                                $sumGerenteSaldo = 0;
                                $sumGerenteCambistaSaldo = 0;
                                //$sumGerenteLancGerente = 0;
                                $sumGerenteLancCambista = 0;
                             }
                           
                         $html = $html . "</tbody>  
                         <tfoot><tr style='font-weight: bold; background-color: #A9F5BC;'>
                                   <th scope='row'> TOTAL GERAL</th>";
                                   if($exibeCambista == 'true' & $perfil == "A")
                                   {
                                    $html = $html . "<td> - </td> ";
                                    } 
                                    $html = $html . "<td align='center'> " .  $sumQtdApostas . " </td>
                                   <td align='right'> R$ " . number_format($sumEntradas, 2, ',', '.') . " </td>
                                   <td align='right'> R$ " . number_format($sumSaidas, 2, ',', '.') . " </td>
                                   <td align='right'> R$ " . number_format($sumComissao, 2, ',', '.') . " </td>
                                   <td align='right'> R$ " . number_format($sumCambistaSaldo, 2, ',', '.') . " </td>";
                                   

                                   if($sumComissaoGerente > 0)
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format($sumComissaoGerente, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_geral = $sumSaldo + $sumLancCambista + $sumLancGerente;
                                        if ($saldo_total_geral > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            }
                                      }else
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format('0', 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumLancCambista, 2, ',', '.') . " </td>";
                                        $html = $html . "<td align='right'> R$ " . number_format($sumLancGerente, 2, ',', '.') . " </td>";
                                        $saldo_total_geral = $sumCambistaSaldo + $sumLancCambista + $sumLancGerente;
                                        if ($saldo_total_geral > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total_geral, 2, ',', '.') . " </td>";
                                            }
                                      }
                                   
                                  
                                   
                                   $html = $html . "</tr>
 
                         </tfoot> 
                     </table>
 
                     
                 </div>
             </div>
     </body> 
 </html> ";

}


/*Bloqueia cambistas e Gerentes*/



$stmt = $con->prepare("UPDATE usuario U set U.saldo = U.limite where U.cod_usuario in (select distinct usu.cod_usuario
from jogo
inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
inner join (select * from usuario) usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
inner join (select * from usuario) geren on (geren.cod_usuario = usu.cod_gerente)
where jogo.cod_site = ?
and geren.cod_gerente = ?
  and bil.status_bilhete not in ('C','F')
  and apo.status not in ('C') 
AND jogo.TP_STATUS = 'P')");
$stmt->bind_param("ii", $site, $cod_usuario);
$stmt->execute();

$stmt4 = $con->prepare("UPDATE usuario U set U.saldo = U.limite where U.cod_usuario in (select distinct usu.cod_gerente
from jogo
inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
inner join (select * from usuario) usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
inner join (select * from usuario) geren on (geren.cod_usuario = usu.cod_gerente)
where jogo.cod_site = ?
and geren.cod_gerente = ?
  and bil.status_bilhete not in ('C','F')
  and apo.status not in ('C') 
AND jogo.TP_STATUS = 'P')");
$stmt4->bind_param("ii", $site, $cod_usuario);
$stmt4->execute();




/*Finaliza Bilhetes*/



$stmt3 = $con->prepare("UPDATE bilhete bUPDT
set status_bilhete = 'F', DATA_PRESTACAO_CONTA = ?
where exists (
select distinct bil.cod_bilhete
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join (select * from bilhete) bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
        where jogo.cod_site = ?
        and geren.cod_gerente = ?
          and bil.status_bilhete not in ('C','F')
          and apo.status not in ('C') 
        AND jogo.TP_STATUS = 'P'
        and bUPDT.cod_bilhete = bil.cod_bilhete)	");
$stmt3->bind_param("sii",$dataPrestacao, $site, $cod_usuario);
$stmt3->execute();




/*Finaliza Jogos*/


$stmt2 = $con->prepare("UPDATE jogo J set TP_status = 'F' where cod_site = ? and tp_status = 'P' 
and not exists (select 1 from bilhete B,aposta A 
where B.cod_bilhete = A.cod_bilhete 
and A.cod_jogo = J.COD_JOGO
and status_bilhete = 'P')");
$stmt2->bind_param("i", $site);
$stmt2->execute();
 

/*

/*Inativar lançamentos*/


$stmt2 = $con->prepare("UPDATE lancamento set status = 'I' where cod_site = ? and status = 'A'");
$stmt2->bind_param("i", $site);
$stmt2->execute();
 
/*

$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtpout.secureserver.net';                 // Specify main and backup server
$mail->Port = 465;                                    // Set the SMTP port
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'contato@betloteria';                // SMTP username
$mail->Password = 'Betloteria$4';                  // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'contato@betloteria';
$mail->FromName = 'BET LOTERIA';
$mail->AddAddress('danylocampos@gmail.com', 'Danylo Campos');  // Add a recipient
//$mail->AddAddress('ellen@example.com');               // Name is optional

$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Here is the subject';
$mail->Body    = 'This is the HTML message body <strong>in bold!</strong>';
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->Send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}
*/


$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
$dompdf->stream("Prestacao_Conta.pdf");

inserir_auditoria(
    $con, 
    $cod_usuario, 
    $site, 
    AUD_ADMIN_PREST_CONTA,
    $html
);

$con->close();





?>
