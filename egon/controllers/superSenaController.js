angular.module('main').controller('superSenaCtrl',
    function ($rootScope, $scope, $q, user, $http, $location, configGeral) {
        $scope.flg_supersena = false;
        $scope.repetir_bilhete = user.getRepetirBilhete();
        $scope.valor_min_aposta;
        $scope.valor_max_aposta;
        $scope.comboConcurso = [];
        $scope.chkManterNumerosSelecionados = true;
        $scope.chkRegistroCliente = false;
        $scope.clientes = [];
        
        configGeral.get().then(function (data) {
            $scope.flg_supersena = user.getflg_supersena() == 'S' && data.flg_supersena;
            $scope.valor_min_aposta = data.valor_min_aposta;
            $scope.valor_max_aposta = data.valor_max_aposta;
        });

        $q.all([
            $http({
                url: 'angularjs-mysql/configuracao.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() 
                    + '&cod_area=' + user.getArea() 
                    + '&operacao=supersena'
                    + '&schema=' + user.getSchema()
            }),
            $http({
                url: 'angularjs-mysql/usuario.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() 
                    + '&cod_usuario=' + user.getCodigo()
                    + '&schema=' + user.getSchema()
            }),
            $http({
                method: "POST",
                url: "angularjs-mysql/clientes.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo()
                    + '&operacao=listar'
                    + '&schema=' + user.getSchema()
            })
        ]).then(function (response) {
            $scope.listaCfg = response[0].data;
            $scope.usuario = response[1].data;
            $scope.clientes = response[2].data;
        });

        $scope.puleRepeticao = user.getPuleRepeticao();

        $scope.apostas = [];
        $scope.listaApostas = [];
        if ($scope.puleRepeticao != "") {
            $http({
                url: 'angularjs-mysql/bilhetes.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() + 
                      '&cod_usuario=' + user.getCodigo() + 
                      '&operacao=conferirBilhete' + 
                      '&cod_bilhete=' + $scope.puleRepeticao +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.listaApostas = response.data;
                $scope.verificaPuleRepeticao();
                user.setPuleRepeticao('');
            });
        }

        $scope.valorTotal = "0,00";

        $scope.verificaPuleRepeticao = function () {
            if ($scope.listaApostas.length > 0) {
                $scope.apostas = [];
                //console.log($scope.listaApostas);
                for (var x = 0; x < $scope.listaApostas.length; x++) {

                    for (var z = 0; z < $scope.listaApostas[x].apostas.length; z++) {
                        var numero = parseFloat($scope.listaApostas[x].apostas[z].possivel_retorno).toFixed(2).split('.');
                        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                        var iRetorno = numero.join(',');

                        var numero = parseFloat($scope.listaApostas[x].apostas[z].retorno5).toFixed(2).split('.');
                        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                        iRetornoQuina = numero.join(',');

                        var numero = parseFloat($scope.listaApostas[x].apostas[z].retorno4).toFixed(2).split('.');
                        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                        iRetornoQuadra = numero.join(',');

                        var objeto = {
                            numeros: $scope.listaApostas[x].apostas[z].txt_aposta,
                            valor: "R$ " + $scope.listaApostas[x].apostas[z].valor_aposta,
                            valorReal: parseFloat($scope.listaApostas[x].apostas[z].valor_aposta.toString().replace(",", ".")),
                            qtd: $scope.listaApostas[x].apostas[z].qtd_numeros,
                            tipo: 'Super Sena',
                            possivelRetornoSena: iRetorno,
                            possivelRetornoSenaReal: $scope.listaApostas[x].apostas[z].possivel_retorno,
                            possivelRetornoQuina: iRetornoQuina,
                            possivelRetornoQuinaReal: $scope.listaApostas[x].apostas[z].retorno5,
                            possivelRetornoQuadra: iRetornoQuadra,
                            possivelRetornoQuadraReal: $scope.listaApostas[x].apostas[z].retorno4,
                            cod_jogo: 0,
                            concurso: 0
                        };

                        $scope.apostas.push(objeto);
                    }
                }
                $scope.getValorTotal();
            }    
        }

            
        $scope.filtrarCliente = function(termo) {
            return $scope.clientes.filter(cliente => cliente.nome.toLowerCase().indexOf(termo)>-1);
        }

        $scope.onSelectCliente = function(cliente) {
            if (cliente) {
                $scope.edtTelefone = cliente.telefone;
                document.getElementById("chkRegistrarCliente").focus();
            } else {
                $scope.edtTelefone = null;
            }
        }

        // Limpa todos os números selecionados
        $scope.limpar = function () {
	        $("button.btn-bg-grid").removeClass("btn-bg-grid").addClass("btn-outline-grid");
	        $(".numerosSelecionados").html("<span>Selecionados: 0</span>");
        }

        // Função para selecionar os números aleatoriamente
        $scope.selecionarNumeros = function (qtdNumeros) {
            $scope.limpar();

            var contador = 0;
            var arrayPintados = [];

            while (contador < qtdNumeros) {

                var pintou = false;
                var randomico = Math.floor(Math.random() * 60) + 1;

                for (i = 0; i < arrayPintados.length; i++) {
                    if (arrayPintados[i] == randomico) {
                        pintou = true;
                        break;
                    }
                }
                if (pintou) {
                    continue;
                }

            	$("button.btn-outline-grid").each(function (index) {
                    var aux = $(this).text();
                    aux = parseInt(aux);

                    if (aux == randomico) {
                    	$(this).removeClass("btn-outline-grid").addClass("btn-bg-grid");
                        arrayPintados.push(randomico);
                    }
                });

                if (!pintou) {
                    contador = contador + 1;
                }
            }
        	$(".numerosSelecionados").html("<span>Selecionados: " + $("button.btn-bg-grid").length + "</span>");
        }

        // Remove aposta
        $scope.removerAposta = function (index) {
            $scope.apostas.splice(index, 1);
            $scope.getValorTotal();
        }

        // Adiciona aposta
        $scope.adicionarAposta = function () {
            if (typeof $scope.edtValor === "undefined") {
                $.alert('Informe o VALOR!');
                return;
            }

            if (!$scope.repetir_bilhete && $scope.comboConcurso.length == 0) {
                $.alert('Informe o CONCURSO!');
                return;
            }

            if ($scope.apostas.length + $scope.comboConcurso.length > 50) {
                $.alert('Limite máximo de 50 apostas por bilhete atingido!');
                return;
            }

            var valorAux = parseFloat($scope.edtValor.toString().replace(",", "."));

            if (valorAux <= 0) {
                $.alert("Informe o valor maior que zero!");
                return;
            } else if ($scope.valor_min_aposta && valorAux < $scope.valor_min_aposta) {
                $.alert("O valor mínimo de aposta é: R$ " + $scope.valor_min_aposta);
                return;
            } else if ($scope.valor_max_aposta && valorAux > $scope.valor_max_aposta) {
                $.alert("O valor máximo de aposta é: R$ " + $scope.valor_max_aposta);
                return;
            }

            try {
                for (i = 14; i <= 40; i++) {
                    if ((i > 25 && i < 30) || (i > 30 && i < 35) || (i > 35 && i < 40)) {
                        continue;
                    }
                	$("#btn" + i + "sel").removeClass('btn-bg-grid');
                }

            	if ($("button.btn-bg-grid").length != 10) {
                    $.alert('É necessário selecionar 10 dezenas!');
                    return;
                }

                var sNumeros = "";

	            $("button.btn-bg-grid").each(function (index) {

                    sNumeros = sNumeros + $(this).text() + '-';
                });

                sNumeros = sNumeros.trim();
                sNumeros = sNumeros.substr(0, sNumeros.length - 2);
                sNumeros = sNumeros.replace(/\./g, '');

                var iQtd = $("button.btn-bg-grid").length;

                var i, j;
                for (i = 0; i < $scope.comboConcurso.length; i++) {
                    for (j = 0; j < $scope.apostas.length; j++) {
                        if ($scope.apostas[j].numeros == sNumeros 
                            && $scope.apostas[j].cod_jogo == $scope.comboConcurso[i].cod_jogo) {
                            $.alert("Aposta duplicada!");
                            return;
                        }
                    }
                }

                var sTipo = "Super Sena"; 
                var sValor = "R$ " + $scope.edtValor.toString();
	            var iRetornoSena = $scope.listaCfg[0].premio_sena * valorAux;
                var iRetornoQuina = $scope.listaCfg[0].premio_quina * valorAux;
                var iRetornoQuadra = $scope.listaCfg[0].premio_quadra * valorAux;

                if (iRetornoSena > $scope.listaCfg[0].premio_maximo) {
                    iRetornoSena = $scope.listaCfg[0].premio_maximo;
                }

                if (iRetornoQuina > $scope.listaCfg[0].premio_maximo) {
                    iRetornoQuina = $scope.listaCfg[0].premio_maximo;
                }

                if (iRetornoQuadra > $scope.listaCfg[0].premio_maximo) {
                    iRetornoQuadra = $scope.listaCfg[0].premio_maximo;
                }

                var iPossivelRetornoSenaReal = iRetornoSena;
                var iPossivelRetornoQuinaReal = iRetornoQuina;
                var iPossivelRetornoQuadraReal = iRetornoQuadra;

                var numero = parseFloat(iRetornoSena).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetornoSena = numero.join(',');

                var numero = parseFloat(iRetornoQuina).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetornoQuina = numero.join(',');

                var numero = parseFloat(iRetornoQuadra).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetornoQuadra = numero.join(',');

                if ($scope.repetir_bilhete) {
                    var objeto = {
                        numeros: sNumeros,
                        valor: sValor,
                        valorReal: valorAux,
                        qtd: iQtd,
                        tipo: sTipo,
                        possivelRetornoSena: iRetornoSena,
                        possivelRetornoSenaReal: iPossivelRetornoSenaReal,
                        possivelRetornoQuina: iRetornoQuina,
                        possivelRetornoQuinaReal: iPossivelRetornoQuinaReal,
                        possivelRetornoQuadra: iRetornoQuadra,
                        possivelRetornoQuadraReal: iPossivelRetornoQuadraReal,
                        cod_jogo: 0,
                        concurso: 0,
                        data_jogo: 0
                    };
                    $scope.apostas.push(objeto);
                } else {
                    for (i = 0; i < $scope.comboConcurso.length; i++) {
                        var objeto = {
                            numeros: sNumeros,
                            valor: sValor,
                            valorReal: valorAux,
                            qtd: iQtd,
                            tipo: sTipo,
                            possivelRetornoSena: iRetornoSena,
                            possivelRetornoSenaReal: iPossivelRetornoSenaReal,
                            possivelRetornoQuina: iRetornoQuina,
                            possivelRetornoQuinaReal: iPossivelRetornoQuinaReal,
                            possivelRetornoQuadra: iRetornoQuadra,
                            possivelRetornoQuadraReal: iPossivelRetornoQuadraReal,
                            cod_jogo: $scope.comboConcurso[i].cod_jogo,
                            concurso: $scope.comboConcurso[i].concurso,
                            data_jogo: $scope.comboConcurso[i].data_jogo
                        };
                        $scope.apostas.push(objeto);
                    }                
                }
                $scope.getValorTotal();

            } finally {
                if (!$scope.chkManterNumerosSelecionados) {
                    $scope.selecionarNumeros(iQtd);
                }
                $("#btn" + iQtd + "sel").addClass('btn-bg-grid');
            }

        }

        $scope.getJogos = function () {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&status=L' + 
                      '&operacao=supersena' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogos = response.data;
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });

        }

        $scope.filtraConcursoAtual = function () {
            if ($scope.jogos.length > 0) {
                return $scope.jogos[0];
            }
        }       

        $scope.getValorTotal = function () {
            var dValorTotal = 0;
            for (i = 0; i < $scope.apostas.length; i++) {
                dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
            }

            var numero = parseFloat(dValorTotal).toFixed(2).split('.');
            numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
            $scope.valorTotal = numero.join(',');
        }

        $scope.getValorTotalReal = function () {
            var dValorTotal = 0;
            for (i = 0; i < $scope.apostas.length; i++) {
                dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
            }

            return dValorTotal;
        }

        $scope.gerarBilhete = function () {

            if ($scope.apostas.length == 0) {
                $.alert('Faça no mínimo uma aposta!');
                return;
            }

            if ($scope.repetir_bilhete && $scope.comboConcursoRepetirBilhete == undefined) {
                $.alert('Informe o CONCURSO!');
                return;
            }

            if (!$scope.edtNome) {
                $.alert('Informe o NOME!');
                return;
            }

            if (parseFloat(user.getSaldoAtualizado()) < $scope.getValorTotalReal()) {
                $.alert('Você não tem saldo para gerar o bilhete!');
                return;
            }

            if ($scope.repetir_bilhete) {
                var concursoSelecionado = $scope.comboConcursoRepetirBilhete.split("|");
                var numerosApostados = [];
                for (j = 0; j < $scope.apostas.length; j++) {
                    if (numerosApostados.includes($scope.apostas[j].numeros)) {
                        $.alert("Aposta duplicada!");
                        return;
                    }
                    numerosApostados[j] = $scope.apostas[j].numeros;
                    $scope.apostas[j].cod_jogo = concursoSelecionado[0]; 
                    $scope.apostas[j].concurso = concursoSelecionado[1];
                    $scope.apostas[j].data_jogo = concursoSelecionado[2];
                }
            }
            
            $.confirm({
                title: '',
                content:"Confirma a geração do bilhete?",
                buttons: {
                    cancelar: function() {},
                    ok: function () {
                        var sTelefone = $scope.edtTelefone;
                        if (typeof sTelefone === 'undefined') {
                            sTelefone = "";
                        }
        
                        var data = new Date();
        
                        var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
                        var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
                        var ano = data.getFullYear();
        
                        var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
                        var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
                        var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };
        
                        var dataAtual = ano + '-' + mes + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;
                        $rootScope.loading = true;
                        $http({
                            url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: "cod_usuario=" + user.getCodigo() +
                                "&cod_site=" + user.getSite() +
                                "&nome_usuario=" + user.getName() +
                                "&nome=" + $scope.edtNome +
                                "&telefone=" + sTelefone +
                                "&registro_cliente=" + $scope.chkRegistroCliente +
                                "&apostas=" + JSON.stringify($scope.apostas) +
                                "&data_atual=" + dataAtual +
                                "&operacao=" +
                                "&schema=" + user.getSchema()
                        }).then(function (response) {
                            if (response.data.status == 'OK') {
                                user.atualizaSaldo(response.data.saldo);
                                user.setBilheteExterno(response.data.pule);
                                user.setRepetirBilhete(false);
                                $scope.repetir_bilhete = false;
                                $location.path("/conferir_bilhete");
                            } else {
                                $rootScope.loading = false;
                                if (response.data.status == 'INATIVO') {
                                    $.alert(response.data.mensagem);
                                    $location.path("/logout");
                                } else {
                                    $.alert(response.data.mensagem);
                                }
                            } 
                        }, function (response) {
                            $rootScope.loading = false;
                            $.alert('Erro no $HTTP: ' + response.status);
                        });
                    }
                }
            });
        }
    });
