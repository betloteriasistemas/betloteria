<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$site = mysqli_real_escape_string($con, $_POST['site']);
$return_arr = array();

$query = "SELECT DESCRICAO, HORA_EXTRACAO, AUTOMATIZADA
          FROM extracao_bicho eb 
          WHERE TIPO_JOGO = 'B' AND COD_SITE = '$site' AND STATUS = 'A' AND eb.COD_EXTRACAO NOT IN
            (
                SELECT COD_EXTRACAO 
                FROM configuracao_limite_bicho clb 
                WHERE COD_SITE = '$site'
            )
          ORDER BY HORA_EXTRACAO, DESCRICAO";

$result = mysqli_query($con, $query);

$retorno = new stdClass();
if (mysqli_num_rows($result) != 0) {
    $manuais= "";
    $automatizadas= "";
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        if ($row['AUTOMATIZADA'] == 'S') {
            $automatizadas = $automatizadas .  $row['HORA_EXTRACAO'] . " - " . $row['DESCRICAO'] . "; ";
        } else {
            $manuais = $manuais .  $row['HORA_EXTRACAO'] . " - " . $row['DESCRICAO'] . "; ";
        }
    }
    $retorno->extracoes = new stdClass();
    $retorno->extracoes->manuais = $manuais;
    $retorno->extracoes->automatizadas = $automatizadas;
    array_push($return_arr, $retorno);
}

$con->close();
echo json_encode($return_arr);
