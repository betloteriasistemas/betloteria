angular.module('main').controller('areasExtracaoCtrl', function ($scope, $http, configGeral, user, $route) {

    $scope.areas = null;
    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
    });

    $http({
        method: "POST",
        url: "angularjs-mysql/areas.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: 'site=' + user.getSite() + 
              '&operacao=listarAtivos' +
              '&schema=' + user.getSchema()
    }).then(function (response) {
        $scope.areas = response.data;		
    }, function (response) {
        console.log(response);
        $.alert('Erro no $HTTP: ' + response.status)
    });

    $scope.carregarExtracoesArea = function (cod_area) {

        $http({
            url: 'angularjs-mysql/areasExtracoes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "site=" + user.getSite() + 
                  "&area=" + cod_area + 
                  "&operacao=listar" +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.areas_extracoes = response.data;
        }, function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });   
    }

    
    $scope.getDescTipoJogo = function (tipo_jogo) {
        if (tipo_jogo == 'B') {
            return 'BICHO';
        } else if (tipo_jogo == '2') {
            return $scope.desc_2pra500;
        }
    }

    $scope.getStatusArea = function(status){
        if(status == "A"){
            return "Ativo";
        }else if (status == "I"){
            return "Inativo";
        }
    }

    $scope.getDescSimNao = function (valor) {
        if (valor === "S") {
            return "SIM";
        } else {
            return "NÃO";
        }
    };     

    $scope.excluir = function (cod_extracao, nome_extracao, status, cod_area) {

        var stString= '';
        if(status == 'A')
        {
            stString = 'Inativação';
        } else{
            stString = 'Ativação';
        }

        $.confirm({
            title: '',
            content:"Confirma " + stString +  " da Extração: " + nome_extracao + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    var cod_site = user.getSite();
                    $http({
                        url: 'angularjs-mysql/areasExtracoes.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'site=' + cod_site + 
                              '&cod_extracao=' + cod_extracao + 
                              "&status=" + status + 
                              '&operacao=excluir' +
                              '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $scope.carregarExtracoesArea(cod_area);
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    }


});