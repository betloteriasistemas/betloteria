var app = angular.module('main', ['ngRoute']);

app.factory('preventTemplateCache', function () {
	return {
		'request': function (config) {
			if (config.url.indexOf('components') !== -1 ||
				config.url.indexOf('controllers') !== -1) {
				config.url = config.url + '?' + new Date().getTime();
			}
			return config;
		}
	}
}).config(function ($httpProvider) {
	$httpProvider.interceptors.push('preventTemplateCache');
});


app.config( function ($routeProvider, $locationProvider) {	
	
	$routeProvider.when('/', {
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	}).when('/logout', {
		resolve: {
			deadResolve: function ($location, user) {
				user.clearData();
				$location.path('/');
			}
		}
	}).when('/login', {
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	}).when('/dashboard', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/dashboard.html',
		controller: 'dashboardCtrl'
	}).when('/clientes', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/clientes.html',
		controller: 'clientesCtrl'
	}).when('/agenda_bot', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/agenda_bot.html',
		controller: 'agendaBotCtrl'		
	}).when('/cadastro_banca_extracoes', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/cadastro_banca_extracoes.html',
		controller: 'cadastroBancaExtracoesCtrl'	
	}).when('/caixa_cliente', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/caixa_cliente.html',
		controller: 'caixaClienteCtrl' 
	}).when('/resultado', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/resultado.html'
	}).when('/controle_clientes', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/controle_clientes.html',
		controller: 'controleClientesCtrl' 		
	}).when('/bloqueios_avulsos', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				}
			},
		},
		templateUrl: './components/bloqueios_avulsos.html',
		controller: 'bloqueiosAvulsosCtrl'		
	}).
	otherwise({
			template: '404'
		});

	$locationProvider.html5Mode(false);
});


app.service('user', function () {
	var username;
	var nome;
	var loggedin = false;
	var id;
	var codigo;
	var site;
	var perfil;
	var saldo;
	var bilheteExterno = "";

	this.getBilheteExterno = function () {
		return bilheteExterno;
	}

	this.setBilheteExterno = function (auxBilheteExterno) {
		bilheteExterno = auxBilheteExterno;
	}

	this.getName = function () {
		return username;
	};

	this.getNome = function () {
		return nome;
	};

	this.setID = function (userID) {
		id = userID;
	};
	this.getID = function () {
		return id;
	};

	this.setSaldo = function (saldoAux) {
		this.saldo = saldoAux;
		saldo = this.saldo;
	};
	this.getSaldo = function () {
		return saldo;
	};

	this.getCodigo = function () {
		return codigo;
	}

	this.getSite = function () {
		return site;
	}
	this.getPerfil = function () {
		return perfil;
	}

	this.isUserLoggedIn = function () {
		if (!!localStorage.getItem('loginADM')) {
			loggedin = true;
			var data = JSON.parse(localStorage.getItem('loginADM'));
			username = data.username;
			id = data.id;
			nome = data.nome;
			codigo = data.codigo;
			site = data.site;
			perfil = data.perfil;
			saldo = data.saldo;
			//alert (site);
		}
		return loggedin;
	};

	this.atualizaSaldo = function(pSaldo){
		localStorage.setItem('saldoAtualizado', JSON.stringify({
			saldo: pSaldo
		}));
	}

	this.getSaldoAtualizado = function(){
		var data = JSON.parse(localStorage.getItem('saldoAtualizado'));
		return data.saldo;
	}
	

	this.saveData = function (data) {
		username = data.user;
		id = data.id;
		nome = data.nome;
		loggedin = true;
		codigo = data.codigo;
		site = data.site;
		perfil = data.perfil;
		saldo = data.saldo;
		localStorage.setItem('saldoAtualizado', JSON.stringify({
			saldo: data.saldo
		}));
		
		localStorage.setItem('loginADM', JSON.stringify({
			username: username,
			id: id,
			nome: nome,
			codigo: codigo,
			site: site,
			perfil: perfil,
			saldo: saldo
		}));
	};

	this.clearData = function () {
		localStorage.removeItem('loginADM');
		localStorage.removeItem('saldoAtualizado');
		username = "";
		id = "";
		nome = "";
		loggedin = false;
		codigo = "";
		site = "";
		perfil = "";
		saldo = 0;
	}

	
})


app.filter('object2Array', function () {
	return function (input) {
		return angular.fromJson(input);
	}
});

app.directive('loading', ['$http', '$rootScope', function ($http, $rootScope) {
	$rootScope.loading = false;
	return {
		restrict: 'A',
		link: function (scope, element) {
			scope.isLoading = function () {
				return $rootScope.loading;
			};
			scope.$watch(scope.isLoading, function (value) {
				if (value && element.css('display') == 'none') {
					element.css('display', 'flex');
				} else {
					element.css('display', 'none');
				}
			});
		}
	};
}]);