angular.module('main').controller('verBilhetesCtrl', function (
    $rootScope, $scope, $http, user, $filter, $location, configGeral, bilhetesAPI, funcoes, $q) {
    /**
     * procurar bilhete
    */
    $scope.isExibirGrids = localStorage.getItem('grid') ? true : false;
    $rootScope.loading = false;
    $scope.desabilitarBotaoCancelar = false;
    $scope.bilhete;
    $scope.iniciou = false;
    $scope.exibeBilhete = false;
    $scope.data_bilhete = null;
    $scope.data_bilhete_js = null;
    $scope.nome_banca = "";
    $scope.tiposJogos = funcoes.getTiposJogos();
    $scope.cambista = "";
    $scope.nome_apostador = "";
    $scope.telefone_apostador = "";
    $scope.concurso = "";
    $scope.txt_pule = "";
    $scope.txt_pule_pai = "";
    $scope.numero_bilhete = "";
    $scope.status = "";
    $scope.status_bilhete = "";
    $scope.data_jogo = null;
    $scope.externo = false;

    $scope.qrcodeString = '';
    $scope.size = 100;
    $scope.correctionLevel = 'M';
    $scope.typeNumber = 0;
    $scope.inputMode = 'ALPHA_NUM';
    $scope.image = false;
    $scope.comboTipoJogo = "";

    $scope.linkZap = "";

    $scope.minutos_cancelamento = 0;
    configGeral.get().then(function (data) {
        $scope.minutos_cancelamento = data.minutos_cancelamento;
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        $scope.desc_2pra500 = funcoes.getDescricaoTipoJogo('2', $scope.desc_2pra500);
    });
    //fim procurar bilhete

    $scope.detalhesBilhetePremiado;
    // $scope.totalizador;
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.records = [];
    $scope.listaApostas = [];
    $scope.bilheteAux = "";
    $scope.ordenacao = true;
    $scope.campoOrdenacao = "";

    $scope.total = 0;
    $scope.totalComissao = 0;

    $scope.edtDataDe = new Date();
    $scope.edtDataAte = new Date();
    let dataInicial;
    let dataFinal;

    $scope.consulta = false;
    /** ORDENAÇÃO */
    $scope.sort = "DATA";
    $scope.sortType = false;

    $scope.totalItens = 0;
    $scope.itensPorPagina = 20;

    $scope.paginacao = {
        atual: 1
    };

    $scope.tipo_relatorio = 'pdf';
    $scope.eh_pdf = true;

    $scope.alteraTipoRelatorio = function() {
        if ($scope.eh_pdf) {
            $scope.eh_pdf = true;
            $scope.tipo_relatorio = 'pdf';
        } else {
            $scope.eh_pdf = false;
            $scope.tipo_relatorio = 'xlsx';
        }
    }

    $scope.gerarRelatorio = function () {

        if ($scope.edtDataDe > $scope.edtDataAte) {
            $.alert('A data final do período deve ser maior ou igual a data inicial.');
            return;
        }

        var dataDe  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        var dataAte = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");

        var tipoJogo = $scope.comboTipoJogo;
        var descTipoJogo = "";
        if (tipoJogo) {
            descTipoJogo = $("#comboTipoJogo")[0].selectedOptions[0].label
        }

        var concurso = $scope.comboConcurso;
        var descConcurso = "";
        if (concurso) {
            descConcurso = $("#comboConcurso")[0].selectedOptions[0].label
        }

        var vendedor= $scope.comboCambista;
        var descVendedor = "";
        if (vendedor) {
            descVendedor = $("#comboCambista")[0].selectedOptions[0].label
        }

        window.open('angularjs-mysql/gerarRelatorioVerBilhetes.php?cod_usuario=' + user.getCodigo()
            + '&perfil_usuario=' + user.getPerfil()
            + '&site=' + user.getSite()
            + '&sort=' + $scope.sort
            + '&sortType=' + $scope.sortType
            + '&concurso=' + $scope.comboConcurso
            + '&usuarioResponsavel=' + $scope.comboCambista
            + '&nomeApostador=' + $scope.edtNome
            + '&statusBilhete=' + $scope.comboStatusBilhete
            + '&tipoJogo=' + $scope.comboTipoJogo
            + '&dataDe=' + dataDe
            + '&dataAte=' + dataAte
            + '&descTipoJogo=' + descTipoJogo
            + '&descConcurso=' + descConcurso
            + '&descVendedor=' + descVendedor
            + '&schema=' + user.getSchema()
            + '&tipoRelatorio=' + $scope.tipo_relatorio);                                       
    } 

    $scope.exibirGrids = function() {
        $scope.isExibirGrids = !$scope.isExibirGrids;
        if ($scope.isExibirGrids) {
            localStorage.setItem('grid', 1);
        } else {
            localStorage.removeItem('grid');
        }
    }
    $scope.alterarPagina = function (newPage) {
        getResultadoBusca(newPage);
    };

    function getResultadoBusca(pageNumber) {
        $scope.index = (pageNumber - 1) * $scope.itensPorPagina;
        $scope.listarBilhetes(dataInicial, dataFinal, $scope.index, $scope.itensPorPagina, null);
    }

    $scope.orderFilter = function (column) {
        if ($scope.sort == column) {
            $scope.sortType = !$scope.sortType;
        } else {
            $scope.sortType = true;
        }
        $scope.sort = column;
        this.alterarPagina(1);
    };


    $scope.filtroCambistas = function (item) {
        if ($scope.perfil == 'A') {
            var retorno = (item.gerente == $scope.comboCambista && item.concurso == $scope.comboConcurso) ||
                (item.gerente == $scope.comboCambista && $scope.comboConcurso == "") ||
                (item.gerente == $scope.comboCambista && typeof $scope.comboConcurso == "undefined") ||
                (item.concurso == $scope.comboConcurso && $scope.comboCambista == "") ||
                (item.concurso == $scope.comboConcurso && typeof $scope.comboCambista == "undefined") ||
                ($scope.comboConcurso == "" && $scope.comboCambista == "") ||
                (typeof $scope.comboConcurso == "undefined" && typeof $scope.comboCambista == "undefined") ||
                (typeof $scope.comboConcurso == "undefined" && $scope.comboCambista == "") ||
                ($scope.comboConcurso == "" && typeof $scope.comboCambista == "undefined");

            return retorno;


        } else {
            return (item.cambista == $scope.comboCambista && item.concurso == $scope.comboConcurso) ||
                (item.cambista == $scope.comboCambista && $scope.comboConcurso == "") ||
                (item.cambista == $scope.comboCambista && typeof $scope.comboConcurso == "undefined") ||
                (item.concurso == $scope.comboConcurso && $scope.comboCambista == "") ||
                (item.concurso == $scope.comboConcurso && typeof $scope.comboCambista == "undefined") ||
                ($scope.comboConcurso == "" && $scope.comboCambista == "") ||
                (typeof $scope.comboConcurso == "undefined" && typeof $scope.comboCambista == "undefined") ||
                (typeof $scope.comboConcurso == "undefined" && $scope.comboCambista == "") ||
                ($scope.comboConcurso == "" && typeof $scope.comboCambista == "undefined");
        }

    };

    $scope.filtroConcursos = function (item) {
        return item.concurso == $scope.comboConcurso || $scope.comboConcurso == "" || typeof $scope.comboConcurso == "undefined";
    };

    $scope.getDescricaoConcurso = function(jogo){
        return funcoes.getDescricaoConcurso(jogo);
    }

    $scope.ajustaDataFimPeriodo = function () {
        if ($scope.edtDataDe > $scope.edtDataAte) {
            $scope.edtDataAte = $scope.edtDataDe;
        }
    }

    $scope.pesquisarBilhetes = function () {

        if ($scope.edtDataDe > $scope.edtDataAte) {
            $.alert('A data final do período deve ser maior ou igual a data inicial.');
            return;
        }

        dataInicial = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        dataFinal = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");
        $scope.records = [];
        $scope.recordsFiltered = [];
        $scope.listarBilhetes(dataInicial, dataFinal);
    }

    function isValidaData(dataInicial, dataFinal) {
        if (!dataInicial || dataInicial.split('-')[0].length > 4) {
            $.alert('Data inválida no campo: Periodo De');
            return;
        }
        if (!dataFinal || dataFinal.split('-')[0].length > 4) {
            $.alert('Data inválida no campo: Periodo Até');
            return;
        }
        return true;
    }

    $scope.listarBilhetes = function (dataInicial, dataFinal, index = 0, qtdElementos = $scope.itensPorPagina,
        buscarTotalizadores = 1) {

        if (!$scope.comboTipoJogo) {
            $scope.comboTipoJogo = "";
        }
        if (!$scope.comboConcurso) {
            $scope.comboConcurso = "";
        }
        if (!$scope.comboCambista) {
            $scope.comboCambista = "";
        }
        if (!$scope.edtNome) {
            $scope.edtNome = "";
        }
        if (!$scope.comboStatusBilhete) {
            $scope.comboStatusBilhete = "";
        }

        if (typeof $scope.edtDataDe === 'undefined' || $scope.edtDataDe == null) {
            dataDe = '0';
        }

        $scope.mostrarFooterTabela = false;
        $scope.processarPagamento = true;
        $scope.consulta = true;
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + '&site=' + user.getSite()
                + '&operacao=listar' + '&dataDe=' + dataInicial
                + '&dataAte=' + dataFinal
                + '&tipoJogo=' + $scope.comboTipoJogo
                + '&concurso=' + $scope.comboConcurso
                + '&nomeApostador=' + $scope.edtNome
                + '&statusBilhete=' + $scope.comboStatusBilhete
                + '&usuarioResponsavel=' + $scope.comboCambista
                + '&index=' + index
                + '&qtdElementos=' + qtdElementos
                + '&sort=' + $scope.sort
                + '&sortType=' + $scope.sortType
                + '&buscarTotalizadores=' + buscarTotalizadores
                + '&schema=' + user.getSchema()
        }).then(function (response) {
            if (buscarTotalizadores == 1) {
                const totalizadores = response.data.filter(obj => obj.totalizadores)[0].totalizadores;
                $scope.totalizador = totalizadores;
                $scope.totalItens = totalizadores['total_registros'];
            }
            $scope.records = response.data.filter(obj => !obj.totalizadores);
            $scope.recordsFiltered = $scope.records;
        }).catch(function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        }).finally(function () {
            $rootScope.loading = false;
            $scope.mostrarFooterTabela = true;
            $scope.processarPagamento = false;
            if ($scope.records) {
                scrollParaTabela();
            }
        });
    }

    function ordenarPorData(a, b) {
        const dataA = a.data_bilhete.split("-");
        const dataB = b.data_bilhete.split("-");
        return new Date(dataA[2], dataA[1] - 1, dataA[0]).getTime() - new Date(dataB[2], dataB[1] - 1, dataB[0]).getTime();
    }
    $scope.getDescricaoTipoJogo = function (tipo_jogo) {
        return funcoes.getDescricaoTipoJogo(tipo_jogo, $scope.desc_2pra500);
    }

    $scope.descricaoModalidadeJogo = function (tipo_jogo) {
        return funcoes.getDescricaoTipoJogoBicho(tipo_jogo);
    }

    $scope.getJogos = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/jogos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + '&finalizados=false'
                + '&operacao=' + '&site=' + user.getSite()
                + '&status=' + '&index='
                + '&qtdElementos='
                + '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.jogos = response.data.filter(obj => !obj.totalizadores);
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.listarApostas = function (cod_bilhete, tipo_jogo) {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + 
                  '&site=' + user.getSite() +
                  '&cod_bilhete=' + cod_bilhete + 
                  '&tipo_jogo=' + tipo_jogo + 
                  '&operacao=listarApostas' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.apostasList = response.data;
        }).catch(function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.getCambistas = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + 
                  '&site=' + user.getSite() + 
                  '&operacao=getCambistas' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.cambistas = response.data;
        }).catch(function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.mudancaDeFiltro = function () {
        $scope.recordsFiltered = $filter('filter')($scope.records, { 'tipo_jogo': $scope.comboTipoJogo });
        $scope.recordsFiltered = $filter('filter')($scope.recordsFiltered, { 'nome_apostador': $scope.edtNome });
        if ($scope.comboStatusBilhete == "Premiado") {
            for (var z = 0; z < $scope.recordsFiltered.length; z++) {
                var ttt = parseFloat($scope.recordsFiltered[z].possivel_retorno);
                if (parseFloat($scope.recordsFiltered[z].possivel_retorno) <= 0) {
                    $scope.recordsFiltered.splice(z, 1);
                }
            }

        } else {
            $scope.recordsFiltered = $filter('filter')($scope.recordsFiltered, { 'status_bilhete': $scope.comboStatusBilhete });
        }

        $scope.recordsFiltered = $filter('filter')($scope.recordsFiltered, $scope.filtroCambistas);

        $scope.total = 0;
        $scope.totalComissao = 0;
        for (var i = 0; i < $scope.recordsFiltered.length; i++) {
            $scope.total += parseFloat($scope.recordsFiltered[i].total);
            $scope.totalComissao += parseFloat($scope.recordsFiltered[i].comissao);
        }

    }


    $scope.getTotalComissao = function () {

    }

    $scope.repetirJogo = function (txt_pule, tipo) {
        user.setPuleRepeticao(txt_pule);
        user.setRepetirBilhete(true);
        switch (tipo) {
            case 'B':
                $location.path("/bicho");
            break;
            case 'S':
                $location.path("/seninha");
            break;
            case 'Q':
                $location.path("/quininha");
            break;
            case 'L':
                $location.path("/lotinha");
            break;
            case 'U':
                $location.path("/supersena");
            break;
        }
    }

    $scope.procurarBilhete = function (codigo, dadosIniciaisBusca = true) {

        $scope.desabilitarBotaoPagarBilhete = true;

        if (dadosIniciaisBusca) {
            $scope.iniciou = true;
            $scope.desabilitarBotaoCancelar = true;
            $scope.detalhesBilhetePremiado = {};
        }

        $rootScope.loading = true;
        const response = bilhetesAPI.getBilhetes(codigo, null);
        response.then(function (result) {
            $scope.bilhete = result;
            $scope.desabilitarBotaoPagarBilhete = false;
            verificarBilhetePremiado($scope.bilhete[0]);
            $rootScope.loading = false;
        });
    };

    $scope.atualizarStatusPagamento = function () {

        $http({
            url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "cod_usuario=" + user.getCodigo() +
                "&cod_site=" + user.getSite() +
                "&nome_usuario=" + user.getName() +
                "&operacao=atualizarStatusPagamento" +
                "&txt_pule=" + $scope.txt_pule +
                "&schema=" + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $scope.procurarBilhete($scope.txt_pule, null);
                $scope.pesquisarBilhetes();
            } else {
                console.log('error');
            }
        }).catch(function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        }).finally(function () {
        });
    }

    function retornoBilhetePremiado(bilhete) {
        if (bilhete.tipo_jogo == 'RIFA') {
            if (bilhete.apostas.filter(aposta => aposta.status === 'Ganho').length > 0) {
                return bilhete.concurso;
            }
        }
        return bilhete.apostas.filter(aposta => aposta.status === 'Ganho')
            .reduce((acumulador, aposta) => acumulador + parseFloat(aposta.valor_ganho), 0);
    }

    function verificarBilhetePremiado(bilhete) {
        const valorPremio = retornoBilhetePremiado(bilhete);
        if (valorPremio !== 0) {
            $scope.detalhesBilhetePremiado = {
                valorPremio,
                mostrarInformacoesBilhetePremiado: true,
                statusPagamento: 'pendente',
                tipo_jogo: bilhete.tipo_jogo
            }
            if (bilhete.status_pagamento === 'S') {
                $scope.detalhesBilhetePremiado.statusPagamento = 'efetivado';
            }
        }
    }

    $scope.$watch('bilhete', function () {
        if ($scope.iniciou) {

            $scope.exibeBilhete = $scope.bilhete.length > 0;
            if (!$scope.exibeBilhete && !$scope.externo) {
                limpaCampos();
                $.alert('Bilhete não encontrado!');
            } else {
                try {
                    $scope.tipo_jogo = $scope.bilhete[0].tipo_jogo;

                    $scope.carregaConfiguracao($scope.tipo_jogo);


                    $scope.data_bilhete = $scope.bilhete[0].data_bilhete;
                    $scope.data_bilhete_js = $scope.bilhete[0].data_bilhete_js;
                    $scope.nome_banca = $scope.bilhete[0].nome_banca;
                    $scope.cambista = $scope.bilhete[0].cambista;
                    $scope.nome_apostador = $scope.bilhete[0].nome_apostador;
                    $scope.telefone_apostador = $scope.bilhete[0].telefone_apostador;
                    $scope.concurso = $scope.bilhete[0].concurso;
                    $scope.txt_pule = $scope.bilhete[0].txt_pule;
                    $scope.txt_pule_pai = $scope.bilhete[0].txt_pule_pai;
                    $scope.numero_bilhete = $scope.bilhete[0].cod_bilhete;
                    $scope.qrcodeString = $scope.bilhete[0].txt_pule;
                    $scope.data_jogo = $scope.bilhete[0].data_jogo;
                    $scope.numeros_sorteados = $scope.bilhete[0].numeros_sorteados;
                    $scope.status_bilhete = $scope.bilhete[0].status_bilhete;
                    $scope.linkZap = $scope.bilhete[0].linkZap;
                    $scope.valor_bilhete = $scope.bilhete[0].valor_bilhete;
                    $scope.numero_bilhete = $scope.bilhete[0].numero_bilhete;
                } catch (error) {

                }
            }
            if ($scope.status_bilhete === 'Cancelado') {
                $scope.desabilitarBotaoCancelar = true;
            } else {
                $scope.desabilitarBotaoCancelar = false;

            }

        }
    });

    function limpaCampos() {
        $scope.exibeBilhete = false;
        $scope.data_bilhete = null;
        $scope.data_bilhete_js = null;
        $scope.nome_banca = "";
        $scope.cambista = "";
        $scope.nome_apostador = "";
        $scope.telefone_apostador = "";
        $scope.concurso = "";
        $scope.txt_pule = "";
        $scope.qrcodeString = "";
        $scope.status = "";
        $scope.status_bilhete = "";
        $scope.data_jogo = null;
    }

    $scope.carregaConfiguracao = function (tipo_jogo) {
        var operacao = '';
        switch (tipo_jogo) {
            case 'BICHO':
                operacao = 'bicho';
            break;
            case 'QUININHA':
                operacao = 'quininha';
            break;
            case 'SENINHA':
                operacao = 'seninha';
            break;
            case 'LOTINHA':
                operacao = 'lotinha';
            break;
            case '2 PRA 500':
                operacao = '2pra500';
            break;
            case 'RIFA':
                operacao = 'rifa';
            break;
            case 'SUPER SENA':
                operacao = 'supersena';
            break;
        }

        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&cod_area=' + user.getArea() + 
                  '&operacao=' + operacao +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.listaCfg = response.data;
        });
    }

    function scrollParaTabela() {
        $('html, body').animate({ scrollTop: $('#dir-paginator-top').offset().top - 25 }, 'slow');
    }

    $scope.cancelarBilhete = function () {
        if (!$scope.verificaPodeCancelar($scope.status_bilhete)) {
            $.alert('Bilhete não pode ser cancelado, já se esgotou o tempo de cancelamento!');
        } else {
            $.confirm({
                title: '',
                content:"Se você deseja realmente cancelar esse bilhete, aperte em OK",
                buttons: {
                    cancelar: function() {},
                    ok: function () {
                        $http({
                            method: "POST",
                            url: "angularjs-mysql/cancelarBilhete.php",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: 'txt_pule=' + $scope.txt_pule +
                                '&cod_site=' + user.getSite() +
                                '&cod_usuario=' + user.getCodigo() +
                                '&operacao=' +
                                '&schema=' + user.getSchema()
            
                        }).then(function (response) {
                            if (response.data.status == 'OK') {
                                user.atualizaSaldo(response.data.saldo);
                                $.alert('Bilhete cancelado com sucesso!');
                                $scope.procurarBilhete($scope.txt_pule);
                            } else {
                                $.alert(response.data.mensagem);
                            }
                        }).catch(function (response) {
                            console.log(response);
                            $.alert('Erro no $HTTP: ' + response.status);
                        });
                    }
                }
            });
        }
    }

    $scope.verificaPodeCancelar = function (status_bilhete) {
        if (user.getPerfil() == 'A') {
            return status_bilhete != 'Cancelado';
        }
        if (user.getPerfil() == 'C' && status_bilhete != 'Aberto') {
            return false;
        }

        if (status_bilhete == 'Pendente de Validação') {
            return false;
        }

        var data = new Date(),
            minutos = parseInt($scope.minutos_cancelamento);

        dataConv = new Date($scope.data_bilhete_js);
        dataConv.setMinutes(dataConv.getMinutes() + minutos);

        if (data > dataConv) {
            return false;
        } else {
            return true;
        }
    }

    $scope.clickZap = function (confirm) {

        var txt_pule = $scope.txt_pule
        if ($scope.numero_bilhete == txt_pule) {
            txt_pule = $scope.txt_pule_pai;
        }

        var linkWeb = 'Para conferir seu bilhete acesse:'
            + '%0ahttp://bilheteloteria.com/#!/' + txt_pule;
        var linkZap = 'Para conferir seu bilhete acesse:'
            + '%0ahttp://bilheteloteria.com/%23!/' + txt_pule;

        if (user.getDominio()) {
            linkWeb = linkWeb + '%0a%0aAposte em:%0a' + user.getDominio();
            linkZap = linkZap + '%0a%0aAposte em:%0a' + user.getDominio();
        }

        linkZap = linkZap + '%0a%0aBoa sorte 🍀!';
        linkWeb = linkWeb + '%0a%0aBoa sorte 🍀!';

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            if (confirm) {
                if ($scope.telefone_apostador != '') {
                    window.open('https://wa.me/55' + $scope.telefone_apostador + '?text=' + linkZap);
                } else {
                    window.open('https://wa.me/?text=' + linkZap);
                }
            } else {
                window.open('https://wa.me/?text=' + linkZap);
            }
        } else {
            if (confirm) {
                if ($scope.telefone_apostador != '') {
                    window.open('https://web.whatsapp.com/send?phone=55' + $scope.telefone_apostador + '&text=' + linkWeb);
                } else {
                    window.open('https://web.whatsapp.com/send?text=' + linkWeb);
                }
            } else {
                window.open('https://web.whatsapp.com/send?text=' + linkWeb);
            }
        }
    };

    $scope.confirmationDialog = function () {
        jQuery("#modalConferirBilhetes").modal('hide');
        if ($scope.telefone_apostador != '') {
            $scope.confirmationDialogConfig = {
                title: "",
                message: "Deseja enviar o bilhete para o número: " + $scope.telefone_apostador + "?",
                buttons: [{
                    label: "Sim",
                    action: "com-telefone"
                },
                {
                    label: "Outro Número",
                    action: "sem-telefone"
                }]
            };
            $scope.showDialog(true);
        } else {
            $scope.clickZap(false);
        }
    }

    $scope.showDialog = function (flag) {
        jQuery("#confirmation-dialog").modal(flag ? 'show' : 'hide');
    }

    $scope.executeDialogAction = function (action) {
        switch (action) {
            case 'com-telefone':
                $scope.clickZap(true);
                break;
            case 'sem-telefone':
                $scope.clickZap(false);
                break;
        }
        $scope.showDialog(false);
    }

    $scope.imprimir = function () {
        var linka = $scope.getLinkMobile($scope.txt_pule);
        window.open(linka);
    }
    $scope.getLinkMobile = function (txt_pule) {
        var strAux = "com.mitsoftwar.appprinter://" + $location.absUrl() 
            + "/angularjs-mysql/bilheteCodificado.php?id=" + txt_pule 
            + "&site=" + user.getSite()
            + "&cod_usuario=" + user.getCodigo()
            + '&schema=' + user.getSchema();

        strAux = strAux.replace('#!/conferir_bilhete/', '');
        strAux = strAux.replace('#!/ver_bilhetes/', '');
        return strAux;
    }

    $scope.filterByTipo = function (jogo) {
        return $scope.comboTipoJogo == "" || jogo.tipo_jogo == $scope.comboTipoJogo;
    }
});
