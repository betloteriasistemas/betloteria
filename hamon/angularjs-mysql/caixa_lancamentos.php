<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$perfil = mysqli_real_escape_string($con, $_POST['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);
$dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);
$index = mysqli_real_escape_string($con, $_POST['index']);
$qtdElementos = mysqli_real_escape_string($con, $_POST['qtdElementos']);
$codigoUsuario = mysqli_real_escape_string($con, $_POST['codigoUsuario']);

$totalizadores = "";

$return_arr = array();


$queryLancamentos = "";

if ($perfil == 'A') {

    $queryLancamentos = "
            SELECT 
                cod_usuario,
                nome,
                data_lancamento,
                motivo,
                data_inteiro,
                SUM(entradas) AS entradas,
                SUM(saidas) AS saidas,
                (SUM(v.entradas) - SUM(v.saidas)) AS saldo
            FROM
                ((SELECT 
                        NULL AS cod_cambista,
                        U.cod_usuario,
                        U.nome,
                        DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                        L.motivo AS motivo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                        (CASE
                            WHEN tipo_lancamento = 'E' THEN (valor)
                            ELSE 0
                        END) AS entradas,
                        (CASE
                            WHEN tipo_lancamento = 'S' THEN (valor)
                            ELSE 0
                        END) AS saidas
                FROM
                    lancamento L, usuario U
                WHERE
                    L.COD_USUARIO_lancamento = U.cod_usuario
                        AND L.cod_site = '$site'
                        AND L.COD_USUARIO_lancamento IN (SELECT 
                            cod_usuario
                        FROM
                            usuario
                        WHERE
                            cod_gerente = '$cod_usuario')
                        AND L.ajuda_custo = 'N'
                        AND L.status = 'A') 
                UNION ALL 
                    (SELECT 
                        U.cod_usuario AS cod_cambista,
                        U.cod_gerente AS cod_usuario,
                        G.nome,
                        DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                        L.motivo AS motivo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                        (CASE
                            WHEN tipo_lancamento = 'E' THEN (valor)
                            ELSE 0
                        END) AS entradas,
                        (CASE
                            WHEN tipo_lancamento = 'S' THEN (valor)
                            ELSE 0
                        END) AS saidas
                FROM
                    lancamento L, usuario U, usuario G
                WHERE
                    L.COD_USUARIO_lancamento = U.cod_usuario
                        AND U.cod_gerente = G.cod_usuario
                        AND L.cod_site = '$site'
                        AND L.COD_USUARIO_lancamento IN (SELECT 
                            U1.cod_usuario
                        FROM
                            usuario U1, usuario U2
                        WHERE
                            U1.cod_gerente = U2.cod_usuario
                                AND U2.cod_gerente = '$cod_usuario')
                        AND L.ajuda_custo = 'N'
                        AND L.status = 'A')) v
            WHERE 1 =1  ";

} else if ($perfil == 'G') {
    $queryLancamentos = "
            SELECT 
                cod_usuario,
                nome,
                data_lancamento,
                motivo,
                data_inteiro,
                SUM(entradas) AS entradas,
                SUM(saidas) AS saidas,
                (SUM(v.entradas) - SUM(v.saidas)) AS saldo
            FROM
                ((SELECT 
                        NULL AS cod_cambista,
                        U.cod_usuario,
                        U.nome,
                        DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                        L.motivo AS motivo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                        (CASE
                            WHEN tipo_lancamento = 'E' THEN (valor)
                            ELSE 0
                        END) AS entradas,
                        (CASE
                            WHEN tipo_lancamento = 'S' THEN (valor)
                            ELSE 0
                        END) AS saidas
                FROM
                    lancamento L, usuario U
                WHERE
                    L.COD_USUARIO_lancamento = U.cod_usuario
                        AND L.cod_site = '$site'
                        AND L.cod_usuario_lancamento = '$cod_usuario'
                        AND L.ajuda_custo = 'N'
                        AND L.status = 'A') 
                UNION ALL 
                (SELECT 
                        U.cod_usuario AS cod_cambista,
                        U.cod_usuario AS cod_usuario,
                        U.nome,
                        DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                        L.motivo AS motivo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                        (CASE
                            WHEN tipo_lancamento = 'E' THEN (valor)
                            ELSE 0
                        END) AS entradas,
                        (CASE
                            WHEN tipo_lancamento = 'S' THEN (valor)
                            ELSE 0
                        END) AS saidas
                FROM
                    lancamento L, usuario U, usuario G
                WHERE
                    L.COD_USUARIO_lancamento = U.cod_usuario
                        AND U.cod_gerente = G.cod_usuario
                        AND L.cod_site = '$site'
                        AND L.COD_USUARIO_lancamento IN (SELECT 
                            cod_usuario
                        FROM
                            usuario
                        WHERE
                            cod_gerente = '$cod_usuario')
                        AND L.ajuda_custo = 'N'
                        AND L.status = 'A')) v
            WHERE
                1 = 1 ";
}
    
if($codigoUsuario) {
    $queryLancamentos = $queryLancamentos . " AND COD_USUARIO = $codigoUsuario ";
}

if ($dataDe != '0') {
    $queryLancamentos = $queryLancamentos . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
}

if ($dataAte != '0') {
    $queryLancamentos = $queryLancamentos . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
}

if ($dataDe == 0 && $dataAte == 0) {
    $queryLancamentos = $queryLancamentos . " AND data_inteiro >= DATE_FORMAT(current_date - 7, '%Y%m%d')
                AND data_inteiro <= DATE_FORMAT(current_date, '%Y%m%d') " ;
}

$queryLancamentos = $queryLancamentos . " group by cod_usuario, nome, data_lancamento, motivo 
                    order by cod_usuario, nome, data_lancamento";
                    
$totalizadoresLancamentos = "SELECT SUM(entradas) as entradas, ".
        "SUM(saidas) as saidas, ".
        "SUM(saldo) as saldo, ".
        "COUNT(1) as 'total_registros' ".
        "FROM ( ". $queryLancamentos . " ) t";


$queryLancamentos = $queryLancamentos." LIMIT $index, $qtdElementos ";   

$result = mysqli_query($con, $queryLancamentos);

$lancamentos_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    $row_array['cod_usuario'] = $row['cod_usuario'];
    $row_array['nome'] = $row['nome'];
    $row_array['data_lancamento'] = $row['data_lancamento'];
    $row_array['motivo'] = $row['motivo'];
    $row_array['data_inteiro'] = $row['data_inteiro'];
    $row_array['entradas'] = $row['entradas'];
    $row_array['saidas'] = $row['saidas'];
    $row_array['saldo'] = $row['saldo'];

    array_push($lancamentos_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}        

$lancamentosJSON{'lancamentos'} = $lancamentos_arr;  

array_push($return_arr, $lancamentosJSON);

$resultadoTotalizadoresLancamentos = mysqli_query($con, $totalizadoresLancamentos);
$row = mysqli_fetch_array($resultadoTotalizadoresLancamentos);

$totalizadoresLancamentosJSON{'totalizadoresLancamentos'} = $row;  

array_push($return_arr, $totalizadoresLancamentosJSON);

echo json_encode($return_arr);

$con->close();
