<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
	die();
}

$site = mysqli_real_escape_string($con, $_POST['site']);

$query = "
select u.`COD_USUARIO`, u.`NOME`, u.`PERFIL`
from usuario u 
where u.`STATUS` = 'A' and u.`COD_SITE` = '$site'
order by u.`NOME` ASC";

$result = mysqli_query($con, $query);
$return_arr = array();
$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    $row_array['cod_usuario'] = $row['COD_USUARIO'];
	$row_array['nome_usuario'] = $row['NOME'];
	$row_array['perfil_usuario'] = $row['PERFIL'];
    array_push($return_arr, $row_array);
	if ($contador == mysqli_num_rows($result)) {
		break;
	}
}

$con->close();
echo json_encode($return_arr, JSON_NUMERIC_CHECK);
