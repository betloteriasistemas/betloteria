<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

try {
    $con->begin_transaction();
    $response = [];
    $auditoria = "";
    $queryOldUsuario = "SELECT u.*, g.NOME as `GERENTE`, a.NOME as `AREA` 
                        FROM usuario u 
                        left join usuario g on (u.`COD_GERENTE` = g.`COD_USUARIO`)
                        inner join areas a on (u.`COD_AREA` = a.`COD_AREA` )";
    $stmtOldUsuario = null;
    $oldUsuario = null;

    $queryOldComissao = "SELECT * FROM configuracao_comissao_bicho 
    WHERE COD_SITE = ? AND (cod_usuario = ? or  cod_usuario in (select cod_usuario from usuario where cod_gerente = ?))";
    $stmtOldComissao = null;
    $oldComissao = null;

    //Informações do usuário logado
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $cod_site_usuario = mysqli_real_escape_string($con, $_POST['cod_site_usuario']);

    //Informações do usuário para cadastro/alteração
    $nome = mysqli_real_escape_string($con, $_POST['nome']);
    $login = mysqli_real_escape_string($con, $_POST['login']);
    $senha = mysqli_real_escape_string($con, $_POST['senha']);
    $perfil = mysqli_real_escape_string($con, $_POST['perfil']);
    $cod_area = mysqli_real_escape_string($con, $_POST['cod_area']);
    $nome_area = mysqli_real_escape_string($con, $_POST['nome_area']);
    $comissao_seninha = mysqli_real_escape_string($con, $_POST['comissaoSeninha']);
    $comissao_quininha = mysqli_real_escape_string($con, $_POST['comissaoQuininha']);
    $comissao_lotinha = mysqli_real_escape_string($con, $_POST['comissaoLotinha']);
    $comissao_bicho = mysqli_real_escape_string($con, $_POST['comissaoBicho']);
    $comissao_2pra500 = mysqli_real_escape_string($con, $_POST['comissao2pra500']);
    $comissao_rifa = mysqli_real_escape_string($con, $_POST['comissaoRifa']);
    $comissao_supersena = mysqli_real_escape_string($con, $_POST['comissaoSuperSena']);
    $email = mysqli_real_escape_string($con, $_POST['email']);
    $flg_seninha = mysqli_real_escape_string($con, $_POST['flg_seninha']);
    $flg_quininha = mysqli_real_escape_string($con, $_POST['flg_quininha']);
    $flg_lotinha = mysqli_real_escape_string($con, $_POST['flg_lotinha']);
    $flg_bicho = mysqli_real_escape_string($con, $_POST['flg_bicho']);
    $flg_2pra500 = mysqli_real_escape_string($con, $_POST['flg_2pra500']);
    $flg_rifa = mysqli_real_escape_string($con, $_POST['flg_rifa']);
    $flg_supersena = mysqli_real_escape_string($con, $_POST['flg_supersena']);
    $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
    $limite = mysqli_real_escape_string($con, $_POST['limite']);
    $codigoSite = mysqli_real_escape_string($con, $_POST['cod_site']);
    $cod_gerente = mysqli_real_escape_string($con, $_POST['cod_gerente']);
    $nome_gerente = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['nome_gerente']), null);

    $codigo_novo_usuario = 0;

    $query = "";

    $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_SITE = '$codigoSite'
              and COD_USUARIO = '$cod_usuario' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $saldo_usuario = $row['SALDO'];
    $perfil_usuario = $row['PERFIL'];

    if ($codigo == 0) {
        verificarUsuarioExistente($con, $codigoSite, $login, $cod_gerente);
        if ($saldo_usuario < $limite && $perfil_usuario != 'A') {
            throw new Exception('Saldo do gerente não permite a inclusão desse limite');
        }

        $stmt = $con->prepare("INSERT INTO usuario(COD_SITE, COD_GERENTE, LOGIN, SENHA, PERFIL, NOME, 
                     PCT_COMISSAO_SENINHA, PCT_COMISSAO_QUININHA, PCT_COMISSAO_LOTINHA, PCT_COMISSAO_BICHO, 
                     PCT_COMISSAO_2PRA500, PCT_COMISSAO_RIFA, PCT_COMISSAO_SUPERSENA, LIMITE, SALDO, 
                     EMAIL, FLG_SENINHA, FLG_QUININHA, FLG_LOTINHA, FLG_BICHO, FLG_2PRA500, FLG_RIFA, 
                     FLG_SUPER_SENA, COD_AREA)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
        $stmt->bind_param(
            "iissssdddddddddssssssssi",
            $codigoSite,
            $cod_gerente,
            $login,
            $senha,
            $perfil,
            $nome,
            $comissao_seninha,
            $comissao_quininha,
            $comissao_lotinha,
            $comissao_bicho,
            $comissao_2pra500,
            $comissao_rifa,
            $comissao_supersena,
            $limite,
            $limite,
            $email,
            $flg_seninha,
            $flg_quininha,
            $flg_lotinha,
            $flg_bicho,
            $flg_2pra500,
            $flg_rifa,
            $flg_supersena,
            $cod_area
        );

        $saldoGerente = $saldo_usuario - $limite;
    } else {
        $queryOldUsuario = $queryOldUsuario . " WHERE u.COD_USUARIO = ? AND u.COD_SITE = ?";
        $stmtOldUsuario = $con->prepare($queryOldUsuario);
        $stmtOldUsuario->bind_param("ii", $codigo, $codigoSite);
        $stmtOldUsuario->execute();
        $result = $stmtOldUsuario->get_result();
        $oldUsuario = $result->fetch_assoc();
        $stmtOldUsuario->close();
        if ($login != $oldUsuario['LOGIN']) {
            verificarUsuarioExistente($con, $codigoSite, $login, $cod_gerente);
        }

        $codigoSiteLimite = mysqli_real_escape_string($con, $_POST['cod_site']);
        $queryLimite = " SELECT LIMITE FROM usuario
            WHERE COD_SITE = '$codigoSiteLimite'
              and cod_usuario = '$codigo' ";

        $resultLimite = mysqli_query($con, $queryLimite);
        $rowLimite = mysqli_fetch_array($resultLimite, MYSQLI_ASSOC);
        $limiteAnterior = $rowLimite['LIMITE'];

        $limiteDesejado = mysqli_real_escape_string($con, $_POST['limite']);
        $saldoNecessario = $limiteDesejado - $limiteAnterior;
        if ($saldo_usuario < $saldoNecessario && $perfil_usuario != 'A') {
            throw new Exception('Saldo do gerente não permite a alteração de limite');
        }

        $saldoGerente = $saldo_usuario - $saldoNecessario;
        $stmt = $con->prepare("UPDATE usuario SET LOGIN = ?, SENHA = ?, PERFIL = ?, NOME = ?,  COD_GERENTE = ?, 
                                PCT_COMISSAO_SENINHA = ?, PCT_COMISSAO_QUININHA = ?, PCT_COMISSAO_LOTINHA = ?, 
                                PCT_COMISSAO_BICHO = ?, PCT_COMISSAO_2PRA500 = ?, PCT_COMISSAO_RIFA = ?, 
                                PCT_COMISSAO_SUPERSENA = ?, LIMITE = ?, SALDO = SALDO + ?, EMAIL = ?, FLG_SENINHA = ?, 
                                FLG_QUININHA = ?, FLG_LOTINHA = ?, FLG_BICHO = ?, FLG_2PRA500 = ?, FLG_RIFA = ?, 
                                FLG_SUPER_SENA = ?, COD_AREA = ?                               
		                       WHERE COD_USUARIO = ? AND COD_SITE = ?");

        $stmt->bind_param(
            "ssssidddddddddssssssssiii",
            $login,
            $senha,
            $perfil,
            $nome,
            $cod_gerente,
            $comissao_seninha,
            $comissao_quininha,
            $comissao_lotinha,
            $comissao_bicho,
            $comissao_2pra500,
            $comissao_rifa,
            $comissao_supersena,
            $limite,
            $saldo,
            $email,
            $flg_seninha,
            $flg_quininha,
            $flg_lotinha,
            $flg_bicho,
            $flg_2pra500,
            $flg_rifa,
            $flg_supersena,
            $cod_area,
            $codigo,
            $codigoSite
        );
        // ATUALIZANDO AS PERMISSÕES DOS CAMBISTAS DO GERENTE
        $stmtFlg = $con->prepare("UPDATE usuario SET FLG_SENINHA = ?, FLG_QUININHA = ?, FLG_LOTINHA = ?, FLG_BICHO = ?, FLG_2PRA500 = ?, FLG_RIFA = ?                                
		                       WHERE COD_GERENTE = ? AND COD_SITE = ?");
        $stmtFlg->bind_param("ssssssii", $flg_seninha, $flg_quininha, $flg_lotinha, $flg_bicho, $flg_2pra500, $flg_rifa, $codigo, $codigoSite);
    }

    if ($codigo != 0) {
        $saldo = $limite - $limiteAnterior;
    }

    $stmt->execute();

    if ($codigo != 0) {
        $stmtFlg->execute();
    }

    if ($codigo == 0) {
        $auditoria = $auditoria . descreverCriacaoUsuario(
            $perfil,
            $nome_gerente,
            $nome,
            $login,
            $email,
            $limite,
            $nome_area,
            $flg_seninha,
            $comissao_seninha,
            $flg_quininha,
            $comissao_quininha,
            $flg_lotinha,
            $comissao_lotinha,
            $flg_bicho,
            $comissao_bicho,
            $flg_2pra500,
            $comissao_2pra500,
            $flg_rifa,
            $comissao_rifa,
            $flg_supersena,
            $comissao_supersena
        );
        $codigo_novo_usuario = $stmt->insert_id;
    } else {
        $auditoria = $auditoria . descreverEdicaoUsuario(
            $oldUsuario,
            $perfil,
            $nome_gerente,
            $nome,
            $login,
            $email,
            $limite,
            $nome_area,
            $flg_seninha,
            $comissao_seninha,
            $flg_quininha,
            $comissao_quininha,
            $flg_lotinha,
            $comissao_lotinha,
            $flg_bicho,
            $comissao_bicho,
            $flg_2pra500,
            $comissao_2pra500,
            $flg_rifa,
            $comissao_rifa,
            $flg_supersena,
            $comissao_supersena
        );
        $codigo_novo_usuario = $codigo;
    }

    if ($perfil_usuario != 'A') {
        $stmt = $con->prepare("UPDATE usuario SET SALDO = ?
		                       WHERE COD_USUARIO = ? AND COD_SITE = ?");
        $stmt->bind_param("dii", $saldoGerente, $cod_usuario, $codigoSite);
        $stmt->execute();
        $response['saldoGerente'] = $saldoGerente;
    } else {
        $response['saldoGerente'] = $saldo_usuario;
    }

    // INSERINDO / ATUALIZANDO CONFIGURAÇÃO DE COMISSÃO        
    $queryConfig = " SELECT count(*) QTD FROM configuracao_comissao_bicho
            WHERE cod_usuario = '$codigo_novo_usuario' ";

    $resultConfig = mysqli_query($con, $queryConfig);
    $rowConfig = mysqli_fetch_array($resultConfig, MYSQLI_ASSOC);
    $inserir_config = $rowConfig['QTD'];

    $COMISSAO_MILHAR_CENTENA    = mysqli_real_escape_string($con, $_POST['comissao_milhar_centena']);
    $COMISSAO_MILHAR_SECA       = mysqli_real_escape_string($con, $_POST['comissao_milhar_seca']);
    $COMISSAO_MILHAR_INVERTIDA  = mysqli_real_escape_string($con, $_POST['comissao_milhar_invertida']);
    $COMISSAO_CENTENA           = mysqli_real_escape_string($con, $_POST['comissao_centena']);
    $COMISSAO_CENTENA_INVERTIDA = mysqli_real_escape_string($con, $_POST['comissao_centena_invertida']);
    $COMISSAO_GRUPO             = mysqli_real_escape_string($con, $_POST['comissao_grupo']);
    $COMISSAO_DUQUE_GRUPO       = mysqli_real_escape_string($con, $_POST['comissao_duque_grupo']);
    $COMISSAO_TERNO_GRUPO       = mysqli_real_escape_string($con, $_POST['comissao_terno_grupo']);
    $COMISSAO_QUINA_GRUPO       = mysqli_real_escape_string($con, $_POST['comissao_quina_grupo']);
    $COMISSAO_DEZENA            = mysqli_real_escape_string($con, $_POST['comissao_dezena']);
    $COMISSAO_DUQUE_DEZENA      = mysqli_real_escape_string($con, $_POST['comissao_duque_dezena']);
    $COMISSAO_TERNO_DEZENA      = mysqli_real_escape_string($con, $_POST['comissao_terno_dezena']);
    $COMISSAO_PASSE_SECO        = mysqli_real_escape_string($con, $_POST['comissao_passe_seco']);
    $COMISSAO_PASSE_COMBINADO   = mysqli_real_escape_string($con, $_POST['comissao_passe_combinado']);
    $COMISSAO_5P100             = mysqli_real_escape_string($con, $_POST['comissao_5p100']);

    if ($inserir_config == 0) {
        $stmtCOM = $con->prepare("INSERT INTO configuracao_comissao_bicho (cod_usuario, cod_site, comissao_milhar_centena, comissao_milhar_seca, comissao_milhar_invertida, comissao_centena,
        comissao_centena_invertida, comissao_grupo, comissao_duque_grupo, comissao_terno_grupo, comissao_quina_grupo, comissao_dezena, comissao_duque_dezena, comissao_terno_dezena, 
        comissao_passe_seco, comissao_passe_combinado, comissao_5p100) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

        $stmtCOM->bind_param(
            "idddddddddddddddd",
            $codigo_novo_usuario,
            $codigoSite,
            $COMISSAO_MILHAR_CENTENA,
            $COMISSAO_MILHAR_SECA,
            $COMISSAO_MILHAR_INVERTIDA,
            $COMISSAO_CENTENA,
            $COMISSAO_CENTENA_INVERTIDA,
            $COMISSAO_GRUPO,
            $COMISSAO_DUQUE_GRUPO,
            $COMISSAO_TERNO_GRUPO,
            $COMISSAO_QUINA_GRUPO,
            $COMISSAO_DEZENA,
            $COMISSAO_DUQUE_DEZENA,
            $COMISSAO_TERNO_DEZENA,
            $COMISSAO_PASSE_SECO,
            $COMISSAO_PASSE_COMBINADO,
            $COMISSAO_5P100
        );

        $auditoria = $auditoria . descreverComissoesBicho(
            $COMISSAO_MILHAR_CENTENA,
            $COMISSAO_MILHAR_SECA,
            $COMISSAO_MILHAR_INVERTIDA,
            $COMISSAO_CENTENA,
            $COMISSAO_CENTENA_INVERTIDA,
            $COMISSAO_GRUPO,
            $COMISSAO_DUQUE_GRUPO,
            $COMISSAO_TERNO_GRUPO,
            $COMISSAO_QUINA_GRUPO,
            $COMISSAO_DEZENA,
            $COMISSAO_DUQUE_DEZENA,
            $COMISSAO_TERNO_DEZENA,
            $COMISSAO_PASSE_SECO,
            $COMISSAO_PASSE_COMBINADO,
            $COMISSAO_5P100
        );
    } else {
        $stmtCOM = $con->prepare("UPDATE configuracao_comissao_bicho
                SET
                COMISSAO_MILHAR_CENTENA = ?,
                COMISSAO_MILHAR_SECA = ?,
                COMISSAO_MILHAR_INVERTIDA = ?,
                COMISSAO_CENTENA = ?,
                COMISSAO_CENTENA_INVERTIDA = ?,
                COMISSAO_GRUPO = ?,
                COMISSAO_DUQUE_GRUPO = ?,
                COMISSAO_TERNO_GRUPO = ?,
                COMISSAO_QUINA_GRUPO = ?,
                COMISSAO_DEZENA = ?,
                COMISSAO_DUQUE_DEZENA = ?,
                COMISSAO_TERNO_DEZENA = ?,
                COMISSAO_PASSE_SECO = ?,
                COMISSAO_PASSE_COMBINADO = ?,
                COMISSAO_5P100 = ?
                WHERE COD_SITE = '$codigoSite' 
                AND cod_usuario = '$codigo'");
        $stmtCOM->bind_param(
            "ddddddddddddddd",
            $COMISSAO_MILHAR_CENTENA,
            $COMISSAO_MILHAR_SECA,
            $COMISSAO_MILHAR_INVERTIDA,
            $COMISSAO_CENTENA,
            $COMISSAO_CENTENA_INVERTIDA,
            $COMISSAO_GRUPO,
            $COMISSAO_DUQUE_GRUPO,
            $COMISSAO_TERNO_GRUPO,
            $COMISSAO_QUINA_GRUPO,
            $COMISSAO_DEZENA,
            $COMISSAO_DUQUE_DEZENA,
            $COMISSAO_TERNO_DEZENA,
            $COMISSAO_PASSE_SECO,
            $COMISSAO_PASSE_COMBINADO,
            $COMISSAO_5P100
        );

        $stmtOldComissao = $con->prepare($queryOldComissao);
        $stmtOldComissao->bind_param("iii", $codigoSite, $codigo, $codigo);
        $stmtOldComissao->execute();
        $result = $stmtOldComissao->get_result();
        $oldComissao = $result->fetch_assoc();
        $stmtOldComissao->close();
        $auditoria = $auditoria . descreverEdicaoComissoesBicho(
            $oldComissao,
            $COMISSAO_MILHAR_CENTENA,
            $COMISSAO_MILHAR_SECA,
            $COMISSAO_MILHAR_INVERTIDA,
            $COMISSAO_CENTENA,
            $COMISSAO_CENTENA_INVERTIDA,
            $COMISSAO_GRUPO,
            $COMISSAO_DUQUE_GRUPO,
            $COMISSAO_TERNO_GRUPO,
            $COMISSAO_QUINA_GRUPO,
            $COMISSAO_DEZENA,
            $COMISSAO_DUQUE_DEZENA,
            $COMISSAO_TERNO_DEZENA,
            $COMISSAO_PASSE_SECO,
            $COMISSAO_PASSE_COMBINADO,
            $COMISSAO_5P100
        );
    }

    $stmtCOM->execute();

    inserir_auditoria(
        $con,
        $cod_usuario,
        $cod_site_usuario,
        ($codigo == 0 ? AUD_USUARIO_CRIADO : AUD_USUARIO_EDITADO),
        $auditoria
    );
    $response['status'] = "OK";
    $con->commit();
    $con->close();
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

function verificarUsuarioExistente($con, $codigoSite, $login, $cod_gerente)
{
    $query = " SELECT cod_gerente FROM usuario
    WHERE COD_SITE = '$codigoSite' and login = '$login' ";

    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if (mysqli_num_rows($result)) {
        if ($cod_gerente != $row['cod_gerente']) {
            throw new Exception('Login já existe e pertence a outro gerente!');
        } else {
            throw new Exception('Login já existe entre seus usuários!');
        }
    }
}
