angular.module('main').controller('usuarioCtrl', function ($rootScope, $scope, $http, configGeral, user, $route) {
    $rootScope.loading = false;
    $scope.editarUsuario = false;
    $scope.isCadastrarUsuario = false;
    $scope.nome = user.getNome();
    $scope.gerentes;
    $scope.nomeUsuarioEditado = null;
    $scope.gerenteUsuarioEditado = null;
    $scope.perfil = user.getPerfil();
    $scope.saldo = user.getSaldoAtualizado();
    $scope.flg_seninha = user.getflg_seninha();
    $scope.flg_quininha = user.getflg_quininha();
    $scope.flg_lotinha = user.getflg_lotinha();
    $scope.flg_bicho = user.getflg_bicho();
    $scope.flg_2pra500 = user.getflg_2pra500();
    $scope.flg_rifa = user.getflg_rifa();
    $scope.flg_supersena = user.getflg_supersena();
    $scope.chkExibirCambistas = false;
    $scope.edtCheckBicho = $scope.flg_bicho == 'S';
    $scope.edtCheck2pra500 = $scope.flg_2pra500 == 'S';
    $scope.edtCheckSeninha = $scope.flg_seninha == 'S';
    $scope.edtCheckQuininha = $scope.flg_quininha == 'S';
    $scope.edtCheckLotinha = $scope.flg_lotinha == 'S';
    $scope.edtCheckRifa = $scope.flg_rifa == 'S';
    $scope.edtCheckSuperSena = $scope.flg_supersena == 'S';

    $scope.nomeUsuario = '';
    $scope.usuarios = [];
    $scope.gerenteATransferir = '';
    $scope.isIconeArrowPermissaoComissao = false;
    $scope.selecionarGerente = {
        gerentes: [],
        gerenteSelecionado: ''
    };
    $scope.areaFiltro = null;
    $scope.totalUsuarios = 0;
    $scope.itensPorPagina = 20;

    $scope.paginacao = {
        atual: 1
    };

    $scope.alterarPagina = function (indexNovaPagina) {
        getResultadoBusca(indexNovaPagina);
    };

    function getResultadoBusca(indexNovaPagina) {
        const index = (indexNovaPagina - 1) * $scope.itensPorPagina;
        $scope.listarUsuarios(index, $scope.itensPorPagina);
    }

    $scope.cadastrarUsuario = function () {
        $scope.isCadastrarUsuario = true;
        limparCampos();
        if (user.getPerfil() == 'G') {
            $scope.comboAreas = user.getArea() + "";
            $scope.cmbPerfil = 'C';
            console.log($scope.comboAreas);
        }
    }

    $scope.getGerentes = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + 
                  '&site=' + user.getSite() + 
                  '&operacao=getCambistas' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.gerentes = response.data;
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    function limparCampos() {
        if ($scope.isCadastrarUsuario) {
            $scope.edtCheckSeninha = $scope.flg_seninha == 'S';
            $scope.edtCheckQuininha = $scope.flg_quininha == 'S';
            $scope.edtCheckLotinha = $scope.flg_lotinha == 'S';
            $scope.edtCheckBicho = $scope.flg_bicho == 'S';
            $scope.edtCheck2pra500 = $scope.flg_2pra500 == 'S';
            $scope.edtCheckRifa = $scope.flg_rifa == 'S';
            $scope.edtCheckSuperSena = $scope.flg_supersena == 'S';
        } else {
            $scope.edtCheckSeninha = undefined;
            $scope.edtCheckQuininha = undefined;
            $scope.edtCheckLotinha = undefined;
            $scope.edtCheckBicho = undefined;
            $scope.edtCheck2pra500 = undefined;
            $scope.edtCheckRifa = undefined;
            $scope.edtCheckSuperSena = undefined;
        }
        $scope.chkExibirCambistas = false;
        $scope.desabilitarCampoNomeCambista = false;
        $scope.nomeUsuario = '';
        $scope.editarUsuario = false;
        $scope.selecionarGerente.gerenteSelecionado = $scope.selecionarGerente.gerentes[0];
        $scope.perfilGerente = false;
        $scope.edtNome = undefined;
        $scope.edtLogin = undefined;
        $scope.cmbPerfil = undefined;
        $scope.cmbGerente = undefined;
        $scope.edtComissaoSeninha = undefined;
        $scope.edtComissaoQuininha = undefined;
        $scope.edtComissaoLotinha = undefined;
        $scope.edtComissaoBicho = undefined;
        $scope.edtComissao2pra500 = undefined;
        $scope.edtComissaoRifa = undefined;
        $scope.edtComissaoSuperSena = undefined;
        $scope.edtLimite = undefined;
        $scope.cod_usuario = undefined;
        $scope.edtSenha = undefined;
        $scope.edtEmail = undefined;
        $scope.comboAreas = undefined;
        $scope.edtComissaoMilharCentena = undefined;
        $scope.edtComissaoMilharSeca = undefined;
        $scope.edtComissaoMilharInvertida = undefined;
        $scope.edtComissaoCentena = undefined;
        $scope.edtComissaoCentenaInvertida = undefined;
        $scope.edtComissaoGrupo = undefined;
        $scope.edtComissaoDuqueGrupo = undefined;
        $scope.edtComissaoTernoGrupo = undefined;
        $scope.edtComissaoQuinaGrupo = undefined;
        $scope.edtComissaoDezena = undefined;
        $scope.edtComissaoDuqueDezena = undefined;
        $scope.edtComissaoTernoDezena = undefined;
        $scope.edtComissaoPasseSeco = undefined;
        $scope.edtComissaoPasseCombinado = undefined;
        $scope.edtComissao5p100 = undefined;
    }
    $scope.exibirCamposComissaoBicho = function () {
        $scope.exibirCamposComissaoEspecificaBicho = !$scope.exibirCamposComissaoEspecificaBicho;
    }

    $scope.lidarSelecaoGerente = function () {
        if ($scope.selecionarGerente.gerenteSelecionado.login) {
            $scope.desabilitarCampoNomeCambista = true;
            $scope.nomeUsuario = '';
            $scope.chkExibirCambistas = true;
            $scope.listarUsuarios(0, 9999, $scope.selecionarGerente.gerenteSelecionado.login);
        } else {
            $scope.desabilitarCampoNomeCambista = false;
            $scope.nomeUsuario = '';
            $scope.records = preencherNomeGerente($scope.usuarios);
        }
    }

    $scope.lidarSelecaoArea = function (codArea) {
        var area = $scope.areas.filter(function (area) {
            return area.cod_area == codArea
        })[0];
        if (area) {
            $scope.edtCheck2pra500 = area.flg_2pra500 == 'S';
            $scope.edtCheckBicho = area.flg_bicho == 'S';
            $scope.edtCheckSeninha = area.flg_seninha == 'S';
            $scope.edtCheckQuininha = area.flg_quininha == 'S';
            $scope.edtCheckLotinha = area.flg_lotinha == 'S';
            $scope.edtCheckRifa = area.flg_rifa == 'S';
            $scope.edtCheckSuperSena = area.flg_supersena == 'S';
            if (user.getPerfil() == 'G') {
                configGeral.get().then(function (data) {
                    $scope.isExibir2pra500 = $scope.edtCheck2pra500 && data.flg_2pra500;
                    $scope.isExibirBicho = $scope.edtCheckBicho && data.flg_bicho;
                    $scope.isExibirSeninha = $scope.edtCheckSeninha && data.flg_seninha;
                    $scope.isExibirQuininha = $scope.edtCheckQuininha && data.flg_quininha;
                    $scope.isExibirLotinha = $scope.edtCheckLotinha && data.flg_lotinha;
                    $scope.isExibirRifa = $scope.edtCheckRifa && data.flg_rifa;
                    $scope.isExibirSuperSena = $scope.edtCheckSuperSena && data.flg_supersena;
                });
            } else {
                $scope.isExibir2pra500 = $scope.edtCheck2pra500;
                $scope.isExibirBicho = $scope.edtCheckBicho;
                $scope.isExibirSeninha = $scope.edtCheckSeninha;
                $scope.isExibirQuininha = $scope.edtCheckQuininha;
                $scope.isExibirLotinha = $scope.edtCheckLotinha;
                $scope.isExibirRifa = $scope.edtCheckRifa;
                $scope.isExibirSuperSena = $scope.edtCheckSuperSena;
            }
        } else {
            $scope.edtCheck2pra500 = true;
            $scope.edtCheckBicho = true;
            $scope.edtCheckSeninha = true;
            $scope.edtCheckQuininha = true;
            $scope.edtCheckLotinha = true;
            $scope.edtCheckSuperSena = true;
            configGeral.get().then(function (data) {
                $scope.isExibir2pra500 = $scope.edtCheck2pra500 && data.flg_2pra500;
                $scope.isExibirBicho = $scope.edtCheckBicho && data.flg_bicho;
                $scope.isExibirSeninha = $scope.edtCheckSeninha && data.flg_seninha;
                $scope.isExibirQuininha = $scope.edtCheckQuininha && data.flg_quininha;
                $scope.isExibirLotinha = $scope.edtCheckLotinha && data.flg_lotinha;
                $scope.isExibirRifa = $scope.edtCheckRifa && data.flg_rifa;
                $scope.isExibirSuperSena = $scope.edtCheckSuperSena && data.flg_supersena;
            });
        }
    }

    $scope.isExibePermissaoComissao = function (flagComissao) {
        return flagComissao;
    }


    $scope.cancelar = function () {

        if ($scope.isCadastrarUsuario) {
            $route.reload();
        } else if ($scope.editarUsuario) {
            if (user.getPerfil() == 'A' && $scope.areas.length > 1) {
                $scope.isExibir2pra500 = false;
                $scope.isExibirBicho = false;
                $scope.isExibirSeninha = false;
                $scope.isExibirQuininha = false;
                $scope.isExibirLotinha = false;
                $scope.isExibirRifa = false;
                $scope.isExibirSuperSena = false;
                for (let a of $scope.areas) {
                    $scope.isExibir2pra500 = $scope.isExibir2pra500 || (a.flg_2pra500 === 'S');
                    $scope.isExibirBicho = $scope.isExibirBicho || (a.flg_bicho === 'S');
                    $scope.isExibirSeninha = $scope.isExibirSeninha || (a.flg_seninha === 'S');
                    $scope.isExibirQuininha = $scope.isExibirQuininha || (a.flg_quininha === 'S');
                    $scope.isExibirLotinha = $scope.isExibirLotinha || (a.flg_lotinha === 'S');
                    $scope.isExibirRifa = $scope.isExibirRifa || (a.flg_rifa === 'S');
                    $scope.isExibirSuperSena = $scope.isExibirSuperSena || (a.flg_supersena === 'S');
                }
            } else {
                configGeral.get().then(function (data) {
                    $scope.isExibir2pra500 = data.flg_2pra500;
                    $scope.isExibirBicho = data.flg_bicho;
                    $scope.isExibirSeninha = data.flg_seninha;
                    $scope.isExibirQuininha = data.flg_quininha;
                    $scope.isExibirLotinha = data.flg_lotinha;
                    $scope.isExibirRifa = data.flg_rifa;
                    $scope.isExibirSuperSena = data.flg_supersena;
                });
            }
            $scope.editarUsuario = false;
        } else {
            $route.reload();
        }
    }

    $scope.listarGerentes = function () {
        if ($scope.selecionarGerente.gerentes.length > 0) {
            $scope.selecionarGerente.gerenteSelecionado = $scope.selecionarGerente.gerentes[0];
        }        
        $scope.listarUsuarios();
    }

    $scope.listarUsuarios = function (
        index = 0,
        qtdElementos = $scope.itensPorPagina,
        username = user.getName()
    ) {
        $http({
            method: "POST",
            url: "angularjs-mysql/usuarios.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + username 
                + '&codArea=' + $scope.areaFiltro
                + '&site=' + user.getSite()
                + '&cambistas=' + $scope.chkExibirCambistas
                + '&index=' + index
                + '&qtdElementos=' + qtdElementos
                + '&perfil=' + user.getPerfil()
                + '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data.filter(obj => !obj.totalizadores);

            const totalizadores = response.data.filter(obj => obj.totalizadores);
            $scope.totalUsuarios = totalizadores[0].totalizadores['total_registros'];
            if (user.getPerfil() == 'A' && !$scope.selecionarGerente.gerenteSelecionado.login) {
                $scope.usuarios = $scope.records;
                $scope.selecionarGerente.gerentes = preencherCombo($scope.records);
                $scope.selecionarGerente.gerenteSelecionado = $scope.selecionarGerente.gerentes[0];
                $scope.records = preencherNomeGerente($scope.records);
            } else if (user.getPerfil() == 'A') {
                $scope.records = preencherNomeGerente($scope.records);
            }
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }
    function preencherNomeGerente(usuarios) {
        usuarios = usuarios.map(usuario => {
            $scope.selecionarGerente.gerentes.map(gerente => {
                if (usuario.cod_gerente === gerente.cod_usuario) {
                    usuario = {
                        ...usuario,
                        nome_gerente: gerente.nome
                    }
                }
            })
            return usuario;
        });
        return usuarios;
    }

    function preencherCombo(usuarios) {
        const combo = usuarios.filter(function (usuario) {
            return usuario.perfil == 'G'
        }).map(usuario => {
            if (usuario.status.toLowerCase() === 'a') {
                usuario = {
                    ...usuario,
                    status_usuario: '[ATIVO]'
                };
            } else {
                usuario = {
                    ...usuario,
                    status_usuario: '[INATIVO]'
                };
            }
            usuario = {
                ...usuario,
                descricao: `${usuario.nome} - ${usuario.status_usuario}`
            }
            return usuario;
        });
        combo.splice(0, 0, {
            descricao: '--- Selecione um Gerente ---',
            login: null
        })
        return combo;
    }

    $scope.getDescSimNao = function (federal) {
        if (federal === "S") {
            return "SIM";
        } else {
            return "NÃO";
        }
    };

    $scope.exibirCambistas = function () {
        $scope.chkExibirCambistas = !$scope.chkExibirCambistas;
        $scope.desabilitarCampoNomeCambista = false;
        if ($scope.chkExibirCambistas) {
            $scope.selecionarGerente.gerentes = preencherCombo($scope.records);
            $scope.selecionarGerente.gerenteSelecionado = $scope.selecionarGerente.gerentes[0];
            $scope.records = [];
        } else {
            $scope.nomeUsuario = '';
            $scope.listarUsuarios();
        }
    }

    $scope.getPerfilUsuario = function (perfil) {
        if (perfil == "C") {
            return "Cambista";
        } else if (perfil == "G") {
            return "Gerente";
        } else if (perfil == "A") {
            return "Admin";
        }
    }

    $scope.getStatusUsuario = function (status) {
        if (status == "A") {
            return "Ativo";
        } else if (status == "I") {
            return "Inativo";
        }
    }


    $scope.prestarConta = function (codigo, nome, login, limite, saldo) {
        $.confirm({
            title: '',
            content:"Confirma a prestação de conta do login:  " + login + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    let valor = limite - saldo;
                    $http({
                        url: 'angularjs-mysql/prestarContaUsuario.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() +
                            '&cod_site=' + user.getSite() +
                            '&codigo=' + codigo +
                            '&nome=' + nome +
                            '&valor=' + valor +
                            '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }).catch(function (err) {
                        console.log('Erro no $HTTP: ' + err.data);
                    });
                }
            }
        });
    }

    $scope.resetarSenha = function (codigo, nome, login) {
        $.confirm({
            title: '',
            content:"Confirma resetar a senha do login:  " + login + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/resetarSenhaUsuario.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() +
                            '&cod_site=' + user.getSite() +
                            '&codigo=' + codigo +
                            '&nome=' + nome +
                            '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $.alert("Senha resetada com sucesso!");
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }).catch(function (err) {
                        console.log('Erro no $HTTP' + err.data);
                    });
                }
            }
        });
    }

    $scope.excluir = function (codigo, nome, login, status) {

        var stString = '';
        if (status == 'A') {
            stString = 'Inativação';
        } else {
            stString = 'Ativação';
        }

        $.confirm({
            title: '',
            content:"Confirma " + stString + " do Login: " + login + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/excluirUsuario.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'codigo=' + codigo +
                            '&nome=' + nome +
                            '&cod_usuario=' + user.getCodigo() +
                            '&cod_site=' + user.getSite() +
                            '&perfil=' + user.getPerfil() +
                            '&status=' + status +
                            '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            if (user.getPerfil() != 'A') {
                                user.atualizaSaldo(response.data.saldo);
                            }
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    }

    $scope.transferirGerencia = function() {

        if ($scope.gerenteATransferir == "undefined" || $scope.gerenteATransferir == '') {
            $.alert("Selecione um novo gerente para transferência dos cambistas!");
            return;
        } 

        jQuery("#modalTransfGerente").modal('hide');

        $.confirm({
            title: '',
            content:"Ao realizar essa operação o usuário " +
            "será excluído do banco de dados e todos os seus registros apagados, essa operação " +
            "não poderá ser desfeita. Confirma a exclusão?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $rootScope.loading = true;
                    $http({
                        url: 'angularjs-mysql/excluirUsuarioFisicamente.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'codigo=' + $scope.cod_gerente_remocao +
                            '&cod_usuario=' + user.getCodigo() +
                            '&cod_site=' + user.getSite() +
                            '&gerente_transferir=' + $scope.gerenteATransferir.cod_usuario +
                            '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                        $rootScope.loading = false;
                    })
                }
            }
        });
    }

    $scope.preencherComboTransferenciaGerente = function(codigo) {
        $scope.gerenteATransferir = '';
        $scope.cod_gerente_remocao = codigo;
        $scope.comboTransferenciaGerente = [];
        for (var i=0; i < $scope.usuarios.length; i++) {
            if ($scope.usuarios[i].perfil == 'G' && $scope.usuarios[i].status == 'A' 
                && $scope.usuarios[i].codigo != codigo) {
                $scope.comboTransferenciaGerente.push($scope.usuarios[i]);
            }
        }
    }
    
    $scope.excluirFisicamente = function (codigo, perfil) {

        if (perfil == 'A') {
        $.alert("Não é possível excluir este usuário pois ele tem perfil ADMIN");
            return;
        }

        $.confirm({
            title: '',
            content:"Ao realizar essa operação o usuário " +
            "será excluído do banco de dados e todos os seus registros apagados, essa operação " +
            "não poderá ser desfeita. Confirma a exclusão?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $rootScope.loading = true;
                    $http({
                        url: 'angularjs-mysql/excluirUsuarioFisicamente.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'codigo=' + codigo +
                            '&cod_usuario=' + user.getCodigo() +
                            '&cod_site=' + user.getSite() +
                            '&gerente_transferir=' +
                            '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                        $rootScope.loading = false;
                    });
                }
            }
        });

    }

    $scope.alterar = function (usuario) {
        topoScroll();
        if (usuario.perfil == 'G') {
            $scope.perfilGerente = true;
        } else {
            $scope.perfilGerente = false;
        }
        $scope.editarUsuario = true;
        $scope.edtNome = usuario.nome;
        $scope.nomeUsuarioEditado = usuario.nome;
        $scope.edtLogin = usuario.login;
        $scope.cmbPerfil = usuario.perfil;
        $scope.cmbGerente = usuario.cod_gerente + "";
        $scope.gerenteUsuarioEditado = usuario.cod_gerente + "";
        $scope.edtComissaoSeninha = parseFloat(usuario.pct_comissao_seninha);
        $scope.edtComissaoQuininha = parseFloat(usuario.pct_comissao_quininha);
        $scope.edtComissaoLotinha = parseFloat(usuario.pct_comissao_lotinha);
        $scope.edtComissaoBicho = parseFloat(usuario.pct_comissao_bicho);
        $scope.edtComissao2pra500 = parseFloat(usuario.pct_comissao_2pra500);
        $scope.edtComissaoRifa = parseFloat(usuario.pct_comissao_rifa);
        $scope.edtComissaoSuperSena = parseFloat(usuario.pct_comissao_supersena);
        $scope.edtLimite = parseFloat(usuario.limite).toFixed(2);
        $scope.edtLimite = $scope.edtLimite.replace(",", "");
        $scope.edtLimite = $scope.edtLimite.replace(".", ",");
        $scope.cod_usuario = usuario.cod_usuario;
        $scope.edtSenha = usuario.senha;
        $scope.edtEmail = usuario.email;
        $scope.comboAreas = usuario.cod_area + "";

        $scope.edtCheckSeninha = usuario.flg_seninha === "S";
        $scope.edtCheckQuininha = usuario.flg_quininha === "S";
        $scope.edtCheckLotinha = usuario.flg_lotinha === "S";
        $scope.edtCheckBicho = usuario.flg_bicho === "S";
        $scope.edtCheck2pra500 = usuario.flg_2pra500 === "S";
        $scope.edtCheckRifa = usuario.flg_rifa === "S";
        $scope.edtCheckSuperSena = usuario.flg_supersena === "S";

        var area = $scope.areas.filter(function (area) {
            return area.cod_area == usuario.cod_area
        })[0];

        if (user.getPerfil() == 'G') {
            configGeral.get().then(function (data) {
                $scope.isExibir2pra500 = $scope.edtCheck2pra500 && data.flg_2pra500;
                $scope.isExibirBicho = $scope.edtCheckBicho && data.flg_bicho;
                $scope.isExibirSeninha = $scope.edtCheckSeninha && data.flg_seninha;
                $scope.isExibirQuininha = $scope.edtCheckQuininha && data.flg_quininha;
                $scope.isExibirLotinha = $scope.edtCheckLotinha && data.flg_lotinha;
                $scope.isExibirRifa = $scope.edtCheckRifa && data.flg_rifa;
                $scope.isExibirSuperSena = $scope.edtCheckSuperSena && data.flg_supersena;
            });
        } else {
            $scope.isExibir2pra500 = area.flg_2pra500 === 'S';
            $scope.isExibirBicho = area.flg_bicho === 'S';
            $scope.isExibirSeninha = area.flg_seninha === 'S';
            $scope.isExibirQuininha = area.flg_quininha === 'S';
            $scope.isExibirLotinha = area.flg_lotinha === 'S';
            $scope.isExibirRifa = area.flg_rifa === 'S';
            $scope.isExibirSuperSena = area.flg_supersena == 'S';
        }


        $scope.edtComissaoMilharCentena = parseFloat(usuario.comissao_milhar_centena);
        $scope.edtComissaoMilharSeca = parseFloat(usuario.comissao_milhar_seca);
        $scope.edtComissaoMilharInvertida = parseFloat(usuario.comissao_milhar_invertida);
        $scope.edtComissaoCentena = parseFloat(usuario.comissao_centena);
        $scope.edtComissaoCentenaInvertida = parseFloat(usuario.comissao_centena_invertida);
        $scope.edtComissaoGrupo = parseFloat(usuario.comissao_grupo);
        $scope.edtComissaoDuqueGrupo = parseFloat(usuario.comissao_duque_grupo);
        $scope.edtComissaoTernoGrupo = parseFloat(usuario.comissao_terno_grupo);
        $scope.edtComissaoQuinaGrupo = parseFloat(usuario.comissao_quina_grupo);
        $scope.edtComissaoDezena = parseFloat(usuario.comissao_dezena);
        $scope.edtComissaoDuqueDezena = parseFloat(usuario.comissao_duque_dezena);
        $scope.edtComissaoTernoDezena = parseFloat(usuario.comissao_terno_dezena);
        $scope.edtComissaoPasseSeco = parseFloat(usuario.comissao_passe_seco);
        $scope.edtComissaoPasseCombinado = parseFloat(usuario.comissao_passe_combinado);
        $scope.edtComissao5p100 = parseFloat(usuario.comissao_5p100);
    }

    $scope.salvar = function (codigo) {
        var nome = $scope.edtNome;
        var login = $scope.edtLogin;
        var senha = $scope.edtSenha;
        var perfil = $scope.cmbPerfil;
        var area = $scope.comboAreas;
        var area_desc = "";

        var comissaoSeninha = typeof $scope.edtComissaoSeninha === "undefined" ? 0 : $scope.edtComissaoSeninha;
        var comissaoQuininha = typeof $scope.edtComissaoQuininha === "undefined" ? 0 : $scope.edtComissaoQuininha;
        var comissaoLotinha = typeof $scope.edtComissaoLotinha === "undefined" ? 0 : $scope.edtComissaoLotinha;
        var comissaoBicho = typeof $scope.edtComissaoBicho === "undefined" ? 0 : $scope.edtComissaoBicho;
        var comissao2pra500 = typeof $scope.edtComissao2pra500 === "undefined" ? 0 : $scope.edtComissao2pra500;
        var comissaoRifa = typeof $scope.edtComissaoRifa === "undefined" ? 0 : $scope.edtComissaoRifa;
        var comissaoSuperSena = typeof $scope.edtComissaoSuperSena === "undefined" ? 0 : $scope.edtComissaoSuperSena;

        var COMISSAO_MILHAR_CENTENA = typeof $scope.edtComissaoMilharCentena === "undefined" ? 0 : $scope.edtComissaoMilharCentena;
        var COMISSAO_MILHAR_SECA = typeof $scope.edtComissaoMilharSeca === "undefined" ? 0 : $scope.edtComissaoMilharSeca;
        var COMISSAO_MILHAR_INVERTIDA = typeof $scope.edtComissaoMilharInvertida === "undefined" ? 0 : $scope.edtComissaoMilharInvertida;
        var COMISSAO_CENTENA = typeof $scope.edtComissaoCentena === "undefined" ? 0 : $scope.edtComissaoCentena;
        var COMISSAO_CENTENA_INVERTIDA = typeof $scope.edtComissaoCentenaInvertida === "undefined" ? 0 : $scope.edtComissaoCentenaInvertida;
        var COMISSAO_GRUPO = typeof $scope.edtComissaoGrupo === "undefined" ? 0 : $scope.edtComissaoGrupo;
        var COMISSAO_DUQUE_GRUPO = typeof $scope.edtComissaoDuqueGrupo === "undefined" ? 0 : $scope.edtComissaoDuqueGrupo;
        var COMISSAO_TERNO_GRUPO = typeof $scope.edtComissaoTernoGrupo === "undefined" ? 0 : $scope.edtComissaoTernoGrupo;
        var COMISSAO_QUINA_GRUPO = typeof $scope.edtComissaoQuinaGrupo === "undefined" ? 0 : $scope.edtComissaoQuinaGrupo;
        var COMISSAO_DEZENA = typeof $scope.edtComissaoDezena === "undefined" ? 0 : $scope.edtComissaoDezena;
        var COMISSAO_DUQUE_DEZENA = typeof $scope.edtComissaoDuqueDezena === "undefined" ? 0 : $scope.edtComissaoDuqueDezena;
        var COMISSAO_TERNO_DEZENA = typeof $scope.edtComissaoTernoDezena === "undefined" ? 0 : $scope.edtComissaoTernoDezena;
        var COMISSAO_PASSE_SECO = typeof $scope.edtComissaoPasseSeco === "undefined" ? 0 : $scope.edtComissaoPasseSeco;
        var COMISSAO_PASSE_COMBINADO = typeof $scope.edtComissaoPasseCombinado === "undefined" ? 0 : $scope.edtComissaoPasseCombinado;
        var COMISSAO_5P100 = typeof $scope.edtComissao5p100 === "undefined" ? 0 : $scope.edtComissao5p100;

        var cod_gerente = null;
        var nome_gerente = null;
        if (user.getPerfil() == 'A' && perfil == 'C') {
            cod_gerente = $scope.cmbGerente;
            var ger = $.grep($scope.gerentes, function (gerente) {
                return gerente.cod_usuario == cod_gerente;
            });
            if (ger.length > 0) {
                nome_gerente = ger[0].nome;
            }
        } else {
            cod_gerente = user.getCodigo();
        }
        var cod_site = user.getSite();
        var limite = $scope.edtLimite;

        var email = $scope.edtEmail;

        if (email == null) {
            email = '';
        }

        var flg_seninha = $scope.edtCheckSeninha;
        var flg_quininha = $scope.edtCheckQuininha;
        var flg_lotinha = $scope.edtCheckLotinha;
        var flg_bicho = $scope.edtCheckBicho;
        var flg_2pra500 = $scope.edtCheck2pra500;
        var flg_rifa = $scope.edtCheckRifa;
        var flg_supersena = $scope.edtCheckSuperSena;

        if (typeof codigo === "undefined") {
            codigo = 0;
            senha = "12345";
        }

        if (flg_seninha == true) {
            flg_seninha = "S";
        } else {
            flg_seninha = "N";
        }

        if (flg_quininha == true) {
            flg_quininha = "S";
        } else {
            flg_quininha = "N";
        }

        if (flg_lotinha == true) {
            flg_lotinha = "S";
        } else {
            flg_lotinha = "N";
        }

        if (flg_bicho == true) {
            flg_bicho = "S";
        } else {
            flg_bicho = "N";
        }

        if (flg_2pra500 == true) {
            flg_2pra500 = "S";
        } else {
            flg_2pra500 = "N";
        }

        if (flg_rifa == true) {
            flg_rifa = "S";
        } else {
            flg_rifa = "N";
        }

        if (flg_supersena == true) {
            flg_supersena = "S";
        } else {
            flg_supersena = "N";
        }

        if (typeof nome === "undefined" || nome == "null" || nome == "") {
            $.alert('Informe o NOME!');
            return;
        } else if (typeof login === "undefined" || login == "null" || login == "") {
            $.alert('Informe o LOGIN!')
            return;
        } else if (typeof senha === "undefined" || senha == "null" || senha == "") {
            $.alert('Informe a SENHA!')
            return;
        } else if (typeof perfil === "undefined" || perfil == "null" || perfil == "") {
            $.alert('Selecione o PERFIL!')
        } else if (typeof cod_gerente === "undefined" || cod_gerente == "null" || cod_gerente == "") {
            $.alert('Selecione o GERENTE!')
        } else if (typeof limite === "undefined" || limite == "null" || limite == "") {
            $.alert('Informe o LIMITE!')
            return;
        } else if ($scope.areas.length > 1 && (typeof area === "undefined" || area == null || area == "")) {
            $.alert('Informe a ÁREA!')
            return;
        } else {

            if (perfil == 'C' && $scope.gerenteUsuarioEditado && $scope.gerenteUsuarioEditado != cod_gerente) {

                $.confirm({
                    title: '',
                    content: "Os lançamentos do cambista serão levados para o novo gerente. Confirma a alteração?",
                    buttons: {                        
                        cancelar: function() {
                            $scope.cmbGerente = $scope.gerenteUsuarioEditado + "";
                            $("#cmbGerente").val($scope.cmbGerente);
                            return;
                        },
                        ok: function () {
                            $scope.persisteUsuario(codigo, nome, login, senha, perfil, area, area_desc, comissaoSeninha, comissaoQuininha, 
                                comissaoLotinha, comissaoBicho, comissao2pra500, comissaoRifa, comissaoSuperSena, COMISSAO_MILHAR_CENTENA, 
                                COMISSAO_MILHAR_SECA, COMISSAO_MILHAR_INVERTIDA, COMISSAO_CENTENA, COMISSAO_CENTENA_INVERTIDA, COMISSAO_GRUPO, 
                                COMISSAO_DUQUE_GRUPO, COMISSAO_TERNO_GRUPO, COMISSAO_QUINA_GRUPO, COMISSAO_DEZENA, COMISSAO_DUQUE_DEZENA, COMISSAO_TERNO_DEZENA, 
                                COMISSAO_PASSE_SECO, COMISSAO_PASSE_COMBINADO, COMISSAO_5P100, cod_gerente, nome_gerente, cod_site, limite, email, flg_seninha, 
                                flg_quininha, flg_lotinha, flg_bicho, flg_2pra500, flg_rifa, flg_supersena);
                        }                
                    }
                });
                
            } else {
                $scope.persisteUsuario(codigo, nome, login, senha, perfil, area, area_desc, comissaoSeninha, comissaoQuininha, 
                    comissaoLotinha, comissaoBicho, comissao2pra500, comissaoRifa, comissaoSuperSena, COMISSAO_MILHAR_CENTENA, 
                    COMISSAO_MILHAR_SECA, COMISSAO_MILHAR_INVERTIDA, COMISSAO_CENTENA, COMISSAO_CENTENA_INVERTIDA, COMISSAO_GRUPO, 
                    COMISSAO_DUQUE_GRUPO, COMISSAO_TERNO_GRUPO, COMISSAO_QUINA_GRUPO, COMISSAO_DEZENA, COMISSAO_DUQUE_DEZENA, COMISSAO_TERNO_DEZENA, 
                    COMISSAO_PASSE_SECO, COMISSAO_PASSE_COMBINADO, COMISSAO_5P100, cod_gerente, nome_gerente, cod_site, limite, email, flg_seninha, 
                    flg_quininha, flg_lotinha, flg_bicho, flg_2pra500, flg_rifa, flg_supersena);
            }
        }
    }

    $scope.persisteUsuario = function(codigo, nome, login, senha, perfil, area, area_desc, comissaoSeninha, comissaoQuininha, 
        comissaoLotinha, comissaoBicho, comissao2pra500, comissaoRifa, comissaoSuperSena, COMISSAO_MILHAR_CENTENA, 
        COMISSAO_MILHAR_SECA, COMISSAO_MILHAR_INVERTIDA, COMISSAO_CENTENA, COMISSAO_CENTENA_INVERTIDA, COMISSAO_GRUPO, 
        COMISSAO_DUQUE_GRUPO, COMISSAO_TERNO_GRUPO, COMISSAO_QUINA_GRUPO, COMISSAO_DEZENA, COMISSAO_DUQUE_DEZENA, COMISSAO_TERNO_DEZENA, 
        COMISSAO_PASSE_SECO, COMISSAO_PASSE_COMBINADO, COMISSAO_5P100, cod_gerente, nome_gerente, cod_site, limite, email, flg_seninha, 
        flg_quininha, flg_lotinha, flg_bicho, flg_2pra500, flg_rifa, flg_supersena) {

        if ($scope.areas.length == 1) {
            area = $scope.areas[0].cod_area;
        }
        limite = limite.replace(/\./g, "");
        limite = limite.replace(",", ".");
        area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;
        $http({

            url: 'angularjs-mysql/salvarUsuario.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() +
                '&cod_site_usuario=' + user.getSite() +
                '&nome=' + nome +
                '&login=' + login +
                '&senha=' + senha +
                '&perfil=' + perfil +
                '&cod_area=' + area +
                '&nome_area=' + area_desc +
                '&comissaoSeninha=' + comissaoSeninha +
                '&comissaoQuininha=' + comissaoQuininha +
                '&comissaoLotinha=' + comissaoLotinha +
                '&comissaoBicho=' + comissaoBicho +
                '&comissao2pra500=' + comissao2pra500 +
                '&comissaoRifa=' + comissaoRifa +
                '&comissaoSuperSena=' + comissaoSuperSena +
                '&cod_gerente=' + cod_gerente +
                '&nome_gerente=' + nome_gerente +
                '&email=' + email +
                '&flg_seninha=' + flg_seninha +
                '&flg_quininha=' + flg_quininha +
                '&flg_lotinha=' + flg_lotinha +
                '&flg_bicho=' + flg_bicho +
                '&flg_2pra500=' + flg_2pra500 +
                '&flg_rifa=' + flg_rifa +
                '&flg_supersena=' + flg_supersena +
                '&codigo=' + codigo +
                '&cod_site=' + cod_site +
                '&limite=' + limite +
                '&comissao_milhar_seca=' + COMISSAO_MILHAR_SECA +
                '&comissao_milhar_centena=' + COMISSAO_MILHAR_CENTENA + 
                '&comissao_milhar_invertida=' + COMISSAO_MILHAR_INVERTIDA + 
                '&comissao_centena=' + COMISSAO_CENTENA +
                '&comissao_centena_invertida=' + COMISSAO_CENTENA_INVERTIDA + 
                '&comissao_grupo=' + COMISSAO_GRUPO +
                '&comissao_duque_grupo=' + COMISSAO_DUQUE_GRUPO + 
                '&comissao_terno_grupo=' + COMISSAO_TERNO_GRUPO +
                '&comissao_quina_grupo=' + COMISSAO_QUINA_GRUPO +
                '&comissao_dezena=' + COMISSAO_DEZENA + 
                '&comissao_duque_dezena=' + COMISSAO_DUQUE_DEZENA +
                '&comissao_terno_dezena=' + COMISSAO_TERNO_DEZENA + 
                '&comissao_passe_seco=' + COMISSAO_PASSE_SECO +
                '&comissao_passe_combinado=' + COMISSAO_PASSE_COMBINADO +
                '&comissao_5p100=' + COMISSAO_5P100 +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            $rootScope.loading = false;
            if (response.data.status == 'OK') {
                user.atualizaSaldo(response.data.saldoGerente);
                if ($scope.editarUsuario) {
                    if (!$scope.selecionarGerente.gerenteSelecionado.login) {
                        limparCampos();
                        $scope.listarUsuarios();
                    } else {
                        $scope.listarUsuarios(0, 9999, $scope.selecionarGerente.gerenteSelecionado.login);
                    }
                } else if ($scope.isCadastrarUsuario) {
                    limparCampos();
                    $scope.listarUsuarios();
                }
                $scope.isCadastrarUsuario = false;
                $scope.editarUsuario = false;
            } else {
                $.alert(response.data.mensagem);
            }
        });
    }

    function topoScroll() {
        $('html, body').animate({ scrollTop: $('.container-fluid').offset().top - 100 }, 'slow');
    }
    
    $scope.carregarAreas = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/areas.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&operacao=listarAtivos' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.areas = response.data;
            if (user.getPerfil() == 'A' && $scope.areas.length > 1) {
                $scope.isExibir2pra500 = false;
                $scope.isExibirBicho = false;
                $scope.isExibirSeninha = false;
                $scope.isExibirQuininha = false;
                $scope.isExibirLotinha = false;
                $scope.isExibirRifa = false;
                $scope.isExibirSuperSena = false;
                for (let a of $scope.areas) {
                    $scope.isExibir2pra500 = $scope.isExibir2pra500 || (a.flg_2pra500 === 'S');
                    $scope.isExibirBicho = $scope.isExibirBicho || (a.flg_bicho === 'S');
                    $scope.isExibirSeninha = $scope.isExibirSeninha || (a.flg_seninha === 'S');
                    $scope.isExibirQuininha = $scope.isExibirQuininha || (a.flg_quininha === 'S');
                    $scope.isExibirLotinha = $scope.isExibirLotinha || (a.flg_lotinha === 'S');
                    $scope.isExibirRifa = $scope.isExibirRifa || (a.flg_rifa === 'S');
                    $scope.isExibirSuperSena = $scope.isExibirSuperSena || (a.flg_supersena === 'S');
                }
            } else {
                configGeral.get().then(function (data) {
                    $scope.isExibir2pra500 = data.flg_2pra500;
                    $scope.isExibirBicho = data.flg_bicho;
                    $scope.isExibirSeninha = data.flg_seninha;
                    $scope.isExibirQuininha = data.flg_quininha;
                    $scope.isExibirLotinha = data.flg_lotinha;
                    $scope.isExibirRifa = data.flg_rifa;
                    $scope.isExibirSuperSena = data.flg_supersena;
                });
            }
        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }
});
