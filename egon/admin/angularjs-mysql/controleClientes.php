<?php
include "conexao.php"; 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

try {
    switch ($operacao) {
        case 'listarCompetencias':
            $query = "SELECT * FROM controle_clientes ORDER BY ano, mes";
            $result = mysqli_query($con, $query);
            $return_arr = array();
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $row_array['codigo'] = $row['CODIGO'];
                $row_array['mes'] = $row['MES'];
                $row_array['ano'] = $row['ANO'];    
                array_push($return_arr, $row_array);
            }
            echo json_encode($return_arr);
        break;

        case 'listarRegistros':
            $codCompetencia = mysqli_real_escape_string($con, $_POST['codCompetencia']);
            $comEntrada = mysqli_real_escape_string($con, $_POST['comEntrada']);
            $campoOrdenacao = mysqli_real_escape_string($con, $_POST['campoOrdenacao']);
            $ordenacao = mysqli_real_escape_string($con, $_POST['ordenacao']);

            $query = "SELECT * 
                        FROM registros_ctrl_clientes r
                            inner join site s on r.cod_site = s.cod_site
                        WHERE r.CODIGO_CTRL_CLIENTES = '$codCompetencia' 
                            AND r.COD_SITE <> 0 ";

            if ($comEntrada == 'S') {
                $query = $query . " AND r.entradas > 0 ";
            }                            
            $query = $query . " ORDER BY $campoOrdenacao $ordenacao";
            $result = mysqli_query($con, $query);
            $return_arr = array();
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $row_array['cod_site'] = $row['COD_SITE'];
                $row_array['qtd_usuarios'] = $row['QTD_USUARIOS'];
                $row_array['qtd_auditorias'] = $row['QTD_AUDITORIAS'];
                $row_array['qtd_apostas'] = $row['QTD_APOSTAS'];
                $row_array['entradas'] = $row['ENTRADAS'];
                $row_array['saidas'] = $row['SAIDAS'];
                $row_array['comissao_cambistas'] = $row['COMISSAO_CAMBISTAS'];
                $row_array['comissao_gerentes'] = $row['COMISSAO_GERENTES'];
                $row_array['saldo'] = $row['SALDO'];
                $row_array['txt_empresa'] = $row['TXT_EMPRESA'];
                $row_array['vencimento'] = $row['vencimento'];
                $row_array['mensalidade'] = $row['mensalidade'];
                array_push($return_arr, $row_array);
            }
            echo json_encode($return_arr);
        break;
    }
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = "Ocorreu o seguinte erro: " . $e->getMessage();
    
    echo json_encode($response);
}