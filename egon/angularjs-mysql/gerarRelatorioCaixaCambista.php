<?php

include "conexao.php";
require_once 'vendor/autoload.php';

if (!isset($_GET)) {
	die();
}

function getResumoDescricaoConcurso($registro)
{
	switch ($registro['tipo_jogo']) {
		case 'B':
		case '2':
			return $registro['hora_extracao'] . " - " . $registro['desc_hora'];
		case 'S':
		case 'Q':
		case 'L':
		case 'U':
			return $registro['concurso'];
		case 'R':
			return $registro['descricao'];
		default:
			return "";
	}
}

function getDescricaoTipoJogo($tipoJogoDoRegistro)
{
	switch ($tipoJogoDoRegistro) {
		case 'S':
			return "Seninha";
		case 'U':
			return "Super Sena";
		case 'Q':
			return "Quininha";
		case 'L':
			return "Lotinha";
		case 'B':
			return "Bicho";
		case '2':
			return $desc_2pra500;
		case 'R':
			return "Rifa";
		default:
			return "";
	}	
}

function getDescricaoStatusJogo($statusJogo)
{
	switch ($statusJogo) {
		case 'L':
			return "Liberado";
		case 'B':
			return "Bloqueado";
		case 'P':
			return "Processado";
		case 'F':
			return "Finalizado";	
		default:
			return "";
	}	
}

$site = mysqli_real_escape_string($con, $_GET['site']);
$perfil = mysqli_real_escape_string($con, $_GET['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$tipoJogo = mysqli_real_escape_string($con, $_GET['tipoJogo']);
$statusJogo = mysqli_real_escape_string($con, $_GET['statusJogo']);
$dataDe = mysqli_real_escape_string($con, $_GET['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_GET['dataAte']);
$codigoUsuarioGerente = mysqli_real_escape_string($con, $_GET['codigoUsuarioGerente']);
$codigoUsuarioCambista = mysqli_real_escape_string($con, $_GET['codigoUsuarioCambista']);
$finalizados = mysqli_real_escape_string($con, $_GET['finalizados']);
$relatorioSintetico = mysqli_real_escape_string($con, $_GET['relatorioSintetico']);
$campoOrdenacao = mysqli_real_escape_string($con, $_GET['campoOrdenacao']);
$ordenacao = mysqli_real_escape_string($con, $_GET['ordenacao']);
$tipoRelatorio = mysqli_real_escape_string($con, $_GET['tipoRelatorio']);

$descTipoJogo = mysqli_real_escape_string($con, $_GET['descTipoJogo']);
$descGerente = mysqli_real_escape_string($con, $_GET['descGerente']);
$descCambista = mysqli_real_escape_string($con, $_GET['descCambista']);


if($dataDe == "undefined") {
    $dataDe = 0;
}

if($dataAte == "undefined") {
    $dataAte = 0;
}

$exibeFinalizados = "SIM";
if($finalizados == 'false') {
    $exibeFinalizados = "NÃO";
}

$ehSintetico = "SIM";
if($relatorioSintetico == 'false') {
    $ehSintetico = "NÃO";
}

$response = [];

$agruparPorDadaJogo = false;
$query = "select geren.nome nome_gerente, usu.nome nome_cambista, "; 

if($relatorioSintetico == "false") {
	$query = $query."DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, ";
	$agruparPorDadaJogo = true;
} 

$query = $query . 
			  "sum(apo.valor_aposta) entradas, sum(apo.valor_ganho) saidas, 
			   sum(apo.comissao) comissao,
			   count(apo.cod_aposta) as qtd_apostas,
			   (sum(apo.valor_aposta) - sum(apo.valor_ganho) - sum(apo.comissao))  saldo_par,
		jogo.tipo_jogo,
		jogo.tp_status    
		from jogo
		inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
		inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
		inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
		inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
		left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
		where jogo.cod_site = '$site'
			and geren.cod_gerente = '$cod_usuario'
			and bil.status_bilhete != 'C'
			and apo.status != 'C' ";

if ($finalizados == 'false') {
	$query = $query . " AND (jogo.tp_status <> 'F' OR jogo.tp_status IS NULL) ";
}

if ($codigoUsuarioGerente != 'null') {
	$query = $query . " AND geren.cod_usuario = $codigoUsuarioGerente ";
}

if ($codigoUsuarioCambista != 'null') {
	$query = $query . " AND usu.cod_usuario = $codigoUsuarioCambista ";
}

if($tipoJogo) {
	$query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
}

if($statusJogo) {
	$query = $query . " AND jogo.tp_status = '$statusJogo' ";
}

if ($dataDe != '0') {
	$query = $query . " AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
}

if ($dataAte != '0') {
	$query = $query . " AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
}

if ($dataDe == 0 && $dataAte == 0) {
	$query = $query . " AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') >= DATE_FORMAT(current_date - 7,, '%Y%m%d')  
						AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') <= DATE_FORMAT(current_date, '%Y%m%d') " ;
}

if ($agruparPorDadaJogo) {
	$query = $query." GROUP BY geren.nome, usu.nome, jogo.data_jogo, jogo.tp_status order by jogo.data_jogo, geren.nome, usu.nome";
} else {
	$query = $query." GROUP BY geren.nome, usu.nome order by geren.nome, usu.nome";
}


$totalizadores = "SELECT SUM(qtd_apostas) as apostas, ".
	"SUM(comissao) as comissao, ".
	"SUM(entradas) as entradas, ".
	"SUM(saidas) as saidas, ".
	"SUM(saldo_par) as saldo_par, ".
	"COUNT(1) as 'total_registros' ".
	"FROM ( ".$query." ) t";

error_log($query, 0);
$result = mysqli_query($con, $query);
$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
   
	$row_array['nome_gerente'] = $row['nome_gerente'];
	$row_array['nome_cambista'] = $row['nome_cambista'];
	if ($agruparPorDadaJogo) {
		$row_array['data_jogo'] = $row['data_jogo'];
	}
	$row_array['tp_status'] = $row['tp_status'];
	$row_array['tipo_jogo'] = $row['tipo_jogo'];
	$row_array['entradas'] = $row['entradas'];
	$row_array['qtd_apostas'] = $row['qtd_apostas'];

	$row_array['comissao'] = $row['comissao'];
	$row_array['saidas'] = $row['saidas'];
	$row_array['saldo_par'] = $row['saldo_par'];

	array_push($return_arr, $row_array);

	if ($contador == mysqli_num_rows($result)) {
		break;
	}
};

$resultado_totalizador = mysqli_query($con, $totalizadores);
$row_totalizadores = mysqli_fetch_array($resultado_totalizador);

$data_de = date_create($dataDe);
$data_ate = date_create($dataAte); 

$html = "
<html>
	<head>
		<meta charset='utf-8'>
		<style>
			table, th, td {
				border-collapse: collapse;
				font-family: Arial, Helvetica, sans-serif;
			}
			th, td {
				border: 1px solid black;
				padding: 3px;
			}
		</style>
	</head>
<body>
	<div class='col-md-12 col-ms-12 col-xs-12'>
		<div class='table-responsive betL_CaixaList' style='padding-top: 30px'>
			<h1> CAIXA CAMBISTA </h1>			 
			<div style='display: inline;'>";

if ($tipoJogo) {
	$html = $html . 
			   "<span style='font-size: 12pt;'> <b>Tipo de Jogo:</b> " . $descTipoJogo . "</span>
				&nbsp;|&nbsp;";
}

if ($statusJogo != "") {
	$descricaoStatusJogo = getDescricaoStatusJogo($statusJogo);
	$html = $html . 
			   "<span style='font-size: 12pt;'> <b>Status Bilhete:</b> " . $descricaoStatusJogo. "</span>
				&nbsp;|&nbsp;";
}

if ($codigoUsuarioGerente != 'null') {
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>Gerente:</b> " . $descGerente . "</span>
				&nbsp;|&nbsp;";	
}

if ($codigoUsuarioCambista != 'null') {
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>Cambista:</b> " . $descCambista . "</span>
				&nbsp;|&nbsp;";
}

$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>De:</b> " . date_format($data_de, 'd-m-Y') . "</span>
				&nbsp;|&nbsp;
				<span style='font-size: 12pt;'> <b>Até:</b> " . date_format($data_ate, 'd-m-Y') . "</span>
				&nbsp;|&nbsp;
				<span style='font-size: 12pt;'> <b>Exibir Finalizados:</b> " . $exibeFinalizados . "</span>
				&nbsp;|&nbsp;
				<span style='font-size: 12pt;'> <b>Relatório Sintético:</b> " . $ehSintetico . "</span>
				&nbsp;
			</div>
			<hr>";
if (count($return_arr) == 0) {
	$html = $html . "
			<hr>
				<h4>Não há dados</h4>
			<hr>";
} else {
	$html = $html . 				 
		   "<table class='table table-hover' style='width: 100%;'>
				<thead>
					<tr style='background-color: #2E64FE; color: white;'>";
	if ($relatorioSintetico == 'false') {
		$html = $html . 	
					   "<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Data</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Status</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Tipo de Jogo</th>";
	}					
	$html = $html . 
					   "<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Gerente</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Cambista</th>
						<th width='10%' style='text-align: center; color: white; font-weight: bold;' scope='col'>Qtd. Apostas</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Entradas</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Saídas</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Comissão</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Saldo</th>
					</tr>
				</thead>
				<tbody class='container' id='conteudoTabela'>";
	$odd = 0;				
	foreach ($return_arr as $registro) {
		$odd = $odd + 1;
		if($odd % 2 == 0) {
			$html = $html . 
				   "<tr style='background-color: #E6E6E6;'>";
		} else {
			$html = $html . 
				   "<tr>";
		}
		if ($relatorioSintetico == 'false') {
			$html = $html .
				   	   "<td align='center'>" . $registro['data_jogo'] . "</td>
						<td align='center'>" . getDescricaoStatusJogo($registro['tp_status']) . "</td>
						<td align='center'>" . getDescricaoTipoJogo($registro['tipo_jogo']) . "</td>";
		}
		$html = $html .
				   	   "<td align='center'>" . $registro['nome_gerente'] . "</td>
						<td align='center'>" . $registro['nome_cambista'] . "</td>
						<td align='center'>" . $registro['qtd_apostas'] . "</td>
						<td align='right'> R$ " . number_format($registro['entradas'], 2, ',', '.') . "</td>
						<td align='right'> R$ " . number_format($registro['saidas'], 2, ',', '.') . "</td>
						<td align='right'> R$ " . number_format($registro['comissao'], 2, ',', '.') . "</td>";
		if ($registro['saldo_par'] >= 0) {
			$html = $html .						
					   "<td align='right' style='color: green'> R$ " . number_format($registro['saldo_par'], 2, ',', '.') . "</td>";
		} else {
			$html = $html .						
					   "<td align='right' style='color: red;'> R$ " . number_format($registro['saldo_par'], 2, ',', '.') . "</td>";
		}
	}
	$html = $html .
				   "</tr>
				</tbody>
				<tr style='font-weight: bold; background-color: #A9F5BC;'>
					<th scope='row'> TOTAL </th>";
	if ($relatorioSintetico == 'false') {
		$html = $html . 	
				   "<td> - </td>
				   	<td> - </td>
					<td> - </td>";
	}		
	$html = $html .
		   		   "<td> - </td>
					<td align='center' style='font-weight: bold;'> " .  $row_totalizadores['apostas'] . " </td>
					<td align='right' style='font-weight: bold;'> R$ " . number_format($row_totalizadores['entradas'], 2, ',', '.') . " </td>
					<td align='right' style='font-weight: bold;'> R$ " . number_format($row_totalizadores['saidas'], 2, ',', '.') . " </td>
					<td align='right' style='font-weight: bold;'> R$ " . number_format($row_totalizadores['comissao'], 2, ',', '.') . " </td>";
	$saldo_total = $row_totalizadores['saldo_par'];
	if ($saldo_total >= 0) {
		$html = $html . "
					<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total, 2, ',', '.') . " </td>";
	} else {
		$html = $html . "
					<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total, 2, ',', '.') . " </td>";
	}	
	$html = $html . "	
				</tr>
			</table>";	
}
$html = $html .
	   "</div>
	</div>
 </body> 
</html> ";

$hoje = date('YmdHis');

if ($tipoRelatorio == 'xlsx') {
    $arquivo = "CaixaCambista_" . $hoje . ".xls";
	header ("Expires: Mon, 18 Nov 1985 18:00:00 GMT");
	header ("Last-Modified: ".gmdate("D,d M YH:i:s")." GMT");
	header ("Cache-Control: no-cache, must-revalidate");
	header ("Pragma: no-cache");
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");	
	header ("Content-Type: text/html; charset=UTF-8");
    
    // Imprime o conteúdo da nossa tabela no arquivo que será gerado
    echo $html;    
} else if ($tipoRelatorio == 'pdf') {
    $arquivo = "CaixaCambista_" . $hoje . ".pdf";
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    $mpdf->Output($arquivo, "D");
} 

$con->close();

?>