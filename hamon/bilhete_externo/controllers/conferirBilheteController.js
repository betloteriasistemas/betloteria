angular.module('main').controller('conferirBilheteCtrl', function (bilhetesAPI, $scope, user, $routeParams, $location, $http) {
    var currentId = $routeParams.id;

    if (typeof currentId != 'undefined' || user.getSiteExterno() != 0) {
        if (typeof currentId != 'undefined') {
            auxSite = currentId;
            user.setSiteExterno(currentId);
        }

        $scope.logomarca = "img/" + currentId + ".png";

        var request = new XMLHttpRequest();
        request.open('HEAD', $scope.logomarca, false);
        request.send();
        if (request.status != 200) {
            $scope.logomarca = "img/logo_transparente.png";
        }
    } else {
        user.setSiteExterno(9999);
        $scope.logomarca = "img/logo_transparente.png";
    }

    // Caso o usuário faca um refresh na pagina, será necessário obter o schema do pin
    if (typeof currentId != 'undefined' && user.getSchema() == undefined) {
        $http({
            method: "POST",
            url: "angularjs-mysql/conexao_login.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: '&site=' + currentId + '&operacao=geral'
        }).then(function (response) {
            if (typeof currentId != 9999) {
                user.saveData(response.data);       
                localStorage.setItem('schema', response.data['schema']);
            }
        }).catch(function (response) {
            deferred.resolve(response);
            alert('Erro no $HTTP: ' + response.status)
        });
    }    

    $scope.nome = user.getNome();
    $scope.listaApostas = [];
    $scope.mapModalidade = new Map();
    $scope.tabModalidadeSelected = -1;
    $scope.previousTabModalidade = null;
    $scope.tabConcursoSelected = -1;
    $scope.previousTabConcurso = null;
    $scope.tabApostas = [];
    $scope.iniciou = false;
    $scope.exibeBilhete = false;
    $scope.data_bilhete = null;
    $scope.totalGanho = 0;
    $scope.nome_banca = "";
    $scope.totalApostado = 0;
    $scope.cambista = "";        
    $scope.nome_apostador = "";
    $scope.telefone_apostador = ""; 
    $scope.concurso = "";
    $scope.txt_pule = "";
    $scope.txt_pule_pai = "";
    $scope.status = "";
    $scope.status_bilhete = "";
    $scope.data_jogo = null;
    $scope.externo = false;
    $scope.tipo_jogo_bilhete = '';
    $scope.msgBilhete = "";
    $scope.mensagem_bilhete = "";

    $scope.linkZap = "";

    $scope.carregaConfiguracao = function(tipo_jogo){        
        var operacao = 'seninha';

        switch (tipo_jogo) {
            case 'BICHO':
                operacao = 'bicho';
            break;
            case 'QUININHA':
                operacao = 'quininha';
            break;
            case 'LOTINHA':
                operacao = 'lotinha';
            break;
            case '2 PRA 500':
                operacao = '2pra500';
            break;
            case 'RIFA':
                operacao = 'rifa';
            break;
        }

        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSiteExterno() + 
                  '&operacao=' + operacao +
                  '&schema=' + user.getSchema()
        }).then(function (response) {            
            $scope.listaCfg = response.data;                        
        });
    }

    $scope.carregaConfiguracao('seninha');
    

    $scope.conferirBilheteExterno = function () {
        if (typeof currentId != 'undefined') {
            aux = currentId;
        } else {
            var aux = user.getBilheteExterno();
        }
        const codBilheteRegex = '[A-Z0-9]{4}\-[A-Z0-9]{4}';
        if (aux != "" && aux.match(codBilheteRegex)) {
            $scope.externo = true;
            $scope.edtCodBilhete = aux;
            $scope.procurarBilhete();
            user.setBilheteExterno("");
        }
    }

    $scope.getTelefoneSemCaracteres = function (telefone) {
        var sAux = telefone;
        sAux = sAux.replace("(", "");
        sAux = sAux.replace(")", "");
        sAux = sAux.replace("-", "");
        sAux = sAux.replace(" ", "");
        return sAux.trim();
    }

    function limpaCampos() {
        $scope.exibeBilhete = false;
        $scope.data_bilhete = null;
        $scope.mensagem_bilhete = "";
        $scope.nome_banca = "";
        $scope.cambista = "";
        $scope.nome_apostador = "";
        $scope.telefone_apostador = "";
        $scope.txt_pule = "";
        $scope.txt_pule_pai = "";
        $scope.status = "";
        $scope.status_bilhete = "";
        $scope.data_jogo = null;
        $scope.totalApostado = 0;
        $scope.mapModalidade = new Map();
        $scope.tabModalidadeSelected = -1;
        $scope.tabConcursoSelected = -1;
    }

    $scope.selectTabModalidade = function(modalidade) {
        $scope.tabConcursoSelected = -1;
        $scope.previousTabConcurso = null;
        if ($scope.previousTabModalidade == $scope.tabModalidadeSelected) {
            $scope.tabModalidadeSelected = -1;
            $scope.previousTabModalidade = null;
        } else {
            $scope.previousTabModalidade = $scope.tabModalidadeSelected;
            $scope.carregaConfiguracao(modalidade);
            if ($scope.mapModalidade.get(modalidade).size == 1) {
                let concurso = $scope.mapModalidade.get(modalidade).keys().next().value;
                $scope.tabConcursoSelected = 0;
                $scope.selectTabConcurso(concurso);
            }
        }
    }

    $scope.selectTabConcurso = function(concurso) {
        if ($scope.previousTabConcurso == $scope.tabConcursoSelected) {
            $scope.tabConcursoSelected = -1;
            $scope.previousTabConcurso = null;
        } else {
            $scope.previousTabConcurso = $scope.tabConcursoSelected;
            const modalidade = Array.from($scope.mapModalidade.keys())[$scope.tabModalidadeSelected];
            extrairInformacoesApostas(modalidade, concurso);
        }
    }

    $scope.deselectAll = function() {
        $scope.tabModalidadeSelected = -1;
        $scope.tabConcursoSelected = -1;
    }

    $scope.getQtdApostasCanceladasPorModalidade = function(concursos) {
        var canceladas = [];
        concursos.forEach((bilhetes, concurso) => {
            bilhetes.forEach(bilhete => {
                canceladas = canceladas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Cancelado";
                }));
            });
        });
        return canceladas.length;
    }

    $scope.getQtdApostasCanceladasPorConcurso = function(bilhetes) {
        var canceladas = [];
        bilhetes.forEach(bilhete => {
            canceladas = canceladas.concat(bilhete.apostas.filter(element => {
                return element.status == "Cancelado";
            }));
        });
        return canceladas.length;
    }

    $scope.getQtdApostasAbertasPorModalidade = function(concursos) {
        var abertas = [];
        concursos.forEach((bilhetes, concurso) => {
            bilhetes.forEach(bilhete => {
                abertas = abertas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Aberto";
                }));
            });
        });
        return abertas.length;
    }

    $scope.getQtdApostasAbertasPorConcurso = function(bilhetes) {
        var abertas = [];
        bilhetes.forEach(bilhete => {
            abertas = abertas.concat(bilhete.apostas.filter(element => {
                return element.status == "Aberto";
            }));
        });
        return abertas.length;
    }

    $scope.getQtdApostasPerdidasPorModalidade = function(concursos) {
        var perdidas = [];
        concursos.forEach((bilhetes, concurso) => {
            bilhetes.forEach(bilhete => {
                perdidas = perdidas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Perdido";
                }));
            });
        });
        return perdidas.length;
    }

    $scope.getQtdApostasPerdidasPorConcurso = function(bilhetes) {
        var perdidas = [];
        bilhetes.forEach(bilhete => {
            perdidas = perdidas.concat(bilhete.apostas.filter(element => {
                return element.status == "Perdido";
            }));
        });
        return perdidas.length;
    }

    $scope.getQtdApostasGanhasPorModalidade = function(concursos) {
        var ganhas = [];
        concursos.forEach((bilhetes, concurso) => {
            bilhetes.forEach(bilhete => {
                ganhas = ganhas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Ganho";
                }));
            });
        });
        if (ganhas.length > 0) {
            return ganhas.length + "(R$ " + ganhas.reduce(function (total, aposta) 
            { return total + parseFloat(aposta.valor_ganho); }, 0) + ")";
        } 
        return ganhas.length;
    }

    $scope.getQtdApostasGanhasPorConcurso = function(bilhetes) {
        var ganhas = [];
        bilhetes.forEach(bilhete => {
            ganhas = ganhas.concat(bilhete.apostas.filter(element => {
                return element.status == "Ganho";
            }));
        })
        if (ganhas.length > 0) {
            return ganhas.length + "(R$ " + ganhas.reduce(function (total, aposta) 
            { return total + parseFloat(aposta.valor_ganho); }, 0) + ")";
        } 
        return ganhas.length;
    }

    $scope.getQtdApostasPorModalidade = function(concursos) {
        var qtd = 0;
        concursos.forEach((bilhetes, concurso) => {
            bilhetes.forEach(bilhete => {
                qtd += bilhete.apostas.length;
            })
        });
        return qtd;
    }

    $scope.getQtdApostasPorConcurso = function(bilhetes) {
        var qtd = 0;
        bilhetes.forEach(bilhete => {
            qtd += bilhete.apostas.length;
        });
        return qtd;
    }

    $scope.getConcursos = function() {
        const modalidade = Array.from($scope.mapModalidade.keys())[$scope.tabModalidadeSelected];
        return $scope.mapModalidade.get(modalidade);
    }

    $scope.abrirBilheteResumido = function(txt_pule) {
        var printer = "com.mitsoftwar.appprinter://" + $location.absUrl() + 
        "/angularjs-mysql/bilheteResumidoCodificado.php?id=" + txt_pule + "&schema=" + user.getSchema();
        printer = printer.replace('#!', '');
        window.open(printer);
    }

    $scope.procurarBilhete = function () {
        limpaCampos();
        $scope.iniciou = true;
        var promessa = bilhetesAPI.getBilhetes($scope.edtCodBilhete, $scope.edtNumeroBilhete);
        promessa.then(function (result) {
            $scope.listaApostas = result;
            $scope.exibeBilhete = $scope.listaApostas.length > 0;
            if (!$scope.exibeBilhete) {
                limpaCampos();
                $.alert('Bilhete não encontrado!');
            }
            for (i = 0; i < $scope.listaApostas.length; i++) {
                const aposta = $scope.listaApostas[i];
                const tipo_jogo = aposta.tipo_jogo_dinamico || aposta.tipo_jogo;
                if ($scope.mapModalidade.get(tipo_jogo) == null) {
                    $scope.mapModalidade.set(tipo_jogo, new Map());
                }
                if ($scope.mapModalidade.get(tipo_jogo).get(aposta.concurso) == null) {
                    $scope.mapModalidade.get(tipo_jogo).set(aposta.concurso, []);
                }
                $scope.mapModalidade.get(tipo_jogo).get(aposta.concurso).push(aposta);
                $scope.totalApostado += parseFloat(aposta.valor_bilhete);
            }
            
            if ($scope.exibeBilhete) {
                if ($scope.mapModalidade.size > 1) {
                    $scope.tipo_jogo_bilhete = 'MULTI';
                } else {
                    $scope.tipo_jogo_bilhete = $scope.listaApostas[0].tipo_jogo;
                    $scope.tabModalidadeSelected = 0;
                    $scope.selectTabModalidade($scope.tipo_jogo_bilhete);
                }
                extrairInformacoesBilhete();
            }
        });
    };

    extrairInformacoesBilhete = function () {
        const bilhete = $scope.listaApostas[0];
        $scope.nome_banca = bilhete.nome_banca;
        $scope.data_bilhete = bilhete.data_bilhete;
        $scope.data_bilhete_js = bilhete.data_bilhete_js;
        $scope.cambista = bilhete.cambista;
        $scope.nome_apostador = bilhete.nome_apostador;
        $scope.telefone_apostador = bilhete.telefone_apostador;
        $scope.txt_pule = bilhete.txt_pule;
        $scope.txt_pule_pai = bilhete.txt_pule_pai;
        $scope.status_bilhete = bilhete.status_bilhete;
    }

    extrairInformacoesApostas = function(modalidade, concurso) {
        const bilhetes = $scope.mapModalidade.get(modalidade).get(concurso);
        const jogo = bilhetes[0];
        $scope.concurso = jogo.concurso;
        $scope.data_jogo = jogo.data_jogo;
        $scope.numeros_sorteados = jogo.numeros_sorteados;
        $scope.tipo_jogo = jogo.tipo_jogo;
        $scope.tipo_jogo_dinamico = jogo.tipo_jogo_dinamico;
        $scope.txt_pule_aposta = jogo.numero_bilhete;
        $scope.status_aposta = jogo.status_bilhete;
        $scope.tabApostas = [];
        $scope.valor_bilhete = 0;
        bilhetes.forEach(bilhete => {
            $scope.tabApostas = $scope.tabApostas.concat(bilhete.apostas);
            $scope.valor_bilhete += parseFloat(bilhete.valor_bilhete);
        });
        $scope.totalGanho = 0;
        for (i = 0; i < $scope.tabApostas.length; i++) {
            var aposta = $scope.tabApostas[i];
            if (aposta.status == 'Ganho') {
                $scope.totalGanho = $scope.totalGanho + parseFloat(aposta.valor_ganho, 0);
            }
        }
    }

    $scope.getTelefoneSemCaracteres = function (telefone) {
        var sAux = telefone;
        sAux = sAux.replace("(", "");
        sAux = sAux.replace(")", "");
        sAux = sAux.replace("-", "");
        sAux = sAux.replace(" ", "");
        return sAux.trim();
    }

    $scope.getLinkMobile = function (txt_pule) {
        //return "com.mitsoftwar.appprinter://http://www.betloteria.com/angularjs-mysql/bilheteCodificado.php?id=" + txt_pule;
        var strAux = "com.mitsoftwar.appprinter://" + $location.absUrl();                

        var n = strAux.indexOf("#!");
        
        strAux = strAux.substr(0, n);                

        strAux = strAux + "angularjs-mysql/bilheteCodificado.php?id=" + txt_pule + "&schema=" + user.getSchema();
                
        return strAux; 

        
    }

    $scope.getDescricaoTipoJogo = function(tipo_jogo_bicho){
        switch(tipo_jogo_bicho) {
            case 'MS':
                return "MS - MILHAR SECA";
            case 'MC':
                return "MC - MILHAR COM CENTENA";
            case 'MI':
                return "MI - MILHAR INVERTIDA";
            case 'MCI':
                return "MCI - MILHAR COM CENTENA INVERTIDA";
            case 'C':
                return "C - CENTENA";
            case 'CI':
                return "CI - CENTENA INVERTIDA";
            case 'G':
                return "G - GRUPO";
            case 'DG':
                return "DG - DUQUE DE GRUPO 1 / 5";
            case 'DG6':
                return "DG6 - DUQUE DE GRUPO 6 / 10";
            case 'DGC':
                return "DGC - DUQUE DE GRUPO COMBINADO";			
            case 'TG':
                return "TG - TERNO DE GRUPO 1 / 5";
            case 'TG10':
                return "TG10 - TERNO DE GRUPO 1 / 10";
            case 'TG6':
                return "TG6 - TERNO DE GRUPO 6 / 10";
            case 'TSE':
                return "TSE - TERNO DE SEQUÊNCIA";
            case 'TSO':
                return "TSO - TERNO DE SOMA";
            case 'TEX-A':
                return "TEX-A - TERNO ESPECIAL ABERTO";
            case 'TEX-F':
                return "TEX-F - TERNO ESPECIAL FECHADO";
            case 'QG':
                return "QG - QUINA DE GRUPO";
            case 'DZ':
                return "DZ - DEZENA";
            case 'DDZ':
                return "DDZ - DUQUE DE DEZENA 1 / 5";
            case 'DDZ6':
                return "DDZ6 - DUQUE DE DEZENA 6 / 10";
            case 'DDZC':
                return "DDZC - DUQUE DE DEZENA COMBINADO";
            case 'TDZ':
                return "TDZ - TERNO DE DEZENA";
            case 'TDZC':
                return "TDZC - TERNO DE DEZENA COMBINADO";
            case 'TDZ10':
                return "TDZ10 - TERNO DE DEZENA 1 / 10";
            case 'TDZ6':
                return "TDZ6 - TERNO DE DEZENA 6 / 10";
            case 'TGC':
                return "TGC - TERNO DE GRUPO COMBINADO";
            case 'PS':
                return "PS - PASSE SECO";
            case 'PS12':
                return "PS12 - PASSE SECO 1 / 2";
            case 'PC':
                return "PC - PASSE COMBINADO";
            default:
                return tipo_jogo_bicho;
        }
    } 

    $scope.abrir = function (txt_pule) {
        var linka = $scope.getLinkMobile(txt_pule);
        window.open(linka);
    }

});