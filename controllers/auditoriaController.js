angular.module('main').controller('auditoriaCtrl', function ($rootScope, $scope, user, $http, $location, $filter) {
    $rootScope.loading = false;
    $scope.perfil = user.getPerfil();

    /** FILTROS */
    $scope.comboOperacoes = "";
    $scope.comboUsuarios = null;
    $scope.edtDataDe = new Date();
    $scope.edtDataAte = new Date();

    /** PAGINAÇÃO */
    $scope.totalItens = 0;
    $scope.index = 0;
    $scope.itensPorPagina = 20;

    /** ORDENAÇÃO */
    $scope.sort = "DATA";
    $scope.sortType = false;

    $scope.paginacao = {
        atual: 1
    };

    $scope.consulta = false;
    $scope.records = [];

    $scope.alterarPagina = function (newPage) {
        $scope.index = (newPage - 1) * $scope.itensPorPagina;
        $scope.paginacao.atual = newPage;
        $scope.pesquisarLogsAuditoria();
    };

    function scrollParaTabela() {
        $('html, body').animate({ scrollTop: $('#dir-paginator-top').offset().top - 25 }, 'slow');
    }

    $scope.getUsuarios = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/usuarios_site.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: '&site=' + user.getSite() +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.usuarios = response.data;
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.getOperacoes = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/auditoria_operacao.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'schema=' + user.getSchema()
        }).then(function (response) {
            $scope.operacoes = response.data;
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.sortTable = function (column) {
        if ($scope.sort == column) {
            $scope.sortType = !$scope.sortType;
        } else {
            $scope.sortType = true;
        }
        $scope.sort = column;
        this.alterarPagina(1);
    }

    $scope.pesquisarLogsAuditoria = function () {
        if (!validarData($scope.edtDataDe, $scope.edtDataAte)) {
            $.alert("'Período De' maior que 'Período Até'");
            return;
        }
        
        $scope.records = [];
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/auditoria_listar.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: '&site=' + user.getSite()
                + '&index=' + $scope.index
                + '&qtdElementos=' + $scope.itensPorPagina
                + '&sort=' + $scope.sort
                + '&sortType=' + $scope.sortType
                + '&usuario=' + ($scope.comboUsuarios || '')
                + '&operacao=' + $scope.comboOperacoes
                + '&dataDe=' + ((typeof $scope.edtDataDe === 'undefined' || $scope.edtDataDe == null) ?
                    '0' : $filter('date')($scope.edtDataDe, "yyyy-MM-dd"))
                + '&dataAte=' + ((typeof $scope.edtDataAte === 'undefined' || $scope.edtDataAte == null) ?
                    '0' : $filter('date')($scope.edtDataAte, "yyyy-MM-dd")) 
                + '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data.filter(obj => obj.totalItens == null);
            $scope.totalItens = response.data.filter(obj => obj.totalItens != null)[0].totalItens;
            $scope.consulta = true;
            scrollParaTabela();
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status);
        }).finally(function () {
            $rootScope.loading = false;
            if ($scope.records) {
                scrollParaTabela();
            }
        });
    }

    $scope.getPerfilUsuario = function (perfil) {
        if (perfil == "C") {
            return "Cambista";
        } else if (perfil == "G") {
            return "Gerente";
        } else if (perfil == "A") {
            return "Admin";
        }
    }

    function validarData(dataDe, dataAte) {
        return dataDe && dataAte && dataAte >= dataDe;
    }

    $scope.export = function (cod_auditoria) {
        window.open('angularjs-mysql/htmlToPdf.php?cod_auditoria=' + cod_auditoria + '&schema=' + user.getSchema());
    }
});