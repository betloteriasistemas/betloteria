<?php

include "conexao.php";

require_once('../../angularjs-mysql/auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];
$auditoria = "";

try {
    $con->begin_transaction();
    $query = 'SET @@session.time_zone = "-03:00"';

    $result = mysqli_query($con, $query);
    $apostas = json_decode($_POST['apostas']);

    // Agrupa as apostas por concurso
    $jogos_apostas = array();

    foreach ($apostas as $aposta) {

        if ($aposta->valorReal < 0) {
            throw new Exception('Não é permitido apostas com valor inferior a zero!');
        }

        if ($aposta->cod_jogo == "null") {
            throw new Exception('Existe aposta sem concurso associado! É necessário removê-la!');
        }

        if (array_key_exists($aposta->cod_jogo, $jogos_apostas)) {
            array_push($jogos_apostas[$aposta->cod_jogo], $aposta);
        } else {
            $jogos_apostas[$aposta->cod_jogo] = array($aposta);
        }
    }

    $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
    $pule = "";
    do {
        $pule = geraPule($cod_site);
        $query = "SELECT txt_pule 
                    FROM bilhete
                    WHERE txt_pule = '$pule' OR txt_pule_pai = '$pule'";

        $result = mysqli_query($con, $query);
    } while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC));

    $codBilhetesGerados = "";
    foreach ($jogos_apostas as $jogo_aposta) {          
        
        $query = "SELECT MIN(COD_USUARIO) AS COD_USUARIO FROM USUARIO 
                    WHERE PERFIL = 'A' 
                        AND STATUS = 'A' 
                        AND COD_SITE = '$cod_site'";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $cod_usuario =$row['COD_USUARIO'];
        $nome_usuario = mysqli_real_escape_string($con, $_POST['nome_usuario']);
        $nome_apostador = mysqli_real_escape_string($con, $_POST['nome']);
        $telefone_apostador = mysqli_real_escape_string($con, $_POST['telefone']);
        $data_atual = mysqli_real_escape_string($con, $_POST['data_atual']);   
    
        $stmt = $con->prepare("insert into bilhete(cod_site, cod_usuario, nome_apostador, telefone_apostador, data_bilhete, txt_pule_pai, status_bilhete)
                                VALUES (?, ?, ?, ?, ?, ?, 'V') ");
        $stmt->bind_param("iissss", $cod_site, $cod_usuario, $nome_apostador, $telefone_apostador, $data_atual, $pule);
    
        $stmt->execute();
        $codBilhete = $con->insert_id;

        $stmt = $con->prepare(" UPDATE bilhete 
                                    SET numero_bilhete = ?,
                                        txt_pule = ?
                                WHERE cod_bilhete = ? ");
        $numero_bilhete = $cod_site . $codBilhete;
        $stmt->bind_param("isi", $numero_bilhete, $numero_bilhete, $codBilhete);
        $stmt->execute();        
    
        $auditoria = descreverCriacaoBilhete(
            $nome_apostador,
            $telefone_apostador,
            $data_atual,
            $pule,
            $cod_site . $codBilhete,
            null
        );
        
        if ($codBilhetesGerados == "") {
            $codBilhetesGerados = $cod_site . $codBilhete;
        } else {
            $codBilhetesGerados = $codBilhetesGerados . " - " . $cod_site . $codBilhete;
        }

        foreach ($jogo_aposta as $aposta) {
    
            $cod_jogo = $aposta->cod_jogo;
            error_log('COD_JOGO: ' . $cod_jogo);

            $query = "SELECT TP_STATUS, HORA_EXTRACAO, DATA_JOGO, TIPO_JOGO,
                    CONCURSO AS COD_EXTRACAO, CURRENT_TIME AGORA, current_date HOJE 
                    FROM jogo
                    WHERE cod_jogo = '$cod_jogo'";
            $result = mysqli_query($con, $query);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if ($row['TP_STATUS'] == 'B') {
                throw new Exception('Concurso Bloqueado! Atualize a página e selecione um concurso válido!');
            }

            $tipo_jogo = $row['TIPO_JOGO'];

            switch ($tipo_jogo) {
                case 'S':
                case 'Q':
                case 'L':
                case 'U':
                    $query = "select (data_jogo < current_date) data_passou,
                             (data_jogo <= current_date) data_retorno,
                             (CURRENT_TIME >= '19:50') hora_retorno
                      from jogo
                      where cod_jogo = '$cod_jogo'";
                break;
                case 'R':
                    $query = "select (data_jogo < current_date) data_passou,
                             0 data_retorno,
                             0 hora_retorno
                      from jogo
                      where cod_jogo = '$cod_jogo'";
                break;
                case 'B':
                    $query = "select (j.data_jogo < current_date) data_passou,
                                    (j.data_jogo <= current_date) data_retorno,
                                    ((CURRENT_TIME + INTERVAL e.prazo_bloqueio MINUTE) >= j.hora_extracao) hora_retorno
                                from jogo j
                                    inner join extracao_bicho e on (j.concurso = e.cod_extracao)
                                where cod_jogo = '$cod_jogo'";
                break;                        
            }

            $result = mysqli_query($con, $query);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
                throw new Exception('Aposta não pode ser feita para esse concurso! Selecione o próximo concurso!');
            }

            $constanteSim = mysqli_real_escape_string($con, 'S');
            $sNumeros = str_replace('.', '', $aposta->numeros);
            if ($tipo_jogo == 'B') {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, txt_aposta, valor_aposta, possivel_retorno, do_premio, ao_premio, tipo_jogo, inversoes, comissao )
                values(?,?,?,?,?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiisddsssid", $cod_site, $cod_jogo, $codBilhete, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoReal, $aposta->premioDoReal, $aposta->premioAoReal, $aposta->tipoJogo, $aposta->inversoes, $comissao);
                $stmt->execute();
                $auditoria = $auditoria . descreverApostaBicho($sNumeros, $aposta->valorReal, $aposta->possivelRetornoReal, $aposta->premioDoReal, $aposta->premioAoReal, $aposta->tipoJogo, $aposta->inversoes, null);
            }
            else if ($tipo_jogo == 'L') {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno )
                values(?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiiisdd", $cod_site, $cod_jogo, $codBilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoReal);
                $stmt->execute();
                $auditoria = $auditoria . descreverApostaLotinha($aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoReal, null);
            } else if ($tipo_jogo == 'U') {                
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno5, retorno4)
                    values(?,?,?,?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiiisddsdd", $cod_site, $cod_jogo, $codBilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoSenaReal, $constanteSim, $aposta->possivelRetornoQuinaReal, $aposta->possivelRetornoQuadraReal);
                $stmt->execute();
                $auditoria = $auditoria . descreverApostaSuperSena($aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoSenaReal, $aposta->possivelRetornoQuinaReal, $aposta->possivelRetornoQuadraReal, null);                 
            } else {
                if ($tipo_jogo == 'S' && $aposta->habilitaSorte == 'S') {
                    $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno5, retorno4 )
                    values(?,?,?,?,?,?,?,?,?,?) ");
                    $stmt->bind_param("iiiisddsdd", $cod_site, $cod_jogo, $codBilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoSenaReal, $constanteSim, $aposta->possivelRetornoQuinaReal, $aposta->possivelRetornoQuadraReal);
                    $stmt->execute();
                    $auditoria = $auditoria . descreverApostaSeninhaSorte($aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoSenaReal, $constanteSim, $aposta->possivelRetornoQuinaReal, $aposta->possivelRetornoQuadraReal, null);
                } else if ($tipo_jogo == 'Q' && $aposta->habilitaSorte == 'S') {
                    $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno4, retorno3 )
                    values(?,?,?,?,?,?,?,?,?,?) ");
                    $stmt->bind_param("iiiisddsdd", $cod_site, $cod_jogo, $codBilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoQuinaReal, $constanteSim, $aposta->possivelRetornoQuadraReal, $aposta->possivelRetornoTernoReal);
                    $stmt->execute();
                    $auditoria = $auditoria . descreverApostaQuininhaSorte($aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoQuinaReal, $constanteSim, $aposta->possivelRetornoQuadraReal, $aposta->possivelRetornoTernoReal, null);
                } else {
                    $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno )
                    values(?,?,?,?,?,?,?) ");
                    $stmt->bind_param("iiiisdd", $cod_site, $cod_jogo, $codBilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoReal);
                    $stmt->execute();
                    $auditoria = $auditoria . descreverApostaQuininhaSeninha($tipo_jogo, $aposta->qtd, $sNumeros, $aposta->valorReal, $aposta->possivelRetornoReal, null);
                }
            }
            
        }
    }

    inserir_auditoria(
        $con,
        $cod_usuario,
        $cod_site,
        AUD_BILHETE_CRIADO,
        $auditoria
    );

    $con->commit();
    $con->close();

    $response['numero_bilhete'] = $codBilhetesGerados;
    $response['pule'] = $pule;
    $response['status'] = "OK";
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

function geraPule($cod_site) {
    $prefixo = strtoupper(dechex($cod_site));
    if (strlen($prefixo) < 4) {
        $prefixo = str_pad($prefixo , 4 , '0' , STR_PAD_LEFT);
    }
    $aleatorio = $prefixo . "-";
    
    $letras = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
    $indice = rand(0, strlen($letras) - 1);

    $tamanho = 4;
    for ($i = 0; $i < $tamanho; $i++) {
        $indice = rand(0, strlen($letras) - 1);
        $aleatorio = $aleatorio . substr($letras, $indice, 1);
    }
    return $aleatorio;
}