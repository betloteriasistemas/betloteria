angular.module('main').controller('dashboardCtrl', function ($scope, user, configGeral, $location) {

    $scope.perfil = user.getPerfil();
    $scope.flg_seninha = false;
    $scope.flg_quininha = false;
    $scope.flg_lotinha = false;
    $scope.flg_bicho = false;
    $scope.flg_2pra500 = false;
    $scope.flg_rifa = false;
    $scope.flg_multijogos = false;
    $scope.flg_supersena = false;
    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.flg_seninha = user.getflg_seninha() == 'S' && data.flg_seninha;
        $scope.flg_quininha = user.getflg_quininha() == 'S' && data.flg_quininha;
        $scope.flg_lotinha = user.getflg_lotinha() == 'S' && data.flg_lotinha;
        $scope.flg_bicho = user.getflg_bicho() == 'S' && data.flg_bicho;
        $scope.flg_2pra500 = user.getflg_2pra500() == 'S' && data.flg_2pra500;
        $scope.flg_rifa = user.getflg_rifa() == 'S' && data.flg_rifa;
        $scope.flg_multijogos = user.getflg_multijogos() == 'S' && data.flg_multijogos;
        $scope.flg_supersena = user.getflg_supersena() == 'S' && data.flg_supersena;
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
    });



    $scope.reiniciaRepeticaoDeBilhete = function (url) {
        user.setRepetirBilhete(false);
        $location.path("/" + url);
    }

});