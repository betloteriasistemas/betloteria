angular.module('main').controller('limiteBichoCtrl', function ($scope, $http, user, $route, notificacoes) {
    notificacoes.get().then(function (data) {
        $scope.notificacoes = data;
    });
    
    parseValorMonetario = function (valor) {
        return valor ? parseFloat(valor).toFixed(2).replace('.', ',').replace(/\d(?=(\d{3})+\,)/g, '$&.') : 0;
    }

    unparseValorMonetario = function (valor) {
        return valor ? valor.replace(/\./g, '').replace(',', '.') : 0;
    }

    $scope.listarConfig = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/limiteBicho.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&operacao=listar' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;
        }, function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.listarExtracoes = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/extracoes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&operacao=listar&tipo_jogo=B' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.extracoes = response.data;
        }, function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }


    $scope.excluir = function (codigo) {
        $.confirm({
            title: '',
            content:"Confirma a exclusão?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/limiteBicho.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() + 
                              '&site=' + user.getSite() +
                              '&codigo=' + codigo + 
                              '&operacao=excluir' +
                              '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    }

    $scope.alterar = function (config) {
        $scope.codigo = config.codigo;
        $("#comboExtracao").val(config.cod_extracao);
        $scope.edtMilhar = parseValorMonetario(config.milhar);
        $scope.edtCentena = parseValorMonetario(config.centena);
        $scope.edtGrupo = parseValorMonetario(config.grupo);
        $scope.edtDuqueGrupo = parseValorMonetario(config.duque_grupo);
        $scope.edtTernoGrupo = parseValorMonetario(config.terno_grupo);
        $scope.edtQuinaGrupo = parseValorMonetario(config.quina_grupo);
        $scope.edtDezena = parseValorMonetario(config.dezena);
        $scope.edtDuqueDezena = parseValorMonetario(config.duque_dezena);
        $scope.edtTernoDezena = parseValorMonetario(config.terno_dezena);
    }

    $scope.salvar = function (codigo) {
        if (typeof codigo === "undefined") {
            codigo = 0;
        }

        if (codigo === 0 && typeof $scope.comboExtracao === "undefined") {
            $.alert("Informe a EXTRAÇÃO!");
            return;
        }
        var cod_extracao = $scope.comboExtracao;
        var desc_extracao = "";
        if (cod_extracao) {
            desc_extracao = $("#comboExtracao")[0].selectedOptions[0].label
        }
        var milhar = unparseValorMonetario($scope.edtMilhar);
        var centena = unparseValorMonetario($scope.edtCentena);
        var grupo = unparseValorMonetario($scope.edtGrupo);
        var duque_grupo = unparseValorMonetario($scope.edtDuqueGrupo);
        var terno_grupo = unparseValorMonetario($scope.edtTernoGrupo);
        var quina_grupo = unparseValorMonetario($scope.edtQuinaGrupo);
        var dezena = unparseValorMonetario($scope.edtDezena);
        var duque_dezena = unparseValorMonetario($scope.edtDuqueDezena);
        var terno_dezena = unparseValorMonetario($scope.edtTernoDezena);

        $http({
            url: 'angularjs-mysql/limiteBicho.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'milhar=' + milhar + '&centena=' + centena + '&grupo=' + grupo + '&duque_grupo=' + duque_grupo +
                '&terno_grupo=' + terno_grupo + '&quina_grupo=' + quina_grupo + '&dezena=' + dezena + '&duque_dezena=' + duque_dezena +
                '&terno_dezena=' + terno_dezena + '&codigo=' + codigo +
                '&site=' + user.getSite() + '&cod_usuario=' + user.getCodigo() +
                '&cod_extracao=' + cod_extracao + '&desc_extracao=' + desc_extracao +
                '&operacao=salvar' + '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                notificacoes.registerUpdateCallback();
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
        })
    }

});