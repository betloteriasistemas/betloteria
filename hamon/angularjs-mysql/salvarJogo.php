<?php

include "conexao.php";

require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

function retornaArrayMilharInvertida($milhar)
{
    $vetor = [];
    for ($m = 0; $m < 4; $m++) {

        $valorMilhar = substr($milhar, $m, 1);

        for ($c = 0; $c < 4; $c++) {

            if ($c == $m) {
                continue;
            } else {
                $valorCentena = substr($milhar, $c, 1);
            }

            for ($d = 0; $d < 4; $d++) {

                if ($d == $c || $d == $m) {
                    continue;
                } else {
                    $valorDezena = substr($milhar, $d, 1);
                }

                for ($u = 0; $u < 4; $u++) {

                    if ($u == $d || $u == $c || $u == $m) {
                        continue;
                    } else {
                        $valor = $valorMilhar . $valorCentena . $valorDezena . substr($milhar, $u, 1);
                    }

                    if (!in_array($valor, $vetor)) {
                        array_push($vetor, $valor);
                    }
                }
            }
        }
    }

    return $vetor;
}

function retornaBicho($arrayBicho, $valor)
{
    foreach ($arrayBicho as $bichoAux) {
        if (
            $bichoAux['numero_1'] == $valor ||
            $bichoAux['numero_2'] == $valor ||
            $bichoAux['numero_3'] == $valor ||
            $bichoAux['numero_4'] == $valor
        ) {
            return $bichoAux['codigo'];
        }
    }
    return 0;
}

function salvarJogo($con, $codigo, $cod_usuario, $cod_site)
{
    $auditoria = "";
    $queryOldJogo = "SELECT * FROM jogo WHERE COD_JOGO = ?";
    $stmtOldJogo = null;
    $oldJogo = null;

    $query = "";
    if ($codigo == 0) {
        $concurso = mysqli_real_escape_string($con, $_POST['concurso']);
        $tipo_jogo = mysqli_real_escape_string($con, $_POST['tipo_jogo']);
        $hora_extracao = mysqli_real_escape_string($con, $_POST['hora_extracao']);
        $desc_hora = mysqli_real_escape_string($con, $_POST['desc_hora']);
        $data = mysqli_real_escape_string($con, $_POST['data']);
        $descricao = mysqli_real_escape_string($con, $_POST['descricao']);
        $valor = mysqli_real_escape_string($con, $_POST['valor']);

        switch ($tipo_jogo) {
            case 'S':
            case 'Q':
            case 'L':
            case 'U':
                $query = " SELECT count(*) qtd FROM jogo
                            WHERE COD_SITE = '$cod_site'
                            and concurso = '$concurso'
                            and tipo_jogo = '$tipo_jogo' ";
            break;
            case '2':
            case 'B':
                $query = " SELECT count(*) qtd FROM jogo
                            WHERE COD_SITE = '$cod_site'
                            and data_jogo = '$data'
                            and tipo_jogo = '$tipo_jogo' 
                            and hora_extracao = '$hora_extracao' and desc_hora = '$desc_hora'";
            break;
            case 'R':
                $query = " SELECT count(*) qtd FROM jogo
                            WHERE COD_SITE = '$cod_site'
                            and data_jogo = '$data'
                            and tipo_jogo = '$tipo_jogo'
                            and descricao = '$descricao'";
            break;
        }
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        if ($row['qtd'] > 0) {
            throw new Exception("Concurso já existe!");
        }

        switch ($tipo_jogo) {
            case 'B':
            case '2':
                $stmt = $con->prepare("INSERT INTO jogo(COD_USUARIO, DATA_JOGO, COD_SITE, TIPO_JOGO, HORA_EXTRACAO, DESC_HORA, CONCURSO, TP_STATUS)
							VALUES (?, ?, ?, ?, ?, ?, ?, 'L') ");
                $stmt->bind_param("isisssi", $cod_usuario, $data, $cod_site, $tipo_jogo, $hora_extracao, $desc_hora, $concurso);
                $auditoria = descreverCriacaoJogo($data, $tipo_jogo, null, $hora_extracao, $desc_hora, null, null);
            break;
            case 'S':
            case 'Q':
            case 'L':
            case 'U':
                $stmt = $con->prepare("INSERT INTO jogo(COD_USUARIO, DATA_JOGO, CONCURSO, COD_SITE, TIPO_JOGO, TP_STATUS)
                    VALUES (?, ?, ?, ?, ?, 'L') ");
                $stmt->bind_param("isiis", $cod_usuario, $data, $concurso, $cod_site, $tipo_jogo);
                $concurso = mysqli_real_escape_string($con, $_POST['concurso']);
                $auditoria = descreverCriacaoJogo($data, $tipo_jogo, $concurso, null, null, null, null);
            break;
            case 'R':
                $stmt = $con->prepare("INSERT INTO jogo(COD_USUARIO, DATA_JOGO, CONCURSO, COD_SITE, TIPO_JOGO, TP_STATUS, DESCRICAO, VALOR)
							VALUES (?, ?, ?, ?, ?, 'L', ?, ?) ");
                $stmt->bind_param("isiissd", $cod_usuario, $data, $concurso, $cod_site, $tipo_jogo, $descricao, $valor);            
                $auditoria = descreverCriacaoJogo($data, $tipo_jogo, $concurso, null, null, $descricao, $valor);
            break;
            default:
                throw new Exception("Erro ao tentar salvar jogo!");
        }
    } else {
        $cod_jogo = $codigo;
        $data = mysqli_real_escape_string($con, $_POST['data']);
        $numero1 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero1']), null);
        $numero2 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero2']), null);
        $numero3 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero3']), null);
        $numero4 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero4']), null);
        $numero5 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero5']), null);
        $numero6 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero6']), null);
        $numero7 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero7']), null);
        $numero8 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero8']), null);
        $numero9 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero9']), null);
        $numero10 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero10']), null);
        $numero11 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero11']), null);
        $numero12 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero12']), null);
        $numero13 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero13']), null);
        $numero14 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero14']), null);
        $numero15 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero15']), null);

        $status = "B";
        if ($numero1 == null) {
            $status = "L";
        }

        $stmt = $con->prepare("UPDATE jogo SET DATA_JOGO = ?, TP_STATUS = ?, NUMERO_1 = ?, NUMERO_2 = ?,NUMERO_3 = ?,NUMERO_4 = ?,NUMERO_5 = ?,NUMERO_6 = ?,NUMERO_7 = ?,
            NUMERO_8 = ?,NUMERO_9 = ?,NUMERO_10 = ?, NUMERO_11 = ?, NUMERO_12 = ?, NUMERO_13 = ?, NUMERO_14 = ?, NUMERO_15 = ?
		                       WHERE COD_JOGO = ? ");
        $stmt->bind_param("ssiiiiiiiiiiiiiiii", 
                        $data, $status, $numero1, $numero2, $numero3, $numero4, $numero5, 
                        $numero6, $numero7, $numero8, $numero9, $numero10, 
                        $numero11, $numero12, $numero13, $numero14, $numero15, $cod_jogo);

        $stmtOldJogo = $con->prepare($queryOldJogo);
        $stmtOldJogo->bind_param("i", $cod_jogo);
        $stmtOldJogo->execute();
        $result = $stmtOldJogo->get_result();
        $oldJogo = $result->fetch_assoc();
        $stmtOldJogo->close();

        $auditoria = descreverEdicaoJogo(
            $oldJogo, $cod_jogo, $data, $status,
            $numero1, $numero2, $numero3, $numero4, $numero5,
            $numero6, $numero7, $numero8, $numero9, $numero10,
            $numero11,$numero12,$numero13,$numero14,$numero15
        );
    }

    $stmt->execute();
    $stmt->close();

    inserir_auditoria(
        $con,
        $cod_usuario,
        $cod_site,
        $codigo == 0 ? AUD_JOGO_CRIADO : AUD_JOGO_EDITADO,
        $auditoria
    );
}

function processarResultado($con, $codigo, $cod_usuario, $cod_site)
{
    $resultado = [];
    $query = " select cod_site, tipo_jogo, concurso,
                      numero_1, numero_2, numero_3, numero_4, numero_5, 
                      numero_6, numero_7, numero_8, numero_9, numero_10, 
                      numero_11, numero_12, numero_13, numero_14, numero_15
	           from jogo where cod_jogo = '$codigo' ";

    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $concurso = $row['concurso'];
    $tipo_jogo = $row['tipo_jogo'];

    switch ($tipo_jogo) {
        case "2":
            if ($row['numero_1'] == null) {
                throw new Exception("É necessário preencher todos os numeros do concurso!");
            } else {
                $resultado[0] = str_pad($row['numero_1'], 4, "0", STR_PAD_LEFT);
            }
        break;
        case "Q":
            if (
                $row['numero_1'] == null && $row['numero_2'] == null && $row['numero_3'] == null && 
                $row['numero_4'] == null && $row['numero_5'] == null
                ) {
                throw new Exception("É necessário preencher todos os numeros do concurso!");
            } else {
                $resultado[0] = str_pad($row['numero_1'], 2, "0", STR_PAD_LEFT);
                $resultado[1] = str_pad($row['numero_2'], 2, "0", STR_PAD_LEFT);
                $resultado[2] = str_pad($row['numero_3'], 2, "0", STR_PAD_LEFT);
                $resultado[3] = str_pad($row['numero_4'], 2, "0", STR_PAD_LEFT);
                $resultado[4] = str_pad($row['numero_5'], 2, "0", STR_PAD_LEFT);
            }
        break;
        case "U":
        case "S":
            if ($row['numero_1'] == null && $row['numero_2'] == null && $row['numero_3'] == null && 
                $row['numero_4'] == null && $row['numero_5'] == null && $row['numero_6'] == null) {
                throw new Exception("É necessário preencher todos os numeros do concurso!");
            } else {
                $resultado[0] = str_pad($row['numero_1'], 2, "0", STR_PAD_LEFT);
                $resultado[1] = str_pad($row['numero_2'], 2, "0", STR_PAD_LEFT);
                $resultado[2] = str_pad($row['numero_3'], 2, "0", STR_PAD_LEFT);
                $resultado[3] = str_pad($row['numero_4'], 2, "0", STR_PAD_LEFT);
                $resultado[4] = str_pad($row['numero_5'], 2, "0", STR_PAD_LEFT);
                $resultado[5] = str_pad($row['numero_6'], 2, "0", STR_PAD_LEFT);
            }
        break;
        case "B":
            if (
                $row['numero_1'] == null && $row['numero_2'] == null && $row['numero_3'] == null && 
                $row['numero_4'] == null && $row['numero_5'] == null && $row['numero_6'] == null &&
                $row['numero_7'] == null && $row['numero_8'] == null && $row['numero_9'] == null && 
                $row['numero_10'] == null        ) {
                throw new Exception("É necessário preencher os numeros do concurso!");
            } else {
                $resultado[0] = str_pad($row['numero_1'], 4, "0", STR_PAD_LEFT);
                $resultado[1] = str_pad($row['numero_2'], 4, "0", STR_PAD_LEFT);
                $resultado[2] = str_pad($row['numero_3'], 4, "0", STR_PAD_LEFT);
                $resultado[3] = str_pad($row['numero_4'], 4, "0", STR_PAD_LEFT);
                $resultado[4] = str_pad($row['numero_5'], 4, "0", STR_PAD_LEFT);
                $resultado[5] = str_pad($row['numero_6'], 4, "0", STR_PAD_LEFT);
                $resultado[6] = str_pad($row['numero_7'], 4, "0", STR_PAD_LEFT);
                $resultado[7] = str_pad($row['numero_8'], 4, "0", STR_PAD_LEFT);
                $resultado[8] = str_pad($row['numero_9'], 4, "0", STR_PAD_LEFT);
                $resultado[9] = str_pad($row['numero_10'], 4, "0", STR_PAD_LEFT);
            }
        break;
        case "L":
            if ($row['numero_1'] == null && $row['numero_2'] == null && $row['numero_3'] == null && 
                $row['numero_4'] == null && $row['numero_5'] == null && $row['numero_6'] == null &&
                $row['numero_7'] == null && $row['numero_8'] == null && $row['numero_9'] == null && 
                $row['numero_10'] == null && $row['numero_11'] == null && $row['numero_12'] == null && 
                $row['numero_13'] == null && $row['numero_14'] == null && $row['numero_15'] == null) {
                    throw new Exception("É necessário preencher os numeros do concurso!");
            } else {
                $resultado[0] = str_pad($row['numero_1'], 2, "0", STR_PAD_LEFT);
                $resultado[1] = str_pad($row['numero_2'], 2, "0", STR_PAD_LEFT);
                $resultado[2] = str_pad($row['numero_3'], 2, "0", STR_PAD_LEFT);
                $resultado[3] = str_pad($row['numero_4'], 2, "0", STR_PAD_LEFT);
                $resultado[4] = str_pad($row['numero_5'], 2, "0", STR_PAD_LEFT);
                $resultado[5] = str_pad($row['numero_6'], 2, "0", STR_PAD_LEFT);
                $resultado[6] = str_pad($row['numero_7'], 2, "0", STR_PAD_LEFT);
                $resultado[7] = str_pad($row['numero_8'], 2, "0", STR_PAD_LEFT);
                $resultado[8] = str_pad($row['numero_9'], 2, "0", STR_PAD_LEFT);
                $resultado[9] = str_pad($row['numero_10'], 2, "0", STR_PAD_LEFT);
                $resultado[10] = str_pad($row['numero_10'], 2, "0", STR_PAD_LEFT);
                $resultado[11] = str_pad($row['numero_11'], 2, "0", STR_PAD_LEFT);
                $resultado[12] = str_pad($row['numero_12'], 2, "0", STR_PAD_LEFT);
                $resultado[13] = str_pad($row['numero_13'], 2, "0", STR_PAD_LEFT);
                $resultado[14] = str_pad($row['numero_14'], 2, "0", STR_PAD_LEFT);
                $resultado[15] = str_pad($row['numero_15'], 2, "0", STR_PAD_LEFT);
            }           
        break;
        case "R":
            if ($row['numero_1'] == null) {
                throw new Exception("É necessário preencher todos os numeros do concurso!");
            } else {
                $resultado[0] = str_pad($row['numero_1'], 3, "0", STR_PAD_LEFT);
            } 
        break;
    }
    
    $cod_site = $row['cod_site'];

    if ($tipo_jogo === "B") {
        $queryConfigBicho = " SELECT
                COD_AREA,
                PREMIO_MILHAR_CENTENA,
                PREMIO_MILHAR_SECA,
                PREMIO_MILHAR_INVERTIDA,
                PREMIO_CENTENA,
                PREMIO_CENTENA_INVERTIDA,
                PREMIO_GRUPO,
                PREMIO_DUQUE_GRUPO,
                PREMIO_TERNO_GRUPO,
                PREMIO_DEZENA,
                PREMIO_DUQUE_DEZENA,
                PREMIO_TERNO_DEZENA,
                PREMIO_TERNO_GRUPO10
                FROM configuracao_bicho
            WHERE COD_SITE = '$cod_site'";

        $resultConfigBicho = mysqli_query($con, $queryConfigBicho);
        $hash_map = array();
        while ($rowBicho = mysqli_fetch_array($resultConfigBicho, MYSQLI_ASSOC)) {
            $row_array['premio_milhar_centena'] = $rowBicho['PREMIO_MILHAR_CENTENA'];
            $row_array['premio_milhar_seca'] = $rowBicho['PREMIO_MILHAR_SECA'];
            $row_array['premio_centena'] = $rowBicho['PREMIO_CENTENA'];
            $row_array['premio_grupo'] = $rowBicho['PREMIO_GRUPO'];
            $row_array['premio_duque_grupo'] = $rowBicho['PREMIO_DUQUE_GRUPO'];
            $row_array['premio_terno_grupo'] = $rowBicho['PREMIO_TERNO_GRUPO'];
            $row_array['premio_dezena'] = $rowBicho['PREMIO_DEZENA'];
            $row_array['premio_duque_dezena'] = $rowBicho['PREMIO_DUQUE_DEZENA'];
            $row_array['premio_terno_dezena'] = $rowBicho['PREMIO_TERNO_DEZENA'];
            $row_array['premio_terno_grupo10'] = $rowBicho['PREMIO_TERNO_GRUPO10'];
            $hash_map[$rowBicho['COD_AREA']] = $row_array;
        }

        $queryBichoGrupo = " select codigo, descricao, numero_1, numero_2, numero_3, numero_4 from bicho ";

        $resultBichoGrupo = mysqli_query($con, $queryBichoGrupo);

        $bicho_grupo = [];

        while ($rowBichoGrupo = mysqli_fetch_array($resultBichoGrupo, MYSQLI_ASSOC)) {
            $row_array_grupo['codigo'] = $rowBichoGrupo['codigo'];
            $row_array_grupo['descricao'] = $rowBichoGrupo['descricao'];
            $row_array_grupo['numero_1'] = $rowBichoGrupo['numero_1'];
            $row_array_grupo['numero_2'] = $rowBichoGrupo['numero_2'];
            $row_array_grupo['numero_3'] = $rowBichoGrupo['numero_3'];
            $row_array_grupo['numero_4'] = $rowBichoGrupo['numero_4'];

            array_push($bicho_grupo, $row_array_grupo);
        }

        $query = " SELECT HABILITA_CENTENA FROM EXTRACAO_BICHO WHERE COD_EXTRACAO = '$concurso' ";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $habilitaCentena = $row['HABILITA_CENTENA'] == 'S';
    }

    $query = " select apo.cod_aposta, apo.cod_bilhete, cast(apo.do_premio as UNSIGNED) do_premio, cast(apo.ao_premio as UNSIGNED) ao_premio, apo.tipo_jogo,
                      apo.valor_aposta, apo.txt_aposta, apo.inversoes, apo.possivel_retorno, apo.flg_sorte, apo.retorno5, apo.retorno4, apo.retorno3,
                      u.cod_area, apo.comissao, apo.comissao_gerente, u.cod_usuario
    from aposta apo
    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete)
    inner join usuario u on (bil.cod_usuario = u.cod_usuario)
     where apo.cod_jogo = '$codigo'
       and apo.status = 'A'
       and bil.status_bilhete = 'A' ";

    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $cod_aposta = $row['cod_aposta'];
        $cod_bilhete = $row['cod_bilhete'];
        $cod_area = $row['cod_area'];
        $do_premio = $row['do_premio'];
        $ao_premio = $row['ao_premio'];
        $tipo_jogo_bicho = $row['tipo_jogo'];
        $valor_aposta = $row['valor_aposta'];
        $flg_sorte = $row['flg_sorte'];
        $valor_ganho = 0;
        $cod_cambista = $row['cod_usuario'];
        $comissao = $row['comissao'];
        $comissao_gerente = $row['comissao_gerente'];

        $array = explode('-', str_replace(' ', '', $row['txt_aposta']));
        $contador = 0;
        $contadorDuque = 0;
        $contadorTerno = 0;
        $contadorQuadra = 0;
        $contadorQuina = 0;

        if ($tipo_jogo != "B") {

            switch ($tipo_jogo) {
                case 2:
                    $resultado2 = array();
                    $resultado2[0] = $resultado[0];
                    foreach ($array as $value) {
                        if (in_array($value, $resultado2)) {
                            $contador = $contador + 1;
                        }
                    } 
                break;
                case "R":
                    $resultadoR = array();
                    $resultadoR[0] = $resultado[0];
                    foreach ($array as $value) {
                        if (in_array($value, $resultadoR)) {
                            $contador = $contador + 1;
                        }
                    } 
                break;
                default:
                    foreach ($array as $value) {
                        if (in_array($value, $resultado)) {
                            $contador = $contador + 1;
                        }
                    }
                break;
            }

            $status = "P";
            switch ($tipo_jogo) {
                case "2":
                    if ($contador == 1) {
                        $status = "G";
                        $valor_ganho = $row['possivel_retorno'];
                        $queryConfGerente = mysqli_query($con,
                                "SELECT G.*, CONF.*                   
                                    FROM USUARIO USU 
                                        INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)                                        
                                        LEFT JOIN CONFIGURACAO_2PRA500 CONF ON G.COD_AREA = CONF.COD_AREA
                                    WHERE USU.COD_USUARIO = '$cod_cambista'"
                        );
                        $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                        $comissao_gerente = (($valor_aposta - $valor_ganho - $comissao) 
                            * $config_gerente_arr['PCT_COMISSAO_2PRA500']) / 100;
                    }
                break;
                case "Q":
                    if ($contador == 5) {
                        $status = "G";
                        $valor_ganho = $row['possivel_retorno'];
                    } else if ($flg_sorte == "S" && $contador == 4) {
                        $status = "G";
                        $valor_ganho = $row['retorno4'];
                    } else if ($flg_sorte == "S" && $contador == 3) {
                        $status = "G";
                        $valor_ganho = $row['retorno3'];
                    }
                    if ($status == "G") {
                        $queryConfGerente = mysqli_query($con,
                                "SELECT G.*, CONF.*                   
                                FROM USUARIO USU 
                                    INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)
                                    LEFT JOIN CONFIGURACAO_QUININHA CONF ON G.COD_AREA = CONF.COD_AREA
                                WHERE USU.COD_USUARIO = '$cod_cambista'"
                        );
                        $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                        $comissao_gerente = (($valor_aposta - $valor_ganho - $comissao) 
                            * $config_gerente_arr['PCT_COMISSAO_QUININHA']) / 100;
                    }
                break;
                case "S":
                    if ($contador == 6) {
                        $status = "G";
                        $valor_ganho = $row['possivel_retorno'];
                    } else if ($flg_sorte == "S" && $contador == 5) {
                        $status = "G";
                        $valor_ganho = $row['retorno5'];
                    } else if ($flg_sorte == "S" && $contador == 4) {
                        $status = "G";
                        $valor_ganho = $row['retorno4'];
                    }
                    if ($status == "G") {
                        $queryConfGerente = mysqli_query($con,
                                "SELECT G.*, CONF.*                   
                                FROM USUARIO USU 
                                    INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)
                                    LEFT JOIN CONFIGURACAO CONF ON G.COD_AREA = CONF.COD_AREA
                                WHERE USU.COD_USUARIO = '$cod_cambista'"
                        );
                        $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                        $comissao_gerente = (($valor_aposta - $valor_ganho - $comissao) 
                            * $config_gerente_arr['PCT_COMISSAO_SENINHA']) / 100;
                    }
                break;
                case "U":
                    if ($contador == 6) {
                        $status = "G";
                        $valor_ganho = $row['possivel_retorno'];
                    } else if ($contador == 5) {
                        $status = "G";
                        $valor_ganho = $row['retorno5'];
                    } else if ($contador == 4) {
                        $status = "G";
                        $valor_ganho = $row['retorno4'];
                    }
                    if ($status == "G") {
                        $queryConfGerente = mysqli_query($con,
                                "SELECT G.*, CONF.*                   
                                FROM USUARIO USU 
                                    INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)
                                    LEFT JOIN CONFIGURACAO_SUPER_SENA CONF ON G.COD_AREA = CONF.COD_AREA
                                WHERE USU.COD_USUARIO = '$cod_cambista'"
                        );
                        $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                        $comissao_gerente = (($valor_aposta - $valor_ganho - $comissao) 
                            * $config_gerente_arr['PCT_COMISSAO_SUPERSENA']) / 100;
                    }
                break;                
                case "L":
                    if ($contador == 15) {
                        $status = "G";
                        $valor_ganho = $row['possivel_retorno'];
                        $queryConfGerente = mysqli_query($con,
                                "SELECT G.*, CONF.*                   
                                FROM USUARIO USU 
                                    INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)
                                    LEFT JOIN CONFIGURACAO_LOTINHA CONF ON G.COD_AREA = CONF.COD_AREA
                                WHERE USU.COD_USUARIO = '$cod_cambista'"
                        ); 
                        $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                        $comissao_gerente = (($valor_aposta - $valor_ganho - $comissao) 
                            * $config_gerente_arr['PCT_COMISSAO_LOTINHA']) / 100;
                    }
                break;
                case "R":
                    if ($contador == 1) {
                        $status = "G";
                        $valor_ganho = $row['possivel_retorno'];
                        $queryConfGerente = mysqli_query($con,
                                "SELECT G.*, CONF.*                   
                                FROM USUARIO USU 
                                    INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)
                                    LEFT JOIN CONFIGURACAO_RIFA CONF ON G.COD_AREA = CONF.COD_AREA
                                WHERE USU.COD_USUARIO = '$cod_cambista'"
                        );
                        $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                        $comissao_gerente = (($valor_aposta - $valor_ganho - $comissao) 
                            * $config_gerente_arr['PCT_COMISSAO_RIFA']) / 100;
                    }
                break;
            }
        } else {
            $contador = 0;
            $retorno = 0;
            $primeiroPasse = true;
            $segundoPasse = false;
            foreach ($array as $value) {
                if ($tipo_jogo_bicho === "MS") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        if ($contadorPremio == 7 and $habilitaCentena) {

                            if ($res != null && $res != 0) {
                                $valorValidar = $res;
                                if (strlen($res) == 4) {
                                    $valorValidar = substr($res, 1, 3);
                                }
    
                                if (substr($value, 1, 3) == $valorValidar) {
                                    $qtdPremios = $ao_premio - $do_premio + 1;
                                    $retorno = $retorno + ($valor_aposta / count($array) / $qtdPremios * $hash_map[$cod_area]['premio_centena']);
                                    $contador = $contador + 1;
                                }
                            }

                        } else {
                            $valorValidar = $res;

                            if ($value == $valorValidar) {
                                $retorno = $retorno + $row['possivel_retorno'];
                                $contador = $contador + 1;
                            }
                        }
                    }

                } else if ($tipo_jogo_bicho === "MC") {
                    $contadorPremio = 0;
                    $milharPremiada = false;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = $res;

                        if ($value == $valorValidar) {
                            $retorno = $retorno + $row['possivel_retorno'];
                            $contador = $contador + 1;
                            $milharPremiada = true;
                        }
                    }

                    if (!$milharPremiada) {
                        $contadorPremio = 0;
                        foreach ($resultado as $res) {
                            $contadorPremio = $contadorPremio + 1;
                            if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                                continue;
                            }
                            if (substr($value, 1, 3) == substr($res, 1, 3)) {

                                $divisor = $ao_premio - $do_premio + 1;
                                $retorno = $retorno + ($valor_aposta * $hash_map[$cod_area]['premio_centena'] / 2 / $divisor / count($array));
                                $contador = $contador + 1;
                            }
                        }
                    }
                } else if ($tipo_jogo_bicho === "MI") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = $res;

                        $vetorMilharInvertida = retornaArrayMilharInvertida($value);
                        foreach ($vetorMilharInvertida as $milharInvertida) {
                            if ($valorValidar == $milharInvertida) {
                                $retorno = $retorno + $row['possivel_retorno'];
                                $contador = $contador + 1;
                                break;
                            }
                        }
                    }
                } else if ($tipo_jogo_bicho === "MCI") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = $res;

                        $vetorMilharInvertida = retornaArrayMilharInvertida($value);
                        foreach ($vetorMilharInvertida as $milharInvertida) {
                            if ($valorValidar == $milharInvertida) {
                                $retorno = $retorno + $row['possivel_retorno'];
                                $contador = $contador + 1;
                                break;
                            }

                            if (substr($valorValidar, 1, 3) == substr($milharInvertida, 1, 3)) {
                                $divisor = $ao_premio - $do_premio + 1;
                                $retorno = $retorno + ($hash_map[$cod_area]['premio_centena'] / ($row['inversoes'] / 2) / $divisor * ($valor_aposta / 2));
                                $contador = $contador + 1;
                                break;
                            }
                        }
                    }
                } else if ($tipo_jogo_bicho === "C") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = $res;
                        if (strlen($res) == 4) {
                            $valorValidar = substr($res, 1, 3);
                        }

                        if ($value == $valorValidar) {
                            $retorno = $retorno + $row['possivel_retorno'];
                            $contador = $contador + 1;
                        }
                    }
                } else if ($tipo_jogo_bicho === "CI") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = $res;
                        if (strlen($res) == 4) {
                            $valorValidar = substr($res, 1, 3);
                        }

                        $vetorCentenaInvertida = retornaArrayMilharInvertida($value);

                        foreach ($vetorCentenaInvertida as $centenaInvertida) {
                            if ($valorValidar == $centenaInvertida) {
                                $retorno = $retorno + $row['possivel_retorno'];
                                $contador = $contador + 1;
                                break;
                            }
                        }
                    }
                } else if ($tipo_jogo_bicho === "G" || $tipo_jogo_bicho == "5P100") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;
                            $retorno = $retorno + $row['possivel_retorno'];
                        }
                    }
                } else if ($tipo_jogo_bicho === "DG" || $tipo_jogo_bicho === "DG6") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;
                            $contadorDuque = $contadorPremio;

                            break;
                        }
                    }
                } else if ($tipo_jogo_bicho === "DGC") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if (
                            $contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio ||
                            $contadorQuadra == $contadorPremio || $contadorQuina == $contadorPremio
                        ) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;

                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else if ($contadorTerno == 0) {
                                $contadorTerno = $contadorPremio;
                            } else if ($contadorQuadra == 0) {
                                $contadorQuadra = $contadorPremio;
                            } else if ($contadorQuina == 0) {
                                $contadorQuina = $contadorPremio;
                            }

                            if ($contador == 3) {
                                $retorno = $row['possivel_retorno'] * 3;
                            } else if ($contador == 4) {
                                $retorno = $row['possivel_retorno'] * 6;
                            } else if ($contador == 5) {
                                $retorno = $row['possivel_retorno'] * 10;
                            } else {
                                $retorno = $row['possivel_retorno'];
                            }

                            break;
                        }
                    }
                } else if ($tipo_jogo_bicho === "TG" || $tipo_jogo_bicho === "TG10" || $tipo_jogo_bicho === "TG6" 
                        || $tipo_jogo_bicho === "TSE" || $tipo_jogo_bicho === "TSO"
                        || $tipo_jogo_bicho === "TEX-A" || $tipo_jogo_bicho === "TEX-F") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;
                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else {
                                $contadorTerno = $contadorPremio;
                            }
                            break;
                        }
                    }
                }  
                else if ($tipo_jogo_bicho === "TGC") {
                    $contadorPremio = 0;

                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if (
                            $contadorPremio < $do_premio || $contadorPremio > $ao_premio ||
                            $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio ||
                            $contadorQuadra == $contadorPremio || $contadorQuina == $contadorPremio
                        ) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;
                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else if ($contadorTerno == 0) {
                                $contadorTerno = $contadorPremio;
                            } else if ($contadorQuadra == 0) {
                                $contadorQuadra = $contadorPremio;
                            } else if ($contadorQuina == 0) {
                                $contadorQuina = $contadorPremio;
                            }

                            if ($contador == 4) {
                                $retorno = $row['possivel_retorno'] * 4;
                            } else if ($contador == 5) {
                                $retorno = $row['possivel_retorno'] * 10;
                            } else {
                                $retorno = $row['possivel_retorno'];
                            }

                            break;
                        }
                    }
                } 
                else if ($tipo_jogo_bicho === "QG") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if (
                            $contadorPremio < $do_premio || $contadorPremio > $ao_premio ||
                            $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio ||
                            $contadorQuadra == $contadorPremio || $contadorQuina == $contadorPremio
                        ) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);
                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;
                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else if ($contadorTerno == 0) {
                                $contadorTerno = $contadorPremio;
                            } else if ($contadorQuadra == 0) {
                                $contadorQuadra = $contadorPremio;
                            } else if ($contadorQuina == 0) {
                                $contadorQuina = $contadorPremio;
                            }
                            break;
                        }
                    }
                }
                else if ($tipo_jogo_bicho === "DZ") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == $valorValidar) {
                            $contador = $contador + 1;
                            $retorno = $retorno + $row['possivel_retorno'];
                        }
                    }
                } else if ($tipo_jogo_bicho === "DDZ" || $tipo_jogo_bicho === "DDZ6") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == $valorValidar) {
                            $contador = $contador + 1;
                            $contadorDuque = $contadorPremio;
                            break;
                        }
                    }
                } else if ($tipo_jogo_bicho === "DDZC") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if (
                            $contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio ||
                            $contadorQuadra == $contadorPremio || $contadorQuina == $contadorPremio
                        ) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == $valorValidar) {
                            $contador = $contador + 1;

                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else if ($contadorTerno == 0) {
                                $contadorTerno = $contadorPremio;
                            } else if ($contadorQuadra == 0) {
                                $contadorQuadra = $contadorPremio;
                            } else if ($contadorQuina == 0) {
                                $contadorQuina = $contadorPremio;
                            }

                            if ($contador == 3) {
                                $retorno = $row['possivel_retorno'] * 3;
                            } else if ($contador == 4) {
                                $retorno = $row['possivel_retorno'] * 6;
                            } else if ($contador == 5) {
                                $retorno = $row['possivel_retorno'] * 10;
                            } else {
                                $retorno = $row['possivel_retorno'];
                            }

                            break;
                        }
                    }
                } else if ($tipo_jogo_bicho === "TDZ" || $tipo_jogo_bicho === "TDZ10" || $tipo_jogo_bicho === "TDZ6") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == $valorValidar) {
                            $contador = $contador + 1;
                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else {
                                $contadorTerno = $contadorPremio;
                            }
                            break;
                        }
                    }
                } else if ($tipo_jogo_bicho === "TDZC") {
                    $contadorPremio = 0;
                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;
                        if (
                            $contadorPremio < $do_premio || $contadorPremio > $ao_premio ||
                            $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio ||
                            $contadorQuadra == $contadorPremio || $contadorQuina == $contadorPremio
                        ) {
                            continue;
                        }
                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == $valorValidar) {
                            $contador = $contador + 1;
                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else if ($contadorTerno == 0) {
                                $contadorTerno = $contadorPremio;
                            } else if ($contadorQuadra == 0) {
                                $contadorQuadra = $contadorPremio;
                            } else if ($contadorQuina == 0) {
                                $contadorQuina = $contadorPremio;
                            }

                            if ($contador == 4) {
                                $retorno = $row['possivel_retorno'] * 4;
                            } else if ($contador == 5) {
                                $retorno = $row['possivel_retorno'] * 10;
                            } else {
                                $retorno = $row['possivel_retorno'];
                            }

                            break;
                        }
                    }
                } else if ($tipo_jogo_bicho === "PS" || $tipo_jogo_bicho === "PS12") {
                    $contadorPremio = 0;

                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;

                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio) {
                            continue;
                        }

                        if (($primeiroPasse == true && $contadorPremio == 1) || ($segundoPasse == true && $contadorPremio > 1)) {
                            $valorValidar = substr($res, strlen($res) - 2, 2);

                            if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                                $contador = $contador + 1;
                                $contadorDuque = $contadorPremio;

                                $primeiroPasse = false;
                                $segundoPasse = true;

                                break;
                            }
                        }
                    }
                } else if ($tipo_jogo_bicho === "PC") {
                    $contadorPremio = 0;

                    foreach ($resultado as $res) {
                        $contadorPremio = $contadorPremio + 1;

                        if ($contadorPremio < $do_premio || $contadorPremio > $ao_premio || $contadorDuque == $contadorPremio || $contadorTerno == $contadorPremio) {
                            continue;
                        }

                        $valorValidar = substr($res, strlen($res) - 2, 2);

                        if ($value == retornaBicho($bicho_grupo, $valorValidar)) {
                            $contador = $contador + 1;
                            if ($contadorDuque == 0) {
                                $contadorDuque = $contadorPremio;
                            } else if ($contadorTerno == 0) {
                                $contadorTerno = $contadorPremio;
                            }

                            break;
                        }
                    }
                }
            }
            $status = "P";
            if (
                (
                    ($tipo_jogo_bicho == "PS" || $tipo_jogo_bicho == "PS12" || 
                     $tipo_jogo_bicho == "DG" || $tipo_jogo_bicho == "DG6" || $tipo_jogo_bicho == "DGC" || 
                     $tipo_jogo_bicho == "DDZ" || $tipo_jogo_bicho == "DDZ6" || $tipo_jogo_bicho == "DDZC") 
                    && ($contador >= 2)
                )
                || 
                (
                    ($contador >= 1) && 
                    ($tipo_jogo_bicho != "PS")  && ($tipo_jogo_bicho != "PS12") && ($tipo_jogo_bicho != "PC") && 
                    ($tipo_jogo_bicho != "DG")  && ($tipo_jogo_bicho != "DG6") && ($tipo_jogo_bicho != "DGC") &&
                    ($tipo_jogo_bicho != "DDZ") && ($tipo_jogo_bicho != "DDZ6") &&  ($tipo_jogo_bicho != "DDZC") && 
                    ($tipo_jogo_bicho != "TG")  && ($tipo_jogo_bicho != "TG6") && ($tipo_jogo_bicho != "TG10") && ($tipo_jogo_bicho != "TGC") && 
                    ($tipo_jogo_bicho != "TSE") && ($tipo_jogo_bicho != "TSO") &&
                    ($tipo_jogo_bicho != "TEX-A") && ($tipo_jogo_bicho != "TEX-F") &&
                    ($tipo_jogo_bicho != "TDZ") && ($tipo_jogo_bicho != "TDZ6") && ($tipo_jogo_bicho != "TDZ10") && ($tipo_jogo_bicho != "TDZC") &&                      
                    ($tipo_jogo_bicho != "QG")
                ) 
                || 
                (
                    ($contador >= 3) && 
                    ($tipo_jogo_bicho == "TG" || $tipo_jogo_bicho == "TG10" || $tipo_jogo_bicho == "TG6" || 
                     $tipo_jogo_bicho == "TSE" ||  $tipo_jogo_bicho == "TSO" ||  
                     $tipo_jogo_bicho == "TEX-A" ||  $tipo_jogo_bicho == "TEX-F" ||  
                     $tipo_jogo_bicho == "TDZ" || $tipo_jogo_bicho == "TDZ10" || $tipo_jogo_bicho == "TDZ6" || 
                     $tipo_jogo_bicho == "TGC" || $tipo_jogo_bicho == "TDZC")
                ) 
                ||
                ($tipo_jogo_bicho == "QG" && $contador >= 5)
                || 
                (
                    $tipo_jogo_bicho == "PC" && $contador >= 2 && 
                    (
                        ($contadorDuque == 1 && $contadorTerno > 1) 
                        || ($contadorDuque > 1 && $contadorTerno == 1)
                    )
                )      
            ) {

                // atualiza o possivel retorno para cada milhar, centena, dezena ou grupo que ele acertou
                if (
                    $tipo_jogo_bicho == "MS" || $tipo_jogo_bicho == "MC" || $tipo_jogo_bicho == "MI"
                    || $tipo_jogo_bicho == "MCI" || $tipo_jogo_bicho == "C" || $tipo_jogo_bicho == "CI"
                    || $tipo_jogo_bicho == "DZ" || $tipo_jogo_bicho == "G" || $tipo_jogo_bicho == "5P100" || 
                    (
                        ($contador >= 4) && $tipo_jogo_bicho == "TGC" || $tipo_jogo_bicho == "TDZC"
                    ) 
                    || 
                    (
                        ($contador >= 3) && $tipo_jogo_bicho == "DGC" || $tipo_jogo_bicho == "DDZC"
                    )
                ) {
                    $valor_ganho = $retorno;
                } else {
                    $valor_ganho = $row['possivel_retorno'];
                }

                $queryConfGerente = mysqli_query($con,
                        "SELECT G.*, CONF.*, CONF_COMISSAO.*   
                            FROM USUARIO USU 
                                INNER JOIN USUARIO G ON (USU.COD_GERENTE = G.COD_USUARIO)
                                LEFT JOIN CONFIGURACAO_COMISSAO_BICHO CONF_COMISSAO ON G.COD_USUARIO = CONF_COMISSAO.COD_USUARIO
                                LEFT JOIN CONFIGURACAO_BICHO CONF ON G.COD_AREA = CONF.COD_AREA
                            WHERE USU.COD_USUARIO = '$cod_cambista'"
                );

                $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
                $saldo = $valor_aposta - $valor_ganho - $comissao;
                $comissao_gerente = obterComissaoBicho(
                    $tipo_jogo_bicho,
                    $saldo,
                    $config_gerente_arr
                );

                $status = "G";
            }
        }

        $sqlUpdate = "Update aposta set status = '$status', valor_ganho = '$valor_ganho', comissao_gerente = '$comissao_gerente' where cod_aposta = '$cod_aposta' ";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao processar aposta: ' . $cod_aposta);
        }

        $sqlUpdate = "Update bilhete set status_bilhete = 'P' where cod_bilhete = '$cod_bilhete' ";

        if ($con->query($sqlUpdate) != true) {
            throw new Exception('Erro ao processar bilhete: ' . $cod_bilhete);
        }
    }

    $sqlUpdate = "Update jogo set tp_status = 'P' where cod_jogo = '$codigo' ";

    if ($con->query($sqlUpdate) === true) {
        inserir_auditoria(
            $con,
            $cod_usuario,
            $cod_site,
            AUD_JOGO_PROCESSADO,
            descreverProcessamentoJogo($codigo)
        );
    } else {
        throw new Exception('Erro ao processar jogo: ' . $codigo);
    }
}

function obterComissaoBicho(
    $tipo_aposta,
    $valor_aposta,
    $comissaoBicho
) {
    $comissao = 0;
    if (
        $tipo_aposta == "MC" && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "MS" && $comissaoBicho['COMISSAO_MILHAR_SECA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_SECA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_SECA'] / 100;
    } else if (($tipo_aposta == "MI" || $tipo_aposta == "MCI") &&
        $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] / 100;
    } else if (
        $tipo_aposta == "C" && $comissaoBicho['COMISSAO_CENTENA'] != null
        && $comissaoBicho['COMISSAO_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "CI" && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] / 100;
    } else if (
        $tipo_aposta == "G" && $comissaoBicho['COMISSAO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_GRUPO'] / 100;
    } else if (
        $tipo_aposta == "5P100" && $comissaoBicho['COMISSAO_5P100'] != null
        && $comissaoBicho['COMISSAO_5P100'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_5P100'] / 100;
    } else if (($tipo_aposta == "DG" || $tipo_aposta == "DG6" || $tipo_aposta == "DGC") && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] != null
        && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_GRUPO'] / 100;
    } else if (($tipo_aposta == "TG" || $tipo_aposta == "TG10" || $tipo_aposta == "TG6" 
             || $tipo_aposta == "TGC" || $tipo_aposta == "TSE" || $tipo_aposta == "TSO"
             || $tipo_aposta == "TEX-A" || $tipo_aposta == "TEX-F")
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_GRUPO'] / 100;
    } else if (
        $tipo_aposta == "DZ" && $comissaoBicho['COMISSAO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DEZENA'] / 100;
    } else if (($tipo_aposta == "DDZ" || $tipo_aposta == "DDZ6" || $tipo_aposta == "DDZC") &&  $comissaoBicho['COMISSAO_DUQUE_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DUQUE_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_DEZENA'] / 100;
    } else if (($tipo_aposta == "TDZ" || $tipo_aposta == "TDZ10" || $tipo_aposta == "TDZ6" || $tipo_aposta == "TDZC") &&
        $comissaoBicho['COMISSAO_TERNO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_TERNO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_DEZENA'] / 100;
    } else if (($tipo_aposta == "PS" || $tipo_aposta == "PS12") && $comissaoBicho['COMISSAO_PASSE_SECO'] != null
        && $comissaoBicho['COMISSAO_PASSE_SECO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_SECO'] / 100;
    } else if (
        $tipo_aposta == "PC" && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] != null
        && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_COMBINADO'] / 100;
    } else {
        $comissao = $valor_aposta * $comissaoBicho['PCT_COMISSAO_BICHO'] / 100;
    }

    return $comissao;
}

$response = [];

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

try {
    if ($operacao == "salvar") {
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        try {
            salvarJogo($con, $codigo, $cod_usuario, $cod_site);
            $response['status'] = "OK";
        } catch (Exception $e) {
            $response['status'] = "ERROR";
            $response['mensagem'] = $e->getMessage();
        }
        echo json_encode($response);
    } else if ($operacao == "salvarProcessar") {
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        try {
            $con->begin_transaction();
            salvarJogo($con, $codigo, $cod_usuario, $cod_site);
            processarResultado($con, $codigo, $cod_usuario, $cod_site);
            $con->commit();
            $response['status'] = "OK";
        } catch (Exception $e) {
            $con->rollback();
            $response['status'] = "ERROR";
            $response['mensagem'] = $e->getMessage();
        }
        echo json_encode($response);
    } else if ($operacao == "bloquearJogo") {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
        $status = mysqli_real_escape_string($con, $_POST['status']);
        $novoStatus;
        if ($status == 'B') {
            $novoStatus = 'L';
        } else {
            $novoStatus = 'B';
        }

        $sqlUpdate = "Update jogo set tp_status = '$novoStatus' where cod_jogo = '$codigo' ";
        if ($con->query($sqlUpdate) === true) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_JOGO_EDITADO,
                descreverBloqueioJogo($codigo, $novoStatus)
            );
            $response['status'] = "OK";
        } else {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
        }
        echo json_encode($response);
    } else if ($operacao == "finalizarConcurso") {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);

        $sqlUpdate = "Update jogo set tp_status = 'F' where cod_jogo = '$codigo' ";

        if ($con->query($sqlUpdate) === true) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_JOGO_FINALIZADO,
                descreverFinalizacaoJogo($codigo)
            );
            $response['status'] = "OK";
        } else {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
        }

        echo json_encode($response);
    } else if ($operacao == "reabrirConcurso") {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);

        $sqlUpdate = "Update jogo set tp_status = 'P' where cod_jogo = '$codigo' ";

        if ($con->query($sqlUpdate) === true) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_JOGO_REABERTO,
                descreverReaberturaJogo($codigo)
            );
            $response['status'] = "OK";
        } else {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
        }

        echo json_encode($response);
    } else if ($operacao == "reabrirJogo") {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);

        $sqlUpdate = "Update aposta set status = 'A', valor_ganho = 0 where status != 'C' and cod_jogo = '$codigo' ";

        if ($con->query($sqlUpdate) != true) {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
            return;
        }

        $query = "select cod_bilhete from aposta
                  where cod_jogo = '$codigo'";

        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

            $cod_bilhete = $row['cod_bilhete'];

            $sqlUpdate = "Update bilhete set status_bilhete = 'A' where status_bilhete != 'C' and cod_bilhete = '$cod_bilhete' ";

            if ($con->query($sqlUpdate) != true) {
                $response['status'] = "ERROR";
                $response['mensagem'] = $con->error;
                return;
            }
        }

        $sqlUpdate = "Update jogo set tp_status = 'B' where cod_jogo = '$codigo' ";

        if ($con->query($sqlUpdate) === true) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_JOGO_REABERTO,
                descreverReaberturaJogo($codigo)
            );
            $response['status'] = "OK";
        } else {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
        }

        echo json_encode($response);
    }
} finally {
    $con->close();
}

