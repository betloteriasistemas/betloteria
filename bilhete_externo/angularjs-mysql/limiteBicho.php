<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

if ($operacao == 'listar' || $operacao == 'carregar') {


    $query = "";

    $query =
        " SELECT b.CODIGO, b.MILHAR, b.CENTENA, b.GRUPO, b.DUQUE_GRUPO, b.TERNO_GRUPO, b.DEZENA,
         b.DUQUE_DEZENA, b.TERNO_DEZENA, ex.COD_EXTRACAO, TIME_FORMAT(ex.hora_extracao, '%H:%i') as HORA_EXTRACAO, ex.DESCRICAO
FROM configuracao_limite_bicho b
inner join extracao_bicho ex on (ex.cod_extracao = b.cod_extracao)
where b.cod_site = '$site' ";

    if ($operacao == 'carregar') {
        $codigos = implode(",", explode(',', mysqli_real_escape_string($con, $_POST['codigo'])));
        $query = $query . " and TIME_FORMAT(ex.hora_extracao, '%H:%i') in (select TIME_FORMAT(hora_extracao, '%H:%i') from jogo where cod_jogo in (" . $codigos . 
        ")) and ex.descricao in (select desc_hora from jogo where cod_jogo in (" . $codigos . ")) ";
    }
    $query = $query . " order by hora_extracao ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['codigo'] = $row['CODIGO'];
        $row_array['milhar'] = $row['MILHAR'];
        $row_array['centena'] = $row['CENTENA'];
        $row_array['grupo'] = $row['GRUPO'];
        $row_array['duque_grupo'] = $row['DUQUE_GRUPO'];
        $row_array['terno_grupo'] = $row['TERNO_GRUPO'];
        $row_array['dezena'] = $row['DEZENA'];
        $row_array['duque_dezena'] = $row['DUQUE_DEZENA'];
        $row_array['terno_dezena'] = $row['TERNO_DEZENA'];
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
        $row_array['descricao'] = $row['DESCRICAO'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

    echo json_encode($return_arr);
}
