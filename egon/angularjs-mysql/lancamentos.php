<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if(!isset($_POST)) die();

$response = [];

$username = mysqli_real_escape_string($con, $_POST['username']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$cambistas = mysqli_real_escape_string($con, $_POST['cambistas']);
$perfil = mysqli_real_escape_string($con, $_POST['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);


$query = "SELECT U.nome as criador, U2.nome, L.cod_usuario,L.cod_lancamento,L.motivo,
 case
        when L.tipo_lancamento = 'E' then 'ENTRADA'
        when L.tipo_lancamento = 'S' then 'SAÍDA' ELSE ' - ' END AS tipo_lancamento,

L.status,L.valor, L.ajuda_custo,
(DATE_FORMAT(L.data_hora, '%d/%m/%Y')) as data_hora FROM lancamento L, usuario U, usuario U2 
WHERE L.cod_usuario = U.cod_usuario
and L.cod_usuario_lancamento = U2.cod_usuario 
and L.Status = 'A' 
and L.cod_site = '$site' ";

if($perfil == 'G'){
	$query = $query . "AND L.cod_usuario_lancamento in (select cod_usuario from usuario where cod_gerente = '$cod_usuario') ";
}

$query = $query . " order by L.data_hora desc;";



$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
    $row_array['cod_usuario'] = $row['cod_usuario'];
	$row_array['nome'] = $row['nome'];
	$row_array['motivo'] = $row['motivo'];
	$row_array['criador'] = $row['criador'];
	$row_array['cod_lancamento'] = $row['cod_lancamento'];
	$row_array['tipo_lancamento'] = $row['tipo_lancamento'];
	$row_array['status'] = $row['status'];
	$row_array['valor'] = $row['valor'];
	$row_array['data_hora'] = $row['data_hora'];
	$row_array['ajuda_custo'] = $row['ajuda_custo'];
	

	array_push($return_arr,$row_array);
	
	if ($contador == mysqli_num_rows($result)){
		break;
	}
};

$con->close();
echo json_encode($return_arr , JSON_NUMERIC_CHECK);