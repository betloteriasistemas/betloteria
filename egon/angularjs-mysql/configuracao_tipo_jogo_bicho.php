<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

switch ($operacao) {
    case "listar_tipos":
        $query = "SELECT * FROM tipo_jogo_bicho tjb ORDER BY tjb.ORDEM";
        $result = mysqli_query($con, $query);
        $return_arr = array();
        
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $row_array['codigo_tipo_jogo'] = $row['COD_TIPO_JOGO_BICHO'];
            $row_array['codigo'] = $row['CODIGO'];
            $row_array['descricao'] = $row['DESCRICAO'];
            array_push($return_arr, $row_array);
        }
        
        $con->close();
        echo json_encode($return_arr);
        break;
    case "listar_tipos_por_site_area":
        $site = mysqli_real_escape_string($con, $_POST['site']);
        $cod_area = mysqli_real_escape_string($con, $_POST['cod_area']);

        $query = "SELECT tjb.*
                  FROM tipo_jogo_bicho tjb
                  INNER JOIN configuracao_tipo_jogo_bicho ctjb on (tjb.COD_TIPO_JOGO_BICHO = ctjb.COD_TIPO_JOGO_BICHO)
                  INNER JOIN configuracao_bicho cb on (ctjb.COD_CONFIGURACAO = cb.CODIGO)
                  WHERE cb.COD_SITE = '$site' and cb.COD_AREA = '$cod_area'
                  ORDER BY tjb.COD_TIPO_JOGO_BICHO";
        $result = mysqli_query($con, $query);
        $return_arr = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $row_array['codigo_tipo_jogo'] = $row['COD_TIPO_JOGO_BICHO'];
            $row_array['codigo'] = $row['CODIGO'];
            $row_array['descricao'] = $row['DESCRICAO'];
            array_push($return_arr, $row_array);
        }
        
        $con->close();
        echo json_encode($return_arr);
        break;    
}