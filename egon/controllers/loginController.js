var app = angular.module('main');

app.controller('loginCtrl', function ($scope, $http, $location, $routeParams, user, $rootScope) {
    $scope.idEscolhido = false;
    $scope.nu_whatsapp = null;
    $scope.logomarca = "img/logo_transparente.png";
    var currentId = $routeParams.id;


    username = localStorage.getItem('usuario');
    senha = localStorage.getItem('senha');
    site = localStorage.getItem('site');

    if (username && senha && site) {
        $scope.edtUsuario = username;
        $scope.edtSenha = senha;
        $scope.edtSite = site;
        $scope.chkLembrar = true;
    } else {
        $scope.edtUsuario = '';
        $scope.edtSenha = '';
        $scope.edtSite = '';
        $scope.chkLembrar = false;
    }


    if (typeof currentId != 'undefined') {
        $scope.edtSite = currentId;
        $scope.idEscolhido = true;
        $scope.logomarca = "img/" + currentId + ".png";

        var request = new XMLHttpRequest();
        request.open('HEAD', $scope.logomarca, false);
        request.send();
        if (request.status != 200) {
            $scope.logomarca = "img/logo_transparente.png";
        }

        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "site=" + $scope.edtSite + 
                  "&cod_area=0" + 
                  "&operacao=geral" +
                  "&schema=b" + $scope.edtSite
        }).then(function (response) {
            $scope.cfgGeral = response.data;
            debugger
            if ($scope.cfgGeral[0].nu_whatsapp != undefined && $scope.cfgGeral[0].nu_whatsapp != '') {
                $scope.nu_whatsapp = $scope.cfgGeral[0].nu_whatsapp;
                $('.centralizado-displaynone')[0].style.display = 'block';
            }
        }).catch(angular.noop);
    }

    if (typeof currentId != 'undefined' && currentId == 4808) {
        $scope.idEscolhido = false;
    }

    $scope.abrirConferir = function () {
        var currentId = $routeParams.id;
        if (typeof currentId == "undefined") {
            currentId = 9999;
        }

        if (typeof currentId != 9999) {
            window.open('bilhete_externo/#!/dashboard/' + currentId, '_self');
        } else {
            window.open('bilhete_externo/#!/seninha', '_self');
        }

    }

    $scope.downloadAplicativo = function () {
        window.open('app/AplicativoBETLoteria.apk');
    }

    $scope.login = function () {
        var site = $scope.edtSite;
        var username = $scope.edtUsuario;
        var password = $scope.edtSenha;

        if (typeof site === "undefined") {
            $.alert('Informe o SITE!');
        } else if (typeof username === "undefined") {
            $.alert('Informe o USUÁRIO!');
        } else if (typeof password === "undefined") {
            $.alert('Informe a SENHA!');
        } else {
            $http({
                url: 'angularjs-mysql/login.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + username + 
                      '&password=' + password + 
                      '&site=' + site + 
                      '&escolhido=' + $scope.idEscolhido + 
                      '&lembrar= ' + $scope.chkLembrar
            }).then(function (response) {
                if (response.data.status == 'loggedin') {
                    if (password == '12345'){
                        $.alert({
                            title: 'Trocar Senha',
                            content: 'Sua senha é padrão, para a sua segurança, recomendamos que troque imediatamente sua senha.\n Para isso, basta clicar no  seu nome de usuário na barra opções e em seguida clicar em trocar senha.',
                            buttons: {
                            ok: function () {
                                procederLogin(site, username, password, response.data);
                                },
                            }
                        })
                    } else {
                        procederLogin(site, username, password, response.data);
                    }                    
                } else {
                    $.alert('Login Inválido!');
                }
            })
        }
    }

    procederLogin = function(site, username, password, usuario) {
        user.saveData(usuario);
        if ($scope.chkLembrar == true) {
            localStorage.setItem('usuario', username);
            localStorage.setItem('senha', password);
            localStorage.setItem('site', site);
            localStorage.setItem('schema', usuario['schema']);
            localStorage.setItem('checkbox', $scope.chkLembrar);
        } else {
            username = localStorage.getItem('usuario');
            senha = localStorage.getItem('senha');
            site = localStorage.getItem('site');
            schema = localStorage.getItem('schema');

            if (username && senha && site) {
                localStorage.removeItem('usuario');
                localStorage.removeItem('senha');
                localStorage.removeItem('site');
                localStorage.removeItem('schema');
            }
        }
        
        $location.path('/dashboard');
        $scope.$apply();
    }

    $scope.clickZap = function () {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            window.open('https://wa.me/55' + $scope.nu_whatsapp);
        } else {
            window.open('https://web.whatsapp.com/send?phone=55' + $scope.nu_whatsapp);
        }
    }
});