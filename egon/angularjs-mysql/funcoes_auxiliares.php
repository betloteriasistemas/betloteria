<?php

function converterTipoJogo($tipo_jogo)
{
    switch ($tipo_jogo) {
        case "B":
            return "BICHO";
        case "2":
            return "2 PRA 500";
        case "S":
            return "SENINHA";
        case "Q":
            return "QUININHA";
        case "L":
            return "LOTINHA";
        case "R":
            return "RIFA";
        case "U":
            return "SUPER SENA";            
        default:
            return "";
    }
}

function converterDescricaoSimplesJogo($registro)
{
	switch ($registro['tipo_jogo']) {
		case 'B':
		case '2':
			return $registro['hora_extracao'] . " - " . $registro['desc_hora'];
		case 'S':
		case 'Q':
		case 'L':
        case 'U':
			return $registro['concurso'];
		case 'R':
			return $registro['descricao'];
		default:
			return "";
	}
}

function converterDescricaoJogo($tipo_jogo, $data_jogo, $concurso, $hora_extracao, $desc_hora) {
    switch ($tipo_jogo) {
        case "B":
        case "2":
            return "EXTRAÇÃO: " . $hora_extracao . " " . $desc_hora;
        case "S":
        case "Q":
        case "L":
        case "U":
            return "CONCURSO: " . $concurso . " " . $data_jogo;
        default:
            return "";
    }
}

function converterDescricaoTipoJogo($tipo_jogo_bicho)
{
    switch($tipo_jogo_bicho) {
        case 'MS':
            return "MS - MILHAR SECA";
        case 'MC':
            return "MC - MILHAR COM CENTENA";
        case 'MI':
            return "MI - MILHAR INVERTIDA";
        case 'MCI':
            return "MCI - MILHAR COM CENTENA INVERTIDA";
        case 'C':
            return "C - CENTENA";
        case 'CI':
            return "CI - CENTENA INVERTIDA";
        case 'G':
            return "G - GRUPO";
        case 'DG':
            return "DG - DUQUE DE GRUPO 1 / 5";
        case 'DG6':
            return "DG6 - DUQUE DE GRUPO 6 / 10";
        case 'TG':
            return "TG - TERNO DE GRUPO 1 / 5";
        case 'TG10':
            return "TG10 - TERNO DE GRUPO 1 / 10";
        case 'TG6':
            return "TG6 - TERNO DE GRUPO 6 / 10";
        case 'TSE':
            return "TSE - TERNO DE SEQUÊNCIA";
        case 'TSO':
            return "TSO - TERNO DE SOMA";
        case 'TEX-A':
            return "TEX-A - TERNO ESPECIAL ABERTO";
        case 'TEX-F':
            return "TEX-F - TERNO ESPECIAL FECHADO";
        case 'QG':
            return "QG - QUINA DE GRUPO";
        case 'DZ':
            return "DZ - DEZENA";
        case 'DDZ':
            return "DDZ - DUQUE DE DEZENA 1 / 5";
        case 'DDZ6':
            return "DDZ6 - DUQUE DE DEZENA 6 / 10";
        case 'TDZ':
            return "TDZ - TERNO DE DEZENA 1 / 5";
        case 'TDZ10':
            return "TDZ10 - TERNO DE DEZENA 1 / 10";
        case 'TDZ6':
            return "TDZ6 - TERNO DE DEZENA 6 / 10";
        case 'TDZC':
            return "TDZC - TERNO DE DEZENA COMBINADO";
        case 'TGC':
            return "TGC - TERNO DE GRUPO COMBINADO";
        case 'PS':
            return "PS - PASSE SECO";
        case 'PS12':
            return "PS12 - PASSE SECO 1 / 2";
        case 'PC':
            return "PC - PASSE COMBINADO";
        case '5P100':
            return "5P100 - 5 PRA 100";
        default:
            return $tipo_jogo_bicho;
    }
}

function converterSimNao(string $sn)
{
    return (!$sn || $sn == 'N') ? 'NÃO' : 'SIM';
}

function converterData($data, $format = 'd/m/Y H:i:s')
{
    try {
        if (validateDate($data, $format)) {
            return $data;
        }
        $time = strtotime($data);
        return date($format, $time);
    } catch (Exception $e) {
        error_log($e);
        return '';
    }
}

function validateDate($date, $format = 'd/m/Y H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

function converterValorReal($val)
{
    if (empty($val)) {
        return 'R$ 0,00';
    }
    return 'R$ ' . number_format($val, 2, ',', '.');
}

function converterValorPercentual($val)
{
    if (empty($val)) {
        return '0.00%';
    }
    return number_format((float) $val, 2) . '%';
}

function converterDescricaoTipoLancamento($tipo)
{
    return !empty($tipo) && ($tipo == 'E' || $tipo == 'ENTRADA') ? 'ENTRADA' : 'SAÍDA';
}

function converterStatusJogo($status)
{
    switch ($status) {
        case "A":
            return "ABERTO";
        case "B":
            return "BLOQUEADO";
        case "C":
            return "CANCELADO";
        case "L":
            return "LIBERADO";
        case "P":
            return "PROCESSADO";
        case "V":
            return "PENDENTE DE VALIDAÇÃO";
        default:
            return "";
    }
}

function converterPerfil($perfil)
{
    if ($perfil) {
        switch ($perfil) {
            case "A":
                return "ADMINISTRADOR";
            case "G":
                return "GERENTE";
            case "C":
                return "CAMBISTA";
            default:
                return "";
        }
    }
    return "";
}

function returnIfDifferent($label, $valOriginal, $valAlterado)
{
    if (!(empty($valOriginal) && empty($valAlterado)) && strcmp($valOriginal, $valAlterado) != 0) {
        return $label .
            getStrike($valOriginal) .
            ' => ' . $valAlterado . '; ';
    }
    return '';
}

function ifStringNullReturn($val, $return)
{
    if ($val == 'null') {
        return $return;
    }
    return $val;
}

function getBold($label)
{
    return '<b>' . $label . '</b>';
}

function getStrike($label)
{
    return '<strike>' . $label . '</strike>';
}

function getQtdInversoes($tipo_jogo, $aposta) {
    switch ($tipo_jogo) {
        case "MI":
        case "CI":
        case "MCI":
            $arr = str_split($aposta);
            $digitos = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            foreach($arr as $a) {
                $digitos[$a]++;
            }
            $fatorDivisor = 1;
            foreach($digitos as $d) {
                $fatorDivisor*=fatorial($d);
            }
            if ($tipo_jogo == "MCI") {
                return 2*(fatorial(strlen($aposta))/$fatorDivisor);
            }
            return fatorial(strlen($aposta))/$fatorDivisor;
        case "DDZC":
        case "DGC":
            $dezenas = explode("-", $aposta);
            return fatorial(count($dezenas))/(fatorial(count($dezenas) - 2)*fatorial(2));
        case "TDZC":
        case "TGC":
            $ternos = explode("-", $aposta);
            return fatorial(count($ternos))/(fatorial(count($ternos) - 3)*fatorial(3));
        default: return 1;
    }
}

function fatorial($val) {
    if ($val == 0 || $val == 1) {
        return 1;
    }
    return $val * fatorial($val - 1);
}

function getLimiteValorBicho($tipo_jogo, $limites) {
    switch ($tipo_jogo) {
        case "MS":
        case "MC":
        case "MI":
        case "MCI":
            return $limites['MILHAR'];
        case "C":
        case "CI":
            return $limites['CENTENA'];
        case "G":
        case "5P100":
            return $limites['GRUPO'];
        case "DG":
        case "DG6":
        case "DGC":
            return $limites['DUQUE_GRUPO'];
        case "TG":
        case "TG10":
        case "TG6":
        case "TGC":
            return $limites['TERNO_GRUPO'];
        case "QG":
             return $limites['QUINA_GRUPO'];
        case "DZ":
            return $limites['DEZENA'];
        case "DDZ":
        case "DDZC":
        case "DDZC6":
            return $limites['DUQUE_DEZENA'];
        case "TDZ":
        case "TDZ10":
        case "TDZ6":
        case "TDZC":
            return $limites['TERNO_DEZENA'];
        case "PS":
            return 10000; 
        case "PS12":
            return 10000;
        case "PC":
            return 5000;
    }
}

function getNomeBanca($banca, $site, $area) {
    switch ($site) {
        case 2536:
            switch ($area) {
                case 618:
                case 620:
                    return '';
                case 621:
                    return 'Vai de Sorte';
            }
        default: return $banca;
    }
}