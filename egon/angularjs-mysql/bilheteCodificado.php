<?php

include "conexao.php";
include "funcoes_auxiliares.php";
header('Access-Control-Allow-Origin: *');

$pule = mysqli_real_escape_string($con, $_GET['id']);
$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);

if ($pule == "") {
    print "Informe o Pule";
    exit;
}

function printNomeBanca($banca, $cod_site, $area, $is2pra500) {
    $nome_banca = getNomeBanca($banca, $cod_site, $area);
    switch ($cod_site) {
        case 3010:
        case 6607:
            print "!AC~" . $nome_banca . "~True~False~False<br/>";
            break;
        case 2536:
            if (!$is2pra500) {
                print "!AC~" . $nome_banca . "~True~False~True<br/>";
            } else {
                print "!AC~" . $nome_banca . "~True~False~False<br/>";
            }
            break;
        default:
            print "!AC~" . $nome_banca . "~True~False~True<br/>";
            break;
    }
}

function getDescricaoTipoJogo($tipo_jogo_bicho){
    switch($tipo_jogo_bicho) {
        case 'MS':
            return "MS - MILHAR SECA";
        case 'MC':
            return "MC - MILHAR COM CENTENA";
        case 'MI':
            return "MI - MILHAR INVERTIDA";
        case 'MCI':
            return "MCI - MILHAR COM CENTENA INVERTIDA";
        case 'C':
            return "C - CENTENA";
        case 'CI':
            return "CI - CENTENA INVERTIDA";
        case 'G':
            return "G - GRUPO";
        case 'DG':
            return "DG - DUQUE DE GRUPO 1 / 5";
        case 'DG6':
            return "DG6 - DUQUE DE GRUPO 6 / 10";
        case 'TG':
            return "TG - TERNO DE GRUPO 1 / 5";
        case 'TG10':
            return "TG10 - TERNO DE GRUPO 1 / 10";
        case 'TG6':
            return "TG6 - TERNO DE GRUPO 6 / 10";
        case 'TGC':
            return "TGC - TERNO DE GRUPO COMBINADO";
        case 'TSE':
            return "TSE - TERNO DE SEQUÊNCIA";
        case 'TSO':
            return "TSO - TERNO DE SOMA";
        case 'TEX-A':
            return "TEX-A - TERNO ESPECIAL ABERTO";
        case 'TEX-F':
            return "TEX-F - TERNO ESPECIAL FECHADO";
        case 'QG':
            return "QG - QUINA DE GRUPO";
        case 'DZ':
            return "DZ - DEZENA";
        case 'DDZ':
            return "DDZ - DUQUE DE DEZENA 1 / 5";
        case 'DDZ6':
            return "DDZ6 - DUQUE DE DEZENA 6 / 10";
        case 'TDZ':
            return "TDZ - TERNO DE DEZENA 1 / 5";
        case 'TDZ10':
            return "TDZ10 - TERNO DE DEZENA 1 / 10";
        case 'TDZ6':
            return "TDZ6 - TERNO DE DEZENA 6 / 10";
        case 'TDZC':
            return "TDZC - TERNO DE DEZENA COMBINADO";
        case 'PS':
            return "PS - PASSE SECO";
        case 'PS12':
            return "PS12 - PASSE SECO 1 / 2";
        case 'PC':
            return "PC - PASSE COMBINADO";
        case '5P100':
            return "5P100 - 5 PRA 100";
        default:
            return $tipo_jogo_bicho;
    }
}

function getQtdLimiteDeNumeros($txt_aposta, $fonteGrande) {
    $qtdLimite = 0;
    $numeros = explode('-', str_replace(' ', '', $txt_aposta));
    if (count($numeros) > 0) {
        $tamanhoNumero = strlen($numeros[0]);
        if ($fonteGrande) {
            $qtdLimite = 3;
            if ($tamanhoNumero == 1 || $tamanhoNumero == 2) {
                $qtdLimite = 5;
            } else if ($tamanhoNumero == 3) {
                $qtdLimite = 4;
            }            
        } else {
            $qtdLimite = 6;
            if ($tamanhoNumero == 1 || $tamanhoNumero == 2) {
                $qtdLimite = 11;
            } else if ($tamanhoNumero == 3) {
                $qtdLimite = 8;
            }            
        }
    }
    return $qtdLimite;
}

function getLinhasDeApostas($txt_aposta, $fonteGrande) {
    $numeros = explode('-', str_replace(' ', '', $txt_aposta));
    $arrayNumerosApostados = array();
    if (count($numeros) > 0) {    
        $qtdLimite = getQtdLimiteDeNumeros($txt_aposta, $fonteGrande);  
        $linha = "";
        $cont = 0;
        foreach( $numeros as $numero ) {
            $cont++;
            if ($cont == 1) {
                $linha = $numero;
            } else if ($cont <= $qtdLimite) {
                $linha = $linha . "-" . $numero;
            } else {
                array_push($arrayNumerosApostados, $linha);
                $linha = $numero;
                $cont = 1;
            }
        }
        if ($linha != "") {
            array_push($arrayNumerosApostados, $linha);
        }
    }
    return $arrayNumerosApostados;
}

$query = "SELECT COUNT(*) > 0 AS tem_jogo_diferente_2pra500
            FROM bilhete bil
                INNER JOIN aposta apo ON (bil.cod_bilhete = apo.cod_bilhete)
                INNER JOIN jogo ON (apo.cod_jogo = jogo.cod_jogo)
            WHERE (bil.txt_pule = '$pule' OR bil.txt_pule_pai = '$pule')
            AND jogo.tipo_jogo in ('S','Q','L','B','R','U') ";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$tem_jogo_diferente_2pra500 = $row['tem_jogo_diferente_2pra500'];

$query = "  SELECT DATE_FORMAT(bil.data_bilhete, '%d/%m/%Y %H:%i:%S') data_bilhete,
     (select s.NOME_BANCA from site s where s.cod_site = bil.cod_site) NOME_BANCA,
     bil.cod_site,
     usu.nome cambista,
     usu.cod_area,
     bil.nome_apostador,
     bil.telefone_apostador,
     bil.txt_pule,
     bil.txt_pule_pai,
     apo.cod_aposta,
     apo.qtd_numeros,
     apo.txt_aposta,
     apo.valor_aposta,
     apo.possivel_retorno,
     apo.flg_sorte,
     apo.retorno5,
     apo.retorno4,
     apo.retorno3,
     apo.valor_ganho,
     apo.do_premio,
     apo.ao_premio,
     apo.tipo_jogo tipo_jogo_bicho,
     bil.numero_bilhete as numero_bilhete,
     case
         when apo.status = 'A' then 'Aberto'
         when apo.status = 'G' then 'Ganho'
         when apo.status = 'C' then 'Cancelado'
         when apo.status = 'P' then 'Perdido' end as status,
     case
         when bil.status_bilhete = 'A' then 'Aberto'
         when bil.status_bilhete = 'P' then 'Processado'
         when bil.status_bilhete = 'C' then 'Cancelado'
         when bil.status_bilhete = 'V' then 'Pendente de Validação' end as status_bilhete,
     DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') AS data_jogo,
     jogo.cod_jogo,
     concat(jogo.cod_jogo, 
            apo.tipo_jogo,
            (case
                when jogo.tipo_jogo IN ('B') then ''
                else apo.qtd_numeros end),
            (case
                when jogo.tipo_jogo IN ('B') then apo.do_premio
                else '' end),
            (case
                when jogo.tipo_jogo IN ('B') then apo.ao_premio
                else '' end)
            ) as registro_aposta,     
     cast(case
        when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
        when jogo.tipo_jogo = 'R' THEN concat(jogo.descricao, ' - ', DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'))
        else jogo.concurso end as char) as concurso,
     case
       when jogo.tipo_jogo = 'S' then 'SENINHA'
       when jogo.tipo_jogo = 'U' then 'SUPER SENA'
       when jogo.tipo_jogo = 'Q' then 'QUININHA'
       when jogo.tipo_jogo = 'L' then 'LOTINHA'
       when jogo.tipo_jogo = 'B' then 'JB'
       when jogo.tipo_jogo = '2' then '2 PRA 500'
       when jogo.tipo_jogo = 'R' then 'RIFA' else ''
       end tipo_jogo,
    case
       when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf.VALOR_APOSTA), ' PRA ', FLOOR(conf.VALOR_ACUMULADO))
       else ''
       end tipo_jogo_dinamico,
     LPAD(jogo.numero_1,4,'0') numero_1,
     LPAD(jogo.numero_2,4,'0') numero_2,
     LPAD(jogo.numero_3,4,'0') numero_3,
     LPAD(jogo.numero_4,4,'0') numero_4,
     LPAD(jogo.numero_5,4,'0') numero_5,
     LPAD(jogo.numero_6,3,'0') numero_6,
     LPAD(jogo.numero_7,3,'0') numero_7,
     LPAD(jogo.numero_8,4,'0') numero_8,
     LPAD(jogo.numero_9,4,'0') numero_9,
     LPAD(jogo.numero_10,4,'0') numero_10,
     LPAD(jogo.numero_11,4,'0') numero_11,
     LPAD(jogo.numero_12,4,'0') numero_12,
     LPAD(jogo.numero_13,4,'0') numero_13,
     LPAD(jogo.numero_14,4,'0') numero_14,
     LPAD(jogo.numero_15,4,'0') numero_15
     FROM bilhete bil
     inner join aposta apo on (bil.cod_bilhete = apo.cod_bilhete)
     inner join jogo on (apo.cod_jogo = jogo.cod_jogo)
     inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
     inner join configuracao_2pra500 conf on (usu.COD_AREA = conf.COD_AREA and usu.COD_SITE = conf.COD_SITE)
     where (bil.txt_pule = '$pule' or bil.txt_pule_pai = '$pule') ";

if ($tem_jogo_diferente_2pra500) {
    $query = $query . " group by TXT_PULE, TIPO_JOGO, COD_JOGO, QTD_NUMEROS, TIPO_JOGO_BICHO, COD_APOSTA, TXT_APOSTA, DO_PREMIO, AO_PREMIO ";
}

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

$bilhete_anterior = "";
$valor_total = 0;
$status_geral = 'Processado';

$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$area = 0;
while ($contador < mysqli_num_rows($result)) {

    $area = $row['cod_area'];
    $bilhete_anterior = $row['txt_pule'];
    $registro_aposta = $row['registro_aposta'];
    $row_array['data_bilhete'] = $row['data_bilhete'];
    $row_array['cod_site'] = $row['cod_site'];
    $row_array['nome_banca'] = $row['NOME_BANCA'];
    $row_array['cambista'] = $row['cambista'];
    $row_array['nome_apostador'] = $row['nome_apostador'];
    $row_array['telefone_apostador'] = $row['telefone_apostador'];
    $row_array['txt_pule'] = $row['txt_pule'];
    if ($row['txt_pule'] == $row['numero_bilhete']) {
        $row_array['txt_pule'] = $row['txt_pule_pai'];
    }
    $row_array['data_jogo'] = $row['data_jogo'];
    $row_array['concurso'] = $row['concurso'];
    $row_array['numero_bilhete'] = $row['numero_bilhete'];

    $tipoJogo = $row['tipo_jogo'];
    if ($tipoJogo == '2 PRA 500') {
        $row_array['valor_bilhete_2pra500'] = $row['valor_aposta'];
    } else {
        $row_array['valor_bilhete_2pra500'] = 0;
    }

    if ($row['numero_1'] == '') {
        $row_array['numeros_sorteados'] = '';
    } else {    
        switch ($tipoJogo) {
            case "2 PRA 500":
                $row_array['numeros_sorteados'] = $row['numero_1'];
            break;
            case "QUININHA":
                $row_array['numeros_sorteados'] = 
                substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                substr($row['numero_5'], strlen($row['numero_5']) - 2, 2);
            break;
            case "SENINHA":
            case "SUPER SENA":
                $row_array['numeros_sorteados'] = 
                substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                substr($row['numero_5'], strlen($row['numero_5']) - 2, 2) . ' - ' .
                substr($row['numero_6'], strlen($row['numero_6']) - 2, 2);
            break;
            case "LOTINHA":
                $row_array['numeros_sorteados'] = 
                substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                substr($row['numero_5'], strlen($row['numero_5']) - 2, 2) . ' - ' .
                substr($row['numero_6'], strlen($row['numero_6']) - 2, 2) . ' - ' .
                substr($row['numero_7'], strlen($row['numero_7']) - 2, 2) . ' - ' .
                substr($row['numero_8'], strlen($row['numero_8']) - 2, 2) . ' - ' .
                substr($row['numero_9'], strlen($row['numero_9']) - 2, 2) . ' - ' .
                substr($row['numero_10'], strlen($row['numero_10']) - 2, 2) . ' - ' .
                substr($row['numero_11'], strlen($row['numero_11']) - 2, 2) . ' - ' .
                substr($row['numero_12'], strlen($row['numero_12']) - 2, 2) . ' - ' .
                substr($row['numero_13'], strlen($row['numero_13']) - 2, 2) . ' - ' .
                substr($row['numero_14'], strlen($row['numero_14']) - 2, 2) . ' - ' .
                substr($row['numero_15'], strlen($row['numero_15']) - 2, 2) . ' - ';
            break;
            case "JB":
                $row_array['numeros_sorteados'] = 
                $row['numero_1'] . ' - ' . $row['numero_2'] . ' - ' . $row['numero_3'] . ' - ' . 
                $row['numero_4'] . ' - ' . $row['numero_5'] . ' - ' . $row['numero_6'] . ' - ' .
                $row['numero_7'];
            break;
            case "RIFA":
                $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 3, 3);
            break;
        }
    }

    $row_array['tipo_jogo'] = $row['tipo_jogo'];
    $row_array['tipo_jogo_dinamico'] = $row['tipo_jogo_dinamico'];

    $row_array['apostas'] = array();

    $valorBilhete = 0;
    $valorPossivelRetorno = 0;
    $flagSorte = 'N';
    $valorRetorno5 = 0;
    $valorRetorno4 = 0;
    $valorRetorno3 = 0; 
    $valorGanho = 0;       
    $statusApostas = 'Aberto';    

    do {
        $row_array_2['qtd_numeros'] = $row['qtd_numeros'];
        $row_array_2['txt_aposta'] = str_replace(' ', '', $row['txt_aposta']);
        $row_array_2['valor_aposta'] = $row['valor_aposta'];
        $row_array_2['possivel_retorno'] = $row['possivel_retorno'];
        $row_array_2['flg_sorte'] = $row['flg_sorte'];
        $row_array_2['retorno5'] = $row['retorno5'];
        $row_array_2['retorno4'] = $row['retorno4'];
        $row_array_2['retorno3'] = $row['retorno3'];
        $row_array_2['valor_ganho'] = $row['valor_ganho'];        
        $row_array_2['status'] = $row['status'];
        $row_array_2['do_premio'] = $row['do_premio'];
        $row_array_2['ao_premio'] = $row['ao_premio'];
        $row_array_2['tipo_jogo_bicho'] = $row['tipo_jogo_bicho'];

        $row_array_2['registro_aposta'] = array();

        if ($tem_jogo_diferente_2pra500) {         
            $valorApostaJogo = 0;   
            do {

                $valorBilhete = $valorBilhete + $row['valor_aposta'];
                $valorApostaJogo = $valorApostaJogo + $row['valor_aposta'];
                $valorPossivelRetorno = $valorPossivelRetorno + $row['possivel_retorno'];
                $flagSorte = $row['flg_sorte'];
                $valorRetorno5 = $valorRetorno5 + $row['retorno5'];
                $valorRetorno4 = $valorRetorno4 + $row['retorno4'];
                $valorRetorno3 = $valorRetorno3 + $row['retorno3'];
                $valorGanho = $valorGanho + $row['valor_ganho'];
                if ($row['status'] == 'Ganho') {
                    $statusApostas = 'Ganho';
                } else if ($statusApostas != 'Ganho' && $row['status'] == 'Perdido') {
                    $statusApostas = 'Perdido';
                } else if ($statusApostas != 'Ganho' && $statusApostas != 'Perdido' && $row['status'] == 'Cancelado') {
                    $statusApostas = 'Cancelado';
                }

                $row_array_3['qtd_numeros'] = $row['qtd_numeros'];
                $row_array_3['tipo_jogo_bicho'] = $row['tipo_jogo_bicho'];
                $row_array_3['txt_aposta'] = str_replace(' ', '', $row['txt_aposta']);
                $row_array_3['possivel_retorno'] = $row['possivel_retorno'];
                $row_array_3['valor_aposta'] = $row['valor_aposta'];
                $row_array_3['flg_sorte'] = $row['flg_sorte'];
                $row_array_3['retorno5'] = $row['retorno5'];
                $row_array_3['retorno4'] = $row['retorno4'];
                $row_array_3['retorno3'] = $row['retorno3'];
                array_push($row_array_2['registro_aposta'], $row_array_3);
                $registro_aposta = $row['registro_aposta'];

                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $contador = $contador + 1;                
                if ($contador == mysqli_num_rows($result)) {
                    break;
                }

            } while ($registro_aposta == $row['registro_aposta']); 
            $row_array_2['valor_aposta_jogo'] = $valorApostaJogo;
            array_push($row_array['apostas'], $row_array_2); 

        } else {
            array_push($row_array['apostas'], $row_array_2);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $contador = $contador + 1;
        }        

        if ($contador == mysqli_num_rows($result)) {
            break;
        }

    } while ($bilhete_anterior == $row['txt_pule']);

    $row_array['valor_bilhete'] = $valorBilhete;
    $row_array['valor_possivel_retorno'] = $valorPossivelRetorno;
    $row_array['flg_sorte'] = $flagSorte;
    $row_array['retorno5'] = $valorRetorno5;
    $row_array['retorno4'] = $valorRetorno4;
    $row_array['retorno3'] = $valorRetorno3;
    $row_array['valor_ganho'] = $valorGanho;
    $row_array['status_apostas'] = $statusApostas;

    $valor_total = $valor_total + $valorBilhete;

    if ($statusApostas == 'Aberto') {
        $status_geral = 'Aberto';
    }

    array_push($return_arr, $row_array);

}

if (count($return_arr) <= 0) {
    print "Registro não existe";
    exit;
}

$tabela_config = "configuracao";
switch ($return_arr[0]['tipo_jogo']) {
    case 'JB':
        $tabela_config = "configuracao_bicho";
    break; 
    case 'QUININHA':
        $tabela_config = "configuracao_quininha";
    break;
    case 'SUPER SENA':
        $tabela_config = "configuracao_super_sena";        
    break;
    case 'LOTINHA':
        $tabela_config = "configuracao_lotinha";
    break;
    case '2 PRA 500':
        $tabela_config = "configuracao_2pra500";
    break;
    case 'RIFA':
        $tabela_config = "configuracao_rifa";
    break;
}

$cod_site = $return_arr[0]['cod_site'];
$query = " select msg_bilhete from " . $tabela_config ." where cod_site = '$cod_site' and cod_area = '$area' ";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$mensagem_bilhete = $row['msg_bilhete'];

$query = " SELECT C.NU_BILHETE_NEGRITO, C.NU_BILHETE_GRANDE, C.NU_BILHETE_SUBLINHADO
            FROM CONFIGURACAO_GERAL C
                INNER JOIN USUARIO U ON (C.COD_AREA = U.COD_AREA)
            WHERE U.COD_USUARIO = '$cod_usuario' ";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$nu_bilhete_negrito = $row['NU_BILHETE_NEGRITO'] == 'S' ? '~True' : '~False';
$nu_bilhete_grande = $row['NU_BILHETE_GRANDE'] == 'S' ? '~True' : '~False';
$nu_bilhete_sublinhado = $row['NU_BILHETE_SUBLINHADO'] == 'S' ? '~True' : '~False';
$fonteGrande = $nu_bilhete_grande == '~True' ? true : false;

$nome_banca = $return_arr[0]['nome_banca'];
$data_bilhete = $return_arr[0]['data_bilhete'];
$colaborador = $return_arr[0]['cambista'];
$cliente = $return_arr[0]['nome_apostador'] . " - " . $return_arr[0]['telefone_apostador'];
$txt_pule = $return_arr[0]['txt_pule'];
$tipo_jogo = $return_arr[0]['tipo_jogo'];

if ($tem_jogo_diferente_2pra500) {
    print "!LF<br/>";
    printNomeBanca($nome_banca, $cod_site, $area, false);
    
    print "!LP<br/>";
    print "!AE~DATA: " . $data_bilhete . "~False~False~False<br/>";
    print "!AE~COLABORADOR: " . $colaborador . "~False~False~False<br/>";
    print "!AE~CLIENTE: " . $cliente . "~False~False~False<br/>";
    print "!LP<br/>";
}

foreach ($return_arr as $dados) {

    if ($tem_jogo_diferente_2pra500) {

        // IMPRESSÃO DE JOGOS DE MODALIDADE DIFERENTES DE 2 PRA 500 (MULTIJOGOS, SENINHA, QUININHA, ETC)
        if ($dados['tipo_jogo'] == '2 PRA 500' && $cod_site == 6638) {
            print "!AC~Bilhetinho Premiado~False~False~False<br/>";
        } else if ($dados['tipo_jogo'] == '2 PRA 500' && $cod_site != 6638) {
            print "!AC~" . $dados['tipo_jogo_dinamico'] . "~False~False~False<br/>";
        } else {
            if ($cod_site == 2536 && $dados['tipo_jogo'] == 'JB') {
                print "!AC~" . "~False~False~False<br/>";
            } else {
                print "!AC~" . $dados['tipo_jogo'] . "~False~False~False<br/>";
            }
        }
        print "!LP<br/>";

        if ($dados['tipo_jogo'] == 'JB' || $dados['tipo_jogo'] == '2 PRA 500' || $dados['tipo_jogo'] == 'RIFA' ){
            print "!AE~CONCURSO: " . $dados['concurso'] . "~False~False~False<br/>";
        }else{
            print "!AE~CONCURSO: " . $dados['concurso'] . " - " . $dados['data_jogo'] . "~False~False~False<br/>";
        }
        
        if ($dados['tipo_jogo'] != '2 PRA 500') {
            print "!AE~NÚMERO BILHETE: " . $dados['numero_bilhete'] . "~False~False~False<br/>";
        }
        if ($dados['numeros_sorteados'] != '') {
            print "!AE~NÚMEROS SORTEADOS:~False~False~False<br/>";
            print "!AE~" . $dados['numeros_sorteados'] . "~False~False~False<br/>";
        }        

        foreach ($dados['apostas'] as $e) {
            print "!LP<br/>";
            if ($dados['tipo_jogo'] != 'JB') {
                print "!AE~QTD. NÚMEROS:" . $e['qtd_numeros'] . "~False~False~False<br/>";
            } else {
                print "!AE~" . getDescricaoTipoJogo($e['tipo_jogo_bicho']) . " DO " . $e['do_premio'] . "º ao " . $e['ao_premio'] . "º" . "~False~False~False<br/>";                
            }            
                      
            foreach ($e['registro_aposta'] as $registro_aposta) {
                print "!LP<br/>";                                
                print "!AE~NÚMEROS APOSTADOS:" . "~False~False~False<br/>";
                $linhasDeAposta = getLinhasDeApostas($registro_aposta['txt_aposta'], $fonteGrande);
                foreach($linhasDeAposta as $linhaDeAposta) {
                    print "!AE~" . $linhaDeAposta . $nu_bilhete_negrito . $nu_bilhete_sublinhado . $nu_bilhete_grande . "<br/>";
                }                
                print "!AE~VALOR APOSTADO: R$ " . number_format($registro_aposta['valor_aposta'], 2, ',', '.') . "~False~False~False<br/>";
                if ($dados['tipo_jogo'] != 'RIFA' && $cod_site != 2536 && $cod_site != 6638) {
                    if ($registro_aposta['flg_sorte'] == 'N') {
                        print "!AE~POSSIVEL RETORNO: R$ " . number_format($registro_aposta['possivel_retorno'], 2, ',', '.') . "~False~False~False<br/>";
                    } else {
                        print "!AE~POSSIVEL RETORNO:~False~False~False<br/>";
                        if ($dados['tipo_jogo'] == 'QUININHA') {
                            print "!AE~R$" . number_format($registro_aposta['possivel_retorno'], 2, ',', '.') . "(5), R$" . number_format($registro_aposta['retorno4'], 2, ',', '.') . "(4), R$" . number_format($registro_aposta['retorno3'], 2, ',', '.') . "(3)~False~False~False<br/>";
                        } else {
                            print "!AE~R$" . number_format($registro_aposta['possivel_retorno'], 2, ',', '.') . "(6), R$" . number_format($registro_aposta['retorno5'], 2, ',', '.') . "(5), R$" . number_format($registro_aposta['retorno4'], 2, ',', '.') . "(4)~False~False~False<br/>";
                        }
                    }                     
                }
            }           
        }
        print "!LP<br/>";
        print "!AE~STATUS:" . $dados['status_apostas'] . "~False~False~False<br/>";
        if ($dados['status_apostas'] == 'Ganho') {
            print "!AE~RETORNO: R$ " . number_format($dados['valor_ganho'], 2, ',', '.') . "~False~False~False<br/>";
        }
        print "!LP<br/>";

    } else {

        // IMPRESSÃO APENAS DE BILHETES 2 PRA 500
        print "!LF<br/>";
        printNomeBanca($nome_banca, $cod_site, $area, true);
        
        print "!LP<br/>";
        print "!AE~DATA: " . $data_bilhete . "~False~False~False<br/>";
        print "!AE~COLABORADOR: " . $colaborador . "~False~False~False<br/>";
        print "!AE~CLIENTE: " . $cliente . "~False~False~False<br/>";
        print "!AE~CONCURSO: " . $dados['concurso'] . "~False~False~False<br/>";
        
        print "!AE~NÚMERO BILHETE: " . $dados['numero_bilhete'] . "~True~False~False<br/>";
        print "!AE~NÚMEROS SORTEADOS:~False~False~False<br/>";
        print "!AE~" . $dados['numeros_sorteados'] . "~False~False~False<br/>";
        print "!LP<br/>";
        if ($cod_site == 6638) {
            print "!AC~Bilhetinho Premiado~False~False~False<br/>";
        } else {
            print "!AC~" . $dados['tipo_jogo_dinamico'] . "~False~False~False<br/>";
        }
        print "!LP<br/>";
        foreach ($dados['apostas'] as $e) {
            print "!AE~QTD. NÚMEROS:" . $e['qtd_numeros'] . "~False~False~False<br/>";    
            print "!AE~NÚMEROS:~False~False~False<br/>";
            if ($fonteGrande) {
                print "!AE~" . $e['txt_aposta'] . $nu_bilhete_negrito . $nu_bilhete_sublinhado . $nu_bilhete_grande . "<br/>";
            } else {
                print "!AE~   " . $e['txt_aposta'] . $nu_bilhete_negrito . $nu_bilhete_sublinhado . $nu_bilhete_grande . "<br/>";
            }
            print "!AE~VALOR APOSTADO: R$ " . number_format($e['valor_aposta'], 2, ',', '.') . "~False~False~False<br/>";
            if ($cod_site != 6638) {
                print "!AE~POSSIVEL RETORNO: R$ " . number_format($e['possivel_retorno'], 2, ',', '.') . "~False~False~False<br/>";
            }
            print "!AE~STATUS:" . $e['status'] . "~False~False~False<br/>";
            print "!LP<br/>";
        }
        print "!LF<br/>";
        print "!AC~BILHETE:" . $dados['txt_pule'] . "~True~False~False<br/>";
        print "!AC~VALOR BILHETE: R$ " . number_format($dados['valor_bilhete_2pra500'], 2, ',', '.') . "~True~False~False<br/>";
        print "!LF<br/>";        

        print "!AE~" . $mensagem_bilhete . "~False~False~False<br/>";
        print "!LF<br/>";
        print "!LP<br/>";
        print "!LF<br/>";
        print "!LF<br/>";
        print "!LF<br/>";       
    
    }

}

if ($tem_jogo_diferente_2pra500) {
    print "!LF<br/>";
    print "!AC~BILHETE:" . $txt_pule . "~True~False~False<br/>";
    print "!AC~VALOR BILHETE: R$ " . number_format($valor_total, 2, ',', '.') . "~True~False~False<br/>";
    print "!LF<br/>"; 

    print "!AE~" . $mensagem_bilhete . "~False~False~False<br/>";
    print "!LF<br/>";
    print "!LP<br/>";
    print "!LF<br/>";
    print "!LF<br/>";
    print "!LF<br/>";    
}


$con->close();
