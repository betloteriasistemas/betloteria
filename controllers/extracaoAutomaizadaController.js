angular.module('main').controller('extracaoAutomatizadaCtrl', function ($scope, $http, configGeral, 
  user, $route, notificacoes, $rootScope) {
  $rootScope.loading = false;
  $scope.tiposJogos = [];
  $scope.desc_2pra500 = '2 PRA 500';
  $scope.edtHabilitaCentena = false;
  configGeral.get().then(function (data) {
      $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
      if (data.flg_bicho) {
          $scope.tiposJogos.push({nome: 'Bicho', codigo: 'B'});
      }
      if (data.flg_2pra500) {
          $scope.tiposJogos.push({nome: $scope.desc_2pra500, codigo: "2"});
      }
  });

  notificacoes.get().then(function (data) {
    $scope.notificacoes = data;
  });

  $scope.isEditando = false;
  $scope.editandoExtracao = {};
  $scope.comboBanca = "";
  $scope.comboHorario = "";
  $scope.comboBloqueio = "0";

  $scope.getDescTipoJogo = function (tipo_jogo) {
    if (tipo_jogo == 'B') {
        return 'BICHO';
    } else if (tipo_jogo == '2') {
        return $scope.desc_2pra500;
    }
  }

  $scope.getDescHora = function (horario) {
    if (horario != undefined && horario.length == 8) {
      return horario.substring(0,5);
    }
    return horario;
  }

  $scope.getDescSimNao = function (variavel) {
    if (variavel === "S") {
        return "SIM";
    } else {
        return "NÃO";
    }
  };

  $scope.haPeloMenosUmDiaSelecionado = function() {
    return $scope.edtCheckDomingo || 
          $scope.edtCheckSegunda ||
          $scope.edtCheckTerca ||
          $scope.edtCheckQuarta ||
          $scope.edtCheckQuinta ||
          $scope.edtCheckSexta ||
          $scope.edtCheckSabado;
  }

  $scope.salvar = function() {
    
    var banca = $scope.comboBanca;
    if (!$scope.isEditando && (banca == undefined || banca == '')) {
      return $.alert('Informe uma banca!');
    }

    var horario = $scope.comboHorario;
    if (!$scope.isEditando && (horario == undefined || horario == '')) {
      return $.alert('Informe um horário!');
    }

    var tipo_jogo = $scope.comboTipoJogo;
    if (tipo_jogo == undefined || tipo_jogo == '') {
      return $.alert('Informe o tipo de jogo!');
    }
    
    if (!$scope.haPeloMenosUmDiaSelecionado()) {
      return $.alert('Selecione pelo menos um dia da semana o tipo de jogo!');
    }

    var nomeBanca = $("#nomeBanca")[0].selectedOptions[0].label;
    var descHorario = $("#horario")[0].selectedOptions[0].label + ":00";
    if ($scope.isEditando) {
      nomeBanca = $scope.editandoExtracao.nome_banca;
      descHorario = $scope.editandoExtracao.horario + ":00";
    }
    
    var bancaCadastrada = haBancaCadastrada(nomeBanca, descHorario, tipo_jogo); 
    if (bancaCadastrada && !$scope.isEditando) {
      return $.alert('Banca e horário já cadastrados, verifique sua lista de extrações automatizadas.')
    }
    
    $rootScope.loading = true;

    var extracao = {
      site: user.getSite(),
      descricao: nomeBanca,
      horario: descHorario,
      tipoJogo: $scope.comboTipoJogo,
      quantidade_premios: $scope.quantidadePremios,
      milhar: unparseValorMonetario($scope.edtMilhar),
      centena: unparseValorMonetario($scope.edtCentena),
      grupo: unparseValorMonetario($scope.edtGrupo),
      duque_grupo: unparseValorMonetario($scope.edtDuqueGrupo),
      terno_grupo: unparseValorMonetario($scope.edtTernoGrupo),
      quina_grupo: unparseValorMonetario($scope.edtQuinaGrupo),
      dezena: unparseValorMonetario($scope.edtDezena),
      duque_dezena: unparseValorMonetario($scope.edtDuqueDezena),
      terno_dezena: unparseValorMonetario($scope.edtTernoDezena),
      domingo: $scope.edtCheckDomingo,
      segunda: $scope.edtCheckSegunda,
      terca: $scope.edtCheckTerca,
      quarta: $scope.edtCheckQuarta,
      quinta: $scope.edtCheckQuinta,
      sexta: $scope.edtCheckSexta,
      sabado: $scope.edtCheckSabado,
      prazo_bloqueio: $scope.comboBloqueio,
      habilita_centena: $scope.edtHabilitaCentena
    };

    $http({
        url: 'angularjs-mysql/extracaoAutomatizada.php',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: "operacao=" + "salvar" 
        + "&extracao=" + JSON.stringify(extracao)
        + "&schema=" + user.getSchema()
        + "&cod_usuario=" + user.getCodigo()
    }).then(function (response) {
      if(response.data[0].status == 'OK') {
        $.alert('Extração salva com sucesso');
        $scope.isEditando = false;
        $route.reload();
      } else {
        $.alert('Aconteceu um erro inesperado ao cadastrar essa extração');
      }
      $scope.comboBloqueio = "0";
    }).catch(function (response) {            
      $.alert('Erro no $HTTP: ' + response.status)
    }).finally(function() {
      $rootScope.loading = false;
    });
    
  }

  function haBancaCadastrada(nomeBanca, descHorario, tipoJogo) {
    for (var x = 0; x < $scope.extracoesCadastradas.length; x++) {
      if ($scope.extracoesCadastradas[x].descricao == nomeBanca &&
          $scope.extracoesCadastradas[x].hora_extracao == descHorario &&
          $scope.extracoesCadastradas[x].tipo_jogo == tipoJogo) {
            return true;
      }
    }
    return false;
  }
  
  $scope.setBancaARemover = function(banca) {
    $scope.bancaARemover = banca;
  }

  //Metodo responsavel por editar uma extracao
  $scope.editarExtracao = function(bancaCadastrada) {
    $scope.isEditando = true;
    $scope.quantidadePremios = bancaCadastrada.qtd_premios;
    $scope.comboTipoJogo = bancaCadastrada.tipo_jogo + '';
    $scope.edtMilhar = parseValorMonetario(bancaCadastrada.milhar);
    $scope.edtCentena = parseValorMonetario(bancaCadastrada.centena);
    $scope.edtGrupo = parseValorMonetario(bancaCadastrada.grupo);
    $scope.edtDuqueGrupo = parseValorMonetario(bancaCadastrada.duque_grupo);
    $scope.edtTernoGrupo = parseValorMonetario(bancaCadastrada.terno_grupo);
    $scope.edtQuinaGrupo = parseValorMonetario(bancaCadastrada.quina_grupo);
    $scope.edtDezena = parseValorMonetario(bancaCadastrada.dezena);
    $scope.edtDuqueDezena = parseValorMonetario(bancaCadastrada.duque_dezena);
    $scope.edtTernoDezena = parseValorMonetario(bancaCadastrada.terno_dezena);
    $scope.edtCheckDomingo = bancaCadastrada.domingo == 'S';
    $scope.edtCheckSegunda = bancaCadastrada.segunda == 'S';
    $scope.edtCheckTerca = bancaCadastrada.terca == 'S';
    $scope.edtCheckQuarta = bancaCadastrada.quarta == 'S';
    $scope.edtCheckQuinta = bancaCadastrada.quinta == 'S';
    $scope.edtCheckSexta = bancaCadastrada.sexta == 'S';
    $scope.edtCheckSabado = bancaCadastrada.sabado == 'S';
    $scope.editandoExtracao.nome_banca = bancaCadastrada.descricao;
    $scope.editandoExtracao.horario = $scope.getDescHora(bancaCadastrada.hora_extracao);
    $scope.comboBloqueio = bancaCadastrada.prazo_bloqueio + "";
    $scope.temDomingo = bancaCadastrada.tem_domingo == 'S';
    $scope.edtHabilitaCentena = bancaCadastrada.habilita_centena == 'S';
  }

  //Metodo responsavel por limpar campos
  $scope.cancelar = function() {
    $scope.comboHorario = '';
    $scope.comboBanca = '';
    $scope.comboTipoJogo = '';
    $scope.quantidadePremios = '';
    $scope.edtCheckDomingo = true;
    $scope.temDomingo = true;
    $scope.edtCheckSegunda = true;
    $scope.edtCheckTerca = true;
    $scope.edtCheckQuarta = true;
    $scope.edtCheckQuinta = true;
    $scope.edtCheckSexta = true;
    $scope.edtCheckSabado = true;
    $scope.bancaEHorarioSelecionados = false;
    $scope.isEditando = false;
    $scope.edtHabilitaCentena = false;
  }

  //Metodo responsavel por remover a extracao (inativar) do usuario
  $scope.removerExtracao = function() {
    $rootScope.loading = true;
    var cod_extracao = $scope.bancaARemover.cod_extracao;

    $http({
        url: 'angularjs-mysql/extracaoAutomatizada.php',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: "operacao=" + "excluir" 
        + "&cod_extracao=" + cod_extracao
        + "&cod_usuario=" + user.getCodigo()
        + "&site=" + user.getSite()
        + "&schema=" + user.getSchema()
    }).then(function (response) {
      $route.reload();
    }).catch(function (response) {            
      $.alert('Erro no $HTTP: ' + response.status)
    }).finally(function() {
      $rootScope.loading = false;
    });
  }

  $scope.getExtracoes = function() {
    $rootScope.loading = true;
    $http(
      {
        method: "POST",
        url: "angularjs-mysql/extracaoAutomatizada.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: "operacao=" + "listar_extracoes" +
              "&schema=" + user.getSchema()
      }
    ).then(function(response) {
      $scope.extracoes = response.data;
    }).catch(function (response) {            
      $.alert('Erro no $HTTP: ' + response.status)
    }).finally(function() {
      $rootScope.loading = false;
    });
  }

  $scope.getHorarios = function() {
    $rootScope.loading = true;
    $scope.comboHorario = "";
    var cod_extracao = $scope.comboBanca;
    $http(
      {
        method: "POST",
        url: "angularjs-mysql/extracaoAutomatizada.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: "operacao=listar_horarios_banca"
          + "&cod_extracao=" + cod_extracao
          + "&schema=" + user.getSchema()
      }
    ).then(function(response) {
      $scope.horarios = response.data;
    }).catch(function (response) {            
      $.alert('Erro no $HTTP: ' + response.status)
    }).finally(function() {
      $rootScope.loading = false;
    });
  }

  $scope.bancaEHorarioSelecionados = false;
  $scope.setBancaEHorarioSelecionados = function() {
    $scope.bancaEHorarioSelecionados = $scope.comboBanca != "" && $scope.comboHorario != "";
  }

  $scope.edtCheckDomingo = true;
  $scope.temDomingo = true;
  $scope.edtCheckSegunda = true;
  $scope.edtCheckTerca = true;
  $scope.edtCheckQuarta = true;
  $scope.edtCheckQuinta = true;
  $scope.edtCheckSexta = true;
  $scope.edtCheckSabado = true;
  $scope.getDiasDaSemana = function() {
    $rootScope.loading = true;
    var cod_extracao = $scope.comboBanca;
    var cod_hora_extracao = $scope.comboHorario;
    $http(
      {
        method: "POST",
        url: "angularjs-mysql/extracaoAutomatizada.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: "operacao=listar_dias"
          + "&cod_extracao=" + cod_extracao
          + "&cod_hora_extracao=" + cod_hora_extracao
          + "&schema=" + user.getSchema()
      }
    ).then(function(response) {
      $scope.diasDaSemana = response.data[0];
      $scope.temDomingo = $scope.diasDaSemana['temDomingo'];
      if (!$scope.temDomingo) {
        $scope.edtCheckDomingo = false;
      }
    }).catch(function (response) {            
      $.alert('Erro no $HTTP: ' + response.status)
    }).finally(function() {
      $rootScope.loading = false;
    });    
  }

  $scope.setDomingo = function() {
    if ($scope.edtCheckDomingo) {
      $scope.edtCheckDomingo = false;
    } else {
      $scope.edtCheckDomingo = true;
    }
  }

  $scope.setSegunda = function() {
    if ($scope.edtCheckSegunda) {
      $scope.edtCheckSegunda = false;
    } else {
      $scope.edtCheckSegunda = true;
    }
  }
  
  $scope.setTerca = function() {
    if ($scope.edtCheckTerca) {
      $scope.edtCheckTerca = false;
    } else {
      $scope.edtCheckTerca = true;
    }
  }  

  $scope.setQuarta = function() {
    if ($scope.edtCheckQuarta) {
      $scope.edtCheckQuarta = false;
    } else {
      $scope.edtCheckQuarta = true;
    }
  }
  
  $scope.setQuinta = function() {
    if ($scope.edtCheckQuinta) {
      $scope.edtCheckQuinta = false;
    } else {
      $scope.edtCheckQuinta = true;
    }
  }
  
  $scope.setSexta = function() {
    if ($scope.edtCheckSexta) {
      $scope.edtCheckSexta = false;
    } else {
      $scope.edtCheckSexta = true;
    }
  }
  
  $scope.setSabado = function() {
    if ($scope.edtCheckSabado) {
      $scope.edtCheckSabado = false;
    } else {
      $scope.edtCheckSabado = true;
    }
  }  

  //Metodo responsavel por buscar todas as extracoes automatizadas de um usuario
  //esse metodo faz o agrupamento das extracoes com base em sua descricao 
  //Ex: [PT-RIO: {dados...}, PT-SP: {dados...}, PT-Bahia: {dados...}],
  $scope.getExtracoesUsuario = function() {    
    var nomeBanca = '';
    if ($scope.comboBanca != 0) {
      nomeBanca = $("#nomeBanca")[0].selectedOptions[0].label;
    }
    $http(
      {
        method: "POST",
        url: "angularjs-mysql/extracaoAutomatizada.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: "operacao=listar_extracoes_usuario" +
              '&cod_site=' + user.getSite() +
              '&desc_extracao=' + nomeBanca +
              '&schema=' + user.getSchema()
      }
    ).then(function(response) {
      $scope.extracoesCadastradas = response.data;      
    }).catch(function (response) {            
      $.alert('Erro no $HTTP: ' + response.status)
    }).finally(function() {

    });
  }

  parseValorMonetario = function (valor) {
    return valor ? parseFloat(valor).toFixed(2).replace('.', ',').replace(/\d(?=(\d{3})+\,)/g, '$&.') : 0;
  }
  
  unparseValorMonetario = function (valor) {
    return valor ? valor.replace(/\./g, '').replace(',', '.') : 0;
  }

});