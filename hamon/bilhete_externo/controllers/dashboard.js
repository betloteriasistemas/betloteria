angular.module('main').controller('dashboardCtrl', function ($http, $routeParams, user) {

    var currentId = $routeParams.id;
    console.log('DASHBOARD: ' + currentId);

    $http({
        method: "POST",
        url: "angularjs-mysql/conexao_login.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: '&site=' + currentId + '&operacao=geral'
    }).then(function (response) {
        if (typeof currentId != 9999) {
            let modalidade = response.data['modalidade_padrao_bv'];
            user.saveData(response.data);       
            localStorage.setItem('schema', response.data['schema']);
            window.open('#!/'+ modalidade + '/' + currentId, '_self');
        } else {
            window.open('#!/seninha', '_self');
        }
    }).catch(function (response) {
        deferred.resolve(response);
        alert('Erro no $HTTP: ' + response.status)
    });
});