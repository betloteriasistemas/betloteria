<?php

include "conexao.php";
require_once 'vendor/autoload.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

if (isset($_POST["operacao"])) {
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);
} else {
    $operacao = mysqli_real_escape_string($con, $_GET['operacao']);
}

if ($operacao == "listar") {
    
    $site = mysqli_real_escape_string($con, $_POST['site']);
    $dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
    $dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);

    $query = "select  cod_prestacao_conta, 
                      DATE_FORMAT(data_prestacao_conta, '%d/%m/%Y') data_prestacao_conta
              from prestacao_conta
              where cod_site = '$site' ";

    if (!is_null($dataDe)) {
        $query = $query . " AND DATE_FORMAT(data_prestacao_conta, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
    }

    if (!is_null($dataAte)) {
        $query = $query . " AND DATE_FORMAT(data_prestacao_conta, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
    }

    if (is_null($dataDe) && is_null($dataAte)) {
        $query . " AND AND DATE_FORMAT(data_prestacao_conta, '%Y%m%d') >= DATE_FORMAT(current_date, '%Y%m%d')
                    AND AND DATE_FORMAT(data_prestacao_conta, '%Y%m%d') <= DATE_FORMAT(current_date, '%Y%m%d') " ;
    }

    $result = mysqli_query($con, $query);
    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_prestacao_conta'] = $row['cod_prestacao_conta'];
        $row_array['data_prestacao_conta'] = $row['data_prestacao_conta'];
        array_push($return_arr, $row_array);
    };
    echo json_encode($return_arr);

} else if ($operacao == "gerarRelatorio") {

    $cod_prestacao_conta = mysqli_real_escape_string($con, $_GET['cod_prestacao_conta']);
    $data_prestacao_conta = mysqli_real_escape_string($con, $_GET['data_prestacao_conta']);
    $tipoRelatorio = mysqli_real_escape_string($con, $_GET['tipoRelatorio']);

    $query = "select  html_prestacao_conta
              from prestacao_conta
              where cod_prestacao_conta = '$cod_prestacao_conta'";
    $result = mysqli_query($con, $query);
    
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $html = $row['html_prestacao_conta'];
    // printf($html);
    if ($tipoRelatorio == 'xlsx') {
        $arquivo = $data_prestacao_conta . "_Relatorio_Prestacao_de_Contas.xls";
        header ("Expires: Mon, 18 Nov 1985 18:00:00 GMT");
        header ("Last-Modified: ".gmdate("D,d M YH:i:s")." GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-type: application/x-msexcel");
        header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");	
        header ("Content-Type: text/html; charset=UTF-8");
        echo $html;    
    } else if ($tipoRelatorio == 'pdf') {
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 
                                'orientation' => 'L', 
                                'margin_left' => '5',
                                'margin_right' => '5',
                                'margin_bottom' => '1',
                                'margin_top' => '2']);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output($data_prestacao_conta . "_Relatorio_Prestacao_de_Contas.pdf", "D");
    }
}

$con->close();
