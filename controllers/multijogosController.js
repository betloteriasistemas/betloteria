angular.module('main').controller('multijogosCtrl', function ($rootScope, $scope, $q, user, $http, $filter, $location) {    
    $rootScope.loading = true;
    $scope.comboConcurso = [];
    $scope.filtroJogosBicho = {"MS":false,
                               "MC":false,
                               "MI":false,
                               "MCI":false,
                               "C":false,
                               "CI":false,
                               "G":false,
                               "DG":false,
                               "DGC":false};     
    $scope.filtroNumerosSeninha = {"14":false,"15":false,"16":false,"17":false,"18":false,"19":false,"20":false,"25":false,"30":false,"35":false,"40":false};                               
    $scope.filtroNumerosQuininha = {"13":false,"14":false,"15":false,"16":false,"17":false,"18":false,"19":false,"20":false,"25":false,"30":false,"35":false,"40":false,"45":false};
    $scope.filtroNumerosLotinha = {"16":false,"17":false,"18":false,"19":false,"20":false,"21":false,"22":false};
    $scope.cartoes = [];
    $scope.chkRegistroCliente = false;
    $scope.clientes = [];

    $q.all([
        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "site=" + user.getSite() 
                + "&cod_area=" + user.getArea() 
                + "&operacao=multijogos"
                + '&schema=' + user.getSchema()
        }),
        $http({
            method: "POST",
            url: "angularjs-mysql/clientes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo()
                + '&operacao=listar'
                + '&schema=' + user.getSchema()
        })
    ]).then(function (response) {
        $scope.listaCfgMulti = response[0].data;
        $scope.valor_minimo_aposta = parseValorMonetario($scope.listaCfgMulti[0].valor_minimo_aposta);
        $scope.valor_cartao_1 = $scope.listaCfgMulti[0].valor_cartao_1;
        $scope.valor_cartao_2 = $scope.listaCfgMulti[0].valor_cartao_2;
        $scope.valor_cartao_3 = $scope.listaCfgMulti[0].valor_cartao_3;
        $scope.valor_cartao_4 = $scope.listaCfgMulti[0].valor_cartao_4;
        $scope.valor_cartao_5 = $scope.listaCfgMulti[0].valor_cartao_5;
        $scope.filtroAcertoSeninha = $scope.montaFiltro($scope.listaCfgMulti[0].qtd_numeros_seninha, $scope.filtroNumerosSeninha);
        $scope.filtroAcertoQuininha = $scope.montaFiltro($scope.listaCfgMulti[0].qtd_numeros_quininha, $scope.filtroNumerosQuininha);
        $scope.filtroAcertoLotinha = $scope.montaFiltro($scope.listaCfgMulti[0].qtd_numeros_lotinha, $scope.filtroNumerosLotinha);                                          
        $scope.filtroTipoJogoBicho = $scope.montaFiltro($scope.listaCfgMulti[0].tipos_jogos_bicho, $scope.filtroJogosBicho);
        $scope.concursos_marcados = $scope.listaCfgMulti[0].concursos_marcados;
        
        $scope.cartoes[0] = {"valor":$scope.valor_cartao_1, "selecionado":true};
        $scope.cartoes[1] = {"valor":$scope.valor_cartao_2, "selecionado":false};
        $scope.cartoes[2] = {"valor":$scope.valor_cartao_3, "selecionado":false};
        $scope.cartoes[3] = {"valor":$scope.valor_cartao_4, "selecionado":false};
        $scope.cartoes[4] = {"valor":$scope.valor_cartao_5, "selecionado":false};

        $scope.valorCartaoInteiro = $scope.valor_cartao_1;
        var numero = parseFloat($scope.valorCartaoInteiro).toFixed(2).split('.');
        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
        $scope.valorCartao = numero.join(',');
        $('#valorCartao').prop("value",  $scope.valorCartao);
        $('#valorCartaoInteiro').prop("value",  $scope.valorCartaoInteiro);

        $scope.clientes = response[1].data;
        $rootScope.loading = false;
    }).catch(angular.noop);
    
    $scope.filtrarCliente = function(termo) {
        return $scope.clientes.filter(cliente => cliente.nome.toLowerCase().indexOf(termo)>-1);
    }

    $scope.onSelectCliente = function(cliente) {
        if (cliente) {
            $scope.edtTelefone = cliente.telefone;
            document.getElementById("chkRegistrarCliente").focus();
        } else {
            $scope.edtTelefone = null;
        }
    }

    $scope.montaFiltro = function(tipos_jogos_bicho, filtroRetorno) {       

        var tipos_jogos_arr = tipos_jogos_bicho.split("-");
        for (var k=0; k < tipos_jogos_arr.length; k++) {
            filtroRetorno[tipos_jogos_arr[k]] = true;
        }           
        return filtroRetorno;
    }

    parseValorMonetario = function (valor, valRetorno = 0) {
        return valor ? parseFloat(valor).toFixed(2).replace('.', ',').replace(/\d(?=(\d{3})+\,)/g, '$&.') : valRetorno;
    }

    unparseValorMonetario = function (valor, valRetorno = 0) {
        return valor ? valor.replace(/\./g, '').replace(',', '.') : valRetorno;
    }

    $scope.edtNome = "";
    $scope.edtTelefone = "";
    var data = new Date;
    $scope.edtDataDe2pra500 = data;
    $scope.edtDataDeBicho = data;
    var hora = data.getHours();
    if (hora >= 20) {
        data.setDate(data.getDate() + 1);
        $scope.edtDataDe2pra500 = data;
        $scope.edtDataDeBicho = data;
    }

    $scope.imprimeNumeros = function(arrayNumeros) {
        var numeros = '';
        for (var i=0; i < arrayNumeros.length; i++ ) {
            if (i == 0) {
                numeros = arrayNumeros[i];
            } else {
                numeros += '-' + arrayNumeros[i];
            }
        }
        return numeros;

    }

    $scope.geraNumerosRandomicos = function(qtdNumeros, numMax) {
        var contador = 0;
        var arrayPintados = [];
        while (contador < qtdNumeros) {
            var randomico = Math.floor(Math.random() * numMax) + 1;
            while (arrayPintados.find(element => element == randomico) != undefined) {
                randomico = Math.floor(Math.random() * numMax) + 1;
            }
            arrayPintados.push(randomico);
            contador++;
        }
        return $scope.imprimeNumeros(arrayPintados.sort(function(a, b){return a-b}));
    }

    $scope.recuperaFiltrosSeleionados = function(filtroMin, filtroMax, arrayFiltros) {
        var filtros = [];
        var cont = 0;
        for (var i = filtroMin; i <=filtroMax; i++) {
            if (arrayFiltros[i]) {
                filtros[cont++] = i;
            }
        }
        return filtros;
    }

    $scope.gerarBilhetes = function() {

        if ($scope.checkSeninha) {
            if ($scope.concursosSeninha.length == 0 && $scope.codConcursoSeninha != 0) {                
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogosSeninha.length; i++) {
                        $scope.concursosSeninha[i] = $scope.jogosSeninha[i].cod_jogo;
                    }
                } else {
                    $scope.concursosSeninha[0] = $scope.codConcursoSeninha;
                }
            }
    
            if ($scope.concursosSeninha.length == 0) {
                $.alert('Não foi selecionado nenhum concurso na Seninha!');
                return;
            }
    
            var filtros = $scope.recuperaFiltrosSeleionados(14, 40, $scope.filtroAcertoSeninha);
            if (filtros.length == 0) {
                $.alert('Não foi selecionada a quantidade de números na Seninha!');
                return;
            }            
        }

        if ($scope.checkSuperSena) {
            if ($scope.concursosSuperSena.length == 0 && $scope.codConcursoSuperSena != 0) {                
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogosSuperSena.length; i++) {
                        $scope.concursosSuperSena[i] = $scope.jogosSuperSena[i].cod_jogo;
                    }
                } else {
                    $scope.concursosSuperSena[0] = $scope.codConcursoSuperSena;
                }
            }
    
            if ($scope.concursosSuperSena.length == 0) {
                $.alert('Não foi selecionado nenhum concurso na Super Sena!');
                return;
            }           
        }        

        if ($scope.checkQuininha) {
            if ($scope.concursosQuininha.length == 0 && $scope.codConcursoQuininha != 0) {
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogosQuininha.length; i++) {
                        $scope.concursosQuininha[i] = $scope.jogosQuininha[i].cod_jogo;
                    }
                } else {
                    $scope.concursosQuininha[0] = $scope.codConcursoQuininha;
                }                
            }
    
            if ($scope.concursosQuininha.length == 0) {
                $.alert('Não foi selecionado nenhum concurso na Quininha!');
                return;
            }
    
            var filtros = $scope.recuperaFiltrosSeleionados(13, 45, $scope.filtroAcertoQuininha);
            if (filtros.length == 0) {
                $.alert('Não foi selecionada a quantidade de números na Quininha!');
                return;
            }
        }

        if ($scope.checkLotinha) {
            if ($scope.concursosLotinha.length == 0 && $scope.codConcursoLotinha != 0) {
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogosLotinha.length; i++) {
                        $scope.concursosLotinha[i] = $scope.jogosLotinha[i].cod_jogo;
                    }
                } else {
                    $scope.concursosLotinha[0] = $scope.codConcursoLotinha;
                }                
            }
    
            if ($scope.concursosLotinha.length == 0) {
                $.alert('Não foi selecionado nenhum concurso na Lotinha!');
                return;
            }
    
            var filtros = $scope.recuperaFiltrosSeleionados(16, 22, $scope.filtroAcertoLotinha);
            if (filtros.length == 0) {
                $.alert('Não foi selecionada a quantidade de números na Lotinha!');
                return;
            }
        }

        if ($scope.checkRifa) {
            if ($scope.concursosRifa.length == 0 && $scope.codConcursoRifa != 0) {
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogosRifa.length; i++) {
                        $scope.concursosRifa[i] = $scope.jogosRifa[i].cod_jogo;
                    }
                } else {                
                    $scope.concursosRifa[0] = $scope.codConcursoRifa;
                }
            }
    
            if ($scope.concursosRifa.length == 0) {
                $.alert('Não foi selecionado nenhum concurso na Rifa!');
                return;
            }
        }

        if ($scope.check2para500) {
            var valorAposta = parseInt($scope.c2pra500.valor_aposta);
            var valorAcumulado = parseInt($scope.c2pra500.valor_acumulado);
            if ($scope.extracoes2pra500.length == 0 && $scope.codExtracao2pra500 != 0) {
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogos2pra500.length; i++) {
                        $scope.extracoes2pra500[i] = $scope.jogos2pra500[i].cod_jogo;
                    }
                } else {  
                    $scope.extracoes2pra500[0] = $scope.codExtracao2pra500;
                }
            }
    
            if ($scope.extracoes2pra500.length == 0) {
                $.alert('Não foi selecionada nenhuma extração no ' + valorAposta 
                    + ' pra ' + valorAcumulado + '!');
                return;
            }
        }

        if ($scope.checkJogoDoBicho) {
            if ($scope.extracoesBicho.length == 0 && $scope.codExtracaoBicho != 0) {
                if ($scope.concursos_marcados == 'T') {
                    for (var i = 0; i < $scope.jogosBicho.length; i++) {
                        $scope.extracoesBicho[i] = $scope.jogosBicho[i].cod_jogo;
                    }
                } else {  
                    $scope.extracoesBicho[0] = $scope.codExtracaoBicho;
                }
            }
    
            if ($scope.extracoesBicho.length == 0) {
                $.alert('Não foi selecionada nenhuma extração no Jogo do Bicho!');
                return;
            }
        }        

        if ($scope.edtNome == "") {
            $.alert('Informe o NOME!');
            return;
        }

        $scope.apostas = [];

        var valorMinimo = $('#valorMinimo').val();
        if (valorMinimo == null) {
            valorMinimo = $scope.listaCfgMulti[0].valor_minimo_aposta;
        }
        valorMinimo = unparseValorMonetario(valorMinimo);

        var valor = $('#valorCartaoInteiro').val();
        if (valor == '') {
            valor = $scope.valorCartaoInteiro;
        } 
        valor = parseFloat(valor);

        if (valorMinimo > valor) {
            $.alert('O valor mínimo informado é superior ao valor do cartão!');
            return;
        }

        var valorPadrao = $scope.listaCfgMulti[0].valor_minimo_aposta;
        if (valorMinimo < valorPadrao) {
            $.alert('O valor mínimo informado é inferior a ' + valorPadrao  + '!');
            return;
        }

        if (parseFloat(user.getSaldoAtualizado()) < valor) {
            $.alert('Você não tem saldo para gerar o bilhete!');
            return;
        }

        $.confirm({
            title: '',
            content:"Confirma a geração dos bilhetes?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $scope.qtdApostas = Math.floor(valor / valorMinimo);
                    $scope.valorApostas = parseFloat((valor / $scope.qtdApostas).toFixed(2));
                    $scope.valorTotal = valor;
            
                    $scope.saldo = valor;
                    while ($scope.saldo > 0) {
                        var saldoControle = $scope.saldo;
                        $scope.geraAposta2pra500();
                        $scope.geraApostaRifa();
                        $scope.geraApostaSeninha();
                        $scope.geraApostaSuperSena();
                        $scope.geraApostaQuininha();
                        $scope.geraApostaLotinha();
                        $scope.geraApostaBicho();
                        if ($scope.saldo == saldoControle) {
                            $.alert('Os filtros inseridos não conseguem gerar o valor do cartão selecionado!');
                            return;
                        }
                    }      
            
                    if ($scope.apostas.length > 0) {
                        $scope.persisteBilhetes();
                    }    
                }
            }
        });
    }    

    $scope.geraApostaSeninha = function() {
        if ($scope.checkSeninha) {            
            var filtros = $scope.recuperaFiltrosSeleionados(14, 40, $scope.filtroAcertoSeninha);
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.concursosSeninha.length; k++) {
                    for (var i=0; i < filtros.length; i++) {
                        if ($scope.saldo > 0) {
                            $scope.saldo = parseFloat(($scope.saldo - $scope.valorApostas).toFixed(2));
                            var vValor = $scope.valorApostas;
                            if ($scope.saldo - $scope.valorApostas < 0) {
                                vValor += $scope.saldo;
                                $scope.saldo = 0;
                            }
                            var sNumeros = $scope.geraNumerosRandomicos(filtros[i], 60);
                            var objeto = {
                                numeros: sNumeros,
                                valorReal: vValor,
                                qtd: filtros[i],
                                tipo: "Seninha",
                                tipoJogo: '',
                                habilitaSorte: $scope.filtroSeninhaSorte,
                                cod_jogo: $scope.concursosSeninha[k]
                            };    
                            $scope.apostas.push(objeto);                             
                        } else {
                            break;
                        }
                    }
                }                    
            }  
        }
    }

    $scope.geraApostaSuperSena = function() {
        if ($scope.checkSuperSena) {            
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.concursosSuperSena.length; k++) {
                    if ($scope.saldo > 0) {
                        $scope.saldo = parseFloat(($scope.saldo - $scope.valorApostas).toFixed(2));
                        var vValor = $scope.valorApostas;
                        if ($scope.saldo - $scope.valorApostas < 0) {
                            vValor += $scope.saldo;
                            $scope.saldo = 0;
                        }
                        var sNumeros = $scope.geraNumerosRandomicos(10, 60);
                        var objeto = {
                            numeros: sNumeros,
                            valorReal: vValor,
                            qtd: 10,
                            tipo: "Super Sena",
                            tipoJogo: '',
                            habilitaSorte: 'S',
                            cod_jogo: $scope.concursosSuperSena[k]
                        };    
                        $scope.apostas.push(objeto);                             
                    } else {
                        break;
                    }
                }                    
            }  
        }
    }    

    $scope.geraApostaQuininha = function() {
        if ($scope.checkQuininha) {
            var filtros = $scope.recuperaFiltrosSeleionados(13, 45, $scope.filtroAcertoQuininha);
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.concursosQuininha.length; k++) {
                    for (var i=0; i < filtros.length; i++) {
                        if ($scope.saldo > 0) {
                            $scope.saldo = parseFloat(($scope.saldo - $scope.valorApostas).toFixed(2));
                            var vValor = $scope.valorApostas;
                            if ($scope.saldo - $scope.valorApostas < 0) {
                                vValor += $scope.saldo;
                                $scope.saldo = 0;
                            }
                            var sNumeros = $scope.geraNumerosRandomicos(filtros[i], 80);
                            var objeto = {
                                numeros: sNumeros,
                                valorReal: vValor,
                                qtd: filtros[i],
                                tipo: "Quininha",
                                tipoJogo: '',
                                habilitaSorte: $scope.filtroQuininhaSorte,
                                cod_jogo: $scope.concursosQuininha[k]
                            };    
                            $scope.apostas.push(objeto);    
                        } else {
                            break;
                        }
                    }
                }                    
            }  
        }
    }    

    $scope.geraApostaLotinha = function() {
        if ($scope.checkLotinha) {            
            var filtros = $scope.recuperaFiltrosSeleionados(16, 22, $scope.filtroAcertoLotinha);
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.concursosLotinha.length; k++) {
                    for (var i=0; i < filtros.length; i++) {
                        if ($scope.saldo > 0) {
                            $scope.saldo = parseFloat(($scope.saldo - $scope.valorApostas).toFixed(2));
                            var vValor = $scope.valorApostas;
                            if ($scope.saldo - $scope.valorApostas < 0) {
                                vValor += $scope.saldo;
                                $scope.saldo = 0;
                            }
                            var sNumeros = $scope.geraNumerosRandomicos(filtros[i], 25);
                            var objeto = {
                                numeros: sNumeros,
                                valorReal: vValor,
                                qtd: filtros[i],
                                tipo: "Lotinha",
                                tipoJogo: '',
                                habilitaSorte: '',
                                cod_jogo: $scope.concursosLotinha[k]
                            };    
                            $scope.apostas.push(objeto);    
                        } else {
                            break;
                        }
                    }
                }                    
            }  
        }
    }

    $scope.geraApostaRifa = function() {
        if ($scope.checkRifa) {               
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.concursosRifa.length; k++) {
                    var valorRifa = $scope.recuperaValorRifa($scope.concursosRifa[k]);
                    if ($scope.saldo > 0 
                            && (($scope.saldo - valorRifa - $scope.valorApostas) >= 0
                                    || ($scope.saldo - valorRifa) == 0)) {
                        $scope.saldo = parseFloat(($scope.saldo - valorRifa).toFixed(2));
                        var objeto = {
                            numeros: '',
                            valorReal: valorRifa,
                            qtd: "1",
                            tipo: "Rifa",
                            tipoJogo: '',
                            habilitaSorte: '',
                            cod_jogo: $scope.concursosRifa[k]
                        };    
                        $scope.apostas.push(objeto);
                    } else {
                        break;
                    }
                }                    
            }  
        }
    }    

    $scope.geraAposta2pra500 = function() {
        if ($scope.check2para500) {
            var valorAposta = parseInt($scope.c2pra500.valor_aposta);                
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.extracoes2pra500.length; k++) {
                    if ($scope.saldo > 0 && 
                            (($scope.saldo - valorAposta - $scope.valorApostas) >= 0 
                                || ($scope.saldo - valorAposta) == 0)) {
                        $scope.saldo = parseFloat(($scope.saldo - valorAposta).toFixed(2));
                        var objeto = {
                            numeros: '',
                            valorReal: valorAposta,
                            qtd: "1",
                            tipo: "2pra500",
                            tipoJogo: '',
                            habilitaSorte: '',
                            cod_jogo: $scope.extracoes2pra500[k]
                        };    
                        $scope.apostas.push(objeto);
                    } else {
                        break;
                    }
                }                    
            }  
        }
    }

    $scope.geraApostaBicho = function() {
        if ($scope.checkJogoDoBicho) {                
            if ($scope.saldo > 0) {
                for (var k=0; k < $scope.extracoesBicho.length; k++) {
                    for (var tipo in $scope.filtroTipoJogoBicho) {
                        if ($scope.saldo > 0) {
                            if ($scope.filtroTipoJogoBicho[tipo]) {
                                $scope.saldo = parseFloat(($scope.saldo - $scope.valorApostas).toFixed(2));
                                var vValor = $scope.valorApostas;
                                if ($scope.saldo - $scope.valorApostas < 0) {
                                    vValor += $scope.saldo;
                                    $scope.saldo = 0;
                                }        
                                var objeto = {
                                    numeros: '',
                                    valorReal: vValor,
                                    qtd: "1",
                                    tipo: "Bicho",
                                    tipoJogo: tipo,
                                    habilitaSorte: '',
                                    cod_jogo: $scope.extracoesBicho[k]
                                };    
                                $scope.apostas.push(objeto);                                
                            }
                        } else {
                            break;
                        }
                    }                    
                }                    
            }  
        }
    }

    $scope.recuperaValorRifa = function (cod_Concurso) {
        var valor = 0;
        if ($scope.jogosRifa.length > 0) {
            for (var i = 0; i < $scope.jogosRifa.length; i++) {
                if ($scope.jogosRifa[i].cod_jogo == cod_Concurso) {
                    return $scope.jogosRifa[i].valor;
                }
            }

        }
        return valor;
    } 

    $scope.persisteBilhetes = function () {       

        var data = new Date();
        var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
        var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
        var ano = data.getFullYear();
        var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
        var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
        var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };
        var dataAtual = ano + '-' + mes + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "cod_usuario=" + user.getCodigo() +
                "&cod_site=" + user.getSite() +
                "&nome_usuario=" + user.getName() +
                "&nome=" + $scope.edtNome +
                "&telefone=" + $scope.edtTelefone +
                "&registro_cliente=" + $scope.chkRegistroCliente +
                "&apostas=" + JSON.stringify($scope.apostas) +
                "&data_atual=" + dataAtual +
                "&operacao=" +
                "&schema=" + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                user.atualizaSaldo(response.data.saldo);
                user.setBilheteExterno(response.data.pule);
                user.setRepetirBilhete(false);
                $scope.repetir_bilhete = false;
                $location.path("/conferir_bilhete");
            } else {
                $rootScope.loading = false;
                if (response.data.status == 'INATIVO') {
                    $.alert(response.data.mensagem);
                    $location.path("/logout");
                } else {
                    $.alert(response.data.mensagem);
                }
            } 
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status);
        });
    }    

    $scope.filtroSeninhaSorte = false;
    $scope.concursosSeninha = [];
    $scope.codConcursoSeninha = 0;
    
    $scope.habilitaSeninha = function () {
        if ($scope.checkSeninha) {
            $scope.checkSeninha = false;
        } else {
            if ($scope.concursosSeninha.length > 0 || $scope.codConcursoSeninha != 0) {
                $scope.checkSeninha = true;
            } else {                
                $.alert('Não há concurso disponível para esta modalidade!');
                $scope.checkSeninha = false;
            }            
        }
    }

    $scope.selecionarNumeroSeninha = function (el, qtdNumeros) {
        var has = $(el).hasClass("multijogos-item-content-options-item-selecionado");
        if (has) {
            $(el).removeClass("multijogos-item-content-options-item-selecionado").addClass("multijogos-item-content-options-item");
            $scope.filtroAcertoSeninha[qtdNumeros] = false;
        } else {
            $(el).addClass("multijogos-item-content-options-item-selecionado");
            $scope.filtroAcertoSeninha[qtdNumeros] = true;
        }
    }

    $scope.concursosSuperSena = [];
    $scope.codConcursoSuperSena = 0;
    
    $scope.habilitaSuperSena = function () {
        if ($scope.checkSuperSena) {
            $scope.checkSuperSena = false;
        } else {
            if ($scope.concursosSuperSena.length > 0 || $scope.codConcursoSuperSena != 0) {
                $scope.checkSuperSena = true;
            } else {                
                $.alert('Não há concurso disponível para esta modalidade!');
                $scope.checkSuperSena = false;
            }            
        }
    } 
    
    $scope.filtroQuininhaSorte = false;
    $scope.concursosQuininha = [];
    $scope.codConcursoQuininha = 0;
    
    $scope.habilitaQuininha = function () {
        if ($scope.checkQuininha) {
            $scope.checkQuininha = false;
        } else {
            if ($scope.concursosQuininha.length > 0 || $scope.codConcursoQuininha != 0) {
                $scope.checkQuininha = true;
            } else {
                $.alert('Não há concurso disponível para esta modalidade!');
                $scope.checkQuininha = false;
            }
        }
    }

    $scope.selecionarNumeroQuininha = function (el, qtdNumeros) {
        var has = $(el).hasClass("multijogos-item-content-options-item-selecionado");
        if (has) {
            $(el).removeClass("multijogos-item-content-options-item-selecionado").addClass("multijogos-item-content-options-item");
            $scope.filtroAcertoQuininha[qtdNumeros] = false;
        } else {
            $(el).addClass("multijogos-item-content-options-item-selecionado");
            $scope.filtroAcertoQuininha[qtdNumeros] = true;
        }
    }    

    $scope.concursosLotinha = [];
    $scope.codConcursoLotinha = 0;
    
    $scope.habilitaLotinha = function () {
        if ($scope.checkLotinha) {
            $scope.checkLotinha = false;
        } else {
            if ($scope.concursosLotinha.length > 0 || $scope.codConcursoLotinha != 0) {
                $scope.checkLotinha = true;
            } else {
                $.alert('Não há concurso disponível para esta modalidade!');
                $scope.checkLotinha = false;
            }
        }
    }

    $scope.selecionarNumeroLotinha = function (el, qtdNumeros) {
        var has = $(el).hasClass("multijogos-item-content-options-item-selecionado");
        if (has) {
            $(el).removeClass("multijogos-item-content-options-item-selecionado").addClass("multijogos-item-content-options-item");
            $scope.filtroAcertoLotinha[qtdNumeros] = false;
        } else {
            $(el).addClass("multijogos-item-content-options-item-selecionado");
            $scope.filtroAcertoLotinha[qtdNumeros] = true;
        }
    }  
    
    $scope.concursosRifa = [];
    $scope.codConcursoRifa = 0;
    
    $scope.habilitaRifa = function () {
        if ($scope.checkRifa) {
            $scope.checkRifa = false;
        } else {
            if ($scope.concursosRifa.length > 0 || $scope.codConcursoRifa != 0) {
                $scope.checkRifa = true;
            } else {
                $.alert('Não há concurso disponível para esta modalidade!');
                $scope.checkRifa = false;
            }
        }
    }   
    
    $scope.extracoes2pra500 = [];
    $scope.codExtracao2pra500 = 0;
    
    $scope.habilita2pra500 = function () {
        if ($scope.check2para500) {
            $scope.check2para500 = false;
        } else {
            if ($scope.extracoes2pra500.length > 0 || $scope.codExtracao2pra500 != 0) {
                $scope.check2para500 = true;
            } else {
                $.alert('Não há extrações disponíveis para esta modalidade!');
                $scope.check2para500 = false;
            }
        }
    }    
    
    $scope.extracoesBicho = [];
    $scope.codExtracaoBicho = 0;
    
    $scope.habilitaBicho = function () {
        if ($scope.checkJogoDoBicho) {
            $scope.checkJogoDoBicho = false;
        } else {
            if ($scope.extracoesBicho.length > 0 || $scope.codExtracaoBicho != 0) {
                $scope.checkJogoDoBicho = true;
            } else {
                $.alert('Não há extrações disponíveis para esta modalidade!');
                $scope.checkJogoDoBicho = false;
            }
        }
    }     
   
    $scope.getJogos = function () {        
        $scope.getJogosSeninha();
        $scope.getJogosSuperSena();
        $scope.getJogosQuininha();
        $scope.getJogosLotinha();
        $scope.getJogosRifa();
        $scope.getJogos2pra500();
        $scope.getJogosBicho();
    }    

    $scope.getJogosSeninha = function () {
        if (!$scope.conf_geral.flg_seninha) {
            $scope.checkSeninha = false;
        } else {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&status=L' + 
                      '&operacao=seninha' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogosSeninha = response.data;
                $scope.checkSeninha = false;
                $('.multItemSeninha').removeClass('multijogos-item-active');
                if ($scope.jogosSeninha.length > 0) {                    
                    $scope.checkSeninha = true;
                    $('.multItemSeninha').addClass('multijogos-item-active');                                                
                    var htm = '';
                    var concursoSelecionado = '';
                    for (var i = 0; i < $scope.jogosSeninha.length; i++) {                
                        if (i == 0) {
                            htm += '<option value="' + $scope.jogosSeninha[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogosSeninha[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosSeninha[i].concurso;
                            $scope.codConcursoSeninha = $scope.jogosSeninha[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option value="' + $scope.jogosSeninha[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogosSeninha[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosSeninha[i].concurso;
                            } else {
                                htm += '<option value="' + $scope.jogosSeninha[i].cod_jogo + '">';
                            }
                        }
                        htm += $scope.jogosSeninha[i].data_formatada 
                            + ' - Concurso: ' + $scope.jogosSeninha[i].concurso + '</option>';
                    }
                    $('#concursosSeninha').append(htm);
                    $('#concursosSeninha').selectpicker('refresh');
                    $("#conteudoConcursoSeninha").find("div.filter-option-inner-inner").text(concursoSelecionado);                
                }
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }     

    $scope.getJogosSuperSena = function () {
        if (!$scope.conf_geral.flg_supersena) {
            $scope.checkSuperSena = false;
        } else {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&status=L' + 
                      '&operacao=supersena' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogosSuperSena = response.data;
                $scope.checkSuperSena = false;
                $('.multItemSuperSena').removeClass('multijogos-item-active');
                if ($scope.jogosSuperSena.length > 0) {                    
                    $scope.checkSuperSena = true;
                    $('.multItemSuperSena').addClass('multijogos-item-active');                                                
                    var htm = '';
                    var concursoSelecionado = '';
                    for (var i = 0; i < $scope.jogosSuperSena.length; i++) {                
                        if (i == 0) {
                            htm += '<option value="' + $scope.jogosSuperSena[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogosSuperSena[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosSuperSena[i].concurso;
                            $scope.codConcursoSuperSena = $scope.jogosSuperSena[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option value="' + $scope.jogosSuperSena[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogosSuperSena[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosSuperSena[i].concurso;
                            } else {
                                htm += '<option value="' + $scope.jogosSuperSena[i].cod_jogo + '">';
                            }
                        }
                        htm += $scope.jogosSuperSena[i].data_formatada 
                            + ' - Concurso: ' + $scope.jogosSuperSena[i].concurso + '</option>';
                    }
                    $('#concursosSuperSena').append(htm);
                    $('#concursosSuperSena').selectpicker('refresh');
                    $("#conteudoConcursoSuperSena").find("div.filter-option-inner-inner").text(concursoSelecionado);                
                }
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }

    $scope.getJogosQuininha = function () {
        if (!$scope.conf_geral.flg_quininha) {
            $scope.checkQuininha = false;
        } else {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&status=L' + 
                      '&operacao=quininha' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogosQuininha = response.data;
                $scope.checkQuininha = false;
                $('.multItemQuininha').removeClass('multijogos-item-active');
                if ($scope.jogosQuininha.length > 0) {
                    $scope.checkQuininha = true;
                    $('.multItemQuininha').addClass('multijogos-item-active');
                    var htm = '';
                    var concursoSelecionado = '';
                    for (var i = 0; i < $scope.jogosQuininha.length; i++) {
                        if (i == 0) {
                            htm += '<option value="' + $scope.jogosQuininha[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogosQuininha[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosQuininha[i].concurso;
                            $scope.codConcursoQuininha = $scope.jogosQuininha[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option value="' + $scope.jogosQuininha[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogosQuininha[i].data_formatada 
                                    + ' - Concurso: ' + $scope.jogosQuininha[i].concurso;
                            } else {
                                htm += '<option value="' + $scope.jogosQuininha[i].cod_jogo + '">';
                            }
                        }                
                        htm += $scope.jogosQuininha[i].data_formatada 
                            + ' - Concurso: ' + $scope.jogosQuininha[i].concurso + '</option>';
                    }
                    $('#concursosQuininha').append(htm);
                    $('#concursosQuininha').selectpicker('refresh');
                    $("#conteudoConcursoQuininha").find("div.filter-option-inner-inner").text(concursoSelecionado);
                    //$scope.concursosSeninha[0] = codConcursoSelecionado; 
                }                    
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }    

    $scope.getJogosLotinha = function () {
        if (!$scope.conf_geral.flg_lotinha) {
            $scope.checkLotinha = false;
        } else {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&status=L' + 
                      '&operacao=lotinha' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogosLotinha = response.data;
                $scope.checkLotinha = false;
                $('.multItemLotinha').removeClass('multijogos-item-active');
                if ($scope.jogosLotinha.length > 0) {
                    $scope.checkLotinha = true;
                    $('.multItemLotinha').addClass('multijogos-item-active');
                    var htm = '';
                    var concursoSelecionado = '';
                    for (var i = 0; i < $scope.jogosLotinha.length; i++) {
                        if (i == 0) {
                            htm += '<option value="' + $scope.jogosLotinha[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogosLotinha[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosLotinha[i].concurso;
                            $scope.codConcursoLotinha = $scope.jogosLotinha[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option value="' + $scope.jogosLotinha[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogosLotinha[i].data_formatada 
                                    + ' - Concurso: ' + $scope.jogosLotinha[i].concurso;
                            } else {
                                htm += '<option value="' + $scope.jogosLotinha[i].cod_jogo + '">';
                            }
                        }                
                        htm += $scope.jogosLotinha[i].data_formatada 
                            + ' - Concurso: ' + $scope.jogosLotinha[i].concurso + '</option>';
                    }
                    $('#concursosLotinha').append(htm);
                    $('#concursosLotinha').selectpicker('refresh');
                    $("#conteudoConcursoLotinha").find("div.filter-option-inner-inner").text(concursoSelecionado);
                    //$scope.concursosSeninha[0] = codConcursoSelecionado;                 
                }       
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }   

    $scope.getJogosRifa = function () {
        if (!$scope.conf_geral.flg_rifa) {
            $scope.checkRifa = false;
        } else {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                    '&finalizados=false' + 
                    '&site=' + user.getSite() + 
                    '&status=L' + '&operacao=rifa' +
                    '&dataAtual=true' +
                    '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogosRifa = response.data;
                $scope.checkRifa = false;
                $('.multItemRifa').removeClass('multijogos-item-active');
                if ($scope.jogosRifa.length > 0) {
                    $scope.checkRifa = true;
                    $('.multItemRifa').addClass('multijogos-item-active');
                    var htm = '';
                    var concursoSelecionado = '';
                    for (var i = 0; i < $scope.jogosRifa.length; i++) {
                        if (i == 0) {
                            htm += '<option value="' + $scope.jogosRifa[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogosRifa[i].data_formatada 
                                + ' - Concurso: ' + $scope.jogosRifa[i].concurso;
                            $scope.codConcursoRifa = $scope.jogosRifa[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option value="' + $scope.jogosRifa[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogosRifa[i].data_formatada 
                                    + ' - Concurso: ' + $scope.jogosRifa[i].concurso;
                            } else {
                                htm += '<option value="' + $scope.jogosRifa[i].cod_jogo + '">';
                            }
                        } 
                        htm += $scope.jogosRifa[i].data_formatada 
                            + ' - Concurso: ' + $scope.jogosRifa[i].concurso + '</option>';
                    }
                    $('#concursosRifa').append(htm);
                    $('#concursosRifa').selectpicker('refresh');                            
                    $("#conteudoConcursoRifa").find("div.filter-option-inner-inner").text(concursoSelecionado);
                    //$scope.concursosSeninha[0] = codConcursoSelecionado;    
                }            
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }

    $scope.getJogosBicho = function () {
        if (!$scope.conf_geral.flg_bicho) {
            $scope.checkJogoDoBicho = false;
        } else {        
            var data = $scope.edtDataDeBicho;
            if (typeof data === "undefined") {
                data = new Date;
            }

            var dataFiltro = $filter('date')(data, "yyyy-MM-dd");
            $rootScope.loading = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&status=L' + 
                      '&operacao=bicho&data=' + dataFiltro +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogosBicho = response.data;
                $scope.checkJogoDoBicho = false;
                $('.multItemJogoDoBicho').removeClass('multijogos-item-active');
                var concursoSelecionado = '';
                if ($scope.jogosBicho.length > 0) {
                    $scope.checkJogoDoBicho = true;
                    $('.multItemJogoDoBicho').addClass('multijogos-item-active');
                    var htm = '';
                    for (var i = 0; i < $scope.jogosBicho.length; i++) {
                        if (i == 0) {
                            htm += '<option class="optJB" value="' + $scope.jogosBicho[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogosBicho[i].hora_extracao 
                                + ' - ' + $scope.jogosBicho[i].desc_hora;
                            $scope.codExtracaoBicho = $scope.jogosBicho[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option class="optJB" value="' + $scope.jogosBicho[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogosBicho[i].hora_extracao 
                                + ' - ' + $scope.jogosBicho[i].desc_hora;
                            } else {
                                htm += '<option class="optJB" value="' + $scope.jogosBicho[i].cod_jogo + '">';
                            }
                        } 
                        htm += $scope.jogosBicho[i].hora_extracao 
                            + ' - ' + $scope.jogosBicho[i].desc_hora + '</option>';
                    }
                    $('#extracoesBicho').append(htm);
                    $('#extracoesBicho').selectpicker('refresh');
                    $("#conteudoConcursoBicho").find("div.filter-option-inner-inner").text(concursoSelecionado);
                    //$scope.concursosSeninha[0] = codConcursoSelecionado;
                } else {
                    $('#extracoesBicho .optJB').remove();
                    $('#extracoesBicho').selectpicker('refresh');
                    $scope.extracoesBicho = [];
                    $scope.codExtracaoBicho = 0;
                }
                $rootScope.loading = false;

            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
                $rootScope.loading = false;
            });
        }
    }    

    $scope.getJogos2pra500 = function () {
        if (!$scope.conf_geral.flg_2pra500) {
            $scope.check2para500 = false;
        } else {  
            var data = $scope.edtDataDe2pra500;
            if (typeof data === "undefined") {
                data = new Date;
            }        

            var dataFiltro = $filter('date')(data, "yyyy-MM-dd");
            $rootScope.loading = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                    '&finalizados=false' + 
                    '&site=' + user.getSite() + 
                    '&status=L' + 
                    '&operacao=2pra500&data=' + dataFiltro +
                    '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogos2pra500 = response.data;
                $scope.check2para500 = false;  
                $('.multItem2para500').removeClass('multijogos-item-active');
                if ($scope.jogos2pra500.length > 0) {
                    $scope.check2para500 = true;  
                    $('.multItem2para500').addClass('multijogos-item-active');
                    var htm = '';
                    var concursoSelecionado = '';
                    for (var i = 0; i < $scope.jogos2pra500.length; i++) {
                        if (i == 0) {
                            htm += '<option class="opt2pra500" value="' + $scope.jogos2pra500[i].cod_jogo + '" selected>';
                            concursoSelecionado = $scope.jogos2pra500[i].hora_extracao 
                                + ' - ' + $scope.jogos2pra500[i].desc_hora;
                            $scope.codExtracao2pra500 = $scope.jogos2pra500[i].cod_jogo;
                        } else {
                            if ($scope.concursos_marcados == 'T') {
                                htm += '<option class="opt2pra500" value="' + $scope.jogos2pra500[i].cod_jogo + '" selected>';
                                concursoSelecionado += ', ' + $scope.jogos2pra500[i].hora_extracao 
                                    + ' - ' + $scope.jogos2pra500[i].desc_hora;
                            } else {                        
                                htm += '<option class="opt2pra500" value="' + $scope.jogos2pra500[i].cod_jogo + '">';
                            }
                        }
                        htm += $scope.jogos2pra500[i].hora_extracao 
                            + ' - ' + $scope.jogos2pra500[i].desc_hora + '</option>';
                    }
                    $('#extracoes2pra500').append(htm);
                    $('#extracoes2pra500').selectpicker('refresh');
                    $("#conteudoConcurso2pra500").find("div.filter-option-inner-inner").text(concursoSelecionado);
                    //$scope.extracoes2pra500[0] = codConcursoSelecionado;
                } else {
                    $('#extracoes2pra500 .opt2pra500').remove();
                    $('#extracoes2pra500').selectpicker('refresh');
                    $scope.extracoes2pra500 = [];
                    $scope.codExtracao2pra500 = 0;
                }
                $rootScope.loading = false;
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status);
                $rootScope.loading = false;
            });
        }
    }     

    $scope.carregaConfiguracoes = function () {
        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "site=" + user.getSite() + 
                  "&cod_area=" + user.getArea() + 
                  "&operacao=todas" +
                  "&schema=" + user.getSchema()
        }).then(function (response) {
            $scope.conf_geral = response.data[0].cg;
            $scope.c2pra500 = response.data[1].c2pra500;
            $scope.conf_sena = response.data[2].sena;
            $scope.conf_supersena = response.data[3].supersena;
            $scope.conf_quina = response.data[4].quina;
            $scope.conf_loto = response.data[5].loto;
            $scope.conf_bicho = response.data[6].bicho;
            $scope.conf_rifa = response.data[7].rifa;
            $scope.getJogos();
        }).catch(angular.noop);       

    }
    
    $scope.getDescricaoConcurso = function(jogo){
        switch (jogo.tipo_jogo) {
            case 'B':
            case '2':
                return jogo.hora_extracao + " - " + jogo.desc_hora;
            case 'S':
            case 'Q':
            case 'L':
                return "Concurso: " + jogo.concurso;
            case 'R':
                return jogo.descricao;
            default:
                return "";
        }
    } 

    $scope.selecionaCartao = function(indice){
        for (var i=0; i<$scope.cartoes.length; i++) {
            if (i == indice) {
                $scope.cartoes[i].selecionado = true;
                var numero = parseFloat($scope.cartoes[i].valor).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                $('#valorCartao').prop("value",  numero.join(','));
                $('#valorCartaoInteiro').prop("value",  $scope.cartoes[i].valor);                
            } else {
                $scope.cartoes[i].selecionado = false;
            }
        }
    }

    $('.selectRifa').selectpicker();
    $('.select2para500').selectpicker();
    $('.selectJb').selectpicker();
    $('.selectLotinha').selectpicker();
    $('.selectQuininha').selectpicker();
    $('.selectSeninha').selectpicker();
    $('.selectSuperSena').selectpicker();

    // START CHECKBOX
    $("#check-seninha").prop("checked", true);
    $("#check-supersena").prop("checked", true);
    $("#check-quininha").prop("checked", true);
    $("#check-jogoDoBicho").prop("checked", true);
    $("#check-lotinha").prop("checked", true);
    $("#check-2para500").prop("checked", true);
    $("#check-rifa").prop("checked", true);
    $('.optionsLotinha').css('display', 'grid');

    // CHECKBOX CLASS ACTIVE
    if ($('#check-seninha').is(":checked")) {
        $('.multItemSeninha').addClass('multijogos-item-active');
    } else {
        $('.multItemSeninha').removeClass('multijogos-item-active');
    }

    if ($('#check-supersena').is(":checked")) {
        $('.multItemSuperSena').addClass('multijogos-item-active');
    } else {
        $('.multItemSuperSena').removeClass('multijogos-item-active');
    }

    if ($('#check-quininha').is(":checked")) {
        $('.multItemQuininha').addClass('multijogos-item-active');
    } else {
        $('.multItemQuininha').removeClass('multijogos-item-active');
    }

    if ($('#check-jogoDoBicho').is(":checked")) {
        $('.multItemJogoDoBicho').addClass('multijogos-item-active');
    } else {
        $('.multItemJogoDoBicho').removeClass('multijogos-item-active');
    }

    if ($('#check-lotinha').is(":checked")) {
        $('.multItemLotinha').addClass('multijogos-item-active');
    } else {
        $('.multItemLotinha').removeClass('multijogos-item-active');
    }

    if ($('#check-2para500').is(":checked")) {
        $('.multItem2para500').addClass('multijogos-item-active');
    } else {
        $('.multItem2para500').removeClass('multijogos-item-active');
    }

    if ($('#check-rifa').is(":checked")) {
        $('.multItemRifa').addClass('multijogos-item-active');
    } else {
        $('.multItemRifa').removeClass('multijogos-item-active');
    }
    

    // CHANGE CHECKBOX
    $('#check-seninha').change(function() {
        if ($('#check-seninha').is(":checked")) {
            $('.multItemSeninha').addClass('multijogos-item-active');
        } else {
            $('.multItemSeninha').removeClass('multijogos-item-active');
        }
    });

    $('#check-supersena').change(function() {
        if ($('#check-supersena').is(":checked")) {
            $('.multItemSuperSena').addClass('multijogos-item-active');
        } else {
            $('.multItemSuperSena').removeClass('multijogos-item-active');
        }
    });

    $('#check-quininha').change(function() {
        if ($('#check-quininha').is(":checked")) {
            $('.multItemQuininha').addClass('multijogos-item-active');
        } else {
            $('.multItemQuininha').removeClass('multijogos-item-active');
        }
    });

    $('#check-jogoDoBicho').change(function() {
        if ($('#check-jogoDoBicho').is(":checked")) {
            $('.multItemJogoDoBicho').addClass('multijogos-item-active');
        } else {
            $('.multItemJogoDoBicho').removeClass('multijogos-item-active');
        }
    });

    $('#check-lotinha').change(function() {
        if ($('#check-lotinha').is(":checked")) {
            $('.multItemLotinha').addClass('multijogos-item-active');
        } else {
            $('.multItemLotinha').removeClass('multijogos-item-active');
        }
    });

    $('#check-2para500').change(function() {
        if ($('#check-2para500').is(":checked")) {
            $('.multItem2para500').addClass('multijogos-item-active');
        } else {
            $('.multItem2para500').removeClass('multijogos-item-active');
        }
    });

    $('#check-rifa').change(function() {
        if ($('#check-rifa').is(":checked")) {
            $('.multItemRifa').addClass('multijogos-item-active');
        } else {
            $('.multItemRifa').removeClass('multijogos-item-active');
        }
    });

    // SENINHA E QUININHA DA SORTE
    if ($('#check-seninhaDaSorte').is(":checked")) {
        $('.optionsSeninha').css('display', 'none');
        $('.optionsSeninhaDaSorte').css('display', 'grid');
    } else {
        $('.optionsSeninha').css('display', 'grid');
        $('.optionsSeninhaDaSorte').css('display', 'none');
    }

    if ($('#check-quininhaDaSorte').is(":checked")) {
        $('.optionsQuininha').css('display', 'none');
        $('.optionsQuininhaDaSorte').css('display', 'grid');
    } else {
        $('.optionsQuininha').css('display', 'grid');
        $('.optionsQuininhaDaSorte').css('display', 'none');
    }

    
    // SENINHA DA SORTE
    $('#check-seninhaDaSorte').change(function() {
        if ($('#check-seninhaDaSorte').is(":checked")) {
            $('.optionsSeninha').css('display', 'none');
            $('.optionsSeninhaDaSorte').css('display', 'grid');
        } else {
            $('.optionsSeninha').css('display', 'grid');
            $('.optionsSeninhaDaSorte').css('display', 'none');
        }
    });

    // QUININHA DA SORTE
    $('#check-quininhaDaSorte').change(function() {
        if ($('#check-quininhaDaSorte').is(":checked")) {
            $('.optionsQuininha').css('display', 'none');
            $('.optionsQuininhaDaSorte').css('display', 'grid');
        } else {
            $('.optionsQuininha').css('display', 'grid');
            $('.optionsQuininhaDaSorte').css('display', 'none');
        }
    });

    $("#BetInputPhone").mask("(00) 0000-00009");
    $('#valorMinimo').mask('000.000.000.000.000,00', {reverse: true});
       
}).directive("owlCarousel", ['$timeout',function($timeout) {
	return {
		restrict: 'E',
		transclude: false,
		link: function (scope) {
  			scope.initCarousel = function(element) {
  			   $timeout(function () {
      			  // provide any default options you want
      				var defaultOptions = {
      				};
      				var customOptions = scope.$eval($(element).attr('data-options'));
      				// combine the two options objects
      				for(var key in customOptions) {
      					defaultOptions[key] = customOptions[key];
      				}
      				// init carousel
      				$(element).owlCarousel({
                        margin: 15,
                        stagePadding: 10,
                        stagePadding: 25,
                        nav: false,
                        dots: false,
                        responsiveClass:true,
                        responsive:{
                            0:{
                                items:1
                            },
                            768:{
                                items:2
                            },
                            1000:{
                                items:3
                            }
                        }
                    });
  			   },50);
  		};
		}
	};
}])
.directive('owlCarouselItem', [function() {
	return {
		restrict: 'A',
		transclude: false,
		link: function(scope, element) {
		  // wait for the last item in the ng-repeat then call init
			if(scope.$last) {
				scope.initCarousel(element.parent());
			}
		}
	};
}]); 

function openAccordion(el) {
    var has = $(el).closest('.itemOpen');
    //var has = $(el).hasClass("itemOpen");
    if (has.length > 0) {
        $(el).closest('.multijogos-item').removeClass('itemOpen');
        $(el).closest('.multijogos-item').addClass('itemClosed');
        $(el).find('.multijogos-item-header-iconArrow').css('transform', 'rotate(0deg)');
        $(el).closest('.multijogos-item').find('.multijogos-item-content').css('display', 'none');
    } else {
        $(el).closest('.multijogos-item').addClass('itemOpen');
        $(el).closest('.multijogos-item').removeClass('itemClosed');
        $(el).find('.multijogos-item-header-iconArrow').css('transform', 'rotate(180deg)');
        $(el).closest('.multijogos-item').find('.multijogos-item-content').css('display', 'flex');
        var width = $(el).find('.numberItem').width();
        $(el).find('.numberItem').css('height', width + 'px');
    }
}

function isSidebar() {
    var e = $('.sidebar').hasClass("sidebarClosed");
    if(e) {
        $('.sidebar').removeClass('sidebarClosed');
        $('.sidebar').addClass('sidebarOpen');
        $('.sidebarbackground').css('display', 'initial');
    } else {
        $('.sidebar').removeClass('sidebarOpen');
        $('.sidebar').addClass('sidebarClosed');
        $('.sidebarbackground').css('display', 'none');
    }
}