<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

// if (!isset($_POST)) {
//     die();
// }

$query = "SELECT codigo, descricao, numero_1, numero_2, numero_3, numero_4 FROM bicho";
        
$result = mysqli_query($con, $query);
$return_arr = array();

$contador = 0;
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  $contador = $contador + 1;
  $row_array['codigo'] = $row['codigo'];
  $row_array['descricao'] = $row['descricao'];
  $row_array['numero_1'] = $row['numero_1'];
  $row_array['numero_2'] = $row['numero_2'];
  $row_array['numero_3'] = $row['numero_3'];
  $row_array['numero_4'] = $row['numero_4'];
  array_push($return_arr, $row_array);
  if ($contador == mysqli_num_rows($result)) {
      break;
  }
}
         
echo json_encode($return_arr);

$con->close();