angular.module('main').controller('agendaBotCtrl', function ($scope, user, $http, $route, $rootScope) {

    $scope.carregarAgenda = function () {
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/agenda_bot.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "&operacao=listar"
        }).then(function (response) {
            $scope.records = response.data;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status);
        }).finally(function() {
            $rootScope.loading = false;
        });
    }

    $scope.salvarAgenda = function () {
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/agenda_bot.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'agenda=' + JSON.stringify($scope.records) +
                '&operacao=salvar'
        }).then(function (response) {
            if (response.status == '200') {
                alert("Salvo com sucesso!");
                $route.reload();
            } else {
                alert(response.data.mensagem);
            }
        }).finally(function() {
            $rootScope.loading = false;
        });
    }

});