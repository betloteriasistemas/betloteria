<?php

include "conexao.php";
require_once 'dompdf/autoload.inc.php';
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


if (!isset($_GET)) {
    die();
}

use Dompdf\Dompdf;

$dompdf = new Dompdf();

$queryVer = "";


$site = mysqli_real_escape_string($con, $_GET['site']);
$perfil = mysqli_real_escape_string($con, $_GET['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$finalizados = mysqli_real_escape_string($con, $_GET['finalizados']);
$dataDe = mysqli_real_escape_string($con, $_GET['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_GET['dataAte']);

$tipoJogo = mysqli_real_escape_string($con, $_GET['tipoJogo']);
$nomeCambista = mysqli_real_escape_string($con, $_GET['cambista']);
$concurso = mysqli_real_escape_string($con, $_GET['concurso']);
$status = mysqli_real_escape_string($con, $_GET['status']);


if($dataDe == "undefined"){
    $dataDe = 0;
}

if($dataAte == "undefined"){
    $dataAte = 0;
}
$exibe = "SIM";
if($finalizados == 'false')
{
    $exibe = "NÃO";
}



if ($perfil == "C") { 
    $query = "select v.data_jogo, v.concurso, v.hora_extracao, v.desc_hora, v.tp_status, v.tipo_jogo,
    v.desc_status, v.desc_jogo, v.cod_usuario, v.nome, sum(v.entradas) entradas, sum(v.qtd_apostas) qtd_apostas, sum(v.comissao) comissao  , 
    sum(v.comissao_gerente) comissao_gerente, sum(v.saidas) saidas, sum(v.entradas) - sum(v.saidas) - sum(v.comissao)  saldo, 0 as saldo_par 
    
    from (
    select 
    DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, 
    cast(case
    when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)  
    else jogo.concurso end as char) as concurso ,  TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora,   
    jogo.tp_status, jogo.tipo_jogo,
    case
    when jogo.tp_status = 'A' then 'ABERTO'
    when jogo.tp_status = 'P' then 'PROCESSADO'
    when jogo.tp_status = 'C' then 'CANCELADO'
    when jogo.tp_status = 'L' then 'LIBERADO'
    when jogo.tp_status = 'B' then 'BLOQUEADO'
    when jogo.tp_status = 'F' then 'FINALIZADO'
    when jogo.tp_status = 'V' then 'PENDENTE DE VALIDAÇÃO' ELSE ' - ' END AS desc_status,

    case
    when jogo.tipo_jogo = 'S' then 'SENINHA'
    when jogo.tipo_jogo = 'Q' then 'QUININHA'
    when jogo.tipo_jogo = 'B' then 'BICHO'
    when jogo.tipo_jogo = '2' then '2 PRA 500'
    when jogo.tipo_jogo = '6' then '6 DA SORTE' ELSE 'MODALIDADE' END AS desc_jogo,
    usu.cod_usuario, usu.nome,
    sum(apo.valor_aposta) entradas,
    count(apo.cod_aposta) qtd_apostas,
    sum(apo.comissao) as comissao,
    0 comissao_gerente,
    sum(apo.valor_ganho) as saidas
    from jogo
    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
    where jogo.cod_site = '$site'
      and usu.cod_usuario = '$cod_usuario'
      and bil.status_bilhete not in ('C')
      and apo.status not in ('C') ";

    if ($finalizados == 'false') {
        $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
    }

    if ($dataDe != '0') {
        $query = $query . " AND jogo.data_jogo >= '$dataDe' ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND jogo.data_jogo <= '$dataAte' ";
    }

    if($tipoJogo != "undefined" && $tipoJogo != "")
    {
        $query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
    }

    if($nomeCambista != "undefined" && $nomeCambista != "")
    {
        $query = $query . " AND usu.cod_usuario = '$nomeCambista' "; 
    }

    if($concurso != "undefined" && $concurso != "")
    {
        $query = $query . " AND jogo.cod_jogo = '$concurso' "; 
    }

    if($status != "undefined" && $status != "")
    {
        $query = $query . " AND jogo.tp_status = '$status' ";
    }


    
    $query = $query . " group by usu.cod_usuario, usu.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo
                            ) v where 1 =1 
                            group by v.data_jogo, v.concurso, v.hora_extracao, v.desc_hora, v.tp_status, v.tipo_jogo,
    v.desc_status, v.desc_jogo, v.cod_usuario, v.nome 
    order by data_jogo, nome, concurso desc ";


} else if ($perfil == "G") {
    $query = "
    select g.cod_usuario, g.nome, g.data_jogo, g.concurso, g.hora_extracao, g.desc_hora, g.tp_status, g.desc_status, g.desc_jogo, g.tipo_jogo,
        sum(g.entradas) as entradas, sum(g.qtd_apostas) as qtd_apostas, sum(g.comissao) as comissao, sum(g.saidas) as saidas,
        sum(g.saldo_par) as saldo_par, sum(g.comissao_gerente) as comissao_gerente,  
        sum(g.saldo_par) - sum(g.comissao_gerente) as saldo  from (
    select v.*, (v.entradas - v.saidas - v.comissao) saldo_par,
    round(((select (CASE WHEN v.Tipo_Jogo = 'S' THEN pct_comissao_seninha 
    WHEN v.Tipo_Jogo = 'Q' THEN  pct_comissao_quininha 
    WHEN v.Tipo_Jogo in ('B','G') THEN pct_comissao_bicho 
    WHEN v.Tipo_Jogo = '2' THEN  pct_comissao_2PRA500 
    WHEN v.Tipo_Jogo = '6' THEN  pct_comissao_6DASORTE ELSE 0 END) from usuario where cod_usuario = '$cod_usuario') * (v.entradas - v.saidas - v.comissao)) / 100 ,2) comissao_gerente
    from (
        select usu.cod_usuario, usu.nome, DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
        when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
        else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, 
        jogo.tp_status, 
        case
        when jogo.tp_status = 'A' then 'ABERTO'
        when jogo.tp_status = 'P' then 'PROCESSADO'
        when jogo.tp_status = 'C' then 'CANCELADO'
        when jogo.tp_status = 'L' then 'LIBERADO'
        when jogo.tp_status = 'B' then 'BLOQUEADO'
        when jogo.tp_status = 'V' then 'PENDENTE DE VALIDAÇÃO'
        when jogo.tp_status = 'F' then 'FINALIZADO'
        ELSE ' - ' END AS desc_status,

        case
        when jogo.tipo_jogo = 'S' then 'SENINHA'
        when jogo.tipo_jogo = 'Q' then 'QUININHA'
        when jogo.tipo_jogo = 'B' then 'BICHO'
        when jogo.tipo_jogo = '2' then '2 PRA 500'
        when jogo.tipo_jogo = '6' then '6 DA SORTE' ELSE 'MODALIDADE' END AS desc_jogo,
        
        jogo.tipo_jogo,
        sum(apo.valor_aposta) entradas,
        count(apo.cod_aposta) qtd_apostas,
        sum(apo.comissao) as comissao,
        sum(apo.valor_ganho) as saidas
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
        where jogo.cod_site = '$site'
          and usu.cod_gerente = '$cod_usuario'
          and bil.status_bilhete not in ('C')
          and apo.status not in ('C') ";

    if ($finalizados == 'false') {
        $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
    }
    if ($dataDe != '0') {
        $query = $query . " AND jogo.data_jogo >= '$dataDe' ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND jogo.data_jogo <= '$dataAte' ";
    } 

    if($tipoJogo != "undefined" && $tipoJogo != "")
    {
        $query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
    }

    if($nomeCambista != "undefined" && $nomeCambista != "")
    {
        $query = $query . " AND usu.cod_usuario = '$nomeCambista' "; 
    }

    if($concurso != "undefined" && $concurso != "")
    {
        $query = $query . " AND jogo.cod_jogo = '$concurso' "; 
    }

    if($status != "undefined" && $status != "")
    {
        $query = $query . " AND jogo.tp_status = '$status' ";
    }

    $query = $query . " group by usu.cod_usuario, usu.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo, apo.cod_aposta
                          ) v where 1 =1 order by data_jogo, nome, concurso desc) g 
                          group by g.cod_usuario, g.nome, g.data_jogo, g.concurso, g.hora_extracao, g.desc_hora, g.tp_status, g.desc_status, g.desc_jogo, g.tipo_jogo ";
} else if ($perfil == "A") {
    $query = "
    select cod_usuario,nome,data_jogo,concurso,hora_extracao,desc_hora,tp_status,desc_status,tipo_jogo,desc_jogo , sum(entradas) as entradas, sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,round((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))) /100,2) as comissao_gerente,(sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
    ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - ((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)))/100)) as saldo from (
        select geren.cod_usuario, geren.nome,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
        when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
        else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, 
        jogo.tp_status,  
        case
        when jogo.tp_status = 'A' then 'ABERTO'
        when jogo.tp_status = 'P' then 'PROCESSADO'
        when jogo.tp_status = 'C' then 'CANCELADO'
        when jogo.tp_status = 'L' then 'LIBERADO'
        when jogo.tp_status = 'B' then 'BLOQUEADO'
        when jogo.tp_status = 'F' then 'FINALIZADO'
        when jogo.tp_status = 'V' then 'PENDENTE DE VALIDAÇÃO'  ELSE ' - ' END AS desc_status,

        case
        when jogo.tipo_jogo = 'S' then 'SENINHA'
        when jogo.tipo_jogo = 'Q' then 'QUININHA'
        when jogo.tipo_jogo = 'B' then 'BICHO'
        when jogo.tipo_jogo = '2' then '2 PRA 500'
        when jogo.tipo_jogo = '6' then '6 DA SORTE' ELSE 'MODALIDADE' END AS desc_jogo,

        jogo.tipo_jogo, (CASE WHEN jogo.Tipo_Jogo = 'S' THEN geren.pct_comissao_seninha 
        WHEN jogo.Tipo_Jogo = 'Q' THEN  geren.pct_comissao_quininha 
        WHEN jogo.Tipo_Jogo in ('B','G') THEN  geren.pct_comissao_bicho 
        WHEN jogo.Tipo_Jogo = '2' THEN  geren.pct_comissao_2PRA500 
        WHEN jogo.Tipo_Jogo = '6' THEN  geren.pct_comissao_6DASORTE ELSE 0 END) as pct_comissao,
        sum(apo.valor_aposta) entradas,
        count(apo.cod_aposta) qtd_apostas,
        sum(apo.comissao) as comissao,
        sum(apo.valor_ganho) as saidas
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
        left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
        where jogo.cod_site = '$site'
        and geren.cod_gerente = '$cod_usuario'
        and bil.status_bilhete not in ('C')
        and apo.status not in ('C') ";
        
        if ($finalizados == 'false') {
            $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
        }
        if ($dataDe != '0') {
            $query = $query . " AND jogo.data_jogo >= '$dataDe' ";
        }
    
        if ($dataAte != '0') {
            $query = $query . " AND jogo.data_jogo <= '$dataAte' ";
        }

        if($tipoJogo != "undefined" && $tipoJogo != "")
        {
            $query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
        }
    
        if($nomeCambista != "undefined" && $nomeCambista != "")
        {
            $query = $query . " AND geren.cod_usuario = '$nomeCambista' "; 
        }
    
        if($concurso != "undefined" && $concurso != "")
        {
            $query = $query . " AND jogo.cod_jogo = '$concurso' "; 
        }
    
        if($status != "undefined" && $status != "")
        {
            $query = $query . " AND jogo.tp_status = '$status' ";
        }

        $query = $query . " group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status,desc_status, jogo.tipo_jogo,desc_jogo, apo.cod_aposta) v
        where 1 =1 group by cod_usuario,nome,data_jogo, concurso, hora_extracao, desc_hora,tp_status,desc_jogo, tipo_jogo,desc_jogo order by data_jogo, nome, concurso desc";
}

$queryVer = $query;



$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    
        $row_array['cod_usuario'] = $row['cod_usuario'];
        $row_array['nome'] = $row['nome'];
    
    $row_array['saldo'] = $row['saldo'];
    $row_array['concurso'] = $row['concurso'];
    $row_array['data_jogo'] = $row['data_jogo'];
    $row_array['tp_status'] = $row['tp_status'];
    $row_array['tipo_jogo'] = $row['tipo_jogo'];
    $row_array['entradas'] = $row['entradas'];
    $row_array['qtd_apostas'] = $row['qtd_apostas'];
    $row_array['desc_status'] = $row['desc_status'];
    $row_array['desc_jogo'] = $row['desc_jogo'];
    

    $row_array['comissao'] = $row['comissao'];
    $row_array['saidas'] = $row['saidas'];
    $row_array['comissao_gerente'] = $row['comissao_gerente'];
    $row_array['saldo_par'] = $row['saldo_par'];

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
};

$sumQtdApostas = 0;
$sumEntradas= 0;
$sumSaidas = 0;
$sumComissao = 0;
$sumComissaoGerente = 0;
$sumSaldo = 0;
$sumSaldoParcial = 0;

$data = date('d-m-Y');
$data .= ' '.date('H:i:s');

$query = " SELECT nome from usuario
            WHERE cod_usuario = '$cod_usuario' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);


if (count($return_arr) == 0) {

    $html = "
   <html>
<body>
<hr>
  <h1> Caixa sem movimentação! </h1>
<hr>
</body>
</html> ";
} else {


    $html = "
   <html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 10px;
}
</style>
</head>
<body>
<div class='col-md-12 col-ms-12 col-xs-12'>
                <div class='table-responsive betL_CaixaList'>

                <h1> CAIXA  </h1>
                &nbsp;
                
                <div style='display: inline;'>
                <span style='font-size: 12pt;'> De: " . $dataDe . "</span>
                <span style='font-size: 12pt;'> De: " . $dataAte . "</span>
                <span style='font-size: 12pt;'> Exibe Finalizados: " . $exibe . "</aspan>
                &nbsp;
                <span style='font-size: 12pt;'> " . $row['nome'] . " em " . $data . 
                "</div>
                
                <div>&nbsp;</div>
           
                
                    <table class='table table-hover' style='width: 980px;'>
                        <thead>
                            <tr style='background-color: #2E64FE; color: white;'>
                                <th style='width: 60px; text-align: center;'>Concurso</th>
                                <th style='width: 60px; text-align: center;'>Data</th>
                                <th style='width: 30px; text-align: center;'>Status</th>
                                <th style='width: 110px; text-align: center;'>Jogo</th>";
                                if ($perfil == "A") {
                                    $html = $html . "<th style='width: 80px; text-align: center;'>Gerente</th>";
                                } else {
                                    $html = $html . "<th style='width: 80px; text-align: center;'>Cambista</th>";
                                }
                                $html = $html . "<th style='width: 30px; text-align: center;'>Qtd</th>
                                <th style='width: 60px; text-align: center;'><a>Entradas</a></th>
                                <th style='width: 60px; text-align: center;'><a>Saídas </a></th>
                                <th style='width: 60px; text-align: center;'>Comissão</th>";
                                if ($perfil != "C") {
                                    $html = $html . "<th style='width: 60px; text-align: center;'>Comissão Gerente</th>";
                                }
                                $html = $html . "<th style='width: 90px; text-align: center;'>Saldo</th>
                            </tr>
                        </thead>
                        <tbody class='container'>";
                         $odd = 0;           
                        foreach ($return_arr as $e) {
                            $odd = $odd + 1;
                            if($odd % 2 == 0)
                            {
                                $html = $html . "<tr style='background-color: #E6E6E6;'>";
                            } else {
                                $html = $html . "<tr>";
                            }

                            $sumQtdApostas = $sumQtdApostas + $e['qtd_apostas'];
                            $sumEntradas= $sumEntradas + $e['entradas'];
                            $sumSaidas = $sumSaidas + $e['saidas'];
                            $sumComissao = $sumComissao + $e['comissao'];
                            $sumComissaoGerente = $sumComissaoGerente + $e['comissao_gerente'];
                            $sumSaldo = $sumSaldo + $e['saldo'];
                            $sumSaldoParcial = $sumSaldoParcial + $e['saldo_par'];
                            
                            $html = $html . " <th scope='row'> " . $e['concurso'] . "</th>
                                  <td> " . $e['data_jogo'] . " </td>
                                  <td> " . $e['desc_status'] . " </td>
                                  <td> " . $e['desc_jogo'] . " </td>
                                  <td> " . $e['nome'] . " </td> 
                                  <td align='center'> " . $e['qtd_apostas'] . " </td>
                                  <td align='right'> R$ " . number_format($e['entradas'], 2, ',', '.') . " </td>
                                  <td align='right'> R$ " . number_format($e['saidas'], 2, ',', '.') . " </td>
                                  <td align='right'> R$ " . number_format($e['comissao'], 2, ',', '.') . " </td>";
                                  if ($perfil != "C") {
                                      if($e['comissao_gerente'] >= 0)
                                      {
                                        $html = $html . "<td align='right'> R$ " . number_format($e['comissao_gerente'], 2, ',', '.') . " </td>";
                                      }else{
                                        $html = $html . "<td align='right'> R$ " . number_format('0', 2, ',', '.') . " </td>";
                                      }
                                    
                                  }
                                  if($e['comissao_gerente'] >= 0)
                                  {
                                    if ($e['saldo'] > 0) {
                                        $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($e['saldo'], 2, ',', '.') . " </td>";
                                        } else{
                                        $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($e['saldo'], 2, ',', '.') . " </td>";
                                        }
                                  } else
                                  {
                                    if ($e['saldo_par'] > 0) {
                                        $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($e['saldo_par'], 2, ',', '.') . " </td>";
                                        } else{
                                        $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($e['saldo_par'], 2, ',', '.') . " </td>";
                                        }
                                  }
                                  
                                  $html = $html . "</tr>";
                        }

                        $html = $html . "</tbody>
                        <tfoot><tr style='font-weight: bold; background-color: #A9F5BC;'>
                                  <th scope='row'> TOTAL </th>
                                  <td> - </td>
                                  <td> - </td>
                                  <td> - </td>
                                  <td> - </td> 
                                  <td align='center'> " .  $sumQtdApostas . " </td>
                                  <td align='right'> R$ " . number_format($sumEntradas, 2, ',', '.') . " </td>
                                  <td align='right'> R$ " . number_format($sumSaidas, 2, ',', '.') . " </td>
                                  <td align='right'> R$ " . number_format($sumComissao, 2, ',', '.') . " </td>";
                                  if ($perfil != "C") {
                                    if($sumComissaoGerente >= 0)
                                    {
                                        $html = $html . "<td align='right'> R$ " . number_format($sumComissaoGerente, 2, ',', '.') . " </td>";
                                    }else{
                                        $html = $html . "<td align='right'> R$ " . number_format('0', 2, ',', '.') . " </td>";
                                    }
                                    
                                  }

                                  if($sumComissaoGerente >= 0)
                                    {
                                        if ($sumSaldo > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($sumSaldo, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($sumSaldo, 2, ',', '.') . " </td>";
                                            }
                                    } else{
                                        if ($sumSaldoParcial  > 0) {
                                            $html = $html . "<td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($sumSaldoParcial, 2, ',', '.') . " </td>";
                                            } else{
                                            $html = $html . "<td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($sumSaldoParcial, 2, ',', '.') . " </td>";
                                            }
                                    }

                                  
                                  $html = $html . "</tr>

                        </tfoot>
                    </table>

                    
                </div>
            </div>
    </body> 
</html> ";

}

// print_r($queryVer);
// printf($html);


$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
$dompdf->stream("Caixa.pdf");

$con->close();
 
?>
