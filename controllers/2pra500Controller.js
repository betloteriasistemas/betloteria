angular.module('main').controller('2pra500Ctrl', 
function ($rootScope, $scope, $q, user, $http, $filter, $location, configGeral) {
    $scope.flg_2pra500 = false;
    $scope.chkRegistroCliente = false;
    $scope.clientes = [];

    configGeral.get().then(function (data) {
        $scope.flg_2pra500= user.getflg_2pra500() == 'S' && data.flg_2pra500;
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        $scope.desc_2pra500 = funcoes.getDescricaoTipoJogo('2', $scope.desc_2pra500);
    });

    $q.all([
        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() 
                + '&cod_area=' + user.getArea() 
                + '&operacao=2pra500'
                + '&schema=' + user.getSchema()
        }),
        $http({
            url: 'angularjs-mysql/usuario.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() 
                + '&cod_usuario=' + user.getCodigo()
                + '&schema=' + user.getSchema()
        }),
        $http({
            method: "POST",
            url: "angularjs-mysql/clientes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo()
                + '&operacao=listar'
                + '&schema=' + user.getSchema()
        })
    ]).then(function (response) {
        $scope.listaCfg = response[0].data;
        $scope.usuario = response[1].data;
        $scope.clientes = response[2].data;
    });

    $scope.edtDataDe = new Date();

    $scope.getJogos = function () {
        var data = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        $http({
            method: "POST",
            url: "angularjs-mysql/jogosArea.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + 
                  '&finalizados=false' + 
                  '&site=' + user.getSite() + 
                  '&area=' + user.getArea() + 
                  '&status=L' + 
                  '&operacao=2pra500' + 
                  '&data=' + data + 
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.jogos = []
            $scope.jogos = response.data;
            $scope.edtQtdBilhetes = null;
            $scope.comboConcurso = "undefined";
        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.filtrarCliente = function(termo) {
        return $scope.clientes.filter(cliente => cliente.nome.toLowerCase().indexOf(termo)>-1);
    }

    $scope.onSelectCliente = function(cliente) {
        if (cliente) {
            $scope.edtTelefone = cliente.telefone;
            document.getElementById("chkRegistrarCliente").focus();
        } else {
            $scope.edtTelefone = null;
        }
    }

    $scope.botaoHabilitado = false;

    $scope.gerarBilhete = function () {                            
        try {
            if (parseFloat(user.getSaldoAtualizado()) < ($scope.edtQtdBilhetes * 2)) {
                alert('Você não tem saldo para gerar o bilhete!');
                return;
            }
    
            if ($scope.comboConcurso == "undefined" || $scope.comboConcurso == null) {
                alert('Informe a EXTRAÇÃO!');
                return;
            }
    
            if (typeof $scope.edtQtdBilhetes === "undefined" || $scope.edtQtdBilhetes == null) {
                alert('Informe a Quantidade de Bilhetes!');
                return;
            } 
            
            if ($scope.edtQtdBilhetes <= 0) {
                alert("Informe uma quantidade de bilhetes maior que zero!");
                return;
            }
    
            if ($scope.chkRegistroCliente && !$scope.edtNome) {
                alert('Para registrar o cliente é necessário informar o NOME!');
                return;
            }
    
            // Confirmação usando window.confirm
            if (!confirm("Confirma a geração do bilhete?")) {
                console.log("Operação cancelada pelo usuário.");
                return;
            }

            $scope.botaoHabilitado = true;

            var data = new Date();
            var dataAtual = data.toISOString().slice(0, 19).replace("T", " ");
    
            var sTelefone = $scope.edtTelefone || "";
            var sNome = $scope.edtNome || "Não Informado";
            var agrupa = $scope.agrupaNumeros ? 1 : 0;
    
            $rootScope.loading = true;
            console.log("Iniciando requisição HTTP...");
    
            var apostas = Array.from({ length: $scope.edtQtdBilhetes }, () => ({
                numeros: '',
                valorReal: $scope.listaCfg[0].valor_aposta,
                qtd: "1",
                tipo: "2pra500",
                tipoJogo: '',
                habilitaSorte: '',
                cod_jogo: $scope.comboConcurso
            }));
    
            $http({
                url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: `cod_usuario=${user.getCodigo()}&cod_site=${user.getSite()}&nome_usuario=${user.getName()}&nome=${sNome}&telefone=${sTelefone}&registro_cliente=${$scope.chkRegistroCliente}&apostas=${JSON.stringify(apostas)}&data_atual=${dataAtual}&operacao=&schema=${user.getSchema()}&agrupaNumeros=${agrupa}&qtdBilhetes=${$scope.edtQtdBilhetes}`
            }).then(response => {
                console.log("Resposta do servidor recebida:", response.data);
    
                if (response.data.status == 'OK') {
                    user.atualizaSaldo(response.data.saldo);
                    user.setBilheteExterno(response.data.pule);
                    console.log("Bilhete gerado com sucesso, redirecionando...");
                    $location.path("/conferir_bilhete");
                } else {
                    $rootScope.loading = false;
                    console.warn("Erro no processamento:", response.data.mensagem);
                    alert(response.data.mensagem);
    
                    if (response.data.status == 'INATIVO') {
                        console.log("Usuário inativo, redirecionando para logout...");
                        $location.path("/logout");
                    }
                    $scope.botaoHabilitado = false;
                }
            }).catch(response => {
                $rootScope.loading = false;
                console.error("Erro no HTTP:", response.status);
                alert('Erro no $HTTP: ' + response.status);
                $scope.botaoHabilitado = false;
            });
    
        } finally {
            console.log("Execução finalizada.");
        }
    };
});
