<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];
if (isset($_POST['operacao'])) {
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);
}
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$nome_usuario = mysqli_real_escape_string($con, $_POST['nome_usuario']);
$cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);

$auditoria = "";

try {
    $con->begin_transaction();
    if ($operacao === "atualizarStatusPagamento") {
        $txt_pule = mysqli_real_escape_string($con, $_POST['txt_pule']);
        $sqlUpdate = "UPDATE bilhete SET STATUS_PAGAMENTO = 'S' WHERE txt_pule = '$txt_pule'";
        if ($con->query($sqlUpdate) === true) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_BILHETE_PAGO,
                descreverPagamentoBilhete($txt_pule)
            );
            $response['status'] = "OK";
        } else {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
        }
    } else {

        $queryValida = " SELECT U.STATUS STATUS_USUARIO, S.STATUS STATUS_SITE, U.COD_GERENTE 
                            from USUARIO U
                            INNER JOIN SITE S ON (S.COD_SITE = U.COD_SITE)
                            where U.COD_USUARIO = '$cod_usuario'";

        $resultValida = mysqli_query($con, $queryValida);
        $rowValida = mysqli_fetch_array($resultValida, MYSQLI_ASSOC);
        $cod_gerente = $rowValida['COD_GERENTE'];
        if ($rowValida['STATUS_USUARIO'] == "I" || $rowValida['STATUS_SITE'] == "I") {
            $response['status'] = "INATIVO";
            $response['mensagem'] = "Site ou cambista Inativo!";
        } else {
            $query = 'SET @@session.time_zone = "-03:00"';

            $result = mysqli_query($con, $query);

            $cod_jogo = mysqli_real_escape_string($con, $_POST['cod_jogo']);
            if ($cod_jogo == "null") {
                throw new Exception('Selecione um concurso!');
            }

            $query = "SELECT J.TP_STATUS, J.HORA_EXTRACAO, J.DATA_JOGO, J.TIPO_JOGO,
                             J.CONCURSO AS COD_EXTRACAO, 
                             CURRENT_TIME + INTERVAL E.PRAZO_BLOQUEIO MINUTE AGORA, 
                             current_date HOJE
                        FROM JOGO J 
                        INNER JOIN EXTRACAO_BICHO E ON (J.CONCURSO = E.COD_EXTRACAO)
                        WHERE COD_JOGO = '$cod_jogo'";

            $result = mysqli_query($con, $query);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if (($row['TP_STATUS'] == 'B') || (($row['TIPO_JOGO'] == 'B' || $row['TIPO_JOGO'] == '2') &&
                $row['HORA_EXTRACAO'] < $row['AGORA'] &&
                $row['DATA_JOGO'] == $row['HOJE'])) {
                throw new Exception('Concurso Bloqueado! Atualize a página e selecione um concurso válido!');
            }
            $tipo_jogo = $row['TIPO_JOGO'];

            $config_usuario_arr = array();
            $queryConf = "";
            $config_gerente_arr = array();
            $queryConfGerente = "";
            $qtdPremios = 10;

            switch ($tipo_jogo) {
                case 'B':
                    $queryConf = mysqli_query($con, "SELECT *                   
                                FROM usuario usu 
                                    left join configuracao_comissao_bicho conf_comissao on usu.cod_usuario = conf_comissao.cod_usuario
                                    left join configuracao_bicho conf on usu.cod_area = conf.cod_area
                                WHERE usu.COD_SITE = '$cod_site' 
                                    AND usu.COD_USUARIO = '$cod_usuario'");
                    
                    $queryLimiteBicho = mysqli_query($con, "SELECT clb.*
                        FROM jogo j 
                        INNER JOIN configuracao_limite_bicho clb ON (j.CONCURSO = clb.COD_EXTRACAO)
                        WHERE clb.COD_SITE = '$cod_site' AND j.COD_JOGO = '$cod_jogo'");
                    $limites =  mysqli_fetch_array($queryLimiteBicho, MYSQLI_ASSOC);                    

                    $queryConfGerente = mysqli_query($con, "SELECT *                   
                        FROM usuario usu 
                            left join configuracao_comissao_bicho conf_comissao on usu.cod_usuario = conf_comissao.cod_usuario
                            left join configuracao_bicho conf on usu.cod_area = conf.cod_area
                        WHERE usu.COD_SITE = '$cod_site' 
                            AND usu.COD_USUARIO = '$cod_gerente'"); 

                    $cod_extracao = $row['COD_EXTRACAO'];
                    $queryExtracao = mysqli_query($con, "SELECT qtd_premios FROM extracao_bicho where COD_EXTRACAO = '$cod_extracao'");
                    $rowExtracao = mysqli_fetch_object($queryExtracao);
                    if ($rowExtracao) {
                        $qtdPremios = $rowExtracao->qtd_premios;
                    }
                break;
                case '2':
                    $queryConf = mysqli_query($con, "SELECT *                   
                                FROM usuario usu 
                                left join configuracao_2pra500 conf on usu.cod_area = conf.cod_area 
                                WHERE usu.COD_SITE = '$cod_site' 
                                    AND usu.COD_USUARIO = '$cod_usuario'");
                    $queryConfGerente = mysqli_query($con, "SELECT *                   
                                FROM usuario usu 
                                left join configuracao_2pra500 conf on usu.cod_area = conf.cod_area 
                                WHERE usu.COD_SITE = '$cod_site' 
                                    AND usu.COD_USUARIO = '$cod_gerente'");
                break;
            }

            $config_usuario_arr =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
            $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);

            if ($tipo_jogo != '2') {
                $apostas = json_decode($_POST['apostas']);
                foreach ($apostas as $e) {
                    if ($e->valorReal < 0) {
                        throw new Exception('Não é permitido apostas com valor inferior a zero!');
                    }
                    if ($tipo_jogo == 'B' && ($e->premioDoReal > $qtdPremios || $e->premioAoReal > $qtdPremios)) {
                        throw new Exception('A quantidade máxima de prêmios para esta extração é de '
                            . $qtdPremios . ' prêmios! Por favor recarregue a página.');
                    }
                }
            }

            $query = "select (data_jogo < current_date) data_passou,
                    (data_jogo <= current_date) data_retorno,
                (CURRENT_TIME >= hora_extracao) hora_retorno
            from jogo
            where cod_jogo = '$cod_jogo'";

            $result = mysqli_query($con, $query);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
                throw new Exception('Aposta não pode ser feita para esse concurso! Selecione o próximo concurso!');
            }

            $qtdBilhetes = 1;
            if ($tipo_jogo === '2') {
                $qtdBilhetes = mysqli_real_escape_string($con, $_POST['qtd_bilhetes']);
                $valor_acumulado = mysqli_real_escape_string($con, $_POST['valor_acumulado']);
                $valor_aposta = mysqli_real_escape_string($con, $_POST['valor_aposta']);
                if ($qtdBilhetes <= 0) {
                    throw new Exception('Informe uma quantidade de bilhetes maior que zero!');
                }
            }

            for ($i = 1; $i <= $qtdBilhetes; $i++) {
                $stmt = $con->prepare("insert into bilhete(cod_site, cod_usuario) VALUES(?, ?)  ");
                $stmt->bind_param("ii", $cod_site, $cod_usuario);

                $stmt->execute();
                $cod_bilhete = $con->insert_id;

                if ($i == 1) {
                    $stmt = $con->prepare(" UPDATE bilhete 
                                    SET  nome_apostador = ?, 
                                        telefone_apostador = ?, 
                                        data_bilhete = current_timestamp, 
                                        txt_pule = ?, 
                                        numero_bilhete = ?
                                    WHERE cod_bilhete = ? ");
                    $stmt->bind_param("sssii", $nome_apostador, $telefone_apostador, $pule, $numero_bilhete, $cod_bilhete);
                } else {
                    $stmt = $con->prepare(" UPDATE bilhete 
                                    SET  nome_apostador = ?, 
                                        telefone_apostador = ?, 
                                        data_bilhete = current_timestamp, 
                                        txt_pule_pai = ?, 
                                        numero_bilhete = ?,
                                        txt_pule = ?
                                    WHERE cod_bilhete = ? ");
                    $stmt->bind_param("sssisi", $nome_apostador, $telefone_apostador, $pule, $numero_bilhete, $cod_bilhete, $cod_bilhete);
                }

                if ($tipo_jogo != '2') {
                    //$numero_bilhete = mysqli_real_escape_string($con, $_POST['numero_bilhete']);
                    // Alterado pra testar o problema de duplicidade - Will 05/09/2019
                    $numero_bilhete = $cod_site . $cod_bilhete;
                }
                $nome_apostador = mysqli_real_escape_string($con, $_POST['nome']);
                $telefone_apostador = mysqli_real_escape_string($con, $_POST['telefone']);
                $pule = mysqli_real_escape_string($con, $_POST['pule']);
                $data_atual = mysqli_real_escape_string($con, $_POST['data_atual']);

                if ($tipo_jogo == '2') {
                    try {
                        if (isset($_POST['numero_bilhete'])) {
                            $numero_bilhete = $cod_site . $cod_bilhete;
                        } else {
                            $numero_bilhete = $cod_site . $cod_bilhete . $i;
                        }
                    } catch (Exception $e) {
                    }
                }

                $stmt->execute();

                $auditoria = descreverCriacaoBilhete(
                    $nome_apostador,
                    $telefone_apostador,
                    $data_atual,
                    $i == 1 ? $pule : $cod_bilhete,
                    $numero_bilhete,
                    $i == 1 ? null : $pule
                );

                if ($tipo_jogo == '2') {
                    $query = " select distinct a.txt_aposta from aposta a
            where a.cod_jogo = '$cod_jogo' ";
                    $result = mysqli_query($con, $query);
                    $return_arr = array();
                    $contador = 0;

                    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                        $contador = $contador + 1;

                        $array = explode('-', str_replace(' ', '', $row['txt_aposta']));
                        foreach ($array as $value) {
                            array_push($return_arr, $value);
                        }

                        if ($contador == mysqli_num_rows($result)) {
                            break;
                        }
                    }

                    $contador = 0;
                    $numero1 = -1;
                    $numero2 = -1;
                    $numero3 = -1;
                    $numero4 = -1;

                    if (count($return_arr) == 10000) {
                        $query = "Delete from bilhete where cod_bilhete = '$cod_bilhete' ";
                        $stmt = $con->prepare($query);
                        $stmt->execute();

                        throw new Exception("Número máximo de milhares atingidas nesse concurso!");
                    }

                    if (count($return_arr) == 0) {
                        $numero1 = rand(0, 9999);
                        $numero2 = rand(0, 9999);
                        $numero3 = rand(0, 9999);
                        $numero4 = rand(0, 9999);
                    } else {
                        $contador = 0;
                        while ($contador < 4) {
                            while (in_array(($n = str_pad(rand(0, 9999), 4, "0", STR_PAD_LEFT)), $return_arr) || $n == $numero1 || $n == $numero2 || $n == $numero3 || $n == $numero4);

                            $numero = $n;
                            $contador = $contador + 1;
                            if ($contador == 1) {
                                $numero1 = $numero;
                            } else if ($contador == 2) {
                                $numero2 = $numero;
                            } else if ($contador == 3) {
                                $numero3 = $numero;
                            } else if ($contador == 4) {
                                $numero4 = $numero;
                            }
                            if ($contador == 4) {
                                break;
                            }
                        }
                    }

                    $apostas = [];
                    $numerosRandom['numeros'] = str_pad($numero1, 4, "0", STR_PAD_LEFT) . "-" .
                        str_pad($numero2, 4, "0", STR_PAD_LEFT) . "-" .
                        str_pad($numero3, 4, "0", STR_PAD_LEFT) . "-" .
                        str_pad($numero4, 4, "0", STR_PAD_LEFT);
                    array_push($apostas, $numerosRandom);
                }

                $contador = 0;
                $valorReal = 0;
                $sNumeros = "";
                $constanteNao = mysqli_real_escape_string($con, 'N');
                $constanteSim = mysqli_real_escape_string($con, 'S');
                foreach ($apostas as $e) {

                    $comissao = 0;
                    $comissao_gerente = 0;
                    $possivel_retorno_calculado = 0;
                    $quina_sorte = 0;
                    $quadra_sorte = 0;
                    $terno_sorte = 0;

                    switch ($tipo_jogo) {
                        case 'B':
                            $possivel_retorno_calculado = getValorPremioBicho($e->numeros, $e->tipoJogo, $e->valorReal, 
                                $e->premioDoReal, $e->premioAoReal, $config_usuario_arr, $limites);
                            $comissao = obterComissaoBicho(
                                $e->tipoJogo,
                                $e->valorReal,
                                $config_usuario_arr
                            );
                            $comissao_gerente = obterComissaoBicho(
                                $e->tipoJogo,
                                $e->valorReal - $comissao,
                                $config_gerente_arr
                            );
                        break;
                        case '2':
                            $comissao = $valor_aposta * $config_usuario_arr['PCT_COMISSAO_2PRA500'] / 100;
                            $comissao_gerente = (($valor_aposta - $comissao) * $config_gerente_arr['PCT_COMISSAO_2PRA500']) / 100;
                        break;
                    }

                    if ($tipo_jogo == 'B') {
                        $sNumeros = str_replace('.', '', $e->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, txt_aposta, valor_aposta, possivel_retorno, do_premio, ao_premio, tipo_jogo, inversoes, comissao, comissao_gerente )
                    values(?,?,?,?,?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiisddsssidd", $cod_site, $cod_jogo, $cod_bilhete, $sNumeros, $e->valorReal, $possivel_retorno_calculado, $e->premioDoReal, $e->premioAoReal, $e->tipoJogo, $e->inversoes, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaBicho($sNumeros, $e->valorReal, $possivel_retorno_calculado, $e->premioDoReal, $e->premioAoReal, $e->tipoJogo, $e->inversoes, $comissao);
                    } else if ($tipo_jogo == '2') {
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, comissao, comissao_gerente )
                        values(?,?,?,4,?,?,?,?,?) ");
                        $stmt->bind_param("iiisdddd", $cod_site, $cod_jogo, $cod_bilhete, $e['numeros'], $valor_aposta, $valor_acumulado, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverAposta2pra500($e['numeros'], $valor_aposta, $valor_acumulado, $comissao);
                    } 

                    if ($tipo_jogo == '2') {
                        $valorReal = 2;
                    } else {
                        $valorReal = $valorReal + $e->valorReal;
                    }

                    $stmt->execute();

                }

                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $cod_site,
                    AUD_BILHETE_CRIADO,
                    $auditoria
                );


                $stmt = $con->prepare("UPDATE usuario SET SALDO = SALDO - ?
		                    WHERE COD_USUARIO = ? AND COD_SITE = ?");
                $stmt->bind_param("dii", $valorReal, $cod_usuario, $cod_site);
                $stmt->execute();
            }

            $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_SITE = '$cod_site'
              and COD_USUARIO = '$cod_usuario' ";
            $result = mysqli_query($con, $query);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            $response['saldo'] = $row['SALDO'];
            $response['status'] = "OK";
        }
    }
    $con->commit();
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}
$con->close();
echo json_encode($response);

function getValorPremio($qtdNumeros, $valor_aposta, $configuracao)
{
    switch ($qtdNumeros) {
        case 13:
            return $valor_aposta * $configuracao['PREMIO_13'];
        case 14:
            return $valor_aposta * $configuracao['PREMIO_14'];
        case 15:
            return $valor_aposta * $configuracao['PREMIO_15'];
        case 16:
            return $valor_aposta * $configuracao['PREMIO_16'];
        case 17:
            return $valor_aposta * $configuracao['PREMIO_17'];
        case 18:
            return $valor_aposta * $configuracao['PREMIO_18'];
        case 19:
            return $valor_aposta * $configuracao['PREMIO_19'];
        case 20:
            return $valor_aposta * $configuracao['PREMIO_20'];
        case 21:
            return $valor_aposta * $configuracao['PREMIO_21'];
        case 22:
            return $valor_aposta * $configuracao['PREMIO_22'];
        case 25:
            return $valor_aposta * $configuracao['PREMIO_25'];
        case 30:
            return $valor_aposta * $configuracao['PREMIO_30'];
        case 35:
            return $valor_aposta * $configuracao['PREMIO_35'];
        case 40:
            return $valor_aposta * $configuracao['PREMIO_40'];
        case 45:
            return $valor_aposta * $configuracao['PREMIO_45'];
    }
}

function getValorPremioBicho(
    $txt_aposta,
    $tipo_jogo,
    $valor_aposta,
    $do_premio,
    $ao_premio,
    $configuracao,
    $limites
) {
    $milhares = explode("-", $txt_aposta);
    $qtdMilhares = count($milhares);
    $qtdPremios = abs($ao_premio - $do_premio) + 1;
    $retorno = 0;
    switch ($tipo_jogo) {
        case "MS":
            if ($valor_aposta == 0) {
                $retorno = $configuracao['VALOR_RETORNO_MILHAR_BRINDE'];
            } else {
                $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_MILHAR_SECA'];            
            }
        break;
        case "MC":
            $retorno = (
                        ($configuracao['PREMIO_MILHAR_SECA'] * ($valor_aposta / 2)) +
                        ($configuracao['PREMIO_CENTENA'] * ($valor_aposta / 2))
                        )/($qtdMilhares * $qtdPremios);
            break;
        case "MI":
            $retorno = ($valor_aposta / ($qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_MILHAR_SECA'];
            break;
        case "MCI":
            $retorno = ($valor_aposta / ($qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_MILHAR_CENTENA'];
            break;
        case "C":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_CENTENA'];
            break;
        case "CI":
            $retorno = ($valor_aposta / ($qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_CENTENA'];
            break;
        case "G":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_GRUPO'];
            break;
        case "DG":
            $retorno = $valor_aposta * $configuracao['PREMIO_DUQUE_GRUPO'];
            break;
        case "DGC":
            $retorno = ($valor_aposta / (getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_DUQUE_GRUPO'];
            break;
        case "TG":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_GRUPO'];
            break;
        case "TG10":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_GRUPO10'];
            break;
        case "TGC":
            $retorno = ($valor_aposta / getQtdInversoes($tipo_jogo, $txt_aposta)) * $configuracao['PREMIO_TERNO_GRUPO'];
            break;
        case "DZ":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_DEZENA'];
            break;
        case "DDZ":
            $retorno = $valor_aposta  * $configuracao['PREMIO_DUQUE_DEZENA'];
            break;
        case "DDZC":
            $retorno = ($valor_aposta / (getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_DUQUE_DEZENA'];
            break;
        case "TDZ":
            $retorno = $valor_aposta  * $configuracao['PREMIO_TERNO_DEZENA'];
            break;
        case "TDZC":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_TERNO_DEZENA'];
            break;
        case "PS":
            $retorno = $valor_aposta * $configuracao['PREMIO_PASSE_SECO'];
            break;
        case "PS12":
            $retorno = $valor_aposta * $configuracao['PREMIO_PASSE_SECO'];
            break;
        case "PC":
            $retorno = $valor_aposta * $configuracao['PREMIO_PASSE_COMBINADO'];
            break;
    }
    if ($limites === null) {
        return $retorno;
    }
    $valorLimite = getLimiteValorBicho($tipo_jogo, $limites);
    if ($retorno > $valorLimite && $valorLimite > 0) {
        return $valorLimite;
    } else {
        return $retorno;
    }
}

function obterComissaoBicho(
    $tipo_aposta,
    $valor_aposta,
    $comissaoBicho
) {
    $comissao = 0;
    if (
        $tipo_aposta == "MC" && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "MS" && $comissaoBicho['COMISSAO_MILHAR_SECA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_SECA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_SECA'] / 100;
    } else if (($tipo_aposta == "MI" || $tipo_aposta == "MCI") &&
        $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] / 100;
    } else if (
        $tipo_aposta == "C" && $comissaoBicho['COMISSAO_CENTENA'] != null
        && $comissaoBicho['COMISSAO_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "CI" && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] / 100;
    } else if (
        $tipo_aposta == "G" && $comissaoBicho['COMISSAO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_GRUPO'] / 100;
    } else if (($tipo_aposta == "DG" || $tipo_aposta == "DGC") && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] != null
        && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_GRUPO'] / 100;
    } else if (($tipo_aposta == "TG" || $tipo_aposta == "TGC" || $tipo_aposta == "TG10")
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_GRUPO'] / 100;
    } else if (
        $tipo_aposta == "DZ" && $comissaoBicho['COMISSAO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DEZENA'] / 100;
    } else if (($tipo_aposta == "DDZ" || $tipo_aposta == "DDZC") &&  $comissaoBicho['COMISSAO_DUQUE_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DUQUE_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_DEZENA'] / 100;
    } else if (($tipo_aposta == "TDZ" || $tipo_aposta == "TDZC") &&
        $comissaoBicho['COMISSAO_TERNO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_TERNO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_DEZENA'] / 100;
    } else if (($tipo_aposta == "PS" || $tipo_aposta == "PS12") && $comissaoBicho['COMISSAO_PASSE_SECO'] != null
        && $comissaoBicho['COMISSAO_PASSE_SECO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_SECO'] / 100;
    } else if (
        $tipo_aposta == "PC" && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] != null
        && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_COMBINADO'] / 100;
    } else {
        $comissao = $valor_aposta * $comissaoBicho['PCT_COMISSAO_BICHO'] / 100;
    }

    return $comissao;
}
