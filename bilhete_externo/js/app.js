var app = angular.module('main', ['ngRoute', 'ngMaterial']);

app.factory('preventTemplateCache', function () {
	return {
		'request': function (config) {
			if (config.url.indexOf('components') !== -1 ||
				config.url.indexOf('controllers') !== -1) {
				config.url = config.url + '?' + new Date().getTime();
			}
			return config;
		}
	}
}).config(function ($httpProvider) {
	$httpProvider.interceptors.push('preventTemplateCache');
});

app.config(function ($routeProvider, $locationProvider) {
	$routeProvider.when('/', {
		templateUrl: './components/conferir_bilhete.html',
		controller: 'conferirBilheteCtrl'
	}).when('/seninha', {
		templateUrl: './components/seninha_virtual.html',
		controller: 'seninhaVirtualCtrl'
	}).when('/seninha/:id', {
		templateUrl: './components/seninha_virtual.html',
		controller: 'seninhaVirtualCtrl'
	}).when('/dashboard/:id', {
		templateUrl: './components/dashboard.html',
		controller: 'dashboardCtrl'
	}).when('/supersena', {
		templateUrl: './components/super_sena_virtual.html',
		controller: 'superSenaVirtualCtrl'
	}).when('/supersena/:id', {
		templateUrl: './components/super_sena_virtual.html',
		controller: 'superSenaVirtualCtrl'		
	}).when('/quininha', {
		templateUrl: './components/quininha_virtual.html',
		controller: 'quininhaVirtualCtrl'
	}).when('/quininha/:id', {
		templateUrl: './components/quininha_virtual.html',
		controller: 'quininhaVirtualCtrl'
	}).when('/lotinha', {
		templateUrl: './components/lotinha_virtual.html',
		controller: 'lotinhaVirtualCtrl'
	}).when('/lotinha/:id', {
		templateUrl: './components/lotinha_virtual.html',
		controller: 'lotinhaVirtualCtrl'
	}).when('/bicho', {
		templateUrl: './components/bicho_virtual.html',
		controller: 'bichoVirtualCtrl'
	}).when('/bicho/:id', {
		templateUrl: './components/bicho_virtual.html',
		controller: 'bichoVirtualCtrl'
	}).when('/:id', {
		templateUrl: './components/conferir_bilhete.html',
		controller: 'conferirBilheteCtrl'
		
	}).otherwise({
		template: '404'
	});

	$locationProvider.html5Mode(false);
});

app.factory("configGeral", ['$http', '$q', '$routeParams', 'user', function ($http, $q, $routeParams, user) {
	var config;
	return {
		get: function () {
			var deferred = $q.defer();
			if (angular.isDefined(config)) {
				deferred.resolve(config);
			} else {
				var currentId = $routeParams.id;
				if (typeof currentId == "undefined") {
					currentId = 9999;
				}
				$http({
					method: "POST",
					url: "angularjs-mysql/configuracao.php",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: '&site=' + currentId + 
					      '&operacao=geral' +
						  '&schema=' + user.getSchema()
				}).then(function (response) {
					config = response.data[0];
					deferred.resolve(response.data[0]);
				}).catch(function (response) {
					deferred.resolve(response);
					alert('Erro no $HTTP: ' + response.status)
				});
			}
			return deferred.promise;
		}
	};
}]);

app.factory("bilhetesAPI", ['$http', 'user', function ($http, user) {

	var _getBilhetes = function (codigo, numero) {
		var cod_bilhete = codigo;
		var numero_bilhete = numero;
		return $http({
			method: "POST",
			url: "angularjs-mysql/bilhetes.php",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: 'cod_usuario=' + user.getCodigo() + 
			      '&site=' + user.getSite() + 
				  '&cod_bilhete=' + cod_bilhete + 
				  '&numero_bilhete=' + numero_bilhete + 
				  '&operacao=conferirBilhete' +
                  '&schema=' + user.getSchema()
		}).then(function (response) {
			return response.data;
		}, function (response) {
			console.log(response);
			alert('Erro no $HTTP: ' + response.status)
		});
	};

	return {
		getBilhetes: _getBilhetes
	};

}]);



app.service('user', function () {
	var username;
	var nome;
	var loggedin = false;
	var id;
	var codigo;
	var site;
	var perfil;
	var bilheteExterno = "";
	var siteExterno = 0;
	var schema;

	this.getBilheteExterno = function () {
		return bilheteExterno;
	}

	this.getSiteExterno = function () {
		return siteExterno;
	}

	this.setBilheteExterno = function (auxBilheteExterno) {
		bilheteExterno = auxBilheteExterno;
	}

	this.setSiteExterno = function (auxSiteExterno) {
		siteExterno = auxSiteExterno;
		schema = 'b' + auxSiteExterno;
	}

	this.getName = function () {
		return username;
	};

	this.setNome = function (auxNome) {
		nome = auxNome;
	}

	this.getNome = function () {
		return nome;
	};

	this.setID = function (userID) {
		id = userID;
	};
	this.getID = function () {
		return id;
	};

	this.setCodigo = function (auxCodigo) {
		codigo = auxCodigo;
	}

	this.getCodigo = function () {
		return codigo;
	}

	this.getSite = function () {
		return site;
	}
	this.getPerfil = function () {
		return perfil;
	}

	this.getSchema = function() {
		return schema;
	}

	this.isUserLoggedIn = function () {
		if (!!localStorage.getItem('login')) {
			loggedin = true;
			var data = JSON.parse(localStorage.getItem('login'));
			username = data.username;
			id = data.id;
			nome = data.nome;
			codigo = data.codigo;
			site = data.site;
			perfil = data.perfil;
			schema = data.schema;
			//alert (site);
		}
		return loggedin;
	};

	this.saveData = function (data) {
		username = data.user;
		id = data.id;
		nome = data.nome;
		loggedin = true;
		codigo = data.codigo;
		site = data.site;
		perfil = data.perfil;
		schema = data.schema;
		localStorage.setItem('login', JSON.stringify({
			username: username,
			id: id,
			nome: nome,
			codigo: codigo,
			site: site,
			perfil: perfil,
			schema: schema
		}));
	};

	this.clearData = function () {
		localStorage.removeItem('login');
		username = "";
		id = "";
		nome = "";
		loggedin = false;
		codigo = "";
		site = "";
		perfil = "";
		schema = "";
	}
});

app.service('funcoes', [function () {
	this.getDescricaoTipoJogoBicho = function (tipo_jogo_bicho) {
		if (tipo_jogo_bicho == 'MS') {
			return "MS - MILHAR SECA";
		} else if (tipo_jogo_bicho == 'MC') {
			return "MC - MILHAR COM CENTENA";
		} else if (tipo_jogo_bicho == 'MI') {
			return "MI - MILHAR INVERTIDA";
		} else if (tipo_jogo_bicho == 'MCI') {
			return "MCI - MILHAR COM CENTENA INVERTIDA";
		} else if (tipo_jogo_bicho == 'C') {
			return "C - CENTENA";
		} else if (tipo_jogo_bicho == 'CI') {
			return "CI - CENTENA INVERTIDA";
		} else if (tipo_jogo_bicho == 'G') {
			return "G - GRUPO";
		} else if (tipo_jogo_bicho == 'DG') {
			return "DG - DUQUE DE GRUPO 1 / 5";
		} else if (tipo_jogo_bicho == 'DG6') {
			return "DG6 - DUQUE DE GRUPO 6 / 10";
		} else if (tipo_jogo_bicho == 'DGC') {
			return "DGC - DUQUE DE GRUPO COMBINADO";			
		} else if (tipo_jogo_bicho == 'TG') {
			return "TG - TERNO DE GRUPO 1 / 5";
		} else if (tipo_jogo_bicho == 'TG10') {
			return "TG10 - TERNO DE GRUPO 1 / 10";
		} else if (tipo_jogo_bicho == 'TG6') {
			return "TG6 - TERNO DE GRUPO 6 / 10";
		} else if (tipo_jogo_bicho == 'TSE') {
			return "TSE - TERNO DE SEQUENCIA";
		} else if (tipo_jogo_bicho == 'TSO') {
			return "TSO - TERNO DE SOMA";
		} else if (tipo_jogo_bicho == 'TEX-A') {
			return "TEX-A - TERNO ESPECIAL ABERTO";
		} else if (tipo_jogo_bicho == 'TEX-F') {
			return "TEX-F - TERNO ESPECIAL FECHADO";
		} else if (tipo_jogo_bicho == 'QG') {
			return "QG - QUINA DE GRUPO";
		} else if (tipo_jogo_bicho == 'DZ') {
			return "DZ - DEZENA";
		} else if (tipo_jogo_bicho == 'DDZ') {
			return "DDZ - DUQUE DE DEZENA 1 / 5";
		} else if (tipo_jogo_bicho == 'DDZ6') {
			return "DDZ - DUQUE DE DEZENA 6 / 10";
		} else if (tipo_jogo_bicho == 'DDZC') {
			return "DDZC - DUQUE DE DEZENA COMBINADO";
		} else if (tipo_jogo_bicho == 'TDZ') {
			return "TDZ - TERNO DE DEZENA 1 / 5";
		} else if (tipo_jogo_bicho == 'TDZ6') {
			return "TDZ6 - TERNO DE DEZENA 6 / 10";
		} else if (tipo_jogo_bicho == 'TDZ10') {
			return "TDZ10 - TERNO DE DEZENA 1 / 10";
		} else if (tipo_jogo_bicho == 'TDZC') {
			return "TDZC - TERNO DE DEZENA COMBINADO";
		} else if (tipo_jogo_bicho == 'TGC') {
			return "TGC - TERNO DE GRUPO COMBINADO";
		} else if (tipo_jogo_bicho == 'PS') {
			return "PS - PASSE SECO";
		} else if (tipo_jogo_bicho == 'PS12') {
			return "PS12 - PASSE SECO 1 / 2";
		} else if (tipo_jogo_bicho == 'PC') {
			return "PC - PASSE COMBINADO";
		} else if (tipo_jogo_bicho == '5P100') {
			return "5P100 - 5 PRA 100";
		}
	}
}]);

app.filter('object2Array', function () {
	return function (input) {
		return angular.fromJson(input);
	}
});

app.filter('range', function () {
	return function (input, min, max) {
		min = parseInt(min);
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});

app.filter('fromMap', function() {
	return function(input) {
	  if (!input) {
		  return;
	  }
	  var out = {};
	  input.forEach((v, k) => out[k] = v);
	  return out;
	};
});

app.directive('loading', ['$http', '$rootScope', function ($http, $rootScope) {
	$rootScope.loading = false;
	return {
		restrict: 'A',
		link: function (scope, element) {
			scope.isLoading = function () {
				return $rootScope.loading;
			};
			scope.$watch(scope.isLoading, function (value) {
				if (value && element.css('display') == 'none') {
					element.css('display', 'flex');
				} else {
					element.css('display', 'none');
				}
			});
		}
	};
}]);