<?php

include "conexao.php";
require_once 'vendor/autoload.php';

if (!isset($_GET)) {
	die();
}

function getResumoDescricaoConcurso($registro)
{
	switch ($registro['tipo_jogo']) {
		case 'B':
		case '2':
			return $registro['hora_extracao'] . " - " . $registro['desc_hora'];
		case 'S':
		case 'Q':
		case 'L':
		case 'U':
			return $registro['concurso'];
		case 'R':
			return $registro['descricao'];
		default:
			return "";
	}
}

function getDescricaoTipoJogo($tipoJogoDoRegistro)
{
	switch ($tipoJogoDoRegistro) {
		case 'S':
			return "Seninha";
		case 'U':
			return "Super Sena";
		case 'Q':
			return "Quininha";
		case 'L':
			return "Lotinha";
		case 'B':
			return "Bicho";
		case '2':
			return $desc_2pra500;
		case 'R':
			return "Rifa";
		default:
			return "";
	}	
}

function getDescricaoStatusBilhete($statusBilhete)
{
	switch ($statusBilhete) {
		case 'A':
			return "Aberto";
		case 'C':
			return "Cancelado";
		case 'P':
			return "Processado";
		case 'G':
			return "Premiado";
		case 'F':
			return "Finalizado";			
		default:
			return "";
	}	
}

$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$perfil_usuario = mysqli_real_escape_string($con, $_GET['perfil_usuario']);
$site = mysqli_real_escape_string($con, $_GET['site']);
$dataDe = mysqli_real_escape_string($con, $_GET['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_GET['dataAte']);
$sort = mysqli_real_escape_string($con, $_GET['sort']);
$sortType = filter_var(mysqli_real_escape_string($con, $_GET['sortType']), FILTER_VALIDATE_BOOLEAN);
$codigoJogo = mysqli_real_escape_string($con, $_GET['concurso']);
$statusBilhete = mysqli_real_escape_string($con, $_GET['statusBilhete']);
$tipoJogo = mysqli_real_escape_string($con, $_GET['tipoJogo']);
$usuarioResponsavel = mysqli_real_escape_string($con, $_GET['usuarioResponsavel']);
$nomeApostador = mysqli_real_escape_string($con, $_GET['nomeApostador']);
$descTipoJogo = mysqli_real_escape_string($con, $_GET['descTipoJogo']);
$descConcurso = mysqli_real_escape_string($con, $_GET['descConcurso']);
$descVendedor = mysqli_real_escape_string($con, $_GET['descVendedor']);
$tipoRelatorio = mysqli_real_escape_string($con, $_GET['tipoRelatorio']);

if($dataDe == "undefined") {
    $dataDe = 0;
}

if($dataAte == "undefined") {
    $dataAte = 0;
}

if ($nomeApostador == "undefined") {
	$nomeApostador = "";
}

if ($statusBilhete == "undefined") {
	$statusBilhete = "";
}

if ($usuarioResponsavel == "undefined") {
	$usuarioResponsavel = "";
}

if ($codigoJogo == "undefined") {
	$codigoJogo = 0;
}

$response = [];

$query = "
select v.cod_bilhete, v.txt_pule, v.txt_pule_pai, v.txt_aposta, v.cambista, v.gerente, v.nome_apostador, v.telefone_apostador,
v.data_bilhete, v.concurso, v.tipo_jogo, v.tipo_jogo_dinamico, v.data_hora_bilhete, v.status_bilhete,
sum(v.qtd_bilhetes) qtd_bilhetes, sum(v.total) total, sum(v.comissao) comissao, sum(v.possivel_retorno) possivel_retorno, v.status_pagamento,
v.apostas_premiadas
from (
	select bi.cod_bilhete, bi.txt_pule, bi.txt_pule_pai, apo.txt_aposta, bi.status_pagamento, usu.nome cambista, geren.nome gerente, bi.nome_apostador, bi.telefone_apostador,
	DATE_FORMAT(bi.data_bilhete, '%d-%m-%Y') data_bilhete, cast(case
	when jogo.tipo_jogo IN('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
	when jogo.tipo_jogo = 'R' then jogo.DESCRICAO
	else jogo.concurso end as char) as concurso , jogo.tipo_jogo,
	(case
		  when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO))
		  else '' end) as tipo_jogo_dinamico,
	DATE_FORMAT(bi.data_bilhete, '%d/%m/%Y %H:%i:%S') data_hora_bilhete,
	count(apo.cod_aposta) qtd_bilhetes,  round(sum(apo.valor_aposta),2) total,
	sum(apo.comissao) as comissao, sum(case when apo.status = 'G' then 1 else 0 end) as apostas_premiadas,
	round(sum(apo.valor_ganho), 2) possivel_retorno,
	case
	when bi.status_bilhete = 'A' then 'Aberto'
	when bi.status_bilhete = 'C' then 'Cancelado'
	when bi.status_bilhete = 'F' then 'Finalizado'
	when bi.status_bilhete = 'P' then 'Processado' end status_bilhete
	from bilhete bi
	inner join aposta apo on (bi.cod_bilhete = apo.cod_bilhete)
	inner join usuario usu on (bi.cod_usuario = usu.cod_usuario)
	inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
	inner join jogo on (jogo.cod_jogo = apo.cod_jogo)
	left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
	left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
	where bi.cod_site = '$site'
	and ( (usu.cod_usuario = '$cod_usuario' or usu.cod_gerente = '$cod_usuario')  or (geren.cod_usuario = '$cod_usuario' or geren.cod_gerente = '$cod_usuario') ) ";
	
if ($tipoJogo) {
$query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
}

if ($codigoJogo > 0) {
$query = $query . " AND jogo.cod_jogo = '$codigoJogo' ";
}

if ($usuarioResponsavel != "") {
$query = $query . " AND (geren.nome = '$usuarioResponsavel' OR usu.nome = '$usuarioResponsavel') ";
}

if ($nomeApostador != "") {
$query = $query . " AND LOWER(bi.nome_apostador) LIKE LOWER('$nomeApostador%') ";
}

if ($statusBilhete) {
	if ($statusBilhete == "G") {
		$query = $query . " AND apo.status = 'G' ";
	} else {
		$query = $query . " AND bi.status_bilhete = '$statusBilhete' ";
	}
}

if ($dataDe != '0') {
	$query = $query . " AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d')  ";
}

if ($dataAte != '0') {
	$query = $query . " AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d')  ";
}

if ($dataDe == 0 && $dataAte == 0) {
	$query = $query . " AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d')  >= DATE_FORMAT(current_date - 7, '%Y%m%d')
				   AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d')  <= DATE_FORMAT(current_date + 1, '%Y%m%d') ";
}

$query = $query . " group by bi.cod_bilhete, bi.txt_pule, usu.nome, geren.nome, bi.nome_apostador, bi.telefone_apostador, bi.data_bilhete, jogo.concurso, jogo.tipo_jogo, apo.cod_aposta
	) v group by v.cod_bilhete, v.txt_pule, v.cambista, v.gerente, v.nome_apostador, v.telefone_apostador,
		v.data_bilhete, v.concurso, v.tipo_jogo, v.data_hora_bilhete, v.status_bilhete ";

$asc_desc = $sortType ? " ASC" : " DESC";
switch ($sort) {
	case "CODIGO":
		$query = $query . "ORDER BY txt_pule";
		break;
	case "GERENTE":
		$query = $query . "ORDER BY gerente";
		break;
	case "CAMBISTA":
		$query = $query . "ORDER BY cambista";
		break;
	case "CONCURSO":
		$query = $query . "ORDER BY concurso";
		break;
	case "TIPO_JOGO":
		$query = $query . "ORDER BY tipo_jogo";
		break;
	case "DATA":
		$query = $query . "ORDER BY STR_TO_DATE(data_hora_bilhete, '%d/%m/%Y %H:%i:%s')";
		break;
	case "APOSTADOR":
		$query = $query . "ORDER BY nome_apostador";
		break;
	case "TELEFONE":
		$query = $query . "ORDER BY telefone_apostador";
		break;
	case "QTD_APOSTAS":
		$query = $query . "ORDER BY qtd_bilhetes";
		break;
	case "TOTAL":
		$query = $query . "ORDER BY total";
		break;
	case "COMISSAO":
		$query = $query . "ORDER BY comissao";
		break;
	case "RETORNO":
		$query = $query . "ORDER BY possivel_retorno";
		break;
	case "STATUS_BILHETE":
		$query = $query . "ORDER BY status_bilhete";
		break;
	case "STATUS_PGTO":
		$query = $query . "ORDER BY case 
					when status_pagamento = 'N' and possivel_retorno > 0 then 2
					when status_pagamento = 'S'  then 1
					else 0 end";
		break;
	default:
		$query = $query . "ORDER BY data_hora_bilhete, '%d/%m/%Y %H:%i:%s')";
		break;
}		

$result = mysqli_query($con, $query);
$return_arr = array();

$totalizadores = "SELECT SUM(total) as total, " .
						"SUM(comissao) as comissao, " .
						"SUM(qtd_bilhetes) as qtd_bilhetes, " .
						"SUM(possivel_retorno) as possivel_retorno, " .
						"COUNT(1) as 'total_registros'" .
				 " FROM ( " . $query . " ) t";
$resultado_totalizador = mysqli_query($con, $totalizadores);
$row_totalizadores = mysqli_fetch_array($resultado_totalizador);				 

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
	$row_array['cod_bilhete'] = $row['cod_bilhete'];
	$row_array['txt_pule'] = $row['txt_pule'];
	$row_array['txt_pule_para_visualizar'] = $row['txt_pule'];
	$row_array['txt_aposta'] = $row['txt_aposta'];
	if ($row['txt_pule'] == $row['cod_bilhete']) {
		$row_array['txt_pule_para_visualizar'] = $row['txt_pule_pai'];
	}        
	$row_array['cambista'] = $row['cambista'];
	$row_array['nome_apostador'] = $row['nome_apostador'];
	$row_array['telefone_apostador'] = $row['telefone_apostador'];
	$row_array['data_bilhete'] = $row['data_bilhete'];
	$row_array['data_hora_bilhete'] = $row['data_hora_bilhete'];

	$row_array['qtd_bilhetes'] = $row['qtd_bilhetes'];
	$row_array['total'] = $row['total'];
	$row_array['comissao'] = $row['comissao'];
	$row_array['possivel_retorno'] = $row['possivel_retorno'];
	$row_array['concurso'] = $row['concurso'];
	$row_array['status_bilhete'] = $row['status_bilhete'];
	$row_array['gerente'] = $row['gerente'];
	$row_array['tipo_jogo'] = $row['tipo_jogo'];
	$row_array['tipo_jogo_dinamico'] = $row['tipo_jogo_dinamico'];
	$row_array['status_pagamento'] = $row['status_pagamento'];
	$row_array['bilhete_premiado'] = $row['apostas_premiadas'] > 0;

	array_push($return_arr, $row_array);

	if ($contador == mysqli_num_rows($result)) {
		break;
	}
};

$data_de = date_create($dataDe);
$data_ate = date_create($dataAte); 

$html = "
<html>
	<head>
		<meta charset='utf-8'>
		<style>
			table, th, td {
				border-collapse: collapse;
				font-family: Arial, Helvetica, sans-serif;
			}
			th, td {
				border: 1px solid black;
				padding: 3px;
			}
		</style>
	</head>
<body>
	<div class='col-md-12 col-ms-12 col-xs-12'>
		<div class='table-responsive betL_CaixaList' style='padding-top: 30px'>
			<h1> VER BILHETES </h1>			 
			<div style='display: inline;'>";

if ($tipoJogo) {
	$html = $html . 
			   "<span style='font-size: 12pt;'> <b>Tipo de Jogo:</b> " . $descTipoJogo . "</span>
				&nbsp;|&nbsp;";
}

if ($descConcurso) {
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>Concurso:</b> " . $descConcurso . "</span>
				&nbsp;|&nbsp;";	
}

if ($nomeApostador != "") {
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>Nome Apostador:</b> " . $nomeApostador . "</span>
				&nbsp;|&nbsp;";
}

if ($statusBilhete != "") {
	$descricaoStatusBilhete = getDescricaoStatusBilhete($statusBilhete);
	$html = $html . 
			   "<span style='font-size: 12pt;'> <b>Status Bilhete:</b> " . $descricaoStatusBilhete . "</span>
				&nbsp;|&nbsp;";
}

if ($descVendedor) {
	$labelVendedor = "Gerente:";
	if ($perfil_usuario == "G") {
		$labelVendedor = "Cambista:";
	}
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>" . $labelVendedor . "</b> " . $descVendedor . "</span>
				&nbsp;|&nbsp;";
}

$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>De:</b> " . date_format($data_de, 'd-m-Y') . "</span>
				&nbsp;|&nbsp;
				<span style='font-size: 12pt;'> <b>Até:</b> " . date_format($data_ate, 'd-m-Y') . "</span>
				&nbsp;
			</div>
			<hr>					 
			<table class='table table-hover' style='width: 100%;'>
				<thead>
					<tr style='background-color: #2E64FE; color: white;'>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>#</th>";
if ($perfil_usuario == 'A') {
	$html = $html .    "<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Gerente</th>";
}								
$html = $html .
					   "<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Cambista</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Concurso</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Tipo de Jogo</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Data</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Nome Apostador</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Telefone</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Números Apostados</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Qtd. Apostas</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Total (R$)</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Comissão (R$)</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Retorno (R$)</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Status Bilhete</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Situação Pagamento</th>
					</tr>
				</thead>
				<tbody class='container' id='conteudoTabela'>";
$odd = 0;				
foreach ($return_arr as $registro) {
	$odd = $odd + 1;
	if($odd % 2 == 0) {
		$html = $html . 
				   "<tr style='background-color: #E6E6E6;'>";
	} else {
		$html = $html . 
				   "<tr>";
	}
	$html = $html .
				   	   "<td style='text-align:center'>" . $registro['txt_pule_para_visualizar'] . "</td>";
	if ($perfil_usuario == 'A') {
		$html = $html .
				   	   "<td style='text-align:center'>" . $registro['gerente'] . "</td>";
	}
	$html = $html .
				   	   "<td style='text-align:center'>" . $registro['cambista'] . "</td>
						<td style='text-align:center'>" . $registro['concurso'] . "</td>";
	if ($registro['tipo_jogo'] == '2') {
		$html = $html .						
					   "<td style='text-align:center'>" . $registro['tipo_jogo_dinamico'] . "</td>";
	} else {
		$html = $html .						
					   "<td style='text-align:center'>" . getDescricaoTipoJogo($registro['tipo_jogo']) . "</td>";
	}
	$html = $html .					   
					   "<td style='text-align:center'>" . $registro['data_hora_bilhete'] . "</td>
					   	<td style='text-align:center'>" . $registro['nome_apostador'] . "</td>
						<td style='text-align:center'>" . $registro['telefone_apostador'] . "</td>
						<td style='text-align:center'>" . $registro['txt_aposta'] . "</td>
						<td style='text-align:center'>" . $registro['qtd_bilhetes'] . "</td>
						<td align='right'>R$ " . number_format($registro['total'], 2, ',', '.') . "</td>
						<td align='right'>R$ " . number_format($registro['comissao'], 2, ',', '.') . "</td>
						<td align='right'>R$ " . number_format($registro['possivel_retorno'], 2, ',', '.') . "</td>
						<td style='text-align:center'>" . $registro['status_bilhete'] . "</td>";
	if ($registro['bilhete_premiado']) {
		if ($registro['status_pagamento'] == 'S') {
			$html = $html .
					   "<td style='text-align:center'>
							<span style='color: #008000; font-weight: bold;'>
								Efetivado
							</span>
						</td>";
		} else {
			$html = $html .
					   "<td style='text-align:center'>
							<span style='color: #ff0000; font-weight: bold;'>
								Pendente
							</span>
						</td>";
		}
	} else {
		$html = $html .
					   "<td style='text-align:center'></td>";
	}
}
$html = $html .
				   "</tr>
				</tbody>
				<tr style='font-weight: bold; background-color: #A9F5BC;'>
					<th scope='row'> TOTAL </th>";
if ($perfil_usuario == 'A') {
	$html = $html .
				   "<td> - </td>";
}					

$html = $html .					
				   "<td> - </td>
				   	<td> - </td>
					<td> - </td>
				   	<td> - </td>
					<td> - </td>
				   	<td> - </td>
					<td align='center' style='font-weight: bold;'> " .  $row_totalizadores['qtd_bilhetes'] . " </td>
					<td align='right' style='font-weight: bold;'> R$ " . number_format($row_totalizadores['total'], 2, ',', '.') . " </td>
					<td align='right' style='font-weight: bold;'> R$ " . number_format($row_totalizadores['comissao'], 2, ',', '.') . " </td>
					<td align='right' style='font-weight: bold;'> R$ " . number_format($row_totalizadores['possivel_retorno'], 2, ',', '.') . " </td>
					<td> - </td>
					<td> - </td>
			    </tr>	
			</table>				 
		</div>
	</div>
 </body> 
</html> ";

$hoje = date('YmdHis');

if ($tipoRelatorio == 'xlsx') {
    $arquivo = "VerBilhetes_" . $hoje . ".xls";
	header ("Expires: Mon, 18 Nov 1985 18:00:00 GMT");
	header ("Last-Modified: ".gmdate("D,d M YH:i:s")." GMT");
	header ("Cache-Control: no-cache, must-revalidate");
	header ("Pragma: no-cache");
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");	
	header ("Content-Type: text/html; charset=UTF-8");
	    
    // Imprime o conteúdo da nossa tabela no arquivo que será gerado
    echo $html;    
} else if ($tipoRelatorio == 'pdf') {
    $arquivo = "VerBilhetes_" . $hoje . ".pdf";
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    $mpdf->Output($arquivo, "D");
} 

$con->close();

?>