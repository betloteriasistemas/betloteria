<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if(!isset($_POST)) die();

$response = [];

$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$site = mysqli_real_escape_string($con, $_POST['site']);

$query = "
	 SELECT usu.COD_SITE, usu.COD_USUARIO, usu.COD_GERENTE, usu.LOGIN, usu.PERFIL, usu.NOME, 
			 usu.PCT_COMISSAO_SENINHA, usu.PCT_COMISSAO_QUININHA, usu.PCT_COMISSAO_LOTINHA, usu.PCT_COMISSAO_BICHO, 
			 usu.PCT_COMISSAO_2PRA500, usu.PCT_COMISSAO_RIFA, usu.SENHA, usu.LIMITE, usu.SALDO, usu.STATUS,
			 usu.EMAIL, usu.FLG_SENINHA, usu.FLG_QUININHA, usu.FLG_LOTINHA, usu.FLG_BICHO, usu.FLG_2PRA500, 
			 usu.FLG_RIFA, usu.FLG_SUPER_SENA, usu.PCT_COMISSAO_SUPERSENA, usu.COD_AREA,
			 conf.COMISSAO_MILHAR_CENTENA, conf.COMISSAO_MILHAR_SECA, conf.COMISSAO_MILHAR_INVERTIDA, 
			 conf.COMISSAO_CENTENA, conf.COMISSAO_CENTENA_INVERTIDA, conf.COMISSAO_GRUPO, 
			 conf.COMISSAO_DUQUE_GRUPO, conf.COMISSAO_TERNO_GRUPO, conf.COMISSAO_QUINA_GRUPO, conf.COMISSAO_DEZENA, 
			 conf.COMISSAO_DUQUE_DEZENA, conf.COMISSAO_TERNO_DEZENA, conf.COMISSAO_PASSE_SECO, 
			 conf.COMISSAO_PASSE_COMBINADO, conf.COMISSAO_5P100
	 FROM usuario usu left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario
	 WHERE usu.COD_SITE = '$site' 
	 	AND usu.COD_USUARIO = '$cod_usuario'"; 

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
    $row_array['cod_usuario'] = $row['COD_USUARIO'];
	$row_array['cod_gerente'] = $row['COD_GERENTE'];
	$row_array['cod_site'] = $row['COD_SITE'];
	$row_array['login'] = $row['LOGIN'];
	$row_array['perfil'] = $row['PERFIL'];
	$row_array['nome'] = $row['NOME'];
	$row_array['pct_comissao_seninha'] = $row['PCT_COMISSAO_SENINHA'];
	$row_array['pct_comissao_quininha'] = $row['PCT_COMISSAO_QUININHA'];
	$row_array['pct_comissao_lotinha'] = $row['PCT_COMISSAO_LOTINHA'];
	$row_array['pct_comissao_bicho'] = $row['PCT_COMISSAO_BICHO'];
	$row_array['pct_comissao_2pra500'] = $row['PCT_COMISSAO_2PRA500'];
	$row_array['pct_comissao_rifa'] = $row['PCT_COMISSAO_RIFA'];
	$row_array['pct_comissao_supersena'] = $row['PCT_COMISSAO_SUPERSENA'];
	$row_array['senha'] = $row['SENHA'];
	$row_array['limite'] = $row['LIMITE'];
	$row_array['saldo'] = $row['SALDO'];
	$row_array['status'] = $row['STATUS'];
	$row_array['email'] = $row['EMAIL'];
	$row_array['flg_seninha'] = $row['FLG_SENINHA'];
	$row_array['flg_quininha'] = $row['FLG_QUININHA'];
	$row_array['flg_lotinha'] = $row['FLG_LOTINHA'];
	$row_array['flg_bicho'] = $row['FLG_BICHO'];
	$row_array['flg_2pra500'] = $row['FLG_2PRA500'];
	$row_array['flg_rifa'] = $row['FLG_RIFA'];
	$row_array['flg_supersena'] = $row['FLG_SUPER_SENA'];
	$row_array['cod_area'] = $row['COD_AREA'];

	$row_array['comissao_milhar_centena'] = $row['COMISSAO_MILHAR_CENTENA'];
	$row_array['comissao_milhar_seca'] = $row['COMISSAO_MILHAR_SECA'];
	$row_array['comissao_milhar_invertida'] = $row['COMISSAO_MILHAR_INVERTIDA'];
	$row_array['comissao_centena'] = $row['COMISSAO_CENTENA'];
	$row_array['comissao_centena_invertida'] = $row['COMISSAO_CENTENA_INVERTIDA'];
	$row_array['comissao_grupo'] = $row['COMISSAO_GRUPO'];
	$row_array['comissao_duque_grupo'] = $row['COMISSAO_DUQUE_GRUPO'];
	$row_array['comissao_terno_grupo'] = $row['COMISSAO_TERNO_GRUPO'];
	$row_array['comissao_quina_grupo'] = $row['COMISSAO_QUINA_GRUPO'];
	$row_array['comissao_dezena'] = $row['COMISSAO_DEZENA'];
	$row_array['comissao_duque_dezena'] = $row['COMISSAO_DUQUE_DEZENA'];
	$row_array['comissao_terno_dezena'] = $row['COMISSAO_TERNO_DEZENA'];
	$row_array['comissao_passe_seco'] = $row['COMISSAO_PASSE_SECO'];
	$row_array['comissao_passe_combinado'] = $row['COMISSAO_PASSE_COMBINADO']; 
	$row_array['comissao_5p100'] = $row['COMISSAO_5P100'];

	array_push($return_arr,$row_array);
	
	if ($contador == mysqli_num_rows($result)){
		break;
	}
};

$con->close();
echo json_encode($return_arr , JSON_NUMERIC_CHECK);