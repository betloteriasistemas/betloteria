angular.module('main').controller('caixaCtrl', function ($scope, user, $http, $filter, $rootScope, funcoes, $location, configGeral) {
    
    $scope.totalizador;
    $scope.totalizadorEntrada = 0;
    $scope.totalizadorSaida = 0;
    $scope.totalizadorSaldo = 0;

    $scope.totalGeralEntrada = 0;
    $scope.totalGeralSaida = 0;
    $scope.totalGeralSaldo = 0;

    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.chkExibirFinalizados = false;
    $scope.tiposJogos = funcoes.getTiposJogos();
    $scope.chkExibirLancamentos = true;
    $scope.isExibirGrids = localStorage.getItem('grid') ? true : false;    
    $scope.chkRelatorioSintetico = false;
    $scope.exibirColuna = true;
    $scope.selecionarGerente = {
        gerentes: [],
        gerenteSelecionado: ''
    };
    $scope.selecionarCambista = {
        cambistas: [],
        cambistaSelecionado: ''
    };
    var dadosCaixa = null;

    var data = new Date();

    var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
    var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
    var ano = data.getFullYear();

    var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
    var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
    var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };    

    $scope.edtDataDe = new Date();
    $scope.edtDataAte = new Date();
    let dataInicial;
    let dataFinal;
    $scope.index = 0;
    $scope.indexLancamentos = 0;

    $scope.consulta = false;
    $scope.records = [];
    $scope.lancamentos = [];
    $scope.comboTipoJogo = "";

    $scope.ordenacao = "ASC";
    $scope.campoOrdenacao = "data_jogo";

    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
    });

    $scope.tipo_relatorio = 'pdf';
    $scope.eh_pdf = true;
    
    $scope.alteraTipoRelatorio = function() {
        if ($scope.eh_pdf) {
            $scope.eh_pdf = true;
            $scope.tipo_relatorio = 'pdf';
        } else {
            $scope.eh_pdf = false;
            $scope.tipo_relatorio = 'xlsx';
        }
    }

    // PAGINAçÃO    

    $scope.totalItens = 0;
    $scope.totalItensLancamentos = 0;
    $scope.itensPorPagina = 20;

    $scope.paginacao = {
        atual: 1
    };
    
    $scope.paginacaoLancamento = {
        atual: 1
    };    
    
    $scope.alterarPagina = function(newPage) {
        getResultadoBusca(newPage);
    };
    
    function getResultadoBusca(pageNumber) {
        $scope.index = (pageNumber - 1) * $scope.itensPorPagina;
        if($scope.selecionarGerente.gerenteSelecionado.cod_usuario) {
            if($scope.selecionarCambista.cambistaSelecionado.cod_usuario) {
                console.log('teste')
                $scope.listarCaixa(
                    dataInicial, 
                    dataFinal, 
                    $scope.itensPorPagina,
                    $scope.selecionarGerente.gerenteSelecionado.cod_usuario,
                    $scope.selecionarCambista.cambistaSelecionado.cod_usuario
                );
            } else {
                $scope.listarCaixa(
                    dataInicial, 
                    dataFinal, 
                    $scope.itensPorPagina,
                    $scope.selecionarGerente.gerenteSelecionado.cod_usuario
                );
            }
        } else {
            
            $scope.listarCaixa(
                dataInicial, 
                dataFinal, 
                $scope.itensPorPagina
            );
        }
    }

    $scope.alterarPaginaLancamentos = function(newPage) {
        getResultadoBuscaLancamentos(newPage);
    };

    function getResultadoBuscaLancamentos(pageNumber) {
        $scope.indexLancamentos = (pageNumber - 1) * $scope.itensPorPagina;
        $scope.listarLancamentos(dataInicial, dataFinal, $scope.itensPorPagina);
    }
    
    $scope.exibirGrids = function() {
        $scope.isExibirGrids = !$scope.isExibirGrids;
        if ($scope.isExibirGrids) {
            localStorage.setItem('grid', 1);
        } else {
            localStorage.removeItem('grid');
        }
    }

    $scope.getJogos = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/jogos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + '&finalizados=false' + '&operacao=' 
                  + '&site=' + user.getSite() + '&status=' 
                  + '&index='
                  + '&qtdElementos=' 
                  + '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.jogos = response.data.filter(obj => !obj.totalizadores);
        }).catch(function (response) {            
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.orderFilter = function (campo) {
        
        if ($scope.ordenacao == "ASC") {
            $scope.ordenacao = "DESC";
        } else {
            $scope.ordenacao = "ASC";
        }

        $scope.campoOrdenacao = campo;
        $scope.listarCaixa(dataInicial, dataFinal, $scope.itensPorPagina);

    };

    $scope.getDescricaoTipoJogo = function (tipo_jogo) {         
        return funcoes.getDescricaoTipoJogo(tipo_jogo, $scope.desc_2pra500);        
    }

    $scope.getDescricaoConcurso = function(jogo){
        return funcoes.getDescricaoConcurso(jogo);
    }

    $scope.getDescricaoStatus = function (status) {
        return funcoes.getDescricaoStatus(status); 
    }

    $scope.listarCaixa = function (
                                   dataDe, 
                                   dataAte, 
                                   qtdElementos,
                                   cod_usuario_gerente,
                                   cod_usuario_cambista,
                                   buscarTotalizadores
                                   ) {
        
        if (!buscarTotalizadores) {
            buscarTotalizadores = 1;
        }

        if (isPesquisarCaixaCambista()) {
            $scope.itensPorPagina = 20;
        }

        if(!qtdElementos) {
            qtdElementos = $scope.itensPorPagina;
        } 

        if (!$scope.comboCambista) {
            $scope.comboCambista = "";
        } 

        if (cod_usuario_cambista) {
            $scope.comboCambista = cod_usuario_cambista;
        }

        if (!cod_usuario_gerente) {
            cod_usuario_gerente = "";
        }

        if(!$scope.comboTipoJogo){
            $scope.comboTipoJogo = "";
        }

        if(!$scope.comboStatus){
            $scope.comboStatus = "";
        }

        if(!$scope.comboConcurso){
            $scope.comboConcurso = "";
        }

        if (!$scope.campoOrdenacao) {
            $scope.campoOrdenacao = 'data_jogo';
        }

        if (!$scope.ordenacao) {
            $scope.ordenacao = "ASC";
        }

	    if (typeof dataDe === 'undefined' || dataDe == null) {
            dataDe = '0';
        }
        if (typeof dataAte === 'undefined' || dataDe == null) {
            dataAte = '0';
        }

        if (typeof $scope.index === 'undefined' || $scope.index == null) {
            $scope.index = 0;
        }   
        $rootScope.loading = true;
        $scope.consulta = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/caixa.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() 
                  + '&finalizados=' + $scope.chkExibirFinalizados 
                  + '&perfil=' + $scope.perfil
                  + '&cod_usuario=' + user.getCodigo() 
                  + '&dataDe=' + dataDe + '&dataAte=' + dataAte 
                  + '&codigoUsuario=' + $scope.comboCambista
                  + '&codigoUsuarioGerente=' + cod_usuario_gerente
                  + '&tipoJogo=' + $scope.comboTipoJogo
                  + '&statusJogo=' + $scope.comboStatus 
                  + '&concurso=' + $scope.comboConcurso 
                  + '&index=' + $scope.index
                  + '&qtdElementos=' + qtdElementos
                  + '&buscarTotalizadores=' + buscarTotalizadores
                  + '&campoOrdenacao=' + $scope.campoOrdenacao
                  + '&ordenacao=' + $scope.ordenacao 
                  + '&relatorioSintetico=' + $scope.chkRelatorioSintetico 
                  + '&pesquisarCaixaCambista=' + isPesquisarCaixaCambista()
                  + '&schema=' + user.getSchema()
        }).then(function (response) {
            if(buscarTotalizadores == 1){
                $scope.totalizador = response.data.filter(obj => obj.totalizadores)[0].totalizadores;
                $scope.totalItens = $scope.totalizador.total_registros;
            }
            if (!isPesquisarCaixaCambista()) {
                $scope.totalizadorEntrada = $scope.totalizador.entradas;
                $scope.totalizadorSaida = parseFloat($scope.totalizador.saidas) + parseFloat($scope.totalizador.comissao);
                if (parseFloat($scope.totalizador.comissao_gerente) >= 0) {
                    $scope.totalizadorSaida = parseFloat($scope.totalizadorSaida) + parseFloat($scope.totalizador.comissao_gerente);
                    $scope.totalizadorSaldo = $scope.totalizador.saldo;
                } else {
                    $scope.totalizadorSaldo = $scope.totalizador.saldo_par;
                }    
                if ($scope.chkExibirLancamentos) {
                    $scope.listarLancamentos(dataInicial, dataFinal, $scope.itensPorPagina);
                } 
            }
            $scope.records = response.data.filter(obj => !obj.totalizadores);
            $scope.totalItems = $scope.records.length;
            $scope.totalItems = Math.ceil($scope.totalItems / 8) * 10;  
                  
        }).catch(function (err){
            $.alert('Erro no $HTTP: ' + err.data);
        }).finally(function() {
            $rootScope.loading = false;
            $scope.mostrarFooterTabela = true;
            if($scope.records) {
                scrollParaTabela();
            }
        });

    }

    $scope.listarLancamentos = function (dataDe, dataAte,
        qtdElementos = $scope.itensPorPagina) {
        if ($scope.perfil != 'C') {
            if(!$scope.comboCambista) {
                $scope.comboCambista = "";
            }

            if (typeof dataDe === 'undefined' || dataDe == null) {
                dataDe = '0';
            }
            if (typeof dataAte === 'undefined' || dataDe == null) {
                dataAte = '0';
            }

            if (typeof $scope.indexLancamentos === 'undefined' || $scope.indexLancamentos == null) {
                $scope.indexLancamentos = 0;
            }   
            $rootScope.loading = true;
            $scope.consulta = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/caixa_lancamentos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() 
                    + '&finalizados=' + $scope.chkExibirFinalizados 
                    + '&perfil=' + $scope.perfil 
                    + '&cod_usuario=' + user.getCodigo() 
                    + '&dataDe=' + dataDe + '&dataAte=' + dataAte 
                    + '&codigoUsuario=' + $scope.comboCambista
                    + '&index=' + $scope.indexLancamentos
                    + '&qtdElementos=' + qtdElementos        
                    + '&schema=' + user.getSchema()        
            }).then(function (response) {
                $scope.lancamentos = response.data.filter(obj => obj.lancamentos)[0].lancamentos;
                $scope.totalizadoresLancamentos = response.data.filter(obj => obj.totalizadoresLancamentos)[0].totalizadoresLancamentos;
                $scope.totalItensLancamentos = $scope.totalizadoresLancamentos.total_registros;         
                $scope.totalGeralEntrada = parseFloat($scope.totalizadorEntrada) + parseFloat($scope.totalizadoresLancamentos.entradas);
                $scope.totalGeralSaida = parseFloat($scope.totalizadorSaida) + parseFloat($scope.totalizadoresLancamentos.saidas);
                $scope.totalGeralSaldo = parseFloat($scope.totalizadorSaldo) + parseFloat($scope.totalizadoresLancamentos.saldo);
            }).catch(function (err){
                $.alert('Erro no $HTTP: ' + err.data);
            }).finally(function() {
                $rootScope.loading = false;
                $scope.mostrarFooterTabela = true;
                if($scope.lancamentos) {
                    scrollParaTabela();
                }
            });
        }
    }

    $scope.ajustaDataFimPeriodo = function () {
        if($scope.edtDataDe > $scope.edtDataAte) {
            $scope.edtDataAte = $scope.edtDataDe;
        }
    }

    function isPesquisarCaixaCambista() {
        return $location.url() === '/caixa_cambista';
    }

    $scope.pesquisar = function () {
        
        if($scope.edtDataDe > $scope.edtDataAte) {
            $.alert('A data final do período deve ser maior ou igual a data inicial.');
            return;
        }
        
        $scope.index = 0;
        $scope.indexLancamentos = 0;
        $scope.records = [];
        $scope.lancamentos = [];
        $scope.mostrarFooterTabela = false;
        dataInicial  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        dataFinal = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");

        if ($scope.chkRelatorioSintetico) {
            $scope.exibirColuna = false;
        } else {
            $scope.exibirColuna = true;
        }

        if($scope.selecionarGerente.gerenteSelecionado.cod_usuario) {
            if($scope.selecionarCambista.cambistaSelecionado.cod_usuario) {
                $scope.listarCaixa(
                    dataInicial, 
                    dataFinal, 
                    $scope.itensPorPagina,
                    $scope.selecionarGerente.gerenteSelecionado.cod_usuario,
                    $scope.selecionarCambista.cambistaSelecionado.cod_usuario
                );
            } else {
                $scope.listarCaixa(
                    dataInicial, 
                    dataFinal, 
                    $scope.itensPorPagina,
                    $scope.selecionarGerente.gerenteSelecionado.cod_usuario
                );
            }
        } else {
            
            $scope.listarCaixa(
                dataInicial, 
                dataFinal, 
                $scope.itensPorPagina
            );
        }
        
        // $scope.listarCaixa(
        //             dataInicial, 
        //             dataFinal, 
        //             $scope.itensPorPagina,
        //         );
        // $scope.listarCaixa(dataDe,dataAte);
    }

    function isValidaData(dataInicial, dataFinal) {
        if(!dataInicial || dataInicial.split('-')[0].length > 4){
            $.alert('Data mínima 01/01/2019');
            return;
        }
        if(!dataFinal || dataFinal.split('-')[0].length > 4){
            $.alert('Data mínima 01/01/2019');
            return;
        }
        return true;
    }  

    $scope.gerarRelatorioCaixa = function () {
        var dataDe  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        var dataAte = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");
        window.open('angularjs-mysql/gerarRelatorioCaixa.php?site=' + user.getSite()  
                  + '&finalizados=' + $scope.chkExibirFinalizados
                  + '&lancamentos=' + $scope.chkExibirLancamentos
                  + '&perfil=' + $scope.perfil
                  + '&cod_usuario=' + user.getCodigo()
                  + '&dataDe=' + dataDe
                  + '&dataAte=' + dataAte
                  + '&tipoJogo=' + $scope.comboTipoJogo
                  + '&cambista=' + $scope.comboCambista
                  + '&concurso=' + $scope.comboConcurso
                  + '&status=' + $scope.comboStatus
                  + '&relatorioSintetico=' + $scope.chkRelatorioSintetico
                  + '&campoOrdenacao=' + $scope.campoOrdenacao
                  + '&ordenacao=' + $scope.ordenacao
                  + '&schema=' + user.getSchema()
                  + '&tipoRelatorio=' + $scope.tipo_relatorio);            
    }
    
    $scope.gerarRelatorioCaixaCambista = function () {
        var dataDe  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        var dataAte = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");

        var tipoJogo = $scope.comboTipoJogo;
        var descTipoJogo = "";
        if (tipoJogo) {
            descTipoJogo = $("#comboTipoJogo")[0].selectedOptions[0].label
        }

        var codGerente = $scope.selecionarGerente.gerenteSelecionado.cod_usuario;
        var descGerente = "";
        if (codGerente != null) {
            descGerente = $("#selecionarGerente")[0].selectedOptions[0].label
        }

        var codCambista = $scope.selecionarCambista.cambistaSelecionado.cod_usuario;
        var descCambista = "";
        if (codCambista != null) {
            descCambista = $("#selecionarCambista")[0].selectedOptions[0].label
        }

        window.open('angularjs-mysql/gerarRelatorioCaixaCambista.php?site=' + user.getSite()  
                  + '&perfil=' + $scope.perfil
                  + '&cod_usuario=' + user.getCodigo()
                  + '&tipoJogo=' + $scope.comboTipoJogo
                  + '&statusJogo=' + $scope.comboStatus
                  + '&dataDe=' + dataDe
                  + '&dataAte=' + dataAte
                  + '&codigoUsuarioGerente=' + $scope.selecionarGerente.gerenteSelecionado.cod_usuario
                  + '&codigoUsuarioCambista=' + $scope.selecionarCambista.cambistaSelecionado.cod_usuario
                  + '&finalizados=' + $scope.chkExibirFinalizados
                  + '&relatorioSintetico=' + $scope.chkRelatorioSintetico
                  + '&campoOrdenacao=' + $scope.campoOrdenacao
                  + '&ordenacao=' + $scope.ordenacao
                  + '&tipoRelatorio=' + $scope.tipo_relatorio
                  + '&descTipoJogo=' + descTipoJogo
                  + '&descGerente=' + descGerente
                  + '&schema=' + user.getSchema()
                  + '&descCambista=' + descCambista);            
    } 

    $scope.exibirFinalizados = function () {
        $scope.chkExibirFinalizados = !$scope.chkExibirFinalizados;
    }

    $scope.exibirLancamentos = function () {
        $scope.chkExibirLancamentos = !$scope.chkExibirLancamentos;

        if ($scope.chkExibirLancamentos) {

            if($scope.edtDataDe > $scope.edtDataAte) {
                $.alert('A data final do período deve ser maior ou igual a data inicial.');
                return;
            }
            
            $scope.indexLancamentos = 0;
            $scope.lancamentos = [];
            $scope.mostrarFooterTabela = false;
            dataInicial  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
            dataFinal = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");
            $scope.listarLancamentos(dataInicial, dataFinal, $scope.itensPorPagina);
        }
    }

    $scope.exibirRelatorioSintetico = function () {
        $scope.chkRelatorioSintetico = !$scope.chkRelatorioSintetico;
    }

    $scope.getCambistas = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + 
                  '&site=' + user.getSite() + 
                  '&operacao=getCambistas' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.cambistas = response.data;
            if($scope.perfil == 'A' && isPesquisarCaixaCambista()) {
                $scope.selecionarGerente.gerentes = $scope.cambistas;
                $scope.selecionarGerente.gerentes.splice(0, 0, {
                    nome: '--- Selecione um gerente ---',
                    cod_usuario: null
                })
                $scope.selecionarGerente.gerenteSelecionado = $scope.selecionarGerente.gerentes[0];
                $scope.getCambistasDeGerentes($scope.selecionarGerente.gerenteSelecionado.cod_usuario);
            }
        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.getCambistasDeGerentes = function (cod_gerente) {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + cod_gerente + 
                  '&site=' + user.getSite() + 
                  '&operacao=getCambistas' + 
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.selecionarCambista.cambistas = response.data;
            $scope.selecionarCambista.cambistas.splice(0, 0, {
                nome: '--- Selecione um Cambista ---',
                cod_usuario: null
            })
            $scope.selecionarCambista.cambistaSelecionado = $scope.selecionarCambista.cambistas[0];

        }).catch(function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }
    $scope.lidarSelecaoGerente = function() {
        $scope.getCambistasDeGerentes($scope.selecionarGerente.gerenteSelecionado.cod_usuario)
    }

    $scope.getTotalizador = function (campo) {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")
                &&
                (typeof $scope.comboTipoJogo === "undefined" ||
                    $scope.comboTipoJogo == $scope.records[i].tipo_jogo ||
                    $scope.comboTipoJogo == "")

            ) {
                if (campo == "entradas") {
                    total += parseFloat($scope.records[i].entradas);
                } else if (campo == "saidas") {
                    total += parseFloat($scope.records[i].saidas);
                } else if (campo == "saldo") {
                    total += parseFloat($scope.records[i].saldo);
                } else if (campo == "comissao") {
                    total += parseFloat($scope.records[i].comissao);
                } else if (campo == "comissao_gerente") {
                    total += parseFloat($scope.records[i].comissao_gerente);
                } else if (campo == "qtd_apostas") {
                    total += parseFloat($scope.records[i].qtd_apostas);
                } else if (campo == "saldo_par") {
                    total += parseFloat($scope.records[i].saldo_par);
                }
            }
        }
        
        if (campo != "qtd_apostas") {
            return total.toFixed(2);
        } else {
            return total;
        }

    }

    $scope.getTotalEntradas = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")

            ) {
                total += parseFloat($scope.records[i].entradas);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalSaidas = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].saidas);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalSaldo = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].saldo);
            }
        }
        return total.toFixed(2);
    }
    $scope.getTotalComissao = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].comissao);
            }
        }
        return total.toFixed(2);
    }

    $scope.getTotalComissaoGerente = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {
            if ((typeof $scope.comboCambista === "undefined" ||
                $scope.comboCambista == $scope.records[i].nome ||
                $scope.comboCambista == "")
                &&
                (typeof $scope.comboConcurso === "undefined" ||
                    $scope.comboConcurso == $scope.records[i].concurso ||
                    $scope.comboConcurso == "")
                &&
                (typeof $scope.comboStatus === "undefined" ||
                    $scope.comboStatus == $scope.records[i].tp_status ||
                    $scope.comboStatus == "")) {
                total += parseFloat($scope.records[i].comissao_gerente);
            }
        }
        return total.toFixed(2);
    }
    
    function scrollParaTabela() {
        $('html, body').animate({ scrollTop: $('#dir-paginator-top').offset().top - 25 }, 'slow');
    }

    $scope.filterByTipo = function (jogo) {
        return $scope.comboTipoJogo == "" || jogo.tipo_jogo == $scope.comboTipoJogo;
    }
});
