<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');

$cod_jogo = mysqli_real_escape_string($con, $_GET['id']);
$site = mysqli_real_escape_string($con, $_GET['site']);
$area = mysqli_real_escape_string($con, $_GET['area']);
if ($cod_jogo == "") {
    print "Informe o Código do Jogo";
    exit;
}

$query = 'SET @@session.time_zone = "-03:00"';
$result = mysqli_query($con, $query);

$query = " select v.*,
case when v.tipo_jogo != 'B' then '' else b.codigo end as grupo,
case when v.tipo_jogo != 'B' then '' else b.descricao end as bicho,
site.nome_banca,    
concat(DATE_FORMAT(current_date(), '%d/%m/%Y'), ' ', TIME_FORMAT(CURRENT_TIME(), '%H:%i:%s')) hoje,    
(select msg_bilhete from configuracao where cod_site = v.cod_site and cod_area = '$area') msg_bilhete
from (
select
cod_site,
cod_jogo,
cast(case
		when jogo.tipo_jogo in ('B','2') then
		concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'),
               case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
        when jogo.tipo_jogo = 'R' THEN jogo.descricao
		else concat(jogo.concurso, '-', DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'))   end as char) as concurso ,
tipo_jogo,
data_jogo,
hora_extracao,
case when tipo_jogo in ('S','Q','L','U') then LPAD(NUMERO_1,2,'0') 
when tipo_jogo = 'R' then LPAD(NUMERO_1,3,'0')
else LPAD(NUMERO_1,4,'0') end numero, '1 º' AS valor, 1 as ordenar
from jogo
where cod_jogo = '$cod_jogo' UNION  ";

  $numeroPremios = 15;

for ($i = 2; $i <= $numeroPremios; $i++) {
    $query = $query . " select
    cod_site,
    cod_jogo,
    '' concurso,
    tipo_jogo,
    data_jogo,
    hora_extracao,    
    case when tipo_jogo != 'B' then LPAD(NUMERO_" . $i . ",2,'0') else LPAD(NUMERO_" . $i . ",4,'0') end numero, '$i º' AS valor, '$i' as ordenar			
    from jogo
    where cod_jogo = '$cod_jogo'
      and tipo_jogo != '2' ";

    if ($i != $numeroPremios) {
        $query = $query . " UNION ";
    }
}

$query = $query .
") v
	inner join bicho b on (b.numero_1 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) or
						b.numero_2 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) or
						b.numero_3 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) or
						b.numero_4 = substr(v.NUMERO,length(v.NUMERO)-1,length(v.NUMERO)) )
                        
	inner join site on (site.cod_site = v.cod_site)     
    order by cast(ordenar as signed), tipo_jogo, data_jogo, hora_extracao ";

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    
    $row_array['cod_site'] = $row['cod_site'];
    $row_array['cod_jogo'] = $row['cod_jogo'];
    $row_array['concurso'] = $row['concurso'];
    $row_array['tipo_jogo'] = $row['tipo_jogo'];
    $row_array['data_jogo'] = $row['data_jogo'];
    $row_array['numero'] = $row['numero'];
    $row_array['valor'] = $row['valor'];
    $row_array['grupo'] = $row['grupo'];
    $row_array['bicho'] = $row['bicho'];
    $row_array['nome_banca'] = $row['nome_banca'];
    $row_array['hoje'] = $row['hoje'];
    $row_array['msg_bilhete'] = $row['msg_bilhete'];        

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}

if (count($return_arr) <= 0) {
    print "Registro não existe";
    exit;
}

$mensagem_bilhete = $row['msg_bilhete'];
switch ($return_arr[0]['tipo_jogo']) {
    case "2":
        $tipo_jogo = '2 PRA 500';
    break;
    case "Q":
        $tipo_jogo = 'QUININHA';
    break;
    case "S":
        $tipo_jogo = 'SENINHA';
    case "U":
        $tipo_jogo = 'SUPER SENA';        
    break;
    case "L":
        $tipo_jogo = 'LOTINHA';
    break;
    case "B":
        $tipo_jogo = 'BICHO';
    break;
    case "R":
        $tipo_jogo = 'RIFA';
    break;
    default:
    $tipo_jogo = '';
    break;
}

print "!LF<br/>";
print "!AC~" . $return_arr[0]['nome_banca'] . "~True~False~True<br/>";
print "!LP<br/>";
print "!AE~DATA: " . $return_arr[0]['hoje'] . "~False~False~False<br/>";
print "!AE~CONCURSO: " . $return_arr[0]['concurso'] . "~False~False~False<br/>";
print "!LP<br/>";
print "!AC~" . $tipo_jogo . "~False~False~False<br/>";
print "!LP<br/>";
foreach ($return_arr as $e) {

    $strAux = "";

    if ($tipo_jogo == 'BICHO'){
        $strAux = $e['valor'] . " - ". $e['numero'] . " " . str_pad($e['grupo'],1,'0') . " ". $e['bicho'];
    }else{
        $strAux = $e['valor'] . " - ". $e['numero'];
    }
    print "!AE~". $strAux . "~True~False~False<br/>";
}
print "!LF<br/>";
print "!AE~".$mensagem_bilhete."~False~False~False<br/>";    
print "!LF<br/>";
print "!LF<br/>";
print "!LF<br/>";
print "!LF<br/>";

 $con->close();
