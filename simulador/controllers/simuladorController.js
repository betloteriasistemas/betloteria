angular.module('main').controller('simuladorCtrl', function ($scope, $http, $rootScope) {

    $scope.apostas =  [];

    $scope.gerar = function(){
      $scope.apostas = [];
      var numeroConcursos = $scope.comboNumeroConcursos;
      var numeroApostas = $scope.comboQtdApostas;
      var qtdNumeros = $scope.comboQtdNumeros;
      var valorBilhete = $scope.edtValorMedio;
      var tipoJogo = $scope.comboTipoJogo;
      var comissao = $scope.edtComissao;

      var listaApostas = [];

      var resultadoConcursos = [];

      for (i=1; i<=numeroConcursos;i++){
        var obj = {
          concurso: i,
          resultado: $scope.gerarResultado(tipoJogo)
        };
        resultadoConcursos.push(obj);

        for (j=0; j<numeroApostas;j++){
          var arrayNumeros = $scope.gerarApostaAleatoria(qtdNumeros, tipoJogo);

          var objeto = {
            numeros: arrayNumeros,
            valor: valorBilhete,
            qtd: qtdNumeros,
            tipo: tipoJogo,
            concurso: i
            //possivelRetorno: iRetorno,
            //possivelRetornoReal: iPossivelRetornoReal
        };

        $scope.apostas.push(objeto);
        }

      }


      var qtdAcertos = 6;
       if (tipoJogo == 'Q'){
         qtdAcertos = 5;
       }

       if (tipoJogo == 'L'){
         qtdAcertos = 15;
       }

      var apostasWin = 0;
      var apostasLose = 0;
      $scope.records = [];
       for (i=0; i<$scope.apostas.length; i++){


        var codCon = $scope.apostas[i].concurso;
        var arrayResult = [];
        for (x=0; x<resultadoConcursos.length; x++){
          if (codCon == resultadoConcursos[x].concurso){
             arrayResult = resultadoConcursos[x].resultado;
             break;
          }
        }

        var counter = 0;
        for (x=0; x<$scope.apostas[i].numeros.length; x++){
          if (arrayResult.includes($scope.apostas[i].numeros[x]) ){
            counter = counter + 1;
          }
        }



        if (counter == qtdAcertos){
          apostasWin = apostasWin + 1;
        }else{
          apostasLose = apostasLose + 1;
        }
      }

      var fatorMultiplicador = 1;
      if (qtdNumeros == 13 || qtdNumeros == 14 ){
        fatorMultiplicador = 6500;
      }else if (qtdNumeros == 15){
        fatorMultiplicador = 4000;
      }else if (qtdNumeros == 16 && tipoJogo != 'L' ){
        fatorMultiplicador = 3000;
      }else if (tipoJogo == 'L' && qtdNumeros == 16){
        fatorMultiplicador = 500;
      }else if (qtdNumeros == 17 && tipoJogo != 'L' ){
        fatorMultiplicador = 2000;
      }else if (tipoJogo == 'L' && qtdNumeros == 17){
        fatorMultiplicador = 200;
      }else if (qtdNumeros == 18 && tipoJogo != 'L' ){
        fatorMultiplicador = 1500;
      }else if (tipoJogo == 'L' && qtdNumeros == 18){
        fatorMultiplicador = 100;
      }else if (qtdNumeros == 19 && tipoJogo != 'L' ){
        fatorMultiplicador = 1000;
      }else if (tipoJogo == 'L' && qtdNumeros == 19){
        fatorMultiplicador = 50;
      }else if (qtdNumeros == 20 && tipoJogo != 'L' ){
        fatorMultiplicador = 900;
      }else if (tipoJogo == 'L' && qtdNumeros == 20){
        fatorMultiplicador = 25;
      }else if (tipoJogo == 'L' && qtdNumeros == 21){
        fatorMultiplicador = 15;
      }else if (tipoJogo == 'L' && qtdNumeros == 22){
        fatorMultiplicador = 8 ;
      }else if (qtdNumeros == 25){
        fatorMultiplicador = 500;
      }else if (qtdNumeros == 30){
        fatorMultiplicador = 200;
      }else if (qtdNumeros == 35){
        fatorMultiplicador = 50;
      }else if (qtdNumeros == 40 || qtdNumeros == 45){
        fatorMultiplicador = 8;
      };

      var valorMulti = valorBilhete * fatorMultiplicador;
      if ( valorMulti > 30000 ){
        valorMulti = 30000;
      }

      var entradasReal = (apostasLose + apostasWin)  * valorBilhete;
      var saidasReal = apostasWin * valorMulti;
      var comissaoReal = entradasReal * comissao / 100;

      var objetoRec = {
        apostasPerdedoras: apostasLose,
        apostasGanhadoras: apostasWin,
        entradas: entradasReal,
        saidas:saidasReal,
        saldo: entradasReal - saidasReal - comissaoReal,
        comissao: comissaoReal
      };

      $scope.records.push(objetoRec);

    }

    $scope.gerarApostaAleatoria = function(qtd, tipoJogo){
      var contador = 0;
      var arrayPintados = [];
      var qtdTipo = 60;
      if (tipoJogo == 'Q'){
        qtdTipo = 80;
      }
      if (tipoJogo =='L'){
        qtdTipo = 25;
      }

      while (contador < qtd) {

          var pintou = false;
          var randomico = Math.floor(Math.random() * qtdTipo) + 1;

          for (ix = 0; ix < arrayPintados.length; ix++) {
              if (arrayPintados[ix] == randomico) {
                  pintou = true;
                  break;
              }
          }
          if (pintou) {
              continue;
          }

          arrayPintados.push(randomico);
          contador = contador + 1;
      }

      return arrayPintados;

    }

    $scope.gerarResultado = function(tipoJogo){
      var contador = 0;
      var arrayPintados = [];
      var qtdTipo = 60;
      if (tipoJogo == 'Q'){
        qtdTipo = 80;
      }
      if (tipoJogo == 'L'){
        qtdTipo = 25;
      }

      var dezenasSorteadas = 6;
      if (tipoJogo == 'Q'){
        dezenasSorteadas = 5;
      }
      if (tipoJogo == 'L'){
        dezenasSorteadas = 15;
      }

      while (contador < dezenasSorteadas) {

          var pintou = false;
          var randomico = Math.floor(Math.random() * qtdTipo) + 1;

          for (ix = 0; ix < arrayPintados.length; ix++) {
              if (arrayPintados[ix] == randomico) {
                  pintou = true;
                  break;
              }
          }
          if (pintou) {
              continue;
          }

          arrayPintados.push(randomico);
          contador = contador + 1;
      }

      return arrayPintados;

    }



});
