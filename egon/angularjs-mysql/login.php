<?php

header('Access-Control-Allow-Origin: *'); 
header('Content-Type: application/json');

if(!isset($_POST)) die();

session_start();

// Conexão Azure
$con_login=mysqli_init(); 
mysqli_real_connect($con_login, "betloteriaprd.mysql.database.azure.com", "betloteria@betloteriaprd", "brq@Rol@maxima", "betloteria", 3306);

// Conexão Local
//$con_login = mysqli_connect('localhost', 'betloteria@betloteriaprd', 'brq@Rol@maxima', 'betloteria');

mysqli_set_charset( $con_login, 'utf8');

$response = [];

$username = mysqli_real_escape_string($con_login, $_POST['username']);
$password = mysqli_real_escape_string($con_login, $_POST['password']);
$site = mysqli_real_escape_string($con_login, $_POST['site']);
$escolhido = mysqli_real_escape_string($con_login, $_POST['escolhido']);

$query = "SELECT * FROM site
		  WHERE cod_site='$site' and status = 'A' ";
$result = mysqli_query($con_login, $query);		 

if(mysqli_num_rows($result) == 0) {

	$response['status'] = 'error';
	
} else {

	// SELECIONA O SCHEMA DO RESPECTIVO SITE. CASO NAO ENCONTRE, UTILIZARA O SCHEMA BETLOTERIA
	$schema_selecionado = 'b' . $site;

	$con=mysqli_init();
	// CONEXAO AZURE
	mysqli_real_connect($con, "betloteriaprd.mysql.database.azure.com", "betloteria@betloteriaprd", "brq@Rol@maxima", $schema_selecionado, 3306);
	// CONEXAO LOCAL
	//$con = mysqli_connect('localhost', 'betloteria@betloteriaprd', 'brq@Rol@maxima', $schema_selecionado);

	$query = "SELECT * FROM usuario usu 
			inner join site on (usu.cod_site = site.cod_site)  
			WHERE usu.status = 'A' and usu.login='$username' 
			AND usu.senha='$password' AND usu.cod_site='$site' and site.status = 'A' ";
	try {
		$result = mysqli_query($con, $query);

		if(mysqli_num_rows($result) > 0) {
			$response['status'] = 'loggedin';

			$row=mysqli_fetch_assoc($result);
			$response['nome'] = $row['NOME'];
			$response['user'] = $username;
			$response['codigo'] = $row['COD_USUARIO'];
			$response['site'] = $row['COD_SITE'];
			$response['perfil'] = $row['PERFIL'];
			$response['saldo'] = $row['SALDO'];

			$response['flg_seninha'] = $row['FLG_SENINHA'];
			$response['flg_quininha'] = $row['FLG_QUININHA'];
			$response['flg_lotinha'] = $row['FLG_LOTINHA'];
			$response['flg_bicho'] = $row['FLG_BICHO'];
			$response['flg_2pra500'] = $row['FLG_2PRA500'];
			$response['flg_rifa'] = $row['FLG_RIFA'];
			$response['flg_multijogos'] = $row['FLG_MULTIJOGOS'];
			$response['flg_supersena'] = $row['FLG_SUPER_SENA'];
			$response['area'] = $row['COD_AREA'];

			$response['dominio'] = $row['TXT_EMPRESA'];

			$response['escolhido'] = $escolhido;
			$response['schema'] = $schema_selecionado;

			$response['id'] = md5(uniqid());
			$_SESSION['id'] = $response['id'];
			$_SESSION['user'] = $username;
			$_SESSION['site'] = $site;
			$_SESSION['perfil'] = $row['PERFIL'];
			$_SESSION['saldo'] = $row['SALDO'];
			$_SESSION['schema'] = $schema_selecionado;

		} else {	
			$response['status'] = 'error';
		}
	}catch(Exception $e) {
		echo $e->getMessage();
	}
	$con->close();
}
$con_login->close();
echo json_encode($response);