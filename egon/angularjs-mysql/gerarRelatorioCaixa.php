<?php

include "conexao.php";
require_once 'vendor/autoload.php';


if (!isset($_GET)) {
    die();
}

$queryVer = "";


$site = mysqli_real_escape_string($con, $_GET['site']);
$perfil = mysqli_real_escape_string($con, $_GET['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$finalizados = mysqli_real_escape_string($con, $_GET['finalizados']);
$lancamentos = mysqli_real_escape_string($con, $_GET['lancamentos']);
$dataDe = mysqli_real_escape_string($con, $_GET['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_GET['dataAte']);

$tipoJogo = mysqli_real_escape_string($con, $_GET['tipoJogo']);
$nomeCambista = mysqli_real_escape_string($con, $_GET['cambista']);
$concurso = mysqli_real_escape_string($con, $_GET['concurso']);
$status = mysqli_real_escape_string($con, $_GET['status']);
$relatorioSintetico = mysqli_real_escape_string($con, $_GET['relatorioSintetico']);
$campoOrdenacao = mysqli_real_escape_string($con, $_GET['campoOrdenacao']);
$ordenacao = mysqli_real_escape_string($con, $_GET['ordenacao']);
$tipoRelatorio = mysqli_real_escape_string($con, $_GET['tipoRelatorio']);


if($dataDe == "undefined") {
    $dataDe = 0;
}

if($dataAte == "undefined") {
    $dataAte = 0;
}

$exibe = "SIM";
if($finalizados == 'false') {
    $exibe = "NÃO";
}

$exibeLancamentos = "SIM";
if($lancamentos == 'false') {
    $exibeLancamentos = "NÃO";
}


if ($perfil == "C") { 
    $query = "select v.data_jogo, v.data_inteiro, v.concurso, v.hora_extracao, v.desc_hora, v.tp_status, v.tipo_jogo,
    v.desc_status, v.desc_jogo, v.cod_usuario, v.nome, sum(v.entradas) entradas, sum(v.qtd_apostas) qtd_apostas, sum(v.comissao) comissao  , 
    sum(v.comissao_gerente) comissao_gerente, sum(v.saidas) saidas, sum(v.entradas) - sum(v.saidas) - sum(v.comissao)  saldo, 0 as saldo_par 
    
    from (
    (select 
    DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo,
    DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro, 
    cast(case
    when jogo.tipo_jogo IN ('B' , '2') then 
        concat(
                DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) 
                end)
    when jogo.tipo_jogo = 'R' then jogo.descricao  
    else jogo.concurso end as char) as concurso ,  TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora,   
    jogo.tp_status, jogo.tipo_jogo,
    case
    when jogo.tp_status = 'A' then 'Aberto'
    when jogo.tp_status = 'P' then 'Processado'
    when jogo.tp_status = 'C' then 'Cancelado'
    when jogo.tp_status = 'L' then 'Liberado'
    when jogo.tp_status = 'B' then 'Bloqueado'
    when jogo.tp_status = 'F' then 'Finalizado'
    when jogo.tp_status = 'V' then 'Pendente de Validação' ELSE ' - ' END AS desc_status,

    case
    when jogo.tipo_jogo = 'S' then 'Seninha'
    when jogo.tipo_jogo = 'U' then 'Super Sena'
    when jogo.tipo_jogo = 'Q' then 'Quininha'
    when jogo.tipo_jogo = 'L' then 'Lotinha'
    when jogo.tipo_jogo = 'B' then 'Bicho'
    when jogo.tipo_jogo = 'R' then 'Rifa'
    when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO))
    when jogo.tipo_jogo = '6' then '6 da Sorte' ELSE 'Modalidade' END AS desc_jogo,
    usu.cod_usuario, usu.nome,
    sum(apo.valor_aposta) entradas,
    count(apo.cod_aposta) qtd_apostas,
    sum(apo.comissao) as comissao,
    0 comissao_gerente,
    sum(apo.valor_ganho) as saidas
    from jogo
    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
    left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
    where jogo.cod_site = '$site'
      and usu.cod_usuario = '$cod_usuario'
      and bil.status_bilhete != 'C'
      and apo.status != 'C' ";

    if ($concurso != "undefined" && $concurso != "") {
        $query = $query . " AND jogo.cod_jogo = '$concurso' "; 
    }      
    $query = $query . " group by usu.cod_usuario, usu.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo)
    union all
    (select DATE_FORMAT(L.data_hora, '%d/%m/%Y') data_jogo, DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, 'LANÇAMENTO' as concurso,null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo,
    null as desc_status, null AS desc_jogo, U.cod_usuario, U.nome,(case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao, 0 as comissao_gerente, 
    (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas
        from lancamento L, usuario U where L.cod_site = '$site' and L.cod_usuario_lancamento = '$cod_usuario'and L.cod_usuario_lancamento = U.cod_usuario and L.ajuda_custo = 'N' and L.status = 'A')
    ) v
    where 1 =1 ";

    if ($finalizados == 'false') {
        $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
    }

    if ($dataDe != '0') {
        $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
    }

    if($tipoJogo != "undefined" && $tipoJogo != "")
    {
        $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
    }

    if($nomeCambista != "undefined" && $nomeCambista != "")
    {
        $query = $query . " AND COD_USUARIO = '$nomeCambista' "; 
    }

    if($status != "undefined" && $status != "")
    {
        $query = $query . " AND TP_STATUS = '$status' ";
    }
    
    $query = $query . " group by v.data_jogo, v.concurso, v.hora_extracao, v.desc_hora, v.tp_status, v.tipo_jogo,
    v.desc_status, v.desc_jogo, v.cod_usuario, v.nome 
    order by $campoOrdenacao $ordenacao, concurso, qtd_apostas ";


} else if ($perfil == "G") {
    $query = "select g.cod_usuario, g.nome,";
    if ($relatorioSintetico == "false") {
        $query = $query . " g.data_jogo, g.concurso, g.hora_extracao, g.desc_hora, g.tp_status, g.desc_status, g.desc_jogo, g.tipo_jogo,";    
    }
    $query = $query . "
        sum(g.entradas) as entradas, sum(g.qtd_apostas) as qtd_apostas, sum(g.comissao) as comissao, sum(g.saidas) as saidas,
        sum(g.saldo_par) as saldo_par, sum(g.comissao_gerente) as comissao_gerente,  
        sum(g.saldo_par) - sum(g.comissao_gerente) as saldo  from (
    select v.*, (v.entradas - v.saidas - v.comissao) saldo_par
    from (
        (select usu.cod_usuario, usu.nome, DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
        when jogo.tipo_jogo IN ('B' , '2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
        when jogo.tipo_jogo = 'R' then jogo.descricao
        else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, 
        jogo.tp_status, 
        case
        when jogo.tp_status = 'A' then 'Aberto'
        when jogo.tp_status = 'P' then 'Processado'
        when jogo.tp_status = 'C' then 'Cancelado'
        when jogo.tp_status = 'L' then 'Liberado'
        when jogo.tp_status = 'B' then 'Bloqueado'
        when jogo.tp_status = 'V' then 'Pendente de Validação'
        when jogo.tp_status = 'F' then 'Finalizado'
        ELSE ' - ' END AS desc_status,

        case
        when jogo.tipo_jogo = 'S' then 'Seninha'
        when jogo.tipo_jogo = 'Q' then 'Quininha'
        when jogo.tipo_jogo = 'L' then 'Lotinha'
        when jogo.tipo_jogo = 'B' then 'Bicho'
        when jogo.tipo_jogo = 'R' then 'Rifa'
        when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO))
        when jogo.tipo_jogo = '6' then '6 da Sorte' ELSE 'Modalidade' END AS desc_jogo,
        
        jogo.tipo_jogo,  
        DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
        sum(apo.valor_aposta) entradas,
        count(apo.cod_aposta) qtd_apostas,
        sum(apo.comissao) as comissao,
        sum(apo.comissao_gerente) as comissao_gerente,
        sum(apo.valor_ganho) as saidas
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
        left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
        where jogo.cod_site = '$site'
          and usu.cod_gerente = '$cod_usuario'
          and bil.status_bilhete != 'C'
          and apo.status != 'C' ";
        if($concurso != "undefined" && $concurso != "")
        {
            $query = $query . " AND jogo.cod_jogo = '$concurso' "; 
        } 
        $query = $query . " group by usu.cod_usuario, usu.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo) ) v 
        ) g where 1 =1  ";        

    if ($finalizados == 'false') {
        $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
    }
    if ($dataDe != '0') {
        $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
    } 

    if($tipoJogo != "undefined" && $tipoJogo != "")
    {
        $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
    }

    if($nomeCambista != "undefined" && $nomeCambista != "")
    {
        $query = $query . " AND cod_usuario = '$nomeCambista' "; 
    }

    if($status != "undefined" && $status != "")
    {
        $query = $query . " AND TP_STATUS = '$status' ";
    }

    if ($relatorioSintetico == "false") {
        $query = $query." GROUP BY g.cod_usuario, g.nome, g.data_jogo, g.concurso, g.hora_extracao, g.desc_hora, g.tp_status,g.tipo_jogo, g.data_inteiro";
    } else {
        $query = $query." GROUP BY g.cod_usuario, g.nome";
    }

    $query = $query . " order by $campoOrdenacao  $ordenacao, concurso, qtd_apostas ";
    
} else if ($perfil == "A") {
    $query = "select cod_usuario,nome,";
    if ($relatorioSintetico == "false") {
        $query = $query . "data_jogo,concurso,hora_extracao,desc_hora,tp_status,desc_status,tipo_jogo,desc_jogo ,";
    }
    $query = $query . "
        sum(entradas) as entradas, sum(qtd_apostas) as qtd_apostas, 
        sum(comissao) as comissao, 
        sum(saidas) as saidas,
        round(sum(comissao_gerente),2) as comissao_gerente,
        (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
        (sum(v.entradas) - sum(v.saidas) - sum(v.comissao) - sum(v.comissao_gerente))  as saldo
    from (
        (select geren.cod_usuario, geren.nome,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
        when jogo.tipo_jogo IN ('B' , '2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
        when jogo.tipo_jogo = 'R' then jogo.descricao 
        else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, 
        jogo.tp_status,  
        case
        when jogo.tp_status = 'A' then 'Aberto'
        when jogo.tp_status = 'P' then 'Processado'
        when jogo.tp_status = 'C' then 'Cancelado'
        when jogo.tp_status = 'L' then 'Liberado'
        when jogo.tp_status = 'B' then 'Bloqueado'
        when jogo.tp_status = 'F' then 'Finalizado'
        when jogo.tp_status = 'V' then 'Pendente de Validação'  ELSE ' - ' END AS desc_status,

        case
        when jogo.tipo_jogo = 'S' then 'Seninha'
        when jogo.tipo_jogo = 'Q' then 'Quininha'
        when jogo.tipo_jogo = 'L' then 'Lotinha'
        when jogo.tipo_jogo = 'B' then 'Bicho'
        when jogo.tipo_jogo = 'R' then 'Rifa'
        when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO))
        when jogo.tipo_jogo = '6' then '6 da Sorte' ELSE 'Modalidade' END AS desc_jogo,
        jogo.tipo_jogo, 
        DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
        sum(apo.valor_aposta) entradas,
        count(apo.cod_aposta) qtd_apostas,
        sum(apo.comissao) as comissao,
        SUM(apo.comissao_gerente) AS comissao_gerente,
        sum(apo.valor_ganho) as saidas
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
        left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
        left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
        where jogo.cod_site = '$site'
        and geren.cod_gerente = '$cod_usuario'
        and bil.status_bilhete != 'C'
        and apo.status != 'C' ";
        
        if($concurso != "undefined" && $concurso != "")
        {
            $query = $query . " AND jogo.cod_jogo = '$concurso' "; 
        }
        $query = $query . " group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status,desc_status, jogo.tipo_jogo,desc_jogo) 
        ) v
        where 1 =1 ";
    
        if ($finalizados == 'false') {
            $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
        }
        if ($dataDe != '0') {
            $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
        }
    
        if ($dataAte != '0') {
            $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
        }

        if($tipoJogo != "undefined" && $tipoJogo != "")
        {
            $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
        }
    
        if($nomeCambista != "undefined" && $nomeCambista != "")
        {
            $query = $query . " AND cod_usuario = '$nomeCambista' "; 
        }
    
        if($status != "undefined" && $status != "")
        {
            $query = $query . " AND TP_STATUS = '$status' ";
        }

        if ($relatorioSintetico == "false") {
            $query = $query." GROUP BY cod_usuario,nome,data_jogo, concurso, hora_extracao, desc_hora,tp_status, tipo_jogo, data_inteiro";
        } else {
            $query = $query." GROUP BY cod_usuario, nome";
        }
            
        $query = $query . " ORDER BY $campoOrdenacao $ordenacao, concurso, qtd_apostas ";    

        $query_totalizadores_por_gerente = "
                select cod_usuario, nome, data_jogo, concurso, hora_extracao,
                       desc_hora, tp_status, desc_status, tipo_jogo, desc_jogo, 
                       sum(entradas) as entradas, 
                       sum(qtd_apostas) as qtd_apostas, 
                       sum(comissao) as comissao, 
                       sum(saidas) as saidas,
                       (CASE WHEN (sum(v.entradas_sem_lancamento) - sum(v.saidas) - sum(v.comissao)) >= 0 THEN
                            round(sum(v.comissao_gerente), 2)
                        ELSE 0 END) as comissao_gerente,
                       (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
                       (CASE WHEN (sum(v.entradas_sem_lancamento) - sum(v.saidas) - sum(v.comissao)) > 0 THEN
                            ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - (round(sum(v.comissao_gerente), 2)))
                        ELSE (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) END) as saldo 
                from (
                    (select geren.cod_usuario, geren.nome,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
                    when jogo.tipo_jogo IN ('B' , '2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end) 
                    else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, 
                    jogo.tp_status,  
                    case
                    when jogo.tp_status = 'A' then 'Aberto'
                    when jogo.tp_status = 'P' then 'Processado'
                    when jogo.tp_status = 'C' then 'Cancelado'
                    when jogo.tp_status = 'L' then 'Liberado'
                    when jogo.tp_status = 'B' then 'Bloqueado'
                    when jogo.tp_status = 'F' then 'Finalizado'
                    when jogo.tp_status = 'V' then 'Pendente de Validação'  ELSE ' - ' END AS desc_status,
            
                    case
                    when jogo.tipo_jogo = 'S' then 'Seninha'
                    when jogo.tipo_jogo = 'Q' then 'Quininha'
                    when jogo.tipo_jogo = 'L' then 'Lotinha'
                    when jogo.tipo_jogo = 'B' then 'Bicho'
                    when jogo.tipo_jogo = '2' then '2 para 500'
                    when jogo.tipo_jogo = '6' then '6 da Sorte' ELSE 'Modalidade' END AS desc_jogo,
            
                    jogo.tipo_jogo, 
                    DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
                    sum(apo.valor_aposta) entradas,
                    sum(apo.valor_aposta) entradas_sem_lancamento,
                    count(apo.cod_aposta) qtd_apostas,
                    sum(apo.comissao) as comissao,
                    sum(apo.comissao_gerente) as comissao_gerente,
                    sum(apo.valor_ganho) as saidas
                    from jogo
                    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                    inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                    left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                    where jogo.cod_site = '$site'
                    and geren.cod_gerente = '$cod_usuario'
                    and bil.status_bilhete != 'C'
                    and apo.status != 'C' ";
            
            if($concurso != "undefined" && $concurso != "")
            {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND jogo.cod_jogo = '$concurso' "; 
            }
            $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status,desc_status, jogo.tipo_jogo,desc_jogo) 
             ) v
                where 1 =1 ";
        
            if ($finalizados == 'false') {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
            }
            if ($dataDe != '0') {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
            }
        
            if ($dataAte != '0') {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
            }
    
            if($tipoJogo != "undefined" && $tipoJogo != "")
            {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND TIPO_JOGO = '$tipoJogo' ";
            }
        
            if($nomeCambista != "undefined" && $nomeCambista != "")
            {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND cod_usuario = '$nomeCambista' "; 
            }
        
            if($status != "undefined" && $status != "")
            {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND TP_STATUS = '$status' ";
            }
    
            $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " group by nome ";

            $totalizadores_comissao_saldo = "SELECT SUM(comissao_gerente) as comissao_gerente, ".            
                                            "SUM(saldo) as saldo ".            
                                            "FROM ( ". $query_totalizadores_por_gerente . " ) t";
                        
}

$queryVer = $query;

$lancamentos_arr = array();
$totalizadores_lancamentos_arr = array();

if ($perfil != "C" && $lancamentos == 'true') {
    
    $queryLancamentos = "";

    if ($perfil == 'A') {
    
        $queryLancamentos = "
            SELECT 
                cod_usuario,
                nome,
                data_lancamento,
                motivo,
                data_inteiro,
                SUM(entradas) AS entradas,
                SUM(saidas) AS saidas,
                (SUM(v.entradas) - SUM(v.saidas)) AS saldo
            FROM
                ((SELECT 
                    NULL AS cod_cambista,
                        U.cod_usuario,
                        U.nome,
                        DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                        L.motivo AS motivo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                        (CASE
                            WHEN tipo_lancamento = 'E' THEN (valor)
                            ELSE 0
                        END) AS entradas,
                        (CASE
                            WHEN tipo_lancamento = 'S' THEN (valor)
                            ELSE 0
                        END) AS saidas
                FROM
                    lancamento L, usuario U
                WHERE
                    L.COD_USUARIO_lancamento = U.cod_usuario
                        AND L.cod_site = '$site'
                        AND L.COD_USUARIO_lancamento IN (SELECT 
                            cod_usuario
                        FROM
                            usuario
                        WHERE
                            cod_gerente = '$cod_usuario')
                        AND L.ajuda_custo = 'N'
                        AND L.status = 'A') 
                UNION ALL 
                    (SELECT 
                        U.cod_usuario AS cod_cambista,
                        U.cod_gerente AS cod_usuario,
                        G.nome,
                        DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                        L.motivo AS motivo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                        (CASE
                            WHEN tipo_lancamento = 'E' THEN (valor)
                            ELSE 0
                        END) AS entradas,
                        (CASE
                            WHEN tipo_lancamento = 'S' THEN (valor)
                            ELSE 0
                        END) AS saidas
                FROM
                    lancamento L, usuario U, usuario G
                WHERE
                    L.COD_USUARIO_lancamento = U.cod_usuario
                        AND U.cod_gerente = G.cod_usuario
                        AND L.cod_site = '$site'
                        AND L.COD_USUARIO_lancamento IN (SELECT 
                            U1.cod_usuario
                        FROM
                            usuario U1, usuario U2
                        WHERE
                            U1.cod_gerente = U2.cod_usuario
                                AND U2.cod_gerente = '$cod_usuario')
                        AND L.ajuda_custo = 'N'
                        AND L.status = 'A')) v
                WHERE 1 =1  ";
    } else if ($perfil == 'G') {
        $queryLancamentos = "
                SELECT 
                    cod_usuario,
                    nome,
                    data_lancamento,
                    motivo,
                    data_inteiro,
                    SUM(entradas) AS entradas,
                    SUM(saidas) AS saidas,
                    (SUM(v.entradas) - SUM(v.saidas)) AS saldo
                FROM
                    ((SELECT 
                            NULL AS cod_cambista,
                            U.cod_usuario,
                            U.nome,
                            DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                            L.motivo AS motivo,
                            DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                            (CASE
                                WHEN tipo_lancamento = 'E' THEN (valor)
                                ELSE 0
                            END) AS entradas,
                            (CASE
                                WHEN tipo_lancamento = 'S' THEN (valor)
                                ELSE 0
                            END) AS saidas
                    FROM
                        lancamento L, usuario U
                    WHERE
                        L.COD_USUARIO_lancamento = U.cod_usuario
                            AND L.cod_site = '$site'
                            AND L.cod_usuario_lancamento = '$cod_usuario'
                            AND L.ajuda_custo = 'N'
                            AND L.status = 'A') 
                    UNION ALL 
                    (SELECT 
                            U.cod_usuario AS cod_cambista,
                            U.cod_usuario AS cod_usuario,
                            U.nome,
                            DATE_FORMAT(L.data_hora, '%d/%m/%Y') AS data_lancamento,
                            L.motivo AS motivo,
                            DATE_FORMAT(L.data_hora, '%Y%m%d') AS data_inteiro,
                            (CASE
                                WHEN tipo_lancamento = 'E' THEN (valor)
                                ELSE 0
                            END) AS entradas,
                            (CASE
                                WHEN tipo_lancamento = 'S' THEN (valor)
                                ELSE 0
                            END) AS saidas
                    FROM
                        lancamento L, usuario U, usuario G
                    WHERE
                        L.COD_USUARIO_lancamento = U.cod_usuario
                            AND U.cod_gerente = G.cod_usuario
                            AND L.cod_site = '$site'
                            AND L.COD_USUARIO_lancamento IN (SELECT 
                                cod_usuario
                            FROM
                                usuario
                            WHERE
                                cod_gerente = '$cod_usuario')
                            AND L.ajuda_custo = 'N'
                            AND L.status = 'A')) v
                WHERE
                    1 = 1 ";
    }

    error_log($nomeCambista, 0);
    if($nomeCambista && $nomeCambista != 'undefined') {
        $queryLancamentos = $queryLancamentos . " AND COD_USUARIO = $nomeCambista ";
    }

    if ($dataDe != '0') {
        $queryLancamentos = $queryLancamentos . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
    }

    if ($dataAte != '0') {
        $queryLancamentos = $queryLancamentos . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
    }

    if ($dataDe == 0 && $dataAte == 0) {
        $queryLancamentos = $queryLancamentos . " AND data_inteiro >= DATE_FORMAT(current_date - 7, '%Y%m%d')
                AND data_inteiro <= DATE_FORMAT(current_date, '%Y%m%d') " ;
    }

    $queryLancamentos = $queryLancamentos . " group by cod_usuario, nome, data_lancamento, motivo 
                    order by cod_usuario, nome, data_lancamento";                    
                      
    $totalizadoresLancamentos = "SELECT SUM(entradas) as entradas, ".
        "SUM(saidas) as saidas, ".
        "SUM(saldo) as saldo ".
        "FROM ( ". $queryLancamentos . " ) t";
     
    $result = mysqli_query($con, $queryLancamentos);

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_usuario'] = $row['cod_usuario'];
        $row_array['nome'] = $row['nome'];
        $row_array['data_lancamento'] = $row['data_lancamento'];
        $row_array['motivo'] = $row['motivo'];
        $row_array['data_inteiro'] = $row['data_inteiro'];
        $row_array['entradas'] = $row['entradas'];
        $row_array['saidas'] = $row['saidas'];
        $row_array['saldo'] = $row['saldo'];

        array_push($lancamentos_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }        

    $resultadoTotalizadoresLancamentos = mysqli_query($con, $totalizadoresLancamentos);
    $totalizadores_lancamentos_arr = mysqli_fetch_array($resultadoTotalizadoresLancamentos);

}


$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    
    $row_array['cod_usuario'] = $row['cod_usuario'];
    $row_array['nome'] = $row['nome'];
    $row_array['saldo'] = $row['saldo'];

    if ($relatorioSintetico == "false") {
        $row_array['concurso'] = $row['concurso'];
        $row_array['data_jogo'] = $row['data_jogo'];
        $row_array['tp_status'] = $row['tp_status'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        $row_array['desc_status'] = $row['desc_status'];
        $row_array['desc_jogo'] = $row['desc_jogo'];    
    }
    $row_array['entradas'] = $row['entradas'];
    $row_array['qtd_apostas'] = $row['qtd_apostas'];
    $row_array['comissao'] = $row['comissao'];
    $row_array['saidas'] = $row['saidas'];
    $row_array['comissao_gerente'] = $row['comissao_gerente'];
    $row_array['saldo_par'] = $row['saldo_par'];

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
};

$comissao_gerente_total = 0;
$saldo_total = 0;
$sumQtdApostas = 0;
$sumEntradas= 0;
$sumSaidas = 0;
$sumComissao = 0;
$sumComissaoGerente = 0;
$sumSaldo = 0;
$sumSaldoParcial = 0;

if ($perfil == "A") {
    $resultado_totalizador_comissao_saldo = mysqli_query($con, $totalizadores_comissao_saldo);
    $row_comissao_saldo = mysqli_fetch_array($resultado_totalizador_comissao_saldo);
    $comissao_gerente_total = $row_comissao_saldo['comissao_gerente'];
    $saldo_total = $row_comissao_saldo['saldo'];
}

$data_de = date_create($dataDe);
$data_ate = date_create($dataAte); 

$data = date('d-m-Y');
$data .= ' '.date('H:i:s');

$query = " SELECT nome from usuario
            WHERE cod_usuario = '$cod_usuario' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);


$html = "
<html>
    <head>
        <meta charset='utf-8'>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 10px;
            }
        </style>
    </head>
    <body>
        <div class='col-md-12 col-ms-12 col-xs-12'>
            <div class='table-responsive betL_CaixaList'>
                <h2>CAIXA:</h2>
                <div style='display: inline;'>
                    <span style='font-size: 12pt;'> <b>De:</b> " . date_format($data_de, 'd-m-Y') . "</span>
                    &nbsp;
                    <span style='font-size: 12pt;'> <b>Até:</b> " . date_format($data_ate, 'd-m-Y') . "</span>
                    &nbsp;
                    <span style='font-size: 12pt;'> <b>Exibe Finalizados:</b> " . $exibe . "</span>";
if ($perfil != "C") {
    $html = $html . "
                    &nbsp;
                    <span style='font-size: 12pt;'> <b>Exibe Lançamentos:</b> " . $exibeLancamentos . "</span>";
}
$html = $html . "                        
                    &nbsp;-&nbsp;
                    <span style='font-size: 12pt;'> " . $row['nome'] . " em " . $data . "</span>
                </div>
                <hr>
                <h3>APOSTAS:</h3>";

if (count($return_arr) == 0) {

    $html = $html . "
                <hr>
                    <h4>Não há dados</h4>
                <hr>";

} else {

    $html = $html . " 
                <table class='table table-hover' style='width: 100%; margin-left:auto; margin-right:auto;'> 
                    <thead>
                        <tr style='background-color: #2E64FE; color: white;'> ";
    if ($relatorioSintetico == "false") {
        $html = $html . " 
        <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>Concurso</th>
        <th style='width: 7%; text-align: center; color: white; font-weight: bold;'>Data</th>
        <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>Status</th> 
        <th style='width: 9%; text-align: center; color: white; font-weight: bold;'>Jogo</th>";
    }
                 
    if ($perfil == "A") {
        $html = $html . " 
                            <th style='width: 9%; text-align: center; color: white; font-weight: bold;'>Gerente</th>
                            <th style='width: 5%; text-align: center; color: white; font-weight: bold;'>Qtd</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'><a>Entradas</a></th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'><a>Saídas </a></th>
                            <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>Comissão</th>
                            <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>Comissão Gerente</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Saldo</th>";
    } else {
        $html = $html . " 
                            <th style='width: 9%; text-align: center; color: white; font-weight: bold;'>Cambista</th>
                            <th style='width: 5%; text-align: center; color: white; font-weight: bold;'>Qtd</th>
                            <th style='width: 13%; text-align: center; color: white; font-weight: bold;'><a>Entradas</a></th>
                            <th style='width: 12%; text-align: center; color: white; font-weight: bold;'><a>Saídas </a></th>
                            <th style='width: 12%; text-align: center; color: white; font-weight: bold;'>Comissão</th>
                            <th style='width: 13%; text-align: center; color: white; font-weight: bold;'>Saldo</th>";
    }
    $html = $html . "   </tr>
                    </thead>
                    <tbody class='container'> ";
    $odd = 0;           
    foreach ($return_arr as $e) {
        $odd = $odd + 1;
        if($odd % 2 == 0) {
            $html = $html . "
                        <tr style='background-color: #E6E6E6;'>";
        } else {
            $html = $html . "
                        <tr>";
        }

        $sumQtdApostas = $sumQtdApostas + $e['qtd_apostas'];
        $sumEntradas= $sumEntradas + $e['entradas'];
        $sumSaidas = $sumSaidas + $e['saidas'];
        $sumComissao = $sumComissao + $e['comissao'];
        $sumComissaoGerente = $sumComissaoGerente + $e['comissao_gerente'];
        $sumSaldo = $sumSaldo + $e['saldo'];
        $sumSaldoParcial = $sumSaldoParcial + $e['saldo_par'];
        
        if ($relatorioSintetico == "false") {
            $html = $html . " 
            <td style='font-weight: bold;'> " . $e['concurso'] . "</td>
            <td> " . $e['data_jogo'] . " </td>
            <td> " . $e['desc_status'] . " </td>
            <td> " . $e['desc_jogo'] . " </td>";    
        }
        $html = $html . " 
                            <td> " . $e['nome'] . " </td> 
                            <td align='center'> " . $e['qtd_apostas'] . " </td>
                            <td align='right'> R$ " . number_format($e['entradas'], 2, ',', '.') . " </td>
                            <td align='right'> R$ " . number_format($e['saidas'], 2, ',', '.') . " </td>
                            <td align='right'> R$ " . number_format($e['comissao'], 2, ',', '.') . " </td>";
        if ($perfil == "A") {
                $html = $html . "
                            <td align='right'> R$ " . number_format($e['comissao_gerente'], 2, ',', '.') . " </td>";
                if ($e['saldo'] >= 0) {
                    $html = $html . "
                            <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($e['saldo'], 2, ',', '.') . " </td>";
                } else {
                    $html = $html . "
                            <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($e['saldo'], 2, ',', '.') . " </td>";
                }
        } else {
                if ($e['saldo'] >= 0) {
                    $html = $html . "
                            <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($e['saldo']+$e['comissao_gerente'], 2, ',', '.') . " </td>";
                } else{
                    $html = $html . "
                            <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($e['saldo'], 2, ',', '.') . " </td>";
                }
        }
                                  
        $html = $html . "
                        </tr>";
    }

    $html = $html . "
                    </tbody>
                    <tr style='font-weight: bold; background-color: #A9F5BC;'>
                        <th scope='row'> TOTAL </th>";
    
    if ($relatorioSintetico == "false") {
        $html = $html .                     "
        <td> - </td>
        <td> - </td>
        <td> - </td>
        <td> - </td> ";
    }
    $html = $html .    "
                        <td align='center' style='font-weight: bold;'> " .  $sumQtdApostas . " </td>
                        <td align='right' style='font-weight: bold;'> R$ " . number_format($sumEntradas, 2, ',', '.') . " </td>
                        <td align='right' style='font-weight: bold;'> R$ " . number_format($sumSaidas, 2, ',', '.') . " </td>
                        <td align='right' style='font-weight: bold;'> R$ " . number_format($sumComissao, 2, ',', '.') . " </td>";
    if ($perfil == "A") {
        if($comissao_gerente_total >= 0) {
            $html = $html . "
                        <td align='right' style='font-weight: bold;'> R$ " . number_format($comissao_gerente_total, 2, ',', '.') . " </td>";
            if ($saldo_total >= 0) {
                $html = $html . "
                        <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total, 2, ',', '.') . " </td>";
            } else {
                $html = $html . "
                        <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total, 2, ',', '.') . " </td>";
            }
        } else {
            $html = $html . "
                        <td align='right' style='font-weight: bold;'> R$ " . number_format('0', 2, ',', '.') . " </td>";
            if ($saldo_total >= 0) {
                $html = $html . "
                        <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($saldo_total, 2, ',', '.') . " </td>";
            } else {
                $html = $html . "
                        <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($saldo_total, 2, ',', '.') . " </td>";
            }
        }
    } else {
        if($sumComissaoGerente >= 0) {
            if ($sumSaldo >= 0) {
                $html = $html . "
                        <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($sumSaldo+$sumComissaoGerente, 2, ',', '.') . " </td>";
            } else {
                $html = $html . "
                        <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($sumSaldo+$sumComissaoGerente, 2, ',', '.') . " </td>";
            }
        } else {
            if ($sumSaldoParcial  >= 0) {
                $html = $html . "
                        <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($sumSaldoParcial, 2, ',', '.') . " </td>";
            } else {
                $html = $html . "
                        <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($sumSaldoParcial, 2, ',', '.') . " </td>";
            }
        }
    } 

    $html = $html . "
                    </tr>                        
                </table> ";
}
if ($perfil != "C" && $lancamentos == 'true') {
    $html = $html . "
                <h3>LANÇAMENTOS:</h3>";

    if (count($lancamentos_arr) == 0) {    
        $html = $html . "
                <hr>
                    <h4>Não há dados</h4>
                <hr>";    
    } else {
        $html = $html . " 
                <table class='table table-hover' style='width: 100%; margin-left:auto; margin-right:auto;  font-size: 10pt;'> 
                    <thead>
                        <tr style='background-color: #2E64FE; color: white;'> 
                            <th style='text-align: center; color: white; font-weight: bold;'>Data</th>
                            <th style='text-align: center; color: white; font-weight: bold;'>Gerente</th>
                            <th style='text-align: center; color: white; font-weight: bold;'>Motivo</th> 
                            <th style='text-align: center; color: white; font-weight: bold;'>Entradas</th>
                            <th style='text-align: center; color: white; font-weight: bold;'>Saídas</th>
                            <th style='text-align: center; color: white; font-weight: bold;'>Saldo</th>
                        </tr>
                    </thead>
                    <tbody class='container'>";
        $odd = 0;           
        foreach ($lancamentos_arr as $lancamento) {
            $odd = $odd + 1;
            if($odd % 2 == 0) {
                $html = $html . "
                        <tr style='background-color: #E6E6E6;'>";
            } else {
                $html = $html . "
                        <tr>";
            }
    
            $html = $html . " 
                            <td style='text-align: center;'>" . $lancamento['data_lancamento'] . "</td>
                            <td> " . $lancamento['nome'] . " </td>
                            <td> " . $lancamento['motivo'] . " </td>
                            <td align='right'> R$ " . number_format($lancamento['entradas'], 2, ',', '.') . " </td>
                            <td align='right'> R$ " . number_format($lancamento['saidas'], 2, ',', '.') . " </td>";
            if ($lancamento['saldo'] >= 0) {
                $html = $html . "
                            <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($lancamento['saldo'], 2, ',', '.') . " </td>";
            } else{
                $html = $html . "
                            <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($lancamento['saldo'], 2, ',', '.') . " </td>";
            }
                                        
            $html = $html . "
                        </tr>";
        }
    
        $html = $html . "
                    </tbody>
                    <tr style='font-weight: bold; background-color: #A9F5BC;'>
                        <th scope='row'> TOTAL </th>
                        <td> - </td>
                        <td> - </td>
                        <td align='right' style='font-weight: bold;'> R$ " . number_format($totalizadores_lancamentos_arr['entradas'], 2, ',', '.') . " </td>
                        <td align='right' style='font-weight: bold;'> R$ " . number_format($totalizadores_lancamentos_arr['saidas'], 2, ',', '.') . " </td>";
        if ($totalizadores_lancamentos_arr['saldo'] >= 0) {
            $html = $html . "
                        <td style='color: green; font-weight: bold;' align='right'> R$ " . number_format($totalizadores_lancamentos_arr['saldo'], 2, ',', '.') . " </td>";
        } else {
            $html = $html . "
                        <td style='color: red; font-weight: bold;' align='right'> R$ " . number_format($totalizadores_lancamentos_arr['saldo'], 2, ',', '.') . " </td>";
        }
    
        $html = $html . "
                    </tr>                        
                </table> ";        
    }    
}
$html = $html . "                            
            </div>
        </div>
    </body> 
</html> ";



// print_r($queryVer);
// printf($html);

$hoje = date('YmdHis');

if ($tipoRelatorio == 'xlsx') {
    $arquivo = "caixa" . $hoje . ".xls";
	header ("Expires: Mon, 18 Nov 1985 18:00:00 GMT");
	header ("Last-Modified: ".gmdate("D,d M YH:i:s")." GMT");
	header ("Cache-Control: no-cache, must-revalidate");
	header ("Pragma: no-cache");
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");	
	header ("Content-Type: text/html; charset=UTF-8");
    
    // Imprime o conteúdo da nossa tabela no arquivo que será gerado
    echo $html;    
} else if ($tipoRelatorio == 'pdf') {
    $arquivo = "caixa" . $hoje . ".pdf";
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    $mpdf->Output($arquivo, "D");
} 

$con->close();
 
?>
