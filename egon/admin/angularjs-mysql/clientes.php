<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$response = [];

$status = mysqli_real_escape_string($con, $_POST['status']);

$query = "select s.*
from site s
where s.status = '$status'";

$result = mysqli_query($con, $query);

$return_arr = array();

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $cod_site = $row['COD_SITE'];
    $row_array['cod_site'] = $cod_site;
    $row_array['txt_empresa'] = $row['TXT_EMPRESA'];
    $row_array['txt_responsavel'] = $row['TXT_RESPONSAVEL'];
    $row_array['txt_telefone'] = $row['TXT_TELEFONE'];
    $row_array['nome_banca'] = $row['NOME_BANCA'];
    $row_array['status'] = $row['STATUS'];
    $row_array['vencimento'] = $row['vencimento'];
    $row_array['mensalidade'] = $row['mensalidade'];
    $row_array['obs'] = $row['OBS'];

    $schema_selecionado = 'b' . $cod_site;

    $queryUsuario = "SELECT UB.COD_USUARIO AS COD_USUARIO, UB.LOGIN, UB.SENHA
                      FROM $schema_selecionado.USUARIO UB 
                      WHERE UB.COD_SITE = '$cod_site'
                        AND UB.PERFIL = 'A'
                        AND UB.LOGIN NOT IN ('admw')
                      ORDER BY UB.COD_USUARIO"; 
    $resultUsuario = mysqli_query($con, $queryUsuario);
    if (mysqli_num_rows($resultUsuario) > 0) {
      $rowUsuario = mysqli_fetch_assoc($resultUsuario);
      $row_array['login'] = $rowUsuario['LOGIN'];
      $row_array['senha'] = $rowUsuario['SENHA'];
    } else {
      $row_array['login'] = '';
      $row_array['senha'] = '';
    }

    array_push($return_arr, $row_array);
}
;

echo json_encode($return_arr, JSON_NUMERIC_CHECK);
