angular.module('main').controller('relatorioBilheteConcursoCtrl',
    function ($scope, user, $http, $rootScope, funcoes, $filter, configGeral) {

        $scope.comboTipoJogo = "";
        $scope.comboConcurso = "";
        $scope.tiposJogos = funcoes.getTiposJogos();
        $scope.desc_2pra500 = '2 PRA 500';
        configGeral.get().then(function (data) {
            $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        });

        $scope.getJogos = function () {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + '&finalizados=false' + '&operacao='
                    + '&site=' + user.getSite() + '&status='
                    + '&index='
                    + '&qtdElementos='
                    + '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogos = response.data.filter(obj => !obj.totalizadores);
            }).catch(function (response) {
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }

        $scope.gerarRelatorio = function () {
            if ($scope.comboConcurso == "") {
                $.alert("Selecione o Concurso!");
                return;
            }
            var jogo = JSON.parse($scope.comboConcurso);
            window.open('angularjs-mysql/relatorioBilheteConcurso.php?' 
                            + 'site=' + user.getSite() 
                            + '&schema=' + user.getSchema()
                            + '&concurso=' + jogo.cod_jogo);
        }

        getFileName = function (jogo) {
            var tipo = $scope.getDescricaoTipoJogo(jogo.tipo_jogo) + "_";
            var dataJogo = $filter('date')(jogo.data_jogo, "dd_MM_yyyy") + "_";
            var concurso = "";
            switch (jogo.tipo_jogo) {
                case 'S':
                case 'Q':
                case 'L':
                case 'U':
                    concurso = jogo.concurso;
                    break;
                case 'B':
                case 2:
                    concurso = jogo.desc_hora + "_" + jogo.hora_extracao;
                    break;
                default: break;
            }
            return tipo + dataJogo + concurso + ".pdf";
        }

        $scope.getDescricaoTipoJogo = function (tipo_jogo) {
            return funcoes.getDescricaoTipoJogo(tipo_jogo, $scope.desc_2pra500); 
        }

        $scope.getDescricaoConcurso = function(jogo){
            return funcoes.getDescricaoConcurso(jogo);
        }

        $scope.filterByTipo = function (jogo) {
            return $scope.comboTipoJogo == "" || jogo.tipo_jogo == $scope.comboTipoJogo;
        }
    })