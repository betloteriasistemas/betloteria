angular.module('main').controller('verBilhetesCtrl', function ($scope, user, $http, $location, $filter) {
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.records = [];
    $scope.listaApostas = [];
    $scope.bilheteAux = "";
    $scope.ordenacao = true;
    $scope.campoOrdenacao = "";

    $scope.total = 0;
    $scope.totalComissao = 0;

    $scope.orderFilter = function (campo) { 
        if ($scope.campoOrdenacao == "") {
            $scope.campoOrdenacao = campo;
        }

        if ($scope.campoOrdenacao == campo) {
            $scope.ordenacao = !$scope.ordenacao;
        } else {
            $scope.campoOrdenacao = campo;
            $scope.ordenacao = false;
        }

        var sAux = "";
        if (!$scope.ordenacao) {
            sAux = "-";
        }

        $scope.records = $filter('orderBy')($scope.records, sAux + campo);
        
    };


    $scope.filtroCambistas = function (item) {
        if ($scope.perfil == 'A') {
            var retorno = (item.gerente == $scope.comboCambista && item.concurso == $scope.comboConcurso) ||
                (item.gerente == $scope.comboCambista && $scope.comboConcurso == "") ||
                (item.gerente == $scope.comboCambista && typeof $scope.comboConcurso == "undefined") ||
                (item.concurso == $scope.comboConcurso && $scope.comboCambista == "") ||
                (item.concurso == $scope.comboConcurso && typeof $scope.comboCambista == "undefined") ||
                ($scope.comboConcurso == "" && $scope.comboCambista == "") ||
                (typeof $scope.comboConcurso == "undefined" && typeof $scope.comboCambista == "undefined") ||
                (typeof $scope.comboConcurso == "undefined" && $scope.comboCambista == "") ||
                ($scope.comboConcurso == "" && typeof $scope.comboCambista == "undefined");

            return retorno;


        } else {
            return (item.cambista == $scope.comboCambista && item.concurso == $scope.comboConcurso) ||
                (item.cambista == $scope.comboCambista && $scope.comboConcurso == "") ||
                (item.cambista == $scope.comboCambista && typeof $scope.comboConcurso == "undefined") ||
                (item.concurso == $scope.comboConcurso && $scope.comboCambista == "") ||
                (item.concurso == $scope.comboConcurso && typeof $scope.comboCambista == "undefined") ||
                ($scope.comboConcurso == "" && $scope.comboCambista == "") ||
                (typeof $scope.comboConcurso == "undefined" && typeof $scope.comboCambista == "undefined") ||
                (typeof $scope.comboConcurso == "undefined" && $scope.comboCambista == "") ||
                ($scope.comboConcurso == "" && typeof $scope.comboCambista == "undefined");
        }

    };

    $scope.filtroConcursos = function (item) {
        return item.concurso == $scope.comboConcurso || $scope.comboConcurso == "" || typeof $scope.comboConcurso == "undefined";
    };

    $scope.getDescricaoConcurso = function(concurso, tipo_jogo, hora_extracao){
        if (tipo_jogo == 'B'){
            return hora_extracao;
        }else{
            return "Concurso: " + concurso + ' - ';
        }
    }



    $scope.listarBilhetes = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + '&site=' + user.getSite() + '&operacao=listar'
        }).then(function (response) {
            $scope.records = response.data;
            $scope.recordsFiltered = $scope.records;            
            $scope.orderFilter('cod_bilhete');
            $scope.mudancaDeFiltro();
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.getDescricaoTipoJogo = function (tipo_jogo) {
        if (tipo_jogo == 'S') {
            return "Seninha";
        } else if (tipo_jogo == 'Q') {
            return "Quininha";
        }else if (tipo_jogo == 'B') {
            return "Bicho";
        }else if (tipo_jogo == '2') {
            return "2 pra 500";
        }
    }

    $scope.getJogos = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/jogos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + '&finalizados=false' + '&operacao=' + '&site=' + user.getSite() + '&status='
        }).then(function (response) {
            $scope.jogos = response.data;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.listarApostas = function (cod_bilhete) {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + '&site=' + user.getSite() + '&cod_bilhete=' + cod_bilhete + '&operacao=listarApostas'
        }).then(function (response) {
            $scope.apostasList = response.data;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.getCambistas = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + '&site=' + user.getSite() + '&operacao=getCambistas'
        }).then(function (response) {
            $scope.cambistas = response.data;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.mudancaDeFiltro = function () {
        $scope.recordsFiltered = $filter('filter')($scope.records, {'tipo_jogo' : $scope.comboTipoJogo});
        $scope.recordsFiltered = $filter('filter')($scope.recordsFiltered, {'nome_apostador' : $scope.edtNome});
        $scope.recordsFiltered = $filter('filter')($scope.recordsFiltered, {'status_bilhete': $scope.comboStatusBilhete});        
        $scope.recordsFiltered = $filter('filter')($scope.recordsFiltered, $scope.filtroCambistas);        

        $scope.total = 0;
        $scope.totalComissao = 0;
        for (var i = 0; i < $scope.recordsFiltered.length; i++) {
            $scope.total += parseFloat($scope.recordsFiltered[i].total);
            $scope.totalComissao += parseFloat($scope.recordsFiltered[i].comissao);
        } 
        
    }

    /*$scope.getTotal = function () {
        var total = 0.0;
        for (var i = 0; i < $scope.records.length; i++) {

            if ($scope.perfil == 'A') {

                if ((typeof $scope.comboCambista == "undefined" ||
                    $scope.comboCambista == $scope.records[i].gerente ||
                    $scope.comboCambista == "")
                    && (typeof $scope.comboConcurso == "undefined" ||
                        $scope.comboConcurso == $scope.records[i].concurso ||
                        $scope.comboConcurso == "")
                    && (typeof $scope.comboTipoJogo == "undefined" ||
                        $scope.comboTipoJogo == $scope.records[i].tipo_jogo ||
                        $scope.comboTipoJogo == "")
                    && (typeof $scope.edtNome == "undefined" ||
                        $scope.records[i].nome_apostador.toUpperCase().indexOf($scope.edtNome.toUpperCase()) >= 0 ||
                        $scope.edtNome == "")
                    && (typeof $scope.comboStatusBilhete == "undefined" ||
                        $scope.comboStatusBilhete == $scope.records[i].status_bilhete ||
                        $scope.comboStatusBilhete == "")) {
                    total += parseFloat($scope.records[i].total);
                }
            } else {
                if ((typeof $scope.comboCambista == "undefined" ||
                    $scope.comboCambista == $scope.records[i].cambista ||
                    $scope.comboCambista == "")
                    && (typeof $scope.comboConcurso == "undefined" ||
                        $scope.comboConcurso == $scope.records[i].concurso ||
                        $scope.comboConcurso == "")
                    && (typeof $scope.comboTipoJogo == "undefined" ||
                        $scope.comboTipoJogo == $scope.records[i].tipo_jogo ||
                        $scope.comboTipoJogo == "")
                    && (
                        $scope.records[i].nome_apostador.toUpperCase() === $scope.edtNome.toUpperCase()
                    )
                    && (typeof $scope.comboStatusBilhete == "undefined" ||
                        $scope.comboStatusBilhete == $scope.records[i].status_bilhete ||
                        $scope.comboStatusBilhete == "")) {
                    total += parseFloat($scope.records[i].total);
                }
            }
        }
        return total.toFixed(2);
    }*/

    $scope.getTotalComissao = function () {

    }

    /*$scope.getTotalComissao = function () {
        var total = 0.0;

        for (var i = 0; i < $scope.records.length; i++) {

            if ($scope.perfil == 'A') {

                if ((typeof $scope.comboCambista == "undefined" ||
                    $scope.comboCambista == $scope.records[i].gerente ||
                    $scope.comboCambista == "")
                    && (typeof $scope.comboConcurso == "undefined" ||
                        $scope.comboConcurso == $scope.records[i].concurso ||
                        $scope.comboConcurso == "")
                    && (typeof $scope.comboTipoJogo == "undefined" ||
                        $scope.comboTipoJogo == $scope.records[i].tipo_jogo ||
                        $scope.comboTipoJogo == "")
                    && (typeof $scope.edtNome == "undefined" ||
                        $scope.records[i].nome_apostador.toUpperCase().indexOf($scope.edtNome.toUpperCase()) >= 0 ||
                        $scope.edtNome == "")
                    && (typeof $scope.comboStatusBilhete == "undefined" ||
                        $scope.comboStatusBilhete == $scope.records[i].status_bilhete ||
                        $scope.comboStatusBilhete == "")) {
                    total += parseFloat($scope.records[i].comissao);
                }
            } else {
                if ((typeof $scope.comboCambista == "undefined" ||
                    $scope.comboCambista == $scope.records[i].cambista ||
                    $scope.comboCambista == "")
                    && (typeof $scope.comboConcurso == "undefined" ||
                        $scope.comboConcurso == $scope.records[i].concurso ||
                        $scope.comboConcurso == "")
                    && (typeof $scope.comboTipoJogo == "undefined" ||
                        $scope.comboTipoJogo == $scope.records[i].tipo_jogo ||
                        $scope.comboTipoJogo == "")
                    && (typeof $scope.edtNome == "undefined" ||
                        $scope.records[i].nome_apostador.toUpperCase().indexOf($scope.edtNome.toUpperCase()) >= 0 ||
                        $scope.edtNome == "")
                    && (typeof $scope.comboStatusBilhete == "undefined" ||
                        $scope.comboStatusBilhete == $scope.records[i].status_bilhete ||
                        $scope.comboStatusBilhete == "")) {
                    total += parseFloat($scope.records[i].comissao);
                }

            }

        }
        return total.toFixed(2);
    }*/

    $scope.imprimirBilhete = function (bilhete) {

        user.setBilheteExterno(bilhete);
        $location.path("/conferir_bilhete");

    }

    $scope.repetirJogo = function (txt_pule) {        
        user.setPuleRepeticao(txt_pule);        
        $location.path("/bicho");                
    }





});