
<!doctype html>
<html lang="pt-br">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="A BLT Sistemas tem a solução ideal para que você possa gerenciar fácil seu negócio. Faça jogo em poucos segundos, Utilize em notebooks, tablets ou celulares. Prestação de contas fácil em apenas 1 clique."/>
        <link rel="canonical" href="http://www.betloteria.com" />
        <meta property="og:locale" content="pt_BR" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="BLT Sistemas" />
        <meta property="og:description" content="A BLT Sistemas tem a solução ideal para que você possa gerenciar fácil seu negócio. Faça jogo em poucos segundos, Utilize em notebooks, tablets ou celulares. Prestação de contas fácil em apenas 1 clique." />
        <meta property="og:url" content="http://www.betloteria.com"/>
        <meta property="og:site_name" content="BLT Sistemas - Tudo o que você precisa para a sua banca" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:description" content="A BLT Sistemas tem a solução ideal para que você possa gerenciar fácil seu negócio. Faça jogo em poucos segundos, Utilize em notebooks, tablets ou celulares. Prestação de contas fácil em apenas 1 clique." />
        <meta name="twitter:title" content="BLT Sistemas - Tudo o que você precisa para a sua banca. Conheça agora!" />
        <title>BLT Sistemas - Tudo o que você precisa para a sua banca.</title>

        <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="./assets/css/main-style.css">
        <link rel="stylesheet" href="./assets/css/responsive.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="shortcut icon" href="./favicon.ico">

        <script defer src="./assets/js/jquery-3.3.1.min.js"></script>
        <script defer src="./assets/js/owl.carousel.min.js"></script>
        <script defer src="./assets/js/popper.min.js"></script>
        <script defer src="./assets/js/bootstrap.min.js"></script>

        <script defer src="./assets/js/zoom.js"></script>

    </head>

    <body>
        <section class="section hero">
            <div class="container">
                <nav class="navbar navbar-dark navbar-expand-lg">
                    <a class="navbar-brand main_logo" href="#"> <img src="./assets/images/logo_2.png" alt="Bemacash. By Totvs"> </a> <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main_navbar" aria-controls="main_navbar" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button> 
                    <div class="collapse navbar-collapse" id="main_navbar">
                        <ul class="navbar-nav ml-auto bema_ulListMenu"> 
                            <a href="index.php#contato" class="header_btn btn btn-primary btn-download">Entre em Contato com nossa equipe de vendas</a> 
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="container">
                <div class="row hero_row">
                    <div class="col-md-6 offset-3">
                        <div class="boxEnviado">
                            <img class="rounded mx-auto d-block" src="assets/images/check.jpg" width="110px" height="110px">
                            <h3 class="text-center">Formulário enviado com sucesso!</h3>
                            <p class="text-center">Em breve entraremos em contato com você. Aguarde...</p>
                            <a href="index.php" class="btn btn-primary btn-lg btn-download">Voltar ao Site</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BEGIN JIVOSITE CODE {literal} -->
        <script type='text/javascript'>
        (function(){ var widget_id = 'vUFjR6HOuP';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
        s.src = '//code.jivosite.com/script/widget/'+widget_id
            ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
        if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
        else{w.addEventListener('load',l,false);}}})();
        </script>
        <!-- {/literal} END JIVOSITE CODE -->
        
        <script src="contact.js"></script>
        <script defer src="./assets/js/main.js"></script>   
        
    </body>
</html>