<?php

header('Access-Control-Allow-Origin: *');
session_start();

// Conexão Azure
$con_login=mysqli_init(); 
mysqli_real_connect($con_login, "betloteriaprd.mysql.database.azure.com", "betloteria@betloteriaprd", "brq@Rol@maxima", "betloteria", 3306);

// Conexão Local
//$con_login = mysqli_connect('localhost', 'betloteria@betloteriaprd', 'brq@Rol@maxima', 'betloteria');

mysqli_set_charset( $con_login, 'utf8');

$site = mysqli_real_escape_string($con_login, $_POST['site']);
$operacao = mysqli_real_escape_string($con_login, $_POST['operacao']);
$schema = 'b' . $site;
$query = "SELECT s.*, c.modalidade_padrao_bv
		  FROM $schema.site s
			LEFT JOIN $schema.configuracao_geral c on s.cod_site = c.cod_site
			LEFT JOIN $schema.areas a on c.cod_area = a.cod_area 
		  WHERE s.cod_site='$site' and s.status = 'A' AND a.PADRAO = 'S' "; 
$result = mysqli_query($con_login, $query);		 

$return_arr = array();

if(mysqli_num_rows($result) == 0) {

	$response['status'] = 'error';
	
} else {
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	$modalidade = $row['modalidade_padrao_bv'];
	switch ($modalidade) {
		case 'B':
			$response['modalidade_padrao_bv'] = 'bicho';
			break;
		case 'Q':
			$response['modalidade_padrao_bv'] = 'quininha';
			break;
		case 'L':
			$response['modalidade_padrao_bv'] = 'lotinha';
			break;
		case 'U':
			$response['modalidade_padrao_bv'] = 'supersena';
			break;
		default:
			$response['modalidade_padrao_bv'] = 'seninha';
			break;						
			
	}
	$response['nome'] = '';
	$response['codigo'] = '';
	$response['site'] = '';
	$response['perfil'] = '';	
	$response['schema'] = $schema;

    $_SESSION['schema'] = $schema; 
}
$con_login->close();
echo json_encode($response);
?>