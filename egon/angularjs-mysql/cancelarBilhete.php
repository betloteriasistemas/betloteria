<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

try {
    $con->begin_transaction();

    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
    $txt_pule = mysqli_real_escape_string($con, $_POST['txt_pule']);

    $query = "select PERFIL = 'A' perfil_admin, COUNT(*) contador from usuario where COD_USUARIO = '$cod_usuario' AND COD_SITE = '$cod_site'";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    if ($row['contador'] == 0) {
        throw new Exception('Bilhete não pode ser cancelado!');
    }

    if ($row['perfil_admin'] == 0) {

        $query = 'SET @@session.time_zone = "-03:00"';
        $result = mysqli_query($con, $query);

        $query = "select distinct
                	(j.DATA_JOGO < current_date) as data_passou,
	                (j.DATA_JOGO <= current_date) as data_retorno,
                    case
                        when j.TIPO_JOGO in ('B','2') THEN (CURRENT_TIME >= j.HORA_EXTRACAO)
                        else (CURRENT_TIME >= '19:50')
                    end as hora_retorno
                    from bilhete b inner join aposta a on b.COD_BILHETE = a.COD_BILHETE
                    inner join jogo j on a.COD_JOGO = j.COD_JOGO
                    where (b.TXT_PULE = '$txt_pule' or b.TXT_PULE_PAI = '$txt_pule')
                        and b.COD_SITE = '$cod_site'";

        $result = mysqli_query($con, $query);

        $contador = 0;
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

            $contador = $contador + 1;

            if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
                throw new Exception('Bilhete não pode ser cancelado!');
            }

            if ($contador == mysqli_num_rows($result)) {
                break;
            }
        }

        if ($contador == 0) {
            throw new Exception('Bilhete não pode ser cancelado!');
        }
    }

    $query = " select sum(valor_aposta) valor_aposta from aposta
                where cod_bilhete IN
                    (select cod_bilhete from bilhete
                    where txt_pule = '$txt_pule' || txt_pule_pai = '$txt_pule') ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $somaAposta = $row['valor_aposta'];

    $query = " select distinct cod_usuario from bilhete
                 where txt_pule = '$txt_pule' || txt_pule_pai = '$txt_pule' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $cod_usuario_bilhete = $row['cod_usuario'];

    $stmt = $con->prepare("update bilhete set status_bilhete = 'C' where txt_pule = ? || txt_pule_pai = ?");
    $stmt->bind_param("ss", $txt_pule, $txt_pule);

    $stmt->execute();

    $query = " select cod_bilhete from bilhete  
                 where txt_pule = '$txt_pule' || txt_pule_pai = '$txt_pule' ";
    $result = mysqli_query($con, $query);
    $bilhete_arr = array();
    $count = 0;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $bilhete_arr[$count] = $row['cod_bilhete'];
        $count = $count + 1;
    }

    if (count($bilhete_arr) > 0) {

        $in  = str_repeat('?,', count($bilhete_arr) - 1) . '?';
        $sql = "update aposta set status = 'C', comissao = 0, comissao_gerente = 0 
                    where cod_bilhete in ($in) ";
        $stmt = $con->prepare($sql);
        $types = str_repeat('i', count($bilhete_arr));
        $stmt->bind_param($types, ...$bilhete_arr);
        $stmt->execute();
    }

    $stmt = $con->prepare("update usuario
                set saldo = saldo + ?
                where cod_usuario = ? ");
    $stmt->bind_param("di", $somaAposta, $cod_usuario_bilhete);
    $stmt->execute();

    $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_SITE = '$cod_site'
              and COD_USUARIO = '$cod_usuario_bilhete' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $response['saldo'] = $row['SALDO'];
    $response['status'] = "OK";

    $stmt->close();

    inserir_auditoria(
        $con,
        $cod_usuario,
        $cod_site,
        AUD_BILHETE_CANCELADO,
        descreverCancelamentoBilhete($txt_pule)
    );
    $con->commit();
    $con->close();
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);