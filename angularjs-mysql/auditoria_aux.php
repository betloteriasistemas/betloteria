<?php

require_once('funcoes_auxiliares.php');

function descreverCriacaoBilhete($nome_apostador, $telefone_apostador, $data_atual, $pule, $numero_bilhete, $pulePai, $qtd_bilhetes_solicitados, $qtd_bilhetes_gerados)
{
    return getBold('Bilhete número: ') . $numero_bilhete .
        (!is_null($nome_apostador) ? '; ' . getBold('Apostador: ') . $nome_apostador : '') .
        (!is_null($telefone_apostador) ? '; ' . getBold('Telefone: ') . $telefone_apostador : '') .
        (!is_null($data_atual) ? '; ' . getBold('Data: ') . converterData($data_atual) : '') .
        (!is_null($pule) ? '; ' . getBold('Pule: ') . $pule : '') .
        (!is_null($pulePai) ? '; ' . getBold('Pule pai: ') . $pulePai : '') .
        (!is_null($qtd_bilhetes_solicitados) ? '; ' . getBold('Qtd Bilhetes Solicitados: ') . $qtd_bilhetes_solicitados : '') .
        (!is_null($qtd_bilhetes_gerados) ? '; ' . getBold('Qtd Bilhetes Gerados: ') . $qtd_bilhetes_gerados : '') .
        '.';
}

function descreverApostaBicho(
    $txtAposta,
    $valorAposta,
    $possivelRetorno,
    $premioDo,
    $premioAo,
    $tipoJogo,
    $inversoes,
    $comissao
) {
    return getBold('** BICHO **') .
        (!is_null($tipoJogo) ? getBold(' Tipo jogo: ') . converterDescricaoTipoJogo($tipoJogo) : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($premioDo) ? '; ' . getBold('Do prêmio: ') . $premioDo : '') .
        (!is_null($premioAo) ? '; ' . getBold('Ao prêmio: ') . $premioAo : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($possivelRetorno) ? '; ' . getBold('Possível retorno: ') . converterValorReal($possivelRetorno) : '') .
        (!is_null($inversoes) ? '; ' . getBold('Inversões: ') . $inversoes : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverAposta2pra500($txtAposta, $valorAposta, $valorAcumulado, $comissao)
{
    return getBold('** ' . intval($valorAposta) . ' PRA ' . intval($valorAcumulado) . ' **') .
        (!is_null($txtAposta) ? getBold(' Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($valorAcumulado) ? '; ' . getBold('Valor acumulado: ') . converterValorReal($valorAcumulado) : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverApostaSeninhaSorte(
    $qtdNumeros,
    $txtAposta,
    $valorAposta,
    $possivelRetorno,
    $seninhaSorte,
    $possivelRetornoQuina,
    $possivelRetornoQuadra,
    $comissao
) {
    return getBold('** SENINHA DA SORTE **') .
        (!is_null($qtdNumeros) ? getBold(' Quantidade de números: ') . $qtdNumeros : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($possivelRetorno) ? '; ' . getBold('Possível retorno: ') . converterValorReal($possivelRetorno) : '') .
        (!is_null($possivelRetornoQuina) ? '; ' . getBold('Possível retorno quina: ') . converterValorReal($possivelRetornoQuina) : '') .
        (!is_null($possivelRetornoQuadra) ? '; ' . getBold('Possível retorno quadra: ') . converterValorReal($possivelRetornoQuadra) : '') .
        (!is_null($seninhaSorte) ? '; ' . getBold('Seninha da sorte: ') . converterSimNao($seninhaSorte) : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverApostaSuperSena(
    $qtdNumeros,
    $txtAposta,
    $valorAposta,
    $possivelRetorno,
    $possivelRetornoQuina,
    $possivelRetornoQuadra,
    $comissao
) {
    return getBold('** SUPER SENA **') .
        (!is_null($qtdNumeros) ? getBold(' Quantidade de números: ') . $qtdNumeros : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($possivelRetorno) ? '; ' . getBold('Possível retorno: ') . converterValorReal($possivelRetorno) : '') .
        (!is_null($possivelRetornoQuina) ? '; ' . getBold('Possível retorno quina: ') . converterValorReal($possivelRetornoQuina) : '') .
        (!is_null($possivelRetornoQuadra) ? '; ' . getBold('Possível retorno quadra: ') . converterValorReal($possivelRetornoQuadra) : '') .        
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverApostaQuininhaSorte($qtdNumeros, $txtAposta, $valorAposta, $possivelRetornoQuina, $quininhaSorte, $possivelRetornoQuadra, $possivelRetornoTerno, $comissao)
{
    return getBold('** QUININHA DA SORTE ** ') .
        (!is_null($qtdNumeros) ? getBold(' Quantidade de números: ') . $qtdNumeros : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($possivelRetornoQuina) ? '; ' . getBold('Possível retorno: ') . converterValorReal($possivelRetornoQuina) : '') .
        (!is_null($possivelRetornoQuadra) ? '; ' . getBold('Possível retorno quadra: ') . converterValorReal($possivelRetornoQuadra) : '') .
        (!is_null($possivelRetornoTerno) ? '; ' . getBold('Possível retorno terno: ') . converterValorReal($possivelRetornoTerno) : '') .
        (!is_null($quininhaSorte) ? '; ' . getBold('Quininha da sorte: ') . converterSimNao($quininhaSorte) : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverApostaLotinha($qtdNumeros, $txtAposta, $valorAposta, $possivelRetorno, $comissao)
{
    return getBold('** LOTINHA ** ') .
        (!is_null($qtdNumeros) ? getBold(' Quantidade de números: ') . $qtdNumeros : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($possivelRetorno) ? '; ' . getBold('Possível retorno: ') . converterValorReal($possivelRetorno) : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverApostaRifa($qtdNumeros, $txtAposta, $valorAposta, $comissao)
{
    return getBold('** RIFA ** ') .
        (!is_null($qtdNumeros) ? getBold(' Quantidade de números: ') . $qtdNumeros : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverApostaQuininhaSeninha($tipo_jogo, $qtdNumeros, $txtAposta, $valorAposta, $possivelRetorno, $comissao)
{
    return (!is_null($tipo_jogo) ? ($tipo_jogo == 'Q' ? getBold('** QUINIHA **') : getBold('** SENINHA **')) .
        getBold(' Quantidade de números: ') . $qtdNumeros : '') .
        (!is_null($txtAposta) ? '; ' . getBold('Aposta: ') . $txtAposta : '') .
        (!is_null($valorAposta) ? '; ' . getBold('Valor da aposta: ') . converterValorReal($valorAposta) : '') .
        (!is_null($possivelRetorno) ? '; ' . getBold('Possível retorno: ') . converterValorReal($possivelRetorno) : '') .
        (!is_null($comissao) ? '; ' . getBold('Comissão: ') . converterValorReal($comissao) : '') .
        '.';
}

function descreverPagamentoBilhete($pule)
{
    return 'Bilhete de pule ' . $pule . ' pago.';
}

function descreverEdicaoUsuario(
    $oldUsuario,
    $perfil,
    $nome_gerente,
    $nome,
    $login,
    $email,
    $limite,
    $area,
    $flg_seninha,
    $comissao_seninha,
    $flg_quininha,
    $comissao_quininha,
    $flg_lotinha,
    $comissao_lotinha,
    $flg_bicho,
    $comissao_bicho,
    $flg_2pra500,
    $comissao_2pra500,
    $flg_rifa,
    $comissao_rifa,
    $flg_supersena,
    $comissao_supersena
) {
    return 
        getBold('Usuário: ') . $nome .
        returnIfDifferent(getBold('Nome do usuário: '), $oldUsuario['NOME'], $nome) .
        returnIfDifferent(getBold('Login: '), $oldUsuario['LOGIN'], $login) .
        returnIfDifferent(getBold('Email: '), $oldUsuario['EMAIL'], $email) .
        (!is_null($perfil) ? returnIfDifferent(getBold('Perfil: '), converterPerfil($oldUsuario['PERFIL']), converterPerfil($perfil)) : '') .
        (!is_null($nome_gerente) ? returnIfDifferent(getBold('Gerente: '), $oldUsuario['GERENTE'],$nome_gerente) : '') .
        returnIfDifferent(getBold('Área: '), $oldUsuario['AREA'], $area) .
        returnIfDifferent(
            getBold('Comissão Seninha: '),
            ($oldUsuario['FLG_SENINHA'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_SENINHA']) : 'desabilitada'),
            ($flg_seninha == 'S' ? converterValorPercentual($comissao_seninha) : 'desabilitada')
        ) .
        returnIfDifferent(
            getBold('Comissão Quininha: '),
            ($oldUsuario['FLG_QUININHA'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_QUININHA']) : 'desabilitada'),
            ($flg_quininha == 'S' ? converterValorPercentual($comissao_quininha) : 'desabilitada')
        ) .
        returnIfDifferent(
            getBold('Comissão Lotinha: '),
            ($oldUsuario['FLG_LOTINHA'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_LOTINHA']) : 'desabilitada'),
            ($flg_lotinha == 'S' ? converterValorPercentual($comissao_lotinha) : 'desabilitada')
        ) .
        returnIfDifferent(
            getBold('Comissão Bicho: '),
            ($oldUsuario['FLG_BICHO'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_BICHO']) : 'desabilitada'),
            ($flg_bicho == 'S' ? converterValorPercentual($comissao_bicho) : 'desabilitada')
        ) .
        returnIfDifferent(
            getBold('Comissão 2 pra 500: '),
            ($oldUsuario['FLG_2PRA500'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_2PRA500']) : 'desabilitada'),
            ($flg_2pra500 == 'S' ? converterValorPercentual($comissao_2pra500) : 'desabilitada')
        ) .
        returnIfDifferent(
            getBold('Comissão RIFA: '),
            ($oldUsuario['FLG_RIFA'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_RIFA']) : 'desabilitada'),
            ($flg_rifa == 'S' ? converterValorPercentual($comissao_rifa) : 'desabilitada')
        ) .
        returnIfDifferent(
            getBold('Comissão SUPER SENA: '),
            ($oldUsuario['FLG_SUPER_SENA'] == 'S' ? converterValorPercentual($oldUsuario['PCT_COMISSAO_SUPERSENA']) : 'desabilitada'),
            ($flg_supersena == 'S' ? converterValorPercentual($comissao_supersena) : 'desabilitada')
        ) .
        returnIfDifferent(getBold('Limite: '), converterValorReal($oldUsuario['LIMITE']), converterValorReal($limite));
}

function descreverCriacaoUsuario(
    $perfil,
    $nome_gerente,
    $nome,
    $login,
    $email,
    $limite,
    $area,
    $flg_seninha,
    $comissao_seninha,
    $flg_quininha,
    $comissao_quininha,
    $flg_lotinha,
    $comissao_lotinha,
    $flg_bicho,
    $comissao_bicho,
    $flg_2pra500,
    $comissao_2pra500,
    $flg_rifa,
    $comissao_rifa,
    $flg_supersena,
    $comissao_supersena
) {
    return 
        (!is_null($nome) ? getBold('Nome do usuário: ') . $nome  . '; ' : '') .
        (!is_null($login) ? getBold('Login: ') . $login  . '; ' : '') .
        (!is_null($email) ? getBold('Email: ') . $email  . '; ' : '') .
        (!is_null($perfil) ? getBold('Perfil: ') . converterPerfil($perfil) . '; '  : '') .
        (!is_null($nome_gerente) ? getBold('Gerente: ') . $nome_gerente  . '; ' : '') .
        (!is_null($email) ? getBold('Área: ') . $area  . '; ' : '') .
        (!is_null($flg_seninha) ? '; ' . getBold('Comissão Seninha: ') . ($flg_seninha == 'S' ? converterValorPercentual($comissao_seninha) : 'desabilitada') : '') .
        (!is_null($flg_quininha) ? '; ' . getBold('Comissão Quininha: ') . ($flg_quininha == 'S' ? converterValorPercentual($comissao_quininha) : 'desabilitada') : '') .
        (!is_null($flg_lotinha) ? '; ' . getBold('Comissão Lotinha: ') . ($flg_lotinha == 'S' ? converterValorPercentual($comissao_lotinha) : 'desabilitada') : '') .
        (!is_null($flg_bicho) ? '; ' . getBold('Comissão Bicho: ')   . ($flg_bicho == 'S' ? converterValorPercentual($comissao_bicho) : 'desabilitada') : '') .
        (!is_null($flg_2pra500) ? '; ' . getBold('Comissão 2 pra 500: ') . ($flg_2pra500 == 'S' ? converterValorPercentual($comissao_2pra500) : 'desabilitada') : '') .
        (!is_null($flg_rifa) ? '; ' . getBold('Comissão Rifa: ') . ($flg_rifa == 'S' ? converterValorPercentual($comissao_rifa) : 'desabilitada') : '') .
        (!is_null($flg_supersena) ? '; ' . getBold('Comissão Super Sena: ') . ($flg_supersena == 'S' ? converterValorPercentual($comissao_supersena) : 'desabilitada') : '') .
        (!is_null($limite) ? '; ' . getBold('Limite: ') . converterValorReal($limite) : '') . '.';
}

function descreverEdicaoComissoesBicho(
    $oldComissao,
    $COMISSAO_MILHAR_CENTENA,
    $COMISSAO_MILHAR_SECA,
    $COMISSAO_MILHAR_INVERTIDA,
    $COMISSAO_CENTENA,
    $COMISSAO_CENTENA_INVERTIDA,
    $COMISSAO_GRUPO,
    $COMISSAO_DUQUE_GRUPO,
    $COMISSAO_TERNO_GRUPO,
    $COMISSAO_QUINA_GRUPO,
    $COMISSAO_DEZENA,
    $COMISSAO_DUQUE_DEZENA,
    $COMISSAO_TERNO_DEZENA,
    $COMISSAO_PASSE_SECO,
    $COMISSAO_PASSE_COMBINADO,
    $COMISSAO_5P100
) {
    $result =
        returnIfDifferent(
            getBold('Comissão Milhar Seca: '),
            converterValorPercentual($oldComissao['COMISSAO_MILHAR_SECA']),
            converterValorPercentual($COMISSAO_MILHAR_SECA)
        ) .

        returnIfDifferent(
            getBold('Comissão Milhar Centena: '),
            converterValorPercentual($oldComissao['COMISSAO_MILHAR_CENTENA']),
            converterValorPercentual($COMISSAO_MILHAR_CENTENA)
        ) .

        returnIfDifferent(
            getBold('Comissão Milhar Invertida: '),
            converterValorPercentual($oldComissao['COMISSAO_MILHAR_INVERTIDA']),
            converterValorPercentual($COMISSAO_MILHAR_INVERTIDA)
        ) .

        returnIfDifferent(
            getBold('Comissão Centena: '),
            converterValorPercentual($oldComissao['COMISSAO_CENTENA']),
            converterValorPercentual($COMISSAO_CENTENA)
        ) .

        returnIfDifferent(
            getBold('Comissão Centena Invertida: '),
            converterValorPercentual($oldComissao['COMISSAO_CENTENA_INVERTIDA']),
            converterValorPercentual($COMISSAO_CENTENA_INVERTIDA)
        ) .

        returnIfDifferent(
            getBold('Comissão Grupo: '),
            converterValorPercentual($oldComissao['COMISSAO_GRUPO']),
            converterValorPercentual($COMISSAO_GRUPO)
        ) .

        returnIfDifferent(
            getBold('Comissão Duque Grupo: '),
            converterValorPercentual($oldComissao['COMISSAO_DUQUE_GRUPO']),
            converterValorPercentual($COMISSAO_DUQUE_GRUPO)
        ) .

        returnIfDifferent(
            getBold('Comissão Terno Grupo: '),
            converterValorPercentual($oldComissao['COMISSAO_TERNO_GRUPO']),
            converterValorPercentual($COMISSAO_TERNO_GRUPO)
        ) .

        returnIfDifferent(
            getBold('Comissão Quina Grupo: '),
            converterValorPercentual($oldComissao['COMISSAO_QUINA_GRUPO']),
            converterValorPercentual($COMISSAO_QUINA_GRUPO)
        ) .

        returnIfDifferent(
            getBold('Comissão Dezena: '),
            converterValorPercentual($oldComissao['COMISSAO_DEZENA']),
            converterValorPercentual($COMISSAO_DEZENA)
        ) .

        returnIfDifferent(
            getBold('Comissão Duque Dezena: '),
            converterValorPercentual($oldComissao['COMISSAO_DUQUE_DEZENA']),
            converterValorPercentual($COMISSAO_DUQUE_DEZENA)
        ) .

        returnIfDifferent(
            getBold('Comissão Terno Dezena: '),
            converterValorPercentual($oldComissao['COMISSAO_TERNO_DEZENA']),
            converterValorPercentual($COMISSAO_TERNO_DEZENA)
        ) .

        returnIfDifferent(
            getBold('Comissão Passe Seco: '),
            converterValorPercentual($oldComissao['COMISSAO_PASSE_SECO']),
            converterValorPercentual($COMISSAO_PASSE_SECO)
        ) .

        returnIfDifferent(
            getBold('Comissão Passe Combinado: '),
            converterValorPercentual($oldComissao['COMISSAO_PASSE_COMBINADO']),
            converterValorPercentual($COMISSAO_PASSE_COMBINADO)
        ).

        returnIfDifferent(
            getBold('Comissão 5 Pra 100: '),
            converterValorPercentual($oldComissao['COMISSAO_5P100']),
            converterValorPercentual($COMISSAO_5P100)
        );

    if (trim($result) != '') {
        $result =  getBold(' ** COMISSÕES BICHO ** ') . $result;
    }
    return $result;
}

function descreverComissoesBicho(
    $COMISSAO_MILHAR_CENTENA,
    $COMISSAO_MILHAR_SECA,
    $COMISSAO_MILHAR_INVERTIDA,
    $COMISSAO_CENTENA,
    $COMISSAO_CENTENA_INVERTIDA,
    $COMISSAO_GRUPO,
    $COMISSAO_DUQUE_GRUPO,
    $COMISSAO_TERNO_GRUPO,
    $COMISSAO_QUINA_GRUPO,
    $COMISSAO_DEZENA,
    $COMISSAO_DUQUE_DEZENA,
    $COMISSAO_TERNO_DEZENA,
    $COMISSAO_PASSE_SECO,
    $COMISSAO_PASSE_COMBINADO,
    $COMISSAO_5P100
) {
    return getBold('** COMISSÕES BICHO **') .
        '; ' . getBold('Comissão Milhar Seca: ') . converterValorPercentual($COMISSAO_MILHAR_SECA) .
        '; ' . getBold('Comissão Milhar Centena: ') . converterValorPercentual($COMISSAO_MILHAR_CENTENA) .
        '; ' . getBold('Comissão Milhar Invertida: ') . converterValorPercentual($COMISSAO_MILHAR_INVERTIDA) .
        '; ' . getBold('Comissão Centena: ') . converterValorPercentual($COMISSAO_CENTENA) .
        '; ' . getBold('Comissão Centena Invertida: ') . converterValorPercentual($COMISSAO_CENTENA_INVERTIDA) .
        '; ' . getBold('Comissão Grupo: ') . converterValorPercentual($COMISSAO_GRUPO) .
        '; ' . getBold('Comissão Duque Grupo: ') . converterValorPercentual($COMISSAO_DUQUE_GRUPO) .
        '; ' . getBold('Comissão Terno Grupo: ') . converterValorPercentual($COMISSAO_TERNO_GRUPO) .
        '; ' . getBold('Comissão Quina Grupo: ') . converterValorPercentual($COMISSAO_QUINA_GRUPO) .
        '; ' . getBold('Comissão Dezena: ') . converterValorPercentual($COMISSAO_DEZENA) .
        '; ' . getBold('Comissão Duque Dezena: ') . converterValorPercentual($COMISSAO_DUQUE_DEZENA) .
        '; ' . getBold('Comissão Terno Dezena: ') . converterValorPercentual($COMISSAO_TERNO_DEZENA) .
        '; ' . getBold('Comissão Passe Seco: ') . converterValorPercentual($COMISSAO_PASSE_SECO) .
        '; ' . getBold('Comissão Passe Combinado: ') . converterValorPercentual($COMISSAO_PASSE_COMBINADO) .
        '; ' . getBold('Comissão 5 Pra 100: ') . converterValorPercentual($COMISSAO_5P100) . '.';
}

function descreverPrestacaoContaUsuario($nome, $valor)
{
    return getBold('Usuário: ') . $nome .
        '; ' . getBold('Valor: ') . converterValorReal($valor);
}

function descreverSenhaResetada($nome)
{
    return 'Senha do usuário "' . $nome . '" resetada.';
}

function descreverDesativacaoUsuario($nome, $status, $perfil)
{
    if ($perfil == 'A') {
        return 'O gerente "' . $nome . '" e seus cambistas foram ' .
            ($status == 'A' ? 'inativados.' : 'ativados.');
    }
    return 'O usuário "' . $nome . '" foi ' .
        ($status == 'A' ? 'inativado.' : 'ativado.');
}

function descreverCancelamentoBilhete($txtPule)
{
    return 'Bilhete de pule ' . $txtPule . ' cancelado.';
}

function descreverLancamento(
    $data,
    $criadoPor,
    $tipo,
    $colaborador,
    $motivo,
    $ajudaCusto,
    $valor
) {
    return getBold('Data: ') . converterData($data, 'd/m/Y') .
        '; ' . getBold('Criado por: ') . $criadoPor .
        '; ' . getBold('Tipo: ') . converterDescricaoTipoLancamento($tipo) .
        '; ' . getBold('Colaborador: ') . $colaborador .
        '; ' . getBold('Motivo: ') . (!empty($motivo) ? $motivo : '') .
        '; ' . getBold('Ajuda de Custo: ') . converterSimNao($ajudaCusto) .
        '; ' . getBold('Valor: ') . converterValorReal($valor) . '.';
}

function descreverExtracao($codigo, $hora, $descricao, $domingo, $segunda, $terca, $quarta, 
            $quinta, $sexta, $sabado, $tipo_jogo, $qtd_premios, $prazo_bloqueio,
            $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena)
{
    return getBold('Extração: ') . '#' . $codigo .
        '; ' . getBold('Hora: ') . $hora .
        '; ' . getBold('Descrição: ') . $descricao .
        '; ' . getBold('Domingo: ') . converterSimNao($domingo) .
        '; ' . getBold('Segunda: ') . converterSimNao($segunda) .
        '; ' . getBold('Terça: ') . converterSimNao($terca) .
        '; ' . getBold('Quarta: ') . converterSimNao($quarta) .
        '; ' . getBold('Quinta: ') . converterSimNao($quinta) .
        '; ' . getBold('Sexta: ') . converterSimNao($sexta) .
        '; ' . getBold('Sábado: ') . converterSimNao($sabado) .
        '; ' . getBold('Tipo do Jogo: ') . converterTipoJogo($tipo_jogo) . 
        '; ' . getBold('Quantidade de Prêmios: ') . $qtd_premios .
        '; ' . getBold('Prazo de Bloqueio: ') . - $prazo_bloqueio . 
        ($tipo_jogo == "B" ?
        '; ' . getBold('** LIMITES **') .
        '; ' . getBold('Milhar: ') . $milhar .
        '; ' . getBold('Centena: ') . $centena .
        '; ' . getBold('Grupo: ') . $grupo .
        '; ' . getBold('Duque de Grupo: ') . $duque_grupo .
        '; ' . getBold('Terno de Grupo: ') . $terno_grupo .
        '; ' . getBold('Quina de Grupo: ') . $quina_grupo .
        '; ' . getBold('Dezena: ') . $dezena .
        '; ' . getBold('Duque de Dezena: ') . $duque_dezena .
        '; ' . getBold('Terno de Dezena: ') . $terno_dezena . '.'
        : '.');
}

function descreverEdicaoExtracao($oldExtracao, $codigo, $hora, $descricao, 
            $domingo, $segunda, $terca, $quarta, $quinta, $sexta, $sabado, 
            $tipo_jogo, $qtd_premios, $prazo_bloqueio, $automatizada,
            $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, 
            $dezena, $duque_dezena, $terno_dezena)
{
    return getBold('Extração: ') . '#' . $codigo . ' ' .
        '; ' . getBold('Descrição: ') . $descricao . 
        '; ' . getBold('Hora: ') . $hora .
        '; ' . getBold('Automatizada: ') . converterSimNao($automatizada) . '; ' . 
        returnIfDifferent(getBold('Hora: '), $oldExtracao['HORA_EXTRACAO'], $hora) .
        returnIfDifferent(getBold('Descrição: '), $oldExtracao['DESCRICAO'], $descricao) .
        returnIfDifferent(getBold('Domingo: '), converterSimNao($oldExtracao['DOMINGO']), converterSimNao($domingo)) .
        returnIfDifferent(getBold('Segunda: '), converterSimNao($oldExtracao['SEGUNDA']), converterSimNao($segunda)) .
        returnIfDifferent(getBold('Terça: '), converterSimNao($oldExtracao['TERCA']), converterSimNao($terca)) .
        returnIfDifferent(getBold('Quarta: '), converterSimNao($oldExtracao['QUARTA']), converterSimNao($quarta)) .
        returnIfDifferent(getBold('Quinta: '), converterSimNao($oldExtracao['QUINTA']), converterSimNao($quinta)) .
        returnIfDifferent(getBold('Sexta: '), converterSimNao($oldExtracao['SEXTA']), converterSimNao($sexta)) .
        returnIfDifferent(getBold('Sábado: '), converterSimNao($oldExtracao['SABADO']), converterSimNao($sabado)) .
        returnIfDifferent(getBold('Tipo do Jogo: '), converterTipoJogo($oldExtracao['TIPO_JOGO']), converterTipoJogo($tipo_jogo)) . 
        returnIfDifferent(getBold('Quantidade de Prêmios: '), $oldExtracao['QTD_PREMIOS'], $qtd_premios) . 
        returnIfDifferent(getBold('Prazo de Bloqueio: '), - $oldExtracao['PRAZO_BLOQUEIO'], - $prazo_bloqueio) . 
        ($tipo_jogo == "B" ?
        '; ' . getBold(' ** LIMITES ** ') .
        returnIfDifferent(getBold('Milhar: '), converterValorPercentual($oldExtracao['MILHAR']), converterValorPercentual($milhar)) .
        returnIfDifferent(getBold('Centena: '), converterValorPercentual($oldExtracao['CENTENA']), converterValorPercentual($centena)) .
        returnIfDifferent(getBold('Grupo: '), converterValorPercentual($oldExtracao['GRUPO']), converterValorPercentual($grupo)) .
        returnIfDifferent(getBold('Duque de Grupo: '), converterValorPercentual($oldExtracao['DUQUE_GRUPO']), converterValorPercentual($duque_grupo)) .
        returnIfDifferent(getBold('Terno de Grupo: '), converterValorPercentual($oldExtracao['TERNO_GRUPO']), converterValorPercentual($terno_grupo)) .
        returnIfDifferent(getBold('Quina de Grupo: '), converterValorPercentual($oldExtracao['QUINA_GRUPO']), converterValorPercentual($quina_grupo)) .
        returnIfDifferent(getBold('Dezena: '), converterValorPercentual($oldExtracao['DEZENA']), converterValorPercentual($dezena)) .
        returnIfDifferent(getBold('Duque de Dezena: '), converterValorPercentual($oldExtracao['DUQUE_DEZENA']), converterValorPercentual($duque_dezena)) .
        returnIfDifferent(getBold('Terno de Dezena: '), converterValorPercentual($oldExtracao['TERNO_DEZENA']), converterValorPercentual($terno_dezena)) . '.'
        : '.');
}

function descreverExclusaoExtracao($codigo)
{
    return getBold('Extração ') . '#' . $codigo . ' excluída.';
}

function descreverCriacaoConfiguracaoGeral(
    $desc_area,
    $FLG_SENINHA,
    $FLG_2PRA500,
    $FLG_QUININHA,
    $FLG_LOTINHA,
    $FLG_BICHO,
    $FLG_RIFA,
    $MINUTOS_CANCELAMENTO,
    $VALOR_MIN_APOSTA,
    $VALOR_MAX_APOSTA,
    $NU_BILHETE_GRANDE,
    $NU_BILHETE_NEGRITO,
    $NU_BILHETE_SUBLINHADO
) {
    return getBold(' Configuração Geral: ') . getBold('Área: ') . $desc_area . '; ' .
        (!is_null($FLG_SENINHA) ? getBold('Habilita Modalidade Seninha: ') . converterSimNao($FLG_SENINHA) . '; ' : '') .
        (!is_null($FLG_QUININHA) ? getBold('Habilita Modalidade Quininha: ') . converterSimNao($FLG_QUININHA) . '; ' : '') .
        (!is_null($FLG_LOTINHA) ? getBold('Habilita Modalidade Lotinha: ') . converterSimNao($FLG_LOTINHA) . '; ' : '') .
        (!is_null($FLG_BICHO) ? getBold('Habilita Modalidade Bicho: ') . converterSimNao($FLG_BICHO) . '; ' : '') .
        (!is_null($FLG_2PRA500) ? getBold('Habilita Modalidade 2 pra 500: ') . converterSimNao($FLG_2PRA500) . '; ' : '') .
        (!is_null($FLG_RIFA) ? getBold('Habilita Modalidade Rifa: ') . converterSimNao($FLG_RIFA) . '; ' : '') .
        (!is_null($NU_BILHETE_GRANDE) ? getBold('Número Grande: ') . converterSimNao($NU_BILHETE_GRANDE) . '; ' : '') .
        (!is_null($NU_BILHETE_NEGRITO) ? getBold('Número em Negrito: ') . converterSimNao($NU_BILHETE_NEGRITO) . '; ' : '') .
        (!is_null($NU_BILHETE_SUBLINHADO) ? getBold('Número Sublinhado: ') . converterSimNao($NU_BILHETE_SUBLINHADO) . '; ' : '') .
        (!is_null($MINUTOS_CANCELAMENTO) ? getBold('Minutos Cancelamento: ') . $MINUTOS_CANCELAMENTO . ';' : '') .
        (!is_null($VALOR_MIN_APOSTA) ? getBold('Valor Mínimo de Aposta: ') . $VALOR_MIN_APOSTA . ';' : '') .
        (!is_null($VALOR_MAX_APOSTA) ? getBold('Valor Máximo de Aposta: ') . $VALOR_MAX_APOSTA . '.' : '');
}

function descreverEdicaoConfiguracaoGeral(
    $oldConfig,
    $desc_area,
    $FLG_SENINHA,
    $FLG_2PRA500,
    $FLG_QUININHA,
    $FLG_LOTINHA,
    $FLG_BICHO,
    $FLG_RIFA,
    $MINUTOS_CANCELAMENTO,
    $VALOR_MIN_APOSTA,
    $VALOR_MAX_APOSTA,
    $NU_BILHETE_GRANDE,
    $NU_BILHETE_NEGRITO,
    $NU_BILHETE_SUBLINHADO
) {
    return getBold(' Configuração Geral: ') . getBold('Área: ') . $desc_area . '; ' .
        returnIfDifferent(getBold('Habilita Modalidade Seninha: '), converterSimNao($oldConfig['FLG_SENINHA']), converterSimNao($FLG_SENINHA)) .
        returnIfDifferent(getBold('Habilita Modalidade Quininha: '), converterSimNao($oldConfig['FLG_QUININHA']), converterSimNao($FLG_QUININHA)) .
        returnIfDifferent(getBold('Habilita Modalidade Lotinha: '), converterSimNao($oldConfig['FLG_LOTINHA']), converterSimNao($FLG_LOTINHA)) .
        returnIfDifferent(getBold('Habilita Modalidade Bicho: '), converterSimNao($oldConfig['FLG_BICHO']), converterSimNao($FLG_BICHO)) .
        returnIfDifferent(getBold('Habilita Modalidade 2 pra 500: '), converterSimNao($oldConfig['FLG_2PRA500']), converterSimNao($FLG_2PRA500)) .
        returnIfDifferent(getBold('Habilita Modalidade Rifa: '), converterSimNao($oldConfig['FLG_RIFA']), converterSimNao($FLG_RIFA)) .
        returnIfDifferent(getBold('Número Grande: '), converterSimNao($oldConfig['NU_BILHETE_GRANDE']), converterSimNao($NU_BILHETE_GRANDE)) .
        returnIfDifferent(getBold('Número em Negrito: '), converterSimNao($oldConfig['NU_BILHETE_NEGRITO']), converterSimNao($NU_BILHETE_NEGRITO)) .
        returnIfDifferent(getBold('Número Sublinhado: '), converterSimNao($oldConfig['NU_BILHETE_SUBLINHADO']), converterSimNao($NU_BILHETE_SUBLINHADO)) .
        returnIfDifferent(getBold('Minutos Cancelamento: '), $oldConfig['MINUTOS_CANCELAMENTO'], $MINUTOS_CANCELAMENTO) .
        returnIfDifferent(getBold('Valor Mínimo de Aposta: '), converterValorReal($oldConfig['VALOR_MIN_APOSTA']), converterValorReal($VALOR_MIN_APOSTA)) .
        returnIfDifferent(getBold('Valor Máximo de Aposta: '), converterValorReal($oldConfig['VALOR_MAX_APOSTA']), converterValorReal($VALOR_MAX_APOSTA)) . '.';
}

function descreverEdicaoConfiguracaoMultijogos(
    $oldConfig,
    $desc_area,
    $VALOR_CARTAO_1,
    $VALOR_CARTAO_2,
    $VALOR_CARTAO_3,
    $VALOR_CARTAO_4,
    $VALOR_CARTAO_5,
    $VALOR_MINIMO,
    $QTD_NUMEROS_SENINHA,
    $QTD_NUMEROS_QUININHA,
    $QTD_NUMEROS_LOTINHA,
    $TIPOS_JOGOS_BICHO,
    $CONCURSOS_MARCADOS
) {
    $head = getBold('Configuração Multijogos: ') . getBold('Área: ') . $desc_area . '; ';
    $result = $head .
        returnIfDifferent(getBold('Valor do 1º Cartão: '), $oldConfig['VALOR_CARTAO_1'], $VALOR_CARTAO_1) .
        returnIfDifferent(getBold('Valor do 2º Cartão: '), $oldConfig['VALOR_CARTAO_2'], $VALOR_CARTAO_2) .
        returnIfDifferent(getBold('Valor do 3º Cartão: '), $oldConfig['VALOR_CARTAO_3'], $VALOR_CARTAO_3) .
        returnIfDifferent(getBold('Valor do 4º Cartão: '), $oldConfig['VALOR_CARTAO_4'], $VALOR_CARTAO_4) .
        returnIfDifferent(getBold('Valor do 5º Cartão: '), $oldConfig['VALOR_CARTAO_5'], $VALOR_CARTAO_5) .
        returnIfDifferent(getBold('Qtd Números Seninha: '), $oldConfig['QTD_NUMEROS_SENINHA'], $QTD_NUMEROS_SENINHA) .
        returnIfDifferent(getBold('Qtd Números Quininha: '), $oldConfig['QTD_NUMEROS_QUININHA'], $QTD_NUMEROS_QUININHA) .
        returnIfDifferent(getBold('Qtd Números Lotinha: '), $oldConfig['QTD_NUMEROS_LOTINHA'], $QTD_NUMEROS_LOTINHA) .
        returnIfDifferent(getBold('Tipos de Jogo do Bicho: '), $oldConfig['TIPOS_JOGOS_BICHO'], $TIPOS_JOGOS_BICHO) .
        returnIfDifferent(getBold('Valor Mín. por Aposta: '), converterValorReal($oldConfig['VALOR_MINIMO_APOSTA']), converterValorReal($VALOR_MINIMO)) .
        returnIfDifferent(getBold('Concurso/Extação: '), $oldConfig['CONCURSOS_MARCADOS'], $CONCURSOS_MARCADOS);
    return $result;
}

function descreverCriacaoConfiguracaoMultijogos(
    $desc_area,
    $VALOR_CARTAO_1,
    $VALOR_CARTAO_2,
    $VALOR_CARTAO_3,
    $VALOR_CARTAO_4,
    $VALOR_CARTAO_5,
    $VALOR_MINIMO,
    $QTD_NUMEROS_SENINHA,
    $QTD_NUMEROS_QUININHA,
    $QTD_NUMEROS_LOTINHA,
    $TIPOS_JOGOS_BICHO,
    $CONCURSOS_MARCADOS
) {
    $head = getBold('Configuração Multijogos: ') . getBold('Área: ') . $desc_area . '; ';
    $result = $head .
        (!is_null($VALOR_CARTAO_1) ? getBold('Valor do 1º Cartão: ') . $VALOR_CARTAO_1 . '; ' : '') .
        (!is_null($VALOR_CARTAO_2) ? getBold('Valor do 2º Cartão: ') . $VALOR_CARTAO_2 . '; ' : '') .
        (!is_null($VALOR_CARTAO_3) ? getBold('Valor do 3º Cartão: ') . $VALOR_CARTAO_3 . '; ' : '') .
        (!is_null($VALOR_CARTAO_4) ? getBold('Valor do 4º Cartão: ') . $VALOR_CARTAO_4 . '; ' : '') .
        (!is_null($VALOR_CARTAO_5) ? getBold('Valor do 5º Cartão: ') . $VALOR_CARTAO_5 . '; ' : '') .
        (!is_null($QTD_NUMEROS_SENINHA) ? getBold('Qtd Números Seninha: ') . $QTD_NUMEROS_SENINHA . '; ' : '') .
        (!is_null($QTD_NUMEROS_QUININHA) ? getBold('Qtd Números Quininha: ') . $QTD_NUMEROS_QUININHA . '; ' : '') .
        (!is_null($QTD_NUMEROS_LOTINHA) ? getBold('Qtd Números Lotinha: ') . $QTD_NUMEROS_LOTINHA . '; ' : '') .
        (!is_null($TIPOS_JOGOS_BICHO) ? getBold('Tipos de Jogo do Bicho: ') . $TIPOS_JOGOS_BICHO . '; ' : '') .
        (!is_null($VALOR_MINIMO) ? getBold('Valor Mín. por Aposta: ') . converterValorReal($VALOR_MINIMO) . '; ' : '') .
        (!is_null($CONCURSOS_MARCADOS) ? getBold('Concurso/Extação: ') . $CONCURSOS_MARCADOS . '; ' : '');
    return $result;
}

function descreverEdicaoConfiguracaoSeninha(
    $oldConfig,
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_14,
    $PREMIO_15,
    $PREMIO_16,
    $PREMIO_17,
    $PREMIO_18,
    $PREMIO_19,
    $PREMIO_20,
    $PREMIO_25,
    $PREMIO_30,
    $PREMIO_35,
    $PREMIO_40,
    $HABILITA_SORTE,
    $SENA_SORTE,
    $QUINA_SORTE,
    $QUADRA_SORTE,
    $MSG_BILHETE
) {
    $head = getBold('Configuração Seninha: ') . getBold('Área: ') . $desc_area . '; ';
    $result = $head .
        returnIfDifferent(getBold('Prêmio Máximo: '), converterValorReal($oldConfig['PREMIO_MAXIMO']), converterValorReal($PREMIO_MAXIMO)) .
        returnIfDifferent(getBold('Prêmio 14: '), converterValorReal($oldConfig['PREMIO_14']), converterValorReal($PREMIO_14)) .
        returnIfDifferent(getBold('Prêmio 15: '), converterValorReal($oldConfig['PREMIO_15']), converterValorReal($PREMIO_15)) .
        returnIfDifferent(getBold('Prêmio 16: '), converterValorReal($oldConfig['PREMIO_16']), converterValorReal($PREMIO_16)) .
        returnIfDifferent(getBold('Prêmio 17: '), converterValorReal($oldConfig['PREMIO_17']), converterValorReal($PREMIO_17)) .
        returnIfDifferent(getBold('Prêmio 18: '), converterValorReal($oldConfig['PREMIO_18']), converterValorReal($PREMIO_18)) .
        returnIfDifferent(getBold('Prêmio 19: '), converterValorReal($oldConfig['PREMIO_19']), converterValorReal($PREMIO_19)) .
        returnIfDifferent(getBold('Prêmio 20: '), converterValorReal($oldConfig['PREMIO_20']), converterValorReal($PREMIO_20)) .
        returnIfDifferent(getBold('Prêmio 25: '), converterValorReal($oldConfig['PREMIO_25']), converterValorReal($PREMIO_25)) .
        returnIfDifferent(getBold('Prêmio 30: '), converterValorReal($oldConfig['PREMIO_30']), converterValorReal($PREMIO_30)) .
        returnIfDifferent(getBold('Prêmio 35: '), converterValorReal($oldConfig['PREMIO_35']), converterValorReal($PREMIO_35)) .
        returnIfDifferent(getBold('Prêmio 40: '), converterValorReal($oldConfig['PREMIO_40']), converterValorReal($PREMIO_40)) .
        returnIfDifferent(getBold('Habilita Sorte: '), converterSimNao($oldConfig['FLG_SORTE']), converterSimNao($HABILITA_SORTE)) .
        ($HABILITA_SORTE == 'S' ?
            returnIfDifferent(getBold('Sena da Sorte: '), converterValorPercentual($oldConfig['SENA_SORTE']), converterValorPercentual($SENA_SORTE)) .
            returnIfDifferent(getBold('Quina da Sorte: '), converterValorPercentual($oldConfig['QUINA_SORTE']), converterValorPercentual($QUINA_SORTE)) .
            returnIfDifferent(getBold('Quadra da Sorte: '), converterValorPercentual($oldConfig['QUADRA_SORTE']), converterValorPercentual($QUADRA_SORTE))
            : '') .
        returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE);
    return $result;
}

function descreverCriacaoConfiguracaoSeninha(
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_14,
    $PREMIO_15,
    $PREMIO_16,
    $PREMIO_17,
    $PREMIO_18,
    $PREMIO_19,
    $PREMIO_20,
    $PREMIO_25,
    $PREMIO_30,
    $PREMIO_35,
    $PREMIO_40,
    $HABILITA_SORTE,
    $SENA_SORTE,
    $QUINA_SORTE,
    $QUADRA_SORTE,
    $MSG_BILHETE
) {
    $head = getBold('Configuração Seninha: ') . getBold('Área: ') . $desc_area . '; ';
    $result = $head .
        (!is_null($PREMIO_MAXIMO) ? getBold('Prêmio Máximo: ') . converterValorReal($PREMIO_MAXIMO) . '; ' : '') .
        (!is_null($PREMIO_14) ? getBold('Prêmio 14: ') . converterValorReal($PREMIO_14) . '; ' : '') .
        (!is_null($PREMIO_15) ? getBold('Prêmio 15: ') . converterValorReal($PREMIO_15) . '; ' : '') .
        (!is_null($PREMIO_16) ? getBold('Prêmio 16: ') . converterValorReal($PREMIO_16) . '; ' : '') .
        (!is_null($PREMIO_17) ? getBold('Prêmio 17: ') . converterValorReal($PREMIO_17) . '; ' : '') .
        (!is_null($PREMIO_18) ? getBold('Prêmio 18: ') . converterValorReal($PREMIO_18) . '; ' : '') .
        (!is_null($PREMIO_19) ? getBold('Prêmio 19: ') . converterValorReal($PREMIO_19) . '; ' : '') .
        (!is_null($PREMIO_20) ? getBold('Prêmio 20: ') . converterValorReal($PREMIO_20) . '; ' : '') .
        (!is_null($PREMIO_25) ? getBold('Prêmio 25: ') . converterValorReal($PREMIO_25) . '; ' : '') .
        (!is_null($PREMIO_30) ? getBold('Prêmio 30: ') . converterValorReal($PREMIO_30) . '; ' : '') .
        (!is_null($PREMIO_35) ? getBold('Prêmio 35: ') . converterValorReal($PREMIO_35) . '; ' : '') .
        (!is_null($PREMIO_40) ? getBold('Prêmio 40: ') . converterValorReal($PREMIO_40) . '; ' : '') .
        (!is_null($HABILITA_SORTE) ? getBold('Habilita Sorte: ') . converterSimNao($HABILITA_SORTE) . '; ' : '') .
        ($HABILITA_SORTE == 'S' ?
            (!is_null($SENA_SORTE) ? getBold('Sena da Sorte: ') . converterValorPercentual($SENA_SORTE) . '; ' : '') .
            (!is_null($QUINA_SORTE) ? getBold('Quina da Sorte: ') . converterValorPercentual($QUINA_SORTE) . '; ' : '') .
            (!is_null($QUADRA_SORTE) ? getBold('Quadra da Sorte: ') . converterValorPercentual($QUADRA_SORTE) . '; ' : '')
            : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . ';' : '');
    return $result;
}

function descreverEdicaoConfiguracaoSuperSena(
    $oldConfig,
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_SENA,
    $PREMIO_QUINA,
    $PREMIO_QUADRA,
    $MSG_BILHETE
) {
    $head = getBold('Configuração Super Sena: ') . getBold('Área: ') . $desc_area . '; ';
    $result = $head .
        returnIfDifferent(getBold('Prêmio Máximo: '), converterValorReal($oldConfig['PREMIO_MAXIMO']), converterValorReal($PREMIO_MAXIMO)) .
        returnIfDifferent(getBold('Prêmio Sena: '), converterValorReal($oldConfig['PREMIO_SENA']), converterValorReal($PREMIO_SENA)) .
        returnIfDifferent(getBold('Prêmio Quina: '), converterValorReal($oldConfig['PREMIO_QUINA']), converterValorReal($PREMIO_QUINA)) .
        returnIfDifferent(getBold('Prêmio Quadra: '), converterValorReal($oldConfig['PREMIO_QUADRA']), converterValorReal($PREMIO_QUADRA)) .
        returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE);
    return $result;
}

function descreverCriacaoConfiguracaoSuperSena(
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_SENA,
    $PREMIO_QUINA,
    $PREMIO_QUADRA,
    $MSG_BILHETE
) {
    $head = getBold('Configuração Super Sena: ') . getBold('Área: ') . $desc_area . '; ';
    $result = $head .
        (!is_null($PREMIO_MAXIMO) ? getBold('Prêmio Máximo: ') . converterValorReal($PREMIO_MAXIMO) . '; ' : '') .
        (!is_null($PREMIO_SENA) ? getBold('Prêmio Sena: ') . converterValorReal($PREMIO_SENA) . '; ' : '') .
        (!is_null($PREMIO_QUINA) ? getBold('Prêmio Quina: ') . converterValorReal($PREMIO_QUINA) . '; ' : '') .
        (!is_null($PREMIO_QUADRA) ? getBold('Prêmio Quadra: ') . converterValorReal($PREMIO_QUADRA) . '; ' : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . ';' : '');
    return $result;
}

function descreverEdicaoConfiguracaoQuininha(
    $oldConfig,
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_13,
    $PREMIO_14,
    $PREMIO_15,
    $PREMIO_16,
    $PREMIO_17,
    $PREMIO_18,
    $PREMIO_19,
    $PREMIO_20,
    $PREMIO_25,
    $PREMIO_30,
    $PREMIO_35,
    $PREMIO_40,
    $PREMIO_45,
    $MSG_BILHETE,
    $HABILITA_SORTE,
    $QUINA_SORTE,
    $QUADRA_SORTE,
    $TERNO_SORTE
) {
    return getBold('Configuração Quininha: ') . getBold('Área: ') . $desc_area . '; ' .
        returnIfDifferent(getBold('Prêmio Máximo: '), converterValorReal($oldConfig['PREMIO_MAXIMO']), converterValorReal($PREMIO_MAXIMO)) .
        returnIfDifferent(getBold('Prêmio 13: '), converterValorReal($oldConfig['PREMIO_13']), converterValorReal($PREMIO_13)) .
        returnIfDifferent(getBold('Prêmio 14: '), converterValorReal($oldConfig['PREMIO_14']), converterValorReal($PREMIO_14)) .
        returnIfDifferent(getBold('Prêmio 15: '), converterValorReal($oldConfig['PREMIO_15']), converterValorReal($PREMIO_15)) .
        returnIfDifferent(getBold('Prêmio 16: '), converterValorReal($oldConfig['PREMIO_16']), converterValorReal($PREMIO_16)) .
        returnIfDifferent(getBold('Prêmio 17: '), converterValorReal($oldConfig['PREMIO_17']), converterValorReal($PREMIO_17)) .
        returnIfDifferent(getBold('Prêmio 18: '), converterValorReal($oldConfig['PREMIO_18']), converterValorReal($PREMIO_18)) .
        returnIfDifferent(getBold('Prêmio 19: '), converterValorReal($oldConfig['PREMIO_19']), converterValorReal($PREMIO_19)) .
        returnIfDifferent(getBold('Prêmio 20: '), converterValorReal($oldConfig['PREMIO_20']), converterValorReal($PREMIO_20)) .
        returnIfDifferent(getBold('Prêmio 25: '), converterValorReal($oldConfig['PREMIO_25']), converterValorReal($PREMIO_25)) .
        returnIfDifferent(getBold('Prêmio 30: '), converterValorReal($oldConfig['PREMIO_30']), converterValorReal($PREMIO_30)) .
        returnIfDifferent(getBold('Prêmio 35: '), converterValorReal($oldConfig['PREMIO_35']), converterValorReal($PREMIO_35)) .
        returnIfDifferent(getBold('Prêmio 40: '), converterValorReal($oldConfig['PREMIO_40']), converterValorReal($PREMIO_40)) .
        returnIfDifferent(getBold('Prêmio 45: '), converterValorReal($oldConfig['PREMIO_45']), converterValorReal($PREMIO_45)) .
        returnIfDifferent(getBold('Habilita Sorte: '), converterSimNao($oldConfig['FLG_SORTE']), converterSimNao($HABILITA_SORTE)) .
        ($HABILITA_SORTE == 'S' ?
            returnIfDifferent(getBold('Quina da Sorte: '), converterValorPercentual($oldConfig['QUINA_SORTE']), converterValorPercentual($QUINA_SORTE)) .
            returnIfDifferent(getBold('Quadra da Sorte: '), converterValorPercentual($oldConfig['QUADRA_SORTE']), converterValorPercentual($QUADRA_SORTE)) .
            returnIfDifferent(getBold('Terno da Sorte: '), converterValorPercentual($oldConfig['TERNO_SORTE']), converterValorPercentual($TERNO_SORTE))
            : '') .
        returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE) . '.';
}

function descreverCriacaoConfiguracaoQuininha(
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_13,
    $PREMIO_14,
    $PREMIO_15,
    $PREMIO_16,
    $PREMIO_17,
    $PREMIO_18,
    $PREMIO_19,
    $PREMIO_20,
    $PREMIO_25,
    $PREMIO_30,
    $PREMIO_35,
    $PREMIO_40,
    $PREMIO_45,
    $MSG_BILHETE,
    $HABILITA_SORTE,
    $QUINA_SORTE,
    $QUADRA_SORTE,
    $TERNO_SORTE
) {
    return getBold('Configuração Quininha: ') . getBold('Área: ') . $desc_area . '; ' .
        (!is_null($PREMIO_MAXIMO) ? getBold('Prêmio Máximo: ') . converterValorReal($PREMIO_MAXIMO) . '; ' : '') .
        (!is_null($PREMIO_13) ? getBold('Prêmio 13: ') . converterValorReal($PREMIO_13) . '; ' : '') .
        (!is_null($PREMIO_14) ? getBold('Prêmio 14: ') . converterValorReal($PREMIO_14) . '; ' : '') .
        (!is_null($PREMIO_15) ? getBold('Prêmio 15: ') . converterValorReal($PREMIO_15) . '; ' : '') .
        (!is_null($PREMIO_16) ? getBold('Prêmio 16: ') . converterValorReal($PREMIO_16) . '; ' : '') .
        (!is_null($PREMIO_17) ? getBold('Prêmio 17: ') . converterValorReal($PREMIO_17) . '; ' : '') .
        (!is_null($PREMIO_18) ? getBold('Prêmio 18: ') . converterValorReal($PREMIO_18) . '; ' : '') .
        (!is_null($PREMIO_19) ? getBold('Prêmio 19: ') . converterValorReal($PREMIO_19) . '; ' : '') .
        (!is_null($PREMIO_20) ? getBold('Prêmio 20: ') . converterValorReal($PREMIO_20) . '; ' : '') .
        (!is_null($PREMIO_25) ? getBold('Prêmio 25: ') . converterValorReal($PREMIO_25) . '; ' : '') .
        (!is_null($PREMIO_30) ? getBold('Prêmio 30: ') . converterValorReal($PREMIO_30) . '; ' : '') .
        (!is_null($PREMIO_35) ? getBold('Prêmio 35: ') . converterValorReal($PREMIO_35) . '; ' : '') .
        (!is_null($PREMIO_40) ? getBold('Prêmio 40: ') . converterValorReal($PREMIO_40) . '; ' : '') .
        (!is_null($PREMIO_45) ? getBold('Prêmio 45: ') . converterValorReal($PREMIO_45) . '; ' : '') .
        (!is_null($HABILITA_SORTE) ? getBold('Habilita Sorte: ') . converterSimNao($HABILITA_SORTE) . '; ' : '') .
        ($HABILITA_SORTE == 'S' ?
            (!is_null($QUINA_SORTE) ? getBold('Quina da Sorte: ') . converterValorPercentual($QUINA_SORTE) . '; ' : '') .
            (!is_null($QUADRA_SORTE) ? getBold('Quadra da Sorte: ') . converterValorPercentual($QUADRA_SORTE) . '; ' : '') .
            (!is_null($TERNO_SORTE) ? getBold('Terno da Sorte: ') . converterValorPercentual($TERNO_SORTE) . '; ' : '')
            : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . '.' : '');
}

function descreverEdicaoConfiguracaoLotinha(
    $oldConfig,
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_16,
    $PREMIO_17,
    $PREMIO_18,
    $PREMIO_19,
    $PREMIO_20,
    $PREMIO_21,
    $PREMIO_22,
    $MSG_BILHETE
) {
    return getBold('Configuração Lotinha: ') . getBold('Área: ') . $desc_area . '; ' .
        returnIfDifferent(getBold('Prêmio Máximo: '), converterValorReal($oldConfig['PREMIO_MAXIMO']), converterValorReal($PREMIO_MAXIMO)) .
        returnIfDifferent(getBold('Prêmio 16: '), converterValorReal($oldConfig['PREMIO_16']), converterValorReal($PREMIO_16)) .
        returnIfDifferent(getBold('Prêmio 17: '), converterValorReal($oldConfig['PREMIO_17']), converterValorReal($PREMIO_17)) .
        returnIfDifferent(getBold('Prêmio 18: '), converterValorReal($oldConfig['PREMIO_18']), converterValorReal($PREMIO_18)) .
        returnIfDifferent(getBold('Prêmio 19: '), converterValorReal($oldConfig['PREMIO_19']), converterValorReal($PREMIO_19)) .
        returnIfDifferent(getBold('Prêmio 20: '), converterValorReal($oldConfig['PREMIO_20']), converterValorReal($PREMIO_20)) .
        returnIfDifferent(getBold('Prêmio 21: '), converterValorReal($oldConfig['PREMIO_21']), converterValorReal($PREMIO_21)) .
        returnIfDifferent(getBold('Prêmio 22: '), converterValorReal($oldConfig['PREMIO_22']), converterValorReal($PREMIO_22)) .
        returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE) . '.';
}

function descreverCriacaoConfiguracaoLotinha(
    $desc_area,
    $PREMIO_MAXIMO,
    $PREMIO_16,
    $PREMIO_17,
    $PREMIO_18,
    $PREMIO_19,
    $PREMIO_20,
    $PREMIO_21,
    $PREMIO_22,
    $MSG_BILHETE
) {
    return getBold('Configuração Lotinha: ') . getBold('Área: ') . $desc_area . '; ' .
        (!is_null($PREMIO_MAXIMO) ? getBold('Prêmio Máximo: ') . converterValorReal($PREMIO_MAXIMO) . '; ' : '') .
        (!is_null($PREMIO_16) ? getBold('Prêmio 16: ') . converterValorReal($PREMIO_16) . '; ' : '') .
        (!is_null($PREMIO_17) ? getBold('Prêmio 17: ') . converterValorReal($PREMIO_17) . '; ' : '') .
        (!is_null($PREMIO_18) ? getBold('Prêmio 18: ') . converterValorReal($PREMIO_18) . '; ' : '') .
        (!is_null($PREMIO_19) ? getBold('Prêmio 19: ') . converterValorReal($PREMIO_19) . '; ' : '') .
        (!is_null($PREMIO_20) ? getBold('Prêmio 20: ') . converterValorReal($PREMIO_20) . '; ' : '') .
        (!is_null($PREMIO_21) ? getBold('Prêmio 21: ') . converterValorReal($PREMIO_21) . '; ' : '') .
        (!is_null($PREMIO_22) ? getBold('Prêmio 22: ') . converterValorReal($PREMIO_22) . '; ' : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . '.' : '');
}

function descreverEdicaoConfiguracaoBicho(
    $oldConfig,
    $desc_area,
    $PREMIO_MILHAR_CENTENA,
    $PREMIO_MILHAR_SECA,
    $PREMIO_MILHAR_INVERTIDA,
    $PREMIO_CENTENA,
    $PREMIO_CENTENA_INVERTIDA,
    $PREMIO_GRUPO,
    $PREMIO_DUQUE_GRUPO,
    $PREMIO_DUQUE_GRUPO6,
    $PREMIO_TERNO_GRUPO,
    $PREMIO_TERNO_GRUPO10,
    $PREMIO_TERNO_GRUPO6,
    $PREMIO_TERNO_SEQUENCIA,
    $PREMIO_TERNO_SOMA,
    $PREMIO_TERNO_ESPECIAL,
    $PREMIO_QUINA_GRUPO,
    $PREMIO_DEZENA,
    $PREMIO_DUQUE_DEZENA,
    $PREMIO_DUQUE_DEZENA6,
    $PREMIO_TERNO_DEZENA,
    $PREMIO_TERNO_DEZENA10,
    $PREMIO_TERNO_DEZENA6,
    $PREMIO_PASSE_SECO,
    $PREMIO_PASSE_COMBINADO,
    $MSG_BILHETE,
    $USA_MILHAR_BRINDE,
    $VALOR_LIBERAR_MILHAR_BRINDE,
    $VALOR_RETORNO_MILHAR_BRINDE
) {
    return getBold('Configuração Jogo do Bicho: ') . getBold('Área: ') . $desc_area . '; ' .
        returnIfDifferent(getBold('Prêmio Milhar e Centena: '), converterValorReal($oldConfig['PREMIO_MILHAR_CENTENA']), converterValorReal($PREMIO_MILHAR_CENTENA)) .
        returnIfDifferent(getBold('Prêmio Milhar Seca: '), converterValorReal($oldConfig['PREMIO_MILHAR_SECA']), converterValorReal($PREMIO_MILHAR_SECA)) .
        returnIfDifferent(getBold('Prêmio Milhar Invertida: '), converterValorReal($oldConfig['PREMIO_MILHAR_INVERTIDA']), converterValorReal($PREMIO_MILHAR_INVERTIDA)) .
        returnIfDifferent(getBold('Prêmio Centena: '), converterValorReal($oldConfig['PREMIO_CENTENA']), converterValorReal($PREMIO_CENTENA)) .
        returnIfDifferent(getBold('Prêmio Centena Invertida: '), converterValorReal($oldConfig['PREMIO_CENTENA_INVERTIDA']), converterValorReal($PREMIO_CENTENA_INVERTIDA)) .
        returnIfDifferent(getBold('Prêmio Grupo: '), converterValorReal($oldConfig['PREMIO_GRUPO']), converterValorReal($PREMIO_GRUPO)) .
        returnIfDifferent(getBold('Prêmio Duque de Grupo 1-5: '), converterValorReal($oldConfig['PREMIO_DUQUE_GRUPO']), converterValorReal($PREMIO_DUQUE_GRUPO)) .
        returnIfDifferent(getBold('Prêmio Duque de Grupo 6-10: '), converterValorReal($oldConfig['PREMIO_DUQUE_GRUPO6']), converterValorReal($PREMIO_DUQUE_GRUPO6)) .
        returnIfDifferent(getBold('Prêmio Terno de Grupo 1-5: '), converterValorReal($oldConfig['PREMIO_TERNO_GRUPO']), converterValorReal($PREMIO_TERNO_GRUPO)) .
        returnIfDifferent(getBold('Prêmio Terno de Grupo 1-10: '), converterValorReal($oldConfig['PREMIO_TERNO_GRUPO10']), converterValorReal($PREMIO_TERNO_GRUPO10)) .
        returnIfDifferent(getBold('Prêmio Terno de Grupo 6-10: '), converterValorReal($oldConfig['PREMIO_TERNO_GRUPO6']), converterValorReal($PREMIO_TERNO_GRUPO6)) .
        returnIfDifferent(getBold('Prêmio Terno de Sequência: '), converterValorReal($oldConfig['PREMIO_TERNO_SEQUENCIA']), converterValorReal($PREMIO_TERNO_SEQUENCIA)) .
        returnIfDifferent(getBold('Prêmio Terno de Soma: '), converterValorReal($oldConfig['PREMIO_TERNO_SOMA']), converterValorReal($PREMIO_TERNO_SOMA)) .
        returnIfDifferent(getBold('Prêmio Terno Especial: '), converterValorReal($oldConfig['PREMIO_TERNO_ESPECIAL']), converterValorReal($PREMIO_TERNO_ESPECIAL)) .
        returnIfDifferent(getBold('Prêmio Quina de Grupo: '), converterValorReal($oldConfig['PREMIO_QUINA_GRUPO']), converterValorReal($PREMIO_QUINA_GRUPO)) .
        returnIfDifferent(getBold('Prêmio Dezena: '), converterValorReal($oldConfig['PREMIO_DEZENA']), converterValorReal($PREMIO_DEZENA)) .
        returnIfDifferent(getBold('Prêmio Duque de Dezena 1-5: '), converterValorReal($oldConfig['PREMIO_DUQUE_DEZENA']), converterValorReal($PREMIO_DUQUE_DEZENA)) .
        returnIfDifferent(getBold('Prêmio Duque de Dezena 6-10: '), converterValorReal($oldConfig['PREMIO_DUQUE_DEZENA6']), converterValorReal($PREMIO_DUQUE_DEZENA6)) .
        returnIfDifferent(getBold('Prêmio Terno de Dezena 1-5: '), converterValorReal($oldConfig['PREMIO_TERNO_DEZENA']), converterValorReal($PREMIO_TERNO_DEZENA)) .
        returnIfDifferent(getBold('Prêmio Terno de Dezena 1-10: '), converterValorReal($oldConfig['PREMIO_TERNO_DEZENA10']), converterValorReal($PREMIO_TERNO_DEZENA10)) .
        returnIfDifferent(getBold('Prêmio Terno de Dezena 6-10: '), converterValorReal($oldConfig['PREMIO_TERNO_DEZENA6']), converterValorReal($PREMIO_TERNO_DEZENA6)) .
        returnIfDifferent(getBold('Prêmio Passe Seco: '), converterValorReal($oldConfig['PREMIO_PASSE_SECO']), converterValorReal($PREMIO_PASSE_SECO)) .
        returnIfDifferent(getBold('Prêmio Passe Combinado: '), converterValorReal($oldConfig['PREMIO_PASSE_COMBINADO']), converterValorReal($PREMIO_PASSE_COMBINADO)) .
        returnIfDifferent(getBold('Usar Milhar Brinde: '), converterSimNao($oldConfig['USA_MILHAR_BRINDE']), converterSimNao($USA_MILHAR_BRINDE)) .
        ($USA_MILHAR_BRINDE == 'S' ?
            returnIfDifferent(getBold('Valor para liberar Milhar Brinde: '), converterValorReal($oldConfig['VALOR_LIBERAR_MILHAR_BRINDE']), converterValorReal($VALOR_LIBERAR_MILHAR_BRINDE)) .
            returnIfDifferent(getBold('Valor retorno Milhar Brinde: '), converterValorReal($oldConfig['VALOR_RETORNO_MILHAR_BRINDE']), converterValorReal($VALOR_RETORNO_MILHAR_BRINDE))
            : '') .
        returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE) . '.';
}

function descreverCriacaoConfiguracaoBicho(
    $desc_area,
    $PREMIO_MILHAR_CENTENA,
    $PREMIO_MILHAR_SECA,
    $PREMIO_MILHAR_INVERTIDA,
    $PREMIO_CENTENA,
    $PREMIO_CENTENA_INVERTIDA,
    $PREMIO_GRUPO,
    $PREMIO_DUQUE_GRUPO,
    $PREMIO_DUQUE_GRUPO6,
    $PREMIO_TERNO_GRUPO,
    $PREMIO_TERNO_GRUPO10,
    $PREMIO_TERNO_GRUPO6,
    $PREMIO_TERNO_SEQUENCIA,
    $PREMIO_TERNO_SOMA,
    $PREMIO_TERNO_ESPECIAL,
    $PREMIO_QUINA_GRUPO,
    $PREMIO_DEZENA,
    $PREMIO_DUQUE_DEZENA,
    $PREMIO_DUQUE_DEZENA6,
    $PREMIO_TERNO_DEZENA,
    $PREMIO_TERNO_DEZENA10,
    $PREMIO_TERNO_DEZENA6,
    $PREMIO_PASSE_SECO,
    $PREMIO_PASSE_COMBINADO,
    $MSG_BILHETE,
    $USA_MILHAR_BRINDE,
    $VALOR_LIBERAR_MILHAR_BRINDE,
    $VALOR_RETORNO_MILHAR_BRINDE
) {
    return getBold('Configuração Jogo do Bicho: ') . getBold('Área: ') . $desc_area . '; ' .
        (!is_null($PREMIO_MILHAR_CENTENA) ? getBold('Prêmio Milhar e Centena: ') . converterValorReal($PREMIO_MILHAR_CENTENA) . '; ' : '') .
        (!is_null($PREMIO_MILHAR_SECA) ? getBold('Prêmio Milhar Seca: ') . converterValorReal($PREMIO_MILHAR_SECA) . '; ' : '') .
        (!is_null($PREMIO_MILHAR_INVERTIDA) ? getBold('Prêmio Milhar Invertida: ') . converterValorReal($PREMIO_MILHAR_INVERTIDA) . '; ' : '') .
        (!is_null($PREMIO_CENTENA) ? getBold('Prêmio Centena: ') . converterValorReal($PREMIO_CENTENA) . '; ' : '') .
        (!is_null($PREMIO_CENTENA_INVERTIDA) ? getBold('Prêmio Centena Invertida: ') . converterValorReal($PREMIO_CENTENA_INVERTIDA) . '; ' : '') .
        (!is_null($PREMIO_GRUPO) ? getBold('Prêmio Grupo: ') . converterValorReal($PREMIO_GRUPO) . '; ' : '') .
        (!is_null($PREMIO_DUQUE_GRUPO) ? getBold('Prêmio Duque de Grupo 1-5: ') . converterValorReal($PREMIO_DUQUE_GRUPO) . '; ' : '') .
        (!is_null($PREMIO_DUQUE_GRUPO6) ? getBold('Prêmio Duque de Grupo 6-10: ') . converterValorReal($PREMIO_DUQUE_GRUPO6) . '; ' : '') .
        (!is_null($PREMIO_TERNO_GRUPO) ? getBold('Prêmio Terno de Grupo 1-5: ') . converterValorReal($PREMIO_TERNO_GRUPO) . '; ' : '') .
        (!is_null($PREMIO_TERNO_GRUPO10) ? getBold('Prêmio Terno de Grupo 1-10: ') . converterValorReal($PREMIO_TERNO_GRUPO10) . '; ' : '') .
        (!is_null($PREMIO_TERNO_GRUPO6) ? getBold('Prêmio Terno de Grupo 6-10: ') . converterValorReal($PREMIO_TERNO_GRUPO6) . '; ' : '') .
        (!is_null($PREMIO_TERNO_SEQUENCIA) ? getBold('Prêmio Terno de Sequência: ') . converterValorReal($PREMIO_TERNO_SEQUENCIA) . '; ' : '') .
        (!is_null($PREMIO_TERNO_SOMA) ? getBold('Prêmio Terno de Soma: ') . converterValorReal($PREMIO_TERNO_SOMA) . '; ' : '') .
        (!is_null($PREMIO_TERNO_ESPECIAL) ? getBold('Prêmio Terno Especial: ') . converterValorReal($PREMIO_TERNO_ESPECIAL) . '; ' : '') .
        (!is_null($PREMIO_QUINA_GRUPO) ? getBold('Prêmio Quina de Grupo: ') . converterValorReal($PREMIO_QUINA_GRUPO) . '; ' : '') .
        (!is_null($PREMIO_DEZENA) ? getBold('Prêmio Dezena: ') . converterValorReal($PREMIO_DEZENA) . '; ' : '') .
        (!is_null($PREMIO_DUQUE_DEZENA) ? getBold('Prêmio Duque de Dezena 1-5: ') . converterValorReal($PREMIO_DUQUE_DEZENA) . '; ' : '') .
        (!is_null($PREMIO_DUQUE_DEZENA6) ? getBold('Prêmio Duque de Dezena 6-10: ') . converterValorReal($PREMIO_DUQUE_DEZENA6) . '; ' : '') .
        (!is_null($PREMIO_TERNO_DEZENA) ? getBold('Prêmio Terno de Dezena 1-5: ') . converterValorReal($PREMIO_TERNO_DEZENA) . '; ' : '') .
        (!is_null($PREMIO_TERNO_DEZENA10) ? getBold('Prêmio Terno de Dezena 1-10: ') . converterValorReal($PREMIO_TERNO_DEZENA10) . '; ' : '') .
        (!is_null($PREMIO_TERNO_DEZENA6) ? getBold('Prêmio Terno de Dezena 6-10: ') . converterValorReal($PREMIO_TERNO_DEZENA6) . '; ' : '') .
        (!is_null($PREMIO_PASSE_SECO) ? getBold('Prêmio Passe Seco: ') . converterValorReal($PREMIO_PASSE_SECO) . '; ' : '') .
        (!is_null($PREMIO_PASSE_COMBINADO) ? getBold('Prêmio Passe Combinado: ') . converterValorReal($PREMIO_PASSE_COMBINADO) . '; ' : '') .
        (!is_null($USA_MILHAR_BRINDE) ? getBold('Usar Milhar Brinde: ') . converterSimNao($USA_MILHAR_BRINDE) . '; ' : '') .
        ($USA_MILHAR_BRINDE == 'S' ?
            (!is_null($VALOR_LIBERAR_MILHAR_BRINDE) ? getBold('Valor para liberar Milhar Brinde: ') . converterValorReal($VALOR_LIBERAR_MILHAR_BRINDE) . '; ' : '') .
            (!is_null($VALOR_RETORNO_MILHAR_BRINDE) ? getBold('Valor retorno Milhar Brinde: ') . converterValorReal($VALOR_RETORNO_MILHAR_BRINDE) . '; ' : '')
            : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . '.' : '');
}

function descreverTiposJogosBichos($adicionados, $removidos) {
    $auditoria = '';
    if (!empty($adicionados)) {
        $auditoria = $auditoria . getBold('Tipos de jogos adicionados: ') . $adicionados . '; ';
    }
    if (!empty($removidos)) {
        $auditoria = $auditoria . getBold('Tipos de jogos removidos: ') . $removidos;
    }
    return $auditoria;
}

function descreverEdicaoConfiguracao2pra500($oldConfig, $desc_area, $VALOR_ACUMULADO, $MSG_BILHETE, $VALOR_APOSTA)
{
    return getBold('Configuração 2 pra 500: ') . getBold('Área: ') . $desc_area . '; ' .
        returnIfDifferent(getBold('Valor acumulado: '), $oldConfig['VALOR_ACUMULADO'], converterValorReal($VALOR_ACUMULADO)) .
        returnIfDifferent(getBold('Valor aposta: '), $oldConfig['VALOR_APOSTA'], converterValorReal($VALOR_APOSTA)) .
        returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE);
}

function descreverCriacaoConfiguracao2pra500($desc_area, $VALOR_ACUMULADO, $MSG_BILHETE, $VALOR_APOSTA)
{
    return getBold('Área: ') . $desc_area . '; ' .
        (!is_null($VALOR_ACUMULADO) ? getBold('Valor acumulado: ') . converterValorReal($VALOR_ACUMULADO) . '; ' : '') .
        (!is_null($VALOR_APOSTA) ? getBold('Valor aposta: ') . converterValorReal($VALOR_APOSTA) . '; ' : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . '.' : '');
}

function descreverEdicaoConfiguracaoRifa($oldConfig, $desc_area, $QTD_RIFAS, $MSG_BILHETE)
{
    return getBold('Configuração Rifa: ') . getBold('Área: ') . $desc_area . '; ' .
    returnIfDifferent(getBold('Quantidade de Rifas: '), $oldConfig['QTD_RIFAS'], $QTD_RIFAS) .    
    returnIfDifferent(getBold('Mensagem Bilhete: '), $oldConfig['MSG_BILHETE'], $MSG_BILHETE);
}

function descreverCriacaoConfiguracaoRifa($desc_area, $QTD_RIFAS, $MSG_BILHETE)
{
    return getBold('Área: ') . $desc_area . '; ' .
        (!is_null($QTD_RIFAS) ? getBold('Quantidade de Rifas: ') . $QTD_RIFAS . '; ' : '') .
        (!is_null($MSG_BILHETE) ? getBold('Mensagem Bilhete: ') . $MSG_BILHETE . '.' : '');
}

function descreverLimiteBicho(
    $codigo,
    $desc_extracao,
    $milhar,
    $centena,
    $grupo,
    $duque_grupo,
    $terno_grupo,
    $quina_grupo,
    $dezena,
    $duque_dezena,
    $terno_dezena
) {
    return ($codigo != 0 ? getBold('Código: ') . '#' . $codigo . '; ' : '') .
        (empty($desc_extracao) ? getBold('Extração: ') . $desc_extracao . '; ' : '') .
        getBold('Milhar: ') . converterValorReal($milhar) .
        '; ' . getBold('Centena: ') . converterValorReal($centena) .
        '; ' . getBold('Grupo: ') . converterValorReal($grupo) .
        '; ' . getBold('Duque Grupo: ') . converterValorReal($duque_grupo) .
        '; ' . getBold('Terno Grupo: ') . converterValorReal($terno_grupo) .
        '; ' . getBold('Quina Grupo: ') . converterValorReal($quina_grupo) .
        '; ' . getBold('Dezena: ') . converterValorReal($dezena) .
        '; ' . getBold('Duque Dezena: ') . converterValorReal($duque_dezena) .
        '; ' . getBold('Terno Dezena: ') . converterValorReal($terno_dezena) . '.';
}

function descreverEdicaoLimiteBicho(
    $oldLimite,
    $codigo,
    $milhar,
    $centena,
    $grupo,
    $duque_grupo,
    $terno_grupo,
    $quina_grupo,
    $dezena,
    $duque_dezena,
    $terno_dezena
) {
    return ($codigo != 0 ? getBold('Código: ') . '#' . $codigo . '; ' : '') .
        returnIfDifferent(getBold('Milhar: '), converterValorReal($oldLimite['MILHAR']), converterValorReal($milhar)) .
        returnIfDifferent(getBold('Centena: '), converterValorReal($oldLimite['CENTENA']), converterValorReal($centena)) .
        returnIfDifferent(getBold('Grupo: '), converterValorReal($oldLimite['GRUPO']), converterValorReal($grupo)) .
        returnIfDifferent(getBold('Duque Grupo: '), converterValorReal($oldLimite['DUQUE_GRUPO']), converterValorReal($duque_grupo)) .
        returnIfDifferent(getBold('Terno Grupo: '), converterValorReal($oldLimite['TERNO_GRUPO']), converterValorReal($terno_grupo)) .
        returnIfDifferent(getBold('Quina Grupo: '), converterValorReal($oldLimite['QUINA_GRUPO']), converterValorReal($quina_grupo)) .
        returnIfDifferent(getBold('Dezena: '), converterValorReal($oldLimite['DEZENA']), converterValorReal($dezena)) .
        returnIfDifferent(getBold('Duque Dezena: '), converterValorReal($oldLimite['DUQUE_DEZENA']), converterValorReal($duque_dezena)) .
        returnIfDifferent(getBold('Terno Dezena: '), converterValorReal($oldLimite['TERNO_DEZENA']), converterValorReal($terno_dezena));
}

function descreverExclusaoLimiteBicho($codigo)
{
    return 'Configuração #' . $codigo . ' excluída.';
}

function descreverCriacaoJogo($data, $tipo_jogo, $concurso, $hora_extracao, $desc_hora, $descricao, $valor)
{
    return getBold('Tipo do jogo: ') . converterTipoJogo($tipo_jogo) .
        '; ' . getBold('Data: ') . converterData($data, $format = 'd/m/Y') .
        (
            $tipo_jogo == 'B' || $tipo_jogo == '2' ?
            ('; ' . getBold('Hora Extração: ') . $hora_extracao .
             '; ' . getBold('Descrição: ') . $desc_hora
            )
            : 
            $tipo_jogo == 'R' ? 
            ('; ' . getBold('Descrição: ') . $descricao .
             '; ' . getBold('Valor: ') . $valor
            )
            :
            ('; ' . getBold('Concurso: ') . $concurso)
        ) . '.';
}

function descreverEdicaoJogo(
    $oldJogo, $cod_jogo, $data, $status, 
    $numero1, $numero2, $numero3, $numero4, $numero5, 
    $numero6, $numero7, $numero8, $numero9, $numero10,
    $numero11, $numero12, $numero13, $numero14, $numero15)
{
    return getBold('Jogo ') . '#' . $cod_jogo . ' ' .
        returnIfDifferent(getBold('Data: '), converterData($oldJogo['DATA_JOGO'], $format = 'd/m/Y'), converterData($data, $format = 'd/m/Y')) .
        returnIfDifferent(getBold('Status: '), converterStatusJogo($oldJogo['TP_STATUS']), converterStatusJogo($status)) .
        returnIfDifferent(getBold('Número 1: '), $oldJogo['NUMERO_1'], $numero1) .
        returnIfDifferent(getBold('Número 2: '), $oldJogo['NUMERO_2'], $numero2) .
        returnIfDifferent(getBold('Número 3: '), $oldJogo['NUMERO_3'], $numero3) .
        returnIfDifferent(getBold('Número 4: '), $oldJogo['NUMERO_4'], $numero4) .
        returnIfDifferent(getBold('Número 5: '), $oldJogo['NUMERO_5'], $numero5) .
        returnIfDifferent(getBold('Número 6: '), $oldJogo['NUMERO_6'], $numero6) .
        returnIfDifferent(getBold('Número 7: '), $oldJogo['NUMERO_7'], $numero7) .
        returnIfDifferent(getBold('Número 8: '), $oldJogo['NUMERO_8'], $numero8) .
        returnIfDifferent(getBold('Número 9: '), $oldJogo['NUMERO_9'], $numero9) .
        returnIfDifferent(getBold('Número 10: '), $oldJogo['NUMERO_10'], $numero10) .
        returnIfDifferent(getBold('Número 11: '), $oldJogo['NUMERO_11'], $numero11) .
        returnIfDifferent(getBold('Número 12: '), $oldJogo['NUMERO_12'], $numero12) .
        returnIfDifferent(getBold('Número 13: '), $oldJogo['NUMERO_13'], $numero13) .
        returnIfDifferent(getBold('Número 14: '), $oldJogo['NUMERO_14'], $numero14) .
        returnIfDifferent(getBold('Número 15: '), $oldJogo['NUMERO_15'], $numero15);
}

function descreverProcessamentoJogo($codigo)
{
    return 'Jogo #' . $codigo . ' processado.';
}

function descreverFinalizacaoJogo($codigo)
{
    return 'Jogo #' . $codigo . ' finalizado.';
}

function descreverReaberturaJogo($codigo)
{
    return 'Jogo #' . $codigo . ' reaberto.';
}

function descreverBloqueioJogo($codigo, $novoStatus)
{
    if ($novoStatus == 'B') {
        return 'Jogo #' . $codigo . ' bloqueado.';
    } else {
        return 'Jogo #' . $codigo . ' desbloqueado.';
    }
}

function descreverTrocaSenhaUusario($nome)
{
    return 'Usuário "' . $nome . '" realizou a troca de senha.';
}

function descreverCriacaoArea($nome, $descricao)
{
    return getBold('Nome: ') . $nome . ' ' .
        getBold('Descrição: ') . $descricao . '.';
}

function descreverEdicaoArea($oldArea, $cod_area, $nome, $descricao)
{
    return getBold('Area ') . '#' . $cod_area . ' ' .
        returnIfDifferent(getBold('Nome: '), $oldArea['NOME'], $nome) .
        returnIfDifferent(getBold('Descrição: '), $oldArea['DESCRICAO'], $descricao);
}

function descreverDesativacaoArea($nome, $status)
{
    return 'A área "' . $nome . '" foi ' .
        ($status == 'A' ? 'inativada.' : 'ativada.');
}
