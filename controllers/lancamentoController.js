angular.module('main').controller('lancamentoCtrl', function ($scope, $http, $location, user, $route, $rootScope) {
    $rootScope.loading = false;
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.saldo = user.getSaldoAtualizado();
    $scope.chkAjudaCusto = false;

    $scope.listarLancamentos = function () {
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/lancamentos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + 
                  '&site=' + user.getSite() + 
                  '&cambistas=' + $scope.chkExibirCambistas + 
                  '&perfil=' + user.getPerfil() + 
                  '&cod_usuario=' + user.getCodigo() +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;		
            $rootScope.loading = false;	
        }, function (response) {
            console.log(response);
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status);
        });

    }


    $scope.getCambistas = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/bilhetes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo() + 
                  '&site=' + user.getSite() + 
                  '&operacao=getCambistas' + 
                  '&todosUsuarios=' + user.getPerfil() +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.cambistas = response.data;
        }, function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.exibirCambistas = function () {
        $scope.chkExibirCambistas = !$scope.chkExibirCambistas;
        $scope.records = [];
        $scope.listarUsuarios();
    }

    $scope.getPerfilUsuario = function (perfil) {
        if (perfil == "C") {
            return "Cambista";
        } else if (perfil == "G") {
            return "Gerente";
        } else if (perfil == "A") {
            return "Admin";
        }
    }

    $scope.getStatusUsuario = function (status) {
        if (status == "A") {
            return "Ativo";
        } else if (status == "I") {
            return "Inativo";
        }
    }

    $scope.getAjudaCusto = function (ajudaCusto) {
        if (ajudaCusto == "S") {
            return "SIM";
        } else if (ajudaCusto == "N") {
            return "NÃO";
        }
    }

    $scope.excluir = function (codigo, data_hora, nome_criador, tipo,
        nomeColaborador, motivo, ajudaCusto, valor) {
            $.confirm({
                title: '',
                content:"Confirma a exclusão do lançamento?",
                buttons: {
                    cancelar: function() {},
                    ok: function () {
                        $rootScope.loading = true;
                        $http({
                            url: 'angularjs-mysql/excluirLancamento.php',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: 'cod_usuario=' + user.getCodigo() +
                                '&cod_site=' + user.getSite() +
                                '&codigo=' + codigo +
                                '&data=' + data_hora +
                                '&valor=' + valor +
                                '&tipo=' + tipo +
                                '&nome_criador=' + nome_criador +
                                '&motivo=' + motivo +
                                '&nomeColaborador=' + nomeColaborador +
                                '&ajudaCusto=' + ajudaCusto +
                                '&schema=' + user.getSchema()
                        }).then(function (response) {
                            if (response.data.status == 'OK') {
                                $route.reload();
                                $rootScope.loading = false;
                            } else {
                                $rootScope.loading = false;
                                $.alert(response.data.mensagem);
                            }
                        })
                    }
                }
            });
    }



    $scope.salvar = function () {
        var usuario_lancamento = $scope.comboColaborador ? JSON.parse($scope.comboColaborador) : null;
        var valor = $scope.edtValor;
        var tipo = $scope.cmbTipo;
        var motivo = $scope.edtMotivo;
        var colaboradorEnviar = $scope.comboColaboradorEntrada ? JSON.parse($scope.comboColaboradorEntrada) : null;
        var ajudaCusto = $scope.chkAjudaCusto;

        if (typeof valor === "undefined") {
            $.alert('Informe o VALOR!');
        } else if (typeof tipo === "undefined" && user.getPerfil() != "G") {
            $.alert('Informe o TIPO!')
        } else if (typeof tipo === "undefined" && !colaboradorEnviar) {
            $.alert('Informe o COLABORADOR!')
        } else if (!usuario_lancamento) {
            $.alert('Informe o COLABORADOR!')
        } else if (typeof motivo === "undefined") {
            $.alert('Informe o MOTIVO!')
        }
        else {
            valor = valor.replace('.', '');
            valor = valor.replace(',', '.');
            $rootScope.loading = true;
            $http({
                url: 'angularjs-mysql/salvarLancamento.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo() +
                    '&nome_usuario=' + user.getNome() +
                    '&cod_site=' + user.getSite() +
                    '&valor=' + valor +
                    '&tipo=' + tipo +
                    '&cod_usuario_lancamento=' + usuario_lancamento.cod_usuario +
                    '&nome_usuario_lancamento=' + usuario_lancamento.nome +
                    '&motivo=' + motivo +
                    '&colaboradorEnviar=' + (colaboradorEnviar === null ? '' : colaboradorEnviar.cod_usuario) +
                    '&nomeColaboradorEnviar=' + (colaboradorEnviar === null ? '' : colaboradorEnviar.nome) +
                    '&ajudaCusto=' + ajudaCusto +
                    '&schema=' + user.getSchema()
            }).then(function (response) {
                if (response.data.status == 'OK') {
                    $route.reload();
                    $rootScope.loading = false;
                } else {
                    $rootScope.loading = false;
                    $.alert(response.data.mensagem);
                }
            })
        }
    }
});