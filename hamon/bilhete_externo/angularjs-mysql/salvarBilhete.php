<?php

include "conexao.php";

require_once('../../angularjs-mysql/auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];
$auditoria = "";

try {
    $con->begin_transaction();
    $query = 'SET @@session.time_zone = "-03:00"';

    $result = mysqli_query($con, $query);
    $cod_jogo = mysqli_real_escape_string($con, $_POST['cod_jogo']);

    $query = "SELECT TP_STATUS, HORA_EXTRACAO, DATA_JOGO, TIPO_JOGO,
              CONCURSO AS COD_EXTRACAO, CURRENT_TIME AGORA, current_date HOJE 
              FROM jogo
              WHERE cod_jogo = '$cod_jogo'";
    $result = mysqli_query($con, $query);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if (($row['TP_STATUS'] == 'B') || ($row['TIPO_JOGO'] == 'B' &&
        $row['HORA_EXTRACAO'] < $row['AGORA'] &&
        $row['DATA_JOGO'] == $row['HOJE'])) {
        throw new Exception('Concurso Bloqueado! Atualize a página e selecione um concurso válido!');
    }

    $tipo_jogo = $row['TIPO_JOGO'];
    $qtdPremios = 10;

    if ($tipo_jogo == 'B') {
        $query = "select (j.data_jogo < current_date) data_passou,
                        (j.data_jogo <= current_date) data_retorno,
                        ((CURRENT_TIME + INTERVAL e.prazo_bloqueio MINUTE) >= j.hora_extracao) hora_retorno
                    from jogo j
                        inner join extracao_bicho e on (j.concurso = e.cod_extracao)
                    where cod_jogo = '$cod_jogo'";
        $cod_extracao = $row['COD_EXTRACAO'];
        $queryExtracao = mysqli_query($con, "SELECT qtd_premios FROM extracao_bicho where COD_EXTRACAO = '$cod_extracao'");
        $rowExtracao = mysqli_fetch_object($queryExtracao);
        if ($rowExtracao) {
            $qtdPremios = $rowExtracao->qtd_premios;
        }
    } else {
        $query = "select (data_jogo < current_date) data_passou,
                     (data_jogo <= current_date) data_retorno,
                    (CURRENT_TIME >= '19:50') hora_retorno
                from jogo
                where cod_jogo = '$cod_jogo'";
    }

    if ($tipo_jogo != '2') {
        $apostas = json_decode($_POST['apostas']);
        foreach ($apostas as $e) {
            if ($e->valorReal < 0) {
                throw new Exception('Não é permitido apostas com valor inferior a zero!');
            }
            if ($tipo_jogo == 'B' && ($e->premioDoReal > $qtdPremios || $e->premioAoReal > $qtdPremios)) {
                throw new Exception('A quantidade máxima de prêmios para esta extração é de '
                    . $qtdPremios . ' prêmios! Por favor recarregue a página.');
            }
        }
    }

    $result = mysqli_query($con, $query);

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
        throw new Exception('Aposta não pode ser feita para esse concurso! Selecione o próximo concurso!');
    }

    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $nome_usuario = mysqli_real_escape_string($con, $_POST['nome_usuario']);
    $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
    $nome_apostador = mysqli_real_escape_string($con, $_POST['nome']);
    $telefone_apostador = mysqli_real_escape_string($con, $_POST['telefone']);
    $data_atual = mysqli_real_escape_string($con, $_POST['data_atual']);
    $pule = mysqli_real_escape_string($con, $_POST['pule']);

    $stmt = $con->prepare("insert into bilhete(cod_site, cod_usuario, nome_apostador, telefone_apostador, data_bilhete, txt_pule, status_bilhete)
							VALUES (?, ?, ?, ?, ?, ?, 'V') ");
    $stmt->bind_param("iissss", $cod_site, $cod_usuario, $nome_apostador, $telefone_apostador, $data_atual, $pule);

    $stmt->execute();
    $codBilhete = $stmt->insert_id;

    $auditoria = descreverCriacaoBilhete(
        $nome_apostador,
        $telefone_apostador,
        $data_atual,
        $pule,
        $codBilhete,
        null
    );

    $apostas = json_decode($_POST['apostas']);

    $contador = 0;
    $valorReal = 0;
    $sNumeros = "";
    $constanteSim = mysqli_real_escape_string($con, 'S');
    foreach ($apostas as $e) {
        $sNumeros = str_replace('.', '', $e->numeros);
        if ($tipo_jogo == 'B') {
            $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, txt_aposta, valor_aposta, possivel_retorno, do_premio, ao_premio, tipo_jogo, inversoes, comissao )
            values(?,?,?,?,?,?,?,?,?,?,?) ");
            $stmt->bind_param("iiisddsssid", $cod_site, $cod_jogo, $codBilhete, $sNumeros, $e->valorReal, $e->possivelRetornoReal, $e->premioDoReal, $e->premioAoReal, $e->tipoJogo, $e->inversoes, $comissao);
            $auditoria = $auditoria . descreverApostaBicho($sNumeros, $e->valorReal, $e->possivelRetornoReal, $e->premioDoReal, $e->premioAoReal, $e->tipoJogo, $e->inversoes, null);
        } else if ($tipo_jogo == 'L') {
            $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno )
            values(?,?,?,?,?,?,?) ");
            $stmt->bind_param("iiiisdd", $cod_site, $cod_jogo, $codBilhete, $e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoReal);
            $auditoria = $auditoria . descreverApostaLotinha($e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoReal, null);
        } else {
            if ($tipo_jogo == 'S' && $e->habilitaSorte == 'S') {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno5, retorno4 )
                values(?,?,?,?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiiisddsdd", $cod_site, $cod_jogo, $codBilhete, $e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoSenaReal, $constanteSim, $e->possivelRetornoQuinaReal, $e->possivelRetornoQuadraReal);
                $auditoria = $auditoria . descreverApostaSeninhaSorte($e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoSenaReal, $constanteSim, $e->possivelRetornoQuinaReal, $e->possivelRetornoQuadraReal, null);
            } else if ($tipo_jogo == 'Q' && $e->habilitaSorte == 'S') {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno4, retorno3 )
                values(?,?,?,?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiiisddsdd", $cod_site, $cod_jogo, $codBilhete, $e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoQuinaReal, $constanteSim, $e->possivelRetornoQuadraReal, $e->possivelRetornoTernoReal);
                $auditoria = $auditoria . descreverApostaQuininhaSorte($e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoQuinaReal, $constanteSim, $e->possivelRetornoQuadraReal, $e->possivelRetornoTernoReal, null);
            } else {
                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno )
                values(?,?,?,?,?,?,?) ");
                $stmt->bind_param("iiiisdd", $cod_site, $cod_jogo, $codBilhete, $e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoReal);
                $auditoria = $auditoria . descreverApostaQuininhaSeninha($cod_jogo, $e->qtd, $sNumeros, $e->valorReal, $e->possivelRetornoReal, null);
            }
        }
        $valorReal = $valorReal + $e->valorReal;

        $stmt->execute();
    }

    inserir_auditoria(
        $con,
        $cod_usuario,
        $cod_site,
        AUD_BILHETE_CRIADO,
        $auditoria
    );

    $stmt = $con->prepare("UPDATE usuario SET SALDO = SALDO - ?
		                    WHERE COD_USUARIO = ? AND COD_SITE = ?");
    $stmt->bind_param("dii", $valorReal, $cod_usuario, $cod_site);
    $stmt->execute();
    $con->commit();
    $con->close();

    $response['numero_bilhete'] = $codBilhete;
    $response['status'] = "OK";
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);
