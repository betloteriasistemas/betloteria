<?php
include "conexao.php"; 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

try {
    switch ($operacao) {
        case 'listar':
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $query = "SELECT codigo, tipo_jogo, data_jogo, horario_bloqueio, status, hora_extracao, descricao_extracao  
                      FROM bloqueios_avulsos
                      WHERE data_jogo = '$data'";
            $result = mysqli_query($con, $query);
            $return_arr = array();
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $row_array['codigo'] = $row['codigo'];
                $row_array['tipo_jogo'] = $row['tipo_jogo'];
                $row_array['data_jogo'] = $row['data_jogo'];
                $row_array['horario_bloqueio'] = $row['horario_bloqueio'];
                $row_array['status'] = $row['status'];
                $row_array['hora_extracao'] = $row['hora_extracao'];
                $row_array['descricao_extracao'] = $row['descricao_extracao'];
    
                array_push($return_arr, $row_array);
            }
            echo json_encode($return_arr);
        break;
    
        case 'inserir':
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $tipo = mysqli_real_escape_string($con, $_POST['tipo']);
            $horaBloqueio = mysqli_real_escape_string($con, $_POST['horaBloqueio']);
            $horaExtracao = mysqli_real_escape_string($con, $_POST['horaExtracao']);
            $descricaoExtracao = mysqli_real_escape_string($con, $_POST['descricaoExtracao']);
            if ($tipo != 'B' && $tipo != '2') {
                $horaExtracao = null;
                $descricaoExtracao = null;
            }
            $stmt = $con->prepare("INSERT INTO bloqueios_avulsos
                      (data_jogo, tipo_jogo, horario_bloqueio, hora_extracao, descricao_extracao)
                      VALUES(?, ?, ?, ?, ?)");
            $stmt->bind_param("sssss", 
            $data, $tipo, $horaBloqueio, $horaExtracao, $descricaoExtracao);
            $stmt->execute();
            $stmt->close();
            $response['status'] = "OK";
            echo json_encode($response);
        break;
    
        case 'atualizar':
            $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $tipo = mysqli_real_escape_string($con, $_POST['tipo']);
            $horaBloqueio = mysqli_real_escape_string($con, $_POST['horaBloqueio']);
            $horaExtracao = mysqli_real_escape_string($con, $_POST['horaExtracao']);
            $descricaoExtracao = mysqli_real_escape_string($con, $_POST['descricaoExtracao']);
            if ($tipo != 'B' && $tipo != '2') {
                $horaExtracao = null;
                $descricaoExtracao = null;
            }
            $stmt = $con->prepare("UPDATE bloqueios_avulsos
                                   SET data_jogo=?, tipo_jogo=?, horario_bloqueio=?, 
                                        hora_extracao=?, descricao_extracao = ?
                                   WHERE codigo=?");
            $stmt->bind_param("sssssi", 
            $data, $tipo, $horaBloqueio, $horaExtracao, $descricaoExtracao, $codigo);
            $stmt->execute();
            $stmt->close();
            $response['status'] = "OK";
            echo json_encode($response);
        break;
    
        case 'excluir':
            $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
            $stmt = $con->prepare("DELETE FROM bloqueios_avulsos
                                   WHERE codigo=?");
            $stmt->bind_param("i", $codigo);
            $stmt->execute();
            $stmt->close();
            $response['status'] = "OK";
            echo json_encode($response);
        break;

    }
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = "Ocorreu o seguinte erro: " . $e->getMessage();
    
    echo json_encode($response);
}

function ifStringNullReturn($val, $return)
{
    if ($val == 'null') {
        return $return;
    }
    return $val;
}