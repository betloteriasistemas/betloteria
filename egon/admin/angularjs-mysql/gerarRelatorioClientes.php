<?php

include "conexao.php";
require_once '../../angularjs-mysql/vendor/autoload.php';


if (!isset($_GET)) {
    die();
}

$codCompetencia = mysqli_real_escape_string($con, $_GET['codCompetencia']);
$comEntrada = mysqli_real_escape_string($con, $_GET['comEntrada']);
$campoOrdenacao = mysqli_real_escape_string($con, $_GET['campoOrdenacao']);
$ordenacao = mysqli_real_escape_string($con, $_GET['ordenacao']);
$competencia = "-";
$comRegistroEntrada = $comEntrada == 'S' ? "Sim" : "Não";

$query = "SELECT * 
            FROM registros_ctrl_clientes r
                inner join site s on r.cod_site = s.cod_site
                inner join controle_clientes c on r.CODIGO_CTRL_CLIENTES = c.codigo
            WHERE r.CODIGO_CTRL_CLIENTES = '$codCompetencia' 
                AND r.COD_SITE <> 0 ";                
if ($comEntrada == 'S') {
    $query = $query . " AND r.entradas > 0 ";
}                            
$query = $query . " ORDER BY $campoOrdenacao $ordenacao";

$result = mysqli_query($con, $query);
$return_arr = array();
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $competencia = $row['MES'] . "/" . $row['ANO'];
    $row_array['cod_site'] = $row['COD_SITE'];
    $row_array['qtd_usuarios'] = $row['QTD_USUARIOS'];
    $row_array['qtd_auditorias'] = $row['QTD_AUDITORIAS'];
    $row_array['qtd_apostas'] = $row['QTD_APOSTAS'];
    $row_array['entradas'] = $row['ENTRADAS'];
    $row_array['saidas'] = $row['SAIDAS'];
    $row_array['comissao_cambistas'] = $row['COMISSAO_CAMBISTAS'];
    $row_array['comissao_gerentes'] = $row['COMISSAO_GERENTES'];
    $row_array['saldo'] = $row['SALDO'];
    $row_array['txt_empresa'] = $row['TXT_EMPRESA'];
    $row_array['vencimento'] = $row['vencimento'];
    $row_array['mensalidade'] = $row['mensalidade'];
    array_push($return_arr, $row_array);
}

$html = "
<html>
    <head>
        <meta charset='utf-8'>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 10px;
            }
        </style>
    </head>
    <body>
        <div class='col-md-12 col-ms-12 col-xs-12'>
            <div class='table-responsive betL_CaixaList'>
                <div style='display: inline;'>
                    <span style='font-size: 12pt;'> <b>Competência:</b> " . $competencia . "</span>
                    &nbsp;| &nbsp;
                    <span style='font-size: 12pt;'> <b>Com entradas registradas:</b> " . $comRegistroEntrada . "</span>
                </div>
                <hr>
                <h3>CLIENTES:</h3>";

if (count($return_arr) == 0) {

    $html = $html . "
                <hr>
                    <h4>Não há dados</h4>
                <hr>";

} else {

    $html = $html . " 
                <table class='table table-hover' style='width: 100%; margin-left:auto; margin-right:auto;'> 
                    <thead>
                        <tr style='background-color: #2E64FE; color: white;'> 
                            <th style='width: 9%; text-align: center; color: white; font-weight: bold;'>Site</th>
                            <th style='width: 15%; text-align: center; color: white; font-weight: bold;'>Domínio</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Vencimento</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Mensalidade</th>
                            <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>Nº Usuários</th>
                            <th style='width: 10%; text-align: center; color: white; font-weight: bold;'>Nº Transações</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Nº Apostas</th>
                            <th style='width: 15%; text-align: center; color: white; font-weight: bold;'>Entradas</th>
                            <th style='width: 15%; text-align: center; color: white; font-weight: bold;'>Saídas</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Comissão Cambistas</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Comissão Gerentes</th>
                            <th style='width: 11%; text-align: center; color: white; font-weight: bold;'>Saldo</th>
                        </tr>
                    </thead>
                    <tbody class='container'> ";
    $odd = 0;           
    foreach ($return_arr as $e) {
        $odd = $odd + 1;
        if($odd % 2 == 0) {
            $html = $html . "
                        <tr style='background-color: #E6E6E6;'>";
        } else {
            $html = $html . "
                        <tr>";
        }
        $mensalidade = ($e['mensalidade'] == 'undefined' || $e['mensalidade'] == ''  || $e['mensalidade'] == '---') 
            ? '-' : number_format($e['mensalidade'], 2, ',', '.');
        $html = $html . " 
                            <td align='center'> " . $e['cod_site'] . " </td>
                            <td align='center'> " . $e['txt_empresa'] . " </td>
                            <td align='center'> " . $e['vencimento'] . " </td>
                            <td align='right'> R$ " . $mensalidade . " </td>
                            <td align='center'> " . $e['qtd_usuarios'] . " </td>
                            <td align='center'> " . $e['qtd_auditorias'] . " </td>
                            <td align='center'> " . $e['qtd_apostas'] . " </td>
                            <td align='right'> R$ " . number_format($e['entradas'], 2, ',', '.') . " </td>
                            <td align='right'> R$ " . number_format($e['saidas'], 2, ',', '.') . " </td>
                            <td align='right'> R$ " . number_format($e['comissao_cambistas'], 2, ',', '.') . " </td>
                            <td align='right'> R$ " . number_format($e['comissao_gerentes'], 2, ',', '.') . " </td>";

        if ($e['saldo'] >= 0) {
            $html = $html . "
                            <td style='color: green; font-weight: bold;' align='right'> R$ " 
                                . number_format($e['saldo'], 2, ',', '.') . " </td>";
        } else {
            $html = $html . "
                            <td style='color: red; font-weight: bold;' align='right'> R$ " 
                                . number_format($e['saldo'], 2, ',', '.') . " </td>";
        }
                            
        
    }
    $html = $html . "
                        </tr>
                    </tbody>
                </table> ";
}

$html = $html . "                       
            </div>
        </div>
    </body> 
</html> ";



// print_r($queryVer);
// printf($html);

$hoje = date('YmdHis');
$arquivo = "relatorio_clientes" . $hoje . ".xls";
header ("Expires: Mon, 18 Nov 1985 18:00:00 GMT");
header ("Last-Modified: ".gmdate("D,d M YH:i:s")." GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");	
header ("Content-Type: text/html; charset=UTF-8");

// Imprime o conteúdo da nossa tabela no arquivo que será gerado
echo $html;
$con->close();
 
?>
