angular.module('main').controller('caixaClienteCtrl', function ($scope, user, $http, $filter, $rootScope) {
    $scope.nome = user.getNome();
    $scope.comboTipoJogo = "";
    $scope.comboStatus = "A";
    $scope.editarCliente = {};
    $scope.dia = new Date().getDate();
    

    $scope.listarClientes = function () {  
        dataInicial  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        dataFinal = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");
        $rootScope.loading = true;
        $http({
            
            method: "POST",
            url: "angularjs-mysql/clienteListar.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data:'dataDe=' + dataInicial + '&dataAte=' + dataFinal + '&operacao=' + 'listar_cliente' 
        }).then(function (response) {
            $scope.records = response.data;
            $rootScope.loading = false;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status);
            $rootScope.loading = false;
        });
    }
       
    
    


        $scope.buscarCliente = function (cliente){
            $scope.editarCliente = angular.copy(cliente);
           
        }
        
        $scope.salvar = function (){
           var cliente = {
               cod_site: $scope.editarCliente.cod_site,
               mensalidade: $scope.editarCliente.mensalidade,
               promocao: $scope.editarCliente.promocao
           };
           $http({
            
            method: "POST",
            url: "angularjs-mysql/clienteListar.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=' + 'salvar'  + '&cliente=' + JSON.stringify(cliente)
        }).then(function (response) {
            console.log(response.data);
            if(response.data[0] == 'OK')
            {
                alert("Alteração com sucesso");
            }else {
            alert("erro inesperado")}

        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        });
           console.log(cliente);
        }
});