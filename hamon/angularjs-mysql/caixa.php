<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$perfil = mysqli_real_escape_string($con, $_POST['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);
$dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);
$index = mysqli_real_escape_string($con, $_POST['index']);
$qtdElementos = mysqli_real_escape_string($con, $_POST['qtdElementos']);
$codigoJogo = mysqli_real_escape_string($con, $_POST['concurso']);
$codigoUsuario = mysqli_real_escape_string($con, $_POST['codigoUsuario']);
$tipoJogo = mysqli_real_escape_string($con, $_POST['tipoJogo']);
$statusJogo = mysqli_real_escape_string($con, $_POST['statusJogo']);
$campoOrdenacao = mysqli_real_escape_string($con, $_POST['campoOrdenacao']);
$ordenacao = mysqli_real_escape_string($con, $_POST['ordenacao']);
$relatorioSintetico = mysqli_real_escape_string($con, $_POST['relatorioSintetico']);
$pesquisarCaixaCambista = mysqli_real_escape_string($con, $_POST['pesquisarCaixaCambista']);
$codigoUsuarioGerente = mysqli_real_escape_string($con, $_POST['codigoUsuarioGerente']);

$totalizadores = "";

switch ($perfil) {
    case "C":
        $query = "
		select v.data_jogo, v.concurso, v.hora_extracao, v.desc_hora, v.tp_status, v.tipo_jogo, v.tipo_jogo_dinamico,
			   v.data_inteiro, sum(v.entradas) entradas, sum(v.qtd_apostas) qtd_apostas, sum(v.comissao) comissao, 
			   sum(v.comissao_gerente) comissao_gerente, sum(v.saidas) saidas, sum(v.entradas) - sum(v.saidas) - sum(v.comissao)  saldo, 0 as saldo_par
		from (
				(
					select DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo,
						   cast(
								case when jogo.tipo_jogo IN ('B','2') then 
										concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
									 when jogo.tipo_jogo = 'R' then jogo.DESCRICAO
									 else jogo.concurso end as char
								) as concurso,  
							TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo,
							(case when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO)) else '' end) as tipo_jogo_dinamico,
							DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
							sum(apo.valor_aposta) entradas,
							count(apo.cod_aposta) qtd_apostas,    
							sum(apo.comissao) as comissao,
							0 comissao_gerente,
							sum(apo.valor_ganho) as saidas
					from jogo
					inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
					inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
					inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
					left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
					left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
					where jogo.cod_site = '$site'
					  and usu.cod_usuario = '$cod_usuario'
					  and bil.status_bilhete != 'C'
					  and apo.status != 'C' ";
        
        if($codigoJogo) {
            $query = $query . " AND jogo.cod_jogo = '$codigoJogo' ";
        }

        $query = $query . " group by jogo.data_jogo, jogo.concurso, jogo.hora_extracao , jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo) "; 
        $query = $query . " union all
                        (select DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,'LANÇAMENTO' as concurso,null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo, null as tipo_jogo_dinamico,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, (case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao, 0 as comissao_gerente, 
                        (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas
                         from lancamento L where cod_site = '$site' and cod_usuario_lancamento = '$cod_usuario' and ajuda_custo = 'N' and L.status = 'A')
                        ) v
                        where 1 =1 ";

        if($finalizados == 'false') {
            $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
        }

        if($codigoUsuario) {
            $query = $query . " AND COD_USUARIO = $codigoUsuario ";
        }
    
        if($statusJogo) {
            $query = $query . " AND TP_STATUS = '$statusJogo' ";
        }
    
        if($tipoJogo) {
            $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
        }
    
        if ($dataDe != '0') {
            $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
        }
    
        if ($dataAte != '0') {
            $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
        }

        if ($dataDe == '0' && $dataAte == '0') {
            $query . " AND data_inteiro >= DATE_FORMAT(current_date - 7, '%Y%m%d')
                       AND data_inteiro <= DATE_FORMAT(current_date, '%Y%m%d') " ;
        }
    
        $query = $query . " group by v.data_jogo, v.concurso, v.hora_extracao, v.desc_hora, v.tp_status, v.tipo_jogo, v.data_inteiro 
                            order by $campoOrdenacao $ordenacao, concurso, qtd_apostas ";

        $totalizadores = "SELECT SUM(qtd_apostas) as apostas, ".
                        "SUM(comissao) as comissao, ".
                        "SUM(comissao_gerente) as comissao_gerente, ".
                        "SUM(entradas) as entradas, ".
                        "SUM(saidas) as saidas, ".
                        "SUM(saldo) as saldo, ".
                        "SUM(saldo_par) as saldo_par, ".
                        "COUNT(1) as 'total_registros' ".
                        "FROM ( ".$query." ) t";
                        
        $query = $query." LIMIT $index,$qtdElementos ";          
    break;
    case "G":
        $query = "select g.cod_usuario, g.nome, "; 
    
        if($relatorioSintetico == "false") {
            $query = $query."g.data_jogo, g.concurso, g.hora_extracao, g.desc_hora, 
            g.tp_status, g.tipo_jogo, g.tipo_jogo_dinamico, g.data_inteiro, ";
        } 
        $query =  $query. "
		sum(g.entradas) entradas, sum(g.qtd_apostas) qtd_apostas, sum(g.comissao) comissao,
			   sum(g.saidas) saidas, sum(g.saldo_par) saldo_par, sum(g.comissao_gerente) comissao_gerente, 
			   sum(g.saldo_par) - sum(g.comissao_gerente) as saldo 
		from (
				(
					select v.*, (v.entradas - v.saidas - v.comissao) saldo_par
					from (
							select usu.cod_usuario, usu.nome, DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, 
								   cast(
										case when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
											 when jogo.tipo_jogo = 'R' then jogo.DESCRICAO
											 else jogo.concurso end as char
										) as concurso,
									TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo,
									(case when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO)) else '' end) as tipo_jogo_dinamico,                 
									DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
									sum(apo.valor_aposta) entradas,
									count(apo.cod_aposta) qtd_apostas,
									sum(apo.comissao) as comissao,
									sum(apo.comissao_gerente) as comissao_gerente,
									sum(apo.valor_ganho) as saidas
							from jogo
							inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
							inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
							inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
							left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
							left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
							where jogo.cod_site = '$site'
							  and usu.cod_gerente = '$cod_usuario'
							  and bil.status_bilhete != 'C'
							  and apo.status != 'C' ";
    
        if($codigoJogo) {
            $query = $query . " AND jogo.cod_jogo = '$codigoJogo' ";
        }
   
        $query = $query . " group by usu.cod_usuario, usu.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo)v ) ) g where 1 =1 ";
    
        if ($finalizados == 'false') {
            $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
        }

        if($codigoUsuario) {
            $query = $query . " AND COD_USUARIO = $codigoUsuario ";
        }

        if($statusJogo) {
            $query = $query . " AND TP_STATUS = '$statusJogo' ";
        }

        if($tipoJogo) {
            $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
        }

        if ($dataDe != '0') {
            $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
        }

        if ($dataAte != '0') {
            $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
        }

        if ($dataDe == '0' && $dataAte == '0') {
            $query . " AND data_inteiro >= DATE_FORMAT(current_date - 7, '%Y%m%d')
                       AND data_inteiro <= DATE_FORMAT(current_date, '%Y%m%d') " ;
        }

        if ($relatorioSintetico == "false") {
            $query = $query." GROUP BY g.cod_usuario, g.nome, g.data_jogo, g.concurso, g.hora_extracao, g.desc_hora, g.tp_status,g.tipo_jogo, g.data_inteiro";
        } else {
            $query = $query." GROUP BY g.cod_usuario, g.nome";
        }

        $query = $query . 
        " order by $campoOrdenacao  $ordenacao, concurso, qtd_apostas ";
        $totalizadores = "SELECT SUM(qtd_apostas) as apostas, ".
                                "SUM(comissao) as comissao, ".
                                "SUM(comissao_gerente) as comissao_gerente, ".
                                "SUM(entradas) as entradas, ".
                                "SUM(saidas) as saidas, ".
                                "SUM(saldo) as saldo, ".
                                "SUM(saldo_par) as saldo_par, ".
                                "COUNT(1) as 'total_registros' ".
                                "FROM ( ".$query." ) t";

        $query = $query." LIMIT $index,$qtdElementos ";
    break;
    case "A":
        if ($pesquisarCaixaCambista == "false") {
            $query = "select cod_usuario, nome, "; 
    
            if($relatorioSintetico == "false") {
                $query = $query."data_jogo,concurso,hora_extracao,desc_hora,tp_status,tipo_jogo,
                v.tipo_jogo_dinamico, data_inteiro, ";
            } 
            $query = $query . "
                       sum(entradas) as entradas,
                       sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
                       round(sum(comissao_gerente),2) as comissao_gerente,
                       (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
                       (sum(v.entradas) - sum(v.saidas) - sum(v.comissao) - sum(v.comissao_gerente))  as saldo from (
                       (
                        select usu.cod_usuario as cod_cambista, geren.cod_usuario, geren.nome,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, 
                               cast(
                                    case when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
                                         when jogo.tipo_jogo = 'R' then jogo.DESCRICAO
                                         else jogo.concurso end as char
                                    ) as concurso,
                                TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status,  jogo.tipo_jogo, 
                                (case when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO)) else '' end) as tipo_jogo_dinamico,
                                DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
                                sum(apo.valor_aposta) entradas,
                                count(apo.cod_aposta) qtd_apostas,
                                sum(apo.comissao) as comissao,
                                SUM(apo.comissao_gerente) AS comissao_gerente,
                                sum(apo.valor_ganho) as saidas
                        from jogo
                        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                        inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                        left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                        left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
                        where jogo.cod_site = '$site'
                              and geren.cod_gerente = '$cod_usuario'
                              and bil.status_bilhete != 'C'
                              and apo.status != 'C' ";
                    
            if($codigoJogo) {
                $query = $query . " AND jogo.cod_jogo = '$codigoJogo' ";
            }
    
            $query = $query . " group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status, jogo.tipo_jogo) ";
                
            $query = $query . " ) v where 1 =1 ";
                
            if ($finalizados == 'false') {
                $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
            }
    
            if($codigoUsuario) {
                $query = $query . " AND COD_USUARIO = $codigoUsuario ";
            }
    
            if($statusJogo) {
                $query = $query . " AND TP_STATUS = '$statusJogo' ";
            }
    
            if($tipoJogo) {
                $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
            }
    
            if ($dataDe != '0') {
                $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
            }
    
            if ($dataAte != '0') {
                $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
            }
    
            if ($dataDe == 0 && $dataAte == 0) {
                $query = $query . " AND data_inteiro >= DATE_FORMAT(current_date - 7, '%Y%m%d')
                                    AND data_inteiro <= DATE_FORMAT(current_date, '%Y%m%d') " ;
            }

            if ($relatorioSintetico == "false") {
                $query = $query." GROUP BY cod_usuario,nome,data_jogo, concurso, hora_extracao, desc_hora,tp_status, tipo_jogo, data_inteiro";
            } else {
                $query = $query." GROUP BY cod_usuario, nome";
            }
                
            $query = $query . " ORDER BY $campoOrdenacao $ordenacao, concurso, qtd_apostas ";
                                
            $totalizadores = "SELECT SUM(qtd_apostas) as apostas, ".
                             "SUM(comissao) as comissao, ".
                             "SUM(comissao_gerente) as comissao_gerente, ".
                             "SUM(entradas) as entradas, ".
                             "SUM(saidas) as saidas, ".
                             "SUM(saldo) as saldo, ".
                             "SUM(saldo_par) as saldo_par, ".
                             "COUNT(1) as 'total_registros' ".
                             "FROM ( ". $query . " ) t";
    
            $query = $query." LIMIT $index, $qtdElementos ";
    
            $query_totalizadores_por_gerente = "
                select cod_usuario,nome,data_jogo,concurso,hora_extracao,desc_hora,tp_status,tipo_jogo,data_inteiro, sum(entradas) as entradas,
                sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
                (CASE WHEN (sum(v.entradas_sem_lancamento) - sum(v.saidas) - sum(v.comissao)) > 0 THEN
                    round(sum(v.comissao_gerente), 2)
                    ELSE 0 END) as comissao_gerente,
                (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
                (CASE WHEN (sum(v.entradas_sem_lancamento) - sum(v.saidas) - sum(v.comissao)) > 0 THEN
                    ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - (round(sum(v.comissao_gerente), 2)))
                    ELSE (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) END) as saldo from (
                    (
                    select usu.cod_usuario as cod_cambista, geren.cod_usuario, geren.nome,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
                    when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
                    else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status,  jogo.tipo_jogo, 
                    DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
                    sum(apo.valor_aposta) entradas,
                    sum(apo.valor_aposta) entradas_sem_lancamento,
                    count(apo.cod_aposta) qtd_apostas,
                    sum(apo.comissao) as comissao,
                    sum(apo.comissao_gerente) as comissao_gerente,
                    sum(apo.valor_ganho) as saidas
                    from jogo
                    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                    inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                    left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                    where jogo.cod_site = '$site'
                    and geren.cod_gerente = '$cod_usuario'
                    and bil.status_bilhete != 'C'
                    and apo.status != 'C' ";
                    
            if($codigoJogo) {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND jogo.cod_jogo = '$codigoJogo' ";
            }
            
            $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status, jogo.tipo_jogo) ";
                    
            $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " ) v where 1 =1 ";
                
            if ($finalizados == 'false') {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
            }
            
            if($codigoUsuario) {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND COD_USUARIO = $codigoUsuario ";
            }
            
            if($statusJogo) {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND TP_STATUS = '$statusJogo' ";
            }
            
            if($tipoJogo) {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND TIPO_JOGO = '$tipoJogo' ";
            }
            
            if ($dataDe != '0') {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
            }
            
            if ($dataAte != '0') {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
            }
            
            if ($dataDe == 0 && $dataAte == 0) {
                $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " AND data_inteiro >= DATE_FORMAT(current_date - 7, '%Y%m%d') AND data_inteiro <= DATE_FORMAT(current_date, '%Y%m%d') " ;
            }
                
            $query_totalizadores_por_gerente = $query_totalizadores_por_gerente . " group by nome";            
                                         
            $totalizadores_comissao_saldo = "SELECT SUM(comissao_gerente) as comissao_gerente, ".            
                        "SUM(saldo) as saldo ".            
                        "FROM ( ". $query_totalizadores_por_gerente . " ) t";     
            
        } else {
            $agruparPorDadaJogo = false;
            $query = "select geren.nome nome_gerente, usu.nome nome_cambista, "; 
    
            if($relatorioSintetico == "false") {
                $query = $query."DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, ";
                $agruparPorDadaJogo = true;
            } 
    
            $query = $query . 
                          "sum(apo.valor_aposta) entradas, sum(apo.valor_ganho) saidas, 
                           sum(apo.comissao) comissao,
                           count(apo.cod_aposta) as qtd_apostas,
                           (sum(apo.valor_aposta) - sum(apo.valor_ganho) - sum(apo.comissao))  saldo_par,
                    jogo.tipo_jogo,
                    jogo.tp_status    
                    from jogo
                    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                    inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                    left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
                    where jogo.cod_site = '$site'
                        and geren.cod_gerente = '$cod_usuario'
                        and bil.status_bilhete != 'C'
                        and apo.status != 'C' ";
            
            if ($finalizados == 'false') {
                $query = $query . " AND (jogo.tp_status <> 'F' OR jogo.tp_status IS NULL) ";
            }
    
            if ($codigoUsuarioGerente) {
                $query = $query . " AND geren.cod_usuario = $codigoUsuarioGerente ";
            }
    
            if ($codigoUsuario) {
                $query = $query . " AND usu.cod_usuario = $codigoUsuario ";
            }
    
            if($tipoJogo) {
                $query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
            }
    
            if($statusJogo) {
                $query = $query . " AND jogo.tp_status = '$statusJogo' ";
            }
    
            if ($dataDe != '0') {
                $query = $query . " AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
            }
    
            if ($dataAte != '0') {
                $query = $query . " AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
            }
    
            if ($dataDe == 0 && $dataAte == 0) {
                $query = $query . " AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') >= DATE_FORMAT(current_date - 7,, '%Y%m%d')  
                                    AND DATE_FORMAT(jogo.data_jogo, '%Y%m%d') <= DATE_FORMAT(current_date, '%Y%m%d') " ;
            }
        
            if ($agruparPorDadaJogo) {
                $query = $query." GROUP BY geren.nome, usu.nome, jogo.data_jogo, jogo.tp_status order by jogo.data_jogo, geren.nome, usu.nome";
            } else {
                $query = $query." GROUP BY geren.nome, usu.nome order by geren.nome, usu.nome";
            }
        
    
            $totalizadores = "SELECT SUM(qtd_apostas) as apostas, ".
                "SUM(comissao) as comissao, ".
                "SUM(entradas) as entradas, ".
                "SUM(saidas) as saidas, ".
                "SUM(saldo_par) as saldo_par, ".
                "COUNT(1) as 'total_registros' ".
                "FROM ( ".$query." ) t";
    
            $query = $query." LIMIT $index, $qtdElementos ";
    
        }
    break;
}

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

if ($pesquisarCaixaCambista == "false") {
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        if ($perfil != "C") {
            $row_array['cod_usuario'] = $row['cod_usuario'];
            $row_array['nome'] = $row['nome'];
        }

        if ($relatorioSintetico == "false") {
            $row_array['concurso'] = $row['concurso'];
            $row_array['data_jogo'] = $row['data_jogo'];
            $row_array['tp_status'] = $row['tp_status'];
            $row_array['tipo_jogo'] = $row['tipo_jogo'];
            $row_array['tipo_jogo_dinamico'] = $row['tipo_jogo_dinamico'];
            $row_array['data_inteiro'] = $row['data_inteiro'];
        }

        $row_array['saldo'] = $row['saldo'];
       
        $row_array['entradas'] = $row['entradas'];
        $row_array['qtd_apostas'] = $row['qtd_apostas'];
    
        $row_array['comissao'] = $row['comissao'];
        $row_array['saidas'] = $row['saidas'];
        $row_array['comissao_gerente'] = $row['comissao_gerente'];
        $row_array['saldo_par'] = $row['saldo_par'];
    
    
        array_push($return_arr, $row_array);
    
        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

    $resultado_totalizador = mysqli_query($con, $totalizadores);
    $row = mysqli_fetch_array($resultado_totalizador);
    if ($row['saldo'] < 0) {
        $row['saldo'] = $row['saldo'] + $row['comissao_gerente'];
        $row['comissao_gerente'] = 0;
    }
    
    if ($perfil == "A") {
        $resultado_totalizador_comissao_saldo = mysqli_query($con, $totalizadores_comissao_saldo);
        $row_comissao_saldo = mysqli_fetch_array($resultado_totalizador_comissao_saldo);
        $row['comissao_gerente'] = $row_comissao_saldo['comissao_gerente'];
        $row['saldo'] = $row_comissao_saldo['saldo'];
    }
    
    $totalizadoresJSON{'totalizadores'} = $row;  
    
    array_push($return_arr,$totalizadoresJSON);    
    
} else {
    
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
       
        $row_array['nome_gerente'] = $row['nome_gerente'];
        $row_array['nome_cambista'] = $row['nome_cambista'];
        if ($agruparPorDadaJogo) {
            $row_array['data_jogo'] = $row['data_jogo'];
        }
        $row_array['tp_status'] = $row['tp_status'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        $row_array['entradas'] = $row['entradas'];
        $row_array['qtd_apostas'] = $row['qtd_apostas'];
    
        $row_array['comissao'] = $row['comissao'];
        $row_array['saidas'] = $row['saidas'];
        $row_array['saldo_par'] = $row['saldo_par'];
    
        array_push($return_arr, $row_array);
    
        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

    $resultado_totalizador = mysqli_query($con, $totalizadores);
    $row = mysqli_fetch_array($resultado_totalizador);
    $totalizadoresJSON{'totalizadores'} = $row;  
    
    array_push($return_arr,$totalizadoresJSON);    
}

echo json_encode($return_arr);

$con->close();
