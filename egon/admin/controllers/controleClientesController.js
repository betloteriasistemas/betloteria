angular.module('main').controller('controleClientesCtrl', function ($scope, $rootScope, $http, $route, $filter) {

    $scope.comboCompetencia = "";
    $scope.ordenacao = "ASC";
    $scope.campoOrdenacao = "vencimento";

    $scope.listarCompetencias = function () {
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/controleClientes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=listarCompetencias'
        }).then(function (response) {
            $rootScope.loading = false;
            $scope.competencias = response.data;
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }
    $scope.listarResultados = function () {
        if ($scope.comboCompetencia != "") {
            $rootScope.loading = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/controleClientes.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'operacao=listarRegistros&codCompetencia=' + $scope.comboCompetencia
                    + '&comEntrada=' + $scope.getDescSN($scope.comEntrada)
                    + '&campoOrdenacao=' + $scope.campoOrdenacao
                    + '&ordenacao=' + $scope.ordenacao 
            }).then(function (response) {
                $rootScope.loading = false;
                $scope.records = response.data;
            }, function (response) {
                $rootScope.loading = false;
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }

    $scope.orderFilter = function (campo) {
        
        if ($scope.ordenacao == "ASC") {
            $scope.ordenacao = "DESC";
        } else {
            $scope.ordenacao = "ASC";
        }

        $scope.campoOrdenacao = campo;
        $scope.listarResultados();

    };

    $scope.gerarRelatorioXls = function () {
        window.open('angularjs-mysql/gerarRelatorioClientes.php?codCompetencia=' + $scope.comboCompetencia
            + '&comEntrada=' + $scope.getDescSN($scope.comEntrada)
            + '&campoOrdenacao=' + $scope.campoOrdenacao
            + '&ordenacao=' + $scope.ordenacao );            
    }

    $scope.getDescricaoMes = function (mes) {
        switch (mes) {
            case "1":
                return 'JAN';
                break;
            case "2":
                return 'FEV';
                break;
            case "3":
                return 'MAR';
                break;   
            case "4":
                return 'ABR';
                break;
            case "5":
                return 'MAI';
                break;
            case "6":
                return 'JUN';
                break; 
            case "7":
                return 'JUL';
                break;
            case "8":
                return 'AGO';
                break;
            case "9":
                return 'SET';
                break;   
            case "10":
                return 'OUT';
                break;
            case "11":
                return 'NOV';
                break;
            case "12":
                return 'DEZ';
                break;                                                                            
        }
        return '';
    }

    $scope.getDescSN = function (comEntrada) {
        if (comEntrada == true) {
            return "S";
        } 
        return "N";
    };

})