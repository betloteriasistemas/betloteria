angular.module('main').controller('2pra500Ctrl', 
function ($rootScope, $scope, $q, user, $http, $filter, $location, configGeral) {
    $scope.flg_2pra500 = false;
    $scope.chkRegistroCliente = false;
    $scope.clientes = [];

    configGeral.get().then(function (data) {
        $scope.flg_2pra500= user.getflg_2pra500() == 'S' && data.flg_2pra500;
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        $scope.desc_2pra500 = funcoes.getDescricaoTipoJogo('2', $scope.desc_2pra500);
    });

    $q.all([
        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() 
                + '&cod_area=' + user.getArea() 
                + '&operacao=2pra500'
                + '&schema=' + user.getSchema()
        }),
        $http({
            url: 'angularjs-mysql/usuario.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() 
                + '&cod_usuario=' + user.getCodigo()
                + '&schema=' + user.getSchema()
        }),
        $http({
            method: "POST",
            url: "angularjs-mysql/clientes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_usuario=' + user.getCodigo()
                + '&operacao=listar'
                + '&schema=' + user.getSchema()
        })
    ]).then(function (response) {
        $scope.listaCfg = response[0].data;
        $scope.usuario = response[1].data;
        $scope.clientes = response[2].data;
    });

    $scope.edtDataDe = new Date();

    $scope.getJogos = function () {
        var data = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        $http({
            method: "POST",
            url: "angularjs-mysql/jogosArea.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + 
                  '&finalizados=false' + 
                  '&site=' + user.getSite() + 
                  '&area=' + user.getArea() + 
                  '&status=L' + 
                  '&operacao=2pra500' + 
                  '&data=' + data + 
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.jogos = []
            $scope.jogos = response.data;
            $scope.edtQtdBilhetes = null;
            $scope.comboConcurso = "undefined";
        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.filtrarCliente = function(termo) {
        return $scope.clientes.filter(cliente => cliente.nome.toLowerCase().indexOf(termo)>-1);
    }

    $scope.onSelectCliente = function(cliente) {
        if (cliente) {
            $scope.edtTelefone = cliente.telefone;
            document.getElementById("chkRegistrarCliente").focus();
        } else {
            $scope.edtTelefone = null;
        }
    }

    $scope.gerarBilhete = function () {                            
        
        try {
            
            if (parseFloat(user.getSaldoAtualizado()) < ($scope.edtQtdBilhetes * 2) ) {
                $.alert('Você não tem saldo para gerar o bilhete!');
                return;
            }

            if ($scope.comboConcurso == "undefined" || $scope.comboConcurso == null) {
                $.alert('Informe a EXTRAÇÃO!');
                return;
            }

            if (typeof $scope.edtQtdBilhetes === "undefined" || $scope.edtQtdBilhetes == null) {
                $.alert('Informe a Quantidade de Bilhetes!');
                return;
            } 
            
            if ($scope.edtQtdBilhetes <= 0) {
                $.alert("Informe uma quantidade de bilhetes maior que zero!");
                return;
            }

            if ($scope.chkRegistroCliente && !$scope.edtNome) {
                $.alert('Para registrar o cliente é necessário informar o NOME!');
                return;
            }

            $.confirm({
                title: '',
                content:"Confirma a geração do bilhete?",
                buttons: {
                    cancelar: function() {},
                    ok: function () {
                        var data = new Date();
        
                        var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
                        var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
                        var ano = data.getFullYear();
        
                        var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
                        var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
                        var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };
        
                        var dataAtual = ano + '-' + mes + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;
        
                        var sTelefone = $scope.edtTelefone;
                        if (!sTelefone) {
                            sTelefone = "";
                        }
                        
                        var sNome = $scope.edtNome;
                        if (!sNome) {
                            sNome = "Não Informado";
                        }

                        var agrupa = 0;
                        if ($scope.agrupaNumeros == true) {
                            agrupa = 1;
                        }

                        $rootScope.loading = true;
                        var apostas = [];
                        for (var i = 0; i < $scope.edtQtdBilhetes; i++) {
                            apostas.push({
                                numeros: '',
                                valorReal: $scope.listaCfg[0].valor_aposta,
                                qtd: "1",
                                tipo: "2pra500",
                                tipoJogo: '',
                                habilitaSorte: '',
                                cod_jogo: $scope.comboConcurso
                            })
                        }
                        $http({
                            url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: "cod_usuario=" + user.getCodigo() +
                            "&cod_site=" + user.getSite() +
                            "&nome_usuario=" + user.getName() +
                            "&nome=" + sNome +
                            "&telefone=" + sTelefone +
                            "&registro_cliente=" + $scope.chkRegistroCliente +
                            "&apostas=" + JSON.stringify(apostas) +
                            "&data_atual=" + dataAtual +
                            "&operacao=" +
                            "&schema=" + user.getSchema() +
                            "&agrupaNumeros=" + agrupa
                        }).then(function (response) {
                            if (response.data.status == 'OK') {
                                user.atualizaSaldo(response.data.saldo);
                                user.setBilheteExterno(response.data.pule);
                                $location.path("/conferir_bilhete");
        
                            } else {
                                $rootScope.loading = false;
                                if (response.data.status == 'INATIVO') {
                                    $.alert(response.data.mensagem);
                                    $location.path("/logout");
                                }else {
                                    $.alert(response.data.mensagem);
                                }
                            } 
                        }, function (response) {
                            $rootScope.loading = false;
                            $.alert('Erro no $HTTP: ' + response.status)
                        });        
                    }
                }
            });
        } finally {
            
        }
    }
});
