<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
	die();
}

$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$perfil_usuario = mysqli_real_escape_string($con, $_POST['perfil_usuario']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$index = mysqli_real_escape_string($con, $_POST['index']);
$qtdElementos = mysqli_real_escape_string($con, $_POST['qtdElementos']);
$sort = mysqli_real_escape_string($con, $_POST['sort']);
$sortType = filter_var(mysqli_real_escape_string($con, $_POST['sortType']), FILTER_VALIDATE_BOOLEAN);
$concurso = mysqli_real_escape_string($con, $_POST['concurso']);
$vendedor = mysqli_real_escape_string($con, $_POST['vendedor']);
$tipoJogo = mysqli_real_escape_string($con, $_POST['tipoJogo']);
$statusPgto = mysqli_real_escape_string($con, $_POST['pago']);
$dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);

$response = [];

$status = 'P';

if ($finalizados == 'true') {
	$status =  implode("','", array('P', 'F'));
}

$query = "
select b.`TXT_PULE`, DATE_FORMAT(b.`DATA_BILHETE`, '%d/%m/%Y %H:%i:%S') DATA_HORA_BILHETE, b.`STATUS_PAGAMENTO`,
	   b2.VALOR_GANHO,
	   j.`CONCURSO`, j.`TIPO_JOGO`, j.`HORA_EXTRACAO`, j.`DESC_HORA`, j.VALOR, j.DESCRICAO,
	   c.`NOME` as `CAMBISTA`, g.`NOME` as `GERENTE`
from bilhete b
inner join usuario c on (b.`COD_USUARIO` = c.`COD_USUARIO` )
inner join usuario g on (c.`COD_GERENTE` = g.`COD_USUARIO` )
inner join 
(select b.`COD_BILHETE`, a.`COD_JOGO`, a.`COD_SITE`, sum(a.`VALOR_GANHO`) as VALOR_GANHO
from bilhete b
inner join aposta a on (b.`COD_BILHETE` = a.`COD_BILHETE` and b.`COD_SITE` = a.`COD_SITE` )
where b.`STATUS_BILHETE` IN ('$status') and a.`STATUS` = 'G' and a.`COD_SITE` = '$site'
group by b.`COD_BILHETE`, a.`COD_JOGO`, a.`COD_SITE`
) b2 on (b.`COD_BILHETE` = b2.`COD_BILHETE`)
inner join jogo j on (b2.`COD_JOGO` = j.`COD_JOGO` and b2.`COD_SITE` = j.`COD_SITE` )
WHERE 1=1";

if ($concurso) {
	$query = $query . " AND j.COD_JOGO = '$concurso' ";
}

if ($tipoJogo) {
	$query = $query . " AND j.TIPO_JOGO = '$tipoJogo' ";
}

if ($vendedor) {
	switch ($perfil_usuario) {
		case 'A':
			$query = $query . " AND (g.`COD_USUARIO` = '$vendedor') ";
			break;
		default:
			$query = $query . " AND (c.`COD_USUARIO` = '$vendedor') ";
			break;
	}
} else {
	switch ($perfil_usuario) {
		case 'G':
			$query = $query . " AND (g.`COD_USUARIO` = '$cod_usuario') ";
			break;
		case 'C':
			$query = $query . " AND (c.`COD_USUARIO` = '$cod_usuario') ";
			break;
		default:
			break;
	}
}

if ($statusPgto) {
	$query = $query . " AND (b.`STATUS_PAGAMENTO` = '$statusPgto') ";
}

if ($dataDe != '0') {
	$query = $query . " AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d')  ";
}

if ($dataAte != '0') {
	$query = $query . " AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d')  ";
}

if ($dataDe == 0 && $dataAte == 0) {
	$query = $query . " AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d')  >= DATE_FORMAT(current_date - 7, '%Y%m%d')
			   AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d')  <= DATE_FORMAT(current_date + 1, '%Y%m%d') ";
}
$queryCount = "select count(*) as total from (" . $query . ") t";
$totalItens = mysqli_query($con, $queryCount);

$asc_desc = $sortType ? " ASC" : " DESC";
switch ($sort) {
	case "CODIGO":
		$query = $query . "ORDER BY TXT_PULE";
		break;
	case "GERENTE":
		$query = $query . "ORDER BY GERENTE";
		break;
	case "CAMBISTA":
		$query = $query . "ORDER BY CAMBISTA";
		break;
	case "CONCURSO":
		$query = $query . "ORDER BY CONCURSO";
		break;
	case "TIPO_JOGO":
		$query = $query . "ORDER BY TIPO_JOGO";
		break;
	case "DATA":
		$query = $query . "ORDER BY STR_TO_DATE(DATA_HORA_BILHETE, '%d/%m/%Y %H:%i:%s')";
		break;
	case "RETORNO":
		$query = $query . "ORDER BY VALOR_GANHO";
		break;
	case "STATUS_PGTO":
		$query = $query . "ORDER BY case 
                        when STATUS_PAGAMENTO = 'N' then 1
                        else 0 end";
		break;
	default:
		$query = $query . "ORDER BY STR_TO_DATE(DATA_HORA_BILHETE, '%d/%m/%Y %H:%i:%s')";
		break;
}

$query = $query . $asc_desc . " LIMIT $index,$qtdElementos ";
$result = mysqli_query($con, $query);
$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
	$row_array['txt_pule'] = $row['TXT_PULE'];
	$row_array['data_bilhete'] = $row['DATA_HORA_BILHETE'];
	$row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
	$row_array['desc_hora'] = $row['DESC_HORA'];
	$row_array['valor'] = $row['VALOR'];
	$row_array['descricao'] = $row['DESCRICAO'];
	$row_array['cambista'] = $row['CAMBISTA'];
	$row_array['gerente'] = $row['GERENTE'];
	$row_array['concurso'] = $row['CONCURSO'];
	$row_array['tipo_jogo'] = $row['TIPO_JOGO'];
	$row_array['valor_ganho'] = $row['VALOR_GANHO'];
	$row_array['status_pagamento'] = $row['STATUS_PAGAMENTO'];
	array_push($return_arr, $row_array);
	if ($contador == mysqli_num_rows($result)) {
		break;
	}
};

$con->close();
$totalItensJSON{
	'totalItens'} = mysqli_fetch_array($totalItens)[0];
array_push($return_arr, $totalItensJSON);
echo json_encode($return_arr, JSON_NUMERIC_CHECK);
