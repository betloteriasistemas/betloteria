<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

session_start();

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$perfil = mysqli_real_escape_string($con, $_POST['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);
$dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);

if ($perfil == "C") {
    $query = "select v.*, (v.entradas - v.saidas - v.comissao)  saldo, 0 as saldo_par from (
    (select
    DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo,
    cast(case
    when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
    else jogo.concurso end as char) as concurso ,  TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora,
    jogo.tp_status, jogo.tipo_jogo,
    DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
    sum(apo.valor_aposta) entradas,
    count(apo.cod_aposta) qtd_apostas,
    (sum(apo.valor_aposta) * (CASE WHEN jogo.Tipo_Jogo = 'S' THEN usu.pct_comissao_seninha 
    WHEN jogo.Tipo_Jogo = 'Q' THEN  usu.pct_comissao_quininha 
    WHEN jogo.Tipo_Jogo in ('B','G') THEN  usu.pct_comissao_bicho 
    WHEN jogo.Tipo_Jogo = '2' THEN  usu.pct_comissao_2PRA500 
    WHEN jogo.Tipo_Jogo = '6' THEN  usu.pct_comissao_6DASORTE ELSE 0 END)) / 100 as comissao,
    0 comissao_gerente,
    sum(case when apo.status = 'G' then apo.possivel_retorno else 0 end) as saidas
    from jogo
    inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
    inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
    inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
    where jogo.cod_site = '$site'
      and usu.cod_usuario = '$cod_usuario'
      and bil.status_bilhete not in ('C','F')
      and apo.status not in ('C') ";

    

    $query = $query . " group by jogo.data_jogo, jogo.concurso, jogo.hora_extracao , jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo
                        ) union all
                        (select DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,'LANÇAMENTO' as concurso,null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo,
                        DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, (case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao, 0 as comissao_gerente, 
                        (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas
                         from lancamento L where cod_site = '$site' and cod_usuario_lancamento = '$cod_usuario' and ajuda_custo = 'N' and L.status = 'A')
                        ) v
                        where 1 =1 ";

                        if ($finalizados == 'false') {
                            $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
                        }
                    
                        if ($dataDe != '0') {
                            $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
                        }
                    
                        if ($dataAte != '0') {
                            $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
                        }
                        
                        $query = $query . " order by data_inteiro, data_jogo, concurso desc";

} else if ($perfil == "G") {
    $query = "
    select g.*, g.saldo_par - g.comissao_gerente as saldo from (
    (select v.*, (v.entradas - v.saidas - v.comissao) saldo_par,
    round(((select (CASE WHEN v.Tipo_Jogo = 'S' THEN pct_comissao_seninha 
    WHEN v.Tipo_Jogo = 'Q' THEN  pct_comissao_quininha 
    WHEN v.Tipo_Jogo in ('B','G') THEN pct_comissao_bicho 
    WHEN v.Tipo_Jogo = '2' THEN  pct_comissao_2PRA500 
    WHEN v.Tipo_Jogo = '6' THEN  pct_comissao_6DASORTE ELSE 0 END) from usuario where cod_usuario = '$cod_usuario') * (v.entradas - v.saidas - v.comissao)) / 100 ,2) comissao_gerente
    from (
        select usu.cod_usuario, usu.nome, DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
        when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
        else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo,
        DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
        sum(apo.valor_aposta) entradas,
        count(apo.cod_aposta) qtd_apostas,
        (sum(apo.valor_aposta) * (CASE WHEN jogo.Tipo_Jogo = 'S' THEN usu.pct_comissao_seninha 
        WHEN jogo.Tipo_Jogo = 'Q' THEN  usu.pct_comissao_quininha 
        WHEN jogo.Tipo_Jogo in ('B','G') THEN  usu.pct_comissao_bicho 
        WHEN jogo.Tipo_Jogo = '2' THEN  usu.pct_comissao_2PRA500 
        WHEN jogo.Tipo_Jogo = '6' THEN  usu.pct_comissao_6DASORTE ELSE 0 END)) / 100 as comissao,
        sum(case when apo.status = 'G' then apo.possivel_retorno else 0 end) as saidas
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        where jogo.cod_site = '$site'
          and usu.cod_gerente = '$cod_usuario'
          and bil.status_bilhete not in ('C','F')
          and apo.status not in ('C') ";

   
    $query = $query . " group by usu.cod_usuario, usu.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora, jogo.tp_status, jogo.tipo_jogo
    ) v )
    union all
    (select U.cod_usuario, U.nome, DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,'LANÇAMENTO' as concurso, null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo,
    DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, (case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao, 
    (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas, ((case when tipo_lancamento = 'S' then (valor*-1) else 0 end)  + (case when tipo_lancamento = 'E' then (valor) else 0 end)) as saldo_par,
    0 as comissao_gerente
    from lancamento L,usuario U where L.COD_USUARIO_lancamento = U.cod_usuario and L.cod_site = '$site' and L.cod_usuario_lancamento = '$cod_usuario' and L.ajuda_custo = 'N' and L.status='A')
    union all 
    (select U.cod_usuario, U.nome, DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,'LANÇAMENTO' as concurso, null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo,
    DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, (case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao,  
    (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas, ((case when tipo_lancamento = 'S' then (valor*-1) else 0 end)  + (case when tipo_lancamento = 'E' then (valor) else 0 end)) as saldo_par, 
    0 as comissao_gerente
    from lancamento L,usuario U where L.COD_USUARIO_lancamento = U.cod_usuario and L.cod_site = '$site' and L.COD_USUARIO_lancamento in (select cod_usuario from usuario where cod_gerente = '$cod_usuario') and L.ajuda_custo = 'N' and L.status='A')
    ) g where 1 =1 ";
    
    if ($finalizados == 'false') {
        $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
    }

    if ($dataDe != '0') {
        $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
    }
    
    
    $query = $query . " order by data_inteiro, data_jogo, nome, concurso desc ";





} else if ($perfil == "A") {
    $query = "
    select cod_usuario,nome,data_jogo,concurso,hora_extracao,desc_hora,tp_status,tipo_jogo,data_inteiro, sum(entradas) as entradas,
    sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
    round((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))) /100,2) as comissao_gerente,
    (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
    ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - ((v.pct_comissao * (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)))/100)) as saldo from (
        (
        select usu.cod_usuario as cod_cambista, geren.cod_usuario, geren.nome,  DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, cast(case
        when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
        else jogo.concurso end as char) as concurso, TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, jogo.desc_hora, jogo.tp_status,  jogo.tipo_jogo, (CASE WHEN jogo.Tipo_Jogo = 'S' THEN geren.pct_comissao_seninha 
        WHEN jogo.Tipo_Jogo = 'Q' THEN  geren.pct_comissao_quininha 
        WHEN jogo.Tipo_Jogo in ('B','G') THEN  geren.pct_comissao_bicho 
        WHEN jogo.Tipo_Jogo = '2' THEN  geren.pct_comissao_2PRA500 
        WHEN jogo.Tipo_Jogo = '6' THEN  geren.pct_comissao_6DASORTE ELSE 0 END) as pct_comissao,
        DATE_FORMAT(jogo.data_jogo, '%Y%m%d') as data_inteiro,
        sum(apo.valor_aposta) entradas,
        count(apo.cod_aposta) qtd_apostas,
        (sum(apo.valor_aposta) * (CASE WHEN jogo.Tipo_Jogo = 'S' THEN usu.pct_comissao_seninha 
        WHEN jogo.Tipo_Jogo = 'Q' THEN  usu.pct_comissao_quininha 
        WHEN jogo.Tipo_Jogo in ('B','G') THEN  usu.pct_comissao_bicho 
        WHEN jogo.Tipo_Jogo = '2' THEN  usu.pct_comissao_2PRA500 
        WHEN jogo.Tipo_Jogo = '6' THEN  usu.pct_comissao_6DASORTE ELSE 0 END)) / 100 as comissao,
        sum(case when apo.status = 'G' then apo.possivel_retorno else 0 end) as saidas
        from jogo
        inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
        inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
        inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
        inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
        where jogo.cod_site = '$site'
        and geren.cod_gerente = '$cod_usuario'
        and bil.status_bilhete not in ('C','F')
        and apo.status not in ('C') ";
    
    $query = $query . " group by usu.cod_usuario, geren.cod_usuario, geren.nome, jogo.data_jogo, jogo.concurso, jogo.hora_extracao, jogo.desc_hora,jogo.tp_status, jogo.tipo_jogo) 
    union all
    (select null as cod_cambista, U.cod_usuario, U.nome, DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,'LANÇAMENTO' as concurso, null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo, 0 as pct_comissao,
    DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, (case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao, 
    (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas
    from lancamento L,usuario U where L.COD_USUARIO_lancamento = U.cod_usuario and L.cod_site = '$site' and L.COD_USUARIO_lancamento in (select cod_usuario from usuario where cod_gerente = '$cod_usuario') and L.ajuda_custo = 'N' and L.status='A')
    union all 
    (select null as cod_cambista, U.cod_usuario, U.nome, DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,'LANÇAMENTO' as concurso, null as hora_extracao, L.motivo as desc_hora, null as TP_STATUS, null as tipo_jogo, 0 as pct_comissao,
    DATE_FORMAT(L.data_hora, '%Y%m%d') as data_inteiro, (case when tipo_lancamento = 'E' then (valor) else 0 end) as entradas, 0 as qtd_apostas, 0 as comissao,  
    (case when tipo_lancamento = 'S' then (valor) else 0 end) as saidas
    from lancamento L,usuario U where L.COD_USUARIO_lancamento = U.cod_usuario and L.cod_site = '$site' and L.COD_USUARIO_lancamento in (select U1.cod_usuario from usuario U1, usuario U2 where U1.cod_gerente = U2.cod_usuario and U2.cod_gerente = '$cod_usuario') and L.ajuda_custo = 'N' and L.status='A')
    ) v
    where 1 =1 ";
    
    if ($finalizados == 'false') {
        $query = $query . " AND (TP_STATUS <> 'F' OR TP_STATUS IS NULL) ";
    }

    if ($dataDe != '0') {
        $query = $query . " AND data_inteiro >= DATE_FORMAT('$dataDe', '%Y%m%d') ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND data_inteiro <= DATE_FORMAT('$dataAte', '%Y%m%d') ";
    }
    
    $query = $query . " group by cod_usuario,nome,data_jogo, concurso, hora_extracao, desc_hora,tp_status, tipo_jogo,data_inteiro order by data_inteiro, data_jogo, nome, concurso desc";
}

/*try {
$status = mysqli_real_escape_string($con, $_POST['status']);

if ($status != "") {
$query = $query . " AND TP_STATUS = '$status'";
}
} catch (Exception $e) {

}*/

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    if ($perfil != "C") {
        $row_array['cod_usuario'] = $row['cod_usuario'];
        $row_array['nome'] = $row['nome'];
    }
    $row_array['saldo'] = $row['saldo'];
    $row_array['concurso'] = $row['concurso'];
    $row_array['data_jogo'] = $row['data_jogo'];
    $row_array['tp_status'] = $row['tp_status'];
    $row_array['tipo_jogo'] = $row['tipo_jogo'];
    $row_array['entradas'] = $row['entradas'];
    $row_array['qtd_apostas'] = $row['qtd_apostas'];

    $row_array['comissao'] = $row['comissao'];
    $row_array['saidas'] = $row['saidas'];
    $row_array['comissao_gerente'] = $row['comissao_gerente'];
    $row_array['saldo_par'] = $row['saldo_par'];
    $row_array['data_inteiro'] = $row['data_inteiro'];

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}
;

echo json_encode($return_arr);
