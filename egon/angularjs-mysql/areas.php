<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$site = mysqli_real_escape_string($con, $_POST['site']);

if ($operacao == "listarAtivos") {
    $query = "select areas.cod_area, areas.nome, areas.descricao, areas.padrao, 
                     cfg.flg_seninha, cfg.flg_quininha, cfg.flg_lotinha, cfg.flg_bicho, cfg.flg_2pra500, 
                     cfg.flg_rifa, cfg.flg_super_Sena
              from areas
              inner join configuracao_geral cfg on areas.cod_area = cfg.cod_area
              where areas.cod_site = '$site' and areas.status = 'A'";

    $result = mysqli_query($con, $query);
    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_area'] = $row['cod_area'];
        $row_array['nome'] = $row['nome'];
        $row_array['descricao'] = $row['descricao'];
        $row_array['flg_quininha'] = $row['flg_quininha'];
        $row_array['flg_lotinha'] = $row['flg_lotinha'];
        $row_array['flg_seninha'] = $row['flg_seninha'];
        $row_array['flg_bicho'] = $row['flg_bicho'];
        $row_array['flg_2pra500'] = $row['flg_2pra500'];
        $row_array['flg_rifa'] = $row['flg_rifa'];
        $row_array['flg_supersena'] = $row['flg_super_Sena'];
        $row_array['padrao'] = $row['padrao'];
        array_push($return_arr, $row_array);
    };
    echo json_encode($return_arr);
} else if ($operacao == "listar") {
    $query = "select  cod_area, nome, descricao, padrao, status
              from areas
              where cod_site = '$site'";

    $result = mysqli_query($con, $query);
    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_area'] = $row['cod_area'];
        $row_array['nome'] = $row['nome'];
        $row_array['descricao'] = $row['descricao'];
        $row_array['padrao'] = $row['padrao'];
        $row_array['status'] = $row['status'];
        array_push($return_arr, $row_array);
    };
    echo json_encode($return_arr);
} else if ($operacao == "inserir") {
    $con->begin_transaction();
    try {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $nome = mysqli_real_escape_string($con, $_POST['nome']);

        $query = "select  nome
        from areas
        where cod_site = '$site' and nome = '$nome'";
        $result = mysqli_query($con, $query);

        if (mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            throw new Exception('Já existe uma área com este nome.');
        } else {

            $codigo = mysqli_real_escape_string($con, $_POST['codigo']);

            if ($codigo == 0) {

                $descricao = mysqli_real_escape_string($con, $_POST['descricao']);
                $result = mysqli_query($con, $query);
                $return_arr = array();

                $stmt = $con->prepare("insert into areas (cod_site, nome, descricao) VALUES (?, ?, ?)");
                $stmt->bind_param("iss", $site, $nome, $descricao);
                $stmt->execute();
                inserir_auditoria($con, $cod_usuario, $site, AUD_AREA_CRIADA, descreverCriacaoArea($nome, $descricao));

                $query = "select  max(cod_area) as cod_area
                            from areas
                            where cod_site = '$site' 
                                and status = 'A' 
                                and nome = '$nome' 
                                and descricao = '$descricao'";
                $result = mysqli_query($con, $query);
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $cod_area = $row['cod_area'];

                /* CONFIGURACAO GERAL */
                $query = "INSERT INTO configuracao_geral (cod_site, cod_area, flg_bicho, flg_quininha, flg_lotinha, flg_seninha, 
                minutos_cancelamento, flg_2pra500, flg_rifa, valor_min_aposta, valor_max_aposta, 
                nu_bilhete_negrito, nu_bilhete_grande, nu_bilhete_sublinhado)
                VALUES($site, $cod_area, 'S', 'S', 'S', 'S', 10, 'S', 'S', NULL, NULL, 'N', 'N', 'N')";
                $con->query($query);
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_GERAL_CRIADA,
                    descreverCriacaoConfiguracaoGeral(
                        $nome,
                        'S',
                        'S',
                        'S',
                        'S',
                        'S',
                        'S',
                        10, 
                        NULL,
                        NULL,
                        'N',
                        'N',
                        'N'
                    )
                );

                /* CONFIGURACAO SENINHA */
                $query = " INSERT INTO configuracao(`COD_SITE`,`PREMIO_MAXIMO`,`PREMIO_14`,`PREMIO_15`,
    `PREMIO_16`,`PREMIO_17`,`PREMIO_18`,`PREMIO_19`,`PREMIO_20`,`PREMIO_25`,`PREMIO_30`,
    `PREMIO_35`,`PREMIO_40`, `MSG_BILHETE`, `SENA_SORTE`, 
    `QUINA_SORTE`, `QUADRA_SORTE`, `COD_AREA`)
        VALUES ($site, 30000, 6500, 4000, 2500, 1600, 1050, 720, 500, 110, 33, 12, 5, 
        'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.',
        70, 3.40, 0.30, $cod_area) ";
                $con->query($query);
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_SENINHA_CRIADA,
                    descreverCriacaoConfiguracaoSeninha(
                        $nome,
                        30000,
                        6500,
                        4000,
                        2500,
                        1600,
                        1050,
                        720,
                        500,
                        110,
                        33,
                        12,
                        5,
                        'S',
                        70,
                        3.40,
                        0.30,
                        "Apostando aqui você estará aceitando o nosso regulamento que consta no site. 
                        Não pagaremos prêmios sem apresentação de bilhete. 
                        Participantes terão que ser maiores de 18 anos de idade."
                    )
                );
                /* CONFIGURACAO_QUININHA*/
                $query = " INSERT INTO configuracao_quininha(`COD_SITE`,`PREMIO_MAXIMO`,`PREMIO_13`, 
                    `PREMIO_14`,`PREMIO_15`,`PREMIO_16`,`PREMIO_17`,`PREMIO_18`,`PREMIO_19`,`PREMIO_20`,`PREMIO_25`,
                    `PREMIO_30`,`PREMIO_35`,`PREMIO_40`, `PREMIO_45`, `MSG_BILHETE`, `QUINA_SORTE`, `QUADRA_SORTE`, 
                    `TERNO_SORTE`, `COD_AREA`)
                    VALUES ($site, 30000, 7000, 4800, 3000, 2200, 1500, 1100, 800, 600, 180, 65, 29, 14, 7, 
                        'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.',
                        70, 2.79, 0.21, $cod_area) ";
                $con->query($query);
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_QUININHA_CRIADA,
                    descreverCriacaoConfiguracaoQuininha(
                        $nome,
                        30000,
                        7000,
                        4800,
                        3000,
                        2200,
                        1500,
                        1100,
                        800,
                        600,
                        180,
                        65,
                        29,
                        14,
                        7,
                        'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.',
                        'S',
                        70,
                        2.79,
                        0.21
                    )
                );

                /* CONFIGURACAO_LOTINHA*/
                $query = " INSERT INTO configuracao_lotinha(`COD_SITE`,`PREMIO_MAXIMO`,
                            `PREMIO_16`,`PREMIO_17`,`PREMIO_18`,`PREMIO_19`,`PREMIO_20`,`PREMIO_21`,
                            `PREMIO_22`,`MSG_BILHETE`, `COD_AREA`)
                            VALUES ($site, 30000, 500, 200, 100, 50, 25, 15, 8, 
                                    'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.',
                                     $cod_area) ";
                            $con->query($query);
                            inserir_auditoria(
                                $con,
                                $cod_usuario,
                                $site,
                                AUD_CONFIG_LOTINHA_CRIADA,
                                descreverCriacaoConfiguracaoLotinha(
                                    $nome,
                                    30000,
                                    500,
                                    200,
                                    100,
                                    50,
                                    25,
                                    15,
                                    8,
                                    'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.'
                                )
                );            

                /* CONFIGURACAO BIXO */
                $query = " INSERT INTO `configuracao_bicho`(cod_site, 
                                premio_milhar_centena, 
                                premio_milhar_seca, 
                                premio_milhar_invertida, 
                                premio_centena,
                                premio_centena_invertida, 
                                premio_grupo, 
                                premio_duque_grupo,
                                premio_duque_grupo6, 
                                premio_terno_grupo, 
                                premio_terno_grupo10, 
                                premio_terno_grupo6,
                                premio_terno_sequencia,
                                premio_terno_soma,
                                premio_terno_especial,
                                premio_quina_grupo, 
                                premio_dezena, 
                                premio_duque_dezena,
                                premio_duque_dezena6, 
                                premio_terno_dezena,
                                premio_terno_dezena10,
                                premio_terno_dezena6, 
                                premio_passe_seco, 
                                premio_passe_combinado, 
                                MSG_BILHETE, 
                                USA_MILHAR_BRINDE, 
                                VALOR_LIBERAR_MILHAR_BRINDE, 
                                VALOR_RETORNO_MILHAR_BRINDE, 
                                COD_AREA)
                            values($site, 4600, 4000, 4000, 600, 600, 18, 18.75, 18.75, 130, 10, 130, 80, 80, 100, 20, 60, 300, 300, 3000, 200, 3000, 18, 90,
                                'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.', 
                                'N', 10, 500, $cod_area) ";
                $con->query($query);
                                
                $codConfiguracao = $con->insert_id;
                $queryTipoJogoBicho = "INSERT INTO configuracao_tipo_jogo_bicho(COD_CONFIGURACAO, COD_TIPO_JOGO_BICHO)
                                       SELECT $codConfiguracao, COD_TIPO_JOGO_BICHO 
                                       FROM tipo_jogo_bicho tjb 
                                       ORDER BY COD_TIPO_JOGO_BICHO";
                $con->query($queryTipoJogoBicho);

                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_BICHO_CRIADA,
                    descreverCriacaoConfiguracaoBicho(
                        $nome,
                        4600,
                        4000,
                        4000,
                        600,
                        600,
                        18,
                        18.75,
                        18.75,
                        130,
                        10,
                        130,
                        80,
                        80,
                        100,
                        20,
                        60,
                        300,
                        300,
                        3000,
                        200,
                        3000,
                        18,
                        90,
                        'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.',
                        'N',
                        10,
                        500
                    )
                );

                /* CONFIGURACAO 2 PRA 500 */
                $query = "INSERT INTO `configuracao_2pra500`
                            (cod_site, valor_acumulado, MSG_BILHETE, VALOR_APOSTA, COD_AREA)
                            values($site, 
                                    500, 
                                    'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.', 
                                    2, 
                                    $cod_area)";
                $con->query($query);
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_2PRA500_CRIADA,
                    descreverCriacaoConfiguracao2pra500($nome, 500, 'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.', 2)
                );

                /* CONFIGURACAO RIFA */
                $query = "INSERT INTO `configuracao_rifa`
                            (cod_site, COD_AREA, QTD_RIFAS, MSG_BILHETE)
                            values($site, $cod_area, 1000,
                            'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.')";
                    $con->query($query);
                    inserir_auditoria(
                        $con,
                        $cod_usuario,
                        $site,
                        AUD_CONFIG_RIFA_CRIADA,
                        descreverCriacaoConfiguracaoRifa($nome, 1000, 'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.')
                    );

                /* CONFIGURACAO_MULTIJOGOS*/
                $query = " INSERT INTO configuracao_multijogos 
                                (COD_SITE, COD_AREA, VALOR_CARTAO_1, VALOR_CARTAO_2, VALOR_CARTAO_3, 
                                VALOR_CARTAO_4, VALOR_CARTAO_5, QTD_NUMEROS_SENINHA, QTD_NUMEROS_QUININHA, 
                                QTD_NUMEROS_LOTINHA, TIPOS_JOGOS_BICHO, VALOR_MINIMO_APOSTA, CONCURSOS_MARCADOS)  
                            VALUES ($site, $cod_area, 10, 20, 40, 60, 100, '14', '13', '16', 'MS-MC-C', 1.50, 'R') ";
                $con->query($query);
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_MULTIJOGOS_CRIADA,
                    descreverCriacaoConfiguracaoMultijogos(
                        $nome,
                        10,
                        20,
                        40,
                        60,
                        100,
                        1.50,
                        '14',
                        '13',
                        '16',
                        'MS-MC-C',
                        'R'
                    )
                );          
                
                /* CONFIGURACAO SUPER SENA */
                $query = " INSERT INTO configuracao_super_sena(COD_SITE,PREMIO_MAXIMO,PREMIO_SENA,
                                PREMIO_QUINA,PREMIO_QUADRA,MSG_BILHETE,COD_AREA)
                            VALUES ($site, 30000, 6500, 221, 19.5, 
                                'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.',
                                $cod_area) ";
                $con->query($query);
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $site,
                    AUD_CONFIG_SUPERSENA_CRIADA,
                    descreverCriacaoConfiguracaoSuperSena(
						$nome,
						30000,
						6500,
						221,
						19.5,
						'Apostando aqui você estará aceitando o nosso regulamento que consta no site. Não pagaremos prêmios sem apresentação de bilhete. Participantes terão que ser maiores de 18 anos de idade.'
					)
                );                

                /* CADASTRA TODAS AS EXTRACOES DA AREA PADRAO */
                $query =
                    "SELECT COD_EXTRACAO
                FROM extracao_bicho 
                WHERE STATUS = 'A' AND COD_SITE = '$site' ";

                $result = mysqli_query($con, $query);

                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $cod_extracao = $row['COD_EXTRACAO'];
                    $query = " INSERT INTO `area_extracao`
                        (cod_site, cod_area, cod_extracao, status)
                        values($site,
                                $cod_area,
                                $cod_extracao,
                                'A') ";
                    $con->query($query);
                }

                $stmt->close();
            } else {
                $descricao = mysqli_real_escape_string($con, $_POST['descricao']);
                $queryOldArea = "SELECT * FROM areas WHERE cod_area = '$codigo'";
                $stmtOldArea = $con->prepare($queryOldArea);
                $stmtOldArea->execute();
                $result = $stmtOldArea->get_result();
                $oldArea = $result->fetch_assoc();

                $stmt = $con->prepare("update areas set nome = ?, descricao = ? where cod_site = ? and cod_area = ?");
                $stmt->bind_param("ssii", $nome, $descricao, $site, $codigo);
                $stmt->execute();
                inserir_auditoria($con, $cod_usuario, $site, AUD_AREA_EDITADA, descreverEdicaoArea($oldArea, $codigo, $nome, $descricao));

                $stmt->close();
            }
        }
        $con->commit();
        $response['status'] = "OK";
    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }
    echo json_encode($response);
} else if ($operacao == "excluir") {

    try {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $cod_area = mysqli_real_escape_string($con, $_POST['cod_area']);
        $nome_area = mysqli_real_escape_string($con, $_POST['nome_area']);
        $status = mysqli_real_escape_string($con, $_POST['status']);

        $query = "select  nome
                  from areas
                  where cod_area = '$cod_area' and padrao = 'S'";
        $result = mysqli_query($con, $query);

        if (mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $response['mensagem'] = "Não é possível excluir a área da configuração padrão.";
        } else {

            if ($status == "A") {

                $query = "update usuario 
                            SET cod_area = (select cod_area 
                                            from areas 
                                            where cod_site = '$site' 
                                                and padrao = 'S') 
                            WHERE cod_area = '$cod_area'
                                and cod_site = '$site'";
                $con->query($query);

                $query = "update area_extracao 
                            set status = 'I' 
                            WHERE cod_area = '$cod_area'
                                and cod_site = '$site'";
                $con->query($query);

                $stmt = $con->prepare("update areas SET status = 'I' WHERE cod_area = ?");
            } else {

                $query = "update area_extracao 
                            set status = 'A' 
                            WHERE cod_area = '$cod_area'
                                and cod_site = '$site'";
                $con->query($query);

                $stmt = $con->prepare("update areas SET status = 'A' WHERE cod_area = ?");
            }
            $stmt->bind_param("i", $cod_area);
            $stmt->execute();
            inserir_auditoria(
                $con,
                $cod_usuario,
                $site,
                $status == "A" ? AUD_USUARIO_DESATIVADO : AUD_USUARIO_ATIVADO,
                descreverDesativacaoArea($nome_area, $status)
            );
            $stmt->close();
            $response['status'] = "OK";
        }
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }
    echo json_encode($response);
}
$con->close();
