<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);

$query = 'SET @@session.time_zone = "-03:00"';
$result = mysqli_query($con, $query);


$query = "SELECT * FROM jogo
        WHERE COD_SITE = '$site'";

if ($finalizados == 'false'){
    $query = $query . " AND TP_STATUS <> 'F' ";
}           

try {
    $status = mysqli_real_escape_string($con, $_POST['status']);

    if ($status != "") {
        $query = $query . " AND TP_STATUS = '$status'";
    }
} catch (Exception $e) {

}

try {
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);

    $tipoJogo = "";
    switch ($operacao) {
        case "seninha":
            $tipoJogo = "S";
        break;
        case "supersena":
            $tipoJogo = "U";
        break;
        case "quininha":
            $tipoJogo = "Q";
        break;
        case "lotinha":
            $tipoJogo = "L";
        break;
        case "bicho":
            $tipoJogo = "B";
            $query = $query . " AND DATA_JOGO >= CURRENT_DATE AND HORA_EXTRACAO > CURRENT_TIME ";
        break;
    }

    if ($tipoJogo != "") {
        $query = $query . " AND TIPO_JOGO = '$tipoJogo' ";
    }
} catch (Exception $e) {

}

$result = mysqli_query($con, $query);
$return_arr = array();
$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    $row_array['cod_jogo'] = $row['COD_JOGO'];
    $row_array['data_jogo'] = $row['DATA_JOGO'];
    $row_array['tp_status'] = $row['TP_STATUS'];
    $row_array['tipo_jogo'] = $row['TIPO_JOGO'];
    $row_array['concurso'] = $row['CONCURSO'];    
    $row_array['hora_extracao'] = $row['HORA_EXTRACAO'];   
    $row_array['selecionado'] = false;
    if ($contador == 1) {
        $row_array['selecionado'] = true;
    } 

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
};

echo json_encode($return_arr, JSON_NUMERIC_CHECK);
