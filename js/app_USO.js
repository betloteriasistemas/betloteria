var app = angular.module('main', ['ngRoute', 'ja.qr']);

app.config([
	'$compileProvider',
	function ($compileProvider) {
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|http|com.mitsoftwar.appprinter):/);
		// Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
	}
]);


app.config(function ($routeProvider, $locationProvider) {

	var verificaUsuarioLogado = function ($location, $q, user) {
		var deferred = $q.defer();
		if (user.isUserLoggedIn()) {
			deferred.resolve();
		} else {
			deferred.reject();
			$location.url('/login');
		}
		return deferred.promise;
	}

	$routeProvider.when('/', {
		resolve: {
			check: function ($location, user) {
				if (user.isUserLoggedIn()) {
					$location.path('/caixa');
				}
			},
		},
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	}).when('/logout', {
		resolve: {
			deadResolve: function ($location, user) {
				var aux = user.getEscolhido();
				var siteAux = user.getSite();
				user.clearData();
				if (aux == 'true'){
					$location.path('/' + siteAux);
				}else{
					$location.path('/');
				}
				
			}
		}
	}).when('/login', {
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	}).when('/usuario', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() == "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/usuario.html',
		controller: 'usuarioCtrl'
	}).when('/lancamento', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() == "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/lancamento.html',
		controller: 'lancamentoCtrl'
	}).when('/configuracao', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/configuracao.html',
		controller: 'configuracaoCtrl'
	}).when('/dashboard', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/dashboard.html',
		controller: 'dashboardCtrl'
	}).when('/gerenciar_jogos', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() == "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/gerenciar_jogos.html',
		controller: 'gerenciarJogosCtrl'
	}).when('/seninha', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/seninha.html',
		controller: 'seninhaCtrl'
	}).when('/quininha', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/quininha.html',
		controller: 'quininhaCtrl'
	}).when('/ver_bilhetes', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/ver_bilhetes.html',
		controller: 'verBilhetesCtrl'
	}).when('/conferir_bilhete', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/conferir_bilhete.html',
		controller: 'conferirBilheteCtrl'
	}).when('/imprime_bilhete', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/conferir_bilhete.html',
		controller: 'conferirBilheteCtrl'
	}).when('/regulamento', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/regulamento.html',
		controller: 'dashboardCtrl'
	}).when('/fale_conosco', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/fale_conosco.html',
		controller: 'dashboardCtrl'
	}).when('/downloads', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/downloads.html',
		controller: 'dashboardCtrl'
	}).when('/caixa', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/caixa.html',
		controller: 'caixaCtrl'
	}).when('/prestacaoConta', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/prestacaoConta.html',
		controller: 'prestacaoContaCtrl'
	}).when('/trocar_senha', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/trocar_senha.html',
		controller: 'trocarSenhaCtrl'
	}).when('/conferir_bilhete_externo', {
		templateUrl: './components/conferir_bilhete_externo.html',
		controller: 'conferirBilheteCtrl'
	}).when('/:id', {
		resolve: {
			check: function ($location, user) {
				if (user.isUserLoggedIn()) {
					$location.path('/caixa');
				}
			},
		},
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	})
		.otherwise({
			template: '404'
		});

	$locationProvider.html5Mode(false);
});

app.factory("bilhetesAPI", ['$http', 'user', function ($http, user) {

	var _getBilhetes = function (codigo) {
		var cod_bilhete = codigo;
		return $http({
			method: "POST",
			url: "angularjs-mysql/bilhetes.php",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: 'cod_usuario=' + user.getCodigo() + '&site=' + user.getSite() + '&cod_bilhete=' + cod_bilhete + '&operacao=conferirBilhete'
		}).then(function (response) {
			return response.data;
		}, function (response) {
			console.log(response);
			alert('Erro no $HTTP: ' + response.status)
		});
	};

	return {
		getBilhetes: _getBilhetes
	};

}]);



app.service('user', function () {
	var username;
	var nome;
	var loggedin = false;
	var id;
	var codigo;
	var site;
	var perfil;
	var saldo;
	var bilheteExterno = "";
	var escolhido;
	var area;

	this.getPIN = function () {
		return PIN;
	}

	this.setPIN = function (auxPIN) {
		this.PIN = auxPIN;
	}

	this.getBilheteExterno = function () {
		return bilheteExterno;
	}

	this.setBilheteExterno = function (auxBilheteExterno) {
		bilheteExterno = auxBilheteExterno;
	}

	this.getName = function () {
		return username;
	};

	this.getNome = function () {
		return nome;
	};

	this.setID = function (userID) {
		id = userID;
	};
	this.getID = function () {
		return id;
	};

	this.setSaldo = function (saldoAux) {
		this.saldo = saldoAux;
		saldo = this.saldo;
	};
	this.getSaldo = function () {
		return saldo;
	};

	this.getCodigo = function () {
		return codigo;
	}

	this.getSite = function () {
		return site;
	}

	this.getEscolhido = function () {
		return escolhido;
	}

	this.getPerfil = function () {
		return perfil;
	}

	this.getArea = function () {
		return area;
	}

	this.isUserLoggedIn = function () {
		if (!!localStorage.getItem('login')) {
			loggedin = true;
			var data = JSON.parse(localStorage.getItem('login'));
			username = data.username;
			id = data.id;
			nome = data.nome;
			codigo = data.codigo;
			site = data.site;
			escolhido = data.escolhido;
			perfil = data.perfil;
			saldo = data.saldo;
			area = data.area;
			//alert (site);
		}
		return loggedin;
	};

	this.atualizaSaldo = function (pSaldo) {
		localStorage.setItem('saldoAtualizado', JSON.stringify({
			saldo: pSaldo
		}));
	}

	this.getSaldoAtualizado = function () {
		var data = JSON.parse(localStorage.getItem('saldoAtualizado'));
		return data.saldo;
	}


	this.saveData = function (data) {
		username = data.user;
		id = data.id;
		nome = data.nome;
		loggedin = true;
		codigo = data.codigo;
		site = data.site;
		escolhido = data.escolhido;
		perfil = data.perfil;
		saldo = data.saldo;
		localStorage.setItem('saldoAtualizado', JSON.stringify({
			saldo: data.saldo
		}));

		localStorage.setItem('login', JSON.stringify({
			username: username,
			id: id,
			nome: nome,
			codigo: codigo,
			site: site,
			escolhido: escolhido,
			perfil: perfil,
			saldo: saldo
		}));
	};

	this.clearData = function () {
		localStorage.removeItem('login');
		localStorage.removeItem('saldoAtualizado');
		username = "";
		id = "";
		nome = "";
		loggedin = false;
		codigo = "";
		site = "";
		escolhido = "";
		perfil = "";
		saldo = 0;
		PIN = "";
	}


})


app.filter('object2Array', function () {
	return function (input) {
		return angular.fromJson(input);
	}
});