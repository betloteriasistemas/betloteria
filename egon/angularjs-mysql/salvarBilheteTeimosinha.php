<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

function obterNumerosRifa($return_arr)
{
    $numeroAleatorio = rand(0, 999);
    while (in_array($numeroAleatorio, $return_arr)) {
        $numeroAleatorio = rand(0, 999);
    }
    return $numeroAleatorio;
}

function obterNumeros2pra500($return_arr)
{
    $qtdNumeros = 1;
    $numerosGerados = '';
    while ($qtdNumeros <= 4) {
        $numeroAleatorio = str_pad(rand(0, 9999), 4, "0", STR_PAD_LEFT);
        while (in_array($numeroAleatorio, $return_arr)) {
            $numeroAleatorio = str_pad(rand(0, 9999), 4, "0", STR_PAD_LEFT);
        }    
        array_push($return_arr, $numeroAleatorio);
        if ($numerosGerados == '') {
            $numerosGerados = $numeroAleatorio;
        } else {
            $numerosGerados = $numerosGerados . '-' . $numeroAleatorio;
        }
        $qtdNumeros = $qtdNumeros + 1;
    }
    return $numerosGerados;
}

function getRetornoBicho($tipoJogoBicho, $config_usuario_arr) {
    switch ($tipoJogoBicho) {
        case "MS":
            return $config_usuario_arr['PREMIO_MILHAR_SECA'];
        case "MC":
            return $config_usuario_arr['PREMIO_MILHAR_SECA'] + $config_usuario_arr['PREMIO_CENTENA'];
        case "MI":
            return $config_usuario_arr['PREMIO_MILHAR_SECA'];
        case "MCI":
            return $config_usuario_arr['PREMIO_MILHAR_SECA'] + $config_usuario_arr['PREMIO_CENTENA'];
        case "C":
            return $config_usuario_arr['PREMIO_CENTENA'];
        case "CI":
            return $config_usuario_arr['PREMIO_CENTENA'];
        case "G":
            return $config_usuario_arr['PREMIO_GRUPO'];
        case "DG":
            return $config_usuario_arr['PREMIO_DUQUE_GRUPO'];
        case "DG6":
            return $config_usuario_arr['PREMIO_DUQUE_GRUPO6'];
        case "DGC":
            return $config_usuario_arr['PREMIO_DUQUE_GRUPO'];
        case "TG":
            return $config_usuario_arr['PREMIO_TERNO_GRUPO'];
        case "TG10":
            return $config_usuario_arr['PREMIO_TERNO_GRUPO10'];
        case "TG6":
            return $config_usuario_arr['PREMIO_TERNO_GRUPO6'];
        case "TGC":
            return $config_usuario_arr['PREMIO_TERNO_GRUPO'];
        case "TSE":
            return $config_usuario_arr['PREMIO_TERNO_SEQUENCIA'];
        case "TSO":
            return $config_usuario_arr['PREMIO_TERNO_SOMA'];
        case "TEX-A":
        case "TEX-F":
            return $config_usuario_arr['PREMIO_TERNO_ESPECIAL'];
        case "QG":
            return $config_usuario_arr['PREMIO_QUINA_GRUPO'];
        case "DZ":
            return $config_usuario_arr['PREMIO_DEZENA'];
        case "DDZ":
            return $config_usuario_arr['PREMIO_DUQUE_DEZENA'];
        case "DDZ6":
            return $config_usuario_arr['PREMIO_DUQUE_DEZENA6'];
        case "DDZC":
            return $config_usuario_arr['PREMIO_DUQUE_DEZENA'];
        case "TDZ":
            return $config_usuario_arr['PREMIO_TERNO_DEZENA'];
        case "TDZ10":
            return $config_usuario_arr['PREMIO_TERNO_DEZENA10'];
        case "TDZ6":
            return $config_usuario_arr['PREMIO_TERNO_DEZENA6'];
        case "TDZC":
            return $config_usuario_arr['PREMIO_TERNO_DEZENA'];
        case "PS":
            return $config_usuario_arr['PREMIO_PASSE_SECO'];
        case "PC":
            return $config_usuario_arr['PREMIO_PASSE_COMBINADO'];
        case "PS12":
            return $config_usuario_arr['PREMIO_PASSE_SECO'];
        case "5P100":
            return 20;
    }
}

function getDoPremio($aposta) {
    if (isset($aposta->premioDoReal)) {
        return $aposta->premioDoReal;
    } else {
        return 1;
    }
}

function getAoPremio($aposta) {
    if (isset($aposta->premioAoReal)) {
        return $aposta->premioAoReal;
    }
    switch ($aposta->tipoJogo) {
        case "MS":
        case "MC":
        case "MI":
        case "MCI":
        case "C":
        case "CI":
        case "G":
        case "DZ":
        case "5P100":
            return 1;
        case "PS12":
            return 2;
        case "DG":
        case "DGC":
        case "TG":
        case "TGC":
        case "QG": 
        case "DDZ":
        case "DDZC":
        case "TDZ":
        case "TDZC":
        case "PS":
        case "PC":
            return 5;
        case "TEX-A":
        case "TEX-F":
            return 7;
        case "DG6":
        case "DDZ6":
        case "TG10":
        case "TG6":
        case "TDZ10":
        case "TDZ6":
        case "TSE":
        case "TSO":
            return 10;
    }
}

function obterNumerosBicho($tipoJogoBicho) 
{    
    switch ($tipoJogoBicho) {
        case "MS":
        case "MC":
        case "MI":
        case "MCI":
            return str_pad(rand(0, 9999), 4, "0", STR_PAD_LEFT);
        case "C":
        case "CI":
            return str_pad(rand(0, 999), 3, "0", STR_PAD_LEFT);
        case "DZ":
            return str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT);
        case "DDZ":
        case "DDZ6":
            return str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT);        
        case "TDZ":
        case "TDZ10":
        case "TDZ6":
            return str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT);
        case "DDZC":
        case "TDZC":
            return str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 99), 2, "0", STR_PAD_LEFT);
        case "G":
        case "5P100":
            return str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT);
        case "DG":
        case "DG6":        
        case "DGC":
        case "PS":
        case "PC":
        case "PS12":        
            return str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT);
        case "TG":
        case "TG10":
        case "TG6":
        case "TSE":
        case "TSO":
        case "TEX-A":
        case "TEX-F":        
            return str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT);
        case "TGC":
            return str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT);
        case "QG":
            return str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . 
                   str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . 
                   str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT) . '-' . str_pad(rand(0, 25), 2, "0", STR_PAD_LEFT);
    }    
}

function calcularRetorno($tipoJogoBicho, $valor, $palpites, $premioDo, $premioAo, $config_usuario_arr, $conf_limite_bicho_arr) {
    $inversoes = 1;
    $retorno = 0;
    $qtdPremios = $premioAo - $premioDo + 1;
    $milhares = explode("-", $palpites);
    $qtdMilhares = count($milhares);
    switch ($tipoJogoBicho) {
        case "MS":
            if ($valor == 0) {
                $retorno = $config_usuario_arr['VALOR_RETORNO_MILHAR_BRINDE'];
            } else {
                $retorno = ($valor / ($qtdMilhares * $qtdPremios)) 
                    * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            }
            break;
        case "C":
        case "G":
        case "DZ":
        case "5P100":
            $retorno = ($valor / ($qtdMilhares * $qtdPremios)) 
                * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            break;
        case "MC":
            $retorno = (
                (getRetornoBicho('MS', $config_usuario_arr) * ($valor / 2)) +
                (getRetornoBicho('C', $config_usuario_arr) * ($valor / 2))
                )/($qtdMilhares * $qtdPremios);
            break;
        case "MI":
        case "CI":
            $inversoes = getQtdInversoes($tipoJogoBicho, $palpites);
            $retorno = ($valor / ($qtdPremios * $inversoes)) * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            break;
        case "MCI":
            $inversoes = getQtdInversoes($tipoJogoBicho, $palpites);
            $retorno = ($valor / ($qtdPremios * $inversoes)) * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            break;
        case "DG":
        case "DG6":
        case "TG":
        case "TG10":
        case "TG6":
        case "TSE":
        case "TSO":
        case "TEX-A":
        case "TEX-F":
        case "QG":
        case "DDZ":
        case "DDZ6":
        case "TDZ":
        case "TDZ10":
        case "TDZ6":
        case "PS":
        case "PS12":
        case "PC":
            $retorno = $valor * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            break;
        case "DGC":
        case "DDZC":
            $inversoes = getQtdInversoes($tipoJogoBicho, $palpites);
            $retorno = ($valor / $inversoes) * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            break;
        case "TGC":
        case "TDZC":
            $inversoes = getQtdInversoes($tipoJogoBicho, $palpites);
            $retorno = ($valor / $inversoes) * getRetornoBicho($tipoJogoBicho, $config_usuario_arr);
            break;
    }
    if ($conf_limite_bicho_arr != null && count($conf_limite_bicho_arr) > 0) {
        $iRetornoMaximo = getLimiteValorBicho($tipoJogoBicho, $conf_limite_bicho_arr);
        if ($retorno > $iRetornoMaximo && $iRetornoMaximo > 0) {
            $retorno = $iRetornoMaximo;
        }
    }
    $retornoArr = array();
    $retornoArr['inversoes'] = $inversoes;
    $retornoArr['retorno'] = $retorno;
    
    return $retornoArr;
}

function obterComissaoBicho(
    $tipo_aposta,
    $valor_aposta,
    $comissaoBicho
) {
    $comissao = 0;
    if (
        $tipo_aposta == "MC" && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "MS" && $comissaoBicho['COMISSAO_MILHAR_SECA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_SECA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_SECA'] / 100;
    } else if (($tipo_aposta == "MI" || $tipo_aposta == "MCI") &&
        $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] / 100;
    } else if (
        $tipo_aposta == "C" && $comissaoBicho['COMISSAO_CENTENA'] != null
        && $comissaoBicho['COMISSAO_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "CI" && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] / 100;
    } else if (
        ($tipo_aposta == "G") && $comissaoBicho['COMISSAO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_GRUPO'] / 100;
    } else if (($tipo_aposta == "DG" || $tipo_aposta == "DG6" || $tipo_aposta == "DGC") && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] != null
        && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_GRUPO'] / 100;
    } else if (($tipo_aposta == "TG" || $tipo_aposta == "TG10" || $tipo_aposta == "TG6" || 
                $tipo_aposta == "TGC" || $tipo_aposta == "TSE" || $tipo_aposta == "TSO" ||
                $tipo_aposta == "TEX-A" || $tipo_aposta == "TEX-F")
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_GRUPO'] / 100;
    } else if ($tipo_aposta == "QG" && $comissaoBicho['COMISSAO_QUINA_GRUPO'] != null 
        && $comissaoBicho['COMISSAO_QUINA_GRUPO'] > 0) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_QUINA_GRUPO'] / 100;
    } else if (
        $tipo_aposta == "DZ" && $comissaoBicho['COMISSAO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DEZENA'] / 100;
    } else if (($tipo_aposta == "DDZ" || $tipo_aposta == "DDZ6" || $tipo_aposta == "DDZC") &&  $comissaoBicho['COMISSAO_DUQUE_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DUQUE_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_DEZENA'] / 100;
    } else if (($tipo_aposta == "TDZ" || $tipo_aposta == "TDZ10" || $tipo_aposta == "TDZ6" || $tipo_aposta == "TDZC") &&
        $comissaoBicho['COMISSAO_TERNO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_TERNO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_DEZENA'] / 100;
    } else if (($tipo_aposta == "PS" || $tipo_aposta == "PS12") && $comissaoBicho['COMISSAO_PASSE_SECO'] != null
        && $comissaoBicho['COMISSAO_PASSE_SECO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_SECO'] / 100;
    } else if (
        $tipo_aposta == "PC" && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] != null
        && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_COMBINADO'] / 100;
    } else if (
        $tipo_aposta == "5P100" && $comissaoBicho['COMISSAO_5P100'] != null
        && $comissaoBicho['COMISSAO_5P100'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_5P100'] / 100;
    } else {
        $comissao = $valor_aposta * $comissaoBicho['PCT_COMISSAO_BICHO'] / 100;
    }
    return $comissao;
}

$response = [];
if (isset($_POST['operacao'])) {
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);
}
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$nome_usuario = mysqli_real_escape_string($con, $_POST['nome_usuario']);
$cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);

$auditoria = "";

try {
    $con->begin_transaction();
    if ($operacao === "atualizarStatusPagamento") {
        $txt_pule = mysqli_real_escape_string($con, $_POST['txt_pule']);
        $sqlUpdate = "UPDATE bilhete SET STATUS_PAGAMENTO = 'S' WHERE txt_pule = '$txt_pule'";
        if ($con->query($sqlUpdate) === true) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_BILHETE_PAGO,
                descreverPagamentoBilhete($txt_pule)
            );
            $response['status'] = "OK";
        } else {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
        }
    } else {

        $queryValida = " SELECT U.STATUS STATUS_USUARIO, S.STATUS STATUS_SITE, U.COD_GERENTE
                            from USUARIO U
                            INNER JOIN SITE S ON (S.COD_SITE = U.COD_SITE)
                            where U.COD_USUARIO = '$cod_usuario'";

        $resultValida = mysqli_query($con, $queryValida);
        $rowValida = mysqli_fetch_array($resultValida, MYSQLI_ASSOC);
        $cod_gerente = $rowValida['COD_GERENTE'];
        if ($rowValida['STATUS_USUARIO'] == "I" || $rowValida['STATUS_SITE'] == "I") {
            $response['status'] = "INATIVO";
            $response['mensagem'] = "Site ou cambista Inativo!";
        } else {

            $nome_apostador = mysqli_real_escape_string($con, $_POST['nome']);
            $telefone_apostador = mysqli_real_escape_string($con, $_POST['telefone']);
            $registro_cliente = mysqli_real_escape_string($con, $_POST['registro_cliente']); 

            $query = 'SET @@session.time_zone = "-03:00"';

            $result = mysqli_query($con, $query);

            $apostas = json_decode($_POST['apostas']);

            // Verifica se há mais de uma milhar brinde nas apostas do bicho.
            $temMaisDeUmaMilharBrinde = false;
            foreach ($apostas as $aposta) {
                if ($aposta->tipo == "Bicho" && $aposta->valorReal == 0) {
                    if ($temMaisDeUmaMilharBrinde) {
                        throw new Exception('Não é permitido mais de uma milhar Brinde!');
                    }
                    $temMaisDeUmaMilharBrinde = true;
                }
            }

            // Agrupa as apostas por concurso
            $jogos_apostas = array();
            // Agrupa as apostas da modalidade 2 pra 500
            $apostas_2pra500 = array();
            $numeros_gerados_2pra500 = array();

            foreach ($apostas as $aposta) {

                if ($aposta->valorReal < 0) {
                    throw new Exception('Não é permitido apostas com valor inferior a zero!');
                }

                if ($aposta->cod_jogo == "null") {
                    throw new Exception('Existe aposta sem concurso associado! É necessário removê-la!');
                }
                $aposta->valorReal = round($aposta->valorReal, 2);
                
                if ($aposta->tipo == "Rifa") {
                    $query = " select txt_aposta from aposta where cod_jogo = '$aposta->cod_jogo' and status <> 'C'	";  
                    $result = mysqli_query($con, $query);
                    $contador = 0;
                    $return_arr = array();
                    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                        $contador = $contador + 1;
		                $array = explode('-', str_replace(' ', '', $row['txt_aposta']));
                        foreach ($array as $value) {	
                            array_push($return_arr, $value);
                        }
		                if ($contador == mysqli_num_rows($result)) {
                            break;
		                }
                    }

                    if ($aposta->numeros == '') {                        
                        $aposta->numeros = obterNumerosRifa($return_arr);
                    } else {
                        $numeros_apostados = explode('-', str_replace(' ', '', $aposta->numeros));
                        $centenas_repetidas = "";
                        foreach($numeros_apostados as $numero) {
                            if (in_array($numero, $return_arr)) {
                                $centenas_repetidas = $centenas_repetidas . $numero . " ";
                            }
                        }
                        if (!empty($centenas_repetidas)) {
                            throw new Exception("Rifa " . $aposta->possivelRetorno . 
                            ": Centenas " . $centenas_repetidas . "já foram jogadas. Por favor recarregue a página.");
                        }    
                    }
                }

                if ($aposta->tipo == "Bicho" && $aposta->numeros == '') {
                    $aposta->numeros = obterNumerosBicho($aposta->tipoJogo);
                }

                if ($aposta->tipo == "2pra500") {
                    $query = " select txt_aposta from aposta where cod_jogo = '$aposta->cod_jogo' and status <> 'C'	";  
                    $result = mysqli_query($con, $query);
                    $contador = 0;
                    $return_arr = array();
                    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                        $contador = $contador + 1;
		                $array = explode('-', str_replace(' ', '', $row['txt_aposta']));
                        foreach ($array as $value) {	
                            array_push($return_arr, $value);
                        }
		                if ($contador == mysqli_num_rows($result)) {
                            break;
		                }
                    }
                    foreach ($numeros_gerados_2pra500 as $numero) {	
                        array_push($return_arr, $numero);
                    }
                    $numeros_gerados = obterNumeros2pra500($return_arr);
                    $array_nu_gerados = explode('-', str_replace(' ', '', $numeros_gerados));
                    foreach ($array_nu_gerados as $numero) {	
                        array_push($numeros_gerados_2pra500, $numero);
                    }
                    $aposta->numeros = $numeros_gerados;
                    array_push($apostas_2pra500, $aposta);

                } else {

                    if (array_key_exists($aposta->cod_jogo, $jogos_apostas)) {
                        array_push($jogos_apostas[$aposta->cod_jogo], $aposta);
                    } else {
                        $jogos_apostas[$aposta->cod_jogo] = array($aposta);
                    }    
                }               
            }        
            
            $pule = "";
            do {
                $pule = geraPule($cod_site);
                $query = "SELECT txt_pule 
                            FROM bilhete
                            WHERE txt_pule = '$pule' OR txt_pule_pai = '$pule'";
        
                $result = mysqli_query($con, $query);
            } while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC));
            

            $valorReal = 0;
            foreach ($jogos_apostas as $jogo_aposta) {               
                $stmt = $con->prepare("INSERT INTO bilhete (cod_site, 
                                                            cod_usuario,
                                                            nome_apostador,
                                                            telefone_apostador,
                                                            data_bilhete,
                                                            txt_pule_pai) 
                                            VALUES (?, ?, ?, ?, current_timestamp, ?) ");
                $stmt->bind_param("iisss", $cod_site, $cod_usuario, $nome_apostador, $telefone_apostador, $pule);
                $stmt->execute();
                $cod_bilhete = $con->insert_id;

                $stmt = $con->prepare(" UPDATE bilhete 
                                            SET numero_bilhete = ?,
                                                txt_pule = ?
                                        WHERE cod_bilhete = ? ");
                $numero_bilhete = $cod_site . '' . $cod_bilhete;
                
                $stmt->bind_param("isi", intval($numero_bilhete), intval($numero_bilhete), $cod_bilhete);
                $stmt->execute();

                $data_atual = mysqli_real_escape_string($con, $_POST['data_atual']);   

                $auditoria = descreverCriacaoBilhete(
                    $nome_apostador,
                    $telefone_apostador,
                    $data_atual,
                    $pule,
                    $cod_bilhete,
                    null
                ); 

                foreach ($jogo_aposta as $aposta) {

                    $cod_jogo = $aposta->cod_jogo;
        
                    $query = "SELECT TP_STATUS, HORA_EXTRACAO, DATA_JOGO, TIPO_JOGO, 
                                  CONCURSO AS COD_EXTRACAO, CURRENT_TIME AGORA, current_date HOJE 
                                  FROM jogo
                                  WHERE cod_jogo = '$cod_jogo'";
        
                    $result = mysqli_query($con, $query);
        
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    if ($row['TP_STATUS'] == 'B') {
                        throw new Exception('Concurso Bloqueado! Atualize a página e selecione um concurso válido!');
                    }
                    $tipo_jogo = $row['TIPO_JOGO'];
        
                    $config_usuario_arr = array();
                    $queryConf = "";
                    $config_gerente_arr = array();
                    $queryConfGerente = "";
                    
                    switch ($tipo_jogo) {
                        case 'S':
                            $queryConf = mysqli_query($con,
                                    "SELECT *                   
                                     FROM usuario usu 
                                     LEFT JOIN configuracao conf ON usu.cod_area = conf.cod_area
                                     WHERE usu.COD_SITE = '$cod_site' 
                                           AND usu.COD_USUARIO = '$cod_usuario'"
                            );
                            $queryConfGerente = mysqli_query($con,
                                    "SELECT *                   
                                    FROM usuario usu 
                                    LEFT JOIN configuracao conf ON usu.cod_area = conf.cod_area
                                    WHERE usu.COD_SITE = '$cod_site' 
                                        AND usu.COD_USUARIO = '$cod_gerente'"
                            );
                        break;
                        case 'U':
                            $queryConf = mysqli_query($con,
                                    "SELECT *                   
                                     FROM usuario usu 
                                     LEFT JOIN configuracao_super_sena conf ON usu.cod_area = conf.cod_area
                                     WHERE usu.COD_SITE = '$cod_site' 
                                           AND usu.COD_USUARIO = '$cod_usuario'"
                            );
                            $queryConfGerente = mysqli_query($con,
                                    "SELECT *                   
                                    FROM usuario usu 
                                    LEFT JOIN configuracao_super_sena conf ON usu.cod_area = conf.cod_area
                                    WHERE usu.COD_SITE = '$cod_site' 
                                        AND usu.COD_USUARIO = '$cod_gerente'"
                            );
                        break;                        
                        case 'Q':
                            $queryConf = mysqli_query($con, "SELECT *                   
                                        FROM usuario usu 
                                            left join configuracao_quininha conf on usu.cod_area = conf.cod_area 
                                        WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_usuario'");
                            $queryConfGerente = mysqli_query($con, "SELECT *                   
                                    FROM usuario usu 
                                        left join configuracao_quininha conf on usu.cod_area = conf.cod_area 
                                    WHERE usu.COD_SITE = '$cod_site' 
                                        AND usu.COD_USUARIO = '$cod_gerente'");                                            
                        break;
                        case 'L':
                            $queryConf = mysqli_query($con, "SELECT *                   
                                        FROM usuario usu 
                                            left join configuracao_lotinha conf on usu.cod_area = conf.cod_area 
                                        WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_usuario'");
                            $queryConfGerente = mysqli_query($con, "SELECT *                   
                                        FROM usuario usu 
                                            left join configuracao_lotinha conf on usu.cod_area = conf.cod_area 
                                        WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_gerente'");
                        break;
                        case 'R':
                            $queryConf = mysqli_query($con, "SELECT *                   
                                         FROM usuario usu 
                                         left join configuracao_rifa conf on usu.cod_area = conf.cod_area 
                                         WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_usuario'");
                            $queryConfGerente = mysqli_query($con, "SELECT *                   
                                        FROM usuario usu 
                                            left join configuracao_rifa conf on usu.cod_area = conf.cod_area 
                                        WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_gerente'");
                        break;
                        case 'B':
                            $queryConfLimiteBicho =  mysqli_query($con, "SELECT *  
                                        FROM configuracao_limite_bicho conf_limite
                                        WHERE conf_limite.COD_SITE = '$cod_site'
                                            AND conf_limite.COD_EXTRACAO = (SELECT CONCURSO FROM JOGO WHERE COD_JOGO = '$cod_jogo')");
                                                                       
                            $conf_limite_bicho_arr =  mysqli_fetch_array($queryConfLimiteBicho, MYSQLI_ASSOC);

                            $queryConf = mysqli_query($con, "SELECT *                   
                                        FROM usuario usu                                         
                                            left join configuracao_comissao_bicho conf_comissao on usu.cod_usuario = conf_comissao.cod_usuario
                                            left join configuracao_bicho conf on usu.cod_area = conf.cod_area
                                        WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_usuario'");
                            
                            $queryConfGerente = mysqli_query($con, "SELECT *                   
                                        FROM usuario usu 
                                            left join configuracao_comissao_bicho conf_comissao on usu.cod_usuario = conf_comissao.cod_usuario
                                            left join configuracao_bicho conf on usu.cod_area = conf.cod_area
                                        WHERE usu.COD_SITE = '$cod_site' 
                                            AND usu.COD_USUARIO = '$cod_gerente'");
                        break;                  
                    }
        
                    $config_usuario_arr =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
                    $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);                    
        
                    $query = "select (data_jogo < current_date) data_passou,
                                     (data_jogo <= current_date) data_retorno,
                                     (CURRENT_TIME >= '19:50') hora_retorno
                                from jogo
                                where cod_jogo = '$cod_jogo'";

                    
                    switch ($tipo_jogo) {
                        case 'S':
                        case 'Q':
                        case 'L':
                        case 'U':
                            $query = "select (data_jogo < current_date) data_passou,
                                     (data_jogo <= current_date) data_retorno,
                                     (CURRENT_TIME >= '19:50') hora_retorno,
                                     concurso
                              from jogo
                              where cod_jogo = '$cod_jogo'";
                        break;
                        case 'R':
                            $query = "select (data_jogo < current_date) data_passou,
                                     0 data_retorno,
                                     0 hora_retorno,
                                     descricao concurso
                              from jogo
                              where cod_jogo = '$cod_jogo'";
                        break;
                        case 'B':
                            $query = "select (j.data_jogo < current_date) data_passou,
                                            (j.data_jogo <= current_date) data_retorno,
                                            ((CURRENT_TIME + INTERVAL e.prazo_bloqueio MINUTE) >= j.hora_extracao) hora_retorno,
                                            concat(DATE_FORMAT(j.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(j.hora_extracao, '%H:%i'), case when j.desc_hora is null then '' else concat('-', j.desc_hora) end) concurso
                                        from jogo j
                                            inner join extracao_bicho e on (j.concurso = e.cod_extracao)
                                        where cod_jogo = '$cod_jogo'";
                        break;                        
                    }
        
                    $result = mysqli_query($con, $query);
        
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
                        throw new Exception('Aposta não pode ser feita para o concurso ' . $row['concurso'] . ', pois este encontra-se expirado. Selecione o próximo concurso!');
                    }           
                    
                    $sNumeros = "";
                    $constanteNao = mysqli_real_escape_string($con, 'N');
                    $constanteSim = mysqli_real_escape_string($con, 'S');
                    $comissao = 0;
                    $comissao_gerente = 0;
                    $possivel_retorno_calculado = 0;
                    $inversoes = 0;
                    $quina_sorte = 0;
                    $quadra_sorte = 0;
                    $terno_sorte = 0;
                    $valor_aposta = 0;

                    switch ($tipo_jogo) {
                        case 'S':
                            $comissao = $aposta->valorReal * $config_usuario_arr['PCT_COMISSAO_SENINHA'] / 100;
                            $comissao_gerente = (($aposta->valorReal - $comissao) * $config_gerente_arr['PCT_COMISSAO_SENINHA']) / 100;
                            $possivel_retorno = getValorPremio($aposta->qtd, $aposta->valorReal, $config_usuario_arr);
                            $possivel_retorno_calculado = $possivel_retorno;
                            if ($possivel_retorno_calculado > $config_usuario_arr['PREMIO_MAXIMO']) {
                                $possivel_retorno_calculado = $config_usuario_arr['PREMIO_MAXIMO'];
                            }

                            if ($config_usuario_arr['FLG_SORTE'] == 'N' && $aposta->habilitaSorte == 'S') {
                                $aposta->habilitaSorte = 'N';
                            }

                            if ($aposta->habilitaSorte == 'S') {
                                $possivel_retorno_calculado = $possivel_retorno * $config_usuario_arr['SENA_SORTE'] / 100;;
                                if ($possivel_retorno_calculado > $config_usuario_arr['PREMIO_MAXIMO']) {
                                    $possivel_retorno_calculado = $config_usuario_arr['PREMIO_MAXIMO'];
                                }

                                $quina_sorte = $possivel_retorno * $config_usuario_arr['QUINA_SORTE'] / 100;
                                if ($quina_sorte > $config_usuario_arr['PREMIO_MAXIMO']) {
                                    $quina_sorte = $config_usuario_arr['PREMIO_MAXIMO'];
                                }

                                $quadra_sorte = $possivel_retorno * $config_usuario_arr['QUADRA_SORTE'] / 100;
                                if ($quadra_sorte > $config_usuario_arr['PREMIO_MAXIMO']) {
                                    $quina_sorte = $config_usuario_arr['PREMIO_MAXIMO'];
                                }
                            }
                        break;
                        case 'U':
                            $comissao = $aposta->valorReal * $config_usuario_arr['PCT_COMISSAO_SUPERSENA'] / 100;
                            $comissao_gerente = (($aposta->valorReal - $comissao) * $config_gerente_arr['PCT_COMISSAO_SUPERSENA']) / 100;

                            $possivel_retorno_calculado = $aposta->valorReal * $config_usuario_arr['PREMIO_SENA'];
                            if ($possivel_retorno_calculado > $config_usuario_arr['PREMIO_MAXIMO']) {
                                $possivel_retorno_calculado = $config_usuario_arr['PREMIO_MAXIMO'];
                            }

                            $quina_sorte = $aposta->valorReal * $config_usuario_arr['PREMIO_QUINA'];
                            if ($quina_sorte > $config_usuario_arr['PREMIO_MAXIMO']) {
                                $quina_sorte = $config_usuario_arr['PREMIO_MAXIMO'];
                            }

                            $quadra_sorte = $aposta->valorReal * $config_usuario_arr['PREMIO_QUADRA'];
                            if ($quadra_sorte > $config_usuario_arr['PREMIO_MAXIMO']) {
                                $quadra_sorte = $config_usuario_arr['PREMIO_MAXIMO'];
                            }
                        break;                        
                        case 'Q':
                            $comissao = $aposta->valorReal * $config_usuario_arr['PCT_COMISSAO_QUININHA'] / 100;
                            $comissao_gerente = (($aposta->valorReal - $comissao) * $config_gerente_arr['PCT_COMISSAO_QUININHA']) / 100;
                            $possivel_retorno = getValorPremio($aposta->qtd, $aposta->valorReal, $config_usuario_arr);
                            $possivel_retorno_calculado = $possivel_retorno;
                            if ($possivel_retorno_calculado > $config_usuario_arr['PREMIO_MAXIMO']) {
                                $possivel_retorno_calculado = $config_usuario_arr['PREMIO_MAXIMO'];
                            }

                            if ($config_usuario_arr['FLG_SORTE'] == 'N' && $aposta->habilitaSorte == 'S') {
                                $aposta->habilitaSorte = 'N';
                            }

                            if ($aposta->habilitaSorte == 'S') {
                                $possivel_retorno_calculado = $possivel_retorno * $config_usuario_arr['QUINA_SORTE'] / 100;;
                                if ($possivel_retorno_calculado > $config_usuario_arr['PREMIO_MAXIMO']) {
                                    $possivel_retorno_calculado = $config_usuario_arr['PREMIO_MAXIMO'];
                                }

                                $quadra_sorte = $possivel_retorno * $config_usuario_arr['QUADRA_SORTE'] / 100;
                                if ($quadra_sorte > $config_usuario_arr['PREMIO_MAXIMO']) {
                                    $quadra_sorte = $config_usuario_arr['PREMIO_MAXIMO'];
                                }

                                $terno_sorte = $possivel_retorno * $config_usuario_arr['TERNO_SORTE'] / 100;
                                if ($terno_sorte > $config_usuario_arr['PREMIO_MAXIMO']) {
                                    $terno_sorte = $config_usuario_arr['PREMIO_MAXIMO'];
                                }
                            }
                        break;
                        case 'L':
                            $comissao = $aposta->valorReal * $config_usuario_arr['PCT_COMISSAO_LOTINHA'] / 100;
                            $comissao_gerente = (($aposta->valorReal - $comissao) * $config_gerente_arr['PCT_COMISSAO_LOTINHA']) / 100;
                            $possivel_retorno = getValorPremio($aposta->qtd, $aposta->valorReal, $config_usuario_arr);
                            $possivel_retorno_calculado = $possivel_retorno;
                            if ($possivel_retorno_calculado > $config_usuario_arr['PREMIO_MAXIMO']) {
                                $possivel_retorno_calculado = $config_usuario_arr['PREMIO_MAXIMO'];
                            }
                        break;
                        case 'R':
                            $comissao = $aposta->valorReal * $config_usuario_arr['PCT_COMISSAO_RIFA'] / 100;
                            $comissao_gerente = (($aposta->valorReal - $comissao) * $config_gerente_arr['PCT_COMISSAO_RIFA']) / 100;
                        break;     
                        case 'B':
                            $retorno_arr = calcularRetorno($aposta->tipoJogo, $aposta->valorReal, $aposta->numeros, getDoPremio($aposta), getAoPremio($aposta), $config_usuario_arr, $conf_limite_bicho_arr);
                            $possivel_retorno_calculado = $retorno_arr['retorno'];
                            $inversoes = $retorno_arr['inversoes'];
                            $comissao = obterComissaoBicho(
                                $aposta->tipoJogo,
                                $aposta->valorReal,
                                $config_usuario_arr
                            );           
                            $comissao_gerente = obterComissaoBicho(
                                $aposta->tipoJogo,
                                $aposta->valorReal - $comissao,
                                $config_gerente_arr
                            );                 
                        break;
                    }

                    if ($tipo_jogo == 'S' && $aposta->habilitaSorte == 'S') {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno5, retorno4, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiiisddsdddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $constanteSim, $quina_sorte, $quadra_sorte, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaSeninhaSorte($aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $constanteSim, $quina_sorte, $quadra_sorte, $comissao);
                    } else if ($tipo_jogo == 'U') {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno5, retorno4, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiiisddsdddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $constanteSim, $quina_sorte, $quadra_sorte, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaSuperSena($aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $quina_sorte, $quadra_sorte, $comissao);                        
                    } else if ($tipo_jogo == 'Q' && $aposta->habilitaSorte == 'S') {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, flg_sorte, retorno4, retorno3, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiiisddsdddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $constanteSim, $quadra_sorte, $terno_sorte, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaQuininhaSorte($aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $constanteSim, $quadra_sorte, $terno_sorte, $comissao);
                    } else if ($tipo_jogo == 'L') {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiiisdddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaLotinha($aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $comissao);
                    } else if ($tipo_jogo == 'R') {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiiisddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaRifa($aposta->qtd, $sNumeros, $aposta->valorReal, $comissao);
                    } else if ($tipo_jogo == 'B') {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $doPremio = getDoPremio($aposta);
                        $aoPremio = getAoPremio($aposta);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, txt_aposta, valor_aposta, possivel_retorno, do_premio, ao_premio, tipo_jogo, inversoes, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?,?,?,?,?) ");                
                        $stmt->bind_param("iiisddiisidd", $cod_site, $cod_jogo, $cod_bilhete, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $doPremio, $aoPremio, $aposta->tipoJogo, $inversoes, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaBicho($sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $doPremio, $aoPremio, $aposta->tipoJogo, $inversoes, $comissao);
                    } else {
                        $sNumeros = str_replace('.', '', $aposta->numeros);
                        $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, comissao, comissao_gerente )
                            values(?,?,?,?,?,?,?,?,?) ");
                        $stmt->bind_param("iiiisdddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $comissao, $comissao_gerente);
                        $auditoria = $auditoria . descreverApostaQuininhaSeninha($tipo_jogo, $aposta->qtd, $sNumeros, $aposta->valorReal, $possivel_retorno_calculado, $comissao);
                    }

                    $valorReal = $valorReal + $aposta->valorReal;

                    $stmt->execute();                    
                }
                
                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $cod_site,
                    AUD_BILHETE_CRIADO,
                    $auditoria
                );

            }

            foreach ($apostas_2pra500 as $aposta) {

                $stmt = $con->prepare("INSERT INTO bilhete (cod_site, 
                                                            cod_usuario,
                                                            nome_apostador,
                                                            telefone_apostador,
                                                            data_bilhete,
                                                            txt_pule_pai) 
                                            VALUES (?, ?, ?, ?, current_timestamp, ?) ");
                $stmt->bind_param("iisss", $cod_site, $cod_usuario, $nome_apostador, $telefone_apostador, $pule);
                $stmt->execute();
                $cod_bilhete = $con->insert_id;

                $stmt = $con->prepare(" UPDATE bilhete 
                                            SET numero_bilhete = ?,
                                                txt_pule = ?
                                        WHERE cod_bilhete = ? ");
                $numero_bilhete = $cod_site . $cod_bilhete;
                $stmt->bind_param("isi", intval($numero_bilhete), intval($numero_bilhete), $cod_bilhete);
                $stmt->execute();

                $data_atual = mysqli_real_escape_string($con, $_POST['data_atual']);   

                $auditoria = descreverCriacaoBilhete(
                    $nome_apostador,
                    $telefone_apostador,
                    $data_atual,
                    $pule,
                    $cod_bilhete,
                    null
                ); 

                $cod_jogo = $aposta->cod_jogo;
           
                $queryConf = mysqli_query($con, "SELECT *                   
                            FROM usuario usu 
                            left join configuracao_2pra500 conf on usu.cod_area = conf.cod_area 
                            WHERE usu.COD_SITE = '$cod_site' 
                                AND usu.COD_USUARIO = '$cod_usuario'");
                $queryConfGerente = mysqli_query($con, "SELECT *                   
                            FROM usuario usu 
                            left join configuracao_2pra500 conf on usu.cod_area = conf.cod_area 
                            WHERE usu.COD_SITE = '$cod_site' 
                                AND usu.COD_USUARIO = '$cod_gerente'");                                
    
                $config_usuario_arr =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
                $config_gerente_arr =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
    
                $query = "select (j.data_jogo < current_date) data_passou,
                                (j.data_jogo <= current_date) data_retorno,
                                ((CURRENT_TIME + INTERVAL e.prazo_bloqueio MINUTE) >= j.hora_extracao) hora_retorno
                            from jogo j
                                inner join extracao_bicho e on (j.concurso = e.cod_extracao)
                            where cod_jogo = '$cod_jogo'";                            
    
                $result = mysqli_query($con, $query);
    
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
                    throw new Exception('Aposta não pode ser feita para o concurso ' . $aposta->possivelRetorno . '! Selecione o próximo concurso!');
                }           
                
                $comissao = 0;
                $possivel_retorno_calculado = 0;
                
                $valor_aposta = $config_usuario_arr['VALOR_APOSTA'];
                $comissao = $config_usuario_arr['VALOR_APOSTA'] * $config_usuario_arr['PCT_COMISSAO_2PRA500'] / 100;
                $comissao_gerente = (($valor_aposta - $comissao) * $config_gerente_arr['PCT_COMISSAO_2PRA500']) / 100;
                $possivel_retorno_calculado = $config_usuario_arr['VALOR_ACUMULADO'];
                

                $stmt = $con->prepare(" insert into aposta(cod_site, cod_jogo, cod_bilhete, qtd_numeros, txt_aposta, valor_aposta, possivel_retorno, comissao, comissao_gerente )
                values(?,?,?,4,?,?,?,?,?) ");
                $stmt->bind_param("iiisdddd", $cod_site, $cod_jogo, $cod_bilhete, $aposta->numeros, $valor_aposta, $possivel_retorno_calculado, $comissao, $comissao_gerente);
                $auditoria = $auditoria . descreverAposta2pra500($aposta->numeros, $valor_aposta, $possivel_retorno_calculado, $comissao);

                $valorReal = $valorReal + $valor_aposta;

                $stmt->execute();

                inserir_auditoria(
                    $con,
                    $cod_usuario,
                    $cod_site,
                    AUD_BILHETE_CRIADO,
                    $auditoria
                );
                
            }

            $stmt = $con->prepare("UPDATE usuario SET SALDO = SALDO - ?
                                    WHERE COD_USUARIO = ? AND COD_SITE = ?");
            $stmt->bind_param("dii", $valorReal, $cod_usuario, $cod_site);
            $stmt->execute();

            registrarCliente($con, $cod_usuario, $registro_cliente, $nome_apostador, $telefone_apostador);

            $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_SITE = '$cod_site'
              and COD_USUARIO = '$cod_usuario' ";
            $result = mysqli_query($con, $query);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            $response['saldo'] = $row['SALDO'];
            $response['pule'] = $pule;
            $response['status'] = "OK";
        }
    }
    $con->commit();
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}
$con->close();
echo json_encode($response);

function getValorPremio($qtdNumeros, $valor_aposta, $configuracao)
{
    switch ($qtdNumeros) {
        case 13:
            return $valor_aposta * $configuracao['PREMIO_13'];
        case 14:
            return $valor_aposta * $configuracao['PREMIO_14'];
        case 15:
            return $valor_aposta * $configuracao['PREMIO_15'];
        case 16:
            return $valor_aposta * $configuracao['PREMIO_16'];
        case 17:
            return $valor_aposta * $configuracao['PREMIO_17'];
        case 18:
            return $valor_aposta * $configuracao['PREMIO_18'];
        case 19:
            return $valor_aposta * $configuracao['PREMIO_19'];
        case 20:
            return $valor_aposta * $configuracao['PREMIO_20'];
        case 21:
            return $valor_aposta * $configuracao['PREMIO_21'];
        case 22:
            return $valor_aposta * $configuracao['PREMIO_22'];
        case 25:
            return $valor_aposta * $configuracao['PREMIO_25'];
        case 30:
            return $valor_aposta * $configuracao['PREMIO_30'];
        case 35:
            return $valor_aposta * $configuracao['PREMIO_35'];
        case 40:
            return $valor_aposta * $configuracao['PREMIO_40'];
        case 45:
            return $valor_aposta * $configuracao['PREMIO_45'];
    }
}

function geraPule($cod_site) {
    $prefixo = strtoupper(dechex($cod_site));
    if (strlen($prefixo) < 4) {
        $prefixo = str_pad($prefixo , 4 , '0' , STR_PAD_LEFT);
    }
    $aleatorio = $prefixo . "-";
    
    $letras = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
    $indice = rand(0, strlen($letras) - 1);

    $tamanho = 4;
    for ($i = 0; $i < $tamanho; $i++) {
        $indice = rand(0, strlen($letras) - 1);
        $aleatorio = $aleatorio . substr($letras, $indice, 1);
    }
    return $aleatorio;
}

function registrarCliente($con, $cod_usuario, $registro_cliente, $nome_apostador, $telefone_apostador) {
    if ($registro_cliente == 'true') {
        $query = "SELECT  COD_CLIENTE_CAMBISTA
        FROM cliente_cambista
        WHERE cod_usuario = '$cod_usuario' and nome = '$nome_apostador'";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        if ($row) {
            $cod_cliente = $row['COD_CLIENTE_CAMBISTA'];
            $stmt = $con->prepare("UPDATE cliente_cambista SET telefone = ? WHERE COD_CLIENTE_CAMBISTA = ?");
            $stmt->bind_param("si", $telefone_apostador, $cod_cliente);
            $stmt->execute();
        } else {
            $stmt = $con->prepare("INSERT INTO cliente_cambista (cod_usuario, nome, telefone) VALUES (?, ?, ?)");
            $stmt->bind_param("iss", $cod_usuario, $nome_apostador, $telefone_apostador);
            $stmt->execute();
        }    
    }
}

