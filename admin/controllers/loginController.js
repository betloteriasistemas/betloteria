var app = angular.module('main');

app.controller('loginCtrl', function ($scope, $http, $location, user) {
    

    $scope.login = function () {        
        var username = $scope.edtUsuario;
        var password = $scope.edtSenha;
        if (typeof username === "undefined") {
            alert('Informe o USUÁRIO!');
        } else if (typeof password === "undefined") {
            alert('Informe a SENHA!');
        } else {            
            $http({                
                url: 'angularjs-mysql/login.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + username + '&password=' + password
            }).then(function (response) {                	
                if (response.data.status == 'loggedin') {
                    user.saveData(response.data);
                    $location.path('/dashboard');
                } else {
                    alert('Login Inválido!');
                }
            })
        }
    }
});