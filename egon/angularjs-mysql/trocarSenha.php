<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

try {

    $query = "";

    $nova_senha = mysqli_real_escape_string($con, $_POST['nova_senha']);
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $site = mysqli_real_escape_string($con, $_POST['site']);
    $nome_usuario = mysqli_real_escape_string($con, $_POST['nome_usuario']);

    $stmt = $con->prepare("UPDATE usuario SET SENHA = ?
	                       WHERE COD_USUARIO = ? AND COD_SITE = ?");
    $stmt->bind_param("sii", $nova_senha, $cod_usuario, $site);

    $stmt->execute();
    inserir_auditoria($con, $cod_usuario, $site, AUD_USUARIO_SENHA_EDITADA, descreverTrocaSenhaUusario($nome_usuario));

    $response['status'] = "OK";
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

$stmt->close();
$con->close();
