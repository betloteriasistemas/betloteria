<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

if ($operacao == "listar") {

    $query = "SELECT AG.COD_AGENDA,
                    TR.NOME,
                    (CASE 
                        WHEN AG.TIPO_JOGO = 'S' THEN 'SENINHA' 
                        WHEN AG.TIPO_JOGO = 'Q' THEN  'QUININHA' 
                        WHEN AG.TIPO_JOGO = 'B' THEN 'BICHO' 
                        WHEN AG.TIPO_JOGO = '2' THEN  '2 PRA 500' 
                        WHEN AG.TIPO_JOGO = 'L' THEN  'LOTINHA' 
                        ELSE 'TODOS'
                    END) AS TIPO_JOGO,
                    CONCAT((CASE 
                                WHEN AG.HORA = '-1' THEN '*'
                                ELSE LPAD(AG.HORA,2,'0')
                            END), ':', LPAD(AG.MINUTO,2,'0')) AS HORARIO,
                    AG.DOMINGO,
                    AG.SEGUNDA,
                    AG.TERCA,
                    AG.QUARTA,
                    AG.QUINTA,
                    AG.SEXTA,
                    AG.SABADO
                FROM BOT_AGENDA AG 
                    INNER JOIN BOT_TAREFA TR ON (AG.COD_TAREFA = TR.COD_TAREFA)
                WHERE STATUS = 'ATIVO'
                ORDER BY AG.TIPO_JOGO DESC";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_agenda'] = $row['COD_AGENDA'];
        $row_array['nome'] = $row['NOME'];
        $row_array['tipo_jogo'] = $row['TIPO_JOGO'];
        $row_array['horario'] = $row['HORARIO'];
        $row_array['domingo'] = $row['DOMINGO'] == '1';
        $row_array['segunda'] = $row['SEGUNDA'] == '1';
        $row_array['terca'] = $row['TERCA'] == '1';
        $row_array['quarta'] = $row['QUARTA'] == '1';
        $row_array['quinta'] = $row['QUINTA'] == '1';    
        $row_array['sexta'] = $row['SEXTA'] == '1';
        $row_array['sabado'] = $row['SABADO'] == '1';

        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);

} else if ($operacao == "salvar") {
    
    $agendas = json_decode($_POST['agenda']);

    try {
        $con->begin_transaction();
        foreach ($agendas as $agenda) {

            $domingo = $agenda->domingo ? 1 : 0;
            $segunda = $agenda->segunda ? 1 : 0;
            $terca = $agenda->terca ? 1 : 0;
            $quarta = $agenda->quarta ? 1 : 0;
            $quinta = $agenda->quinta ? 1 : 0;
            $sexta = $agenda->sexta ? 1 : 0;
            $sabado = $agenda->sabado ? 1 : 0;

            $stmt = $con->prepare(" UPDATE BOT_AGENDA 
                                        SET DOMINGO = ?, 
                                            SEGUNDA = ?, 
                                            TERCA = ?, 
                                            QUARTA = ?, 
                                            QUINTA = ?,
                                            SEXTA = ?,
                                            SABADO = ?
                                        WHERE COD_AGENDA = ? ");
            $stmt->bind_param("iiiiiiii", $domingo, $segunda, $terca, $quarta, $quinta, $sexta, $sabado, $agenda->cod_agenda);
            $stmt->execute();
        }
        $con->commit();

    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

}

$con->close();

