<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$site = mysqli_real_escape_string($con, $_POST['site']);

$query = "";

if ($operacao == 'listar') {
    $area = mysqli_real_escape_string($con, $_POST['area']);
    if ($area == 0) {

        $queryArea = "SELECT COD_AREA
        FROM areas
        WHERE cod_site = '$site' AND status = 'A'";
    
       $resultArea = mysqli_query($con, $queryArea);
       $rowArea = mysqli_fetch_array($resultArea, MYSQLI_ASSOC);
       $area = $rowArea['COD_AREA']; 
        
    }

    $query =
        " SELECT AE.COD_AREA_EXTRACAO, 
                 E.TIPO_JOGO, 
                 concat(E.DESCRICAO, ' - ',E.HORA_EXTRACAO) AS DESCRICAO, 
                 AE.STATUS,
                 E.AUTOMATIZADA,
                 AE.COD_AREA
        FROM AREA_EXTRACAO AE INNER JOIN EXTRACAO_BICHO E ON (AE.COD_EXTRACAO = E.COD_EXTRACAO)
        WHERE AE.COD_AREA = '$area' 
            AND AE.COD_SITE = '$site'
            AND E.STATUS = 'A'
        ORDER BY E.HORA_EXTRACAO ";

    $result = mysqli_query($con, $query);
    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_extracao'] = $row['COD_AREA_EXTRACAO'];
        $row_array['tipo_jogo'] = $row['TIPO_JOGO'];
        $row_array['descricao'] = $row['DESCRICAO'];
        $row_array['status'] = $row['STATUS'];
        $row_array['automatizada'] = $row['AUTOMATIZADA'];
        $row_array['cod_area'] = $row['COD_AREA'];
        array_push($return_arr, $row_array);
    };

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);

} else if ($operacao == "excluir") {

    try {
        $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);

        if ($_POST['status'] == "A") {

            $stmt = $con->prepare("update area_extracao SET status = 'I' WHERE cod_area_extracao = ?");

        } else {

            $stmt = $con->prepare("update area_extracao SET status = 'A' WHERE cod_area_extracao = ?");

        }
        $stmt->bind_param("i", $cod_extracao);
        $stmt->execute();
        $stmt->close();
        $response['status'] = "OK";
    
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }
    echo json_encode($response);

}
$con->close();