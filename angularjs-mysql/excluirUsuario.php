<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

try {
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);

    $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
    $perfil = mysqli_real_escape_string($con, $_POST['perfil']);
    $nome = mysqli_real_escape_string($con, $_POST['nome']);
    $status = mysqli_real_escape_string($con, $_POST['status']);

    if ($status == "A") {
        $stmt = $con->prepare("UPDATE usuario SET STATUS = 'I' WHERE COD_USUARIO = ?");
        $stmt->bind_param("i", $codigo);
    } else {
        $stmt = $con->prepare("UPDATE usuario SET STATUS = 'A' WHERE COD_USUARIO = ?");
        $stmt->bind_param("i", $codigo);
    }

    $stmt->execute();

    if ($perfil != 'A') {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);

        $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_USUARIO = '$codigo' ";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $saldo = $row['SALDO'];
        $stmt = $con->prepare("UPDATE usuario SET SALDO = SALDO + ?
		                       WHERE COD_USUARIO = ?");
        $stmt->bind_param("di", $saldo, $cod_usuario);
        $stmt->execute();
        $query = " SELECT SALDO, PERFIL  FROM usuario
            WHERE COD_USUARIO = '$cod_usuario' ";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        $response['saldo'] = $row['SALDO'];
    } else {
        if ($_POST['status'] == 'A') {
            $stmt2 = $con->prepare("UPDATE usuario SET STATUS = 'I' WHERE COD_GERENTE = ?");
            $stmt2->bind_param("i", $codigo);
        } else {
            $stmt2 = $con->prepare("UPDATE usuario SET STATUS = 'A' WHERE COD_GERENTE = ?");
            $stmt2->bind_param("i", $codigo);
        }
        $stmt2->execute();
    }

    inserir_auditoria(
        $con,
        $cod_usuario,
        $cod_site,
        $status == "A" ? AUD_USUARIO_DESATIVADO : AUD_USUARIO_ATIVADO,
        descreverDesativacaoUsuario($nome, $status, $perfil)
    );

    $response['status'] = "OK";
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

$stmt->close();
$con->close();
