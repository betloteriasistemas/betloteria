angular.module('main').controller('relatorioPrestacaoContaCtrl', function ($scope, user, $http, $filter, $rootScope) {
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.site = user.getSite();
    $scope.edtDataDe = new Date();
    $scope.edtDataAte = new Date();
    $scope.consulta = false;
    $scope.tipo_relatorio = 'pdf';
    $scope.eh_pdf = true;
    
    $scope.alteraTipoRelatorio = function() {
        if ($scope.eh_pdf) {
            $scope.eh_pdf = true;
            $scope.tipo_relatorio = 'pdf';
        } else {
            $scope.eh_pdf = false;
            $scope.tipo_relatorio = 'xlsx';
        }
    }
    
    $scope.ajustaDataFimPeriodo = function () {
        if($scope.edtDataDe > $scope.edtDataAte) {
            $scope.edtDataAte = $scope.edtDataDe;
        }
    }
    
    $scope.listarRelatorios = function () {

        if($scope.edtDataDe > $scope.edtDataAte) {
            $.alert('A data final do período deve ser maior ou igual a data inicial.');
            return;
        }

        $rootScope.loading = true;
        $scope.consulta = true;
        
        dataInicial  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        dataFinal = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");

        $http({
            method: "POST",
            url: "angularjs-mysql/relatorioPrestacaoConta.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite()
                    + '&operacao=listar' 
                    + '&dataDe=' + dataInicial 
                    + '&dataAte=' + dataFinal
                    + '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;
            //$scope.records = JSON.stringify(response.data);	
        }).catch(function (err) {
            console.log(err.data);
            $.alert('Erro no $HTTP: ' + err.data);
        }).finally(function() {
            $rootScope.loading = false;
        });

    }

    $scope.gerarRelatorio = function ($cod_prestacao_conta, $data_prestacao_conta) {
        var dataPrestacao = $filter('date')($data_prestacao_conta, "yyyyMMdd");
        window.open('angularjs-mysql/relatorioPrestacaoConta.php?' 
            + 'cod_prestacao_conta=' + $cod_prestacao_conta 
            + '&data_prestacao_conta=' + dataPrestacao
            + '&tipoRelatorio=' + $scope.tipo_relatorio
            + '&schema=' + user.getSchema()
            + '&operacao=gerarRelatorio');
    }
});