<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

try {
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $codigoSite = mysqli_real_escape_string($con, $_POST['cod_site']);
    $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
    $data = mysqli_real_escape_string($con, $_POST['data']);
    $nome_criador = mysqli_real_escape_string($con, $_POST['nome_criador']);
    $valor = mysqli_real_escape_string($con, $_POST['valor']);
    $tipo = mysqli_real_escape_string($con, $_POST['tipo']);
    $motivo = mysqli_real_escape_string($con, $_POST['motivo']);
    $nomeColaborador = mysqli_real_escape_string($con, $_POST['nomeColaborador']);
    $ajudaCusto = mysqli_real_escape_string($con, $_POST['ajudaCusto']);

    $query = "select cod_pai
                from lancamento l    
                where cod_lancamento = '$codigo'";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC); 
    $cod_pai = $row['cod_pai'];

    if ($cod_pai == null) {
        $query = "select cod_lancamento
                    from lancamento l    
                    where cod_pai = '$codigo'";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC); 
        $cod_pai = $row['cod_lancamento'];
    }

    $stmt = $con->prepare("UPDATE lancamento SET status = 'I' WHERE cod_lancamento in (?, ?)");
    $stmt->bind_param("ii", $codigo, $cod_pai);
    $stmt->execute();

    inserir_auditoria($con, $cod_usuario, $codigoSite, AUD_LANCAMENTO_EXCLUIDO,
    descreverLancamento($data,
    $nome_criador,
    $tipo,
    $nomeColaborador,
    $motivo,
    $ajudaCusto,
    $valor));

    $response['status'] = "OK";
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

$stmt->close();
$con->close();
