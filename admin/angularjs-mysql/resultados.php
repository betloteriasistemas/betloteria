<?php
include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$url = "https://www.lotodicas.com.br/api/"; // mega-sena/"; // . $numero;

$return_arr = array();

$response = [];

$operacao = $_POST['operacao'];
// $operacao = $_GET['operacao'];

if ($operacao == "getResultados") {
    $mensagem = "";
    try {

        $query = "select distinct jogo.concurso, jogo.tipo_jogo, jogo.data_jogo
        from jogo inner join site on (jogo.cod_site = site.cod_site)
                    where jogo.data_jogo <= current_date
                    and jogo.tp_status in ('L', 'B')
                    and site.STATUS = 'A'
                    and tipo_jogo <> 'B' ";

        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $tipo_jogo = "mega-sena/";
            if ($row["tipo_jogo"] == 'Q') {
                $tipo_jogo = "quina/";
            }
            $urlAux = $url . $tipo_jogo . $row["concurso"];

            //echo $urlAux;

            try {
                $dados = file_get_contents($urlAux);
                if ($dados == 'false') {
                    $mensagem = $mensagem . $row["data_jogo"] . " - ";
                    continue;
                }
                $deco = json_decode($dados, true);

                $concurso = $deco['numero'];
                $numero1 = $deco['sorteio']['0'];
                $numero2 = $deco['sorteio']['1'];
                $numero3 = $deco['sorteio']['2'];
                $numero4 = $deco['sorteio']['3'];
                $numero5 = $deco['sorteio']['4'];
                if ($row["tipo_jogo"] == 'S') {
                    $numero6 = $deco['sorteio']['5'];
                }

                $queryUpdt = " update jogo
                    set numero_1 = " . $numero1 . " ,
                    numero_2 = " . $numero2 . ",
                    numero_3 = " . $numero3 . ",
                    numero_4 = " . $numero4 . ",
                    numero_5 = " . $numero5 . ", ";

                if ($row["tipo_jogo"] == 'S') {
                    $queryUpdt = $queryUpdt . " numero_6 = " . $numero6 . ", ";
                }

                $queryUpdt = $queryUpdt . " tp_status = 'B' where concurso = " . $concurso;

                if ($con->query($queryUpdt) != true) {
                    $response['status'] = "ERROR";
                    $response['mensagem'] = $con->error;
                    return;
                }

            } catch (Exception $e) {
                continue;
            }

        }

        $response['status'] = "OK";
        $response['mensagem'] = $mensagem;
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

    echo json_encode($response);

} else if ($operacao == "bloquear") {
    try {

        $atras = $_POST['atras'];

        $sinal = "<=";
        if ($atras == 1) {
            $sinal = "<";
        }

        $query = "update jogo
        set tp_status = 'B'
        where jogo.data_jogo " . $sinal . " current_date
        and jogo.tp_status = 'L' ";

        if ($con->query($query) != true) {
            $response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
            return;
        }

        $response['status'] = "OK";
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

    echo json_encode($response);

} else if ($operacao == "criarJogos") {

    //$data = date('2018-09-12');
    date_default_timezone_set('Etc/GMT+3');
    $data = date('Y-m-d');
    //$data = date('2018-09-12');

    //echo $data;

    $dia = date('w', strtotime($data));
    //echo $dia . "\n";
    // Domingo = 0 / Segunda = 1 / Terça = 2 / Quarta = 3 / Quinta = 4 / Sexta = 5 / Sabado = 6

    $query = "select cod_site from site
             where cod_site >= 1000  ";

    $result = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_site'] = $row['cod_site'];

        array_push($return_arr, $row_array);
    }

    //print_r($return_arr);

    try {

        // SENINHA E QUININHA
        for ($i = 1; $i <= 2; $i++) {
            if ($i == 1) {
                $urlAux = $url . "mega-sena/";
                $tipo_jogo = "S";
            } else {
                $urlAux = $url . "quina/";
                $tipo_jogo = "Q";
            }

            $dados = file_get_contents($urlAux);
            $deco = json_decode($dados, true);

            $concurso = $deco['numero'];
            $proximoConcurso = $concurso + 1;
            $proximo_data = $deco['proximo_data'];
            $dataProximaSql = date($proximo_data);

            $iAux = 1;

            //echo $dia;
            for ($d = $dia; $d <= 6; $d++) {
                if ($tipo_jogo == "S" && ($d != 3 && $d != 6)) {
                    if ($iAux != 1) {
                        $dataProximaSql = date('Y-m-d', strtotime($dataProximaSql . ' + 1 days'));
                    }
                    continue;
                } else if ($tipo_jogo == "Q" && $d == 6) {
                    continue;
                }

                $iAux = 2;

                foreach ($return_arr as $value) {
                    $cod_site = $value['cod_site'];

                    // SE FOR DE LEANDRO, NÃO CRIAR OS JOGOS DO BICHO. ELES VÃO CRIAR AUTOMATICO NESSE PRIMEIRO MOMENTO.
                    //if ($cod_site != 1005) {

                        // BICHO
                        $queryInsereBicho =
                            " insert into jogo(cod_site, cod_usuario, data_jogo, tp_status, tipo_jogo, concurso, hora_extracao, desc_hora)
                        select $cod_site, (select min(cod_usuario) from usuario where cod_site = $cod_site), '$dataProximaSql', 'L', tipo_jogo, 0, hora_extracao, descricao
                        from extracao_bicho ex
                        where ex.cod_site = $cod_site
                        and status = 'A'
                        and federal = 'N'                        
                        and not exists (
                            select * from jogo j
                            where j.cod_site = ex.cod_site
                              and j.data_jogo = '$dataProximaSql'
                              and j.hora_extracao = ex.hora_extracao
                              and (j.desc_hora = ex.descricao or (j.desc_hora is null and ex.descricao is null) )
                          )
                        order by hora_extracao ";

                        if ($con->query($queryInsereBicho) != true) {
                            echo $queryInsereBicho . "\n";
                            continue;
                        }

                        $diaFederal = date('w', strtotime($dataProximaSql));

                        if ($diaFederal == 3 || $diaFederal == 6) {
                            $queryInsereBichoFederal =
                                " insert into jogo(cod_site, cod_usuario, data_jogo, tp_status, tipo_jogo, concurso, hora_extracao, desc_hora)
                            select $cod_site, (select min(cod_usuario) from usuario where cod_site = $cod_site), '$dataProximaSql', 'L', 'B', 0, hora_extracao, descricao
                            from extracao_bicho ex
                            where ex.cod_site = $cod_site
                            and status = 'A'
                            and federal = 'S'
                            and not exists (
                                select * from jogo j
                                where j.cod_site = ex.cod_site
                                  and j.data_jogo = '$dataProximaSql'
                                  and j.hora_extracao = ex.hora_extracao
                                  and (j.desc_hora = ex.descricao or (j.desc_hora is null and ex.descricao is null) )
                              )
                            order by hora_extracao ";

                            if ($con->query($queryInsereBichoFederal) != true) {
                                echo $queryInsereBichoFederal . "\n";
                                continue;
                            }
                        }

                    //}

                    $queryExisteConcurso =
                        "select count(cod_jogo) qtd from jogo where cod_site = $cod_site and concurso = $proximoConcurso ";
                    $resultExiste = mysqli_query($con, $queryExisteConcurso);
                    $rowExiste = mysqli_fetch_array($resultExiste, MYSQLI_ASSOC);
                    if ($rowExiste['qtd'] > 0) {
                        continue;
                    }

                    $queryUpdt = "insert into jogo(cod_site, cod_usuario, data_jogo, tp_status, tipo_jogo, concurso , hora_extracao, desc_hora)
                                    select $cod_site, min(cod_usuario), '$dataProximaSql', 'L', '$tipo_jogo', $proximoConcurso, ' ', null
                                    from usuario where cod_site = $cod_site";

                    //echo $queryUpdt . "\n";

                    if ($con->query($queryUpdt) != true) {
                        echo $queryUpdt . "\n";
                        continue;
                    }

                }
                $dataProximaSql = date('Y-m-d', strtotime($dataProximaSql . ' + 1 days'));
                $proximoConcurso = $proximoConcurso + 1;
            }

        }

        $response['status'] = "OK";
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    } finally {
        echo json_encode($response);
    }
} else if ($operacao == "processarJogos9999") {
    try {

        $query = "select apo.cod_aposta, jLeandro.COD_JOGO COD_JOGO_LEANDRO, jLeandro.CONCURSO CONCURSO_LEANDRO,
                                             jogo.COD_JOGO cod_9999, jogo.CONCURSO CON_999
        from aposta apo
        inner join jogo on (jogo.COD_JOGO = apo.COD_JOGO)
        inner join jogo jLeandro on (jLeandro.COD_SITE = 9191 and jLeandro.CONCURSO = jogo.CONCURSO)
        where apo.cod_site = 9191
          and jogo.COD_SITE = 9999 ";

        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

            $codAposta = $row["cod_aposta"];
            $codJogo = $row["COD_JOGO_LEANDRO"];
            $query = "update aposta
                         set cod_jogo = '$codJogo'
                       where cod_aposta =  '$codAposta' ";

            if ($con->query($query) != true) {
                $response['status'] = "ERROR";
                $response['mensagem'] = $con->error;
                return;
            }

        }

        $response['status'] = "OK";
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

    echo json_encode($response);

}
