<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_GET)) {
    die();
}

$site = mysqli_real_escape_string($con, $_GET['site']);
$dataInicial = mysqli_real_escape_string($con, $_GET['dataInicial']);
$dataFinal = mysqli_real_escape_string($con, $_GET['dataFinal']);
$modalidade = mysqli_real_escape_string($con, $_GET['modalidade']);
$codigoExtracao = mysqli_real_escape_string($con, $_GET['codigoExtracao']);
$aoPremio = mysqli_real_escape_string($con, $_GET['aoPremio']);


$arrayJogos = array();
$arrayBichos = array();
$returnArray = array();

$query = "select apo.cod_aposta, apo.cod_jogo, apo.cod_bilhete, apo.txt_aposta, ".
         "apo.possivel_retorno, apo.valor_aposta, apo.tipo_jogo ".
         "from aposta apo ".
         "inner join bilhete b on apo.cod_bilhete = b.cod_bilhete ".
         "inner join jogo on apo.cod_jogo = jogo.cod_jogo ".
         "inner join extracao_bicho ext on jogo.concurso = ext.cod_extracao ".
         "where apo.cod_site = '$site' ".
         "and apo.tipo_jogo = '$modalidade' ".
         "and ext.cod_extracao = '$codigoExtracao' ".
         "and apo.do_premio >= 1 ".
         "and apo.ao_premio <= '$aoPremio' ".
         "and jogo.tipo_jogo = 'B' ".
         "and b.status_bilhete not in ('C', 'V') ".
         "and apo.possivel_retorno > 0 and apo.status <> 'C' ";
  if ($dataInicial == '0' || $dataFinal == '0') {
   $query =  $query . 
              "and date(jogo.data_jogo) >= DATE_FORMAT(current_date, '%Y%m%d') ".
              "and date(jogo.data_jogo) <= DATE_FORMAT(current_date, '%Y%m%d') ";
  }       

  if ($dataInicial != '0') {
    $query = $query . " and date(jogo.data_jogo) >= date_format('$dataInicial', '%Y%m%d') ";
  }

  if ($dataFinal != '0') {
    $query = $query . " and date(jogo.data_jogo) <= date_format('$dataFinal', '%Y%m%d') ";
  }  
 
$result = mysqli_query($con, $query);

$contador = 0;
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  $contador = $contador + 1;
  $row_array['codigo_aposta'] = $row['cod_aposta'];
  $row_array['codigo_jogo'] = $row['cod_jogo'];
  $row_array['codigo_bilhete'] = $row['cod_bilhete'];
  $row_array['aposta'] = $row['txt_aposta'];
  $row_array['valor_aposta'] = $row['valor_aposta'];
  $row_array['possivel_retorno'] = $row['possivel_retorno'];
  $row_array['tipo_jogo'] = $row['tipo_jogo'];

  array_push($arrayJogos, $row_array);
  if ($contador == mysqli_num_rows($result)) {
      break;
  }
}

$queryTabelaBicho = "select * from bicho";

$resultBicho = mysqli_query($con, $queryTabelaBicho);

$contador = 0;
while ($row = mysqli_fetch_array($resultBicho, MYSQLI_ASSOC)) {
  $contador = $contador + 1;
  $rowArray['codigo'] = $row['codigo'];
  $rowArray['descricao'] = $row['descricao'];
  $rowArray['numero_1'] = $row['numero_1'];
  $rowArray['numero_2'] = $row['numero_2'];
  $rowArray['numero_3'] = $row['numero_3'];
  $rowArray['numero_4'] = $row['numero_4'];
  array_push($arrayBichos, $rowArray);
  if ($contador == mysqli_num_rows($resultBicho)) {
      break;
  }
}


// $tabelaJogosJSON{"jogos"} = $arrayJogos;


// $tabelaBichoJSON{'bichos'} = '$arrayBichos';


// array_push($returnArray,$tabelaBichoJSON);
// array_push($returnArray,$arrayJogos);
// echo json_encode($returnArray, JSON_FORCE_OBJECT);
$matricula = date("Y-m-d H:i:s");
$resultadoConsulta = array(
  'apostas'   => $arrayJogos,
  'bichos'     => $arrayBichos,
  'data' => $matricula
);

echo json_encode($resultadoConsulta);
$con->close();