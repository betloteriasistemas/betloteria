<?php

include "conexao.php";
require_once 'dompdf/autoload.inc.php';
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_GET)) {
    die();
}

use Dompdf\Dompdf;

$dompdf = new Dompdf();

$codigo = mysqli_real_escape_string($con, $_GET['codigo']);
$cod_site = mysqli_real_escape_string($con, $_GET['cod_site']);

$query = " SELECT concurso from jogo
            WHERE COD_SITE = '$cod_site'
              and cod_jogo = '$codigo' ";
$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

$concurso = $row['concurso'];

//$codigo = 27;
//$cod_site = 1;

$query = " select distinct bil.cod_bilhete, usu.login, bil.nome_apostador,
            bil.telefone_apostador, DATE_FORMAT(bil.data_bilhete, '%d/%m/%Y %H:%i:%S') data_bilhete,
            bil.txt_pule, jogo.concurso,
            sum(apo.valor_aposta) total_bilhete,
            count(apo.cod_aposta) qtd_apostas
            from aposta apo
            inner join bilhete bil on (apo.cod_bilhete = bil.cod_bilhete)
            inner join usuario usu on (bil.cod_usuario = usu.cod_usuario)
            inner join jogo on (jogo.cod_jogo = apo.cod_jogo)
            where apo.cod_jogo = '$codigo'
              and apo.status = 'A'
              and bil.status_bilhete = 'A'
            group by bil.cod_bilhete, usu.login, bil.nome_apostador,
            bil.telefone_apostador, bil.data_bilhete, bil.txt_pule, jogo.concurso";
$result = mysqli_query($con, $query);

$array_bilhetes_pdf = array();

$qtd_geral = 0;
$total_geral = 0;
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

    $row_array['cod_bilhete'] = $row['cod_bilhete'];
    $row_array['login'] = $row['login'];
    $row_array['nome_apostador'] = $row['nome_apostador'];
    $row_array['telefone_apostador'] = $row['telefone_apostador'];
    $row_array['data_bilhete'] = $row['data_bilhete'];
    $row_array['txt_pule'] = $row['txt_pule'];
    $row_array['concurso'] = $row['concurso'];
    $row_array['total_bilhete'] = $row['total_bilhete'];
    $row_array['qtd_apostas'] = $row['qtd_apostas'];

    $qtd_geral = $qtd_geral + $row['qtd_apostas'];
    $total_geral = $total_geral + $row['total_bilhete'];

    array_push($array_bilhetes_pdf, $row_array);
}

if (count($array_bilhetes_pdf) == 0) {

    $html = "
   <html>
<body>
<hr>
  <h1> Concurso: " . $concurso . " sem apostas! </h1>
<hr>
</body>
</html> ";
} else {

    $query = " SELECT nome_banca from site
            WHERE COD_SITE = '$cod_site' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $html = "
   <html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 10px;
}
</style>
</head>
<body>
   <hr>
    <h1> Banca: " . $row['nome_banca'] . " - Concurso: " . $concurso . "</h1>
   <table style='width:100%'>
      <thead>
          <tr>
              <th scope='col'>#</th>
              <th scope='col'>Login</th>
              <th scope='col'>Nome Apostador</th>
              <th scope='col'>Telefone Apostador</th>
              <th scope='col'>Data Bilhete</th>
              <th scope='col'>Bilhete</th>
              <th scope='col'>Qtd. Apostas</th>
              <th scope='col'>Valor Bilhete</th>
          </tr>
      </thead>
      <tbody>";
    foreach ($array_bilhetes_pdf as $e) {
        $html = $html .
        "<tr>
              <th scope='row'> " . $e['cod_bilhete'] . "</th>
              <td> " . $e['login'] . " </td>
              <td> " . $e['nome_apostador'] . " </td>
              <td> " . $e['telefone_apostador'] . " </td>
              <td> " . $e['data_bilhete'] . " </td>
              <td> " . $e['txt_pule'] . " </td>
              <td align='center'> " . $e['qtd_apostas'] . " </td>
              <td align='right'> R$ " . number_format($e['total_bilhete'], 2, ',', '.') . " </td>
           </tr>
       </tbody>";
    }
    $html = $html . "<tfoot>
          <tr>
              <th scope='col'>&nbsp;</th>
              <th scope='col'>&nbsp;</th>
              <th scope='col'>&nbsp;</th>
              <th scope='col'>&nbsp;</th>
              <th scope='col'>&nbsp;</th>
              <th scope='col'>&nbsp;</th>
              <th align='center' scope='col'>" . $qtd_geral . "</th>
              <th align='right' scope='col'> R$ " . number_format($total_geral, 2, ',', '.') . "</th>
          </tr>
      </tfoot>
    </table> </body> </html> ";

}

$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
$dompdf->stream("Concurso " . $concurso . " .pdf");

$con->close();