<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];


$site = mysqli_real_escape_string($con, $_POST['site']);
$perfil = mysqli_real_escape_string($con, $_POST['perfil']);
$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$exibeCambista = mysqli_real_escape_string($con, $_POST['exibeCambista']);

if ($perfil == "G") {
    $query = "select g.*, g.saldo_par - (IFNULL(g.comissao_gerente,0)) as saldo from (
        select v.*, (v.entradas - v.saidas - v.comissao) saldo_par,
          round(((select (CASE WHEN v.Tipo_Jogo = 'S' THEN pct_comissao_seninha 
          WHEN v.Tipo_Jogo = 'Q' THEN  pct_comissao_quininha 
          WHEN v.Tipo_Jogo in ('B','G') THEN  pct_comissao_bicho 
          WHEN v.Tipo_Jogo = '2' THEN  pct_comissao_2PRA500 
          WHEN v.Tipo_Jogo = 'R' THEN  pct_comissao_rifa 
          WHEN v.Tipo_Jogo = '6' THEN  pct_comissao_6DASORTE ELSE 0 END) from usuario where cod_usuario = '$cod_usuario') * (v.entradas - v.saidas - v.comissao)) / 100 ,2) comissao_gerente
        from (
            select usu.cod_usuario, usu.nome, null as gerente,
            sum(apo.valor_aposta) entradas,
            count(apo.cod_aposta) qtd_apostas,
            sum(apo.comissao) as comissao,
            sum(apo.valor_ganho) as saidas
            from jogo
            inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
            inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
            inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
            where jogo.cod_site = '$site'
            and usu.cod_gerente = '$cod_usuario'
              and bil.status_bilhete not in ('C','F')
              and apo.status not in ('C') 
            AND jogo.TP_STATUS = 'P'
            group by usu.cod_usuario, usu.nome) v where 1 =1 order by nome desc) g; ";

} else if ($perfil == "A") {
    if($exibeCambista == 'false') {                    
        $query = "select cod_usuario,nome, ID_GERENTE,gerente,sum(entradas) as entradas, sum(qtd_apostas) as qtd_apostas, sum(comissao) as comissao, sum(saidas) as saidas,
                    (CASE WHEN (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) > 0 THEN
                        round(sum(v.comissao_gerente),2) 
                    ELSE 0 END) as comissao_gerente,
                    (sum(v.entradas) - sum(v.saidas) - sum(v.comissao))  saldo_par,
                    (CASE WHEN (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) > 0 THEN
                        ((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - (round(sum(v.comissao_gerente),2)))
                    ELSE (sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) END) as saldo,
                    sum(COALESCE(saldo_lancamento_gerente,0)) as saldo_lancamento_gerente,
                    sum(COALESCE(saldo_lancamento_cambista,0)) as saldo_lancamento_cambista,
                    (((sum(v.entradas) - sum(v.saidas) - sum(v.comissao)) - (sum(v.comissao_gerente)))+(saldo_lancamento_gerente+sum(saldo_lancamento_cambista))) saldo_pos_lancamento
            from (
            (select  usu.cod_usuario,
                        usu.nome, 
                        geren.cod_usuario as ID_GERENTE, 
                        geren.nome as gerente,  
                        DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y') data_jogo, 
                        cast(case
                        when jogo.tipo_jogo = 'B' then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), 
                            case when jogo.desc_hora is null then '' 
                                else concat('-',jogo.desc_hora) 
                                end) 
                        when jogo.tipo_jogo = 'R' then jogo.descricao
                        else jogo.concurso end as char) as concurso, 
                        TIME_FORMAT(jogo.hora_extracao, '%H:%i') hora_extracao, 
                        jogo.desc_hora, 
                        jogo.tp_status,  
                        jogo.tipo_jogo,
                    sum(apo.valor_aposta) entradas,
                    count(apo.cod_aposta) qtd_apostas,
                    sum(apo.comissao) as comissao,
                    sum(apo.comissao_gerente) as comissao_gerente,
                    sum(apo.valor_ganho) as saidas,
                    0 as saldo_lancamento_gerente,
                    0 as saldo_lancamento_cambista
                from jogo
                inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario 
                where jogo.cod_site = '$site'
                and geren.cod_gerente = '$cod_usuario'
                and bil.status_bilhete not in ('C','F')
                and apo.status != 'C' 
                AND jogo.TP_STATUS = 'P'
                group by geren.cod_usuario)
            union all
            (select U.cod_usuario, 
                    U.nome, 
                    U.cod_usuario as ID_GERENTE, 
                    U.nome as gerente, 
                    DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,
                    'LANÇAMEN- TO' as concurso, 
                    null as hora_extracao, 
                    L.motivo as desc_hora, 
                    null as TP_STATUS, 
                    null as tipo_jogo,
                    0 as entradas, 
                    0 as qtd_apostas, 
                    0 as comissao, 
                    0 as comissao_gerente,
                    0 as saidas,
                    sum(case when L.tipo_lancamento = 'E' then (L.valor) else (L.valor*-1) end) as saldo_lancamento_gerente,
                    0 as saldo_lancamento_cambista
                from lancamento L
                    inner join usuario U on (L.COD_USUARIO_lancamento = U.cod_usuario)                                
                where L.cod_site = '$site' 
                and L.COD_USUARIO_lancamento in (select cod_usuario 
                                                    from usuario 
                                                    where cod_gerente = '$cod_usuario')
                    and L.ajuda_custo = 'N' and L.status='A'
                group by U.cod_usuario)
            union all 
            (select U.cod_usuario, 
                    U.nome, 
                    U.cod_gerente as ID_GERENTE, 
                    geren.nome as gerente, 
                    DATE_FORMAT(L.data_hora, '%d/%m/%Y') as data_jogo,
                    'LANÇAMEN- TO' as concurso, 
                    null as hora_extracao, 
                    L.motivo as desc_hora, 
                    null as TP_STATUS, 
                    null as tipo_jogo,
                    0 as entradas, 
                    0 as qtd_apostas, 
                    0 as comissao, 
                    0 as comissao_gerente,
                    0 as saidas,
                    0 as saldo_lancamento_gerente,
                    sum(case when L.tipo_lancamento = 'E' then (L.valor) else (L.valor*-1) end) as saldo_lancamento_cambista
                from lancamento L
                    inner join usuario U on (L.COD_USUARIO_lancamento = U.cod_usuario)
                    inner join usuario geren on (U.cod_gerente = geren.cod_usuario)
                where L.cod_site = '$site' 
                    and L.COD_USUARIO_lancamento in (select U1.cod_usuario 
                                                        from usuario U1, usuario U2 
                                                        where U1.cod_gerente = U2.cod_usuario 
                                                        and U2.cod_gerente = '$cod_usuario') 
                    and L.ajuda_custo = 'N' 
                    and L.status='A'
                group by U.cod_gerente)
                ) v 
                where 1 =1 group by ID_GERENTE order by gerente asc ";
    } else {
        $query = "select g.*, g.saldo_par - (IFNULL(g.comissao_gerente,0)) as saldo from (
            select v.*, (v.entradas - v.saidas - v.comissao) saldo_par,
            round(((select (CASE WHEN v.Tipo_Jogo = 'S' THEN pct_comissao_seninha 
            WHEN v.Tipo_Jogo = 'Q' THEN  pct_comissao_quininha 
            WHEN v.Tipo_Jogo in ('B','G') THEN  pct_comissao_bicho 
            WHEN v.Tipo_Jogo = '2' THEN  pct_comissao_2PRA500
            WHEN v.Tipo_Jogo = 'R' THEN  pct_comissao_rifa 
            WHEN v.Tipo_Jogo = '6' THEN  pct_comissao_6DASORTE ELSE 0 END) from usuario u where u.cod_usuario = ID_GERENTE) * (v.entradas - v.saidas - v.comissao)) / 100,2) comissao_gerente
            from (
                select usu.cod_usuario, usu.nome, geren.cod_usuario as ID_GERENTE, geren.nome as gerente,
                sum(apo.valor_aposta) entradas,
                count(apo.cod_aposta) qtd_apostas,
                sum(apo.comissao) as comissao,
                sum(apo.valor_ganho) as saidas
                from jogo
                inner join aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
                inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
                inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
                where jogo.cod_site = '$site'
                and geren.cod_gerente = '$cod_usuario'
                    and bil.status_bilhete not in ('C','F')
                    and apo.status not in ('C') 
                AND jogo.TP_STATUS = 'P'
                group by usu.cod_usuario, usu.nome) v where 1 =1 order by gerente,nome desc) g";
    }         
}

// CONTROLE PARA VERIFICAR SE AS COMISSOES DOS GERENTES POR MODALIDADE SAO IGUAIS - INICIO
$query_gerentes = "SELECT NOME, FLG_SENINHA, PCT_COMISSAO_SENINHA, 
                                FLG_QUININHA, PCT_COMISSAO_QUININHA, 
                                FLG_LOTINHA, PCT_COMISSAO_LOTINHA,
                                FLG_SUPER_SENA, PCT_COMISSAO_SUPERSENA,
                                FLG_BICHO, PCT_COMISSAO_BICHO,
                                FLG_RIFA, PCT_COMISSAO_RIFA,
                                FLG_2PRA500, PCT_COMISSAO_2PRA500
                    FROM USUARIO 
                    WHERE COD_SITE = '$site' 
                    AND perfil = 'G' 
                    AND STATUS = 'A'";
$result_gerentes = mysqli_query($con, $query_gerentes);
$ctrl_comissoes_gerentes_arr = array(); 
$contador = 0;
while ($row = mysqli_fetch_array($result_gerentes, MYSQLI_ASSOC)) {
    $contador = $contador + 1;

    $comissoes_arr = array();
    if ($row['FLG_SENINHA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_SENINHA']);
    }

    if ($row['FLG_QUININHA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_QUININHA']);
    }

    if ($row['FLG_LOTINHA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_LOTINHA']);
    }

    if ($row['FLG_SUPER_SENA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_SUPERSENA']);
    }

    if ($row['FLG_BICHO'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_BICHO']);
    }

    if ($row['FLG_RIFA'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_RIFA']);
    }

    if ($row['FLG_2PRA500'] == 'S') {
        array_push($comissoes_arr, $row['PCT_COMISSAO_2PRA500']);
    }
    $valor_comissao = 0;
    $comissoes_iguais = 1;
    $contador = 0;
    foreach ($comissoes_arr as $comissao) {
        if ($contador == 0) {
            $valor_comissao = $comissao;
        }
        $contador++;
        if ($comissao != $valor_comissao) {
            $comissoes_iguais = 0;
            break;
        }
    }

    if ($valor_comissao > 0) {
        $valor_comissao = $valor_comissao / 100;
    }

    $row_array['gerente'] = $row['NOME'];
    $row_array['comissoes_iguais'] = $comissoes_iguais;
    $row_array['comissao'] = $valor_comissao;
    array_push($ctrl_comissoes_gerentes_arr, $row_array);

    if ($contador == mysqli_num_rows($result_gerentes)) {
        break;
    }
}
// CONTROLE PARA VERIFICAR SE AS COMISSOES DOS GERENTES POR MODALIDADE SAO IGUAIS - FIM

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    
    if ($row['cod_usuario'] != null) {
        $row_array['cod_usuario'] = $row['cod_usuario'];
        $row_array['nome'] = $row['nome'];
        
        $row_array['saldo'] = $row['saldo'];
        
        $row_array['entradas'] = $row['entradas'];
        $row_array['qtd_apostas'] = $row['qtd_apostas'];
    
        $row_array['comissao'] = $row['comissao'];
        $row_array['saidas'] = $row['saidas'];
        $row_array['gerente'] = $row['gerente'];
        $row_array['saldo_par'] = $row['saldo_par'];
    
        //$row_array['saldo_pos_lancamento'] = $row['saldo_pos_lancamento'];
        $row_array['saldo_lancamento_gerente'] = $row['saldo_lancamento_gerente'];
        $row_array['saldo_lancamento_cambista'] = $row['saldo_lancamento_cambista'];

        $comissoes_iguais = 0;
        $valor_comissao = 0;
        foreach ($ctrl_comissoes_gerentes_arr as $ctrl_comissao) {
            if ($ctrl_comissao['gerente'] == $row['nome']) {
                $comissoes_iguais = $ctrl_comissao['comissoes_iguais'];
                $valor_comissao = $ctrl_comissao['comissao'];
                break;
            }
        }

        if ($comissoes_iguais == 1 && $row['comissao_gerente'] != 0) {
            // CASO TODAS AS COMISSOES POR MODALIDADE SEJAM IGUAIS, EH REALIZADO O CALCULO DA COMISSAO
            $row_array['comissao_gerente'] = $saldoGerente * $valor_comissao;
        } else {
            $row_array['comissao_gerente'] = $row['comissao_gerente'];
        }
    
        array_push($return_arr, $row_array);
    
    }

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}

$con->close();
echo json_encode($return_arr);
?>
