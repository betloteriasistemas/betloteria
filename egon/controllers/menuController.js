angular.module('main').controller('menuCtrl', function ($scope, user, configGeral, $location, notificacoes, funcoes) {
    var labelsHeader = [];
    $(document).ready(function(){
		$('.table').find('thead th').each(function(){
			labelsHeader.push($(this).text());
        })
        // seleciona o nó alvo
        var corpoTabela = $('.table tbody')[0];
        // cria uma nova instância de observador
        var observer = new MutationObserver(function() {
            $('.table tbody').find('tr').each(function() {
                $(this).find('td').each(function(index) {
                    $(this).attr('data-label', labelsHeader[index]);
                })
            });
            
            if ($('tfoot')[0]) {
                $('tfoot tr td').each(function(index) {
                    if ($(this).text() == '\xa0') {
                        $(this).attr('data-remove', 'true');
                    } else {
                        $(this).attr('data-label', labelsHeader[index]);
                    }
                })
            }
        });
        
        var config = { childList: true };
        if (corpoTabela) {
            observer.observe(corpoTabela, config);
        }
        
	});
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.saldo = user.getSaldoAtualizado();
    $scope.site = user.getSite();
    $scope.escolhido = user.getEscolhido();

    $scope.flg_seninha = false;
    $scope.flg_quininha = false;
    $scope.flg_lotinha = false;
    $scope.flg_bicho = false;
    $scope.flg_2pra500 = false;
    $scope.flg_rifa = false;
    $scope.flg_multijogos = false;
    $scope.flg_supersena = false;
    $scope.existe_area_bicho = false;
    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.flg_seninha = user.getflg_seninha() == 'S' && data.flg_seninha;
        $scope.flg_quininha = user.getflg_quininha() == 'S' && data.flg_quininha;
        $scope.flg_lotinha = user.getflg_lotinha() == 'S' && data.flg_lotinha;
        $scope.flg_bicho = user.getflg_bicho() == 'S' && data.flg_bicho;
        $scope.flg_2pra500 = user.getflg_2pra500() == 'S' && data.flg_2pra500;
        $scope.flg_rifa = user.getflg_rifa() == 'S' && data.flg_rifa;
        $scope.flg_multijogos = user.getflg_multijogos() == 'S' && data.flg_multijogos;
        $scope.flg_supersena = user.getflg_supersena() == 'S' && data.flg_supersena;
        $scope.desc_2pra500 = funcoes.getDescricaoTipoJogo('2', $scope.desc_2pra500);
        $scope.existe_area_bicho = data.existe_area_bicho;
    });

    notificacoes.get().then(function (data) {
        $scope.notificacoes = data;
    });

    if ($scope.escolhido == 'true') {
        $scope.logomarca = "img/" + $scope.site + ".png";

        var request = new XMLHttpRequest();
        request.open('HEAD', $scope.logomarca, false);
        request.send();
        if (request.status != 200) {
            $scope.logomarca = "img/logo_transparente.png";
        }
    } else {
        $scope.logomarca = "img/logo_transparente.png";
    }
    


    $scope.getManual = function () {
        if ($scope.perfil == 'C') {
            return "downloads/manualcambista.docx";
        } else if ($scope.perfil == 'G') {
            return "downloads/manualge84.docx";
        } else if ($scope.perfil == 'A') {
            return "downloads/manualad36.docx";
        }
    }

    $scope.reiniciaRepeticaoDeBilhete = function (url) {
        user.setRepetirBilhete(false);
        $location.path("/" + url);
    }

});