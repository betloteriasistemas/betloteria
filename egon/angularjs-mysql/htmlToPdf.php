<?php

include "conexao.php";
require_once 'vendor/autoload.php';

if (!isset($_GET)) {
    die();
}

$cod_auditoria = $_GET['cod_auditoria'];
$query = "SELECT TXT_DESCRICAO
            from AUDITORIA
            where COD_AUDITORIA = '$cod_auditoria'";

$result = mysqli_query($con, $query);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
$html = $row['TXT_DESCRICAO'];

$hoje = date('YmdHis');
$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->Output($hoje . "_Relatorio_Prestacao_de_Contas.pdf", "D");

