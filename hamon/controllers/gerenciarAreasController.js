angular.module('main').controller('gerenciarAreasCtrl', function ($rootScope, $scope, user, $http, $route) {
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.site = user.getSite();

    $scope.listarAreas = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/areas.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&operacao=listar' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;
            //$scope.records = JSON.stringify(response.data);			
        }, function (response) {
            console.log(response);
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.getStatusArea = function (status) {
        if (status == "A") {
            return "Ativo";
        } else if (status == "I") {
            return "Inativo";
        }
    }

    $scope.alterar = function (area) {
        $scope.edtNome = area.nome;
        $scope.edtDescricao = area.descricao;
        $scope.cod_area = area.cod_area;
    }
    
    $scope.salvar = function (codigo) {
        var nome = $scope.edtNome;
        var descricao = $scope.edtDescricao;
        var cod_site = user.getSite();

        if (typeof codigo === "undefined") {
            codigo = 0;
        }

        if (typeof nome === "undefined" || nome === "") {
            $.alert('Informe o NOME!');
        } else if (typeof descricao === "undefined" || descricao === "") {
            $.alert('Informe a DESCRIÇÃO!')
        } else {
            $rootScope.loading = true;
            $http({

                url: 'angularjs-mysql/areas.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo() + 
                      '&site=' + cod_site + 
                      '&codigo=' + codigo + 
                      '&nome=' + nome + 
                      '&descricao=' + descricao + 
                      '&operacao=inserir' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $rootScope.loading = false;
                if (response.data.status == 'OK') {
                    $route.reload();
                } else {
                    $.alert(response.data.mensagem);
                }
            })
        }
    }

    $scope.excluir = function (cod_area, nome_area, status) {

        var stString = '';
        if (status == 'A') {
            stString = 'Inativação';
        } else {
            stString = 'Ativação';
        }

        $.confirm({
            title: '',
            content:"Confirma " + stString + " da Área: " + nome_area + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    var cod_site = user.getSite();
                    $http({
                        url: 'angularjs-mysql/areas.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() 
                        + '&site=' + cod_site 
                        + '&cod_area=' + cod_area 
                        + '&nome_area=' + nome_area 
                        + "&status=" + status 
                        + '&operacao=excluir'
                        + '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    }

});