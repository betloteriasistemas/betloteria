angular.module('main').controller('resultadoCtrl', function ($scope, $rootScope, $http, $route, $filter) {

    $scope.codigo = 0;
    $scope.data = new Date();
    $scope.tipoJogo = '';
    $scope.tiposJogos = [{ nome: 'Seninha', codigo: 'S' }, { nome: 'Quininha', codigo: 'Q' },
    { nome: 'Lotinha', codigo: 'L' }, { nome: 'Bicho', codigo: 'B' }];
    $scope.concurso = null;
    $scope.extracao = null;


    $scope.listarResultados = function () {
        var data  = $filter('date')($scope.data, "yyyy-MM-dd");
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/resultado.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=listar&data=' + data + '&tipoJogo=' + $scope.tipoJogo
        }).then(function (response) {
            $rootScope.loading = false;
            $scope.records = response.data;
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.salvar = function () {
        var codigo = $scope.codigo;
        var data_jogo = $filter('date')($scope.data, "yyyy-MM-dd");
        var tipo_jogo = $scope.tipoJogo;
        var concurso = $scope.concurso;
        var hora_extracao = $scope.comboHorario;
        var descricao_extracao =  $('#nomeBanca :selected').text().replace(/(\r\n|\n|\r)/gm, "").trim();
        
        var numero1 = $scope.numero1 || null;
        var numero2 = $scope.numero2 || null;
        var numero3 = $scope.numero3 || null;
        var numero4 = $scope.numero4 || null;
        var numero5 = $scope.numero5 || null;
        var numero6 = $scope.numero6 || null;
        var numero7 = $scope.numero7 || null;
        var numero8 = $scope.numero8 || null;
        var numero9 = $scope.numero9 || null;
        var numero10 = $scope.numero10 || null;
        var numero11 = $scope.numero11 || null;
        var numero12 = $scope.numero12 || null;
        var numero13 = $scope.numero13 || null;
        var numero14 = $scope.numero14 || null;
        var numero15 = $scope.numero15 || null;
        
        $operacao = "inserir";
        if (codigo != 0) {
            $operacao = "atualizar";
        }

        if (data_jogo == "" || data_jogo == null) {
            $.alert('Informe a data do resultado!');
            return;
        }

        if (tipo_jogo == "") {
            $.alert('Informe o tipo de jogo!');
            return;
        }

        
        if (tipo_jogo != 'B' && concurso == null) {
            $.alert('Informe a banca e o horário!');
            return;
        }

        if (tipo_jogo == 'B' && (hora_extracao == null || descricao_extracao == null)) {
            $.alert('Informe a banca e o horário!');
            return;
        }

        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/resultado.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 
                'codigo=' + codigo +
                '&data=' + data_jogo +
                '&tipo=' + tipo_jogo +
                '&concurso=' + concurso +
                '&horaExtracao=' + hora_extracao + 
                '&descricaoExtracao=' + descricao_extracao +
                '&numero1=' + numero1 +
                '&numero2=' + numero2 +
                '&numero3=' + numero3 +
                '&numero4=' + numero4 +
                '&numero5=' + numero5 +
                '&numero6=' + numero6 +
                '&numero7=' + numero7 +
                '&numero8=' + numero8 +
                '&numero9=' + numero9 +
                '&numero10=' + numero10 +
                '&numero11=' + numero11 +
                '&numero12=' + numero12 +
                '&numero13=' + numero13 +
                '&numero14=' + numero14 +
                '&numero15=' + numero15 +
                '&operacao=' + $operacao
        }).then(function (response) {
            $rootScope.loading = false;
            if (response.data.status == 'OK') {
                if (codigo != 0) {
                    $.alert('Resultado atualizado com sucesso!');
                } else {
                    $.alert('Resultado criado com sucesso!');
                }
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status);
        });
    }

    $scope.alterar = function(resultado) {
        $scope.codigo = resultado.codigo;
        $scope.data = new Date(resultado.data_jogo.replace(/-/g, '\/'));
        $scope.tipoJogo = resultado.tipo_jogo;
        $scope.concurso = resultado.concurso;
        $scope.numero1 = resultado.numero_1;
        $scope.numero2 = resultado.numero_2;
        $scope.numero3 = resultado.numero_3;
        $scope.numero4 = resultado.numero_4;
        $scope.numero5 = resultado.numero_5;
        $scope.numero6 = resultado.numero_6;
        $scope.numero7 = resultado.numero_7;
        $scope.numero8 = resultado.numero_8;
        $scope.numero9 = resultado.numero_9;
        $scope.numero10 = resultado.numero_10;
        $scope.numero11 = resultado.numero_11;
        $scope.numero12 = resultado.numero_12;
        $scope.numero13 = resultado.numero_13;
        $scope.numero14 = resultado.numero_14;
        $scope.numero15 = resultado.numero_15;
    }

    $scope.excluir = function (codigo) {
        $.confirm({
            title: '',
            content:"Confirma a exclusão do resultado?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $rootScope.loading = true;
                    $http({
                        url: 'angularjs-mysql/resultado.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 
                            'codigo=' + codigo +
                            '&operacao=excluir'
                    }).then(function (response) {
                        $rootScope.loading = false;
                        if (response.data.status == 'OK') {
                            $.alert('Resultado excluído com sucesso!');
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }, function (response) {
                        $rootScope.loading = false;
                        $.alert('Erro no $HTTP: ' + response.status);
                    });
                }
            }
        });
    }

    $scope.getExtracoes = function() {
        $rootScope.loading = true;
        $http(
          {
            method: "POST",
            url: "angularjs-mysql/resultado.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "operacao=" + "listar_bancas"
          }
        ).then(function(response) {
          $scope.extracoes = response.data;
        }).catch(function (response) {            
          $.alert('Erro no $HTTP: ' + response.status)
        }).finally(function() {
          $rootScope.loading = false;
        });
    }

    $scope.getHorarios = function() {
        $rootScope.loading = true;
        $scope.comboHorario = "";
        var cod_extracao = $scope.comboBanca;
        $http(
          {
            method: "POST",
            url: "angularjs-mysql/resultado.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "operacao=listar_horarios_banca"
              + "&cod_extracao=" + cod_extracao
          }
        ).then(function(response) {
          $scope.horarios = response.data;
        }).catch(function (response) {            
          $.alert('Erro no $HTTP: ' + response.status)
        }).finally(function() {
          $rootScope.loading = false;
        });
      }
    
    $scope.showInputPremio = function (premio) {
        switch (String($scope.tipoJogo)) {
            case 'Q':
                return premio >= 1 && premio <= 5;
            case 'S':
            case 'U':
                return premio >= 1 && premio <= 6;
            case 'L':
                return premio >= 1 && premio <= 15;
            case 'B':
                if ($scope.extracao && $scope.extracao.qtd_premios) {
                    return premio <= $scope.extracao.qtd_premios;
                }
                return premio >= 1 && premio <= 10;
            default: return false;
        }
    }

    $scope.getDescricaoTipoJogo = function (tipo_jogo) {
        switch (tipo_jogo) {
            case 'S':
                return "Seninha";
            case 'Q':
                return "Quininha";
            case 'L':
                return "Lotinha";
            case 'B':
                return "Bicho";
            case 2:
                return $scope.desc_2pra500;
            case 'R':
                return "Rifa";
            case 'U':
                return "Super Sena";
            default:
                return "";
        }
    }

    $scope.getDescricaoConcurso = function(resultado){
        switch (resultado.tipo_jogo) {
            case 'B':
            case 2:
                return resultado.hora_extracao + " - " + resultado.descricao_extracao;
            case 'S':
            case 'Q':
            case 'L':
            case 'U':
                return resultado.concurso;
            case 'R':
                return resultado.descricao;
            default:
                return "";
        }
    }

    $scope.getNumerosSorteados = function(resultado) {
        switch (resultado.tipo_jogo) {
            case 'B':    
                return resultado.numero_1 + " - " + resultado.numero_2 +  " - " + resultado.numero_3 + " - " +
                       resultado.numero_4 + " - " + resultado.numero_5 +  " - " + resultado.numero_6 + " - " +
                       resultado.numero_7 + " - " + resultado.numero_8 +  " - " + resultado.numero_9 + " - " +
                       resultado.numero_10;
            case 'S':
            case 'U':
                return resultado.numero_1 + " - " + resultado.numero_2 +  " - " + resultado.numero_3 + " - " +
                       resultado.numero_4 + " - " + resultado.numero_5 +  " - " + resultado.numero_6;
            case 'Q':
                return resultado.numero_1 + " - " + resultado.numero_2 +  " - " + resultado.numero_3 + " - " +
                       resultado.numero_4 + " - " + resultado.numero_5 ;
            case 'L':
                return resultado.numero_1 + " - " + resultado.numero_2 +  " - " + resultado.numero_3 + " - " +
                       resultado.numero_4 + " - " + resultado.numero_5 +  " - " + resultado.numero_6 + " - " +
                       resultado.numero_7 + " - " + resultado.numero_8 +  " - " + resultado.numero_9 + " - " +
                       resultado.numero_10 + " - " + resultado.numero_11 +  " - " + resultado.numero_12 + " - " +
                       resultado.numero_13 + " - " + resultado.numero_14 +  " - " + resultado.numero_15;
            default:
                return "";
        }
    }

})