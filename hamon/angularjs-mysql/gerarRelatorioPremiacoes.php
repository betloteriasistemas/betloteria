<?php

include "conexao.php";
require_once 'vendor/autoload.php';

if (!isset($_GET)) {
	die();
}

function getResumoDescricaoConcurso($registro)
{
	switch ($registro['tipo_jogo']) {
		case 'B':
		case '2':
			return $registro['hora_extracao'] . " - " . $registro['desc_hora'];
		case 'S':
		case 'Q':
		case 'L':
		case 'U':
			return $registro['concurso'];
		case 'R':
			return $registro['descricao'];
		default:
			return "";
	}
}

function getDescricaoTipoJogo($tipoJogoDoRegistro)
{
	switch ($tipoJogoDoRegistro) {
		case 'S':
			return "Seninha";
		case 'U':
			return "Super Sena";			
		case 'Q':
			return "Quininha";
		case 'L':
			return "Lotinha";
		case 'B':
			return "Bicho";
		case '2':
			return $desc_2pra500;
		case 'R':
			return "Rifa";
		default:
			return "";
	}	
}

$cod_usuario = mysqli_real_escape_string($con, $_GET['cod_usuario']);
$perfil_usuario = mysqli_real_escape_string($con, $_GET['perfil_usuario']);
$site = mysqli_real_escape_string($con, $_GET['site']);
$index = mysqli_real_escape_string($con, $_GET['index']);
$qtdElementos = mysqli_real_escape_string($con, $_GET['qtdElementos']);
$sort = mysqli_real_escape_string($con, $_GET['sort']);
$sortType = filter_var(mysqli_real_escape_string($con, $_GET['sortType']), FILTER_VALIDATE_BOOLEAN);
$concurso = mysqli_real_escape_string($con, $_GET['concurso']);
$vendedor = mysqli_real_escape_string($con, $_GET['vendedor']);
$tipoJogo = mysqli_real_escape_string($con, $_GET['tipoJogo']);
$statusPgto = mysqli_real_escape_string($con, $_GET['pago']);
$dataDe = mysqli_real_escape_string($con, $_GET['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_GET['dataAte']);
$finalizados = mysqli_real_escape_string($con, $_GET['finalizados']);
$descTipoJogo = mysqli_real_escape_string($con, $_GET['descTipoJogo']);
$descConcurso = mysqli_real_escape_string($con, $_GET['descConcurso']);
$descVendedor = mysqli_real_escape_string($con, $_GET['descVendedor']);
$tipoRelatorio = mysqli_real_escape_string($con, $_GET['tipoRelatorio']);
$desc_2pra500 = mysqli_real_escape_string($con, $_GET['desc_2pra500']);


if($dataDe == "undefined") {
    $dataDe = 0;
}

if($dataAte == "undefined") {
    $dataAte = 0;
}

$exibeFinalizados = "SIM";
if($finalizados == 'false') {
    $exibeFinalizados = "NÃO";
}

$response = [];

$status = 'P';

if ($finalizados == 'true') {
	$status =  implode("','", array('P', 'F'));
}

$query = "
select b.`TXT_PULE`, DATE_FORMAT(b.`DATA_BILHETE`, '%d/%m/%Y %H:%i:%S') DATA_HORA_BILHETE, b.`STATUS_PAGAMENTO`,
	   b2.VALOR_GANHO,
	   j.`CONCURSO`, j.`TIPO_JOGO`, j.`HORA_EXTRACAO`, j.`DESC_HORA`, j.VALOR, j.DESCRICAO,
	   c.`NOME` as `CAMBISTA`, g.`NOME` as `GERENTE`
from bilhete b
inner join usuario c on (b.`COD_USUARIO` = c.`COD_USUARIO` )
inner join usuario g on (c.`COD_GERENTE` = g.`COD_USUARIO` )
inner join 
(select b.`COD_BILHETE`, a.`COD_JOGO`, a.`COD_SITE`, sum(a.`VALOR_GANHO`) as VALOR_GANHO
from bilhete b
inner join aposta a on (b.`COD_BILHETE` = a.`COD_BILHETE` and b.`COD_SITE` = a.`COD_SITE` )
where b.`STATUS_BILHETE` IN ('$status') and a.`STATUS` = 'G' and a.`COD_SITE` = '$site'
group by b.`COD_BILHETE`, a.`COD_JOGO`, a.`COD_SITE`
) b2 on (b.`COD_BILHETE` = b2.`COD_BILHETE`)
inner join jogo j on (b2.`COD_JOGO` = j.`COD_JOGO` and b2.`COD_SITE` = j.`COD_SITE` )
WHERE 1=1";

if ($concurso) {
	$query = $query . " AND j.COD_JOGO = '$concurso' ";
}

if ($tipoJogo) {
	$query = $query . " AND j.TIPO_JOGO = '$tipoJogo' ";
}

if ($vendedor) {
	switch ($perfil_usuario) {
		case 'A':
			$query = $query . " AND (g.`COD_USUARIO` = '$vendedor') ";
			break;
		default:
			$query = $query . " AND (c.`COD_USUARIO` = '$vendedor') ";
			break;
	}
} else {
	switch ($perfil_usuario) {
		case 'G':
			$query = $query . " AND (g.`COD_USUARIO` = '$cod_usuario') ";
			break;
		case 'C':
			$query = $query . " AND (c.`COD_USUARIO` = '$cod_usuario') ";
			break;
		default:
			break;
	}
}

if ($statusPgto) {
	$query = $query . " AND (b.`STATUS_PAGAMENTO` = '$statusPgto') ";
}

if ($dataDe != '0') {
	$query = $query . " AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d')  ";
}

if ($dataAte != '0') {
	$query = $query . " AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d')  ";
}

if ($dataDe == 0 && $dataAte == 0) {
	$query = $query . " AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d')  >= DATE_FORMAT(current_date - 7, '%Y%m%d')
			   AND DATE_FORMAT(b.`DATA_BILHETE`, '%Y%m%d')  <= DATE_FORMAT(current_date + 1, '%Y%m%d') ";
}
$queryCount = "select count(*) as total from (" . $query . ") t";
$totalItens = mysqli_query($con, $queryCount);

$asc_desc = $sortType ? " ASC" : " DESC";
switch ($sort) {
	case "CODIGO":
		$query = $query . "ORDER BY TXT_PULE";
		break;
	case "GERENTE":
		$query = $query . "ORDER BY GERENTE";
		break;
	case "CAMBISTA":
		$query = $query . "ORDER BY CAMBISTA";
		break;
	case "CONCURSO":
		$query = $query . "ORDER BY CONCURSO";
		break;
	case "TIPO_JOGO":
		$query = $query . "ORDER BY TIPO_JOGO";
		break;
	case "DATA":
		$query = $query . "ORDER BY STR_TO_DATE(DATA_HORA_BILHETE, '%d/%m/%Y %H:%i:%s')";
		break;
	case "RETORNO":
		$query = $query . "ORDER BY VALOR_GANHO";
		break;
	case "STATUS_PGTO":
		$query = $query . "ORDER BY case 
                        when STATUS_PAGAMENTO = 'N' then 1
                        else 0 end";
		break;
	default:
		$query = $query . "ORDER BY STR_TO_DATE(DATA_HORA_BILHETE, '%d/%m/%Y %H:%i:%s')";
		break;
}

$query = $query . $asc_desc . " LIMIT $index,$qtdElementos ";
$result = mysqli_query($con, $query);
$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
	$row_array['txt_pule'] = $row['TXT_PULE'];
	$row_array['data_bilhete'] = $row['DATA_HORA_BILHETE'];
	$row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
	$row_array['desc_hora'] = $row['DESC_HORA'];
	$row_array['valor'] = $row['VALOR'];
	$row_array['descricao'] = $row['DESCRICAO'];
	$row_array['cambista'] = $row['CAMBISTA'];
	$row_array['gerente'] = $row['GERENTE'];
	$row_array['concurso'] = $row['CONCURSO'];
	$row_array['tipo_jogo'] = $row['TIPO_JOGO'];
	$row_array['valor_ganho'] = $row['VALOR_GANHO'];
	$row_array['status_pagamento'] = $row['STATUS_PAGAMENTO'];
	array_push($return_arr, $row_array);
	if ($contador == mysqli_num_rows($result)) {
		break;
	}
};

$data_de = date_create($dataDe);
$data_ate = date_create($dataAte); 

$html = "
<html>
	<head>
		<meta charset='utf-8'>
		<style>
			table, th, td {
				border-collapse: collapse;
				font-family: Arial, Helvetica, sans-serif;
			}
			th, td {
				border: 1px solid black;
				padding: 3px;
			}
		</style>
	</head>
<body>
	<div class='col-md-12 col-ms-12 col-xs-12'>
		<div class='table-responsive betL_CaixaList' style='padding-top: 30px'>
			<h1> PREMIAÇÕES </h1>			 
			<div style='display: inline;'>";

if ($tipoJogo) {
	$html = $html . 
			   "<span style='font-size: 12pt;'> <b>Tipo de Jogo:</b> " . $descTipoJogo . "</span>
				&nbsp;|&nbsp;";
}

if ($concurso) {
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>Concurso:</b> " . $descConcurso . "</span>
				&nbsp;|&nbsp;";	
}

if ($vendedor) {
	$labelVendedor = "Gerente:";
	if ($perfil_usuario == "G") {
		$labelVendedor = "Cambista:";
	}
	$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>" . $labelVendedor . "</b> " . $descVendedor . "</span>
				&nbsp;|&nbsp;";
}

if ($statusPgto) {
	$descricaoStatusPgto = "Efetivados";
	if ($statusPgto == "N") {
		$descricaoStatusPgto = "Pendentes";
	}
	$html = $html . 
			   "<span style='font-size: 12pt;'> <b>Status Pagamento:</b> " . $descricaoStatusPgto . "</span>
				&nbsp;|&nbsp;";
}

$html = $html . 
		   	   "<span style='font-size: 12pt;'> <b>De:</b> " . date_format($data_de, 'd-m-Y') . "</span>
				&nbsp;|&nbsp;
				<span style='font-size: 12pt;'> <b>Até:</b> " . date_format($data_ate, 'd-m-Y') . "</span>
				&nbsp;|&nbsp;
				<span style='font-size: 12pt;'> <b>Exibe Finalizados:</b> " . $exibeFinalizados . "</span>
				&nbsp;
			</div>
			<hr>					 
			<table class='table table-hover' style='width: 100%;'>
				<thead>
					<tr style='background-color: #2E64FE; color: white;'>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>#</th>";
if ($perfil_usuario == 'A') {
	$html = $html .    "<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Gerente</th>";
}									
$html = $html .
					   "<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Cambista</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Concurso</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Tipo de Jogo</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Data</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Retorno</th>
						<th style='text-align: center; color: white; font-weight: bold;' scope='col'>Situação Pagamento</th>
					</tr>
				</thead>
				<tbody class='container' id='conteudoTabela'>";
$odd = 0;				
foreach ($return_arr as $registro) {
	$odd = $odd + 1;
	if($odd % 2 == 0) {
		$html = $html . 
				   "<tr style='background-color: #E6E6E6;'>";
	} else {
		$html = $html . 
				   "<tr>";
	}
	$html = $html .
				   	   "<td style='text-align:center'>" . $registro['txt_pule'] . "</td>";
	if ($perfil_usuario == 'A') {
		$html = $html .
				   	   "<td style='text-align:center'>" . $registro['gerente'] . "</td>";
	}
	$html = $html .
				   	   "<td style='text-align:center'>" . $registro['cambista'] . "</td>
						<td style='text-align:center'>" . getResumoDescricaoConcurso($registro) . "</td>
						<td style='text-align:center'>" . getDescricaoTipoJogo($registro['tipo_jogo']) . "</td>
						<td style='text-align:center'>" . $registro['data_bilhete'] . "</td>
						<td style='text-align:center'>" . $registro['valor_ganho'] . "</td>";
	if ($registro['status_pagamento'] == 'S') {
		$html = $html .
					   "<td style='text-align:center'>
							<span style='color: #008000; font-weight: bold;'>
								Efetivado
							</span>
						</td>";
	} else {
		$html = $html .
					   "<td style='text-align:center'>
							<span style='color: #ff0000; font-weight: bold;'>
								Pendente
							</span>
						</td>";
	}	  
}
$html = $html .
				   "</tr>
				</tbody>		
			</table>				 
		</div>
	</div>
 </body> 
</html> ";

$hoje = date('YmdHis');

if ($tipoRelatorio == 'xlsx') {
    $arquivo = "Premiacoes_" . $hoje . ".xls";
	header ("Expires: Mon, 18 Nov 1985 18:00:00 GMT");
	header ("Last-Modified: ".gmdate("D,d M YH:i:s")." GMT");
	header ("Cache-Control: no-cache, must-revalidate");
	header ("Pragma: no-cache");
	header ("Content-type: application/x-msexcel");
	header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");	
	header ("Content-Type: text/html; charset=UTF-8");
    
    // Imprime o conteúdo da nossa tabela no arquivo que será gerado
    echo $html;    
} else if ($tipoRelatorio == 'pdf') {
    $arquivo = "Premiacoes_" . $hoje . ".pdf";
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    $mpdf->Output($arquivo, "D");
} 

$con->close();

?>