<?php

include "conexao.php";

include_once('bitly.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$operacao = "conferirBilhete";

$contador = 0;

if ($operacao == "conferirBilhete") {

    

    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);
    if (isset($_POST['numero_bilhete'])) {
        $numero_bilhete = mysqli_real_escape_string($con, $_POST['numero_bilhete']);
    } else {
        $numero_bilhete = -1;
    }

    $query = "  SELECT DATE_FORMAT(bil.data_bilhete, '%d/%m/%Y %H:%i:%S') data_bilhete,
                       DATE_FORMAT(bil.data_bilhete, '%Y-%m-%dT%H:%i:%S') data_bilhete_js,
                site.NOME_BANCA NOME_BANCA,
                site.cod_site,
                usu.nome cambista,
                bil.nome_apostador,
                bil.telefone_apostador,
                bil.txt_pule,
                bil.txt_pule_pai,
                case
                    when bil.status_bilhete = 'A' then 'Aberto'
                    when bil.status_bilhete = 'P' then 'Processado'
                    when bil.status_bilhete = 'C' then 'Cancelado'
                    when bil.status_bilhete = 'V' then 'Pendente de Validação' end as status_bilhete,
                apo.qtd_numeros,
                apo.txt_aposta,
                apo.valor_aposta,
                apo.possivel_retorno,
                apo.flg_sorte,
                apo.retorno5,
                apo.retorno4,
                apo.retorno3,
				apo.valor_ganho,
                apo.do_premio,
                apo.ao_premio,
                apo.tipo_jogo tipo_jogo_bicho,
                apo.inversoes,
                bil.numero_bilhete as numero_bilhete,
                case
                    when apo.status = 'A' then 'Aberto'
                    when apo.status = 'G' then 'Ganho'
                    when apo.status = 'C' then 'Cancelado'
                    when apo.status = 'P' then 'Perdido' end as status,
                jogo.data_jogo,
                cast(case
                 when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
                 when jogo.tipo_jogo = 'R' then jogo.descricao
                  else jogo.concurso end as char) as concurso,
                case
                  when jogo.tipo_jogo = 'S' then 'SENINHA'
                  when jogo.tipo_jogo = 'Q' then 'QUININHA'
                  when jogo.tipo_jogo = 'L' then 'LOTINHA'
                  when jogo.tipo_jogo = 'B' then 'BICHO'
                  when jogo.tipo_jogo = '2' then '2 PRA 500'
                  when jogo.tipo_jogo = 'R' then 'RIFA' 
                  else ''
                  end tipo_jogo,
                LPAD(jogo.numero_1,4,'0') numero_1,
                LPAD(jogo.numero_2,4,'0') numero_2,
                LPAD(jogo.numero_3,4,'0') numero_3,
                LPAD(jogo.numero_4,4,'0') numero_4,
                LPAD(jogo.numero_5,4,'0') numero_5,
                LPAD(jogo.numero_6,4,'0') numero_6,
                LPAD(jogo.numero_7,4,'0') numero_7,
                LPAD(jogo.numero_8,4,'0') numero_8,
                LPAD(jogo.numero_9,4,'0') numero_9,
                LPAD(jogo.numero_10,4,'0') numero_10,
                LPAD(jogo.numero_11,4,'0') numero_11,
                LPAD(jogo.numero_12,4,'0') numero_12,
                LPAD(jogo.numero_13,4,'0') numero_13,
                LPAD(jogo.numero_14,4,'0') numero_14,
                LPAD(jogo.numero_15,4,'0') numero_15
                FROM bilhete bil
                inner join site on (site.cod_site = bil.cod_site)
                inner join aposta apo on (bil.cod_bilhete = apo.cod_bilhete)
                inner join jogo on (apo.cod_jogo = jogo.cod_jogo)
                inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                where (bil.txt_pule = '$cod_bilhete' or bil.txt_pule_pai = '$cod_bilhete' or bil.numero_bilhete = '$numero_bilhete') ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;
    $linkZap = "";

    // token willianae@gmail.com
    //$user_access_token = '8afa4e6c4ce593125a900430230be297e5b3e7f7';

    // token sabatina8@o2.pl
    // $user_access_token = '01099036338c09d46ce757703f983990b4a625e5';

    $user_access_token = '08c3557fb3ae656c9261e901986c77b25ce9ae10';

    $bilhete_anterior = "";

    //while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    while ($contador < mysqli_num_rows($result)){
        $bilhete_anterior = $row['txt_pule'];
        $row_array['linkZap'] = '';

        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_bilhete_js'] = $row['data_bilhete_js'];
        $row_array['nome_banca'] = $row['NOME_BANCA'];
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['status_bilhete'] = $row['status_bilhete'];
        $row_array['txt_pule'] = $row['txt_pule'];
        $row_array['txt_pule_pai'] = $row['txt_pule_pai'];
        $row_array['numero_bilhete'] = $row['numero_bilhete'];
        $row_array['data_jogo'] = $row['data_jogo'];
        $row_array['concurso'] = $row['concurso'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        
        $tipoJogo = $row['tipo_jogo'];
        switch ($tipoJogo) {
            case "2 PRA 500":
                $row_array['numeros_sorteados'] = $row['numero_1'];
            break;
            case "QUININHA":
                $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                    substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                    substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                    substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                    substr($row['numero_5'], strlen($row['numero_5']) - 2, 2);
            break;
            case "SENINHA":
                $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                    substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                    substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                    substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                    substr($row['numero_5'], strlen($row['numero_5']) - 2, 2) . ' - ' .
                    substr($row['numero_6'], strlen($row['numero_6']) - 2, 2);
            break;
            case "LOTINHA":
                $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                    substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                    substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                    substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                    substr($row['numero_5'], strlen($row['numero_5']) - 2, 2) . ' - ' .
                    substr($row['numero_6'], strlen($row['numero_6']) - 2, 2) . ' - ' .
                    substr($row['numero_7'], strlen($row['numero_7']) - 2, 2) . ' - ' .
                    substr($row['numero_8'], strlen($row['numero_8']) - 2, 2) . ' - ' .
                    substr($row['numero_9'], strlen($row['numero_9']) - 2, 2) . ' - ' .
                    substr($row['numero_10'], strlen($row['numero_10']) - 2, 2) . ' - ' .
                    substr($row['numero_11'], strlen($row['numero_11']) - 2, 2) . ' - ' .
                    substr($row['numero_12'], strlen($row['numero_12']) - 2, 2) . ' - ' .
                    substr($row['numero_13'], strlen($row['numero_13']) - 2, 2) . ' - ' .
                    substr($row['numero_14'], strlen($row['numero_14']) - 2, 2) . ' - ' .
                    substr($row['numero_15'], strlen($row['numero_15']) - 2, 2);
            break;
            case "BICHO":
                $row_array['numeros_sorteados'] = 
                $row['numero_1'] . ' - ' . $row['numero_2'] . ' - ' .
                $row['numero_3'] . ' - ' . $row['numero_4'] . ' - ' .
                $row['numero_5'] . ' - ' . $row['numero_6'] . ' - ' .
                $row['numero_7'] . ' - ' . $row['numero_8'] . ' - ' .
                $row['numero_9'] . ' - ' . $row['numero_10'];
            break;
            case "RIFA":
                $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 3, 3);
            break;
        }

        $row_array['apostas'] = array();

        $valorBilhete = 0;
        do {
            $row_array_2['qtd_numeros'] = $row['qtd_numeros'];
            $row_array_2['txt_aposta'] = $row['txt_aposta'];
            $row_array_2['valor_aposta'] = $row['valor_aposta'];
            $row_array_2['possivel_retorno'] = $row['possivel_retorno'];
            $row_array_2['flg_sorte'] = $row['flg_sorte'];
            $row_array_2['retorno5'] = $row['retorno5'];
            $row_array_2['retorno4'] = $row['retorno4'];
            $row_array_2['retorno3'] = $row['retorno3'];
			$row_array_2['valor_ganho'] = $row['valor_ganho'];
            $row_array_2['status'] = $row['status'];
            $row_array_2['do_premio'] = $row['do_premio'];
            $row_array_2['ao_premio'] = $row['ao_premio'];
            $row_array_2['tipo_jogo_bicho'] = $row['tipo_jogo_bicho'];
            $row_array_2['inversoes'] = $row['inversoes'];            
            $valorBilhete = $valorBilhete + $row['valor_aposta'];

            array_push($row_array['apostas'], $row_array_2);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $contador = $contador + 1;

            if ($contador == mysqli_num_rows($result)) {
                break;
            }

        } while ($bilhete_anterior == $row['txt_pule']);

        if ($tipoJogo == '2 PRA 500'){
            $row_array['valor_bilhete'] = 2;
        }else{
            $row_array['valor_bilhete'] = $valorBilhete;
        }        

        array_push($return_arr, $row_array);

    }

    echo json_encode($return_arr);

}
