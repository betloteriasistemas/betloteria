var app = angular.module('main', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
	$routeProvider.when('/', {
		templateUrl: './components/simulador.html',
		controller: 'simuladorCtrl'
	}).otherwise({
			template: '404'
		});

	$locationProvider.html5Mode(false);
});
