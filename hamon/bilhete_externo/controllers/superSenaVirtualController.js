angular.module('main').controller('superSenaVirtualCtrl',
    function ($rootScope, $scope, user, $http, $routeParams, configGeral) {
        $rootScope.loading = false;
        $scope.flg_supersena = false;
        $scope.valor_min_aposta;
        $scope.valor_max_aposta;
        $scope.comboConcurso = [];
        $scope.chkManterNumerosSelecionados = true;
        
        configGeral.get().then(function (data) {
            $scope.flg_supersena = data.flg_supersena;
            $scope.valor_min_aposta = data.valor_min_aposta;
            $scope.valor_max_aposta = data.valor_max_aposta;
        });

        $scope.currentId = $routeParams.id;

        if (typeof $scope.currentId != 'undefined' || user.getSiteExterno() != 0) {
            if (typeof $scope.currentId != 'undefined') {
                auxSite = $scope.currentId;
                user.setSiteExterno($scope.currentId);
            }
    
            $scope.logomarca = "img/" + $scope.currentId + ".png";
    
            var request = new XMLHttpRequest();
            request.open('HEAD', $scope.logomarca, false);
            request.send();
            if (request.status != 200) {
                $scope.logomarca = "img/logo_transparente.png";
            }
        } else {
            user.setSiteExterno(9999);
            $scope.logomarca = "img/logo_transparente.png";
        }
    
        // Caso o usuário faca um refresh na pagina, será necessário obter o schema do pin
        if (typeof $scope.currentId != 'undefined' && user.getSchema() == undefined) {
            $http({
                method: "POST",
                url: "angularjs-mysql/conexao_login.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: '&site=' + $scope.currentId + '&operacao=geral'
            }).then(function (response) {
                if (typeof $scope.currentId != 9999) {
                    user.saveData(response.data);       
                    localStorage.setItem('schema', response.data['schema']);
                }
            }).catch(function (response) {
                deferred.resolve(response);
                alert('Erro no $HTTP: ' + response.status)
            });
        }        

        $scope.puleGerado = "";
        
        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSiteExterno() + 
                  '&operacao=supersena' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.listaCfg = response.data;
        });

        $scope.apostas = [];
        $scope.listaApostas = [];
        
        $scope.valorTotal = "0,00";

        // Limpa todos os números selecionados
        $scope.limpar = function () {
	        $("button.btn-bg-grid").removeClass("btn-bg-grid").addClass("btn-outline-grid");
	        $(".numerosSelecionados").html("<span>Selecionados: 0</span>");
        }

        // Função para selecionar os números aleatoriamente
        $scope.selecionarNumeros = function (qtdNumeros) {
            $scope.limpar();

            var contador = 0;
            var arrayPintados = [];

            while (contador < qtdNumeros) {

                var pintou = false;
                var randomico = Math.floor(Math.random() * 60) + 1;

                for (i = 0; i < arrayPintados.length; i++) {
                    if (arrayPintados[i] == randomico) {
                        pintou = true;
                        break;
                    }
                }
                if (pintou) {
                    continue;
                }

            	$("button.btn-outline-grid").each(function (index) {
                    var aux = $(this).text();
                    aux = parseInt(aux);

                    if (aux == randomico) {
                    	$(this).removeClass("btn-outline-grid").addClass("btn-bg-grid");
                        arrayPintados.push(randomico);
                    }
                });

                if (!pintou) {
                    contador = contador + 1;
                }
            }
        	$(".numerosSelecionados").html("<span>Selecionados: " + $("button.btn-bg-grid").length + "</span>");
        }

        // Remove aposta
        $scope.removerAposta = function (index) {
            $scope.apostas.splice(index, 1);
            $scope.getValorTotal();
        }

        // Adiciona aposta
        $scope.adicionarAposta = function () {
            if (typeof $scope.edtValor === "undefined") {
                $.alert('Informe o VALOR!');
                return;
            }

            if (!$scope.repetir_bilhete && $scope.comboConcurso.length == 0) {
                $.alert('Informe o CONCURSO!');
                return;
            }

            if ($scope.apostas.length + $scope.comboConcurso.length > 50) {
                $.alert('Limite máximo de 50 apostas por bilhete atingido!');
                return;
            }

            var valorAux = parseFloat($scope.edtValor.toString().replace(",", "."));

            if (valorAux <= 0) {
                $.alert("Informe o valor maior que zero!");
                return;
            } else if ($scope.valor_min_aposta && valorAux < $scope.valor_min_aposta) {
                $.alert("O valor mínimo de aposta é: R$ " + $scope.valor_min_aposta);
                return;
            } else if ($scope.valor_max_aposta && valorAux > $scope.valor_max_aposta) {
                $.alert("O valor máximo de aposta é: R$ " + $scope.valor_max_aposta);
                return;
            }

            try {
                for (i = 14; i <= 40; i++) {
                    if ((i > 25 && i < 30) || (i > 30 && i < 35) || (i > 35 && i < 40)) {
                        continue;
                    }
                	$("#btn" + i + "sel").removeClass('btn-bg-grid');
                }

            	if ($("button.btn-bg-grid").length != 10) {
                    $.alert('É necessário selecionar 10 dezenas!');
                    return;
                }

                var sNumeros = "";

	            $("button.btn-bg-grid").each(function (index) {

                    sNumeros = sNumeros + $(this).text() + '-';
                });

                sNumeros = sNumeros.trim();
                sNumeros = sNumeros.substr(0, sNumeros.length - 2);
                sNumeros = sNumeros.replace(/\./g, '');

                var iQtd = $("button.btn-bg-grid").length;

                var i, j;
                for (i = 0; i < $scope.comboConcurso.length; i++) {
                    for (j = 0; j < $scope.apostas.length; j++) {
                        if ($scope.apostas[j].numeros == sNumeros 
                            && $scope.apostas[j].cod_jogo == $scope.comboConcurso[i].cod_jogo) {
                            $.alert("Aposta duplicada!");
                            return;
                        }
                    }
                }

                var sTipo = "Super Sena"; 
                var sValor = "R$ " + $scope.edtValor.toString();
	            var iRetornoSena = $scope.listaCfg[0].premio_sena * valorAux;
                var iRetornoQuina = $scope.listaCfg[0].premio_quina * valorAux;
                var iRetornoQuadra = $scope.listaCfg[0].premio_quadra * valorAux;

                if (iRetornoSena > $scope.listaCfg[0].premio_maximo) {
                    iRetornoSena = $scope.listaCfg[0].premio_maximo;
                }

                if (iRetornoQuina > $scope.listaCfg[0].premio_maximo) {
                    iRetornoQuina = $scope.listaCfg[0].premio_maximo;
                }

                if (iRetornoQuadra > $scope.listaCfg[0].premio_maximo) {
                    iRetornoQuadra = $scope.listaCfg[0].premio_maximo;
                }

                var iPossivelRetornoSenaReal = iRetornoSena;
                var iPossivelRetornoQuinaReal = iRetornoQuina;
                var iPossivelRetornoQuadraReal = iRetornoQuadra;

                var numero = parseFloat(iRetornoSena).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetornoSena = numero.join(',');

                var numero = parseFloat(iRetornoQuina).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetornoQuina = numero.join(',');

                var numero = parseFloat(iRetornoQuadra).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetornoQuadra = numero.join(',');

                if ($scope.repetir_bilhete) {
                    var objeto = {
                        numeros: sNumeros,
                        valor: sValor,
                        valorReal: valorAux,
                        qtd: iQtd,
                        tipo: sTipo,
                        possivelRetornoSena: iRetornoSena,
                        possivelRetornoSenaReal: iPossivelRetornoSenaReal,
                        possivelRetornoQuina: iRetornoQuina,
                        possivelRetornoQuinaReal: iPossivelRetornoQuinaReal,
                        possivelRetornoQuadra: iRetornoQuadra,
                        possivelRetornoQuadraReal: iPossivelRetornoQuadraReal,
                        cod_jogo: 0,
                        concurso: 0,
                        data_jogo: 0
                    };
                    $scope.apostas.push(objeto);
                } else {
                    for (i = 0; i < $scope.comboConcurso.length; i++) {
                        var objeto = {
                            numeros: sNumeros,
                            valor: sValor,
                            valorReal: valorAux,
                            qtd: iQtd,
                            tipo: sTipo,
                            possivelRetornoSena: iRetornoSena,
                            possivelRetornoSenaReal: iPossivelRetornoSenaReal,
                            possivelRetornoQuina: iRetornoQuina,
                            possivelRetornoQuinaReal: iPossivelRetornoQuinaReal,
                            possivelRetornoQuadra: iRetornoQuadra,
                            possivelRetornoQuadraReal: iPossivelRetornoQuadraReal,
                            cod_jogo: $scope.comboConcurso[i].cod_jogo,
                            concurso: $scope.comboConcurso[i].concurso,
                            data_jogo: $scope.comboConcurso[i].data_jogo
                        };
                        $scope.apostas.push(objeto);
                    }                
                }
                $scope.getValorTotal();

            } finally {
                if (!$scope.chkManterNumerosSelecionados) {
                    $scope.selecionarNumeros(iQtd);
                }
                $("#btn" + iQtd + "sel").addClass('btn-bg-grid');
            }

        }

        $scope.getJogos = function () {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'finalizados=false' + 
                      '&site=' + user.getSiteExterno() + 
                      '&status=L' + 
                      '&operacao=supersena' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogos = response.data;
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });

        }

        $scope.filtraConcursoAtual = function () {
            if ($scope.jogos.length > 0) {
                return $scope.jogos[0];
            }
        }       

        $scope.getValorTotal = function () {
            var dValorTotal = 0;
            for (i = 0; i < $scope.apostas.length; i++) {
                dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
            }

            var numero = parseFloat(dValorTotal).toFixed(2).split('.');
            numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
            $scope.valorTotal = numero.join(',');
        }

        $scope.getValorTotalReal = function () {
            var dValorTotal = 0;
            for (i = 0; i < $scope.apostas.length; i++) {
                dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
            }

            return dValorTotal;
        }

        $scope.gerarBilhete = function () {

            if ($scope.apostas.length == 0) {
                $.alert('Faça no mínimo uma aposta!');
                return;
            }

            if (typeof $scope.edtNome === "undefined") {
                $.alert('Informe o NOME!');
                return;
            }

            $.confirm({
                title: '',
                content:"Confirma a geração do bilhete?",
                buttons: {
                    cancelar: function() {},
                    ok: function () {
                        var sTelefone = $scope.edtTelefone;
                        if (typeof sTelefone === 'undefined') {
                            sTelefone = "";
                        }
        
                        var data = new Date();
        
                        var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
                        var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
                        var ano = data.getFullYear();
        
                        var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
                        var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
                        var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };
        
                        var dataAtual = ano + '-' + mes + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;
                        $http({
                            url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: "cod_site=" + user.getSiteExterno() +
                                "&nome_usuario=Bilhete Virtual" +
                                "&nome=" + $scope.edtNome +
                                "&telefone=" + sTelefone +
                                "&apostas=" + JSON.stringify($scope.apostas) +
                                "&data_atual=" + dataAtual +
                                "&operacao=" +
                                '&schema=' + user.getSchema()
                        }).then(function (response) {
                            if (response.data.status == 'OK') {
                                $scope.numeroBilhete = response.data.numero_bilhete;
                                $scope.puleGerado = response.data.pule;
                                $scope.limpar();
                                $scope.apostas = [];
                                $scope.valorTotal = "0,00";
                                $scope.edtNome = '';
                                $scope.edtTelefone = '';
                                $('html, body').animate({ scrollTop: 0 }, 'slow'); //slow, medium, fast
                            } else {
                                $.alert(response.data.mensagem);
                            } 
                        }, function (response) {
                            console.log(response);
                            $.alert('Erro no $HTTP: ' + response.status)
                        });
                    }
                }
            });
        }
    });
