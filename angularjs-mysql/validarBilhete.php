<?php

include "conexao.php";

require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];
$auditoria = "";

try {
    $con->begin_transaction();

    $query = 'SET @@session.time_zone = "-03:00"';
    $result = mysqli_query($con, $query);

    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
    $txt_pule = mysqli_real_escape_string($con, $_POST['txt_pule']);

    $result = mysqli_query($con, "select * from usuario where cod_usuario = $cod_usuario");
    $rowUsuario = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $area = $rowUsuario['COD_AREA'];
    $flg_seninha = $rowUsuario['FLG_SENINHA'];
    $flg_quininha = $rowUsuario['FLG_QUININHA'];
    $flg_bicho = $rowUsuario['FLG_BICHO'];
    $cod_gerente = $rowUsuario['COD_GERENTE'];

    // Vai buscar o concurso e o tipo_jogo no site que for puxar o PIN
    $query = " select apo.*, 
                      jogoPin.cod_jogo as JOGO, 
                      jogoPin.tipo_jogo as MODALIDADE,
                      bil.data_validacao_bilhete as DATA_VALIDACAO
        from bilhete bil
        inner join aposta apo on (apo.cod_bilhete = bil.cod_bilhete)
        inner join jogo j on (j.cod_jogo = apo.cod_jogo)
        inner join jogo jogoPin on (j.data_jogo  = jogoPin.data_jogo 
                                and j.concurso = jogoPin.concurso 
                                and j.tipo_jogo = jogoPin.tipo_Jogo 
                                and jogoPin.cod_site = '$cod_site')
        where bil.txt_pule = '$txt_pule' or bil.txt_pule_pai = '$txt_pule'
        and j.tp_status = 'L' ";

    $querySoma = "select sum(v.valor_aposta) total from ( " . $query . " ) v";

    $result = mysqli_query($con, $query);
    $contador = 0;
    $valor_aposta = 0;

    $configuracao;
    $comissaoBicho;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        validarJogo($con, $row['JOGO'], $row['MODALIDADE']);

        if ($row['MODALIDADE'] == "S" && $flg_seninha == "N") {
            throw new Exception("O seu usuário não está habilitado para validar este tipo de jogo!");
        }

        if ($row['MODALIDADE'] == "Q" && $flg_quininha == "N") {
            throw new Exception("O seu usuário não está habilitado para validar este tipo de jogo!");
        }

        if ($row['MODALIDADE'] == "B" && $flg_bicho == "N") {
            throw new Exception("O seu usuário não está habilitado para validar este tipo de jogo!");
        }

        if ($row['DATA_VALIDACAO'] != null) {
            throw new Exception("Este bilhete já foi validado!");
        }
        
        if ($contador == 1) {
            $resultSoma = mysqli_query($con, $querySoma);
            $rowSoma = mysqli_fetch_array($resultSoma, MYSQLI_ASSOC);
            if ($rowUsuario['SALDO'] < $rowSoma['total']) {
                throw new Exception('Você não tem saldo para validar essa aposta!');
            }
        }
        $stmt = $con->prepare("update bilhete set cod_site = ?, 
                                        cod_usuario = ?, 
                                        status_bilhete = 'A',
                                        data_validacao_bilhete = CURRENT_TIME,
                                        data_bilhete = CURRENT_TIME
                                where cod_bilhete = ? ");
        $stmt->bind_param("iii", $cod_site, $cod_usuario, $row['COD_BILHETE']);
        $stmt->execute();

        switch ($row['MODALIDADE']) {
            case 'S':
                $queryConf = mysqli_query($con, "SELECT *
                            FROM configuracao c
                            WHERE `COD_SITE` = '$cod_site' AND `COD_AREA` = '$area'");
                $configuracao =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
                $queryConfGerente = mysqli_query($con, "SELECT *                   
                            FROM usuario usu 
                            LEFT JOIN configuracao conf ON usu.cod_area = conf.cod_area
                            WHERE usu.COD_SITE = '$cod_site' 
                                AND usu.COD_USUARIO = '$cod_gerente'");
                $configuracaoGerente =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);                

                $possivel_retorno = $row['POSSIVEL_RETORNO'];
                $premio_novo = getValorPremio($row['QTD_NUMEROS'], $row['VALOR_APOSTA'], $configuracao);
                if ($row['FLG_SORTE'] == 'S' && $configuracao['FLG_SORTE'] == 'S') {
                    $possivel_retorno = $premio_novo * $configuracao['SENA_SORTE'] / 100;
                    if ($possivel_retorno > $configuracao['PREMIO_MAXIMO']) {
                        $possivel_retorno = $configuracao['PREMIO_MAXIMO'];
                    }

                    $retorno_med = $premio_novo * $configuracao['QUINA_SORTE'] / 100;
                    if ($retorno_med > $configuracao['PREMIO_MAXIMO']) {
                        $retorno_med = $configuracao['PREMIO_MAXIMO'];
                    }

                    $retorno_min = $premio_novo * $configuracao['QUADRA_SORTE'] / 100;
                    if ($retorno_min > $configuracao['PREMIO_MAXIMO']) {
                        $retorno_min = $configuracao['PREMIO_MAXIMO'];
                    }
                    $flgSorte = 'S';
                } else {
                    $retorno_med = 0;
                    $retorno_min = 0;
                    $flgSorte = 'N';
                    $possivel_retorno = $premio_novo;
                    if ($possivel_retorno > $configuracao['PREMIO_MAXIMO']) {
                        $possivel_retorno = $configuracao['PREMIO_MAXIMO'];
                    }
                }
                $comissao = $row['VALOR_APOSTA'] * $rowUsuario['PCT_COMISSAO_SENINHA'] / 100;
                $comissao_gerente = (($row['VALOR_APOSTA'] - $comissao) * $configuracaoGerente['PCT_COMISSAO_SENINHA']) / 100;

                $stmt = $con->prepare("UPDATE aposta SET 
                            cod_jogo = ?, cod_site = ?,
                            possivel_retorno = ?, retorno5 = ?, retorno4 = ?, 
                            flg_sorte = ?, comissao = ?, comissao_gerente = ?
                            WHERE cod_aposta = ?");
                $stmt->bind_param("iidddsddi",
                    $row['JOGO'],
                    $cod_site,
                    $possivel_retorno,
                    $retorno_med,
                    $retorno_min,
                    $flgSorte,
                    $comissao,
                    $comissao_gerente,
                    $row['COD_APOSTA']
                );
                $stmt->execute();
            break;
            case 'Q':
                $queryConf = mysqli_query($con, "SELECT *
                        FROM configuracao_quininha c
                        WHERE `COD_SITE` = '$cod_site' and `COD_AREA` = '$area'");
                $configuracao =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
                $queryConfGerente = mysqli_query($con, "SELECT *                   
                        FROM usuario usu 
                            left join configuracao_quininha conf on usu.cod_area = conf.cod_area 
                        WHERE usu.COD_SITE = '$cod_site' 
                            AND usu.COD_USUARIO = '$cod_gerente'");
                $configuracaoGerente =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);                  
    
                $possivel_retorno = $row['POSSIVEL_RETORNO'];
                $premio_novo = getValorPremio($row['QTD_NUMEROS'], $row['VALOR_APOSTA'], $configuracao);
                if ($row['FLG_SORTE'] == 'S' && $configuracao['FLG_SORTE'] == 'S') {
                    $possivel_retorno = $premio_novo * $configuracao['QUINA_SORTE'] / 100;
                    if ($possivel_retorno > $configuracao['PREMIO_MAXIMO']) {
                        $possivel_retorno = $configuracao['PREMIO_MAXIMO'];
                    }
    
                    $retorno_med = $premio_novo * $configuracao['QUADRA_SORTE'] / 100;
                    if ($retorno_med > $configuracao['PREMIO_MAXIMO']) {
                        $retorno_med = $configuracao['PREMIO_MAXIMO'];
                    }
    
                    $retorno_min = $premio_novo * $configuracao['TERNO_SORTE'] / 100;
                    if ($retorno_min > $configuracao['PREMIO_MAXIMO']) {
                        $retorno_min = $configuracao['PREMIO_MAXIMO'];
                    }
                    $flgSorte = 'S';
                } else {
                    $retorno_med = 0;
                    $retorno_min = 0;
                    $flgSorte = 'N';
                    $possivel_retorno = $premio_novo;
                    if ($possivel_retorno > $configuracao['PREMIO_MAXIMO']) {
                        $possivel_retorno = $configuracao['PREMIO_MAXIMO'];
                    }
                }
    
                $comissao = $row['VALOR_APOSTA'] * $rowUsuario['PCT_COMISSAO_QUININHA'] / 100;
                $comissao_gerente = (($row['VALOR_APOSTA'] - $comissao) * $configuracaoGerente['PCT_COMISSAO_QUININHA']) / 100;
    
                $stmt = $con->prepare("UPDATE aposta SET 
                        cod_jogo = ?, cod_site = ?,
                        possivel_retorno = ?, retorno4 = ?, retorno3 = ?, 
                        flg_sorte = ?, comissao = ?, comissao_gerente = ?
                        WHERE cod_aposta = ?");
                $stmt->bind_param(
                    "iidddsddi",
                    $row['JOGO'],
                    $cod_site,
                    $possivel_retorno,
                    $retorno_med,
                    $retorno_min,
                    $flgSorte,
                    $comissao,
                    $comissao_gerente,
                    $row['COD_APOSTA']
                );
                $stmt->execute();
            break;
            case 'L':
                $queryConf = mysqli_query($con, "SELECT *
                        FROM configuracao_lotinha c
                        WHERE `COD_SITE` = '$cod_site' and `COD_AREA` = '$area'");
                $configuracao =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
                $queryConfGerente = mysqli_query($con, "SELECT *                   
                        FROM usuario usu 
                            left join configuracao_lotinha conf on usu.cod_area = conf.cod_area 
                        WHERE usu.COD_SITE = '$cod_site' 
                            AND usu.COD_USUARIO = '$cod_gerente'");
                $configuracaoGerente =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);  

                $possivel_retorno = getValorPremio($row['QTD_NUMEROS'], $row['VALOR_APOSTA'], $configuracao);
                if ($possivel_retorno > $configuracao['PREMIO_MAXIMO']) {
                    $possivel_retorno = $configuracao['PREMIO_MAXIMO'];
                }
                $comissao = $row['VALOR_APOSTA'] * $rowUsuario['PCT_COMISSAO_LOTINHA'] / 100;
                $comissao_gerente = (($row['VALOR_APOSTA'] - $comissao) * $configuracaoGerente['PCT_COMISSAO_LOTINHA']) / 100;
                $stmt = $con->prepare("UPDATE aposta SET 
                        cod_jogo = ?, cod_site = ?,
                        possivel_retorno = ?, comissao = ?,
                        comissao_gerente = ?
                        WHERE cod_aposta = ?");
                $stmt->bind_param(
                    "iidddi",
                    $row['JOGO'],
                    $cod_site,
                    $possivel_retorno,
                    $comissao,
                    $comissao_gerente,
                    $row['COD_APOSTA']
                );
                $stmt->execute();
            break;
            case 'U':
                $queryConf = mysqli_query($con, "SELECT *
                        FROM configuracao_super_sena c
                        WHERE `COD_SITE` = '$cod_site' and `COD_AREA` = '$area'");
                $configuracao =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);
                $queryConfGerente = mysqli_query($con, "SELECT *                   
                        FROM usuario usu 
                            left join configuracao_super_sena conf on usu.cod_area = conf.cod_area 
                        WHERE usu.COD_SITE = '$cod_site' 
                            AND usu.COD_USUARIO = '$cod_gerente'");
                $configuracaoGerente =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);  

                $possivel_retorno = $row['VALOR_APOSTA'] * $configuracao['PREMIO_SENA'];
                if ($possivel_retorno > $configuracao['PREMIO_MAXIMO']) {
                    $possivel_retorno = $configuracao['PREMIO_MAXIMO'];
                }

                $quina_sorte = $row['VALOR_APOSTA'] * $configuracao['PREMIO_QUINA'];
                if ($quina_sorte > $configuracao['PREMIO_MAXIMO']) {
                    $quina_sorte = $configuracao['PREMIO_MAXIMO'];
                }

                $quadra_sorte = $row['VALOR_APOSTA'] * $configuracao['PREMIO_QUADRA'];
                if ($quadra_sorte > $configuracao['PREMIO_MAXIMO']) {
                    $quadra_sorte = $configuracao['PREMIO_MAXIMO'];
                }

                $comissao = $row['VALOR_APOSTA'] * $rowUsuario['PCT_COMISSAO_SUPERSENA'] / 100;
                $comissao_gerente = (($row['VALOR_APOSTA'] - $comissao) * $configuracaoGerente['PCT_COMISSAO_SUPERSENA']) / 100;
                $stmt = $con->prepare("UPDATE aposta SET 
                        cod_jogo = ?, cod_site = ?,
                        possivel_retorno = ?, 
                        retorno5 = ?,
                        retorno4 = ?,
                        comissao = ?,
                        comissao_gerente = ?
                        WHERE cod_aposta = ?");
                $stmt->bind_param(
                    "iidddddi",
                    $row['JOGO'],
                    $cod_site,
                    $possivel_retorno,
                    $quina_sorte,
                    $quadra_sorte,
                    $comissao,
                    $comissao_gerente,
                    $row['COD_APOSTA']
                );
                $stmt->execute();
            break;
            case 'B':
                $queryConf = mysqli_query($con, "SELECT *
                        FROM configuracao_bicho c
                        WHERE `COD_SITE` = '$cod_site' and `COD_AREA` = '$area'");
                $configuracao =  mysqli_fetch_array($queryConf, MYSQLI_ASSOC);

                $cod_extracao = $row['JOGO'];
                $queryLimiteBicho = mysqli_query($con, "SELECT clb.*
                        FROM jogo j 
                        INNER JOIN configuracao_limite_bicho clb ON (j.CONCURSO = clb.COD_EXTRACAO)
                        WHERE clb.COD_SITE = '$cod_site' AND j.COD_JOGO = '$cod_extracao'");
                $limites =  mysqli_fetch_array($queryLimiteBicho, MYSQLI_ASSOC);
                
                $queryComissaoBicho = mysqli_query($con, "SELECT u.`PCT_COMISSAO_BICHO`, ccb.*
                        FROM usuario u
                        LEFT JOIN configuracao_comissao_bicho ccb ON (u.`COD_USUARIO` = ccb.`COD_USUARIO`)
                        WHERE u.`COD_USUARIO` =  '$cod_usuario'"
                );
                $comissaoBicho = mysqli_fetch_array($queryComissaoBicho, MYSQLI_ASSOC);
                $queryConfGerente = mysqli_query($con, "SELECT u.`PCT_COMISSAO_BICHO`, ccb.*
                        FROM usuario u
                        LEFT JOIN configuracao_comissao_bicho ccb ON (u.`COD_USUARIO` = ccb.`COD_USUARIO`)
                        WHERE u.`COD_USUARIO` =  '$cod_gerente'");
                $comissaoBichoGerente =  mysqli_fetch_array($queryConfGerente, MYSQLI_ASSOC);
    
                $comissao = obterComissaoBicho(
                    $row['TIPO_JOGO'],
                    $row['VALOR_APOSTA'],
                    $comissaoBicho
                );
                $comissao_gerente = obterComissaoBicho(
                    $row['TIPO_JOGO'],
                    $row['VALOR_APOSTA'] - $comissao,
                    $comissaoBichoGerente
                );

                $possivel_retorno_bicho = getValorPremioBicho(
                    $row['TXT_APOSTA'],
                    $row['TIPO_JOGO'],
                    $row['VALOR_APOSTA'],
                    $row['DO_PREMIO'],
                    $row['AO_PREMIO'],
                    $configuracao,
                    $limites
                );
    
                $stmt = $con->prepare("UPDATE aposta SET cod_jogo = ?, cod_site = ?,
                        possivel_retorno = ?, comissao = ?, flg_sorte = 'N', retorno5 = 0, retorno4 = 0, 
                        retorno3 = 0, comissao_gerente = ?
                        WHERE cod_aposta = ?");
                $stmt->bind_param(
                    "iidddi",
                    $row['JOGO'],
                    $cod_site,
                    $possivel_retorno_bicho,
                    $comissao,
                    $comissao_gerente,
                    $row['COD_APOSTA']
                );
                $stmt->execute();
            break;
        }
        $valor_aposta = $valor_aposta + $row['VALOR_APOSTA'];
    }

    if ($contador == 0) {
        throw new Exception("O Bilhete gerado perdeu a validade. Favor gerar o bilhete novamente!");
    }

    $stmt = $con->prepare("UPDATE usuario SET saldo = saldo - ? WHERE cod_usuario = ?");
    $stmt->bind_param("di", $valor_aposta, $cod_usuario);
    $stmt->execute();

    $query = " SELECT SALDO, PERFIL FROM usuario
            WHERE COD_SITE = '$cod_site'
              AND COD_USUARIO = '$cod_usuario' ";
    $result = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $response['saldo'] = $row['SALDO'];
    $response['status'] = "OK";

    $stmt->close();

    $queryBilhete = "SELECT `NOME_APOSTADOR`, `TELEFONE_APOSTADOR`, `DATA_BILHETE`, `TXT_PULE`, `COD_BILHETE` 
                     FROM bilhete
                     WHERE txt_pule = '$txt_pule' or txt_pule_pai = '$txt_pule'";

    $result = mysqli_query($con, $queryBilhete);
        
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $auditoria = descreverCriacaoBilhete(
            $row['NOME_APOSTADOR'],
            $row['TELEFONE_APOSTADOR'],
            $row['DATA_BILHETE'],
            $row['TXT_PULE'],
            $row['COD_BILHETE'],
            null,
            null,
            null
        );

        $idBilhete = $row['COD_BILHETE'];
        $queryApostas = "SELECT j.`TIPO_JOGO` AS `MODALIDADE`, a.*
                         FROM aposta a 
                         INNER JOIN jogo j ON (a.`COD_JOGO` = j.`COD_JOGO`)
                         WHERE a.`COD_BILHETE` = '$idBilhete'";
        $apostas = mysqli_query($con, $queryApostas);

        if (mysqli_num_rows($apostas) > 0) {
            while ($e = mysqli_fetch_assoc($apostas)) {
                if ($e['MODALIDADE'] == 'B') {
                    $auditoria = $auditoria . descreverApostaBicho(
                        $e['TXT_APOSTA'],
                        $e['VALOR_APOSTA'],
                        $e['POSSIVEL_RETORNO'],
                        $e['DO_PREMIO'],
                        $e['AO_PREMIO'],
                        $e['TIPO_JOGO'],
                        $e['INVERSOES'],
                        $e['COMISSAO']
                    );
                } else if ($e['MODALIDADE'] == '2') {
                    $auditoria = $auditoria . descreverAposta2pra500($e['TXT_APOSTA'], $e['VALOR_APOSTA'], $e['POSSIVEL_RETORNO'], $e['COMISSAO']);
                } else if ($e['MODALIDADE'] == 'S' && $e['FLG_SORTE'] == 'S') {
                    $auditoria = $auditoria . descreverApostaSeninhaSorte(
                        $e['QTD_NUMEROS'],
                        $e['TXT_APOSTA'],
                        $e['VALOR_APOSTA'],
                        $e['POSSIVEL_RETORNO'],
                        'S',
                        $e['RETORNO5'],
                        $e['RETORNO4'],
                        $e['COMISSAO']
                    );
                } else if ($e['MODALIDADE'] == 'Q' && $e['FLG_SORTE'] == 'S') {
                    $auditoria = $auditoria . descreverApostaQuininhaSorte(
                        $e['QTD_NUMEROS'],
                        $e['TXT_APOSTA'],
                        $e['VALOR_APOSTA'],
                        $e['POSSIVEL_RETORNO'],
                        'S',
                        $e['RETORNO4'],
                        $e['RETORNO3'],
                        $e['COMISSAO']
                    );
                } else if ($e['MODALIDADE'] == 'L') {
                    $auditoria = $auditoria . descreverApostaLotinha(
                        $e['QTD_NUMEROS'],
                        $e['TXT_APOSTA'],
                        $e['VALOR_APOSTA'],
                        $e['POSSIVEL_RETORNO'],
                        $e['COMISSAO']);
                } else if ($e['MODALIDADE'] == 'U') {
                    $auditoria = $auditoria . descreverApostaSuperSena(
                        $e['QTD_NUMEROS'],
                        $e['TXT_APOSTA'],
                        $e['VALOR_APOSTA'],
                        $e['POSSIVEL_RETORNO'],
                        $e['RETORNO5'],
                        $e['RETORNO4'],
                        $e['COMISSAO']);                        
                } else {
                    $auditoria = $auditoria . descreverApostaQuininhaSeninha(
                        $e['MODALIDADE'],
                        $e['QTD_NUMEROS'],
                        $e['TXT_APOSTA'],
                        $e['VALOR_APOSTA'],
                        $e['POSSIVEL_RETORNO'],
                        $e['COMISSAO']
                    );
                }
            }
        }
        inserir_auditoria(
            $con,
            $cod_usuario,
            $cod_site,
            AUD_BILHETE_VIRTUAL_VALIDADO,
            $auditoria
        );
    }
    $con->commit();
} catch (Exception $e) {
    $con->rollback();
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}
$con->close();
echo json_encode($response);

function getValorPremio($qtdNumeros, $valor_aposta, $configuracao)
{
    switch ($qtdNumeros) {
        case 13:
            return $valor_aposta * $configuracao['PREMIO_13'];
        case 14:
            return $valor_aposta * $configuracao['PREMIO_14'];
        case 15:
            return $valor_aposta * $configuracao['PREMIO_15'];
        case 16:
            return $valor_aposta * $configuracao['PREMIO_16'];
        case 17:
            return $valor_aposta * $configuracao['PREMIO_17'];
        case 18:
            return $valor_aposta * $configuracao['PREMIO_18'];
        case 19:
            return $valor_aposta * $configuracao['PREMIO_19'];
        case 20:
            return $valor_aposta * $configuracao['PREMIO_20'];
        case 21:
            return $valor_aposta * $configuracao['PREMIO_21'];
        case 22:
            return $valor_aposta * $configuracao['PREMIO_22'];
        case 25:
            return $valor_aposta * $configuracao['PREMIO_25'];
        case 30:
            return $valor_aposta * $configuracao['PREMIO_30'];
        case 35:
            return $valor_aposta * $configuracao['PREMIO_35'];
        case 40:
            return $valor_aposta * $configuracao['PREMIO_40'];
        case 45:
            return $valor_aposta * $configuracao['PREMIO_45'];
    }
}

function getValorPremioBicho(
    $txt_aposta,
    $tipo_jogo,
    $valor_aposta,
    $do_premio,
    $ao_premio,
    $configuracao,
    $limites
) {
    $milhares = explode("-", $txt_aposta);
    $qtdMilhares = count($milhares);
    $qtdPremios = abs($ao_premio - $do_premio) + 1;
    $retorno = 0;
    switch ($tipo_jogo) {
        case "MS":
            if ($valor_aposta == 0) {
                $retorno = $configuracao['VALOR_RETORNO_MILHAR_BRINDE'];
            } else {
                $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_MILHAR_SECA'];            }
        break;
        case "MC":
            $retorno = (
                ($configuracao['PREMIO_MILHAR_SECA'] * ($valor_aposta / 2)) +
                ($configuracao['PREMIO_CENTENA'] * ($valor_aposta / 2))
                )/($qtdMilhares * $qtdPremios);
            break;
        case "MI":
            $retorno = ($valor_aposta / ($qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_MILHAR_SECA'];
            break;
        case "MCI":
            $retorno = ($valor_aposta / ($qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_MILHAR_CENTENA'];
            break;
        case "C":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_CENTENA'];
            break;
        case "CI":
            $retorno = ($valor_aposta / ($qtdPremios * getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_CENTENA'];
            break;
        case "G":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_GRUPO'];
            break;
        case "5P100":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * 20;
            break;
        case "DG":
            $retorno = $valor_aposta * $configuracao['PREMIO_DUQUE_GRUPO'];
            break;
        case "DG6":
            $retorno = $valor_aposta * $configuracao['PREMIO_DUQUE_GRUPO6'];
            break;
        case "DGC":
            $retorno = ($valor_aposta / (getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_DUQUE_GRUPO'];
            break;
        case "TG":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_GRUPO'];
            break;
        case "TG10":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_GRUPO10'];
            break;
        case "TG6":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_GRUPO6'];
            break;
        case "TGC":
            $retorno = ($valor_aposta / getQtdInversoes($tipo_jogo, $txt_aposta)) * $configuracao['PREMIO_TERNO_GRUPO'];
            break;
        case "TSE":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_SEQUENCIA'];
            break;
        case "TSO":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_SOMA'];
            break;
        case "TEX-A":
        case "TEX-F":
            $retorno = $valor_aposta * $configuracao['PREMIO_TERNO_ESPECIAL'];
            break;
        case "QG":
            $retorno = $valor_aposta * $configuracao['PREMIO_QUINA_GRUPO'];
            break;
        case "DZ":
            $retorno = ($valor_aposta / ($qtdMilhares * $qtdPremios)) * $configuracao['PREMIO_DEZENA'];
            break;
        case "DDZ":
            $retorno = $valor_aposta  * $configuracao['PREMIO_DUQUE_DEZENA'];
            break;
         case "DDZ6":
            $retorno = $valor_aposta * $configuracao['PREMIO_DUQUE_DEZENA6'];
            break;
        case "DDZC":
            $retorno = ($valor_aposta / (getQtdInversoes($tipo_jogo, $txt_aposta))) * $configuracao['PREMIO_DUQUE_DEZENA'];
            break;
        case "TDZ":
            $retorno = $valor_aposta  * $configuracao['PREMIO_TERNO_DEZENA'];
            break;
        case "TDZ10":
            $retorno = $valor_aposta  * $configuracao['PREMIO_TERNO_DEZENA10'];
            break;
        case "TDZ6":
            $retorno = $valor_aposta  * $configuracao['PREMIO_TERNO_DEZENA6'];
            break;
        case "TDZC":
            $retorno = ($valor_aposta / getQtdInversoes($tipo_jogo, $txt_aposta)) * $configuracao['PREMIO_TERNO_DEZENA'];
            break;
        case "PS":
            $retorno = $valor_aposta * $configuracao['PREMIO_PASSE_SECO'];
            break;
        case "PS12":
            $retorno = $valor_aposta * $configuracao['PREMIO_PASSE_SECO'];
            break;
        case "PC":
            $retorno = $valor_aposta * $configuracao['PREMIO_PASSE_COMBINADO'];
            break;
    }
    if ($limites === null) {
        return $retorno;
    }
    $valorLimite = getLimiteValorBicho($tipo_jogo, $limites);
    if ($retorno > $valorLimite && $valorLimite > 0) {
        return $valorLimite;
    } else {
        return $retorno;
    }
}

function obterComissaoBicho(
    $tipo_aposta,
    $valor_aposta,
    $comissaoBicho
) {
    $comissao = 0;
    if (
        $tipo_aposta == "MC" && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "MS" && $comissaoBicho['COMISSAO_MILHAR_SECA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_SECA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_SECA'] / 100;
    } else if (($tipo_aposta == "MI" || $tipo_aposta == "MCI") &&
        $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_MILHAR_INVERTIDA'] / 100;
    } else if (
        $tipo_aposta == "C" && $comissaoBicho['COMISSAO_CENTENA'] != null
        && $comissaoBicho['COMISSAO_CENTENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA'] / 100;
    } else if (
        $tipo_aposta == "CI" && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] != null
        && $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_CENTENA_INVERTIDA'] / 100;
    } else if (
        ($tipo_aposta == "G") && $comissaoBicho['COMISSAO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_GRUPO'] / 100;
    } else if (($tipo_aposta == "DG" || $tipo_aposta == "DG6" || $tipo_aposta == "DGC") && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] != null
        && $comissaoBicho['COMISSAO_DUQUE_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_GRUPO'] / 100;
    } else if (($tipo_aposta == "TG" || $tipo_aposta == "TG10" || $tipo_aposta == "TG6" || $tipo_aposta == "TGC" 
             || $tipo_aposta == "TSE" || $tipo_aposta == "TSO" || $tipo_aposta == "TEX-A" || $tipo_aposta == "TEX-F")
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] != null
        && $comissaoBicho['COMISSAO_TERNO_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_GRUPO'] / 100;
    } else if ($tipo_aposta == "QG" && $comissaoBicho['COMISSAO_QUINA_GRUPO'] != null
        && $comissaoBicho['COMISSAO_QUINA_GRUPO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_QUINA_GRUPO'] / 100;
    } else if (
        $tipo_aposta == "DZ" && $comissaoBicho['COMISSAO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DEZENA'] / 100;
    } else if (($tipo_aposta == "DDZ" || $tipo_aposta == "DDZ6" || $tipo_aposta == "DDZC") &&  $comissaoBicho['COMISSAO_DUQUE_DEZENA'] != null
        && $comissaoBicho['COMISSAO_DUQUE_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_DUQUE_DEZENA'] / 100;
    } else if (($tipo_aposta == "TDZ" || $tipo_aposta == "TDZ6" || $tipo_aposta == "TDZ10" || $tipo_aposta == "TDZC") &&
        $comissaoBicho['COMISSAO_TERNO_DEZENA'] != null
        && $comissaoBicho['COMISSAO_TERNO_DEZENA'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_TERNO_DEZENA'] / 100;
    } else if (($tipo_aposta == "PS" || $tipo_aposta == "PS12") && $comissaoBicho['COMISSAO_PASSE_SECO'] != null
        && $comissaoBicho['COMISSAO_PASSE_SECO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_SECO'] / 100;
    } else if (
        $tipo_aposta == "PC" && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] != null
        && $comissaoBicho['COMISSAO_PASSE_COMBINADO'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_PASSE_COMBINADO'] / 100;
    } else if (
        $tipo_aposta == "5P100" && $comissaoBicho['COMISSAO_5P100'] != null
        && $comissaoBicho['COMISSAO_5P100'] > 0
    ) {
        $comissao = $valor_aposta * $comissaoBicho['COMISSAO_5P100'] / 100;
    } else {
        $comissao = $valor_aposta * $comissaoBicho['PCT_COMISSAO_BICHO'] / 100;
    }

    return $comissao;
}

function validarJogo($con, $cod_jogo, $tipo_jogo) {
    $query = "select (data_jogo < current_date) data_passou,
              (data_jogo <= current_date) data_retorno,
              (CURRENT_TIME >= '19:50') hora_retorno
              from jogo
              where cod_jogo = '$cod_jogo'";

    switch ($tipo_jogo) {
        case 'S':
        case 'Q':
        case 'L':
        case 'U':
            $query = "select (data_jogo < current_date) data_passou,
                      (data_jogo <= current_date) data_retorno,
                      (CURRENT_TIME >= '19:50') hora_retorno
                      from jogo
                      where cod_jogo = '$cod_jogo'";
            break;
        case 'R':
            $query = "select (data_jogo < current_date) data_passou,
                      0 data_retorno,
                      0 hora_retorno
                      from jogo
                      where cod_jogo = '$cod_jogo'";
            break;
        case 'B':
            $query = "select (j.data_jogo < current_date) data_passou,
                            (j.data_jogo <= current_date) data_retorno,
                            ((CURRENT_TIME + INTERVAL e.prazo_bloqueio MINUTE) >= j.hora_extracao) hora_retorno
                        from jogo j
                            inner join extracao_bicho e on (j.concurso = e.cod_extracao)
                        where cod_jogo = '$cod_jogo'";
            break;                        
        }
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        if (($row['data_passou'] == 1) || ($row['data_retorno'] == 1 && $row['hora_retorno'] == 1)) {
            throw new Exception('Aposta não pode ser feita para o concurso, pois este encontra-se expirado.');
        }
}