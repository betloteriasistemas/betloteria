angular.module('main').controller('clientesCtrl', function ($scope, user, $http, $route, $rootScope) {
    $scope.nome = user.getNome();
    $scope.comboTipoJogo = "";
    $scope.comboStatus = "A";
    $scope.comboPromo = "---";
    $scope.chkExibirAtivos = true;

    $scope.listarClientes = function () {
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/clientes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'status=' + $scope.comboStatus
        }).then(function (response) {
               $scope.records = response.data;
               $rootScope.loading = false;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
            $rootScope.loading = false;
        });

    }

    $scope.getDescricaoStatus = function (status) {
        if (status == 'A') {
            return 'Ativo';
        } else if (status == 'I') {
            return 'Inativo';
        }
    }


    $scope.alterar = function (cliente) {
        $scope.cod_site = cliente.cod_site;
        $scope.edtCodigoEdit = $scope.cod_site;
        $scope.edtNomeEdit = cliente.txt_empresa;
        $scope.edtResponsavelEdit = cliente.txt_responsavel;
        $scope.edtTelefoneEdit = cliente.txt_telefone;
        $scope.edtBancaEdit = cliente.nome_banca;
        $scope.comboStatusEdit = cliente.status;
        $scope.obsEdit = cliente.obs;
        $scope.edtMensalidadeEdit = cliente.mensalidade;
        $scope.edtVencimentoEdit = cliente.vencimento;
        $scope.comboPromoEdit = cliente.promocao;
    }

    $scope.salvar = function (codigo) {
        
        var booInserir = true;
        
        if (codigo != 0) {
            booInserir = false;
            var nome = $scope.edtNomeEdit;
            var responsavel = $scope.edtResponsavelEdit;
            var telefone = $scope.edtTelefoneEdit;
            var banca = $scope.edtBancaEdit;
            var status = $scope.comboStatusEdit;
            var obs = $scope.obsEdit;
            var mensalidade = $scope.edtMensalidadeEdit;
            var vencimento = $scope.edtVencimentoEdit;
            var promocao = $scope.comboPromoEdit;
        } else {
            codigo = $scope.edtCodigo;
            var nome = $scope.edtNome;
            var responsavel = $scope.edtResponsavel;
            var telefone = $scope.edtTelefone;
            var banca = $scope.edtNomeBanca;
            var status = $scope.comboStatus;
            var obs = $scope.obs;
            var mensalidade = $scope.edtMensalidade;
            var vencimento = $scope.edtVencimento;
            var promocao = $scope.comboPromo;
        }

        if (typeof nome === "undefined" && codigo == 0) {
            alert('Informe o NOME DA EMPRESA!');
        } else if (typeof responsavel === "undefined" && codigo == 0) {
            alert('Informe o RESPONSÁVEL!');
        }else if (typeof telefone === "undefined" && codigo == 0) {
            alert('Informe o TELEFONE!');
        }
        else {

            if (!booInserir) {
                $rootScope.loading = true;
            }
            $http({
                url: 'angularjs-mysql/salvarCliente.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'codigo=' + codigo + '&nome=' + nome + '&responsavel=' + responsavel + '&telefone=' + telefone + '&banca=' + banca +
                '&status=' + status + '&obs=' + obs + '&login_admin=' + $scope.edtLoginAdm + '&mensalidade=' + mensalidade + '&vencimento=' + vencimento + '&promocao=' + promocao
            }).then(function (response) {
                if (!booInserir) {
                    alert('Os dados do cliente de pin ' + codigo + ' foram atualizados.');                    
                    $rootScope.loading = false;
                }    
            });
            
            if (booInserir) {
                alert('Aguarde alguns minutos enquanto os dados do cliente de pin ' + codigo + ' estão sendo criados!');
            }

        }
    }
});