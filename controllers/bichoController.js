angular.module('main').controller('bichoCtrl',
    function ($rootScope, $scope, $q, user, $http, $filter, $location, configGeral, funcoes) {
        $rootScope.loading = true;
        $scope.flg_bicho = false;
        $scope.valor_min_aposta;
        $scope.valor_max_aposta;
        configGeral.get().then(function (data) {
            $scope.flg_bicho = user.getflg_bicho() == 'S' && data.flg_bicho;
            $scope.valor_min_aposta = data.valor_min_aposta;
            $scope.valor_max_aposta = data.valor_max_aposta;
        });
        $scope.palpites = [];
        $scope.comboConcurso = [];
        $scope.premio = {do: 1, ao: 1, disabled: false};
        $scope.valoracao = {valor: null, operacao: "D", disabled: false};
        $scope.disabledComboOperacao = false;
        $scope.chkHabilitarNaQuadra = false;
        $scope.edtDataDe = new Date();
        $scope.apostas = [];
        $scope.comboTipoJogo = "VV";
        $scope.valorTotal = "0,00";
        $scope.botaoHabilitado = false;
        $scope.maxPremioRepeticao = 0;
        $scope.chkRegistroCliente = false;
        $scope.clientes = [];
        $scope.tipoPalpite = 'simples';
        $scope.palpitesMultiplo = [];
        $scope.todosJogosMultiplo = {
            tipo: 'todos',
            valor: null,
            premiacao: {
                do: 1,
                ao: 1
            }
        }

        $q.all([
            $http({
                url: 'angularjs-mysql/configuracao.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() + 
                      '&cod_area=' + user.getArea() + 
                      '&operacao=bicho' +
                      '&schema=' + user.getSchema()
            }),
            $http({
                url: 'angularjs-mysql/usuario.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() 
                    + '&cod_usuario=' + user.getCodigo()
                    + '&schema=' + user.getSchema()
            }),
            $http({
                method: "POST",
                url: "angularjs-mysql/clientes.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo()
                    + '&operacao=listar'
                    + '&schema=' + user.getSchema()
            })
        ]).then(function (response) {
            $scope.listaCfg = response[0].data;
            $scope.usuario = response[1].data;
            $scope.clientes = response[2].data;
            $rootScope.loading = false;
        });

        $scope.site = user.getSite();
        $scope.qtd_premios_extracao;
        $scope.puleRepeticao = user.getPuleRepeticao();
        $scope.listaApostas = [];

        if ($scope.puleRepeticao != "") {
            $http({
                url: 'angularjs-mysql/bilhetes.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() + 
                      '&cod_usuario=' + user.getCodigo() + 
                      '&operacao=conferirBilhete' + 
                      '&cod_bilhete=' + $scope.puleRepeticao +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.listaApostas = response.data;
                $scope.maxPremioRepeticao = 0;
                for (var i = 0; i < $scope.listaApostas[0].apostas.length; i++) {
                    var aoPremio = parseInt($scope.listaApostas[0].apostas[i].ao_premio);
                    if (aoPremio > $scope.maxPremioRepeticao) {
                        $scope.maxPremioRepeticao = aoPremio;
                    }
                }
                user.setPuleRepeticao('');
                jQuery("#extracao-dialog").modal('show');
            });
        }

        $scope.filtrarCliente = function(termo) {
            return $scope.clientes.filter(cliente => cliente.nome.toLowerCase().indexOf(termo)>-1);
        }

        $scope.onSelectCliente = function(cliente) {
            if (cliente) {
                $scope.edtTelefone = cliente.telefone;
                document.getElementById("chkRegistrarCliente").focus();
            } else {
                $scope.edtTelefone = null;
            }
        }

        $scope.selecionarExtracoesRepeticao = function() {
            var apostas = [];
            for (var x = 0; x < $scope.listaApostas.length; x++) {
                for (var z = 0; z < $scope.listaApostas[x].apostas.length; z++) {
                    const aposta = $scope.listaApostas[x].apostas[z];
                    if (!apostas.find(a => a.do_premio == aposta.do_premio && a.ao_premio == aposta.ao_premio
                        && a.tipo_jogo_bicho == aposta.tipo_jogo_bicho && a.valor_aposta == aposta.valor_aposta
                        && a.txt_aposta == aposta.txt_aposta && a.cod_jogo != aposta.cod_jogo)) {
                            apostas.push(aposta);
                    }
                }
            }
            for (var x = 0; x < apostas.length; x++) {
                const aposta = apostas[x];
                const palpites = aposta.txt_aposta.split("-");
                const valor = parseFloat(aposta.valor_aposta);
                if (valor != '0.00') {
                    adicionarAposta(palpites, aposta.tipo_jogo_bicho, parseInt(aposta.do_premio), parseInt(aposta.ao_premio), valor, "D");
                } else if ($scope.mostraMilharBrinde()) {
                    $scope.edtMilharBrinde = aposta.txt_aposta;
                }
            }
            $scope.palpites = [];
            $scope.edtPalpite = null;
            $scope.edtNaQuadra = null;
            $scope.chkHabilitarNaQuadra = false;
            $scope.comboTipoJogo = "VV";
            $scope.valoracao = {valor: null, operacao: "D", disabled: false};
            $scope.premio = {do: 1, ao: 1, disabled: false};        
            jQuery("#extracao-dialog").modal('hide');
        }

        $scope.getShowMilharNaQuadra = function () {
            var retorno = false;
            if ($scope.comboTipoJogo == "MS" || $scope.comboTipoJogo == "MC" || $scope.comboTipoJogo == "C") {
                retorno = true;
            }
            return retorno;
        }

        $scope.carregaLimite = function () {
            if ($scope.comboConcurso.length == 0) {
                return;
            }
            let qtdPremios = []; 
            for (var j = 0; j < $scope.jogos.length; j++) {
                const jogo = $scope.jogos[j];
                for (var c = 0; c < $scope.comboConcurso.length; c++) {
                    const concurso = $scope.comboConcurso[c];
                    if (jogo.cod_jogo == concurso) {
                        qtdPremios.push(jogo.qtd_premios)
                    }
                }
            }
            $scope.qtd_premios_extracao = Math.min(...qtdPremios);
            $http({
                url: 'angularjs-mysql/limiteBicho.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() + 
                      '&operacao=carregar' + 
                      '&codigo=' + $scope.comboConcurso +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.listaLimite = response.data;
                $scope.comboTipoJogo = "VV";
                $scope.premio = {do: 1, ao: 1, disabled: false}
            });
        }

        $scope.incluirPalpite = function() {
            const maxLength = $scope.getMaxLength($scope.comboTipoJogo);
            if ($scope.edtPalpite == undefined || $scope.edtPalpite == null) {
                return;
            }
            var palpite = $scope.edtPalpite.toString();
            if (palpite.length == maxLength && !$scope.chkHabilitarNaQuadra) {
                switch($scope.comboTipoJogo) {
                    case "TSE":
                        $scope.palpites = getTernoSequencia(palpite).map(terno => {return {palpite: terno} });    
                    break;
                    case "TSO":    
                        if ($scope.palpites.length == 0) {
                            $scope.palpites.push({palpite});
                        } else {
                            $scope.palpites = getTernoSoma([$scope.palpites[0].palpite, palpite]).map(terno => {return {palpite: terno} });    
                        }
                    break;
                    default:
                        $scope.palpites.push({palpite});
                    break;
                }
                $scope.edtPalpite = null;
            } else if (palpite.length > maxLength) {
                $scope.edtPalpite = palpite.slice(0, maxLength);
            }
            calcularApostaValorFixo($scope.comboTipoJogo, $scope.valoracao, $scope.palpites, $scope.premio.do, $scope.premio.ao);
        }

        $scope.incluirPalpiteMultiplo = function() {
            const maxLength = 4;
            if ($scope.edtPalpite == undefined || $scope.edtPalpite == null) {
                return;
            }
            var palpite = $scope.edtPalpite.toString();
            if (palpite.length == maxLength && !$scope.chkHabilitarNaQuadra) {
                $scope.palpites.push({palpite});
                $scope.edtPalpite = null;
            } else if (palpite.length > maxLength) {
                $scope.edtPalpite = palpite.slice(0, maxLength);
            }
        }

        $scope.getMaxLength = function (tipoJogo) {
            switch(tipoJogo) {
                case "MS":
                case "MC":
                case "MI":
                case "MCI":
                    return 4;
                case "C":
                case "CI":
                    return 3;
                case "G":
                case "DG":
                case "DG6":
                case "DGC":
                case "TG":
                case "TGC":
                case "TG6":
                case "TG10":
                case "TSE":
                case "TSO":
                case "TEX-A":
                case "TEX-F":
                case "QG":
                case "DZ":
                case "DDZ":
                case "DDZ6":
                case "DDZC":
                case "TDZ":
                case "TDZ6":
                case "TDZ10":
                case "TDZC":
                case "PS":
                case "PC":
                case "PS12":
                case "5P100":
                    return 2;
            }
        }

        $scope.getQtdPalpitesPermitidos = function (tipoJogo) {
            switch(tipoJogo) {
                case "MS":
                case "MC":
                case "C":
                case "G":
                case "DZ":
                case "5P100":
                    return {min: 1, max: 1000};
                case "MI":
                case "MCI":
                case "CI":
                    return {min: 1, max: 1};                
                case "DG":
                case "DG6":       
                case "DDZ":
                case "DDZ6":         
                case "PS":
                case "PC":
                case "PS12":        
                    return {min: 2, max: 2};
                case "TG":
                case "TG6":
                case "TG10":
                case "TDZ":
                case "TDZ6":
                case "TDZ10":
                case "TSE":
                case "TSO":
                case "TEX-A":
                case "TEX-F":
                    return {min: 3, max: 3};
                case "DGC":
                case "DDZC":
                    return {min: 3, max: 10};
				case "TGC":
				case "TDZC":
					return {min: 4, max: 10};
                case "QG":
                    return {min: 15, max:15}
            }
        }

        $scope.mostraMilharBrinde = function () {
            var validacao = typeof $scope.listaCfg === "undefined";
            if (!validacao) {
                var retorno = $scope.listaCfg[0].usa_milhar_brinde && $scope.getValorTotalReal() >= $scope.listaCfg[0].valor_liberar_milhar_brinde;
                return retorno;
            } else {
                return false;
            }
        }

        $scope.onChangeNaQuadra = function() {
            const maxLength = $scope.getMaxLength($scope.comboTipoJogo);
            if ($scope.edtNaQuadra == null || $scope.edtNaQuadra.toString().length < 2) {
                return;
            } 
            
            if ($scope.edtNaQuadra.toString().length > 2) {
                $scope.edtNaQuadra = parseInt($scope.edtNaQuadra.toString().slice(0, 2));
            }

            if (!$scope.edtPalpite || $scope.edtPalpite.toString().length < maxLength) {
                $.alert("Informe o palpite!");
                return;
            }
            
            var palpite = parseInt($scope.edtPalpite);
            
            while (("0".repeat(maxLength) + palpite).slice(-2) != $scope.edtNaQuadra) {                
                $scope.palpites.push({palpite: ("0".repeat(maxLength) + palpite).slice(-maxLength)});
                palpite++;
            }
            $scope.palpites.push({palpite: ("0".repeat(maxLength) + palpite).slice(-maxLength)});
            $scope.edtPalpite = null;
            $scope.edtNaQuadra = null;
        }

        $scope.onChangeTipoJogo = function() {
            inicializarVariaveis();
            checarPalpitesFixos($scope.comboTipoJogo);
        }

        $scope.onChangeTipoPalpite = function() {
            inicializarVariaveis();
        }

        inicializarVariaveis = function() {
            $scope.premio = getPremiacao($scope.comboTipoJogo);
            $scope.valoracao = getValoracao($scope.comboTipoJogo);
            $scope.palpites = [];
            $scope.edtPalpite = null;
            $scope.edtNaQuadra = null;
            $scope.chkHabilitarNaQuadra = false;
            checarDesabilitacaoComboOperacao($scope.comboTipoJogo);
        }

        checarPalpitesFixos = function(tipo) {
            switch(tipo) {
                case "TEX-A":
                    $scope.palpites = getTernoEspecialAberto().map(terno => {return {palpite: terno} });
                    break;
                case "TEX-F":
                    $scope.palpites = getTernoEspecialFechado().map(terno => {return {palpite: terno} });
                    break;
                default:
                    break;
            }
        }

        checarDesabilitacaoComboOperacao = function(tipo) {
            switch(tipo) {
                case "MS":
                case "MI":
                case "MC":
                case "MCI":
                case "C":
                case "CI":
                case "G":
                case "DZ":
                    $scope.disabledComboOperacao = false;
                    break;
                default:
                    $scope.valoracao.operacao = "D";
                    $scope.disabledComboOperacao = true;
            }
        }

        // Remove aposta
        $scope.removerAposta = function (index) {
            $scope.apostas.splice(index, 1);
            $scope.getValorTotal();
        }

        $scope.repetirAposta = function (aposta) {
            $scope.comboTipoJogo = aposta.tipoJogo;
            $scope.onChangeTipoJogo();
            $scope.palpites = aposta.numeros.split("-").map(palpite => { return {palpite} });
            $('html, body').animate({ scrollTop: $('#edtPalpite').offset().top }, 'slow');
        }

        $scope.getRetorno = function (jogo) {
            switch (jogo) {
                case "MS":
                    return $scope.listaCfg[0].premio_milhar_seca;
                case "MC":
                    return parseFloat($scope.listaCfg[0].premio_milhar_seca) + parseFloat($scope.listaCfg[0].premio_centena);
                case "MI":
                    return $scope.listaCfg[0].premio_milhar_seca;
                case "MCI":
                    return parseFloat($scope.listaCfg[0].premio_milhar_seca) + parseFloat($scope.listaCfg[0].premio_centena);
                case "C":
                    return $scope.listaCfg[0].premio_centena;
                case "CI":
                    return $scope.listaCfg[0].premio_centena;
                case "G":
                    return $scope.listaCfg[0].premio_grupo;
                case "DG":
                    return $scope.listaCfg[0].premio_duque_grupo;
                case "DG6":
                    return $scope.listaCfg[0].premio_duque_grupo6;
                case "DGC":
                    return $scope.listaCfg[0].premio_duque_grupo;
                case "TG":
                    return $scope.listaCfg[0].premio_terno_grupo;
                case "TG6":
                    return $scope.listaCfg[0].premio_terno_grupo6;    
                case "TG10":
                    return $scope.listaCfg[0].premio_terno_grupo10;
                case "TGC":
                    return $scope.listaCfg[0].premio_terno_grupo;
                case "TSE":
                    return $scope.listaCfg[0].premio_terno_sequencia;
                case "TSO":
                    return $scope.listaCfg[0].premio_terno_soma;
                case "TEX-A":
                case "TEX-F":
                    return $scope.listaCfg[0].premio_terno_especial;
                case "QG":
                    return $scope.listaCfg[0].premio_quina_grupo;
                case "DZ":
                    return $scope.listaCfg[0].premio_dezena;
                case "DDZ":
                    return $scope.listaCfg[0].premio_duque_dezena;
                case "DDZ6":
                    return $scope.listaCfg[0].premio_duque_dezena6;
                case "DDZC":
                    return $scope.listaCfg[0].premio_duque_dezena;
                case "TDZ":
                    return $scope.listaCfg[0].premio_terno_dezena;
                case "TDZ6":
                    return $scope.listaCfg[0].premio_terno_dezena6;
                case "TDZ10":
                    return $scope.listaCfg[0].premio_terno_dezena10;
                case "TDZC":
                    return $scope.listaCfg[0].premio_terno_dezena;
                case "PS":
                    return $scope.listaCfg[0].premio_passe_seco;
                case "PC":
                    return $scope.listaCfg[0].premio_passe_combinado;
                case "PS12":
                    return $scope.listaCfg[0].premio_passe_seco;
                case "5P100":
                    return 20;
            }
        }

        $scope.getRetornoLimite = function (extracao, jogo) {
            if (!$scope.listaLimite || $scope.listaLimite.length == 0) {
                return 0;
            }
            
            var limites_extracao = $scope.listaLimite.find(l => l.hora_extracao + ' - ' + l.descricao == extracao);
            
            if (!limites_extracao) {
                return 0;
            }
            
            switch (jogo) {
                case "MS":
                case "MC":
                case "MI":
                case "MCI":
                    return limites_extracao.milhar;
                case "C":
                case "CI":
                    return limites_extracao.centena;
                case "G":
                case "5P100":
                    return limites_extracao.grupo; 
                case "DG":
                case "DG6":
                case "DGC":
                    return limites_extracao.duque_grupo;
                case "TG":
                case "TG6":
                case "TG10":
                case "TGC":
                case "TSE":
                case "TSO":
                case "TEX-A":
                case "TEX-F":
                    return limites_extracao.terno_grupo;
                case "QG":
                    return limites_extracao.quina_grupo;
                case "DZ":
                    return limites_extracao.dezena;
                case "DDZ":
                case "DDZ6":
                case "DDZC":
                    return limites_extracao.duque_dezena;
                case "TDZ":
                case "TDZ6":
                case "TDZ10":
                case "TDZC":
                    return limites_extracao.terno_dezena;
                case "PS":
                    return 10000; //$scope.listaLimite[0].premio_passe_seco;
                case "PS12":
                    return 10000;
                case "PC":
                    return 5000;//$scope.listaLimite[0].premio_passe_combinado;
            }
        }

        $scope.pad = function (n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        validarAposta = function(palpites, tipoJogo, doPremio, aoPremio, valor) {
            if (aoPremio < doPremio) {
                throw new Error('O prêmio final deverá ser igual ou maior ao prêmio inicial!');
            }
            if (palpites.length == 0) {
                throw new Error("Informe o(s) palpite(s)");
            }
            validarValor(valor);
            validarQtdPalpites(palpites, tipoJogo);
            validarTamanhoPalpites(palpites, tipoJogo);
        }

        validarValor = function(valor) {
            if (typeof valor === "undefined" || valor == "" || valor == null) {
                document.getElementById("edtValor").focus();
                throw new Error("Informe o VALOR!");
            }
            var valorAux = parseFloat(valor.toString().replace(",", "."));
            if (valorAux <= 0) {
                document.getElementById("edtValor").focus();
                throw new Error("Informe o valor maior que zero!");
            } else if ($scope.valor_min_aposta && valorAux < $scope.valor_min_aposta) {
                document.getElementById("edtValor").focus();
                throw new Error("O valor mínimo de aposta é: R$ " + $scope.valor_min_aposta);
            } else if ($scope.valor_max_aposta && valorAux > $scope.valor_max_aposta) {
                document.getElementById("edtValor").focus();
                throw new Error("O valor máximo de aposta é R$ : " + $scope.valor_max_aposta);
            }
        }

        validarQtdPalpites = function(palpites, tipoJogo) {
            const max = $scope.getQtdPalpitesPermitidos(tipoJogo).max;
            const min = $scope.getQtdPalpitesPermitidos(tipoJogo).min;
            if (palpites.length > max) {
                throw new Error("A quantidade máxima de palpites para esse tipo de jogo é de: " + max);
            }
            if (palpites.length < min) {
                throw new Error("A quantidade mínima de palpites para esse tipo de jogo é de: " + min);
            }   
        }

        validarTamanhoPalpites = function(palpites, tipoJogo) {
            const maxLength = $scope.getMaxLength(tipoJogo);
            for (const palpite of palpites) {
                if (palpite.length != maxLength) {
                    throw new Error("Para este tipo de jogo deve ser informados palpites com " +
                    maxLength + " dígitos.");
                }
                const numero = parseInt(palpite);
                switch (tipoJogo) {
                    case "MS":
                    case "MC":
                    case "MI":
                    case "MCI":
                        if (numero < 0 || numero > 9999) {
                            throw new Error("As milhares devem estar compreendidas no intervalo 0000 a 9999");
                        }
                        break;
                    case "C":
                    case "CI":
                        if (numero < 0 || numero > 999) {
                            throw new Error("As centenas devem estar compreendidas no intervalo 000 a 999");
                        }
                        break;
                    case "DZ":
                    case "DDZ":
                    case "DDZ6":
                    case "DDZC":
                    case "TDZ":
                    case "TDZ6":
                    case "TDZ10":
                    case "TDZC":
                        if (numero < 0 || numero > 99) {
                            throw new Error("As dezenas devem estar compreendidas no intervalo 00 a 99");
                        }
                        break;
                    case "G":
                    case "DG":
                    case "DG6":
                    case "DGC":
                    case "TG":
                    case "TGC":
                    case "TG6":
                    case "TG10":
                    case "TSE":
                    case "TSO":
                    case "TEX-A":
                    case "TEX-F":
                    case "QG":
                    case "PS":
                    case "PC":
                    case "PS12":
                    case "5P100":
                        if (numero < 1 || numero > 25) {
                            throw new Error("Os grupos devem estar compreendidos no intervalo 01 a 25");
                        }
                        break;
                    default:
                        throw new Error("Tipo de jogo não definido!");
                }
            }
        }

        // Adiciona aposta simples
        $scope.adicionarApostaSimples = function () {
            try {
                let palpites = $scope.palpites.map(obj => obj.palpite);
                adicionarAposta(palpites, $scope.comboTipoJogo, $scope.premio.do, $scope.premio.ao, $scope.valoracao.valor, $scope.valoracao.operacao);
                $scope.valoracao.valor = null;
                $scope.palpites = [];
                $scope.edtPalpite = null;
                $scope.edtNaQuadra = null;
                $scope.chkHabilitarNaQuadra = false;
                if ($scope.comboTipoJogo == "TEX-A" || $scope.comboTipoJogo == "TEX-F") {
                    $scope.premio = {do: 1, ao: 1, disabled: false};
                    $scope.comboTipoJogo = "VV";
                }
            } catch(e) {
                $.alert(e.message);
            }
        }

        $scope.adicionarApostaMultipla = function () {
            const qtdApostas = $scope.apostas.length;
            try {
                for (const aposta of $scope.palpitesMultiplo) {
                    adicionarAposta(aposta.palpite, aposta.tipo, aposta.premiacao.do, aposta.premiacao.ao, aposta.valoracao.valor, aposta.valoracao.operacao);
                }
                $scope.edtPalpite = null;
                $scope.palpites = [];
                $scope.palpitesMultiplo = [];
            } catch(e) {
                $scope.apostas = $scope.apostas.splice(0, qtdApostas);
                $scope.getValorTotal();
                $.alert(e.message);
            }
        }

        adicionarAposta = function (palpites, tipoJogo, doPremio, aoPremio, valor, operacao) {
            validarAposta(palpites, tipoJogo, doPremio, aoPremio, valor);
            var sTipo = "Bicho";
            var sValor = "R$ " + valor.toString();
            var sPremio = doPremio + "º ao " + aoPremio + "º Prêmio";
            var sNumeros = palpites.toString().replace(/,/g, "-");
            var valor = getValorAposta(palpites, tipoJogo, doPremio, aoPremio, valor, operacao);
            for (var i = 0; i < $scope.comboConcurso.length; i++) {
                var jogo = $scope.findJogo($scope.comboConcurso[i]);
                const extracao = jogo.hora_extracao + " - " + jogo.desc_hora;
                var retorno = calcularRetorno(extracao, tipoJogo, valor, palpites, doPremio, aoPremio);
                var inversoes = retorno.inversoes;
                var iRetorno = retorno.retorno;
                var numero = parseFloat(iRetorno).toFixed(2).split('.');
                numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                iRetorno = numero.join(',');
                var iPossivelRetornoReal = retorno.retorno;
                var objeto = {
                    numeros: sNumeros,
                    valor: sValor,
                    valorReal: valor,
                    premio: sPremio,
                    premioDoReal: doPremio,
                    premioAoReal: aoPremio,
                    tipo: sTipo,
                    tipoJogo: tipoJogo,
                    cod_jogo: $scope.comboConcurso[i],
                    extracao: extracao,
                    possivelRetorno: iRetorno,
                    possivelRetornoReal: iPossivelRetornoReal,
                    inversoes: inversoes
                };
                $scope.apostas.push(objeto);
            }
            $scope.getValorTotal();
        }

        calcularRetorno = function(extracao, tipo, valor, palpites, premioDo, premioAo) {
            var inversoes = 1;
            var retorno = 0;
            var qtdPremios = premioAo - premioDo + 1;
            switch (tipo) {
                case "MS":
                case "C":
                case "G":
                case "DZ":
                case "5P100":
                    retorno = (valor / (palpites.length * qtdPremios)) * $scope.getRetorno(tipo);
                    break;
                case "MC":
                    retorno = (
                        ($scope.getRetorno('MS') * (valor / 2)) +
                        ($scope.getRetorno('C') * (valor / 2))
                        )/(palpites.length * qtdPremios);
                    break;
                case "MI":
                case "CI":
                    inversoes = $scope.getQtdInversoes(palpites[0]);
                    retorno = (valor / (qtdPremios * inversoes)) * $scope.getRetorno(tipo);
                    break;
                case "MCI":
                    inversoes = $scope.getQtdInversoes(palpites[0]) * 2;
                    retorno = (valor / (qtdPremios * inversoes)) * $scope.getRetorno(tipo);
                    break;
                case "DG":
                case "DG6":
                case "TG":
                case "TG6":
                case "TG10":
                case "TSE":
                case "TSO":
                case "TEX-A":
                case "TEX-F":
                case "QG":
                case "DDZ":
                case "DDZ6":
                case "TDZ":
                case "TDZ6":
                case "TDZ10":
                case "PS":
                case "PS12":
                case "PC":
                    retorno = valor * $scope.getRetorno(tipo);
                    break;
                case "DGC":
                case "DDZC":
                    inversoes = $scope.getQtdInversoesDuqueCombinado(palpites);
                    retorno = (valor / inversoes) * $scope.getRetorno(tipo);
                    break;
                case "TGC":
                case "TDZC":
                    inversoes = $scope.getQtdInversoesTernoCombinado(palpites);
                    retorno = (valor / inversoes) * $scope.getRetorno(tipo);
                    break;
            }
            if ($scope.listaLimite && $scope.listaLimite.length > 0) {
                var iRetornoMaximo = $scope.getRetornoLimite(extracao, tipo);
                if (retorno > iRetornoMaximo && iRetornoMaximo > 0) {
                    retorno = iRetornoMaximo;
                }
            }
            return {inversoes: inversoes, retorno: retorno}
        }

        $scope.getQtdInversoesTernoCombinado = function (arrayPalpite) {
            if (arrayPalpite.length == 3) {
                return 1;
            } else if (arrayPalpite.length == 4) {
                return 4;
            } else if (arrayPalpite.length == 5) {
                return 10;
            } else if (arrayPalpite.length == 6) {
                return 20;
            } else if (arrayPalpite.length == 7) {
                return 35;
            } else if (arrayPalpite.length == 8) {
                return 56;
            } else if (arrayPalpite.length == 9) {
                return 84;
            } else if (arrayPalpite.length == 10) {
                return 120;
            }
        }

        $scope.getQtdInversoesDuqueCombinado = function (arrayPalpite) {
            if (arrayPalpite.length == 2) {
                return 1;
            } else if (arrayPalpite.length == 3) {
                return 3;
            } else if (arrayPalpite.length == 4) {
                return 6;
            } else if (arrayPalpite.length == 5) {
                return 10;
            } else if (arrayPalpite.length == 6) {
                return 15;
            } else if (arrayPalpite.length == 7) {
                return 21;
            } else if (arrayPalpite.length == 8) {
                return 28;
            } else if (arrayPalpite.length == 9) {
                return 36;
            } else if (arrayPalpite.length == 10) {
                return 45;
            }
        }

        $scope.getQtdInversoes = function (milhar) {
            var valorMilhar = "";
            var valorCentena = "";
            var valorDezena = "";
            var qtdInversao = 0;
            var vetor = [];
            var valor = "";

            for (m = 0; m <= 3; m++) {
                valorMilhar = milhar.substring(m, m + 1);

                for (c = 0; c <= 3; c++) {

                    if (c == m) {
                        continue;
                    } else {
                        valorCentena = milhar.substring(c, c + 1)
                    }

                    for (d = 0; d <= 3; d++) {

                        if (d == c || d == m) {
                            continue;
                        } else {
                            valorDezena = milhar.substring(d, d + 1);
                        }

                        for (u = 0; u <= 3; u++) {

                            if (u == d || u == c || u == m) {
                                continue;
                            } else {
                                valor = valorMilhar + valorCentena + valorDezena + milhar.substring(u, u + 1);
                            }

                            if (!existeItemNoVetor(vetor, valor)) {
                                qtdInversao = qtdInversao + 1;
                                vetor.push(valor);
                                break;
                            }

                        }

                    }

                }

            }
            return qtdInversao;
        }

        function existeItemNoVetor(vetor, valor) {
            var retorno = false;
            for (i = 0; i < vetor.length; i++) {
                if (valor == vetor[i]) {
                    retorno = true;
                    break;
                }
            }
            return retorno;
        }

        $scope.getJogos = function () {
            var data = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
            $http({
                method: "POST",
                url: "angularjs-mysql/jogosArea.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + 
                      '&finalizados=false' + 
                      '&site=' + user.getSite() + 
                      '&area=' + user.getArea() + 
                      '&status=L' + 
                      '&operacao=bicho' + 
                      '&data=' + data +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.jogos = response.data;
            }, function (response) {
                $.alert('Erro no $HTTP: ' + response.status)
            });

        }

        getValorAposta = function(palpites, tipoJogo, doPremio, aoPremio, valor, operacao) {
            var valorAux = parseFloat(valor.toString().replace(",", "."));
            switch (tipoJogo) {
                case "MS":
                case "MI":
                case "MC":
                case "MCI":
                case "C":
                case "CI":
                case "G":
                case "DZ":
                    if (operacao == "M") {
                        var iDivisor = aoPremio - doPremio + 1;
                        valorAux = valorAux * (palpites.length * (iDivisor));
                    }
            }            
            return valorAux;
        }

        $scope.getValorTotal = function () {
            var dValorTotal = 0;
            for (i = 0; i < $scope.apostas.length; i++) {
                dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
            }


            var numero = parseFloat(dValorTotal).toFixed(2).split('.');
            numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
            $scope.valorTotal = numero.join(',');
        }

        $scope.getValorTotalReal = function () {
            var dValorTotal = 0;
            for (i = 0; i < $scope.apostas.length; i++) {
                dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
            }

            return dValorTotal;
        }

        $scope.findJogo = function(cod_jogo) {
            for(var j = 0; j < $scope.jogos.length; j++) {
                var jogo = $scope.jogos[j];
                if (jogo.cod_jogo == cod_jogo) {
                    return jogo;
                }
            }
        }

        $scope.gerarBilhete = function () {
            try {
                if ($scope.comboConcurso.length === 0) {
                    $.alert('Informe a EXTRAÇÃO!');
                    return;
                }
                if (!$scope.edtNome) {
                    $.alert('Informe o NOME!');
                    return;
                }
                if ($scope.apostas.length == 0) {
                    $.alert('Faça no mínimo uma aposta!');
                    return;
                }
                if (parseFloat(user.getSaldoAtualizado()) < $scope.getValorTotalReal()) {
                    $.alert('Você não tem saldo para gerar o bilhete!');
                    return;
                }
                if ($scope.mostraMilharBrinde()) {
                    if (typeof $scope.edtMilharBrinde === "undefined" || $scope.edtMilharBrinde == "") {
                        $.alert('Informe a Milhar de Brinde!');
                        document.getElementById("edtMilharBrinde").focus();
                        return;
                    }
                    if ($scope.edtMilharBrinde.length != 4) {
                        $.alert('Informe a Milhar de Brinde com 4 dígitos!');
                        document.getElementById("edtMilharBrinde").focus();
                        return;
                    }

                }
                $.confirm({
                    title: '',
                    content:"Confirma a geração do bilhete?",
                    buttons: {
                        cancelar: function() {},
                        ok: function () {
                            if ($rootScope.loading == true) {
                                return;
                            }                
                            $rootScope.loading = true;
                            var sTelefone = $scope.edtTelefone;
                            if (typeof sTelefone === 'undefined') {
                                sTelefone = "";
                            }
                            var data = new Date();
                            var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
                            var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
                            var ano = data.getFullYear();
                            var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
                            var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
                            var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };
                            var dataAtual = ano + '-' + mes + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;
        
                            // adicionando a milhar de brinde
                            if ($scope.mostraMilharBrinde() && $scope.apostas.length > 0 
                                && !$scope.apostas.find(item => item.numeros === $scope.edtMilharBrinde
                                    && item.valor === 0)) {

                                var jogo = $scope.findJogo($scope.apostas[0].cod_jogo);
                                const extracao = jogo.hora_extracao + " - " + jogo.desc_hora;            
                                var brinde = {
                                    numeros: $scope.edtMilharBrinde,
                                    valor: 0,
                                    valorReal: 0,
                                    premio: "1º ao 1º Prêmio",
                                    premioDoReal: 1,
                                    premioAoReal: 1,
                                    tipo: "Bicho",
                                    cod_jogo: $scope.apostas[0].cod_jogo,
                                    extracao: extracao,
                                    tipoJogo: "MS",
                                    possivelRetorno: $scope.listaCfg[0].valor_retorno_milhar_brinde,
                                    possivelRetornoReal: $scope.listaCfg[0].valor_retorno_milhar_brinde,
                                    inversoes: 1
                                };
                                $scope.apostas.push(brinde);

                            }
                            $http({
                                url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: "cod_usuario=" + user.getCodigo() +
                                    "&cod_site=" + user.getSite() +
                                    "&nome_usuario=" + user.getName() +
                                    "&cod_jogo=" + $scope.comboConcurso +
                                    "&nome=" + $scope.edtNome +
                                    "&telefone=" + sTelefone +
                                    "&registro_cliente=" + $scope.chkRegistroCliente +
                                    "&apostas=" + JSON.stringify($scope.apostas) +
                                    "&data_atual=" + dataAtual +
                                    "&operacao=" +
                                    "&schema=" + user.getSchema()
                            }).then(function (response) {
                                if (response.data.status == 'OK') {
                                    user.atualizaSaldo(response.data.saldo);
                                    user.setBilheteExterno(response.data.pule);
                                    $location.path("/conferir_bilhete");
                                } else {
                                    $rootScope.loading = false;
                                    if (response.data.status == 'INATIVO') {
                                        $.alert(response.data.mensagem);
                                        $location.path("/logout");
                                    } else {
                                        if ($scope.mostraMilharBrinde()) {
                                            $scope.apostas.splice($scope.apostas.length - 1, 1);
                                        }
                                        $.alert(response.data.mensagem);
                                    }
                                }
                            }, function (response) {
                                $rootScope.loading = false;
                                $.alert('Erro no $HTTP: ' + response.status)
                            });        
                        }
                    }
                });
            } finally {
            }
        }

        $scope.togglePalpiteMultiplo = function(tipo) {
            var index = $scope.palpitesMultiplo.findIndex(element => element.tipo == tipo);
            if (index >= 0) {
                $scope.palpitesMultiplo.splice(index, 1);
            } else {
                const palpites = $scope.palpites.map(p => p.palpite);
                const palpite = getPalpiteDaMilhar(tipo, palpites);
                const premiacao = getPremiacao(tipo);
                const valoracao = getValoracao(tipo);
                calcularApostaValorFixo(tipo, valoracao, palpites, premiacao.do, premiacao.ao);
                $scope.palpitesMultiplo.push({tipo: tipo, palpite: palpite, premiacao: premiacao, valoracao: valoracao});
            }
        }

        $scope.removerPalpiteMultiplo = function(index) {
            $scope.palpitesMultiplo.splice(index, 1);
        }

        $scope.duplicarPalpiteMultiplo = function(index) {
            const palpite = JSON.parse(JSON.stringify($scope.palpitesMultiplo.at(index)));
            $scope.palpitesMultiplo.splice(index, 0, palpite);
        }

        $scope.possuiPalpiteMultiplo = function(tipo) {
            return $scope.palpitesMultiplo.findIndex(element => element.tipo == tipo) >= 0;
        }

        function getPalpiteDaMilhar(tipo, palpites) {
            switch(tipo) {
                case 'MS':
                case 'MC':
                case 'MI':
                case 'MCI':
                    return palpites;
                case 'C':
                case 'CI':
                    var centenas = [];
                    for (const milhar of palpites) {
                        centenas.push(milhar.substring(1, 4));
                    }
                    return centenas;
                case 'G':
                case 'DG':
                case 'DGC':
                case 'TG':
                case 'TG10':
                case 'TG6':
                case 'TGC':
                case 'QG':
                case 'PS':
                case 'PS12':
                case 'PC':
                case '5P100':
                    var grupos = [];
                    for (const milhar of palpites) {
                        grupos.push(getGrupoBicho(milhar.substring(2, 4)));
                    }
                    return grupos;
                case 'DZ':
                case 'DDZ':
                case 'DDZ6':
                case 'DDZC':
                case 'TDZ':
                case 'TDZ6':
                case 'TDZ10':
                case 'TDZC':
                    var dezenas = [];
                    for (const milhar of palpites) {
                        dezenas.push(milhar.substring(2, 4));
                    }
                    return dezenas;
                case 'TSE':
                    const milhar = palpites[0];
                    return getTernoSequencia(getGrupoBicho(milhar.substring(2, 4)));
                case 'TSO':
                    return getTernoSoma(palpites.map(milhar => getGrupoBicho(milhar.substring(2, 4))));
                case "TEX-A":
                    return getTernoEspecialAberto();
                case "TEX-F":
                    return getTernoEspecialFechado();
            }
        }

        function getGrupoBicho(dezena) {
            const numero = parseInt(dezena);
            if (numero == 0) {
                return '25';
            }
            const divisao = parseInt(numero/4);
            const resto = numero % 4;
            if (resto == 0) {
                return divisao.toString().padStart(2, '0');
            } else {
                return (divisao + 1).toString().padStart(2, '0');
            }
        }

        function getPremiacao(tipo_jogo) {
            switch(tipo_jogo) {
                case "VV":
                case "MS":
                case "MC":
                case "MI":
                case "MCI":
                case "C":
                case "CI":
                case "G":
                case "DZ":
                case "5P100":
                    if (tipo_jogo == "G" && user.getSite() == 2536 && user.getArea() == 460) {
                        return {do: 1, ao: 1, disabled: false, qtdPremiosVariavel: 5}
                    } else if (tipo_jogo == "G" && user.getSite() == 2536 && ([399, 618].indexOf(parseInt(user.getArea())) == -1)) {
                        return {do: 1, ao: 1, disabled: true};
                    } else if (tipo_jogo == "G" && user.getSite() == 6600) {
                        return {do: 1, ao: 1, disabled: false, qtdPremiosVariavel: 5}
                    } else {
                        return {do: 1, ao: 1, disabled: false};
                    }
                case "DG":
                case "DGC":
                case "TGC":
                case "DDZ":
                case "DDZC":                
                case "PS":
                case "PC":
                case "TG":
                case "QG":
                    if (user.getSite() == 1414) {
                        return {do: 1, ao: 5, disabled: false}
                    } else {
                        return {do: 1, ao: 5, disabled: true}
                    }     
                case "TDZ":
                case "TDZC":
                    if (user.getSite() == 1414 || user.getSite() == 6643) {
                        return {do: 1, ao: 5, disabled: false}
                    } else {
                        return {do: 1, ao: 5, disabled: true}
                    }                               
                case "DG6":
                case "TG6":
                case "DDZ6":
                case "TDZ6":
                    return {do: 6, ao: 10, disabled: true};    
                case "TG10":
                case "TDZ10":
                    return {do: 1, ao: 10, disabled: true};
                case "PS12":
                    return {do: 1, ao: 2, disabled: true};            
                case "TSE":
                case "TSO":
                    return {do: 1, ao: $scope.qtd_premios_extracao, disabled: true};
                case "TEX-A":
                case "TEX-F":
                    return {do: 1, ao: 7, disabled: true};
            }
        }

        function getValoracao(tipo) {
            switch(tipo) {
                case "5P100":
                    return {valor: null, operacao: "M", disabled: true};
                default:
                    return {valor: null, operacao: "D", disabled: false};
            }
        }

        function calcularApostaValorFixo(tipo, valoracao, palpites, doPremio, aoPremio) {
            switch(tipo) {
                case "5P100":
                    valoracao.valor = palpites.length * 5 * (Math.abs(aoPremio - doPremio) + 1);
                default:
                    return;
            } 
        }

        function getTernoSequencia(palpite) {
            const grupo = parseInt(palpite);
            if (grupo <= 0) {
                return ["01", "02", "03"];
            } else if (grupo <= 23) {
                return [grupo.toString().padStart(2, '0'), (grupo + 1).toString().padStart(2, '0'), (grupo + 2).toString().padStart(2, '0')];    
            } else if (grupo == 24) {
                return [(grupo - 1).toString(), grupo.toString(), (grupo + 1).toString()];
            } else {
                return ["23", "24", "25"];                    
            }
        }

        function getTernoSoma(palpites) {
            const grupo1 = parseInt(palpites[0]);
            const grupo2 = parseInt(palpites[1]);
            if (grupo1 + grupo2 > 25) {
                return [grupo1, grupo2, Math.abs(grupo2 - grupo1)].sort((a, b) => a - b).map(grupo => grupo.toString().padStart(2, '0'));
            } else {
                return [grupo1, grupo2, (grupo1 + grupo2)].sort((a, b) => a - b).map(grupo => grupo.toString().padStart(2, '0'));    
            }
        }

        function getTernoEspecialAberto() {
            return ["01", "02", "03"];
        }

        function getTernoEspecialFechado() {
            return ["23", "24", "25"];
        }

        $scope.onRemovePalpite = function() {
            switch($scope.comboTipoJogo) {
                case "TSE":
                case "TSO":
                    $scope.palpites = [];
                break;
                case "TEX-A":
                case "TEX-F":
                    $scope.palpites = [];
                    $scope.comboTipoJogo = "VV";
                    $scope.premio = {do: 1, ao: 1, disabled: false};
                break;
                default:
                break;
            }
            calcularApostaValorFixo($scope.comboTipoJogo, $scope.valoracao, $scope.palpites, $scope.premio.do, $scope.premio.ao);
        }

        $scope.getDescricaoTipoJogoBicho = function (tipo_jogo) {
            return funcoes.getDescricaoTipoJogoBicho(tipo_jogo);
        }

        $scope.alterarTipoMultiplo = function() {
            $scope.alterarValorMultiplo();
        }

        $scope.alterarValorMultiplo = function() {
            let valor = null;
            if ($scope.todosJogosMultiplo.tipo == 'todos') {
                const divisor = $scope.palpitesMultiplo.filter(p => !p.valoracao.disabled).length;
                const divisao = $scope.todosJogosMultiplo.valor/divisor;
                valor = Math.round((divisao + Number.EPSILON) * 100) / 100;
            } else {
                valor = $scope.todosJogosMultiplo.valor;
            }
            for (var i = 0; i < $scope.palpitesMultiplo.length; i++) {
                if (!$scope.palpitesMultiplo[i].valoracao.disabled) {
                    $scope.palpitesMultiplo[i].valoracao.valor = valor;
                }
            }
        }

        $scope.alterarDoPremio = function() {
            calcularApostaValorFixo($scope.comboTipoJogo, $scope.valoracao, $scope.palpites, $scope.premio.do, $scope.premio.ao);
        }

        $scope.alterarDoPremioMultiplo = function(i) {
            if ($scope.palpitesMultiplo[i].valoracao.disabled) {
                calcularApostaValorFixo($scope.palpitesMultiplo[i].tipo, $scope.palpitesMultiplo[i].valoracao, $scope.palpitesMultiplo[i].palpite, $scope.palpitesMultiplo[i].premiacao.do, $scope.palpitesMultiplo[i].premiacao.ao);
            }
        }

        $scope.alterarDoPremioMultiploTodosJogos = function() {
            for (var i = 0; i < $scope.palpitesMultiplo.length; i++) {
                if (!$scope.palpitesMultiplo[i].premiacao.disabled) {
                    $scope.palpitesMultiplo[i].premiacao.do = $scope.todosJogosMultiplo.premiacao.do;
                }
                if ($scope.palpitesMultiplo[i].valoracao.disabled) {
                    calcularApostaValorFixo($scope.palpitesMultiplo[i].tipo, $scope.palpitesMultiplo[i].valoracao, $scope.palpitesMultiplo[i].palpite, $scope.palpitesMultiplo[i].premiacao.do, $scope.palpitesMultiplo[i].premiacao.ao);
                }
            }
        }

        $scope.alterarAoPremio = function() {
            calcularApostaValorFixo($scope.comboTipoJogo, $scope.valoracao, $scope.palpites, $scope.premio.do, $scope.premio.ao);
        }

        $scope.alterarAoPremioMultiplo = function(i) {
            if ($scope.palpitesMultiplo[i].valoracao.disabled) {
                calcularApostaValorFixo($scope.palpitesMultiplo[i].tipo, $scope.palpitesMultiplo[i].valoracao, $scope.palpitesMultiplo[i].palpite, $scope.palpitesMultiplo[i].premiacao.do, $scope.palpitesMultiplo[i].premiacao.ao);
            }
        }

        $scope.alterarAoPremioMultiploTodosJogos = function() {
            for (var i = 0; i < $scope.palpitesMultiplo.length; i++) {
                if (!$scope.palpitesMultiplo[i].premiacao.disabled) {
                    $scope.palpitesMultiplo[i].premiacao.ao = $scope.todosJogosMultiplo.premiacao.ao;
                }
                if ($scope.palpitesMultiplo[i].valoracao.disabled) {
                    calcularApostaValorFixo($scope.palpitesMultiplo[i].tipo, $scope.palpitesMultiplo[i].valoracao, $scope.palpitesMultiplo[i].palpite, $scope.palpitesMultiplo[i].premiacao.do, $scope.palpitesMultiplo[i].premiacao.ao);
                }
            }
        }

        $scope.habilitarPalpiteMultiplo = function(tipo) {
            switch(tipo) {
                case "TSE":
                    return $scope.palpites.length == 1;
                case "TSO":
                    return $scope.palpites.length == 2;
                case "TEX-A":
                    return $scope.palpites.length == 3 && palpitesContemGrupos(getTernoEspecialAberto());
                case "TEX-F":
                    return $scope.palpites.length == 3 && palpitesContemGrupos(getTernoEspecialFechado());                        
                default:
                    return !($scope.palpites.length < $scope.getQtdPalpitesPermitidos(tipo).min  || 
                             $scope.palpites.length > $scope.getQtdPalpitesPermitidos(tipo).max);
            }
        }

        palpitesContemGrupos = function(grupos) {
            for (const grupo of grupos) {
                if (!$scope.palpites.find(milhar => grupo == getGrupoBicho(milhar.palpite.substring(2, 4)))) {
                    return false;
                }
            }
            return true;
        }

        $scope.showPalpites = function(palpites) {
            $.alert(palpites.join(", "));
        }

        $scope.closeSelectBox = () => {
            $("md-backdrop").trigger("click");
        }
    });
