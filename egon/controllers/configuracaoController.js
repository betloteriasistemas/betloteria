angular.module('main').controller('configuracaoCtrl', 
function ($rootScope, $scope, $q, user, $http, $route, configGeral) {
    $scope.nome = user.getNome();
    $scope.areas = null;
    $scope.tiposJogos = [];
    $rootScope.loading = false;
    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        if (user.getSite() == 6638) {
            $scope.desc_2pra500 = 'Bilhetinho Premiado';
        }
    });
    $scope.filtroNumerosSeninha = [{qtd: "14", valor:false},
                                   {qtd: "15", valor:false},
                                   {qtd: "16", valor:false},
                                   {qtd: "17", valor:false},
                                   {qtd: "18", valor:false},
                                   {qtd: "19", valor:false},
                                   {qtd: "20", valor:false},
                                   {qtd: "25", valor:false},
                                   {qtd: "30", valor:false},
                                   {qtd: "35", valor:false},
                                   {qtd: "40", valor:false}];

    $scope.filtroNumerosQuininha = [{qtd:"13", valor:false},
                                    {qtd:"14", valor:false},
                                    {qtd:"15", valor:false},
                                    {qtd:"16", valor:false},
                                    {qtd:"17", valor:false},
                                    {qtd:"18", valor:false},
                                    {qtd:"19", valor:false},
                                    {qtd:"20", valor:false},
                                    {qtd:"25", valor:false},
                                    {qtd:"30", valor:false},
                                    {qtd:"35", valor:false},
                                    {qtd:"40", valor:false},
                                    {qtd:"45", valor:false}];      
                                    
    $scope.filtroNumerosLotinha = [{qtd:"16", valor:false},
                                   {qtd:"17", valor:false},
                                   {qtd:"18", valor:false},
                                   {qtd:"19", valor:false},
                                   {qtd:"20", valor:false},
                                   {qtd:"21", valor:false},
                                   {qtd:"22", valor:false}];   
                                   
    $scope.filtroJogosBicho = [{descricao:"Milhar Seca", tipo:"MS", valor:false},
                               {descricao:"Milhar e Centena", tipo:"MC", valor:false},
                               {descricao:"Milhar Invertida", tipo:"MI", valor:false},
                               {descricao:"Milhar e Centena Invertida", tipo:"MCI", valor:false},
                               {descricao:"Centena", tipo:"C", valor:false},
                               {descricao:"Centena Invertida", tipo:"CI", valor:false},
                               {descricao:"Grupo", tipo:"G", valor:false},
                               {descricao:"Duque de Grupo", tipo:"DG", valor:false},
                               {descricao:"Duque de Grupo Combinado", tipo:"DGC", valor:false}];                                    

    parseValorMonetario = function (valor, valRetorno = 0) {
        return valor ? parseFloat(valor).toFixed(2).replace('.', ',').replace(/\d(?=(\d{3})+\,)/g, '$&.') : valRetorno;
    }

    unparseValorMonetario = function (valor, valRetorno = 0) {
        return valor ? valor.replace(/\./g, '').replace(',', '.') : valRetorno;
    }

    $http({
        method: "POST",
        url: "angularjs-mysql/areas.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: 'site=' + user.getSite() + 
              '&operacao=listarAtivos' +
              '&schema=' + user.getSchema()
    }).then(function (response) {
        $scope.areas = response.data;
    }, function (response) {
        $.alert('Erro no $HTTP: ' + response.status)
    });

    $scope.carregarConfigsArea = function (cod_area) {
        $rootScope.loading = true;
        $scope.comboTipoJogo = null;
        $http({
            url: 'angularjs-mysql/configuracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "site=" + user.getSite() + 
                  "&cod_area=" + cod_area + 
                  "&operacao=geral" +
                  "&schema=" + user.getSchema()
        }).then(function (response) {
            $rootScope.loading = false;
            $scope.listaCfgGeral = response.data;
            $scope.listaCfgGeral[0].valor_min_aposta = parseValorMonetario($scope.listaCfgGeral[0].valor_min_aposta, null);
            $scope.listaCfgGeral[0].valor_max_aposta = parseValorMonetario($scope.listaCfgGeral[0].valor_max_aposta, null);
            
            $scope.tiposJogos = [];
            if ($scope.listaCfgGeral[0].flg_seninha) {
                $scope.tiposJogos.push({nome: 'Seninha', codigo: 'S'});
            }
            if ($scope.listaCfgGeral[0].flg_quininha) {
                $scope.tiposJogos.push({nome: 'Quininha', codigo: 'Q'});
            }
            if ($scope.listaCfgGeral[0].flg_lotinha) {
				$scope.tiposJogos.push({ nome: 'Lotinha', codigo: 'L' });
			}	
            if ($scope.listaCfgGeral[0].flg_bicho) {
                $scope.tiposJogos.push({nome: 'Bicho', codigo: 'B'});
            }
            if ($scope.listaCfgGeral[0].flg_2pra500) {
                $scope.tiposJogos.push({nome: $scope.desc_2pra500, codigo: '2'});
            } 
            if ($scope.listaCfgGeral[0].flg_rifa) {
                $scope.tiposJogos.push({nome: 'Rifa', codigo: 'R'});
            } 
            if ($scope.tiposJogos.length > 0 && $scope.listaCfgGeral[0].flg_multijogos) {
                $scope.tiposJogos.push({nome: 'Multijogos', codigo: 'M'});
            }
            if ($scope.listaCfgGeral[0].flg_super_sena) {
                $scope.tiposJogos.push({nome: 'Super Sena', codigo: 'U'});
            }
            $scope.tiposJogos.push({nome: "Conf. Geral", codigo: "G"});
        }).catch(angular.noop);
    }

    $scope.carregarConfigsTipoJogo = function (cod_area, tipo) {
        $rootScope.loading = true;
        switch(tipo) {
            case 'S':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=seninha" + 
                          '&schema=' + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    $scope.listaCfg = response.data;
                    $scope.listaCfg[0].premio_maximo = parseValorMonetario($scope.listaCfg[0].premio_maximo);
                    $scope.listaCfg[0].premio_14 = parseValorMonetario($scope.listaCfg[0].premio_14);
                    $scope.listaCfg[0].premio_15 = parseValorMonetario($scope.listaCfg[0].premio_15);
                    $scope.listaCfg[0].premio_16 = parseValorMonetario($scope.listaCfg[0].premio_16);
                    $scope.listaCfg[0].premio_17 = parseValorMonetario($scope.listaCfg[0].premio_17);
                    $scope.listaCfg[0].premio_18 = parseValorMonetario($scope.listaCfg[0].premio_18);
                    $scope.listaCfg[0].premio_19 = parseValorMonetario($scope.listaCfg[0].premio_19);
                    $scope.listaCfg[0].premio_20 = parseValorMonetario($scope.listaCfg[0].premio_20);
                    $scope.listaCfg[0].premio_25 = parseValorMonetario($scope.listaCfg[0].premio_25);
                    $scope.listaCfg[0].premio_30 = parseValorMonetario($scope.listaCfg[0].premio_30);
                    $scope.listaCfg[0].premio_35 = parseValorMonetario($scope.listaCfg[0].premio_35);
                    $scope.listaCfg[0].premio_40 = parseValorMonetario($scope.listaCfg[0].premio_40);
                }).catch(angular.noop);
                break;
            case 'U':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=supersena" +
                          '&schema=' + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    $scope.listaCfgSuperSena = response.data;
                    $scope.listaCfgSuperSena[0].premio_maximo = parseValorMonetario($scope.listaCfgSuperSena[0].premio_maximo);
                    $scope.listaCfgSuperSena[0].premio_sena = parseValorMonetario($scope.listaCfgSuperSena[0].premio_sena);
                    $scope.listaCfgSuperSena[0].premio_quina = parseValorMonetario($scope.listaCfgSuperSena[0].premio_quina);
                    $scope.listaCfgSuperSena[0].premio_quadra = parseValorMonetario($scope.listaCfgSuperSena[0].premio_quadra);            
                }).catch(angular.noop);   
                break;
            case 'L':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=lotinha" +
                          "&schema=" + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    $scope.listaCfgLotinha = response.data;
                    $scope.listaCfgLotinha[0].premio_maximo = parseValorMonetario($scope.listaCfgLotinha[0].premio_maximo);
                    $scope.listaCfgLotinha[0].premio_16 = parseValorMonetario($scope.listaCfgLotinha[0].premio_16);
                    $scope.listaCfgLotinha[0].premio_17 = parseValorMonetario($scope.listaCfgLotinha[0].premio_17);
                    $scope.listaCfgLotinha[0].premio_18 = parseValorMonetario($scope.listaCfgLotinha[0].premio_18);
                    $scope.listaCfgLotinha[0].premio_19 = parseValorMonetario($scope.listaCfgLotinha[0].premio_19);
                    $scope.listaCfgLotinha[0].premio_20 = parseValorMonetario($scope.listaCfgLotinha[0].premio_20);
                    $scope.listaCfgLotinha[0].premio_21 = parseValorMonetario($scope.listaCfgLotinha[0].premio_21);
                    $scope.listaCfgLotinha[0].premio_22 = parseValorMonetario($scope.listaCfgLotinha[0].premio_22);
                }).catch(angular.noop);
                break;
            case 'Q':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=quininha" +
                          '&schema=' + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    $scope.listaCfgQuininha = response.data;
                    $scope.listaCfgQuininha[0].premio_maximo = parseValorMonetario($scope.listaCfgQuininha[0].premio_maximo);
                    $scope.listaCfgQuininha[0].premio_13 = parseValorMonetario($scope.listaCfgQuininha[0].premio_13);
                    $scope.listaCfgQuininha[0].premio_14 = parseValorMonetario($scope.listaCfgQuininha[0].premio_14);
                    $scope.listaCfgQuininha[0].premio_15 = parseValorMonetario($scope.listaCfgQuininha[0].premio_15);
                    $scope.listaCfgQuininha[0].premio_16 = parseValorMonetario($scope.listaCfgQuininha[0].premio_16);
                    $scope.listaCfgQuininha[0].premio_17 = parseValorMonetario($scope.listaCfgQuininha[0].premio_17);
                    $scope.listaCfgQuininha[0].premio_18 = parseValorMonetario($scope.listaCfgQuininha[0].premio_18);
                    $scope.listaCfgQuininha[0].premio_19 = parseValorMonetario($scope.listaCfgQuininha[0].premio_19);
                    $scope.listaCfgQuininha[0].premio_20 = parseValorMonetario($scope.listaCfgQuininha[0].premio_20);
                    $scope.listaCfgQuininha[0].premio_25 = parseValorMonetario($scope.listaCfgQuininha[0].premio_25);
                    $scope.listaCfgQuininha[0].premio_30 = parseValorMonetario($scope.listaCfgQuininha[0].premio_30);
                    $scope.listaCfgQuininha[0].premio_35 = parseValorMonetario($scope.listaCfgQuininha[0].premio_35);
                    $scope.listaCfgQuininha[0].premio_40 = parseValorMonetario($scope.listaCfgQuininha[0].premio_40);
                    $scope.listaCfgQuininha[0].premio_45 = parseValorMonetario($scope.listaCfgQuininha[0].premio_45);
                }).catch(angular.noop);
                break;
            case 'B':
                $q.all([
                    $http({
                        url: 'angularjs-mysql/configuracao.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: "site=" + user.getSite() + 
                              "&cod_area=" + cod_area + 
                              "&operacao=bicho" +
                              "&schema=" + user.getSchema()
                    }),
                    $http({
                        url: 'angularjs-mysql/configuracao_tipo_jogo_bicho.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: "&operacao=listar_tipos" +
                              "&schema=" + user.getSchema()
                    })    
                ]).then(function (response) {
                        $rootScope.loading = false;
                        $scope.listaCfgBicho = response[0].data;
                        $scope.listaCfgBicho[0].premio_milhar_centena = parseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca/2 + $scope.listaCfgBicho[0].premio_centena/2);
                        $scope.listaCfgBicho[0].premio_milhar_invertida = parseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca);
                        $scope.listaCfgBicho[0].premio_milhar_seca = parseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca);
                        $scope.listaCfgBicho[0].premio_centena = parseValorMonetario($scope.listaCfgBicho[0].premio_centena);
                        $scope.listaCfgBicho[0].premio_centena_invertida = parseValorMonetario($scope.listaCfgBicho[0].premio_centena_invertida);
                        $scope.listaCfgBicho[0].premio_grupo = parseValorMonetario($scope.listaCfgBicho[0].premio_grupo);
                        $scope.listaCfgBicho[0].premio_duque_grupo = parseValorMonetario($scope.listaCfgBicho[0].premio_duque_grupo);
                        $scope.listaCfgBicho[0].premio_duque_grupo6 = parseValorMonetario($scope.listaCfgBicho[0].premio_duque_grupo6);
                        $scope.listaCfgBicho[0].premio_terno_grupo = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_grupo);
                        $scope.listaCfgBicho[0].premio_terno_grupo10 = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_grupo10);
                        $scope.listaCfgBicho[0].premio_terno_grupo6 = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_grupo6);
                        $scope.listaCfgBicho[0].premio_terno_sequencia = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_sequencia);
                        $scope.listaCfgBicho[0].premio_terno_soma = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_soma);
                        $scope.listaCfgBicho[0].premio_terno_especial = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_especial);
                        $scope.listaCfgBicho[0].premio_quina_grupo = parseValorMonetario($scope.listaCfgBicho[0].premio_quina_grupo);
                        $scope.listaCfgBicho[0].premio_dezena = parseValorMonetario($scope.listaCfgBicho[0].premio_dezena);
                        $scope.listaCfgBicho[0].premio_duque_dezena = parseValorMonetario($scope.listaCfgBicho[0].premio_duque_dezena);
                        $scope.listaCfgBicho[0].premio_duque_dezena6 = parseValorMonetario($scope.listaCfgBicho[0].premio_duque_dezena6);
                        $scope.listaCfgBicho[0].premio_terno_dezena = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_dezena);
                        $scope.listaCfgBicho[0].premio_terno_dezena10 = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_dezena10);
                        $scope.listaCfgBicho[0].premio_terno_dezena6 = parseValorMonetario($scope.listaCfgBicho[0].premio_terno_dezena6);
                        $scope.listaCfgBicho[0].premio_passe_seco = parseValorMonetario($scope.listaCfgBicho[0].premio_passe_seco);
                        $scope.listaCfgBicho[0].premio_passe_combinado = parseValorMonetario($scope.listaCfgBicho[0].premio_passe_combinado);
                        $scope.listaCfgBicho[0].valor_liberar_milhar_brinde = parseValorMonetario($scope.listaCfgBicho[0].valor_liberar_milhar_brinde);
                        $scope.listaCfgBicho[0].valor_retorno_milhar_brinde = parseValorMonetario($scope.listaCfgBicho[0].valor_retorno_milhar_brinde);
                        $scope.listaCfgBicho[0].tipos_jogos_bicho = $scope.listaCfgBicho[0].tipos_jogos_bicho;
                        $scope.tiposJogosBichoSelecionados = $scope.listaCfgBicho[0].tipos_jogos_bicho.map(tipo => tipo.codigo_tipo_jogo);
                        $scope.tiposJogosBicho = response[1].data;
                });
                break;
            case '2':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=2pra500" +
                          "&schema=" + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    $scope.listaCfg2pra500 = response.data;
                    $scope.listaCfg2pra500[0].valor_acumulado = parseValorMonetario($scope.listaCfg2pra500[0].valor_acumulado);
                    $scope.listaCfg2pra500[0].valor_aposta = parseValorMonetario($scope.listaCfg2pra500[0].valor_aposta);
                }).catch(angular.noop);
                break;
            case 'R':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=rifa" +
                          "&schema=" + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    response.data[0].qtd_rifas = parseInt(response.data[0].qtd_rifas);
                    $scope.listaCfgRifa = response.data;
                }).catch(angular.noop);
                break;
            case 'M':
                $http({
                    url: 'angularjs-mysql/configuracao.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "site=" + user.getSite() + 
                          "&cod_area=" + cod_area + 
                          "&operacao=multijogos" +
                          "&schema=" + user.getSchema()
                }).then(function (response) {
                    $rootScope.loading = false;
                    $scope.listaCfgMulti = response.data;
                    $scope.listaCfgMulti[0].valor_minimo_aposta = parseValorMonetario($scope.listaCfgMulti[0].valor_minimo_aposta);
                    $scope.filtroAcertoSeninha = $scope.montaFiltroNumeros($scope.listaCfgMulti[0].qtd_numeros_seninha, $scope.filtroNumerosSeninha);
                    $scope.filtroAcertoQuininha = $scope.montaFiltroNumeros($scope.listaCfgMulti[0].qtd_numeros_quininha, $scope.filtroNumerosQuininha);
                    $scope.filtroAcertoLotinha = $scope.montaFiltroNumeros($scope.listaCfgMulti[0].qtd_numeros_lotinha, $scope.filtroNumerosLotinha);                                          
                    $scope.filtroTipoJogoBicho = $scope.montaFiltroTipoJogoBicho($scope.listaCfgMulti[0].tipos_jogos_bicho, $scope.filtroJogosBicho);
                }).catch(angular.noop);       
                break;
            case 'G':
                $rootScope.loading = false;
                break;
        }
    }

    $scope.montaFiltroNumeros = function(qtd_numeros, filtroRetorno) {       

        var qtd_numeros_arr = qtd_numeros.split("-");
        for (var i=0; i < filtroRetorno.length; i++) {
            filtroRetorno[i].valor = false;
            for (var k=0; k < qtd_numeros_arr.length; k++) {
                if (filtroRetorno[i].qtd == qtd_numeros_arr[k]) {
                    filtroRetorno[i].valor = true;
                    break;
                }
            }           
        }
        return filtroRetorno;
    }

    $scope.converteFitroNumerosParaPersistir = function(filtro_arr) {
        var retorno = '';
        for (var i=0; i<filtro_arr.length; i++) {
            if (filtro_arr[i].valor) {
                if (retorno == '') {
                    retorno = filtro_arr[i].qtd;
                } else {
                    retorno += '-' + filtro_arr[i].qtd;
                }
            }
        }
        return retorno;
    }

    $scope.montaFiltroTipoJogoBicho = function(tipos_jogos_bicho, filtroRetorno) {       

        var tipos_jogos_arr = tipos_jogos_bicho.split("-");
        for (var i=0; i < filtroRetorno.length; i++) {
            filtroRetorno[i].valor = false;
            for (var k=0; k < tipos_jogos_arr.length; k++) {
                if (filtroRetorno[i].tipo == tipos_jogos_arr[k]) {
                    filtroRetorno[i].valor = true;
                    break;
                }
            }           
        }
        return filtroRetorno;
    }

    $scope.converteFitroTiposJogosBichoParaPersistir = function(filtro_arr) {
        var retorno = '';
        for (var i=0; i<filtro_arr.length; i++) {
            if (filtro_arr[i].valor) {
                if (retorno == '') {
                    retorno = filtro_arr[i].tipo;
                } else {
                    retorno += '-' + filtro_arr[i].tipo;
                }
            }
        }
        return retorno;
    }    

    $scope.salvarMultiJogos = function (cod_area) {
        var VALOR_CARTAO_1 = $scope.listaCfgMulti[0].valor_cartao_1;
        var VALOR_CARTAO_2 = $scope.listaCfgMulti[0].valor_cartao_2;
        var VALOR_CARTAO_3 = $scope.listaCfgMulti[0].valor_cartao_3;
        var VALOR_CARTAO_4 = $scope.listaCfgMulti[0].valor_cartao_4;
        var VALOR_CARTAO_5 = $scope.listaCfgMulti[0].valor_cartao_5;
        var VALOR_MINIMO = unparseValorMonetario($scope.listaCfgMulti[0].valor_minimo_aposta);
        var QTD_NUMEROS_SENINHA = $scope.converteFitroNumerosParaPersistir($scope.filtroAcertoSeninha);
        var QTD_NUMEROS_QUININHA = $scope.converteFitroNumerosParaPersistir($scope.filtroAcertoQuininha);
        var QTD_NUMEROS_LOTINHA = $scope.converteFitroNumerosParaPersistir($scope.filtroAcertoLotinha);
        var TIPOS_JOGOS_BICHO = $scope.converteFitroTiposJogosBichoParaPersistir($scope.filtroTipoJogoBicho);
        var CONCURSOS_MARCADOS = $scope.listaCfgMulti[0].concursos_marcados;

        if (VALOR_CARTAO_1 == 0 || VALOR_CARTAO_1 == '' || VALOR_CARTAO_1 < 0) {
            $.alert('Preencha o valor do 1º Cartão com um valor maior ou igual a zero!');
            return;
        }

        if (VALOR_CARTAO_2 == 0 || VALOR_CARTAO_2 == '' || VALOR_CARTAO_2 < 0) {
            $.alert('Preencha o valor do 2º Cartão com um valor maior ou igual a zero!');
            return;
        }
        
        if (VALOR_CARTAO_3 == 0 || VALOR_CARTAO_3 == '' || VALOR_CARTAO_3 < 0) {
            $.alert('Preencha o valor do 3º Cartão com um valor maior ou igual a zero!');
            return;
        }        

        if (VALOR_CARTAO_4 == 0 || VALOR_CARTAO_4 == '' || VALOR_CARTAO_4 < 0) {
            $.alert('Preencha o valor do 4º Cartão com um valor maior ou igual a zero!');
            return;
        }
        
        if (VALOR_CARTAO_5 == 0 || VALOR_CARTAO_5 == '' || VALOR_CARTAO_5 < 0) {
            $.alert('Preencha o valor do 5º Cartão com um valor maior ou igual a zero!');
            return;
        }        

        if (VALOR_MINIMO == 0 || VALOR_MINIMO == '' || VALOR_MINIMO < 0) {
            $.alert('Preencha o Valor Mín. por Aposta com um valor maior ou igual a zero!');
            return;
        }

        if (QTD_NUMEROS_SENINHA == '') {
            $.alert('É necessário selecionar pelo menos uma quantidade de números para vir marcado na Seninha!');
            return;
        }

        if (QTD_NUMEROS_QUININHA == '') {
            $.alert('É necessário selecionar pelo menos uma quantidade de números para vir marcado na Quininha!');
            return;
        }
        
        if (QTD_NUMEROS_LOTINHA == '') {
            $.alert('É necessário selecionar pelo menos uma quantidade de números para vir marcado na Lotinha!');
            return;
        }
        
        if (TIPOS_JOGOS_BICHO == '') {
            $.alert('É necessário selecionar pelo menos um tipo de jogo para vir marcado no Jogo do Bicho!');
            return;
        }        

        if (CONCURSOS_MARCADOS == '' || CONCURSOS_MARCADOS == 'undefined') {
            CONCURSOS_MARCADOS = 'R';
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'valor_cartao_1=' + VALOR_CARTAO_1 +
                '&valor_cartao_2=' + VALOR_CARTAO_2 +
                '&valor_cartao_3=' + VALOR_CARTAO_3 +
                '&valor_cartao_4=' + VALOR_CARTAO_4 +
                '&valor_cartao_5=' + VALOR_CARTAO_5 +
                '&valor_minimo=' + VALOR_MINIMO +
                '&qtd_numeros_seninha=' + QTD_NUMEROS_SENINHA +
                '&qtd_numeros_quininha=' + QTD_NUMEROS_QUININHA +
                '&qtd_numeros_lotinha=' + QTD_NUMEROS_LOTINHA +
                '&tipos_jogos_bicho=' + TIPOS_JOGOS_BICHO +
                '&concursos_marcados=' + CONCURSOS_MARCADOS +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=multijogos' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvar2pra500 = function (cod_area) {
        var VALOR_ACUMULADO = unparseValorMonetario($scope.listaCfg2pra500[0].valor_acumulado);
        var MSG_BILHETE = $scope.listaCfg2pra500[0].msg_bilhete;
        var VALOR_APOSTA = unparseValorMonetario($scope.listaCfg2pra500[0].valor_aposta);

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        if (VALOR_ACUMULADO == 0 || VALOR_ACUMULADO == '' || VALOR_ACUMULADO < 0) {
            $.alert('Preencha o Valor Acumulado com um valor maior ou igual a zero!');
            return;
        }

        if (VALOR_APOSTA == 0 || VALOR_APOSTA == '' || VALOR_APOSTA < 0) {
            $.alert('Preencha o Valor Aposta com um valor maior ou igual a zero!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'valor_acumulado=' + VALOR_ACUMULADO +
                '&valor_aposta=' + VALOR_APOSTA +
                '&msg_bilhete=' + MSG_BILHETE +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=2pra500' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvar = function (cod_area) {
        var PREMIO_MAXIMO = unparseValorMonetario($scope.listaCfg[0].premio_maximo);
        var PREMIO_14 = unparseValorMonetario($scope.listaCfg[0].premio_14);
        var PREMIO_15 = unparseValorMonetario($scope.listaCfg[0].premio_15);
        var PREMIO_16 = unparseValorMonetario($scope.listaCfg[0].premio_16);
        var PREMIO_17 = unparseValorMonetario($scope.listaCfg[0].premio_17);
        var PREMIO_18 = unparseValorMonetario($scope.listaCfg[0].premio_18);
        var PREMIO_19 = unparseValorMonetario($scope.listaCfg[0].premio_19);
        var PREMIO_20 = unparseValorMonetario($scope.listaCfg[0].premio_20);
        var PREMIO_25 = unparseValorMonetario($scope.listaCfg[0].premio_25);
        var PREMIO_30 = unparseValorMonetario($scope.listaCfg[0].premio_30);
        var PREMIO_35 = unparseValorMonetario($scope.listaCfg[0].premio_35);
        var PREMIO_40 = unparseValorMonetario($scope.listaCfg[0].premio_40);
        
        var HABILITA_SORTE = $scope.listaCfg[0].habilita_sorte;
        if (HABILITA_SORTE == true) {
            HABILITA_SORTE = "S";
        } else {
            HABILITA_SORTE = "N";
        }
        var SENA_SORTE = $scope.listaCfg[0].sena_sorte;
        var QUINA_SORTE = $scope.listaCfg[0].quina_sorte;
        var QUADRA_SORTE = $scope.listaCfg[0].quadra_sorte;
        var MSG_BILHETE = $scope.listaCfg[0].msg_bilhete;

        if (PREMIO_MAXIMO == '0.00' || PREMIO_MAXIMO == '' || PREMIO_MAXIMO == '0,' ||
            PREMIO_MAXIMO == '0,0' || PREMIO_MAXIMO == '0,00' || PREMIO_MAXIMO < 0) {
            $.alert('Preencha o Prêmio Máximo com um valor maior que zero!');
            return;
        }

        if (PREMIO_14 == '0' || PREMIO_14 == '' || PREMIO_14 < 0) {
            $.alert('Preencha o Prêmio 14 com um valor maior que zero!');
            return;
        }

        if (PREMIO_15 == '0' || PREMIO_15 == '' || PREMIO_15 < 0) {
            $.alert('Preencha o Prêmio 15 com um valor maior que zero!');
            return;
        }

        if (PREMIO_16 == '0' || PREMIO_16 == '' || PREMIO_16 < 0) {
            $.alert('Preencha o Prêmio 16 com um valor maior que zero!');
            return;
        }

        if (PREMIO_17 == '0' || PREMIO_17 == '' || PREMIO_17 < 0) {
            $.alert('Preencha o Prêmio 17 com um valor maior que zero!');
            return;
        }

        if (PREMIO_18 == '0' || PREMIO_18 == '' || PREMIO_18 < 0) {
            $.alert('Preencha o Prêmio 18 com um valor maior que zero!');
            return;
        }

        if (PREMIO_19 == '0' || PREMIO_19 == '' || PREMIO_19 < 0) {
            $.alert('Preencha o Prêmio 19 com um valor maior que zero!');
            return;
        }

        if (PREMIO_20 == '0' || PREMIO_20 == '' || PREMIO_20 < 0) {
            $.alert('Preencha o Prêmio 20 com um valor maior que zero!');
            return;
        }

        if (PREMIO_25 == '0' || PREMIO_25 == '' || PREMIO_25 < 0) {
            $.alert('Preencha o Prêmio 25 com um valor maior que zero!');
            return;
        }

        if (PREMIO_30 == '0' || PREMIO_30 == '' || PREMIO_30 < 0) {
            $.alert('Preencha o Prêmio 30 com um valor maior que zero!');
            return;
        }

        if (PREMIO_35 == '0' || PREMIO_35 == '' || PREMIO_35 < 0) {
            $.alert('Preencha o Prêmio 35 com um valor maior que zero!');
            return;
        }

        if (PREMIO_40 == '0' || PREMIO_40 == '' || PREMIO_40 < 0) {
            $.alert('Preencha o Prêmio 40 com um valor maior que zero!');
            return;
        }

        if (HABILITA_SORTE == 'S' && (SENA_SORTE == '0.00' || SENA_SORTE == '' || SENA_SORTE < 0)) {
            $.alert('Preencha o percentual da Sena da Sorte com um valor maior que zero!');
            return;
        }

        if (HABILITA_SORTE == 'S' && (QUINA_SORTE == '0.00' || QUINA_SORTE == '' || QUINA_SORTE < 0)) {
            $.alert('Preencha o percentual da Quina da Sorte com um valor maior que zero!');
            return;
        }

        if (HABILITA_SORTE == 'S' && (QUADRA_SORTE == '0.00' || QUADRA_SORTE == '' || QUADRA_SORTE < 0)) {
            $.alert('Preencha o percentual da Quadra da Sorte com um valor maior que zero!');
            return;
        }

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;

        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'premio_maximo=' + PREMIO_MAXIMO +
                '&premio_14=' + PREMIO_14 +
                '&premio_15=' + PREMIO_15 +
                '&premio_16=' + PREMIO_16 +
                '&premio_17=' + PREMIO_17 +
                '&premio_18=' + PREMIO_18 +
                '&premio_19=' + PREMIO_19 +
                '&premio_20=' + PREMIO_20 +
                '&premio_25=' + PREMIO_25 +
                '&premio_30=' + PREMIO_30 +
                '&premio_35=' + PREMIO_35 +
                '&premio_40=' + PREMIO_40 +
                '&habilita_sorte=' + HABILITA_SORTE +
                '&sena_sorte=' + SENA_SORTE +
                '&quina_sorte=' + QUINA_SORTE +
                '&quadra_sorte=' + QUADRA_SORTE +
                '&msg_bilhete=' + MSG_BILHETE +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=seninha' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvarSuperSena = function (cod_area) {
        var PREMIO_MAXIMO = unparseValorMonetario($scope.listaCfgSuperSena[0].premio_maximo);
        var PREMIO_SENA = unparseValorMonetario($scope.listaCfgSuperSena[0].premio_sena);
        var PREMIO_QUINA = unparseValorMonetario($scope.listaCfgSuperSena[0].premio_quina);
        var PREMIO_QUADRA = unparseValorMonetario($scope.listaCfgSuperSena[0].premio_quadra);
        var MSG_BILHETE = $scope.listaCfgSuperSena[0].msg_bilhete;

        if (PREMIO_MAXIMO == '0.00' || PREMIO_MAXIMO == '' || PREMIO_MAXIMO == '0,' ||
            PREMIO_MAXIMO == '0,0' || PREMIO_MAXIMO == '0,00' || PREMIO_MAXIMO < 0) {
            $.alert('Preencha o Prêmio Máximo com um valor maior que zero!');
            return;
        }

        if (PREMIO_SENA == '0' || PREMIO_SENA == '' || PREMIO_SENA < 0) {
            $.alert('Preencha o Prêmio Sena com um valor maior que zero!');
            return;
        }

        if (PREMIO_QUINA == '0' || PREMIO_QUINA == '' || PREMIO_QUINA < 0) {
            $.alert('Preencha o Prêmio Quina com um valor maior que zero!');
            return;
        }

        if (PREMIO_QUADRA == '0' || PREMIO_QUADRA == '' || PREMIO_QUADRA < 0) {
            $.alert('Preencha o Prêmio Quadra com um valor maior que zero!');
            return;
        }

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;

        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'premio_maximo=' + PREMIO_MAXIMO +
                '&premio_sena=' + PREMIO_SENA +
                '&premio_quina=' + PREMIO_QUINA +
                '&premio_quadra=' + PREMIO_QUADRA +
                '&msg_bilhete=' + MSG_BILHETE +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=supersena' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }    

    $scope.salvarQuininha = function (cod_area) {
        var PREMIO_MAXIMO = unparseValorMonetario($scope.listaCfgQuininha[0].premio_maximo);
        var PREMIO_13 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_13);
        var PREMIO_14 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_14);
        var PREMIO_15 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_15);
        var PREMIO_16 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_16);
        var PREMIO_17 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_17);
        var PREMIO_18 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_18);
        var PREMIO_19 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_19);
        var PREMIO_20 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_20);
        var PREMIO_25 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_25);
        var PREMIO_30 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_30);
        var PREMIO_35 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_35);
        var PREMIO_40 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_40);
        var PREMIO_45 = unparseValorMonetario($scope.listaCfgQuininha[0].premio_45);

        var MSG_BILHETE = $scope.listaCfgQuininha[0].msg_bilhete;
        var HABILITA_SORTE = $scope.listaCfgQuininha[0].habilita_sorte;
        if (HABILITA_SORTE == true) {
            HABILITA_SORTE = "S";
        } else {
            HABILITA_SORTE = "N";
        }
        var QUINA_SORTE = $scope.listaCfgQuininha[0].quina_sorte;
        var QUADRA_SORTE = $scope.listaCfgQuininha[0].quadra_sorte;
        var TERNO_SORTE = $scope.listaCfgQuininha[0].terno_sorte;

        if (PREMIO_MAXIMO == 0 || PREMIO_MAXIMO == '' || PREMIO_MAXIMO == '0,' ||
            PREMIO_MAXIMO == '0,0' || PREMIO_MAXIMO == '0,00' || PREMIO_MAXIMO < 0) {
            $.alert('Preencha o Prêmio Máximo com um valor maior que zero!');
            return;
        }

        if (PREMIO_13 == '0' || PREMIO_13 == '' || PREMIO_13 < 0) {
            $.alert('Preencha o Prêmio 13 com um valor maior que zero!');
            return;
        }

        if (PREMIO_14 == '0' || PREMIO_14 == '' || PREMIO_14 < 0) {
            $.alert('Preencha o Prêmio 14 com um valor maior que zero!');
            return;
        }

        if (PREMIO_15 == '0' || PREMIO_15 == '' || PREMIO_15 < 0) {
            $.alert('Preencha o Prêmio 15 com um valor maior que zero!');
            return;
        }

        if (PREMIO_16 == '0' || PREMIO_16 == '' || PREMIO_16 < 0) {
            $.alert('Preencha o Prêmio 16 com um valor maior que zero!');
            return;
        }

        if (PREMIO_17 == '0' || PREMIO_17 == '' || PREMIO_17 < 0) {
            $.alert('Preencha o Prêmio 17 com um valor maior que zero!');
            return;
        }

        if (PREMIO_18 == '0' || PREMIO_18 == '' || PREMIO_18 < 0) {
            $.alert('Preencha o Prêmio 18 com um valor maior que zero!');
            return;
        }

        if (PREMIO_19 == '0' || PREMIO_19 == '' || PREMIO_19 < 0) {
            $.alert('Preencha o Prêmio 19 com um valor maior que zero!');
            return;
        }

        if (PREMIO_20 == '0' || PREMIO_20 == '' || PREMIO_20 < 0) {
            $.alert('Preencha o Prêmio 20 com um valor maior que zero!');
            return;
        }

        if (PREMIO_25 == '0' || PREMIO_25 == '' || PREMIO_25 < 0) {
            $.alert('Preencha o Prêmio 25 com um valor maior que zero!');
            return;
        }

        if (PREMIO_30 == '0' || PREMIO_30 == '' || PREMIO_30 < 0) {
            $.alert('Preencha o Prêmio 30 com um valor maior que zero!');
            return;
        }

        if (PREMIO_35 == '0' || PREMIO_35 == '' || PREMIO_35 < 0) {
            $.alert('Preencha o Prêmio 35 com um valor maior que zero!');
            return;
        }

        if (PREMIO_40 == '0' || PREMIO_40 == '' || PREMIO_40 < 0) {
            $.alert('Preencha o Prêmio 40 com um valor maior que zero!');
            return;
        }

        if (PREMIO_45 == '0' || PREMIO_45 == '' || PREMIO_45 < 0) {
            $.alert('Preencha o Prêmio 45 com um valor maior que zero!');
            return;
        }

        if (HABILITA_SORTE == 'S' && (QUINA_SORTE == '0.00' || QUINA_SORTE == '' || QUINA_SORTE < 0)) {
            $.alert('Preencha o percentual da Quina da Sorte com um valor maior que zero!');
            return;
        }

        if (HABILITA_SORTE == 'S' && (QUADRA_SORTE == '0.00' || QUADRA_SORTE == '' || QUADRA_SORTE < 0)) {
            $.alert('Preencha o percentual da Quadra da Sorte com um valor maior que zero!');
            return;
        }

        if (HABILITA_SORTE == 'S' && (TERNO_SORTE == '0.00' || TERNO_SORTE == '' || TERNO_SORTE < 0)) {
            $.alert('Preencha o percentual do Terno da Sorte com um valor maior que zero!');
            return;
        }

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;

        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'premio_maximo=' + PREMIO_MAXIMO +
                '&premio_13=' + PREMIO_13 +
                '&premio_14=' + PREMIO_14 +
                '&premio_15=' + PREMIO_15 +
                '&premio_16=' + PREMIO_16 +
                '&premio_17=' + PREMIO_17 +
                '&premio_18=' + PREMIO_18 +
                '&premio_19=' + PREMIO_19 +
                '&premio_20=' + PREMIO_20 +
                '&premio_25=' + PREMIO_25 +
                '&premio_30=' + PREMIO_30 +
                '&premio_35=' + PREMIO_35 +
                '&premio_40=' + PREMIO_40 +
                '&premio_45=' + PREMIO_45 +
                '&msg_bilhete=' + MSG_BILHETE +
                '&habilita_sorte=' + HABILITA_SORTE +
                '&quina_sorte=' + QUINA_SORTE +
                '&quadra_sorte=' + QUADRA_SORTE +
                '&terno_sorte=' + TERNO_SORTE +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=quininha' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvarLotinha = function (cod_area) {
        var PREMIO_MAXIMO = unparseValorMonetario($scope.listaCfgLotinha[0].premio_maximo);
        var PREMIO_16 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_16);
        var PREMIO_17 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_17);
        var PREMIO_18 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_18);
        var PREMIO_19 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_19);
        var PREMIO_20 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_20);
        var PREMIO_21 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_21);
        var PREMIO_22 = unparseValorMonetario($scope.listaCfgLotinha[0].premio_22);
        
        var MSG_BILHETE = $scope.listaCfgLotinha[0].msg_bilhete;
        
        if (PREMIO_MAXIMO == 0 || PREMIO_MAXIMO == '' || PREMIO_MAXIMO == '0,' ||
            PREMIO_MAXIMO == '0,0' || PREMIO_MAXIMO == '0,00' || PREMIO_MAXIMO < 0) {
            $.alert('Preencha o Prêmio Máximo com um valor maior que zero!');
            return;
        }

        if (PREMIO_16 == '0' || PREMIO_16 == '' || PREMIO_16 < 0) {
            $.alert('Preencha o Prêmio 16 com um valor maior que zero!');
            return;
        }

        if (PREMIO_17 == '0' || PREMIO_17 == '' || PREMIO_17 < 0) {
            $.alert('Preencha o Prêmio 17 com um valor maior que zero!');
            return;
        }

        if (PREMIO_18 == '0' || PREMIO_18 == '' || PREMIO_18 < 0) {
            $.alert('Preencha o Prêmio 18 com um valor maior que zero!');
            return;
        }

        if (PREMIO_19 == '0' || PREMIO_19 == '' || PREMIO_19 < 0) {
            $.alert('Preencha o Prêmio 19 com um valor maior que zero!');
            return;
        }

        if (PREMIO_20 == '0' || PREMIO_20 == '' || PREMIO_20 < 0) {
            $.alert('Preencha o Prêmio 20 com um valor maior que zero!');
            return;
        }

        if (PREMIO_21 == '0' || PREMIO_21 == '' || PREMIO_21 < 0) {
            $.alert('Preencha o Prêmio 21 com um valor maior que zero!');
            return;
        }

        if (PREMIO_22 == '0' || PREMIO_22 == '' || PREMIO_22 < 0) {
            $.alert('Preencha o Prêmio 22 com um valor maior que zero!');
            return;
        }

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;

        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'premio_maximo=' + PREMIO_MAXIMO +
                '&premio_16=' + PREMIO_16 +
                '&premio_17=' + PREMIO_17 +
                '&premio_18=' + PREMIO_18 +
                '&premio_19=' + PREMIO_19 +
                '&premio_20=' + PREMIO_20 +
                '&premio_21=' + PREMIO_21 +
                '&premio_22=' + PREMIO_22 +
                '&msg_bilhete=' + MSG_BILHETE +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=lotinha' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvarBicho = function (cod_area) {
        var PREMIO_MILHAR_CENTENA = unparseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca)/2 + unparseValorMonetario($scope.listaCfgBicho[0].premio_centena)/2;
        var PREMIO_MILHAR_SECA = unparseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca);
        var PREMIO_MILHAR_INVERTIDA = unparseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca);
        var PREMIO_CENTENA = unparseValorMonetario($scope.listaCfgBicho[0].premio_centena);
        var PREMIO_CENTENA_INVERTIDA = unparseValorMonetario($scope.listaCfgBicho[0].premio_centena);
        var PREMIO_GRUPO = unparseValorMonetario($scope.listaCfgBicho[0].premio_grupo);
        var PREMIO_DUQUE_GRUPO = unparseValorMonetario($scope.listaCfgBicho[0].premio_duque_grupo);
        var PREMIO_DUQUE_GRUPO6 = unparseValorMonetario($scope.listaCfgBicho[0].premio_duque_grupo6);
        var PREMIO_TERNO_GRUPO = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_grupo);
        var PREMIO_TERNO_GRUPO10 = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_grupo10);
        var PREMIO_TERNO_GRUPO6 = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_grupo6);
        var PREMIO_TERNO_SEQUENCIA = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_sequencia);
        var PREMIO_TERNO_SOMA = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_soma);
        var PREMIO_TERNO_ESPECIAL = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_especial);
        var PREMIO_QUINA_GRUPO = unparseValorMonetario($scope.listaCfgBicho[0].premio_quina_grupo);
        var PREMIO_DEZENA = unparseValorMonetario($scope.listaCfgBicho[0].premio_dezena);
        var PREMIO_DUQUE_DEZENA = unparseValorMonetario($scope.listaCfgBicho[0].premio_duque_dezena);
        var PREMIO_DUQUE_DEZENA6 = unparseValorMonetario($scope.listaCfgBicho[0].premio_duque_dezena6);
        var PREMIO_TERNO_DEZENA = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_dezena);
        var PREMIO_TERNO_DEZENA10 = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_dezena10);
        var PREMIO_TERNO_DEZENA6 = unparseValorMonetario($scope.listaCfgBicho[0].premio_terno_dezena6);
        var PREMIO_PASSE_SECO = unparseValorMonetario($scope.listaCfgBicho[0].premio_passe_seco);
        var PREMIO_PASSE_COMBINADO = unparseValorMonetario($scope.listaCfgBicho[0].premio_passe_combinado);

        var USA_MILHAR_BRINDE = $scope.listaCfgBicho[0].usa_milhar_brinde;
        var VALOR_LIBERAR_MILHAR_BRINDE = unparseValorMonetario($scope.listaCfgBicho[0].valor_liberar_milhar_brinde);
        var VALOR_RETORNO_MILHAR_BRINDE = unparseValorMonetario($scope.listaCfgBicho[0].valor_retorno_milhar_brinde);

        if (USA_MILHAR_BRINDE == true) {
            USA_MILHAR_BRINDE = "S";
        } else {
            USA_MILHAR_BRINDE = "N";
        }

        var MSG_BILHETE = $scope.listaCfgBicho[0].msg_bilhete;

        if (PREMIO_MILHAR_CENTENA == '0' || PREMIO_MILHAR_CENTENA == '' || PREMIO_MILHAR_CENTENA < 0) {
            $.alert('Preencha o prêmio da Milhar e Centena com um valor maior que zero!');
            return;
        }

        if (PREMIO_MILHAR_SECA == '0' || PREMIO_MILHAR_SECA == '' || PREMIO_MILHAR_SECA < 0) {
            $.alert('Preencha o prêmio da Milhar Seca com um valor maior que zero!');
            return;
        }

        if (PREMIO_CENTENA == '0' || PREMIO_CENTENA == '' || PREMIO_CENTENA < 0) {
            $.alert('Preencha o prêmio da Centena com um valor maior que zero!');
            return;
        }

        if (PREMIO_GRUPO == '0' || PREMIO_GRUPO == '' || PREMIO_GRUPO < 0) {
            $.alert('Preencha o prêmio do Grupo com um valor maior que zero!');
            return;
        }

        if (PREMIO_DUQUE_GRUPO == '0' || PREMIO_DUQUE_GRUPO == '' || PREMIO_DUQUE_GRUPO < 0) {
            $.alert('Preencha o prêmio do Duque Grupo 1/5 com um valor maior que zero!');
            return;
        }

        if (PREMIO_DUQUE_GRUPO6 == '0' || PREMIO_DUQUE_GRUPO6 == '' || PREMIO_DUQUE_GRUPO6 < 0) {
            $.alert('Preencha o prêmio do Duque Grupo 6/10 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_GRUPO == '0' || PREMIO_TERNO_GRUPO == '' || PREMIO_TERNO_GRUPO < 0) {
            $.alert('Preencha o prêmio do Terno Grupo 1/5 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_GRUPO10 == '0' || PREMIO_TERNO_GRUPO10 == '' || PREMIO_TERNO_GRUPO10 < 0) {
            $.alert('Preencha o prêmio do Terno Grupo 1/10 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_GRUPO6 == '0' || PREMIO_TERNO_GRUPO6 == '' || PREMIO_TERNO_GRUPO6 < 0) {
            $.alert('Preencha o prêmio do Terno Grupo 6/10 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_SEQUENCIA == '0' || PREMIO_TERNO_SEQUENCIA == '' || PREMIO_TERNO_SEQUENCIA < 0) {
            $.alert('Preencha o prêmio do Terno de Sequência com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_SOMA == '0' || PREMIO_TERNO_SOMA == '' || PREMIO_TERNO_SOMA < 0) {
            $.alert('Preencha o prêmio do Terno de Soma com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_ESPECIAL == '0' || PREMIO_TERNO_ESPECIAL == '' || PREMIO_TERNO_ESPECIAL < 0) {
            $.alert('Preencha o prêmio do Terno Especial com um valor maior que zero!');
            return;
        }

        if (PREMIO_QUINA_GRUPO == '0' || PREMIO_QUINA_GRUPO == '' || PREMIO_QUINA_GRUPO < 0) {
            $.alert('Preencha o prêmio da Quina de Grupo com um valor maior que zero!');
            return;
        }

        if (PREMIO_DEZENA == '0' || PREMIO_DEZENA == '' || PREMIO_DEZENA < 0) {
            $.alert('Preencha o prêmio da Dezena com um valor maior que zero!');
            return;
        }

        if (PREMIO_DUQUE_DEZENA == '0' || PREMIO_DUQUE_DEZENA == '' || PREMIO_DUQUE_DEZENA < 0) {
            $.alert('Preencha o prêmio do Duque Dezena 1/5 com um valor maior que zero!');
            return;
        }

        if (PREMIO_DUQUE_DEZENA6 == '0' || PREMIO_DUQUE_DEZENA6 == '' || PREMIO_DUQUE_DEZENA6 < 0) {
            $.alert('Preencha o prêmio do Duque Dezena 6/10 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_DEZENA == '0' || PREMIO_TERNO_DEZENA == '' || PREMIO_TERNO_DEZENA < 0) {
            $.alert('Preencha o prêmio do Terno de Dezena 1/5 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_DEZENA10 == '0' || PREMIO_TERNO_DEZENA10 == '' || PREMIO_TERNO_DEZENA10 < 0) {
            $.alert('Preencha o prêmio do Terno de Dezena 1/10 com um valor maior que zero!');
            return;
        }

        if (PREMIO_TERNO_DEZENA6 == '0' || PREMIO_TERNO_DEZENA6 == '' || PREMIO_TERNO_DEZENA6 < 0) {
            $.alert('Preencha o prêmio do Terno de Dezena 6/10 com um valor maior que zero!');
            return;
        }

        if (PREMIO_PASSE_SECO == '0' || PREMIO_PASSE_SECO == '' || PREMIO_PASSE_SECO < 0) {
            $.alert('Preencha o prêmio do Passe Seco com um valor maior que zero!');
            return;
        }

        if (PREMIO_PASSE_COMBINADO == '0' || PREMIO_PASSE_COMBINADO == '' || PREMIO_PASSE_COMBINADO < 0) {
            $.alert('Preencha o prêmio do Passe Combinado com um valor maior que zero!');
            return;
        }

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        if (USA_MILHAR_BRINDE == 'S' && (VALOR_LIBERAR_MILHAR_BRINDE == '' || VALOR_LIBERAR_MILHAR_BRINDE < 0)) {
            $.alert('Preencha o valor para liberar milhar brinde com um valor maior ou igual a zero!');
            return;
        }

        if (USA_MILHAR_BRINDE == 'S' && (VALOR_RETORNO_MILHAR_BRINDE == 0 || VALOR_RETORNO_MILHAR_BRINDE == '' || VALOR_RETORNO_MILHAR_BRINDE < 0)) {
            $.alert('Preencha o Retorno da Milhar Brinde com um valor maior que zero!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;

        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'premio_milhar_centena=' + PREMIO_MILHAR_CENTENA 
                + '&premio_milhar_seca=' + PREMIO_MILHAR_SECA
                + '&premio_milhar_invertida=' + PREMIO_MILHAR_INVERTIDA 
                + '&premio_centena=' + PREMIO_CENTENA
                + '&premio_centena_invertida=' + PREMIO_CENTENA_INVERTIDA 
                + '&premio_grupo=' + PREMIO_GRUPO
                + '&premio_duque_grupo=' + PREMIO_DUQUE_GRUPO 
                + '&premio_duque_grupo6=' + PREMIO_DUQUE_GRUPO6 
                + '&premio_terno_grupo=' + PREMIO_TERNO_GRUPO 
                + '&premio_terno_grupo10=' + PREMIO_TERNO_GRUPO10
                + '&premio_terno_grupo6=' + PREMIO_TERNO_GRUPO6
                + '&premio_terno_sequencia=' + PREMIO_TERNO_SEQUENCIA
                + '&premio_terno_soma=' + PREMIO_TERNO_SOMA
                + '&premio_terno_especial=' + PREMIO_TERNO_ESPECIAL
                + '&premio_quina_grupo=' + PREMIO_QUINA_GRUPO
                + '&premio_dezena=' + PREMIO_DEZENA 
                + '&premio_duque_dezena=' + PREMIO_DUQUE_DEZENA
                + '&premio_duque_dezena6=' + PREMIO_DUQUE_DEZENA6
                + '&premio_terno_dezena=' + PREMIO_TERNO_DEZENA
                + '&premio_terno_dezena10=' + PREMIO_TERNO_DEZENA10
                + '&premio_terno_dezena6=' + PREMIO_TERNO_DEZENA6 
                + '&premio_passe_seco=' + PREMIO_PASSE_SECO
                + '&premio_passe_combinado=' + PREMIO_PASSE_COMBINADO 
                + '&msg_bilhete=' + MSG_BILHETE  
                + '&usa_milhar_brinde=' + USA_MILHAR_BRINDE 
                + '&valor_liberar_milhar_brinde=' + VALOR_LIBERAR_MILHAR_BRINDE
                + '&valor_retorno_milhar_brinde=' + VALOR_RETORNO_MILHAR_BRINDE
                + '&tipos_jogos_bicho=' + JSON.stringify($scope.tiposJogosBichoSelecionados)
                + '&site=' + user.getSite()
                + '&cod_usuario=' + user.getCodigo()
                + '&cod_area=' + cod_area
                + '&desc_area=' + area_desc
                + '&operacao=bicho'
                + '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvarRifa = function (cod_area) {
        var MSG_BILHETE = $scope.listaCfgRifa[0].msg_bilhete;
        var QTD_RIFAS = $scope.listaCfgRifa[0].qtd_rifas;

        if (QTD_RIFAS < 100 || QTD_RIFAS > 10000) {
            $.alert('Informe um valor entre 100 e 10000 rifas!');
            return;
        }

        if (MSG_BILHETE == '' || MSG_BILHETE == 'undefined') {
            $.alert('Preencha a Mensagem do Bilhete com algum texto!');
            return;
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data:'msg_bilhete=' + MSG_BILHETE +
                '&qtd_rifas=' + QTD_RIFAS +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&operacao=rifa' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.salvarConfGeral = function (cod_area) {
        var MINUTOS_CANCELAMENTO = $scope.listaCfgGeral[0].minutos_cancelamento;
        var FLG_SENINHA = $scope.listaCfgGeral[0].flg_seninha == true ? 'S' : 'N';
        var FLG_QUININHA = $scope.listaCfgGeral[0].flg_quininha == true ? 'S' : 'N';
        var FLG_LOTINHA = $scope.listaCfgGeral[0].flg_lotinha == true ? 'S' : 'N';
        var FLG_BICHO = $scope.listaCfgGeral[0].flg_bicho == true ? 'S' : 'N';
        var FLG_2PRA500 = $scope.listaCfgGeral[0].flg_2pra500 == true ? 'S' : 'N';
        var FLG_RIFA = $scope.listaCfgGeral[0].flg_rifa == true ? 'S' : 'N';
        var FLG_MULTIJOGOS = $scope.listaCfgGeral[0].flg_multijogos == true ? 'S' : 'N';
        var FLG_SUPERSENA = $scope.listaCfgGeral[0].flg_super_sena == true ? 'S' : 'N';
        var NU_BILHETE_GRANDE = $scope.listaCfgGeral[0].nu_bilhete_grande == true ? 'S' : 'N';
        var NU_BILHETE_NEGRITO = $scope.listaCfgGeral[0].nu_bilhete_negrito == true ? 'S' : 'N';
        var NU_BILHETE_SUBLINHADO = $scope.listaCfgGeral[0].nu_bilhete_sublinhado == true ? 'S' : 'N';
        var VALOR_MIN_APOSTA = unparseValorMonetario($scope.listaCfgGeral[0].valor_min_aposta, null);
        var VALOR_MAX_APOSTA = unparseValorMonetario($scope.listaCfgGeral[0].valor_max_aposta, null);
        var PRESTAR_CONTA_AUTOMATIZADA = $scope.listaCfgGeral[0].prestar_conta_automatizada;
        var NU_WHATSAPP = $scope.listaCfgGeral[0].nu_whatsapp;
        var MODALIDADE_PADRAO_BV = $scope.listaCfgGeral[0].modalidade_padrao_bv;

        if (MINUTOS_CANCELAMENTO == '' || MINUTOS_CANCELAMENTO == 'undefined' || MINUTOS_CANCELAMENTO < 0) {
            $.alert('Preencha os Minutos para Cancelamento com um valor maior ou igual a zero!');
            return;
        }

        if (VALOR_MIN_APOSTA != null && VALOR_MIN_APOSTA <= 0) {
            $.alert('Preencha o Valor Mínimo de Aposta com um valor maior que zero!');
        }

        if (VALOR_MAX_APOSTA != null && VALOR_MAX_APOSTA <= 0) {
            $.alert('Preencha o Valor Máximo de Aposta com um valor maior que zero!');
        }

        var area_desc = $("#comboAreas")[0].selectedOptions[0].label;
        $rootScope.loading = true;

        $http({
            url: 'angularjs-mysql/salvarConfiguracao.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data:
                'flg_seninha=' + FLG_SENINHA +
                '&flg_quininha=' + FLG_QUININHA +
                '&flg_lotinha=' + FLG_LOTINHA +
                '&flg_bicho=' + FLG_BICHO +
                '&flg_2pra500=' + FLG_2PRA500 +
                '&flg_rifa=' + FLG_RIFA +
                '&flg_multijogos=' + FLG_MULTIJOGOS +
                '&flg_supersena=' + FLG_SUPERSENA +
                '&nu_bilhete_grande=' + NU_BILHETE_GRANDE +
                '&nu_bilhete_negrito=' + NU_BILHETE_NEGRITO +
                '&nu_bilhete_sublinhado=' + NU_BILHETE_SUBLINHADO +
                '&minutos_cancelamento=' + MINUTOS_CANCELAMENTO +
                '&valor_min_aposta=' + VALOR_MIN_APOSTA +
                '&valor_max_aposta=' + VALOR_MAX_APOSTA +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&cod_area=' + cod_area +
                '&desc_area=' + area_desc +
                '&prestar_conta_automatizada=' + PRESTAR_CONTA_AUTOMATIZADA +
                '&nu_whatsapp=' + NU_WHATSAPP +
                '&modalidade_padrao_bv=' + MODALIDADE_PADRAO_BV +
                '&operacao=geral' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert("Salvo com sucesso!");
                configGeral.registerUpdateCallback();
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
            $rootScope.loading = false;
        })
    }

    $scope.showHelp = function (tipo) {
        switch(tipo) {
            case 'MI':
                $.alert("<b>Obs.:</b>Cotação da Milhar Invertida é a mesma da Milhar Seca. <br> <b>Cálculo do Possível Retorno:</b> <br>\
                 Possível Retorno = (Valor da aposta x Cotação da Milhar Seca)&#247;(Quantidade de prêmios x Quantidade de Inversões)");
                break;
            case 'CI':
                $.alert('<b>Obs.:</b>Cotação da Centena Invertida é a mesma da Centena. <br> <b>Cálculo do Possível Retorno:</b> <br>\
                Possível Retorno = (Valor da aposta x Cotação da Centena)&#247;(Quantidade de prêmios x Quantidade de Inversões)');
                break;
            case 'MC':
                $.alert('<b>Obs.:</b>Cotação da Milhar com Centena = (Cotação da Milhar Seca + Cotação da Centena) &#247; 2. <br> <b>Cálculo do Possível Retorno:</b> <br> \
                Possível Retorno = (Valor da aposta x Cotação da Milhar Seca) + (Valor da aposta x Cotação da Centena) &#247; (2 x Quantidade de prêmios)');
                break;
            case 'MCI':
                $.alert('<b>Obs.:</b>Cotação da Milhar com Centena Invertida = (Cotação da Milhar Seca + Cotação da Centena) &#247; 2. <br> <b>Cálculo do Possível Retorno:</b> <br> \
                Possível Retorno = ((Valor da aposta x Cotação da Milhar Seca) + (Valor da aposta x Cotação da Centena))&#247;(2 x Quantidade de prêmios x Quantidade de Inversões)');
                break;
            case 'PS12':
                $.alert('<b>Obs.:</b>Cotação do Passe 1/2 é a mesma do Passe Seco. <br> <b>Cálculo do Possível Retorno:</b> <br> \
                 Possível Retorno = Valor da aposta x Cotação do Passe Seco');
                break;
        }
    }

    $scope.calcularMilharCentena = function() {
        $scope.listaCfgBicho[0].premio_milhar_centena = parseValorMonetario(unparseValorMonetario($scope.listaCfgBicho[0].premio_milhar_seca)/2 + unparseValorMonetario($scope.listaCfgBicho[0].premio_centena)/2);
    }
});