<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

$response = [];

$return_arr = array();

$query = "";

 if($operacao == 'listar_extracoes') {
    $query = "SELECT EA.COD_EXTRACAO,
                     EA.NOME_BANCA                   
                FROM EXTRACAO_AUTOMATIZADA EA
                    INNER JOIN DIA_EXTRACAO_AUTOMATIZADA DEA ON EA.COD_EXTRACAO = DEA.COD_EXTRACAO
                    INNER JOIN DIA_HORA_EXTRACAO DHE ON DEA.COD_DIA_EXTRACAO = DHE.COD_DIA_EXTRACAO
                    INNER JOIN HORA_EXTRACAO_AUTOMATIZADA HEA ON DHE.COD_HORA_EXTRACAO = HEA.COD_HORA_EXTRACAO
                WHERE DHE.STATUS = 'A'
                GROUP BY EA.COD_EXTRACAO
                ORDER BY EA.NOME_BANCA";

    $result = mysqli_query($con, $query);

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['nome_banca'] = $row['NOME_BANCA'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };
 } else if ($operacao == 'listar_horarios') { 
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);

    $query =
        "select extracaoauto.cod_extracao, 
                extracaoauto.nome_banca, 
                hora.cod_hora_extracao, 
                hora.hora, 
                dia.cod_dia_extracao, 
                dia.dia 
        from extracao_automatizada extracaoauto
            inner join dia_extracao_automatizada dia 
                on extracaoauto.cod_extracao = dia.cod_extracao
            inner join dia_hora_extracao dia_hora
                on dia_hora.cod_dia_extracao = dia.cod_dia_extracao
            inner join hora_extracao_automatizada hora
                on dia_hora.cod_hora_extracao = hora.cod_hora_extracao
        where extracaoauto.cod_extracao = '$cod_extracao'
            and dia_hora.status = 'A'";

    $result = mysqli_query($con, $query);


    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_extracao'] = $row['cod_extracao'];
        $row_array['nome_banca'] = $row['nome_banca'];
        $row_array['cod_hora_extracao'] = $row['cod_hora_extracao'];
        $row_array['hora'] = $row['hora'];
        $row_array['cod_dia_extracao'] = $row['cod_dia_extracao'];
        $row_array['dia'] = $row['dia'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

} else if ($operacao == 'listar_horarios_banca') { 
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);

    $query =
        "select distinct extracaoauto.cod_extracao, 
                extracaoauto.nome_banca, 
                hora.cod_hora_extracao, 
                hora.hora
        from extracao_automatizada extracaoauto
            inner join dia_extracao_automatizada dia 
                on extracaoauto.cod_extracao = dia.cod_extracao
            inner join dia_hora_extracao dia_hora
                on dia_hora.cod_dia_extracao = dia.cod_dia_extracao
            inner join hora_extracao_automatizada hora
                on dia_hora.cod_hora_extracao = hora.cod_hora_extracao
        where extracaoauto.cod_extracao = '$cod_extracao'
            order by hora.hora";

    $result = mysqli_query($con, $query);


    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_extracao'] = $row['cod_extracao'];
        $row_array['nome_banca'] = $row['nome_banca'];
        $row_array['cod_hora_extracao'] = $row['cod_hora_extracao'];
        $row_array['hora'] = $row['hora'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

} else if ($operacao == 'listar_dias') { 
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);
    $cod_hora_extracao = mysqli_real_escape_string($con, $_POST['cod_hora_extracao']);

    $query =
        "select dia.cod_dia_extracao,
                dia.cod_extracao,
                dia.dia
            from extracao_automatizada extracaoauto
                inner join dia_extracao_automatizada dia 
                    on extracaoauto.cod_extracao = dia.cod_extracao
                inner join dia_hora_extracao dia_hora
                    on dia_hora.cod_dia_extracao = dia.cod_dia_extracao
                inner join hora_extracao_automatizada hora
                    on dia_hora.cod_hora_extracao = hora.cod_hora_extracao
            where extracaoauto.cod_extracao = '$cod_extracao'
                and dia_hora.cod_hora_extracao = '$cod_hora_extracao'
                and dia_hora.status = 'A'";

    $result = mysqli_query($con, $query);


    $contador = 0;

    $row_array['cod_dia_extracao'] = $cod_hora_extracao;
    $row_array['cod_extracao'] = $cod_extracao;
    $domingo = false;
    $temDomingo = false;
    $segunda = false;
    $terca = false;
    $quarta = false;
    $quinta = false;
    $sexta = false;
    $sabado = false;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        switch ($row['dia']) {
            case "DOM":
                $domingo = true;
                $temDomingo = true;
                break;
            case "SEG":
                $segunda = true;
                break;    
            case "TER":
                $terca = true;
                break; 
            case "QUA":
                $quarta = true;
                break; 
            case "QUI":
                $quinta = true;
                break; 
            case "SEX":
                $sexta = true;
                break; 
            case "SAB":
                $sabado = true;
                break;                                                                                                                                     
            default:
                break;
        }
        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }
    $row_array['domingo'] = $domingo;
    $row_array['temDomingo'] = $temDomingo;
    $row_array['segunda'] = $segunda;
    $row_array['terca'] = $terca;
    $row_array['quarta'] = $quarta;
    $row_array['quinta'] = $quinta;
    $row_array['sexta'] = $sexta;
    $row_array['sabado'] = $sabado;
    array_push($return_arr, $row_array);

 } else if ($operacao == 'salvar') {
    
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $extracao = json_decode($_POST['extracao']);
    $cod_site = $extracao->site;
    $descricao = $extracao->descricao;
    $horario = $extracao->horario;
    $domingo = $extracao->domingo == true ? 'S' : 'N';
    $segunda = $extracao->segunda == true ? 'S' : 'N';
    $terca = $extracao->terca == true ? 'S' : 'N';
    $quarta = $extracao->quarta == true ? 'S' : 'N';
    $quinta = $extracao->quinta == true ? 'S' : 'N';
    $sexta = $extracao->sexta == true ? 'S' : 'N';
    $sabado = $extracao->sabado == true ? 'S' : 'N';
    $habilita_centena = $extracao->habilita_centena == true ? 'S' : 'N';
    $quantidadePremios = $extracao->quantidade_premios;
    $automatizada = "S";
    $tipoJogo = $extracao->tipoJogo;
    if ($tipoJogo == "2") {
        $quantidadePremios = 1;
    }
    $federal = "N";
    $milhar = $extracao->milhar;
    $centena = $extracao->centena;
    $grupo = $extracao->grupo;
    $duque_grupo = $extracao->duque_grupo;
    $terno_grupo = $extracao->terno_grupo;
    $quina_grupo = $extracao->quina_grupo;
    $dezena = $extracao->dezena;
    $duque_dezena = $extracao->duque_dezena;
    $terno_dezena = $extracao->terno_dezena;
    $prazo_bloqueio = $extracao->prazo_bloqueio;    

    try {
        $con->begin_transaction();
            $buscarExtracao = "select cod_extracao, 
            cod_site, 
            hora_extracao, 
            descricao, 
            status,
            segunda, 
            terca, 
            quarta, 
            quinta, 
            sexta, 
            federal,
            sabado, 
            domingo, 
            automatizada,
            qtd_premios 
            from extracao_bicho
            where cod_site = '$cod_site'
                and automatizada = 'S'
                and descricao = '$descricao'
                and tipo_jogo = '$tipoJogo'
                and hora_extracao = '$horario'";
        $resultadoBusca = mysqli_query($con, $buscarExtracao);
        $row = mysqli_fetch_array($resultadoBusca, MYSQLI_ASSOC);
        
        if ($row) {
            $queryOldExtracao = "SELECT eb.*, clb.MILHAR, clb.CENTENA, clb.GRUPO, clb.DUQUE_GRUPO,
                        clb.TERNO_GRUPO, clb.QUINA_GRUPO, clb.DEZENA, clb.DUQUE_DEZENA, clb.TERNO_DEZENA
                        FROM extracao_bicho eb
                        LEFT JOIN configuracao_limite_bicho clb ON (eb.cod_extracao = clb.cod_extracao)
                        WHERE eb.cod_extracao = ?";
            $stmtOldExtracao = $con->prepare($queryOldExtracao);
            $stmtOldExtracao->bind_param("i", $row['cod_extracao']);
            $stmtOldExtracao->execute();
            $result = $stmtOldExtracao->get_result();
            $oldExtracao = $result->fetch_assoc();
            $stmtOldExtracao->close();
            $stmt = $con->prepare("UPDATE extracao_bicho
                                                set status = 'A',
                                                federal = ?,
                                                domingo = ?,
                                                segunda = ?,
                                                terca = ?,
                                                quarta = ?,
                                                quinta = ?,
                                                sexta = ?,
                                                sabado = ?,
                                                qtd_premios = ?,
                                                prazo_bloqueio = ?,
                                                habilita_centena = ?
                                                WHERE cod_extracao = ?
                                                AND cod_site = ?
                                                AND tipo_jogo = ?");
            $stmt->bind_param(
                "ssssssssiisiis", 
                $federal, 
                $domingo,
                $segunda,
                $terca,
                $quarta, 
                $quinta,
                $sexta,
                $sabado, 
                $quantidadePremios, 
                $prazo_bloqueio,
                $habilita_centena,
                $row['cod_extracao'], 
                $cod_site,
                $tipoJogo
            );
            $stmt->execute();
            if ($tipoJogo == "B") {
                inserir_atualizar_limites($con, $row['cod_extracao'], $cod_site, $milhar, $centena, $grupo, $duque_grupo,
                $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena);
            }            
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_EXTRACAO_EDITADA,
                descreverEdicaoExtracao($oldExtracao, $row['cod_extracao'], $horario, $row['descricao'], 
                $domingo, $segunda, $terca, $quarta, $quinta, $sexta, $sabado, $tipoJogo, $quantidadePremios,
                $prazo_bloqueio, true, $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, 
                $quina_grupo, $dezena, $duque_dezena, $terno_dezena)
            );       
        } else {
            $stmt = $con->prepare("INSERT INTO extracao_bicho(cod_site, hora_extracao, descricao, 
                                        federal, segunda, terca, quarta, quinta, sexta, sabado, domingo, 
                                        tipo_jogo, automatizada, qtd_premios, prazo_bloqueio, habilita_centena)
            values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
            $stmt->bind_param(
                "issssssssssssiis", 
                $cod_site, 
                $horario, 
                $descricao, 
                $federal, 
                $segunda, 
                $terca, 
                $quarta, 
                $quinta, 
                $sexta, 
                $sabado, 
                $domingo, 
                $tipoJogo, 
                $automatizada,
                $quantidadePremios,
                $prazo_bloqueio,
                $habilita_centena
            );
            $stmt->execute();
            $cod_extracao = $stmt->insert_id;
            
            if ($tipoJogo == "B") {
                inserir_atualizar_limites($con, $cod_extracao, $cod_site, $milhar, $centena, $grupo, $duque_grupo,
                $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena);
            }

            $buscarCodExtracao = " SELECT cod_extracao FROM extracao_bicho
            WHERE COD_SITE = '$cod_site'
            and hora_extracao = '$horario'
            and descricao = '$descricao'
            and tipo_jogo = '$tipoJogo'
            and automatizada = 'S' ";
            $result = mysqli_query($con, $buscarCodExtracao);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $cod_extracao = $row['cod_extracao'];
            // array_push($return_arr, $buscarCodExtracao);
            $inserirAreaExtracao = " INSERT INTO area_extracao (cod_site, cod_area, cod_extracao, status)
                        SELECT a.cod_site, a.cod_area, '$cod_extracao', 'A'
                        FROM areas a  
                        WHERE a.cod_site = '$cod_site' ";
            $con->query($inserirAreaExtracao);                
            inserir_auditoria(
                $con,
                $cod_usuario,
                $cod_site,
                AUD_EXTRACAO_CRIADA,
                descreverExtracao($cod_extracao, $horario, $descricao, $domingo, $segunda, 
                    $terca, $quarta, $quinta, $sexta, $sabado, $tipoJogo, $quantidadePremios,
                    $prazo_bloqueio, $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, 
                    $dezena, $duque_dezena, $terno_dezena)
            );
        }
        $response['status'] = "OK";
        $con->commit();
    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    } finally {
        array_push($return_arr, $response);
    }

 } else if ($operacao == 'excluir') {

    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $site = mysqli_real_escape_string($con, $_POST['site']);
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);
    $stmt = $con->prepare("UPDATE extracao_bicho
                            set status = 'I'
                            WHERE cod_extracao = ?");
    $stmt->bind_param("i", $cod_extracao);
    $stmt->execute();
    inserir_auditoria(
        $con,
        $cod_usuario,
        $site,
        AUD_EXTRACAO_EXCLUIDA,
        descreverExclusaoExtracao($cod_extracao)
    );

 } else if($operacao == 'listar_extracoes_usuario') {
    $queryDomingosCadastrados = " SELECT extracaoauto.nome_banca, CONCAT(hora.hora, ':00') AS horario
                                    FROM extracao_automatizada extracaoauto
                                        INNER JOIN dia_extracao_automatizada dia 
                                            ON extracaoauto.cod_extracao = dia.cod_extracao
                                        INNER JOIN dia_hora_extracao dia_hora
                                            ON dia_hora.cod_dia_extracao = dia.cod_dia_extracao
                                        INNER JOIN hora_extracao_automatizada hora
                                            ON dia_hora.cod_hora_extracao = hora.cod_hora_extracao
                                    WHERE dia_hora.status = 'A'
                                        AND dia.dia = 'DOM' ";
    $result = mysqli_query($con, $queryDomingosCadastrados);  
    $contador = 0;
    $domingos_arr = array();
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['nome_banca'] = $row['nome_banca'];
        $row_array['horario'] = $row['horario'];
        array_push($domingos_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }        
    }                                         
    
    $cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
    $desc_extracao = mysqli_real_escape_string($con, $_POST['desc_extracao']);
    $query = "SELECT eb.cod_extracao, eb.cod_site, hora_extracao, status, descricao, 
                    segunda, terca, quarta, quinta, sexta, sabado, domingo, 
                    automatizada, qtd_premios, tipo_jogo, prazo_bloqueio, habilita_centena,
                    clb.MILHAR, clb.CENTENA, clb.GRUPO, clb.DUQUE_GRUPO, clb.TERNO_GRUPO, clb.QUINA_GRUPO,
                    clb.DEZENA, clb.DUQUE_DEZENA, clb.TERNO_DEZENA
                FROM extracao_bicho eb
                LEFT JOIN configuracao_limite_bicho clb on (eb.COD_EXTRACAO = clb.COD_EXTRACAO)
                WHERE eb.cod_site = '$cod_site'
                    AND automatizada = 'S'
                    AND status = 'A'";
    if ($desc_extracao != '') {
        $query = $query . " AND descricao = '$desc_extracao'";
    }                                        
    $query = $query . " GROUP BY descricao, hora_extracao, tipo_jogo";
    
    $result = mysqli_query($con, $query);
    $contador = 0;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_extracao'] = $row['cod_extracao'];
        $row_array['cod_site'] = $row['cod_site'];
        $row_array['hora_extracao'] = $row['hora_extracao'];
        $row_array['status'] = $row['status'];
        $row_array['descricao'] = $row['descricao'];
        $row_array['segunda'] = $row['segunda'];
        $row_array['terca'] = $row['terca'];
        $row_array['quarta'] = $row['quarta'];
        $row_array['quinta'] = $row['quinta'];
        $row_array['sexta'] = $row['sexta'];
        $row_array['sabado'] = $row['sabado'];
        $row_array['domingo'] = $row['domingo'];
        $row_array['automatizada'] = $row['automatizada'];
        $row_array['qtd_premios'] = $row['qtd_premios'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        $row_array['prazo_bloqueio'] = $row['prazo_bloqueio'];
        $row_array['habilita_centena'] = $row['habilita_centena'];
        $row_array['milhar'] = $row['MILHAR'];
        $row_array['centena'] = $row['CENTENA'];
        $row_array['grupo'] = $row['GRUPO'];
        $row_array['duque_grupo'] = $row['DUQUE_GRUPO'];
        $row_array['terno_grupo'] = $row['TERNO_GRUPO'];
        $row_array['quina_grupo'] = $row['QUINA_GRUPO'];
        $row_array['dezena'] = $row['DEZENA'];
        $row_array['duque_dezena'] = $row['DUQUE_DEZENA'];
        $row_array['terno_dezena'] = $row['TERNO_DEZENA'];

        $tem_domingo = 'N';
        foreach ($domingos_arr as $registro) {
            if ($registro['nome_banca'] == $row_array['descricao'] 
                && $registro['horario'] == $row_array['hora_extracao']) {
                    $tem_domingo = 'S';
                    break;
            }
        }
        $row_array['tem_domingo'] = $tem_domingo;

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };
   
 }

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);
$con->close();

function inserir_atualizar_limites($con, $codigo_extracao, $cod_site, $milhar, $centena, $grupo, $duque_grupo,
    $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena) {
    $queryLimiteBicho = " SELECT codigo FROM configuracao_limite_bicho
    WHERE COD_EXTRACAO = '$codigo_extracao' AND COD_SITE = '$cod_site' ";
    $result = mysqli_query($con, $queryLimiteBicho);
    if (mysqli_num_rows($result)) {
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $codigo_configuracao = $row['codigo'];
        $stmtLimiteBicho = $con->prepare("UPDATE configuracao_limite_bicho
                SET MILHAR = ?, CENTENA = ?, GRUPO = ?, DUQUE_GRUPO = ?,
                    TERNO_GRUPO = ?, QUINA_GRUPO = ?,  DEZENA = ?, DUQUE_DEZENA = ?, TERNO_DEZENA = ?
                WHERE CODIGO = ? ");
        $stmtLimiteBicho->bind_param(
            "dddddddddi", $milhar, $centena, $grupo, $duque_grupo,
                     $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena,
                     $codigo_configuracao
        );
    } else {
        $stmtLimiteBicho = $con->prepare("INSERT INTO configuracao_limite_bicho(COD_SITE, COD_EXTRACAO, MILHAR, CENTENA,
                                GRUPO, DUQUE_GRUPO, TERNO_GRUPO, QUINA_GRUPO, DEZENA, DUQUE_DEZENA, TERNO_DEZENA)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
        $stmtLimiteBicho->bind_param("iiddddddddd", $cod_site, $codigo_extracao, 
         $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo,
         $dezena, $duque_dezena, $terno_dezena);  
    }
    $stmtLimiteBicho->execute();
    $stmtLimiteBicho->close();
}
