<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

try {

    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $nome_usuario = mysqli_real_escape_string($con, $_POST['nome_usuario']);
    $cod_usuario_lancamento = mysqli_real_escape_string($con, $_POST['cod_usuario_lancamento']);
    $nome_usuario_lancamento = mysqli_real_escape_string($con, $_POST['nome_usuario_lancamento']);
    $valor = mysqli_real_escape_string($con, $_POST['valor']);
    $codigoSite = mysqli_real_escape_string($con, $_POST['cod_site']);
    $tipo = mysqli_real_escape_string($con, $_POST['tipo']);
    $motivo = mysqli_real_escape_string($con, $_POST['motivo']);
    $colaboradorEnviar = mysqli_real_escape_string($con, $_POST['colaboradorEnviar']);
    $nomeColaboradorEnviar = mysqli_real_escape_string($con, $_POST['nomeColaboradorEnviar']);
    $ajudaCusto = mysqli_real_escape_string($con, $_POST['ajudaCusto']);

    if ($tipo == "undefined" || $tipo == "") {
        $tipo = 'S';
    }

    if ($ajudaCusto == 'true') {
        $ajudaCusto = 'S';
    } else {
        $ajudaCusto = 'N';
    }
    $query = 'SET @@session.time_zone = "-03:00"';
    $result = mysqli_query($con, $query);

    $stmt = $con->prepare("INSERT INTO lancamento (cod_site,cod_usuario_lancamento,cod_usuario,motivo,tipo_lancamento,status,valor,data_hora,ajuda_custo) VALUES (?,?,?,?,?,'A',?,now(),?)");
    $stmt->bind_param("iiissds", $codigoSite, $cod_usuario_lancamento, $cod_usuario, $motivo, $tipo, $valor, $ajudaCusto);
    $stmt->execute();
    $cod_lancamento = $con->insert_id;    

    inserir_auditoria(
        $con,
        $cod_usuario,
        $codigoSite,
        AUD_LANCAMENTO_CRIADO,
        descreverLancamento(
            date("Y-m-d"),
            $nome_usuario,
            $tipo,
            $nome_usuario_lancamento,
            $motivo,
            $ajudaCusto,
            $valor
        )
    );    

    if ($colaboradorEnviar != "undefined" && $colaboradorEnviar != "") {
        $tipo = 'E';
        $stmt2 = $con->prepare("INSERT INTO lancamento (cod_site,cod_usuario_lancamento,cod_usuario,motivo,tipo_lancamento,status,valor,data_hora,cod_pai) VALUES (?,?,?,?,?,'A',?,now(),?)");
        $stmt2->bind_param("iiissdi", $codigoSite, $colaboradorEnviar, $cod_usuario, $motivo, $tipo, $valor, $cod_lancamento);
        $stmt2->execute();
        inserir_auditoria(
            $con,
            $cod_usuario,
            $codigoSite,
            AUD_LANCAMENTO_CRIADO,
            descreverLancamento(
                date("Y-m-d"),
                $nome_usuario,
                $tipo,
                $nomeColaboradorEnviar,
                $motivo,
                $ajudaCusto,
                $valor
            )
        );
    }

    $response['status'] = "OK";
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
}

$con->close();
echo json_encode($response);
