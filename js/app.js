var app = angular.module('main', ['ngRoute', 'ja.qr', 'angularUtils.directives.dirPagination','ngMaterial']);

app.factory("configGeral", ['$http', '$q', 'user',
	function ($http, $q, user) {
		var config;
		var idUser;
		return {
			get: function () {
				var deferred = $q.defer();
				if (angular.isDefined(config) && (idUser === user.getCodigo())) {
					deferred.resolve(config);
				} else {
					$http({
						method: "POST",
						url: "angularjs-mysql/configuracao.php",
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						data: 'site=' + user.getSite() + 
							  '&cod_area=' + user.getArea() + 
							  '&operacao=geral2pra500' + 
							  '&schema=' + user.getSchema()
					}).then(function (response) {
						idUser = user.getCodigo();
						config = response.data[0];
						deferred.resolve(response.data[0]);
					}).catch(function (response) {
						deferred.resolve(response);
						alert('Erro no $HTTP: ' + response.status)
					});
				}
				return deferred.promise;
			},

			registerUpdateCallback: function () {
				config = undefined;
				this.get();
			}
		};
	}]);

app.factory("notificacoes", ['$http', '$q', 'user',
	function ($http, $q, user) {
		var notificacoes;
		var idUser;
		return {
			get: function () {
				var deferred = $q.defer();
				if (angular.isDefined(notificacoes) && (idUser === user.getCodigo())) {
					deferred.resolve(notificacoes);
				} else {
					$http({
						method: "POST",
						url: "angularjs-mysql/notificacoes.php",
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						data: 'site=' + user.getSite() + 
							  '&schema=' + user.getSchema()
					}).then(function (response) {
						idUser = user.getCodigo();
						notificacoes = response.data[0];
						deferred.resolve(response.data[0]);
					}).catch(function (response) {
						deferred.resolve(response);
						alert('Erro no $HTTP: ' + response.status)
					});
				}
				return deferred.promise;
			},

			registerUpdateCallback: function () {
				notificacoes = undefined;
				this.get();
			}
		};
	}]);

app.factory('preventTemplateCache', function () {
	return {
		'request': function (config) {
			if (config.url.indexOf('components') !== -1 ||
				config.url.indexOf('controllers') !== -1) {
				config.url = config.url + '?' + new Date().getTime();
			}
			return config;
		}
	}
}).config(function ($httpProvider) {
	$httpProvider.interceptors.push('preventTemplateCache');
});

app.config([
	'$compileProvider',
	function ($compileProvider) {
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|http|com.mitsoftwar.appprinter):/);
		// Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
	}
]);

app.config(function ($routeProvider, $locationProvider) {

	var verificaUsuarioLogado = function ($location, $q, user) {
		var deferred = $q.defer();
		if (user.isUserLoggedIn()) {
			deferred.resolve();
		} else {
			deferred.reject();
			$location.url('/login');
		}
		return deferred.promise;
	}

	var verificaAdmin = function ($location, $q, user) {
		var deferred = $q.defer();
		if (user.getPerfil() == "A") {
			deferred.resolve();
		} else {
			deferred.reject();
			$location.url('/dashboard');
		}
		return deferred.promise;
	}

	$routeProvider.when('/', {
		resolve: {
			check: function ($location, user) {
				if (user.isUserLoggedIn()) {
					$location.path('/caixa');
				} else if (sessionStorage.getItem('siteAux') != null) {
					$location.path('/' + sessionStorage.getItem('siteAux'));
				}
			},
		},
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	}).when('/logout', {
		resolve: {
			deadResolve: function ($location, user) {
				var aux = user.getEscolhido();
				var siteAux = user.getSite();
				user.clearData();
				if (aux == 'true') {
					$location.path('/' + siteAux);
					sessionStorage.setItem('siteAux', siteAux);
				} else {
					$location.path('/');
				}
			}
		}
	}).when('/login', {
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	}).when('/usuario', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() == "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/usuario.html'
	}).when('/lancamento', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() == "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/lancamento.html'
	}).when('/configuracao', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/configuracao.html'
	}).when('/gerenciar_areas', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/gerenciar_areas.html'
		}).when('/relatorioprestacaoconta', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/relatorio_prestacao_conta.html',
		controller: 'relatorioPrestacaoContaCtrl'
	}).when('/relatoriobilheteconcurso', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/relatorio_bilhete_concurso.html'
	}).when('/limite_bicho', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/limite_bicho.html'
	}).when('/dashboard', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/dashboard.html',
		controller: 'dashboardCtrl'
	}).when('/gerenciar_jogos', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/gerenciar_jogos.html'
	}).when('/multi_jogos', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/multiJogos.html'
	}).when('/seninha', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/seninha.html'
	}).when('/supersena', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/super_sena.html'		
	}).when('/quininha', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/quininha.html'
	}).when('/lotinha', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/lotinha.html'
	}).when('/bicho', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/bicho.html'
	}).when('/2pra500', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/2pra500.html'
	}).when('/rifa', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/rifa.html'
	}).when('/ver_bilhetes', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/ver_bilhetes.html'
	}).when('/premiacoes', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/premiacoes.html',
	}).when('/clientes', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "C") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/clientes.html'
	}).when('/auditoria', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/auditoria.html'
	}).when('/extracoes', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/extracoes.html'
	}).when('/extracoes_automatizadas', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/extracoes_automatizadas.html',
		controller: 'extracaoAutomatizadaCtrl'
	}).when('/areas_extracoes', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/dashboard');
				}
			},
		},
		templateUrl: './components/areas_extracoes.html'
	}).when('/conferir_bilhete', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/conferir_bilhete.html'
	}).when('/imprime_bilhete', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/conferir_bilhete.html',
		controller: 'conferirBilheteCtrl'
	}).when('/resultados', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/resultados.html'
	}).when('/regulamento', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/regulamento.html',
		controller: 'dashboardCtrl'
	}).when('/fale_conosco', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/fale_conosco.html',
		controller: 'dashboardCtrl'
	}).when('/downloads', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/downloads.html',
		controller: 'dashboardCtrl'
	}).when('/caixa', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/caixa.html',
		controller: 'caixaCtrl'
	}).when('/caixa_cambista', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/caixa');
				}
			},
		},
		templateUrl: './components/caixa_cambista.html',
		controller: 'caixaCtrl'
	}).when('/mapaAposta', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/caixa');
				}
			},
		},
		templateUrl: './components/mapa_aposta.html'
	}).when('/prestacaoConta', {
		resolve: {
			check: function ($location, user) {
				if (!user.isUserLoggedIn()) {
					$location.path('/login');
				} if (user.getPerfil() != "A") {
					$location.path('/caixa');
				}
			},
		},
		templateUrl: './components/prestacaoConta.html'
	}).when('/trocar_senha', {
		resolve: {
			loggedIn: verificaUsuarioLogado
		},
		templateUrl: './components/trocar_senha.html'
	}).when('/conferir_bilhete_externo', {
		templateUrl: './components/conferir_bilhete_externo.html',
		controller: 'conferirBilheteCtrl'
	}).when('/:id', {
		resolve: {
			check: function ($location, user) {
				if (user.isUserLoggedIn()) {
					$location.path('/caixa');
				}
			},
		},
		templateUrl: './components/login.html',
		controller: 'loginCtrl'
	})
		.otherwise({
			template: '404'
		});

	$locationProvider.html5Mode(false);
});

app.factory("bilhetesAPI", ['$http', 'user', function ($http, user) {

	var _getBilhetes = function (codigo, numero) {
		var cod_bilhete = codigo;
		var numero_bilhete = numero;
		return $http({
			method: "POST",
			url: "angularjs-mysql/bilhetes.php",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: 'cod_usuario=' + user.getCodigo() + 
				  '&site=' + user.getSite() + 
				  '&cod_bilhete=' + cod_bilhete + 
				  '&numero_bilhete=' + numero_bilhete + 
				  '&operacao=conferirBilhete' + 
                  '&schema=' + user.getSchema()
		}).then(function (response) {
			return response.data;
		}, function (response) {
			alert('Erro no $HTTP: ' + response.status)
		});
	};

	return {
		getBilhetes: _getBilhetes
	};

}]);



app.service('user', function () {
	var username;
	var nome;
	var loggedin = false;
	var id;
	var codigo;
	var site;
	var perfil;
	var saldo;
	var bilheteExterno = "";
	var escolhido;
	var puleRepeticao = "";
	var flg_seninha;
	var flg_quininha;
	var flg_lotinha;
	var flg_bicho;
	var flg_2pra500;
	var flg_rifa;
	var flg_multijogos;
	var flg_supersena;
	var area;
	var dominio;
	var repetirBilhete = false;
	var schema;

	this.getflg_seninha = function () {
		return flg_seninha;
	}

	this.setflg_seninha = function (auxflg_seninha) {
		this.flg_seninha = auxflg_seninha;
	}

	this.getflg_quininha = function () {
		return flg_quininha;
	}

	this.setflg_quininha = function (auxflg_quininha) {
		this.flg_quininha = auxflg_quininha;
	}

	this.getflg_lotinha = function () {
		return flg_lotinha;
	}

	this.setflg_lotinha = function (auxflg_lotinha) {
		this.flg_lotinha = auxflg_lotinha;
	}

	this.getflg_bicho = function () {
		return flg_bicho;
	}

	this.setflg_bicho = function (auxflg_bicho) {
		this.flg_bicho = auxflg_bicho;
	}

	this.getflg_2pra500 = function () {
		return flg_2pra500;
	}

	this.setflg_2pra500 = function (auxflg_2pra500) {
		this.flg_2pra500 = auxflg_2pra500;
	}

	this.getflg_rifa = function () {
		return flg_rifa;
	}

	this.setflg_rifa = function (auxflg_rifa) {
		this.flg_rifa = auxflg_rifa;
	}

	this.getflg_multijogos = function () {
		return flg_multijogos;
	}

	this.setflg_multijogos = function (auxflg_multijogos) {
		this.flg_multijogos = auxflg_multijogos;
	}

	this.getflg_supersena = function () {
		return flg_supersena;
	}

	this.setflg_supersena = function (auxflg_supersena) {
		this.flg_supersena = auxflg_supersena;
	}

	this.getPIN = function () {
		return PIN;
	}

	this.setPIN = function (auxPIN) {
		this.PIN = auxPIN;
	}

	this.getPuleRepeticao = function () {
		return puleRepeticao;
	}

	this.setPuleRepeticao = function (auxPuleRepeticao) {
		puleRepeticao = auxPuleRepeticao;
	}

	this.getBilheteExterno = function () {
		return bilheteExterno;
	}

	this.setBilheteExterno = function (auxBilheteExterno) {
		bilheteExterno = auxBilheteExterno;
	}

	this.getName = function () {
		return username;
	};

	this.getNome = function () {
		return nome;
	};

	this.setID = function (userID) {
		id = userID;
	};
	this.getID = function () {
		return id;
	};

	this.setSaldo = function (saldoAux) {
		this.saldo = saldoAux;
		saldo = this.saldo;
	};
	this.getSaldo = function () {
		return saldo;
	};

	this.getCodigo = function () {
		return codigo;
	}

	this.getSite = function () {
		return site;
	}

	this.getEscolhido = function () {
		return escolhido;
	}

	this.getPerfil = function () {
		return perfil;
	}

	this.getArea = function () {
		return area;
	}

	this.getDominio = function () {
		return dominio;
	}

	this.getRepetirBilhete = function () {
		return this.repetirBilhete;
	}

	this.setRepetirBilhete = function (auxRepetirBilhete) {
		this.repetirBilhete = auxRepetirBilhete;
	}

	this.getSchema = function() {
		return schema;
	}

	this.isUserLoggedIn = function () {
		if (!!sessionStorage.getItem('login')) {
			loggedin = true;
			var data = JSON.parse(sessionStorage.getItem('login'));
			username = data.username;
			id = data.id;
			nome = data.nome;
			codigo = data.codigo;
			site = data.site;
			escolhido = data.escolhido;
			perfil = data.perfil;
			saldo = data.saldo;
			flg_seninha = data.flg_seninha;
			flg_quininha = data.flg_quininha;
			flg_lotinha = data.flg_lotinha;
			flg_bicho = data.flg_bicho;
			flg_2pra500 = data.flg_2pra500;
			flg_rifa = data.flg_rifa;
			flg_multijogos = data.flg_multijogos;
			flg_supersena = data.flg_supersena;
			area = data.area;
			dominio = data.dominio;
			schema = data.schema;
		}
		return loggedin;
	};

	this.atualizaSaldo = function (pSaldo) {
		sessionStorage.setItem('saldoAtualizado', JSON.stringify({
			saldo: pSaldo
		}));
	}

	this.getSaldoAtualizado = function () {
		var data = JSON.parse(sessionStorage.getItem('saldoAtualizado'));
		return data.saldo;
	}


	this.saveData = function (data) {
		username = data.user;
		id = data.id;
		nome = data.nome;
		loggedin = true;
		codigo = data.codigo;
		site = data.site;
		escolhido = data.escolhido;
		perfil = data.perfil;
		saldo = data.saldo;
		flg_seninha = data.flg_seninha;
		flg_quininha = data.flg_quininha;
		flg_lotinha = data.flg_lotinha;
		flg_bicho = data.flg_bicho;
		flg_2pra500 = data.flg_2pra500;
		flg_rifa = data.flg_rifa;
		flg_multijogos = data.flg_multijogos;
		flg_supersena = data.flg_supersena;
		area = data.area;
		dominio = data.dominio;
		schema = data.schema;

		sessionStorage.setItem('saldoAtualizado', JSON.stringify({
			saldo: data.saldo
		}));

		sessionStorage.setItem('login', JSON.stringify({
			username: username,
			id: id,
			nome: nome,
			codigo: codigo,
			site: site,
			escolhido: escolhido,
			perfil: perfil,
			saldo: saldo,
			flg_seninha: flg_seninha,
			flg_quininha: flg_quininha,
			flg_lotinha: flg_lotinha,
			flg_bicho: flg_bicho,
			flg_2pra500: flg_2pra500,
			flg_rifa: flg_rifa,
			flg_multijogos: flg_multijogos,
			flg_supersena: flg_supersena,
			area: area,
			dominio: dominio,
			schema: schema
		}));
	};

	this.clearData = function () {
		sessionStorage.removeItem('login');
		sessionStorage.removeItem('saldoAtualizado');
		username = "";
		id = "";
		nome = "";
		loggedin = false;
		codigo = "";
		site = "";
		escolhido = "";
		perfil = "";
		saldo = 0;
		PIN = "";
		flg_seninha = "";
		flg_quininha = "";
		flg_lotinha = "";
		flg_bicho = "";
		flg_2pra500 = "";
		flg_rifa = "";
		flg_multijogos = "";
		flg_supersena = "";
		dominio = "";
		schema = "";
	}


});

app.service('funcoes', ['configGeral','user', function (configGeral, user) {

	this.getDescricaoTipoJogoBicho = function (tipo_jogo_bicho) {
		switch (tipo_jogo_bicho) {
			case 'MS':
				return "MS - MILHAR SECA";
			case 'MC':
				return "MC - MILHAR COM CENTENA";
			case 'MI':
				return "MI - MILHAR INVERTIDA";
			case 'MCI':
				return "MCI - MILHAR COM CENTENA INVERTIDA";
			case 'C':
				return "C - CENTENA";
			case 'CI':
				return "CI - CENTENA INVERTIDA";
			case 'G':
				return "G - GRUPO";
			case 'DG':
				return "DG - DUQUE DE GRUPO 1 / 5";
			case 'DG6':
				return "DG6 - DUQUE DE GRUPO 6 / 10";
			case 'DGC':
				return "DGC - DUQUE DE GRUPO COMBINADO";			
			case 'TG':
				return "TG - TERNO DE GRUPO 1 / 5";
			case 'TG10':
				return "TG10 - TERNO DE GRUPO 1 / 10";
			case 'TG6':
				return "TG6 - TERNO DE GRUPO 6 / 10";
			case 'TSE':
				return "TSE - TERNO DE SEQUENCIA";
			case 'TSO':
				return "TSO - TERNO DE SOMA";
			case 'TEX-A':
				return "TEX-A - TERNO ESPECIAL ABERTO";
			case 'TEX-F':
				return "TEX-F - TERNO ESPECIAL FECHADO";
			case 'QG':
				return "QG - QUINA DE GRUPO";
			case 'DZ':
				return "DZ - DEZENA";
			case 'DDZ':
				return "DDZ - DUQUE DE DEZENA 1 / 5";
			case 'DDZ6':
				return "DDZ6 - DUQUE DE DEZENA 6 / 10";
			case 'DDZC':
				return "DDZC - DUQUE DE DEZENA COMBINADO";
			case 'TDZ':
				return "TDZ - TERNO DE DEZENA 1 / 5";
			case 'TDZ6':
				return "TDZ6 - TERNO DE DEZENA 6 / 10";
			case 'TDZ10':
				return "TDZ10 - TERNO DE DEZENA 1 / 10";
			case 'TDZC':
				return "TDZC - TERNO DE DEZENA COMBINADO";
			case 'TGC':
				return "TGC - TERNO DE GRUPO COMBINADO";
			case 'PS':
				return "PS - PASSE SECO";
			case 'PS12':
				return "PS12 - PASSE SECO 1 / 2";
			case 'PC':
				return "PC - PASSE COMBINADO";
			case '5P100':
				return "5P100 - 5 PRA 100";
		}
	}

	this.getTiposJogos = function () {
		tipos = [];
		configGeral.get().then(function (data) {
			if (data.flg_seninha && !tipos.find(x => x.codigo === 'S')) {
				tipos.push({ nome: 'Seninha', codigo: 'S' });
			}
			if (data.flg_quininha && !tipos.find(x => x.codigo === 'Q')) {
				tipos.push({ nome: 'Quininha', codigo: 'Q' });
			}
			if (data.flg_lotinha && !tipos.find(x => x.codigo === 'L')) {
				tipos.push({ nome: 'Lotinha', codigo: 'L' });
			}
			if (data.flg_bicho && !tipos.find(x => x.codigo === 'B')) {
				tipos.push({ nome: 'Bicho', codigo: 'B' });
			}
			if (data.flg_2pra500 && !tipos.find(x => x.codigo === '2')) {
				descricao2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
				if (user.getSite() == 6638) {
					descricao2pra500 = 'Bilhetinho Premiado';
				}
				tipos.push({ nome: descricao2pra500, codigo: '2' });
			}
			if (data.flg_rifa && !tipos.find(x => x.codigo === 'R')) {
				tipos.push({ nome: 'Rifa', codigo: 'R' });
			}
			if (data.flg_supersena && !tipos.find(x => x.codigo === 'U')) {
				tipos.push({ nome: 'Super Sena', codigo: 'U' });
			}
		});
		return tipos;
	}

	this.getDescricaoTipoJogo = function (tipo_jogo, desc_2pra500) {
		tipo_jogo = tipo_jogo + '';
        switch (tipo_jogo) {
            case 'S':
                return "Seninha";
			case 'U':
				return "Super Sena";				
            case 'Q':
                return "Quininha";
            case 'L':
                return "Lotinha";
            case 'B':
                return "Bicho";
            case '2':
				if (user.getSite() == 6638) {
					return 'Bilhetinho Premiado';
				}
                return desc_2pra500;
            case 'R':
                return "Rifa";
            default:
                return "";
        }
	}

	this.getDescricaoConcurso = function (jogo) {
		switch (jogo.tipo_jogo) {
            case 'B':
            case 2:
                return jogo.hora_extracao + " - " + jogo.desc_hora;
            case 'S':
            case 'Q':
            case 'L':
			case 'U':
                return "Concurso: " + jogo.concurso + ' - ';
            case 'R':
                return jogo.descricao;
            default:
                return "";
        }
	}

	this.getDescricaoStatus = function (status) {
		if (status == 'B') {
            return "Bloqueado";
        } else if (status == 'L') {
            return "Liberado";
        } else if (status == 'P') {
            return "Processado";
        } else if (status == 'F') {
            return "Finalizado";
        }
	}

}]);

app.filter('object2Array', function () {
	return function (input) {
		return angular.fromJson(input);
	}
});

app.filter('strReplace', function () {
	return function (input, from, to) {
		input = input || '';
		from = from || '';
		to = to || '';
		return input.replace(new RegExp(from, 'g'), to);
	};
});

app.filter('renderHTMLCorrectly', function ($sce) {
	return function (stringToParse) {
		return $sce.trustAsHtml(stringToParse);
	}
});

app.filter('range', function () {
	return function (input, min, max) {
		min = parseInt(min);
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});

app.filter('min', function () {
	return function (input, compareNumber) {
		return Math.min(input, compareNumber);
	};
});

app.filter('fromMap', function() {
	return function(input) {
	  if (!input) {
		  return;
	  }
	  var out = {};
	  input.forEach((v, k) => out[k] = v);
	  return out;
	};
});

app.directive('loading', ['$http', '$rootScope', function ($http, $rootScope) {
	$rootScope.loading = false;
	return {
		restrict: 'A',
		link: function (scope, element) {
			scope.isLoading = function () {
				return $rootScope.loading;
			};
			scope.$watch(scope.isLoading, function (value) {
				if (value && element.css('display') == 'none') {
					element.css('display', 'flex');
				} else {
					element.css('display', 'none');
				}
			});
		}
	};
}]);