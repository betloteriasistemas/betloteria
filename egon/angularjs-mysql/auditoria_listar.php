<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
	die();
}

$site = mysqli_real_escape_string($con, $_POST['site']);
$index = mysqli_real_escape_string($con, $_POST['index']);
$qtdElementos = mysqli_real_escape_string($con, $_POST['qtdElementos']);
$sort = mysqli_real_escape_string($con, $_POST['sort']);
$sortType = filter_var(mysqli_real_escape_string($con, $_POST['sortType']), FILTER_VALIDATE_BOOLEAN);

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$usuario = mysqli_real_escape_string($con, $_POST['usuario']);
$dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
$dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);

$response = [];

$query = "
select DATE_FORMAT(a.`DATA_AUDITORIA`, '%d/%m/%Y %T') DATA_AUDITORIA, a.`TXT_DESCRICAO` as `TXT_AUDITORIA`,
       u.`NOME`, u.`PERFIL`,
	   ao.`TXT_DESCRICAO` as `TXT_OPERACAO`,
	   a.COD_AUDITORIA,
	   INET6_NTOA(a.ENDERECO_IP) as ENDERECO_IP
from auditoria a 
inner join usuario u on (a.`COD_USUARIO` = u.`COD_USUARIO` and a.`COD_SITE` = u.`COD_SITE` )
inner join auditoria_operacao ao on (a.`COD_OPERACAO` = ao.`COD_OPERACAO` ) 
WHERE a.`COD_SITE` = '$site'";

if ($usuario) {
	$query = $query . " AND u.COD_USUARIO = '$usuario' ";
}

if ($operacao) {
	$query = $query . " AND ao.COD_OPERACAO = '$operacao' ";
}

if ($dataDe != '0') {
	$query = $query . " AND DATE_FORMAT(a.`DATA_AUDITORIA`, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d')  ";
}

if ($dataAte != '0') {
	$query = $query . " AND DATE_FORMAT(a.`DATA_AUDITORIA`, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d')  ";
}

if ($dataDe == 0 && $dataAte == 0) {
	$query = $query . " AND DATE_FORMAT(a.`DATA_AUDITORIA`, '%Y%m%d')  >= DATE_FORMAT(current_date - 7, '%Y%m%d')
			   AND DATE_FORMAT(a.`DATA_AUDITORIA`, '%Y%m%d')  <= DATE_FORMAT(current_date + 1, '%Y%m%d') ";
}
$queryCount = "select count(*) as total from (" . $query . ") t";
$totalItens = mysqli_query($con, $queryCount);

$asc_desc = $sortType ? " ASC" : " DESC";

switch ($sort) {
	case "USUARIO":
		$query = $query . "ORDER BY u.`NOME`";
		break;
	case "OPERACAO":
		$query = $query . "ORDER BY ao.`TXT_DESCRICAO`";
		break;
	default:
		$query = $query . "ORDER BY a.`DATA_AUDITORIA`";
		break;
}

$query = $query . $asc_desc . " LIMIT $index,$qtdElementos ";
$result = mysqli_query($con, $query);
$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
	$contador = $contador + 1;
	$row_array['data_auditoria'] = $row['DATA_AUDITORIA'];
	$row_array['auditoria'] = $row['TXT_AUDITORIA'];
	$row_array['nome_usuario'] = $row['NOME'];
	$row_array['perfil_usuario'] = $row['PERFIL'];
	$row_array['operacao'] = $row['TXT_OPERACAO'];
	$row_array['cod_auditoria'] = $row['COD_AUDITORIA'];
	$row_array['ip'] = $row['ENDERECO_IP'];

	array_push($return_arr, $row_array);
	if ($contador == mysqli_num_rows($result)) {
		break;
	}
};

$con->close();
$totalItensJSON{
	'totalItens'} = mysqli_fetch_array($totalItens)[0];
array_push($return_arr, $totalItensJSON);
echo json_encode($return_arr, JSON_NUMERIC_CHECK);
