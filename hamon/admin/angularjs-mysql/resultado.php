<?php
include "conexao.php"; 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

try {
    switch ($operacao) {
        case 'listar':
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $query = "SELECT codigo, tipo_jogo, data_jogo, concurso, hora_extracao, descricao_extracao,
                             CASE WHEN tipo_jogo IN ('B','2') THEN LPAD(numero_1,4,'0') else numero_1 end numero_1,
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_2,4,'0') else numero_2 end numero_2,
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_3,4,'0') else numero_3 end numero_3,
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_4,4,'0') else numero_4 end numero_4,
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_5,4,'0') else numero_5 end numero_5,
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_6,4,'0') else numero_6 end numero_6,
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_7,4,'0') else numero_7 end numero_7, 
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_8,4,'0') else numero_8 end numero_8, 
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_9,4,'0') else numero_9 end numero_9, 
                             CASE WHEN tipo_jogo = 'B' THEN LPAD(numero_10,4,'0') else numero_10 end numero_10,
                             numero_11,
                             numero_12,
                             numero_13,
                             numero_14,
                             numero_15,
                             CASE WHEN status = 'A' THEN 'ATIVO' else 'PROCESSADO' end status_resultado
                      FROM resultados_avulsos
                      WHERE data_jogo = '$data'";
            $result = mysqli_query($con, $query);
            $return_arr = array();
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $row_array['codigo'] = $row['codigo'];
                $row_array['tipo_jogo'] = $row['tipo_jogo'];
                $row_array['data_jogo'] = $row['data_jogo'];
                $row_array['concurso'] = $row['concurso'];
                $row_array['hora_extracao'] = $row['hora_extracao'];
                $row_array['descricao_extracao'] = $row['descricao_extracao'];
                $row_array['numero_1'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_1']), '');
                $row_array['numero_2'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_2']), '');
                $row_array['numero_3'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_3']), '');
                $row_array['numero_4'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_4']), '');
                $row_array['numero_5'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_5']), '');
                $row_array['numero_6'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_6']), '');
                $row_array['numero_7'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_7']), '');
                $row_array['numero_8'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_8']), '');
                $row_array['numero_9'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_9']), '');
                $row_array['numero_10'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_10']), '');
                $row_array['numero_11'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_11']), '');
                $row_array['numero_12'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_12']), '');
                $row_array['numero_13'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_13']), '');
                $row_array['numero_14'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_14']), '');
                $row_array['numero_15'] = ifStringNullReturn(mysqli_real_escape_string($con, $row['numero_15']), '');
                $row_array['status_resultado'] = $row['status_resultado'];
    
                array_push($return_arr, $row_array);
            }
            echo json_encode($return_arr);
        break;
    
        case 'inserir':
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $tipo = mysqli_real_escape_string($con, $_POST['tipo']);
            $concurso = mysqli_real_escape_string($con, $_POST['concurso']);
            $horaExtracao = mysqli_real_escape_string($con, $_POST['horaExtracao']);
            $descricaoExtracao = mysqli_real_escape_string($con, $_POST['descricaoExtracao']);
            $numero1 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero1']), null);
            $numero2 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero2']), null);
            $numero3 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero3']), null);
            $numero4 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero4']), null);
            $numero5 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero5']), null);
            $numero6 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero6']), null);
            $numero7 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero7']), null);
            $numero8 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero8']), null);
            $numero9 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero9']), null);
            $numero10 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero10']), null);
            $numero11 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero11']), null);
            $numero12 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero12']), null);
            $numero13 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero13']), null);
            $numero14 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero14']), null);
            $numero15 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero15']), null);
            $stmt = $con->prepare("INSERT INTO resultados_avulsos
                      (data_jogo, tipo_jogo, concurso, hora_extracao, descricao_extracao, 
                      numero_1, numero_2, numero_3, numero_4, numero_5, numero_6, numero_7, 
                      numero_8, numero_9, numero_10, numero_11, numero_12, numero_13, numero_14, numero_15)
                      VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssissiiiiiiiiiiiiiii", 
            $data, $tipo, $concurso, $horaExtracao, $descricaoExtracao, 
            $numero1, $numero2, $numero3, $numero4, $numero5, 
            $numero6, $numero7, $numero8, $numero9, $numero10, 
            $numero11, $numero12, $numero13, $numero14, $numero15);
            $stmt->execute();
            $stmt->close();
            $response['status'] = "OK";
            echo json_encode($response);
        break;
    
        case 'atualizar':
            $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $tipo = mysqli_real_escape_string($con, $_POST['tipo']);
            $concurso = mysqli_real_escape_string($con, $_POST['concurso']);
            $horaExtracao = mysqli_real_escape_string($con, $_POST['horaExtracao']);
            $descricaoExtracao = mysqli_real_escape_string($con, $_POST['descricaoExtracao']);
            $numero1 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero1']), null);
            $numero2 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero2']), null);
            $numero3 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero3']), null);
            $numero4 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero4']), null);
            $numero5 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero5']), null);
            $numero6 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero6']), null);
            $numero7 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero7']), null);
            $numero8 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero8']), null);
            $numero9 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero9']), null);
            $numero10 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero10']), null);
            $numero11 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero11']), null);
            $numero12 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero12']), null);
            $numero13 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero13']), null);
            $numero14 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero14']), null);
            $numero15 = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['numero15']), null);
            $stmt = $con->prepare("UPDATE resultados_avulsos
                                   SET data_jogo=?, tipo_jogo=?, concurso=?, hora_extracao=?, descricao_extracao=?, 
                                   numero_1=?, numero_2=?, numero_3=?, numero_4=?, numero_5=?, numero_6=?, numero_7=?,
                                   numero_8=?, numero_9=?, numero_10=?, numero_11=?, numero_12=?, numero_13=?, 
                                   numero_14=?, numero_15=?
                                   WHERE codigo=?");
            $stmt->bind_param("ssissiiiiiiiiiiiiiiii", 
            $data, $tipo, $concurso, $horaExtracao, $descricaoExtracao, 
            $numero1, $numero2, $numero3, $numero4, $numero5, 
            $numero6, $numero7, $numero8, $numero9, $numero10, 
            $numero11, $numero12, $numero13, $numero14, $numero15,
            $codigo);
            $stmt->execute();
            $stmt->close();
            $response['status'] = "OK";
            echo json_encode($response);
        break;
    
        case 'excluir':
            $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
            $stmt = $con->prepare("DELETE FROM resultados_avulsos
                                   WHERE codigo=?");
            $stmt->bind_param("i", $codigo);
            $stmt->execute();
            $stmt->close();
            $response['status'] = "OK";
            echo json_encode($response);
        break;

        case 'listar_bancas':
            $query = "SELECT EA.COD_EXTRACAO,
                     EA.NOME_BANCA                   
                FROM EXTRACAO_AUTOMATIZADA EA
                    INNER JOIN DIA_EXTRACAO_AUTOMATIZADA DEA ON EA.COD_EXTRACAO = DEA.COD_EXTRACAO
                    INNER JOIN DIA_HORA_EXTRACAO DHE ON DEA.COD_DIA_EXTRACAO = DHE.COD_DIA_EXTRACAO
                    INNER JOIN HORA_EXTRACAO_AUTOMATIZADA HEA ON DHE.COD_HORA_EXTRACAO = HEA.COD_HORA_EXTRACAO
                WHERE DHE.STATUS = 'A'
                GROUP BY EA.COD_EXTRACAO
                ORDER BY EA.NOME_BANCA";

            $result = mysqli_query($con, $query);

            $contador = 0;
            $return_arr = array();

            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $contador = $contador + 1;
                $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
                $row_array['nome_banca'] = $row['NOME_BANCA'];
                array_push($return_arr, $row_array);
                if ($contador == mysqli_num_rows($result)) {
                    break;
                }
            };
            echo json_encode($return_arr);
        break;

        case 'listar_horarios_banca':
            $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);

            $query =
                "select distinct extracaoauto.cod_extracao, 
                        extracaoauto.nome_banca, 
                        hora.cod_hora_extracao, 
                        hora.hora
                from extracao_automatizada extracaoauto
                inner join dia_extracao_automatizada dia on extracaoauto.cod_extracao = dia.cod_extracao
                inner join dia_hora_extracao dia_hora on dia_hora.cod_dia_extracao = dia.cod_dia_extracao
                inner join hora_extracao_automatizada hora on dia_hora.cod_hora_extracao = hora.cod_hora_extracao
                where extracaoauto.cod_extracao = '$cod_extracao'";

            $result = mysqli_query($con, $query);
            $return_arr = array();
            $contador = 0;
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $contador = $contador + 1;
                $row_array['cod_extracao'] = $row['cod_extracao'];
                $row_array['nome_banca'] = $row['nome_banca'];
                $row_array['cod_hora_extracao'] = $row['cod_hora_extracao'];
                $row_array['hora'] = $row['hora'];
                array_push($return_arr, $row_array);
                if ($contador == mysqli_num_rows($result)) {
                    break;
                }
            };
            echo json_encode($return_arr);
        break;
    }
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = "Ocorreu o seguinte erro: " . $e->getMessage();
    
    echo json_encode($response);
}

function ifStringNullReturn($val, $return)
{
    if ($val == 'null') {
        return $return;
    }
    return $val;
}