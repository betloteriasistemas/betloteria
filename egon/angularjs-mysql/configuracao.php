<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$cod_area = mysqli_real_escape_string($con, $_POST['cod_area']);

if ($cod_area == 0) {

    $queryArea = "SELECT COD_AREA
    FROM areas
    WHERE cod_site = '$site' AND padrao = 'S'";

    $resultArea = mysqli_query($con, $queryArea);
    $rowArea = mysqli_fetch_array($resultArea, MYSQLI_ASSOC);
    $cod_area = $rowArea['COD_AREA'];

}

// RECUPERANDO O PULE DO BILHETE
/*$queryMaxPule = " select max(cod_aposta) + 1 AS maxPule from aposta
WHERE COD_SITE = '$site' "; */
$queryMaxPule = "SELECT AUTO_INCREMENT
                    FROM information_schema.tables
                   WHERE table_name = 'bilhete'
                     AND table_schema = 'betloteria' ";

$resultMaxPule = mysqli_query($con, $queryMaxPule);
$rowMaxPule = mysqli_fetch_array($resultMaxPule, MYSQLI_ASSOC);
$valorMaxPule = $rowMaxPule['AUTO_INCREMENT'];

switch ($operacao) {
    case "geral":
        $query = "SELECT c.FLG_BICHO, c.FLG_QUININHA, c.FLG_SENINHA, c.FLG_2PRA500, c.FLG_LOTINHA, c.FLG_RIFA,
                         c.MINUTOS_CANCELAMENTO, c.VALOR_MIN_APOSTA, c.VALOR_MAX_APOSTA,
                         c.PRESTAR_CONTA_AUTOMATIZADA, a.PADRAO, c.NU_BILHETE_NEGRITO, c.NU_BILHETE_GRANDE,
                         c.NU_BILHETE_SUBLINHADO, c.FLG_MULTIJOGOS, c.FLG_SUPER_SENA, c.NU_WHATSAPP, c.MODALIDADE_PADRAO_BV
        FROM configuracao_geral c
        INNER JOIN areas a ON (c.COD_AREA = a.COD_AREA)
        WHERE c.COD_SITE = '$site' AND c.COD_AREA = '$cod_area'";
    break;
    case "seninha":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_14, PREMIO_15, PREMIO_16, PREMIO_17, PREMIO_18, 
                         PREMIO_19, PREMIO_20, PREMIO_25, PREMIO_30, PREMIO_35, PREMIO_40,
                         MSG_BILHETE, FLG_SORTE, SENA_SORTE, QUINA_SORTE, QUADRA_SORTE
                  FROM configuracao
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "supersena":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_SENA, MSG_BILHETE, PREMIO_QUINA, PREMIO_QUADRA
                  FROM configuracao_super_sena
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;    
    case "quininha":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_13, PREMIO_14, PREMIO_15, PREMIO_16, PREMIO_17, PREMIO_18, 
                         PREMIO_19, PREMIO_20, PREMIO_25, PREMIO_30, PREMIO_35, PREMIO_40, PREMIO_45,
                         MSG_BILHETE, FLG_SORTE, QUINA_SORTE, QUADRA_SORTE, TERNO_SORTE
                  FROM configuracao_quininha
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "lotinha":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_16, PREMIO_17, PREMIO_18, PREMIO_19, 
                         PREMIO_20, PREMIO_21, PREMIO_22, MSG_BILHETE
                  FROM configuracao_lotinha
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "bicho":
        $query = "SELECT PREMIO_MILHAR_CENTENA, PREMIO_MILHAR_SECA, PREMIO_MILHAR_INVERTIDA,
                         PREMIO_CENTENA, PREMIO_CENTENA_INVERTIDA, 
                         PREMIO_GRUPO, PREMIO_DUQUE_GRUPO, PREMIO_DUQUE_GRUPO6, PREMIO_TERNO_GRUPO, PREMIO_TERNO_GRUPO10, PREMIO_TERNO_GRUPO6, PREMIO_TERNO_SEQUENCIA, PREMIO_TERNO_SOMA, 
                         PREMIO_TERNO_ESPECIAL, PREMIO_QUINA_GRUPO,
                         PREMIO_DEZENA, PREMIO_DUQUE_DEZENA, PREMIO_DUQUE_DEZENA6, PREMIO_TERNO_DEZENA, PREMIO_TERNO_DEZENA10, PREMIO_TERNO_DEZENA6,
                         PREMIO_PASSE_SECO, PREMIO_PASSE_COMBINADO,
                         MSG_BILHETE, USA_MILHAR_BRINDE, VALOR_LIBERAR_MILHAR_BRINDE, VALOR_RETORNO_MILHAR_BRINDE
                  FROM configuracao_bicho
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
        
        $queryTiposJogosBicho = "SELECT tjb.*
                            FROM tipo_jogo_bicho tjb
                            INNER JOIN configuracao_tipo_jogo_bicho ctjb on (tjb.COD_TIPO_JOGO_BICHO = ctjb.COD_TIPO_JOGO_BICHO)
                            INNER JOIN configuracao_bicho cb on (ctjb.COD_CONFIGURACAO = cb.CODIGO)
                            WHERE cb.COD_SITE = '$site' and cb.COD_AREA = '$cod_area'
                            ORDER BY tjb.ORDEM";
    break;
    case "2pra500":
        $query = "SELECT VALOR_ACUMULADO, VALOR_APOSTA, MSG_BILHETE
                  FROM configuracao_2pra500
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "rifa":
        $query = "SELECT QTD_RIFAS, MSG_BILHETE
                  FROM configuracao_rifa
                  WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "geral2pra500":
        $query = "SELECT c.FLG_BICHO, c.FLG_QUININHA, c.FLG_SENINHA, c.FLG_2PRA500, c.FLG_LOTINHA, c.FLG_RIFA,
                         c.MINUTOS_CANCELAMENTO, c.VALOR_MIN_APOSTA, c.VALOR_MAX_APOSTA,
                         c.PRESTAR_CONTA_AUTOMATIZADA, a.PADRAO, c.NU_BILHETE_NEGRITO, c.NU_BILHETE_GRANDE,
                         c.NU_BILHETE_SUBLINHADO, conf2pra500.VALOR_ACUMULADO, conf2pra500.VALOR_APOSTA, 
                         conf2pra500. MSG_BILHETE, c.FLG_MULTIJOGOS, c.FLG_SUPER_SENA, c.NU_WHATSAPP, 
                         c.MODALIDADE_PADRAO_BV
                  FROM configuracao_geral c
                  INNER JOIN areas a ON (c.COD_AREA = a.COD_AREA)
                  INNER JOIN configuracao_2pra500 conf2pra500 ON (conf2pra500.COD_AREA = a.COD_AREA and conf2pra500.COD_SITE = c.COD_SITE)
                  WHERE c.COD_SITE = '$site' AND c.COD_AREA = '$cod_area'";

        $query_area_bicho = "SELECT COUNT(*) AS QTD_AREA_BICHO
                                FROM configuracao_geral c
                                INNER JOIN areas a ON (c.COD_AREA = a.COD_AREA)
                                WHERE c.COD_SITE = '$site'
                                    AND a.STATUS = 'A'
                                    AND c.FLG_BICHO = 'S'";
    break;
    case "multijogos":
        $query = "SELECT c.VALOR_CARTAO_1, c.VALOR_CARTAO_2, c.VALOR_CARTAO_3, c.VALOR_CARTAO_4, c.VALOR_CARTAO_5,
                    c.QTD_NUMEROS_SENINHA, c.QTD_NUMEROS_QUININHA, c.QTD_NUMEROS_LOTINHA, c.TIPOS_JOGOS_BICHO,
                    c.VALOR_MINIMO_APOSTA, c.CONCURSOS_MARCADOS
                  FROM configuracao_multijogos c
                  WHERE c.COD_SITE = '$site' AND c.COD_AREA = '$cod_area'";
    break;
    case "todas":
        $query = "SELECT 
                        c.FLG_BICHO,
                        c.FLG_QUININHA,
                        c.FLG_SENINHA,
                        c.FLG_2PRA500,
                        c.FLG_LOTINHA,
                        c.FLG_RIFA,
                        c.MINUTOS_CANCELAMENTO,
                        c.VALOR_MIN_APOSTA,
                        c.VALOR_MAX_APOSTA,
                        c.PRESTAR_CONTA_AUTOMATIZADA,
                        c.NU_BILHETE_NEGRITO, 
                        c.NU_BILHETE_GRANDE,
                        c.NU_BILHETE_SUBLINHADO,
                        c.FLG_MULTIJOGOS, 
                        c.FLG_SUPER_SENA,
                        c.NU_WHATSAPP, 
                        c.MODALIDADE_PADRAO_BV,
                        a.PADRAO,
                        c2pra500.VALOR_ACUMULADO,
                        c2pra500.VALOR_APOSTA,
                        c2pra500.MSG_BILHETE AS MSG_BIL_2PRA500,
                        sena.PREMIO_MAXIMO AS S_PREMIO_MAXIMO,
                        sena.PREMIO_14 AS S_PREMIO_14,
                        sena.PREMIO_15 AS S_PREMIO_15,
                        sena.PREMIO_16 AS S_PREMIO_16,
                        sena.PREMIO_17 AS S_PREMIO_17,
                        sena.PREMIO_18 AS S_PREMIO_18,
                        sena.PREMIO_19 AS S_PREMIO_19,
                        sena.PREMIO_20 AS S_PREMIO_20,
                        sena.PREMIO_25 AS S_PREMIO_25,
                        sena.PREMIO_30 AS S_PREMIO_30,
                        sena.PREMIO_35 AS S_PREMIO_35,
                        sena.PREMIO_40 AS S_PREMIO_40,
                        sena.MSG_BILHETE AS S_MSG_BILHETE,
                        sena.FLG_SORTE AS S_FLG_SORTE,
                        sena.SENA_SORTE AS S_SENA_SORTE,
                        sena.QUINA_SORTE AS S_QUINA_SORTE,
                        sena.QUADRA_SORTE AS S_QUADRA_SORTE,
                        quina.PREMIO_MAXIMO AS Q_PREMIO_MAXIMO,
                        supersena.PREMIO_MAXIMO AS SS_PREMIO_MAXIMO, 
                        supersena.PREMIO_SENA AS SS_PREMIO_SENA, 
                        supersena.PREMIO_QUINA AS SS_PREMIO_QUINA, 
                        supersena.PREMIO_QUADRA AS SS_PREMIO_QUADRA,
                        supersena.MSG_BILHETE AS SS_MSG_BILHETE,
                        quina.PREMIO_13 AS Q_PREMIO_13,
                        quina.PREMIO_14 AS Q_PREMIO_14,
                        quina.PREMIO_15 AS Q_PREMIO_15,
                        quina.PREMIO_16 AS Q_PREMIO_16,
                        quina.PREMIO_17 AS Q_PREMIO_17,
                        quina.PREMIO_18 AS Q_PREMIO_18,
                        quina.PREMIO_19 AS Q_PREMIO_19,
                        quina.PREMIO_20 AS Q_PREMIO_20,
                        quina.PREMIO_25 AS Q_PREMIO_25,
                        quina.PREMIO_30 AS Q_PREMIO_30,
                        quina.PREMIO_35 AS Q_PREMIO_35,
                        quina.PREMIO_40 AS Q_PREMIO_40,
                        quina.PREMIO_45 AS Q_PREMIO_45,
                        quina.MSG_BILHETE AS Q_MSG_BILHETE,
                        quina.FLG_SORTE AS Q_FLG_SORTE,
                        quina.QUINA_SORTE AS Q_QUINA_SORTE,
                        quina.QUADRA_SORTE AS Q_QUADRA_SORTE,
                        quina.TERNO_SORTE AS Q_TERNO_SORTE,
                        loto.PREMIO_MAXIMO AS L_PREMIO_MAXIMO,
                        loto.PREMIO_16 AS L_PREMIO_16,
                        loto.PREMIO_17 AS L_PREMIO_17,
                        loto.PREMIO_18 AS L_PREMIO_18,
                        loto.PREMIO_19 AS L_PREMIO_19,
                        loto.PREMIO_20 AS L_PREMIO_20,
                        loto.PREMIO_21 AS L_PREMIO_21,
                        loto.PREMIO_22 AS L_PREMIO_22,
                        loto.MSG_BILHETE AS L_MSG_BILHETE,
                        bicho.PREMIO_MILHAR_CENTENA,
                        bicho.PREMIO_MILHAR_SECA,
                        bicho.PREMIO_MILHAR_INVERTIDA,
                        bicho.PREMIO_CENTENA,
                        bicho.PREMIO_CENTENA_INVERTIDA,
                        bicho.PREMIO_GRUPO,
                        bicho.PREMIO_DUQUE_GRUPO,
                        bicho.PREMIO_DUQUE_GRUPO6,
                        bicho.PREMIO_TERNO_GRUPO,
                        bicho.PREMIO_TERNO_GRUPO10,
                        bicho.PREMIO_TERNO_GRUPO6,
                        bicho.PREMIO_TERNO_SEQUENCIA,
                        bicho.PREMIO_TERNO_SOMA,
                        bicho.PREMIO_TERNO_ESPECIAL,
                        bicho.PREMIO_QUINA_GRUPO,
                        bicho.PREMIO_DEZENA,
                        bicho.PREMIO_DUQUE_DEZENA,
                        bicho.PREMIO_DUQUE_DEZENA6,
                        bicho.PREMIO_TERNO_DEZENA,
                        bicho.PREMIO_TERNO_DEZENA10,
                        bicho.PREMIO_TERNO_DEZENA6,
                        bicho.PREMIO_PASSE_SECO,
                        bicho.PREMIO_PASSE_COMBINADO,
                        bicho.MSG_BILHETE AS B_MSG_BILHETE,
                        bicho.USA_MILHAR_BRINDE,
                        bicho.VALOR_LIBERAR_MILHAR_BRINDE,
                        bicho.VALOR_RETORNO_MILHAR_BRINDE,
                        rifa.MSG_BILHETE AS R_MSG_BILHETE,
                        multi.VALOR_CARTAO_1, 
                        multi.VALOR_CARTAO_2, 
                        multi.VALOR_CARTAO_3, 
                        multi.VALOR_CARTAO_4, 
                        multi.VALOR_CARTAO_5,
                        multi.QTD_NUMEROS_SENINHA, 
                        multi.QTD_NUMEROS_QUININHA, 
                        multi.QTD_NUMEROS_LOTINHA, 
                        multi.TIPOS_JOGOS_BICHO,
                        multi.VALOR_MINIMO_APOSTA,
                        multi.CONCURSOS_MARCADOS
                    FROM
                        areas a
                            INNER JOIN
                        configuracao_geral c ON (a.COD_AREA = c.COD_AREA
                            AND a.COD_SITE = c.COD_SITE)
                            INNER JOIN
                        configuracao_2pra500 c2pra500 ON (a.COD_AREA = c2pra500.COD_AREA
                            AND a.COD_SITE = c2pra500.COD_SITE)
                            INNER JOIN
                        configuracao sena ON (a.COD_AREA = sena.COD_AREA
                            AND a.COD_SITE = sena.COD_SITE)
                            INNER JOIN
                        configuracao_super_sena supersena ON (a.COD_AREA = supersena.COD_AREA
                            AND a.COD_SITE = supersena.COD_SITE)
                            INNER JOIN                            
                        configuracao_quininha quina ON (a.COD_AREA = quina.COD_AREA
                            AND a.COD_SITE = quina.COD_SITE)
                            INNER JOIN
                        configuracao_lotinha loto ON (a.COD_AREA = loto.COD_AREA
                            AND a.COD_SITE = loto.COD_SITE)
                            INNER JOIN
                        configuracao_bicho bicho ON (a.COD_AREA = bicho.COD_AREA
                            AND a.COD_SITE = bicho.COD_SITE)
                            LEFT JOIN
                        configuracao_rifa rifa ON (a.COD_AREA = rifa.COD_AREA
                            AND a.COD_SITE = rifa.COD_SITE)
                            INNER JOIN
                        configuracao_multijogos multi ON (a.COD_AREA = multi.COD_AREA
                            AND a.COD_SITE = multi.COD_SITE)
                    WHERE
                        a.COD_SITE = '$site' 
                            AND a.COD_AREA = '$cod_area'";
    break;    
    default:
    break;
}

$result = mysqli_query($con, $query);

$return_arr = array();

$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    switch ($operacao) {
        case "geral":
            $row_array['flg_bicho'] = $row['FLG_BICHO'] == 'S';
            $row_array['flg_seninha'] = $row['FLG_SENINHA'] == 'S';
            $row_array['flg_quininha'] = $row['FLG_QUININHA'] == 'S';
            $row_array['flg_lotinha'] = $row['FLG_LOTINHA'] == 'S';
            $row_array['flg_2pra500'] = $row['FLG_2PRA500'] == 'S';
            $row_array['flg_rifa'] = $row['FLG_RIFA'] == 'S';
            $row_array['minutos_cancelamento'] = $row['MINUTOS_CANCELAMENTO'];
            $row_array['valor_min_aposta'] = $row['VALOR_MIN_APOSTA'];
            $row_array['valor_max_aposta'] = $row['VALOR_MAX_APOSTA'];
            $row_array['prestar_conta_automatizada'] = $row['PRESTAR_CONTA_AUTOMATIZADA'];    
            $row_array['padrao'] = $row['PADRAO'];            
            $row_array['nu_bilhete_negrito'] = $row['NU_BILHETE_NEGRITO'] == 'S'; 
            $row_array['nu_bilhete_grande'] = $row['NU_BILHETE_GRANDE'] == 'S'; 
            $row_array['nu_bilhete_sublinhado'] = $row['NU_BILHETE_SUBLINHADO'] == 'S'; 
            $row_array['flg_multijogos'] = $row['FLG_MULTIJOGOS'] == 'S'; 
            $row_array['flg_super_sena'] = $row['FLG_SUPER_SENA'] == 'S'; 
            $row_array['nu_whatsapp'] = $row['NU_WHATSAPP']; 
            $row_array['modalidade_padrao_bv'] = $row['MODALIDADE_PADRAO_BV']; 
        break;
        case "2pra500":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['valor_acumulado'] = $row['VALOR_ACUMULADO'];
            $row_array['valor_aposta'] = $row['VALOR_APOSTA'];
        break;
        case "seninha":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_14'] = $row['PREMIO_14'];
            $row_array['premio_15'] = $row['PREMIO_15'];
            $row_array['premio_16'] = $row['PREMIO_16'];
            $row_array['premio_17'] = $row['PREMIO_17'];
            $row_array['premio_18'] = $row['PREMIO_18'];
            $row_array['premio_19'] = $row['PREMIO_19'];
            $row_array['premio_20'] = $row['PREMIO_20'];
            $row_array['premio_25'] = $row['PREMIO_25'];
            $row_array['premio_30'] = $row['PREMIO_30'];
            $row_array['premio_35'] = $row['PREMIO_35'];
            $row_array['premio_40'] = $row['PREMIO_40'];
            $row_array['habilita_sorte'] = $row['FLG_SORTE'] == 'S';
            $row_array['quadra_sorte'] = $row['QUADRA_SORTE'];
            $row_array['quina_sorte'] = $row['QUINA_SORTE'];
            $row_array['sena_sorte'] = $row['SENA_SORTE'];
        break;
        case "supersena":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_sena'] = $row['PREMIO_SENA'];
            $row_array['premio_quina'] = $row['PREMIO_QUINA'];
            $row_array['premio_quadra'] = $row['PREMIO_QUADRA'];
        break;
        case "quininha":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_13'] = $row['PREMIO_13'];
            $row_array['premio_14'] = $row['PREMIO_14'];
            $row_array['premio_15'] = $row['PREMIO_15'];
            $row_array['premio_16'] = $row['PREMIO_16'];
            $row_array['premio_17'] = $row['PREMIO_17'];
            $row_array['premio_18'] = $row['PREMIO_18'];
            $row_array['premio_19'] = $row['PREMIO_19'];
            $row_array['premio_20'] = $row['PREMIO_20'];
            $row_array['premio_25'] = $row['PREMIO_25'];
            $row_array['premio_30'] = $row['PREMIO_30'];
            $row_array['premio_35'] = $row['PREMIO_35'];
            $row_array['premio_40'] = $row['PREMIO_40'];
            $row_array['premio_45'] = $row['PREMIO_45'];
            $row_array['habilita_sorte'] = $row['FLG_SORTE'] == 'S';
            $row_array['terno_sorte'] = $row['TERNO_SORTE'];
            $row_array['quadra_sorte'] = $row['QUADRA_SORTE'];
            $row_array['quina_sorte'] = $row['QUINA_SORTE'];
        break;
        case "lotinha":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_16'] = $row['PREMIO_16'];
            $row_array['premio_17'] = $row['PREMIO_17'];
            $row_array['premio_18'] = $row['PREMIO_18'];
            $row_array['premio_19'] = $row['PREMIO_19'];
            $row_array['premio_20'] = $row['PREMIO_20'];
            $row_array['premio_21'] = $row['PREMIO_21'];
            $row_array['premio_22'] = $row['PREMIO_22'];
        break;
        case "bicho":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_milhar_centena'] = $row['PREMIO_MILHAR_CENTENA'];
            $row_array['premio_milhar_seca'] = $row['PREMIO_MILHAR_SECA'];
            $row_array['premio_milhar_invertida'] = $row['PREMIO_MILHAR_INVERTIDA'];
            $row_array['premio_centena'] = $row['PREMIO_CENTENA'];
            $row_array['premio_centena_invertida'] = $row['PREMIO_CENTENA_INVERTIDA'];
            $row_array['premio_grupo'] = $row['PREMIO_GRUPO'];
            $row_array['premio_duque_grupo'] = $row['PREMIO_DUQUE_GRUPO'];
            $row_array['premio_duque_grupo6'] = $row['PREMIO_DUQUE_GRUPO6'];
            $row_array['premio_terno_grupo'] = $row['PREMIO_TERNO_GRUPO'];
            $row_array['premio_terno_grupo10'] = $row['PREMIO_TERNO_GRUPO10'];
            $row_array['premio_terno_grupo6'] = $row['PREMIO_TERNO_GRUPO6'];
            $row_array['premio_terno_sequencia'] = $row['PREMIO_TERNO_SEQUENCIA'];
            $row_array['premio_terno_soma'] = $row['PREMIO_TERNO_SOMA'];
            $row_array['premio_terno_especial'] = $row['PREMIO_TERNO_ESPECIAL'];
            $row_array['premio_quina_grupo'] = $row['PREMIO_QUINA_GRUPO'];
            $row_array['premio_dezena'] = $row['PREMIO_DEZENA'];
            $row_array['premio_duque_dezena'] = $row['PREMIO_DUQUE_DEZENA'];
            $row_array['premio_duque_dezena6'] = $row['PREMIO_DUQUE_DEZENA6'];
            $row_array['premio_terno_dezena'] = $row['PREMIO_TERNO_DEZENA'];
            $row_array['premio_terno_dezena10'] = $row['PREMIO_TERNO_DEZENA10'];
            $row_array['premio_terno_dezena6'] = $row['PREMIO_TERNO_DEZENA6'];
            $row_array['premio_passe_seco'] = $row['PREMIO_PASSE_SECO'];
            $row_array['premio_passe_combinado'] = $row['PREMIO_PASSE_COMBINADO'];
            $row_array['usa_milhar_brinde'] = $row['USA_MILHAR_BRINDE'] == "S";
            $row_array['valor_liberar_milhar_brinde'] = $row['VALOR_LIBERAR_MILHAR_BRINDE'];
            $row_array['valor_retorno_milhar_brinde'] = $row['VALOR_RETORNO_MILHAR_BRINDE'];
            $row_array['valorMaxPule'] = $valorMaxPule;

            $result_tipos_jogos_bicho = mysqli_query($con, $queryTiposJogosBicho); 
            $tiposJogosBicho = array();
            while ($row_tipos_jogos_bicho = mysqli_fetch_array($result_tipos_jogos_bicho, MYSQLI_ASSOC)) {
                $row_tipo_jogos_bicho['codigo_tipo_jogo'] = $row_tipos_jogos_bicho['COD_TIPO_JOGO_BICHO'];
                $row_tipo_jogos_bicho['codigo'] = $row_tipos_jogos_bicho['CODIGO'];
                $row_tipo_jogos_bicho['descricao'] = $row_tipos_jogos_bicho['DESCRICAO'];
                array_push($tiposJogosBicho, $row_tipo_jogos_bicho);
            }
            $row_array['tipos_jogos_bicho'] = $tiposJogosBicho;  
        break;
        case "rifa":
            $row_array['qtd_rifas'] = $row['QTD_RIFAS'];
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
        break;
        case "geral2pra500":
            $row_array['flg_bicho'] = $row['FLG_BICHO'] == 'S';
            $row_array['flg_seninha'] = $row['FLG_SENINHA'] == 'S';
            $row_array['flg_quininha'] = $row['FLG_QUININHA'] == 'S';
            $row_array['flg_lotinha'] = $row['FLG_LOTINHA'] == 'S';
            $row_array['flg_2pra500'] = $row['FLG_2PRA500'] == 'S';
            $row_array['flg_rifa'] = $row['FLG_RIFA'] == 'S';
            $row_array['minutos_cancelamento'] = $row['MINUTOS_CANCELAMENTO'];
            $row_array['valor_min_aposta'] = $row['VALOR_MIN_APOSTA'];
            $row_array['valor_max_aposta'] = $row['VALOR_MAX_APOSTA'];
            $row_array['prestar_conta_automatizada'] = $row['PRESTAR_CONTA_AUTOMATIZADA'];    
            $row_array['padrao'] = $row['PADRAO'];      
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['valor_acumulado'] = $row['VALOR_ACUMULADO'];
            $row_array['valor_aposta'] = $row['VALOR_APOSTA'];    
            $row_array['nu_bilhete_negrito'] = $row['NU_BILHETE_NEGRITO'] == 'S'; 
            $row_array['nu_bilhete_grande'] = $row['NU_BILHETE_GRANDE'] == 'S'; 
            $row_array['nu_bilhete_sublinhado'] = $row['NU_BILHETE_SUBLINHADO'] == 'S'; 
            $row_array['flg_multijogos'] = $row['FLG_MULTIJOGOS'] == 'S'; 
            $row_array['flg_supersena'] = $row['FLG_SUPER_SENA'] == 'S';    
            $row_array['nu_whatsapp'] = $row['NU_WHATSAPP']; 
            $row_array['modalidade_padrao_bv'] = $row['MODALIDADE_PADRAO_BV']; 

            $result_area_bicho = mysqli_query($con, $query_area_bicho); 
            $row_area_bicho = mysqli_fetch_array($result_area_bicho, MYSQLI_ASSOC);
            $row_array['existe_area_bicho'] = $row_area_bicho['QTD_AREA_BICHO'] > 0;  

        break;
        case "multijogos":
            $row_array['valor_cartao_1'] = $row['VALOR_CARTAO_1']; 
            $row_array['valor_cartao_2'] = $row['VALOR_CARTAO_2']; 
            $row_array['valor_cartao_3'] = $row['VALOR_CARTAO_3']; 
            $row_array['valor_cartao_4'] = $row['VALOR_CARTAO_4']; 
            $row_array['valor_cartao_5'] = $row['VALOR_CARTAO_5']; 
            $row_array['qtd_numeros_seninha'] = $row['QTD_NUMEROS_SENINHA']; 
            $row_array['qtd_numeros_quininha'] = $row['QTD_NUMEROS_QUININHA']; 
            $row_array['qtd_numeros_lotinha'] = $row['QTD_NUMEROS_LOTINHA']; 
            $row_array['tipos_jogos_bicho'] = $row['TIPOS_JOGOS_BICHO']; 
            $row_array['valor_minimo_aposta'] = $row['VALOR_MINIMO_APOSTA']; 
            $row_array['concursos_marcados'] = $row['CONCURSOS_MARCADOS']; 
        break;
        case "todas":
            $row_cg['flg_bicho'] = $row['FLG_BICHO'] == 'S';
            $row_cg['flg_seninha'] = $row['FLG_SENINHA'] == 'S';
            $row_cg['flg_quininha'] = $row['FLG_QUININHA'] == 'S';
            $row_cg['flg_lotinha'] = $row['FLG_LOTINHA'] == 'S';
            $row_cg['flg_2pra500'] = $row['FLG_2PRA500'] == 'S';
            $row_cg['flg_rifa'] = $row['FLG_RIFA'] == 'S';
            $row_cg['minutos_cancelamento'] = $row['MINUTOS_CANCELAMENTO'];
            $row_cg['valor_min_aposta'] = $row['VALOR_MIN_APOSTA'];
            $row_cg['valor_max_aposta'] = $row['VALOR_MAX_APOSTA'];
            $row_cg['prestar_conta_automatizada'] = $row['PRESTAR_CONTA_AUTOMATIZADA'];    
            $row_cg['padrao'] = $row['PADRAO'];      
            $row_cg['nu_bilhete_negrito'] = $row['NU_BILHETE_NEGRITO'] == 'S'; 
            $row_cg['nu_bilhete_grande'] = $row['NU_BILHETE_GRANDE'] == 'S'; 
            $row_cg['nu_bilhete_sublinhado'] = $row['NU_BILHETE_SUBLINHADO'] == 'S'; 
            $row_cg['flg_multijogos'] = $row['FLG_MULTIJOGOS'] == 'S'; 
            $row_cg['flg_supersena'] = $row['FLG_SUPER_SENA'] == 'S';   
            $row_cg['nu_whatsapp'] = $row['NU_WHATSAPP']; 
            $row_cg['modalidade_padrao_bv'] = $row['MODALIDADE_PADRAO_BV'];           
            $cgJSON['cg'] = $row_cg;   
            array_push($return_arr, $cgJSON);   

            $row_2pra500['msg_bilhete'] = $row['MSG_BIL_2PRA500'];
            $row_2pra500['valor_acumulado'] = $row['VALOR_ACUMULADO'];
            $row_2pra500['valor_aposta'] = $row['VALOR_APOSTA']; 
            $c2pra500JSON['c2pra500'] = $row_2pra500;   
            array_push($return_arr, $c2pra500JSON);   

            $row_sena['msg_bilhete'] = $row['S_MSG_BILHETE'];
            $row_sena['premio_maximo'] = $row['S_PREMIO_MAXIMO'];
            $row_sena['premio_14'] = $row['S_PREMIO_14'];
            $row_sena['premio_15'] = $row['S_PREMIO_15'];
            $row_sena['premio_16'] = $row['S_PREMIO_16'];
            $row_sena['premio_17'] = $row['S_PREMIO_17'];
            $row_sena['premio_18'] = $row['S_PREMIO_18'];
            $row_sena['premio_19'] = $row['S_PREMIO_19'];
            $row_sena['premio_20'] = $row['S_PREMIO_20'];
            $row_sena['premio_25'] = $row['S_PREMIO_25'];
            $row_sena['premio_30'] = $row['S_PREMIO_30'];
            $row_sena['premio_35'] = $row['S_PREMIO_35'];
            $row_sena['premio_40'] = $row['S_PREMIO_40'];
            $row_sena['habilita_sorte'] = $row['S_FLG_SORTE'] == 'S';
            $row_sena['quadra_sorte'] = $row['S_QUADRA_SORTE'];
            $row_sena['quina_sorte'] = $row['S_QUINA_SORTE'];
            $row_sena['sena_sorte'] = $row['S_SENA_SORTE'];  
            $senaJSON['sena'] = $row_sena;   
            array_push($return_arr, $senaJSON);         
                        
            $row_supersena['msg_bilhete'] = $row['SS_MSG_BILHETE'];
            $row_supersena['premio_maximo'] = $row['SS_PREMIO_MAXIMO'];
            $row_supersena['premio_sena'] = $row['SS_PREMIO_SENA'];
            $row_supersena['premio_quina'] = $row['SS_PREMIO_QUINA'];
            $row_supersena['premio_quadra'] = $row['SS_PREMIO_QUADRA'];  
            $supersenaJSON['supersena'] = $row_supersena;   
            array_push($return_arr, $supersenaJSON);

            $row_quina['msg_bilhete'] = $row['Q_MSG_BILHETE'];
            $row_quina['premio_maximo'] = $row['Q_PREMIO_MAXIMO'];
            $row_quina['premio_13'] = $row['Q_PREMIO_13'];
            $row_quina['premio_14'] = $row['Q_PREMIO_14'];
            $row_quina['premio_15'] = $row['Q_PREMIO_15'];
            $row_quina['premio_16'] = $row['Q_PREMIO_16'];
            $row_quina['premio_17'] = $row['Q_PREMIO_17'];
            $row_quina['premio_18'] = $row['Q_PREMIO_18'];
            $row_quina['premio_19'] = $row['Q_PREMIO_19'];
            $row_quina['premio_20'] = $row['Q_PREMIO_20'];
            $row_quina['premio_25'] = $row['Q_PREMIO_25'];
            $row_quina['premio_30'] = $row['Q_PREMIO_30'];
            $row_quina['premio_35'] = $row['Q_PREMIO_35'];
            $row_quina['premio_40'] = $row['Q_PREMIO_40'];
            $row_quina['premio_45'] = $row['Q_PREMIO_45'];
            $row_quina['habilita_sorte'] = $row['Q_FLG_SORTE'] == 'S';
            $row_quina['terno_sorte'] = $row['Q_TERNO_SORTE'];
            $row_quina['quadra_sorte'] = $row['Q_QUADRA_SORTE'];
            $row_quina['quina_sorte'] = $row['Q_QUINA_SORTE']; 
            $quinaJSON['quina'] = $row_quina;   
            array_push($return_arr, $quinaJSON);            

            $row_loto['msg_bilhete'] = $row['L_MSG_BILHETE'];
            $row_loto['premio_maximo'] = $row['L_PREMIO_MAXIMO'];
            $row_loto['premio_16'] = $row['L_PREMIO_16'];
            $row_loto['premio_17'] = $row['L_PREMIO_17'];
            $row_loto['premio_18'] = $row['L_PREMIO_18'];
            $row_loto['premio_19'] = $row['L_PREMIO_19'];
            $row_loto['premio_20'] = $row['L_PREMIO_20'];
            $row_loto['premio_21'] = $row['L_PREMIO_21'];
            $row_loto['premio_22'] = $row['L_PREMIO_22'];
            $lotoJSON['loto'] = $row_loto;   
            array_push($return_arr, $lotoJSON);  

            $row_bicho['msg_bilhete'] = $row['B_MSG_BILHETE'];
            $row_bicho['premio_milhar_centena'] = $row['PREMIO_MILHAR_CENTENA'];
            $row_bicho['premio_milhar_seca'] = $row['PREMIO_MILHAR_SECA'];
            $row_bicho['premio_milhar_invertida'] = $row['PREMIO_MILHAR_INVERTIDA'];
            $row_bicho['premio_centena'] = $row['PREMIO_CENTENA'];
            $row_bicho['premio_centena_invertida'] = $row['PREMIO_CENTENA_INVERTIDA'];
            $row_bicho['premio_grupo'] = $row['PREMIO_GRUPO'];
            $row_bicho['premio_duque_grupo'] = $row['PREMIO_DUQUE_GRUPO'];
            $row_bicho['premio_duque_grupo6'] = $row['PREMIO_DUQUE_GRUPO6'];
            $row_bicho['premio_terno_grupo'] = $row['PREMIO_TERNO_GRUPO'];
            $row_bicho['premio_terno_grupo10'] = $row['PREMIO_TERNO_GRUPO10'];
            $row_bicho['premio_terno_grupo6'] = $row['PREMIO_TERNO_GRUPO6'];
            $row_bicho['premio_terno_sequencia'] = $row['PREMIO_TERNO_SEQUENCIA'];
            $row_bicho['premio_terno_soma'] = $row['PREMIO_TERNO_SOMA'];
            $row_bicho['premio_terno_especial'] = $row['PREMIO_TERNO_ESPECIAL'];
            $row_bicho['premio_quina_grupo'] = $row['PREMIO_QUINA_GRUPO'];
            $row_bicho['premio_dezena'] = $row['PREMIO_DEZENA'];
            $row_bicho['premio_duque_dezena'] = $row['PREMIO_DUQUE_DEZENA'];
            $row_bicho['premio_duque_dezena6'] = $row['PREMIO_DUQUE_DEZENA6'];
            $row_bicho['premio_terno_dezena'] = $row['PREMIO_TERNO_DEZENA'];
            $row_bicho['premio_terno_dezena10'] = $row['PREMIO_TERNO_DEZENA10'];
            $row_bicho['premio_terno_dezena6'] = $row['PREMIO_TERNO_DEZENA6'];
            $row_bicho['premio_passe_seco'] = $row['PREMIO_PASSE_SECO'];
            $row_bicho['premio_passe_combinado'] = $row['PREMIO_PASSE_COMBINADO'];
            $row_bicho['usa_milhar_brinde'] = $row['USA_MILHAR_BRINDE'] == "S";
            $row_bicho['valor_liberar_milhar_brinde'] = $row['VALOR_LIBERAR_MILHAR_BRINDE'];
            $row_bicho['valor_retorno_milhar_brinde'] = $row['VALOR_RETORNO_MILHAR_BRINDE'];
            $row_bicho['valorMaxPule'] = $valorMaxPule;
            $bichoJSON['bicho'] = $row_bicho;   
            array_push($return_arr, $bichoJSON); 

            $row_rifa['qtd_rifas'] = $row['QTD_RIFAS'];
            $row_rifa['msg_bilhete'] = $row['R_MSG_BILHETE'];        
            $rifaJSON['rifa'] = $row_rifa;   
            array_push($return_arr, $rifaJSON);     

            $row_multi['valor_cartao_1'] = $row['VALOR_CARTAO_1']; 
            $row_multi['valor_cartao_2'] = $row['VALOR_CARTAO_2']; 
            $row_multi['valor_cartao_3'] = $row['VALOR_CARTAO_3']; 
            $row_multi['valor_cartao_4'] = $row['VALOR_CARTAO_4']; 
            $row_multi['valor_cartao_5'] = $row['VALOR_CARTAO_5']; 
            $row_multi['qtd_numeros_seninha'] = $row['QTD_NUMEROS_SENINHA']; 
            $row_multi['qtd_numeros_quininha'] = $row['QTD_NUMEROS_QUININHA']; 
            $row_multi['qtd_numeros_lotinha'] = $row['QTD_NUMEROS_LOTINHA']; 
            $row_multi['tipos_jogos_bicho'] = $row['TIPOS_JOGOS_BICHO']; 
            $row_multi['valor_minimo_aposta'] = $row['VALOR_MINIMO_APOSTA']; 
            $row_multi['concursos_marcados'] = $row['CONCURSOS_MARCADOS']; 
            $multiJSON['multi'] = $row_multi;  
            array_push($return_arr, $multiJSON); 
                       
        break;
    }
    
    if ($operacao != "todas") {
        array_push($return_arr, $row_array);
    }

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}

$con->close();
echo json_encode($return_arr);
