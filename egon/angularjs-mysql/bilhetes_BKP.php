<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

session_start();

$response = [];

$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
//$username = "will";
//$site = 1;

$contador = 0;

if ($operacao == "listar") {
    $query = "select bi.cod_bilhete, bi.txt_pule, usu.nome cambista, geren.nome gerente, bi.nome_apostador, bi.telefone_apostador,
            DATE_FORMAT(bi.data_bilhete, '%d-%m-%Y') data_bilhete, jogo.concurso, jogo.tipo_jogo,
            DATE_FORMAT(bi.data_bilhete, '%d/%m/%Y %H:%i:%S') data_hora_bilhete,
            count(apo.cod_aposta) qtd_bilhetes,  round(sum(apo.valor_aposta),2) total,
            round(sum(apo.valor_aposta) * usu.pct_comissao / 100,2) as comissao,
            round(sum(case
            when apo.status = 'G' then apo.possivel_retorno 
            else 0 end), 2) possivel_retorno,
            case 
            when bi.status_bilhete = 'A' then 'Aberto'
            when bi.status_bilhete = 'C' then 'Cancelado'
            when bi.status_bilhete = 'P' then 'Processado' end status_bilhete
            from bilhete bi
            inner join aposta apo on (bi.cod_bilhete = apo.cod_bilhete)
            inner join usuario usu on (bi.cod_usuario = usu.cod_usuario)
            inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
            inner join jogo on (jogo.cod_jogo = apo.cod_jogo)
            where bi.cod_site = '$site'
            and ( (usu.cod_usuario = '$cod_usuario' or usu.cod_gerente = '$cod_usuario')  or (geren.cod_usuario = '$cod_usuario' or geren.cod_gerente = '$cod_usuario') )
            group by bi.cod_bilhete, bi.txt_pule, usu.nome, geren.nome, bi.nome_apostador, bi.telefone_apostador, bi.data_bilhete, jogo.concurso, jogo.tipo_jogo ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_bilhete'] = $row['cod_bilhete'];
        $row_array['txt_pule'] = $row['txt_pule'];        
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_hora_bilhete'] = $row['data_hora_bilhete'];
        
        $row_array['qtd_bilhetes'] = $row['qtd_bilhetes'];
        $row_array['total'] = $row['total'];
        $row_array['comissao'] = $row['comissao'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['concurso'] = $row['concurso']; 
        $row_array['status_bilhete'] = $row['status_bilhete']; 
        $row_array['gerente'] = $row['gerente']; 
        $row_array['tipo_jogo'] = $row['tipo_jogo']; 
        

               

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }
    ;

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);
} else if ($operacao == "getCambistas") {
    $query = /*" select nome, cod_usuario from usuario
               where cod_usuario = '$cod_usuario'
                 and cod_site = '$site'
               union "*/
              " select nome, cod_usuario from usuario
               where cod_gerente = '$cod_usuario'
                 and cod_site = '$site'
                 and status = 'A'
               order by nome ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['nome'] = $row['nome'];
        $row_array['cod_usuario'] = $row['cod_usuario'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);
} else if ($operacao == "listarApostas") {
    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);

    $query = " select txt_aposta, qtd_numeros, valor_aposta, possivel_retorno,
    case
     when status = 'A' then 'Aberta'
     when status = 'G' then 'Ganho'
     when status = 'C' then 'Cancelado'
     when status = 'P' then 'Perdido' end as status
    from aposta
    where cod_bilhete = '$cod_bilhete'
      and cod_site = '$site' ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['txt_aposta'] = $row['txt_aposta'];
        $row_array['qtd_numeros'] = $row['qtd_numeros'];
        $row_array['valor_aposta'] = $row['valor_aposta'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['status'] = $row['status'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);
} else if ($operacao == "conferirBilhete") {

    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);

    $query = "  SELECT DATE_FORMAT(bil.data_bilhete, '%d/%m/%Y %H:%i:%S') data_bilhete,
                       DATE_FORMAT(bil.data_bilhete, '%Y-%m-%dT%H:%i:%S') data_bilhete_js,
                site.NOME_BANCA NOME_BANCA,
                usu.nome cambista,
                bil.nome_apostador,
                bil.telefone_apostador,
                bil.txt_pule,
                case
                    when bil.status_bilhete = 'A' then 'Aberto'
                    when bil.status_bilhete = 'P' then 'Processado'
                    when bil.status_bilhete = 'C' then 'Cancelado' end as status_bilhete,
                apo.qtd_numeros,
                apo.txt_aposta,
                apo.valor_aposta,
                apo.possivel_retorno,
                case
                    when apo.status = 'A' then 'Aberto'
                    when apo.status = 'G' then 'Ganho'
                    when apo.status = 'C' then 'Cancelado'
                    when apo.status = 'P' then 'Perdido' end as status,
                jogo.data_jogo,
                jogo.concurso,
                case
                  when jogo.tipo_jogo = 'S' then 'SENINHA'
                  when jogo.tipo_jogo = 'Q' then 'QUININHA' else ''
                  end tipo_jogo, 
                CONCAT(LPAD(jogo.numero_1,2,'0'), ' - ', LPAD(jogo.numero_2,2,'0'), ' - ', LPAD(jogo.numero_3,2,'0'), ' - ',
                       LPAD(jogo.numero_4,2,'0'), ' - ', LPAD(jogo.numero_5,2,'0'), ' - ', LPAD(jogo.numero_6,2,'0')) numeros_sorteados
                FROM betloteria.bilhete bil
                inner join site on (site.cod_site = bil.cod_site)
                inner join aposta apo on (bil.cod_bilhete = apo.cod_bilhete)
                inner join jogo on (apo.cod_jogo = jogo.cod_jogo)
                inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                where bil.txt_pule = '$cod_bilhete'
                and bil.cod_site = '$site' ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_bilhete_js'] = $row['data_bilhete_js'];      
        $row_array['nome_banca'] = $row['NOME_BANCA'];              
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['status_bilhete'] = $row['status_bilhete'];        
        $row_array['txt_pule'] = $row['txt_pule'];
        $row_array['qtd_numeros'] = $row['qtd_numeros'];
        $row_array['txt_aposta'] = $row['txt_aposta'];
        $row_array['valor_aposta'] = $row['valor_aposta'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['status'] = $row['status'];
        $row_array['data_jogo'] = $row['data_jogo'];
        $row_array['concurso'] = $row['concurso'];
        $row_array['numeros_sorteados'] = $row['numeros_sorteados'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);

}
