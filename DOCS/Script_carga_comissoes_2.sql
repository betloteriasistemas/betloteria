SET SQL_SAFE_UPDATES = 0;

UPDATE aposta a
        JOIN
    (SELECT 
        apo.cod_aposta,
            CASE
                WHEN jogo.Tipo_Jogo = 'S' THEN SUM(apo.valor_aposta) * usu.pct_comissao_seninha / 100
                WHEN jogo.Tipo_Jogo = 'Q' THEN SUM(apo.valor_aposta) * usu.pct_comissao_quininha / 100
                WHEN jogo.Tipo_Jogo = '2' THEN SUM(apo.valor_aposta) * usu.pct_comissao_2PRA500 / 100
                WHEN jogo.Tipo_Jogo = '6' THEN SUM(apo.valor_aposta) * usu.pct_comissao_6DASORTE / 100
                WHEN
                    jogo.Tipo_Jogo IN ('B' , 'G')
                THEN
                    CASE
                        WHEN
                            apo.TIPO_JOGO = 'MC'
                                AND conf.COMISSAO_MILHAR_CENTENA IS NOT NULL
                                AND conf.COMISSAO_MILHAR_CENTENA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_MILHAR_CENTENA / 100
                        WHEN
                            apo.TIPO_JOGO = 'MS'
                                AND conf.COMISSAO_MILHAR_SECA IS NOT NULL
                                AND conf.COMISSAO_MILHAR_SECA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_MILHAR_SECA / 100
                        WHEN
                            (apo.TIPO_JOGO = 'MI'
                                OR apo.TIPO_JOGO = 'MCI')
                                AND conf.COMISSAO_MILHAR_INVERTIDA IS NOT NULL
                                AND conf.COMISSAO_MILHAR_INVERTIDA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_MILHAR_INVERTIDA / 100
                        WHEN
                            apo.TIPO_JOGO = 'C'
                                AND conf.COMISSAO_CENTENA IS NOT NULL
                                AND conf.COMISSAO_CENTENA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_CENTENA / 100
                        WHEN
                            apo.TIPO_JOGO = 'CI'
                                AND conf.COMISSAO_CENTENA_INVERTIDA IS NOT NULL
                                AND conf.COMISSAO_CENTENA_INVERTIDA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_CENTENA_INVERTIDA / 100
                        WHEN
                            apo.TIPO_JOGO = 'G'
                                AND conf.COMISSAO_GRUPO IS NOT NULL
                                AND conf.COMISSAO_GRUPO > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_GRUPO / 100
                        WHEN
                            (apo.TIPO_JOGO = 'DG'
                                OR apo.TIPO_JOGO = 'DGC')
                                AND conf.COMISSAO_DUQUE_GRUPO IS NOT NULL
                                AND conf.COMISSAO_DUQUE_GRUPO > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_DUQUE_GRUPO / 100
                        WHEN
                            (apo.TIPO_JOGO = 'TG'
                                OR apo.TIPO_JOGO = 'TGC'
                                OR apo.TIPO_JOGO = 'TG10')
                                AND conf.COMISSAO_TERNO_GRUPO IS NOT NULL
                                AND conf.COMISSAO_TERNO_GRUPO > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_TERNO_GRUPO / 100
                        WHEN
                            apo.TIPO_JOGO = 'DZ'
                                AND conf.COMISSAO_DEZENA IS NOT NULL
                                AND conf.COMISSAO_DEZENA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_DEZENA / 100
                        WHEN
                            (apo.TIPO_JOGO = 'DDZ'
                                OR apo.TIPO_JOGO = 'DDZC')
                                AND conf.COMISSAO_DUQUE_DEZENA IS NOT NULL
                                AND conf.COMISSAO_DUQUE_DEZENA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_DUQUE_DEZENA / 100
                        WHEN
                            (apo.TIPO_JOGO = 'TDZ'
                                OR apo.TIPO_JOGO = 'TDZC')
                                AND conf.COMISSAO_TERNO_DEZENA IS NOT NULL
                                AND conf.COMISSAO_TERNO_DEZENA > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_TERNO_DEZENA / 100
                        WHEN
                            (apo.TIPO_JOGO = 'PS'
                                OR apo.TIPO_JOGO = 'PS12')
                                AND conf.COMISSAO_PASSE_SECO IS NOT NULL
                                AND conf.COMISSAO_PASSE_SECO > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_PASSE_SECO / 100
                        WHEN
                            apo.TIPO_JOGO = 'PC'
                                AND conf.COMISSAO_PASSE_COMBINADO IS NOT NULL
                                AND conf.COMISSAO_PASSE_COMBINADO > 0
                        THEN
                            SUM(apo.valor_aposta) * conf.COMISSAO_PASSE_COMBINADO / 100
                        ELSE SUM(apo.valor_aposta) * usu.pct_comissao_bicho / 100
                    END
            END AS comissao
    FROM
        jogo
    INNER JOIN aposta apo ON (apo.cod_jogo = jogo.cod_jogo
        AND apo.cod_site = jogo.cod_site)
    INNER JOIN bilhete bil ON (bil.cod_bilhete = apo.cod_bilhete
        AND apo.cod_site = bil.cod_site)
    INNER JOIN usuario usu ON (bil.cod_usuario = usu.cod_usuario
        AND bil.cod_site = usu.cod_site)
    LEFT JOIN configuracao_comissao_bicho conf ON usu.cod_usuario = conf.cod_usuario
    WHERE
        bil.status_bilhete NOT IN ('C' , 'F')
            AND apo.status NOT IN ('C')
            AND bil.COD_SITE = 500 -- 1005,1015,1111,1122,1200,1414,1616,2306,2536,4343,4808,4809,4810,4811,4812,6543,6655,7788,8899,9998,9999
            AND apo.COD_SITE = 500 -- 1005,1015,1111,1122,1200,1414,1616,2306,2536,4343,4808,4809,4810,4811,4812,6543,6655,7788,8899,9998,9999
    GROUP BY apo.cod_aposta) AS b ON a.cod_aposta = b.cod_aposta 
SET 
    a.comissao = b.comissao
WHERE
    a.status NOT IN ('C')
    AND a.COD_SITE = 500; -- 1005,1015,1111,1122,1200,1414,1616,2306,2536,4343,4808,4809,4810,4811,4812,6543,6655,7788,8899,9998,9999
	
SET SQL_SAFE_UPDATES = 1;