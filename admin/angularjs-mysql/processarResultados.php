<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];
$mensagem = "";

try {

    $queryMain = " select * from jogo
where tp_status = 'B'  
  and tipo_jogo <> 'B' 
  and cod_site >= 1000
  and cod_site <> 9999; ";

    $resultMain = mysqli_query($con, $queryMain);

    while ($rowMain = mysqli_fetch_array($resultMain, MYSQLI_ASSOC)) {
        $resultado = [];
        $resultado[0] = str_pad($rowMain['NUMERO_1'], 2, "0", STR_PAD_LEFT);
        $resultado[1] = str_pad($rowMain['NUMERO_2'], 2, "0", STR_PAD_LEFT);
        $resultado[2] = str_pad($rowMain['NUMERO_3'], 2, "0", STR_PAD_LEFT);
        $resultado[3] = str_pad($rowMain['NUMERO_4'], 2, "0", STR_PAD_LEFT);
        $resultado[4] = str_pad($rowMain['NUMERO_5'], 2, "0", STR_PAD_LEFT);

        $tipo_jogo = $rowMain['TIPO_JOGO'];
        $codigo = $rowMain['COD_JOGO'];

        if ($tipo_jogo === "S") {
            $resultado[5] = str_pad($rowMain['NUMERO_6'], 2, "0", STR_PAD_LEFT);
        }

        if ($resultado[0] === "00" || $resultado[1] === "00" || $resultado[2] === "00" || $resultado[3] === "00" ||
            $resultado[4] === "00" || ($tipo_jogo === "S" && $resultado[5] === "00")) {
                $mensagem = $mensagem . "Site: " . $rowMain['COD_SITE'] ." Concurso: " . $rowMain['CONCURSO'] . " | "; 
                continue;
            /*$response['status'] = "ERROR";
        $response['mensagem'] = "É necessário preencher todos os numeros do concurso!";
        echo json_encode($response);
        $con->close();
        return;*/
        }

        $query = " select apo.cod_aposta, apo.txt_aposta, apo.cod_bilhete 
              from aposta apo
              inner join bilhete bil on (bil.cod_bilhete = apo.cod_bilhete)
               where apo.cod_jogo = '$codigo' 
                 and apo.status = 'A' 
                 and bil.status_bilhete = 'A' ";

        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $cod_aposta = $row['cod_aposta'];
            $cod_bilhete = $row['cod_bilhete'];
            $array = explode('-', str_replace(' ', '', $row['txt_aposta']));
            $contador = 0;
            foreach ($array as $value) {
                if (in_array($value, $resultado)) {
                    $contador = $contador + 1;
                }
            }
            $status = "P";
            if ($contador == 6 && $tipo_jogo === "S") {
                $status = "G";
            }
            if ($contador == 5 && $tipo_jogo === "Q") {
                $status = "G";
            }

            $sqlUpdate = "Update aposta set status = '$status' where cod_aposta = '$cod_aposta' ";

            if ($con->query($sqlUpdate) != true) {
                continue;
                /*$response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
            return;*/
            }

            $sqlUpdate = "Update bilhete set status_bilhete = 'P' where cod_bilhete = '$cod_bilhete' ";

            if ($con->query($sqlUpdate) != true) {
                continue;
                /*$response['status'] = "ERROR";
            $response['mensagem'] = $con->error;
            return;*/
            }

        }

        $sqlUpdate = "Update jogo set tp_status = 'P' where cod_jogo = '$codigo' ";

        if ($con->query($sqlUpdate) === true) {
            //$response['status'] = "OK";
            continue;
        } /*else {
    $response['status'] = "ERROR";
    $response['mensagem'] = $con->error;
    }*/

    }

    $response['status'] = "OK";
    $response['mensagem'] = $mensagem;
} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = $e->getMessage();
} finally {
    echo json_encode($response);
    $con->close();
}
