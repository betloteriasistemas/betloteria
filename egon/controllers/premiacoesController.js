angular.module('main').controller('premiacoesCtrl',
    function (bilhetesAPI, $rootScope, $scope, user, $http, $location, $filter, funcoes, configGeral) {
        $rootScope.loading = false;
        $scope.perfil = user.getPerfil();

        $scope.comboConcurso = "";
        $scope.comboVendedor = "";
        $scope.comboTipoJogo = "";
        $scope.tiposJogos = funcoes.getTiposJogos();
        $scope.comboPagos = "";
        $scope.edtDataDe = new Date();
        $scope.edtDataAte = new Date();
        $scope.chkExibirFinalizados = false;
        $scope.desc_2pra500 = '2 PRA 500';
        configGeral.get().then(function (data) {
            $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        });

        /** ORDENAÇÃO */
        $scope.sort = "DATA";
        $scope.sortType = false;

        $scope.totalItens = 0;
        $scope.index = 0;
        $scope.itensPorPagina = 20;

        $scope.paginacao = {
            atual: 1
        };

        /**
         * Procurar Bilhete
         */
        $scope.bilhete
        $scope.detalhesBilhetePremiado;
        $scope.iniciou = false;
        $scope.exibeBilhete = false;
        $scope.data_bilhete = null;
        $scope.data_bilhete_js = null;
        $scope.nome_banca = "";
        $scope.cambista = "";
        $scope.nome_apostador = "";
        $scope.telefone_apostador = "";
        $scope.concurso = "";
        $scope.txt_pule = "";
        $scope.status = "";
        $scope.status_bilhete = "";
        $scope.data_jogo = null;
        $scope.externo = false;
        $scope.qrcodeString = '';

        $scope.records = [];

        $scope.tipo_relatorio = 'pdf';
        $scope.eh_pdf = true;
    
        $scope.alteraTipoRelatorio = function() {
            if ($scope.eh_pdf) {
                $scope.eh_pdf = true;
                $scope.tipo_relatorio = 'pdf';
            } else {
                $scope.eh_pdf = false;
                $scope.tipo_relatorio = 'xlsx';
            }
        }

        $scope.gerarRelatorio = function () {

            if ($scope.edtDataDe > $scope.edtDataAte) {
                alert('A data final do período deve ser maior ou igual a data inicial.');
                return;
            }

            var dataDe  = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
            var dataAte = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");

            var tipoJogo = $scope.comboTipoJogo;
            var descTipoJogo = "";
            if (tipoJogo) {
                descTipoJogo = $("#comboTipoJogo")[0].selectedOptions[0].label
            }

            var concurso = $scope.comboConcurso;
            var descConcurso = "";
            if (concurso) {
                descConcurso = $("#comboConcurso")[0].selectedOptions[0].label
            }

            var vendedor= $scope.comboVendedor;
            var descVendedor = "";
            if (vendedor) {
                descVendedor = $("#comboVendedor")[0].selectedOptions[0].label
            }

            window.open('angularjs-mysql/gerarRelatorioPremiacoes.php?cod_usuario=' + user.getCodigo()
                + '&perfil_usuario=' + user.getPerfil()
                + '&site=' + user.getSite()
                + '&index=' + $scope.index
                + '&qtdElementos=' + $scope.itensPorPagina
                + '&sort=' + $scope.sort
                + '&sortType=' + $scope.sortType
                + '&concurso=' + $scope.comboConcurso
                + '&vendedor=' + $scope.comboVendedor
                + '&tipoJogo=' + $scope.comboTipoJogo
                + '&pago=' + $scope.comboPagos
                + '&finalizados=' + $scope.chkExibirFinalizados
                + '&dataDe=' + dataDe
                + '&dataAte=' + dataAte
                + '&descTipoJogo=' + descTipoJogo
                + '&descConcurso=' + descConcurso
                + '&descVendedor=' + descVendedor
                + '&desc_2pra500=' + $scope.desc_2pra500
                + '&schema=' + user.getSchema()
                + '&tipoRelatorio=' + $scope.tipo_relatorio);                                       
        }

        $scope.alterarPagina = function (newPage) {
            $scope.index = (newPage - 1) * $scope.itensPorPagina;
            $scope.pesquisarBilhetesPremiados();
        };

        $scope.orderFilter = function (column) {
            if ($scope.sort == column) {
                $scope.sortType = !$scope.sortType;
            } else {
                $scope.sortType = true;
            }
            $scope.sort = column;
            this.alterarPagina(1);
        };

        $scope.getJogos = function () {
            $http({
                method: "POST",
                url: "angularjs-mysql/jogos.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'username=' + user.getName() + '&finalizados=true'
                    + '&operacao=' + '&site=' + user.getSite()
                    + '&status=' + '&index='
                    + '&qtdElementos=' + '&schema=' + user.getSchema()                    
            }).then(function (response) {
                $scope.jogos = response.data.filter(obj => !obj.totalizadores);
            }).catch(function (response) {
                alert('Erro no $HTTP: ' + response.status)
            });
        }

        $scope.getDescricaoTipoJogo = function (tipo_jogo) {
            return funcoes.getDescricaoTipoJogo(tipo_jogo, $scope.desc_2pra500);
        }

        $scope.getDescricaoConcurso = function (jogo) {
            return funcoes.getDescricaoConcurso(jogo);
        }

        $scope.getResumoDescricaoConcurso = function (jogo) {
            switch (jogo.tipo_jogo) {
                case 'B':
                case '2':
                    return jogo.hora_extracao + " - " + jogo.desc_hora;
                case 'S':
                case 'Q':
                case 'L':
                case 'U':
                    return jogo.concurso;
                case 'R':
                    return jogo.descricao;
                default:
                    return "";
            }
        }


        $scope.descricaoModalidadeJogo = function (tipo_jogo) {
            return funcoes.getDescricaoTipoJogo(tipo_jogo);
        }

        $scope.getCambistas = function () {
            if (user.getCodigo() == 'C') {
                $scope.comboVendedor = user.getName();
                return;
            }
            $http({
                method: "POST",
                url: "angularjs-mysql/bilhetes.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo() + 
                      '&site=' + user.getSite() + 
                      '&operacao=getCambistas' +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.cambistas = response.data;
            }).catch(function (response) {
                alert('Erro no $HTTP: ' + response.status)
            });
        }

        $scope.ajustaDataFimPeriodo = function () {
            if ($scope.edtDataDe > $scope.edtDataAte) {
                $scope.edtDataAte = $scope.edtDataDe;
            }
        }

        $scope.pesquisarBilhetesPremiados = function () {
            if ($scope.edtDataDe > $scope.edtDataAte) {
                alert('A data final do período deve ser maior ou igual a data inicial.');
                return;
            }
            $scope.records = [];
            $rootScope.loading = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/premiacoes.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'cod_usuario=' + user.getCodigo()
                    + '&perfil_usuario=' + user.getPerfil()
                    + '&site=' + user.getSite()
                    + '&index=' + $scope.index
                    + '&qtdElementos=' + $scope.itensPorPagina
                    + '&sort=' + $scope.sort
                    + '&sortType=' + $scope.sortType
                    + '&concurso=' + $scope.comboConcurso
                    + '&vendedor=' + $scope.comboVendedor
                    + '&tipoJogo=' + $scope.comboTipoJogo
                    + '&pago=' + $scope.comboPagos
                    + '&finalizados=' + $scope.chkExibirFinalizados
                    + '&dataDe=' + ((typeof $scope.edtDataDe === 'undefined' || $scope.edtDataDe == null) ?
                        '0' : $filter('date')($scope.edtDataDe, "yyyy-MM-dd"))
                    + '&dataAte=' + ((typeof $scope.edtDataAte === 'undefined' || $scope.edtDataAte == null) ?
                        '0' : $filter('date')($scope.edtDataAte, "yyyy-MM-dd"))
                    + '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.records = response.data.filter(obj => obj.totalItens == null);
                $scope.totalItens = response.data.filter(obj => obj.totalItens != null)[0].totalItens;
                scrollParaTabela();
            }).catch(function (response) {
                console.log(response);
                alert('Erro no $HTTP: ' + response.status);
            }).finally(function () {
                $rootScope.loading = false;
                if ($scope.records) {
                    scrollParaTabela();
                }
                if ($scope.records.length == 0) {
                    alert('Nenhum resultado encontrado para os filtros selecionados!');
                }
            });
        }

        $scope.procurarBilhete = function (codigo, dadosIniciaisBusca = true) {
            $scope.detalhesBilhetePremiado = {};
            $scope.iniciou = true;
            const response = bilhetesAPI.getBilhetes(codigo, null);
            response.then(function (result) {
                $scope.bilhete = result;
                verificarBilhetePremiado($scope.bilhete[0]);
            });
        }

        $scope.atualizarStatusPagamento = function () {
            $http({
                url: 'angularjs-mysql/salvarBilheteTeimosinha.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: "cod_usuario=" + user.getCodigo() +
                    "&cod_site=" + user.getSite() +
                    "&nome_usuario=" + user.getName() +
                    "&operacao=atualizarStatusPagamento" +
                    "&txt_pule=" + $scope.txt_pule +
                    "&schema=" + user.getSchema()
            }).then(function (response) {
                if (response.data.status == 'OK') {
                    $scope.procurarBilhete($scope.txt_pule, null);
                    $scope.pesquisarBilhetesPremiados();
                } else {
                    console.log('error');
                }
            }).catch(function (response) {
                console.log(response);
                alert('Erro no $HTTP: ' + response.status)
            }).finally(function () {
            });
        }


        function verificarBilhetePremiado(bilhete) {
            const valorPremio = retornoBilhetePremiado(bilhete);
            if (valorPremio !== 0) {
                $scope.detalhesBilhetePremiado = {
                    valorPremio,
                    mostrarInformacoesBilhetePremiado: true,
                    statusPagamento: 'pendente'
                }
                if (bilhete.status_pagamento === 'S') {
                    $scope.detalhesBilhetePremiado.statusPagamento = 'efetivado';
                }
            }
        }

        function retornoBilhetePremiado(bilhete) {
            return bilhete.apostas.filter(aposta => aposta.status === 'Ganho')
                .reduce((acumulador, aposta) => acumulador + parseFloat(aposta.valor_ganho), 0);
        }

        $scope.$watch('bilhete', function () {
            if ($scope.iniciou) {
                $scope.exibeBilhete = $scope.bilhete.length > 0;
                if (!$scope.exibeBilhete && !$scope.externo) {
                    limpaCampos();
                    alert('Bilhete não encontrado!');
                } else {
                    try {
                        $scope.tipo_jogo = $scope.bilhete[0].tipo_jogo;
                        $scope.data_bilhete = $scope.bilhete[0].data_bilhete;
                        $scope.data_bilhete_js = $scope.bilhete[0].data_bilhete_js;
                        $scope.nome_banca = $scope.bilhete[0].nome_banca;
                        $scope.cambista = $scope.bilhete[0].cambista;
                        $scope.nome_apostador = $scope.bilhete[0].nome_apostador;
                        $scope.telefone_apostador = $scope.bilhete[0].telefone_apostador;
                        $scope.concurso = $scope.bilhete[0].concurso;
                        $scope.txt_pule = $scope.bilhete[0].txt_pule;
                        $scope.qrcodeString = $scope.bilhete[0].txt_pule;
                        $scope.data_jogo = $scope.bilhete[0].data_jogo;
                        $scope.numeros_sorteados = $scope.bilhete[0].numeros_sorteados;
                        $scope.status_bilhete = $scope.bilhete[0].status_bilhete;
                        $scope.linkZap = $scope.bilhete[0].linkZap;
                        $scope.valor_bilhete = $scope.bilhete[0].valor_bilhete;
                        $scope.numero_bilhete = $scope.bilhete[0].numero_bilhete;
                    } catch (error) {
                        console.log(error);
                    }
                }
            }
        });

        function limpaCampos() {
            $scope.exibeBilhete = false;
            $scope.data_bilhete = null;
            $scope.data_bilhete_js = null;
            $scope.nome_banca = "";
            $scope.cambista = "";
            $scope.nome_apostador = "";
            $scope.telefone_apostador = "";
            $scope.concurso = "";
            $scope.txt_pule = "";
            $scope.qrcodeString = "";
            $scope.status = "";
            $scope.status_bilhete = "";
            $scope.data_jogo = null;
        }

        $scope.clickZap = function (confirm) {
            var linkWeb = 'Para conferir seu bilhete acesse:'
                + '%0ahttp://bilheteloteria.com/#!/' + $scope.txt_pule;
            var linkZap = 'Para conferir seu bilhete acesse:'
                + '%0ahttp://bilheteloteria.com/%23!/' + $scope.txt_pule;

            if (user.getDominio()) {
                linkWeb = linkWeb + '%0a%0aAposte em:%0a' + user.getDominio();
                linkZap = linkZap + '%0a%0aAposte em:%0a' + user.getDominio();
            }

            linkZap = linkZap + '%0a%0aBoa sorte 🍀!';
            linkWeb = linkWeb + '%0a%0aBoa sorte 🍀!';

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                if (confirm) {
                    if ($scope.telefone_apostador != '') {
                        window.open('https://wa.me/55' + $scope.telefone_apostador + '?text=' + linkZap);
                    } else {
                        window.open('https://wa.me/?text=' + linkZap);
                    }
                } else {
                    window.open('https://wa.me/?text=' + linkZap);
                }
            } else {
                if (confirm) {
                    if ($scope.telefone_apostador != '') {
                        window.open('https://web.whatsapp.com/send?phone=55' + $scope.telefone_apostador + '&text=' + linkWeb);
                    } else {
                        window.open('https://web.whatsapp.com/send?text=' + linkWeb);
                    }
                } else {
                    window.open('https://web.whatsapp.com/send?text=' + linkWeb);
                }
            }
        }

        $scope.confirmationDialog = function () {
            jQuery("#modalConferirBilhetes").modal('hide');
            if ($scope.telefone_apostador != '') {
                $scope.confirmationDialogConfig = {
                    title: "",
                    message: "Deseja enviar o bilhete para o número: " + $scope.telefone_apostador + "?",
                    buttons: [{
                        label: "Sim",
                        action: "com-telefone"
                    },
                    {
                        label: "Outro Número",
                        action: "sem-telefone"
                    }]
                };
                $scope.showDialog(true);
            } else {
                $scope.clickZap(false);
            }
        }

        $scope.showDialog = function (flag) {
            jQuery("#confirmation-dialog").modal(flag ? 'show' : 'hide');
        }

        $scope.executeDialogAction = function (action) {
            switch (action) {
                case 'com-telefone':
                    $scope.clickZap(true);
                    break;
                case 'sem-telefone':
                    $scope.clickZap(false);
                    break;
            }
            $scope.showDialog(false);
        }

        $scope.imprimir = function () {
            var linka = $scope.getLinkMobile($scope.txt_pule);
            window.open(linka);
        }
        
        $scope.getLinkMobile = function (txt_pule) {
            var strAux = "com.mitsoftwar.appprinter://" + $location.absUrl() 
                + "/angularjs-mysql/bilheteCodificado.php?id=" + txt_pule 
                + "&site=" + user.getSite()
                + "&cod_usuario=" + user.getCodigo();
            strAux = strAux.replace('#!/premiacoes/', '');
            return strAux;
        }

        function scrollParaTabela() {
            $('html, body').animate({ scrollTop: $('#dir-paginator-top').offset().top - 25 }, 'slow');
        }

        $scope.filterByTipo = function (jogo) {
            return $scope.comboTipoJogo == "" || jogo.tipo_jogo == $scope.comboTipoJogo;
        }
    });