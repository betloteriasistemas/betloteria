angular.module('main').controller('bichoCtrl', function (bilhetesAPI, $scope, user, $http, $filter, $location) {

    $http({
        url: 'angularjs-mysql/configuracao.php',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: 'site=' + user.getSite() + '&operacao=bicho'
    }).then(function (response) {
        $scope.listaCfg = response.data;
    });
    $scope.site = user.getSite();

    $scope.puleRepeticao = user.getPuleRepeticao();
    $scope.listaApostas = [];
    if ($scope.puleRepeticao != "") {
        $http({
            url: 'angularjs-mysql/bilhetes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + '&cod_usuario=' + user.getCodigo() + '&operacao=conferirBilhete' + '&cod_bilhete=' + $scope.puleRepeticao
        }).then(function (response) {
            $scope.listaApostas = response.data;
            $scope.verificaPuleRepeticao();
            user.setPuleRepeticao('');
        });
    }

    $scope.edtDataDe = new Date();

    $scope.apostas = [];

    $scope.comboTipoJogo = "VV";

    $scope.valorTotal = "0,00";

    $scope.botaoHabilitado = false;

    $scope.getShowMilharNaQuadra = function () {        
        var retorno = false;
        if ($scope.comboTipoJogo == "MS" || $scope.comboTipoJogo == "MC" || $scope.comboTipoJogo == "C") {
            retorno = true;
        }        
        return retorno;
    }

    $scope.carregaLimite = function () {
        //$scope.apostas = [];
        //$scope.valorTotal = "0,00";
        $http({
            url: 'angularjs-mysql/limiteBicho.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + '&operacao=carregar' + '&codigo=' + $scope.comboConcurso
        }).then(function (response) {
            $scope.listaLimite = response.data;            
        });
    }

    // Limpa todos os números selecionados
    $scope.limpar = function () {
        $("button.btn-danger").removeClass("btn-danger").addClass("btn-outline-danger");
        $("#numerosSelecionados").html("Selecionados: 0");
    }

    $scope.getMaxLength = function () {
        var tipo = $scope.comboTipoJogo;
        if (tipo == "DDZC" || tipo == "DGC" || tipo == "TDZC" || tipo == "TGC" ||
            tipo == "DDZ" || tipo == "DG" || tipo == "TDZ" || tipo == "TG" || 
            tipo == "MS" || tipo == "MC" || tipo == "C") {
            return 100;
        } else if (tipo == "MI" || tipo == "MCI") {
            return 4;
        } else if (tipo == "CI") {
            return 3;
        } else {
            return 2;
        }
    }

    $scope.mostrarPalpites = function (numero) {
        var tipo = $scope.comboTipoJogo;
        //if ((tipo == "DG" || tipo == "DDZ" || tipo == "TDZ" || tipo == "TG" || tipo == "PS" || tipo == "PC") && numero == 2) {
        if ((tipo == "PS" || tipo == "PC") && numero == 2) {
            return true;
            //} else if ((tipo == "TDZ" || tipo == "TG") && numero == 3) {
            //return true;
        } else {
            return false;
        }
    }

    $scope.limpaPalpites = function () {
        $scope.edtPalpite1 = '';
        $scope.edtPalpite2 = '';
        $scope.edtPalpite3 = '';

        $scope.comboDoPremio = "1";
        $scope.comboAoPremio = "undefined";
        if ($scope.comboTipoJogo == "TG" || $scope.comboTipoJogo == "TGC" ||
            $scope.comboTipoJogo == "TDZC" || $scope.comboTipoJogo == "TDZ" ||
            $scope.comboTipoJogo == "DDZC" || $scope.comboTipoJogo == "DGC" ||
            $scope.comboTipoJogo == "DDZ" || $scope.comboTipoJogo == "DG" ||
            $scope.comboTipoJogo == "PS" || $scope.comboTipoJogo == "PC") {
            $scope.comboDoPremio = "1";
            $scope.comboAoPremio = "5";
        }
    }

    // Remove aposta
    $scope.removerAposta = function (index) {
        $scope.apostas.splice(index, 1);
        $scope.getValorTotal();
    }

    $scope.getRetorno = function (jogo) {
        switch (jogo) {
            case "MS":
                return $scope.listaCfg[0].premio_milhar_seca;
            case "MC":
                return parseFloat($scope.listaCfg[0].premio_milhar_seca) + parseFloat($scope.listaCfg[0].premio_centena);
            case "MI":
                return $scope.listaCfg[0].premio_milhar_seca;
            case "MCI":
                return parseFloat($scope.listaCfg[0].premio_milhar_seca) + parseFloat($scope.listaCfg[0].premio_centena);
            case "C":
                return $scope.listaCfg[0].premio_centena;
            case "CI":
                return $scope.listaCfg[0].premio_centena;
            case "G":
                return $scope.listaCfg[0].premio_grupo;
            case "DG":
                return $scope.listaCfg[0].premio_duque_grupo;
            case "DGC":
                return $scope.listaCfg[0].premio_duque_grupo;
            case "TG":
                return $scope.listaCfg[0].premio_terno_grupo;
            case "TGC":
                return $scope.listaCfg[0].premio_terno_grupo;
            case "DZ":
                return $scope.listaCfg[0].premio_dezena;
            case "DDZ":
                return $scope.listaCfg[0].premio_duque_dezena;
            case "DDZC":
                return $scope.listaCfg[0].premio_duque_dezena;
            case "TDZ":
                return $scope.listaCfg[0].premio_terno_dezena;
            case "TDZC":
                return $scope.listaCfg[0].premio_terno_dezena;
            case "PS":
                return $scope.listaCfg[0].premio_passe_seco;
            case "PC":
                return $scope.listaCfg[0].premio_passe_combinado;
        }
    }

    $scope.getRetornoLimite = function (jogo) {
        switch (jogo) {
            case "MS":
                return $scope.listaLimite[0].milhar;
            case "MC":
                return $scope.listaLimite[0].milhar;
            case "MI":
                return $scope.listaLimite[0].milhar;
            case "MCI":
                return $scope.listaLimite[0].milhar;
            case "C":
                return $scope.listaLimite[0].centena;
            case "CI":
                return $scope.listaLimite[0].centena;
            case "G":
                return $scope.listaLimite[0].grupo;
            case "DG":
                return $scope.listaLimite[0].duque_grupo;
            case "DGC":
                return $scope.listaLimite[0].duque_grupo;
            case "TG":
                return $scope.listaLimite[0].terno_grupo;
            case "TGC":
                return $scope.listaLimite[0].terno_grupo;
            case "DZ":
                return $scope.listaLimite[0].dezena;
            case "DDZ":
                return $scope.listaLimite[0].duque_dezena;
            case "DDZC":
                return $scope.listaLimite[0].duque_dezena;
            case "TDZ":
                return $scope.listaLimite[0].terno_dezena;
            case "TDZC":
                return $scope.listaLimite[0].terno_dezena;
            case "PS":
                return 10000; //$scope.listaLimite[0].premio_passe_seco;
            case "PC":
                return 5000;//$scope.listaLimite[0].premio_passe_combinado;
        }
    }

    $scope.pad = function (n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    $scope.verificaSite = function(){
        if ($scope.site == 3333 || $scope.site == 4343 || $scope.site == 2536 || $scope.site == 2537 || $scope.site == 2538 || $scope.site == 1414
            || $scope.site == 2233 || $scope.site == 1015 ||  $scope.site == 2906 || $scope.site == 1122 || $scope.site == 5512 || $scope.site == 5507){
            return true;
        }else{
            return false;
        }
    }
    

    // Adiciona aposta
    $scope.adicionarAposta = function () {

        if (typeof $scope.listaLimite === "undefined") {
            alert('Informe a EXTRAÇÃO!');
            return;
        }

        if (typeof $scope.comboDoPremio === "undefined" || $scope.comboDoPremio === "undefined") {
            alert('Informe o DO PRÊMIO!');
            return;
        }

        if (typeof $scope.comboAoPremio === "undefined" || $scope.comboAoPremio === "undefined") {
            alert('Informe o AO PRÊMIO!');
            return;
        }        

        var intDoPremio = parseFloat($scope.comboDoPremio);
        var intAoPremio = parseFloat($scope.comboAoPremio);

        if (intAoPremio < intDoPremio) {
            alert('O prêmio final deverá ser igual ou maior ao prêmio inicial!');
            return;
        }

        if (typeof $scope.edtPalpite1 === "undefined" || $scope.edtPalpite1 == "") {
            alert('Informe o PALPITE 1!');
            document.getElementById("edtPalpite1").focus();
            return;
        }

        var valorNaQuadra = "";
        if (typeof $scope.edtNaQuadra != "undefined" && $scope.edtNaQuadra != "") {
            //valorNaQuadra = parseFloat($scope.edtNaQuadra);            

            if (!$scope.verificaApenasNumeros($scope.edtNaQuadra)) {
                alert('Apenas números são permitidos no valor da Quadra!');
                document.getElementById("edtNaQuadra").focus();
                return;
            } else {
                valorNaQuadra = $scope.edtNaQuadra;
            }

        }



        var valorAuxP1 = parseFloat($scope.edtPalpite1);
        var valorAuxP2 = parseFloat($scope.edtPalpite2);
        //var valorAuxP3 = parseFloat($scope.edtPalpite3);

        var tipo = $scope.comboTipoJogo;

        if (!$scope.verificaApenasNumeros($scope.edtPalpite1) && tipo != "TDZC" && tipo != "TGC" && tipo != "DDZC" && tipo != "DGC" &&
            tipo != "DG" && tipo != "DDZ" && tipo != "TDZ" && tipo != "TG" && tipo != "MS" && tipo != "MC" && tipo != "C") {
            alert("Apenas números são permitidos no palpite 1!");
            document.getElementById("edtPalpite1").focus();
            return;
        }

        /*if ((tipo == "PS" || tipo == "PC") &&
            (intDoPremio != 1 || intAoPremio != 5)
        ) {
            alert("Esse tipo de aposta só será permitido do 1º ao 5º prêmio.");
            return;
        }*/

        if ((tipo == "DDZ" || tipo == "DG" || tipo == "TDZ" || tipo == "TG" || tipo == "TDZC" || 
             tipo == "TGC" || tipo == "DDZC" || tipo == "DGC" || tipo == "PS" || tipo == "PC") &&
            (intDoPremio != 1 || intAoPremio != 5) && (user.getSite() != 2536 && user.getSite() != 4343 && user.getSite() != 1414)
        ) {
            alert("Esse tipo de aposta só será permitido do 1º ao 5º prêmio.");
            return;
        }

        /*if ((tipo == "MS" || tipo == "MC" || tipo == "MI" || tipo == "MCI") &&
            (intDoPremio == 7 || intAoPremio == 7)
        ) {
            alert("Milhares não podem ser apostadas no 7º prêmio !");
            document.getElementById("edtPalpite1").focus();
            return;
        }*/

        /*if (tipo == "DD" || tipo == "DG") {
            if (typeof $scope.edtPalpite2 === "undefined" || $scope.edtPalpite2 == "") {
                alert('Informe o PALPITE 2!');
                document.getElementById("edtPalpite2").focus();
                return;
            }

            if (isNaN(valorAuxP2)) {
                alert("Apenas números são permitidos no palpite 2!");
                document.getElementById("edtPalpite2").focus();
                return;
            }
        }*/

        try {

            if ((tipo == "MS" || tipo == "MC" || tipo == "MI" || tipo == "MCI") && $scope.edtPalpite1.length < 4) {
                alert('Informe ao menos 4 números no PALPITE 1');
                document.getElementById("edtPalpite1").focus();
                return;
            }

            if ((tipo == "C" || tipo == "CI") && $scope.edtPalpite1.length < 3) {
                alert('Informe ao menos 3 números no PALPITE 1');
                document.getElementById("edtPalpite1").focus();
                return;
            }

            if (tipo == "PS" || tipo == "PC") {
                if ($scope.edtPalpite1.length < 2) {
                    alert('Informe ao menos 2 números no PALPITE 1');
                    document.getElementById("edtPalpite1").focus();
                    return;
                } else if ($scope.edtPalpite2.length < 2) {
                    alert('Informe ao menos 2 números no PALPITE 2');
                    document.getElementById("edtPalpite2").focus();
                    return;
                }

                if (tipo == "PS" || tipo == "PC") {
                    if (valorAuxP1 < 1 || valorAuxP1 > 25) {
                        alert('Informe um GRUPO entre 1 a 25 no PALPITE 1!');
                        document.getElementById("edtPalpite1").focus();
                        return;
                    } else if ((valorAuxP2 < 1 || valorAuxP2 > 25) && (tipo != "G")) {
                        alert('Informe um GRUPO entre 1 a 25 no PALPITE 2!');
                        document.getElementById("edtPalpite2").focus();
                        return;
                    }
                }
            }

            if (tipo == "DG" || tipo == "DDZ" || tipo == "TDZ" || tipo == "TG" ||
                tipo == "TDZC" || tipo == "TGC" || tipo == "DDZC" || tipo == "DGC" || tipo == "G" ||
                tipo == "MS" || tipo == "MC" || tipo == "C") {
                $scope.edtPalpite1 = $scope.edtPalpite1.replace(/ /g, "");

                for (var x = 0; x < $scope.edtPalpite1.length; x++) {
                    if ("1234567890,".indexOf($scope.edtPalpite1.charAt(x)) < 0) {
                        alert('Apenas números e virgula são permitidos!');
                        return;
                    }
                }



                // $scope.edtPalpite1 = $scope.edtPalpite1.replace(/,/g, "-");
                var arrayPalpite = $scope.edtPalpite1.split(",");

                if ((arrayPalpite.length != 2) && (tipo == "DDZ" || tipo == "DG")) {
                    if (tipo == "DG") {
                        alert('Informe 2 grupos!');
                    } else {
                        alert('Informe 2 dezenas!');
                    }

                    document.getElementById("edtPalpite1").focus();
                    return;
                }

                if ((arrayPalpite.length != 3) && (tipo == "TDZ" || tipo == "TG")) {
                    if (tipo == "TG") {
                        alert('Informe 3 grupos!');
                    } else {
                        alert('Informe 3 dezenas!');
                    }

                    document.getElementById("edtPalpite1").focus();
                    return;
                }

                if ((arrayPalpite.length < 4) && (tipo == "TDZC" || tipo == "TGC")) {
                    if (tipo == "TGC") {
                        alert('Informe ao menos 4 grupos!');
                    } else {
                        alert('Informe ao menos 4 dezenas!');
                    }

                    document.getElementById("edtPalpite1").focus();
                    return;
                } else if ((arrayPalpite.length < 3) && (tipo == "DDZC" || tipo == "DGC")) {
                    if (tipo == "DGC") {
                        alert('Informe ao menos 3 grupos!');
                    } else {
                        alert('Informe ao menos 3 dezenas!');
                    }

                    document.getElementById("edtPalpite1").focus();
                    return;
                } else if (arrayPalpite.length > 10) {
                    alert('O limite de dezenas pro jogo combinado são 10!');
                    document.getElementById("edtPalpite1").focus();
                    return;
                }


                //$scope.edtPalpite1 = "";
                if (tipo != "MS" && tipo != "MC" && tipo != "C")
                for (x = 0; x < arrayPalpite.length; x++) {
                    if (arrayPalpite[x].length == 1) {
                        arrayPalpite[x] = "0" + arrayPalpite[x];
                    }
                    if (arrayPalpite[x].length > 2) {
                        alert('Apenas números de 2 digitos são permitidos!')
                        return;
                    }
                    if ((tipo == "TG" || tipo == "DG" || tipo == "TGC" || tipo == "DGC" || tipo == "G") && ((parseFloat(arrayPalpite[x]) < 1) || parseFloat(arrayPalpite[x]) > 25)) {
                        alert("Informe GRUPOS entre 1 a 25!");
                        return;
                    }
                    //$scope.edtPalpite1 = $scope.edtPalpite1 + arrayPalpite[x] + ","
                }

                $scope.edtPalpite1 = "";
                $scope.edtPalpite1 = arrayPalpite.join();

                /*arrayPalpite.forEach(element => {                    
                    if (element.length == 1){
                        alert("TTTTT: " + "0" + element);
                        element = "0" + element;
                        alert(element)
                    }
                });
                alert("Depois: " + arrayPalpite);
                */
            }

            if (typeof $scope.edtValor === "undefined" || $scope.edtValor == "" || $scope.edtValor == null) {
                alert('Informe o VALOR!');
                document.getElementById("edtValor").focus();
                return;
            }

            var valorAux = parseFloat($scope.edtValor.toString().replace(",", "."));

            if (valorAux <= 0) {
                alert("Informe o valor maior que zero!");
                document.getElementById("edtValor").focus();
                return;
            }

            var sNumeros = "";

            /*if (tipo == "TDZC" || tipo == "TGC" || tipo == "DDZC" || tipo == "DGC" ||
                tipo == "TDZ" || tipo == "TG" || tipo == "DDZ" || tipo == "DG") {
            */
            if (tipo != "PS" && tipo != "PC") {
           if (arrayPalpite.length > 1){

                $scope.edtPalpite1 = $scope.edtPalpite1.replace(/ /g, "");
                $scope.edtPalpite1 = $scope.edtPalpite1.replace(/,/g, "-");
            }
            }


            sNumeros = $scope.edtPalpite1;

            if (tipo == "PS" || tipo == "PC") {
                sNumeros = sNumeros + '-' + $scope.edtPalpite2
            }
            /*

            if (tipo == "TDZ" || tipo == "TG") {
                sNumeros = sNumeros + '-' + $scope.edtPalpite2 + '-' + $scope.edtPalpite3;
            }*/

            sNumeros = sNumeros.trim();

            var sNumerosAux = sNumeros;

            var forDe = 1;
            var forPara = 1;
            var divisaoDePara = 1;
            if (($scope.comboTipoJogo == "MS" || $scope.comboTipoJogo == "MC" || $scope.comboTipoJogo == "C") && (valorNaQuadra != "")) {
                forDe = parseFloat(sNumeros);

                var centenaOuMilha = 2;
                if ($scope.comboTipoJogo == "C") {
                    centenaOuMilha = 1;
                }


                forPara = parseFloat(sNumeros.substring(0, centenaOuMilha) + valorNaQuadra.toString())

                if (forPara < forDe) {
                    var creepAux = parseFloat(sNumeros.substring(0, centenaOuMilha)) + 1;
                    forPara = parseFloat(creepAux.toString() + valorNaQuadra.toString());
                }



                divisaoDePara = forPara - forDe + 1;

                sNumeros = "";

                /*alert(forDe);
                alert(forPara);
                alert('QTD MILHARES:' + divisaoDePara);
                return;
                */

            }



            for (var y = forDe; y <= forPara; y++) {
                if (($scope.comboTipoJogo == "MS" || $scope.comboTipoJogo == "MC" || $scope.comboTipoJogo == "C") && (valorNaQuadra != "")) {
                    if ($scope.comboTipoJogo == "C") {
                        sNumeros = sNumeros + $scope.pad(y.toString(), 3) + '-';
                    } else {
                        sNumeros = sNumeros + $scope.pad(y.toString(), 4) + '-';
                    }

                }
            }

            if (($scope.comboTipoJogo == "MS" || $scope.comboTipoJogo == "MC" || $scope.comboTipoJogo == "C") && (valorNaQuadra != "")) {
                sNumeros = sNumeros.substring(0, sNumeros.length - 1);
            }

            valorAux = parseFloat($scope.edtValor.toString().replace(",", "."));



            var tamanhoArray, i;

            tamanhoArray = $scope.apostas.length;


            var sTipo = "Bicho";
            var sValor = "R$ " + $scope.edtValor.toString();
            var sPremio = "";

            sPremio = $scope.comboDoPremio + "º ao " + $scope.comboAoPremio + "º Prêmio"
            /*if ($scope.comboPremios == '1'){
                sPremio = '1º Prêmio (Cabeça)';
            }else{
                sPremio = '1º ao 5º Prêmio';
            }*/


            /*
            RETIRADO A PEDIDOS
            for (i = 0; i < tamanhoArray; i++) {
                if ($scope.apostas[i].numeros == sNumeros && $scope.apostas[i].tipoJogo == tipo &&
                    $scope.apostas[i].premio == sPremio) {
                    alert("Aposta duplicada!");
                    return;
                }
            }*/

            var inversoes = 1;
            if (tipo == "MI" || tipo == "CI" || tipo == "MCI") {
                inversoes = $scope.getQtdInversoes(sNumeros);
                if (tipo == "MCI") {
                    inversoes = inversoes * 2;
                }

            }

            if (tipo == "TDZC" || tipo == "TGC") {
                inversoes = $scope.getQtdInversoesTernoCombinado(arrayPalpite);
            } else if (tipo == "DDZC" || tipo == "DGC") {
                inversoes = $scope.getQtdInversoesDuqueCombinado(arrayPalpite);
            }

            var iRetorno = ($scope.getRetorno(tipo) * valorAux) / inversoes;

            if ((tipo == "MC")) {
                iRetorno = ($scope.getRetorno('MS') * (valorAux / 2) / inversoes) +
                    ($scope.getRetorno('C') * (valorAux / 2) / inversoes);
            }

            if (($scope.comboTipoJogo == "MS" || $scope.comboTipoJogo == "MC" || $scope.comboTipoJogo == "C")) {
                if (valorNaQuadra != ""){
                    iRetorno = iRetorno / divisaoDePara;
                }else{
                    iRetorno = iRetorno / arrayPalpite.length;
                }
                                
            }


            var iDivisor = intAoPremio - intDoPremio + 1;

            if (!(tipo == "DDZ" || tipo == "DG" || tipo == "TDZ" || tipo == "TG" || tipo == "TDZC"
                || tipo == "TGC" || tipo == "DDZC" || tipo == "DGC" || tipo == "PS" || tipo == "PC")) {
                iRetorno = iRetorno / iDivisor;
            }

        /* try{
             console.log('Maximo: ' + $scope.listaCfg[0].premio_maximo);
             console.log('Retorno: ' + iRetorno);
            if (iRetorno > $scope.listaCfg[0].premio_maximo) {
                iRetorno = $scope.listaCfg[0].premio_maximo;
            }
         }catch{
                console.log("ERRO");
         }*/

            if ($scope.listaLimite.length > 0) {
                var iRetornoMaximo = $scope.getRetornoLimite(tipo);
                if (iRetorno > iRetornoMaximo && iRetornoMaximo > 0) {
                    iRetorno = iRetornoMaximo;
                }
            }

            var iPossivelRetornoReal = iRetorno;

            var numero = parseFloat(iRetorno).toFixed(2).split('.');
            numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
            iRetorno = numero.join(',');

            var objeto = {
                numeros: sNumeros,
                valor: sValor,
                valorReal: valorAux,
                premio: sPremio,
                premioDoReal: intDoPremio,
                premioAoReal: intAoPremio,
                tipo: sTipo,
                tipoJogo: tipo,
                possivelRetorno: iRetorno,
                possivelRetornoReal: iPossivelRetornoReal,
                inversoes: inversoes
            };

            $scope.apostas.push(objeto);


            $scope.getValorTotal();


            //$scope.edtPalpite1 = '';
            //$scope.edtPalpite2 = '';
            //$scope.edtPalpite3 = '';
        } finally {
            $scope.edtPalpite1 = '';
            document.getElementById("edtPalpite1").focus();
        }

    }

    $scope.getQtdInversoesTernoCombinado = function (arrayPalpite) {
        if (arrayPalpite.length == 3) {
            return 1;
        } else if (arrayPalpite.length == 4) {
            return 4;
        } else if (arrayPalpite.length == 5) {
            return 10;
        } else if (arrayPalpite.length == 6) {
            return 20;
        } else if (arrayPalpite.length == 7) {
            return 35;
        } else if (arrayPalpite.length == 8) {
            return 56;
        } else if (arrayPalpite.length == 9) {
            return 84;
        } else if (arrayPalpite.length == 10) {
            return 120;
        }
    }

    $scope.getQtdInversoesDuqueCombinado = function (arrayPalpite) {
        if (arrayPalpite.length == 2) {
            return 1;
        } else if (arrayPalpite.length == 3) {
            return 3;
        } else if (arrayPalpite.length == 4) {
            return 6;
        } else if (arrayPalpite.length == 5) {
            return 10;
        } else if (arrayPalpite.length == 6) {
            return 15;
        } else if (arrayPalpite.length == 7) {
            return 21;
        } else if (arrayPalpite.length == 8) {
            return 28;
        } else if (arrayPalpite.length == 9) {
            return 36;
        } else if (arrayPalpite.length == 10) {
            return 45;
        }
    }

    $scope.verificarInversoes = function () {
        var milhar = "123456";
        $scope.getQtdInversoesWILL(milhar);
    }

    $scope.verificaApenasNumeros = function (numero) {
        for (var i = 0; i < numero.length; i++) {
            if (isNaN(numero.substring(i, i + 1))) {
                return false;
            }
        }
        return true;
    }

    $scope.verificaPuleRepeticao = function () {
        if ($scope.listaApostas.length > 0) {
            $scope.apostas = [];            
            for (var x = 0; x < $scope.listaApostas.length; x++) {

                for (var z = 0; z < $scope.listaApostas[x].apostas.length; z++) {
                    var numero = parseFloat($scope.listaApostas[x].apostas[z].possivel_retorno).toFixed(2).split('.');
                    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
                    var iRetorno = numero.join(',');
                    var sPremio = "";

                    sPremio = $scope.listaApostas[x].apostas[z].do_premio + "º ao " + $scope.listaApostas[x].apostas[z].ao_premio + "º Prêmio";

                    var objeto = {
                        numeros: $scope.listaApostas[x].apostas[z].txt_aposta,
                        valor: "R$ " + $scope.listaApostas[x].apostas[z].valor_aposta,
                        valorReal: parseFloat($scope.listaApostas[x].apostas[z].valor_aposta.toString().replace(",", ".")),
                        premio: sPremio,
                        premioDoReal: $scope.listaApostas[x].apostas[z].do_premio,
                        premioAoReal: $scope.listaApostas[x].apostas[z].ao_premio,
                        tipo: 'Bicho',
                        tipoJogo: $scope.listaApostas[x].apostas[z].tipo_jogo_bicho,
                        possivelRetorno: iRetorno,
                        possivelRetornoReal: $scope.listaApostas[x].apostas[z].possivel_retorno,
                        inversoes: $scope.listaApostas[x].apostas[z].inversoes
                    };

                    $scope.apostas.push(objeto);
                }
            }

            $scope.getValorTotal();

        }
    }

    $scope.getQtdInversoes = function (milhar) {
        var valorMilhar = "";
        var valorCentena = "";
        var valorDezena = "";
        var qtdInversao = 0;
        var vetor = [];
        var valor = "";

        for (m = 0; m <= 3; m++) {
            valorMilhar = milhar.substring(m, m + 1);

            for (c = 0; c <= 3; c++) {

                if (c == m) {
                    continue;
                } else {
                    valorCentena = milhar.substring(c, c + 1)
                }

                for (d = 0; d <= 3; d++) {

                    if (d == c || d == m) {
                        continue;
                    } else {
                        valorDezena = milhar.substring(d, d + 1);
                    }

                    for (u = 0; u <= 3; u++) {

                        if (u == d || u == c || u == m) {
                            continue;
                        } else {
                            valor = valorMilhar + valorCentena + valorDezena + milhar.substring(u, u + 1);
                        }

                        if (!existeItemNoVetor(vetor, valor)) {
                            qtdInversao = qtdInversao + 1;
                            vetor.push(valor);
                            break;
                        }

                    }

                }

            }

        }
        return qtdInversao;
    }

    function existeItemNoVetor(vetor, valor) {
        var retorno = false;
        for (i = 0; i < vetor.length; i++) {
            if (valor == vetor[i]) {
                retorno = true;
                break;
            }
        }
        return retorno;
    }

    $scope.getJogos = function () {
        var data = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
        $http({
            method: "POST",
            url: "angularjs-mysql/jogos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + '&finalizados=false' + '&site=' + user.getSite() + '&status=L' + '&operacao=bicho' + '&data=' + data
        }).then(function (response) {
            $scope.jogos = []
            $scope.jogos = response.data;
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.getValorTotal = function () {
        var dValorTotal = 0;
        for (i = 0; i < $scope.apostas.length; i++) {
            dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
        }


        var numero = parseFloat(dValorTotal).toFixed(2).split('.');
        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
        $scope.valorTotal = numero.join(',');
    }

    $scope.getValorTotalReal = function () {
        var dValorTotal = 0;
        for (i = 0; i < $scope.apostas.length; i++) {
            dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
        }

        return dValorTotal;
    }


    function textoAleatorio() {
        var tamanho = 8;
        var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ';
        var aleatorio = '';
        for (var i = 0; i < tamanho; i++) {
            var rnum = Math.floor(Math.random() * letras.length);
            aleatorio += letras.substring(rnum, rnum + 1);
            if (i == 3) {
                aleatorio += '-';
            }
        }
        return aleatorio;
    }

    $scope.habilitar = function(){
        $('#btnGerarBilhete').removeAttr("disabled");        
    }

    $scope.desabilitar = function(){
        $('#btnGerarBilhete').attr("disabled", true);	         
    }

    $scope.gerarBilhete = function () {        

        try {

            if (typeof $scope.comboConcurso === "undefined") {
                alert('Informe a EXTRAÇÃO!');
                return;
            }
            if (typeof $scope.edtNome === "undefined") {
                alert('Informe o NOME!');
                return;
            }

            if ($scope.apostas.length == 0) {
                alert('Faça no mínimo uma aposta!');
                return;
            }

            if (user.getSaldoAtualizado() < $scope.getValorTotalReal()) {
                alert('Você não tem saldo para gerar o bilhete!');
                return;
            }

            var confirmacao = window.confirm("Confirma a geração do bilhete?");
            if (confirmacao) {
                var sTelefone = $scope.edtTelefone;
                if (typeof sTelefone === 'undefined') {
                    sTelefone = "";
                }
                var sPule = textoAleatorio();

                var data = new Date();

                var dia = data.getDate(); if (dia.toString().length == 1) { dia = '0' + dia };
                var mes = data.getMonth() + 1; if (mes.toString().length == 1) { mes = '0' + mes };
                var ano = data.getFullYear();

                var horas = data.getHours(); if (horas.toString().length == 1) { horas = '0' + horas };
                var minutos = data.getMinutes(); if (minutos.toString().length == 1) { minutos = '0' + minutos };
                var segundos = data.getSeconds(); if (segundos.toString().length == 1) { segundos = '0' + segundos };

                var dataAtual = ano + '-' + mes + '-' + dia + ' ' + horas + ':' + minutos + ':' + segundos;

                $scope.desabilitar();

                $http({
                    url: 'angularjs-mysql/salvarBilhete.php',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: "cod_usuario=" + user.getCodigo() +
                        "&cod_site=" + user.getSite() +
                        "&nome_usuario=" + user.getName() + 
                        "&cod_jogo=" + $scope.comboConcurso +
                        "&nome=" + $scope.edtNome +
                        "&telefone=" + sTelefone +
                        "&apostas=" + JSON.stringify($scope.apostas) +
                        "&pule=" + sPule +
                        "&data_atual=" + dataAtual +
                        "&numero_bilhete=" + $scope.listaCfg[0].valorMaxPule
                }).then(function (response) {
                    if (response.data.status == 'OK') {
                        user.atualizaSaldo(response.data.saldo);
                        user.setBilheteExterno(sPule);
                        $location.path("/conferir_bilhete");                        
                    } else {
                        alert(response.data.mensagem);
                    }
                    $scope.habilitar();
                }, function (response) {
                    console.log(response);
                    $scope.habilitar();
                    alert('Erro no $HTTP: ' + response.status)
                });

            }
        } finally {            
            
        }
    }
});
