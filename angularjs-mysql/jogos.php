<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$username = mysqli_real_escape_string($con, $_POST['username']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);

$query = 'SET @@session.time_zone = "-03:00"';
$result = mysqli_query($con, $query);


$query = "SELECT j.COD_JOGO, j.COD_SITE, j.COD_USUARIO, j.DATA_JOGO,
        j.TP_STATUS, j.TIPO_JOGO, j.CONCURSO AS COD_EXTRACAO, j.DESCRICAO, j.VALOR,
        CASE WHEN j.TIPO_JOGO IN ('B','2') THEN LPAD(j.NUMERO_1,4,'0') else j.NUMERO_1 end NUMERO_1,
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_2,4,'0') else j.NUMERO_2 end NUMERO_2,
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_3,4,'0') else j.NUMERO_3 end NUMERO_3,
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_4,4,'0') else j.NUMERO_4 end NUMERO_4,
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_5,4,'0') else j.NUMERO_5 end NUMERO_5,
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_6,4,'0') else j.NUMERO_6 end NUMERO_6,
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_7,4,'0') else j.NUMERO_7 end NUMERO_7, 
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_8,4,'0') else j.NUMERO_8 end NUMERO_8, 
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_9,4,'0') else j.NUMERO_9 end NUMERO_9, 
        CASE WHEN j.TIPO_JOGO = 'B' THEN LPAD(j.NUMERO_10,4,'0') else j.NUMERO_10 end NUMERO_10,
        NUMERO_11,
        NUMERO_12,
        NUMERO_13,
        NUMERO_14,
        NUMERO_15, 
        CAST(
            CASE
                WHEN j.TIPO_JOGO IN ('B','2') THEN 
                    concat(
                        DATE_FORMAT(j.DATA_JOGO, '%d/%m/%Y'), '-', TIME_FORMAT(j.HORA_EXTRACAO, '%H:%i'),
                        case when j.desc_hora is null then '' 
                        else concat('-', j.desc_hora) end
                        )
                WHEN j.TIPO_JOGO = 'R' THEN j.DESCRICAO 
                ELSE j.CONCURSO END AS CHAR
            ) AS CONCURSO, 
        TIME_FORMAT(j.HORA_EXTRACAO, '%H:%i')  HORA_EXTRACAO, 
        case when j.DESC_HORA is null then '' else j.DESC_HORA END DESC_HORA,
        CASE 
            WHEN j.TIPO_JOGO in ('B', '2') THEN e.AUTOMATIZADA
            WHEN j.TIPO_JOGO in ('Q', 'S', 'L') THEN 'S'
            WHEN j.TIPO_JOGO = 'R' THEN 'N'
            ELSE 'N'
        END AUTOMATIZADO,
        DATE_FORMAT(j.DATA_JOGO, '%d/%m/%Y') AS DATA_FORMATADA
        from jogo j
            left join extracao_bicho e on (j.concurso = e.cod_extracao)
        WHERE j.COD_SITE =  '$site'";

if ($finalizados == 'false') {
    $query = $query . " AND j.TP_STATUS <> 'F' ";
}

try {
    $status = mysqli_real_escape_string($con, $_POST['status']);
    

    if ($status != "") {
        $query = $query . " AND j.TP_STATUS = '$status'";
    }
} catch (Exception $e) {

}

try {
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);        

    $tipoJogo = "";
    switch ($operacao) {
        case "seninha":
            $tipoJogo = "S";
        break;
        case "supersena":
            $tipoJogo = "U";
        break;
        case "quininha":
            $tipoJogo = "Q";
        break;
        case "lotinha":
            $tipoJogo = "L";
        break;
        case "rifa":
            $tipoJogo = "R";
        break;
        case "bicho":
        case "2pra500":
            if ($operacao == "bicho") {
                $tipoJogo = "B";
            } else {
                $tipoJogo = "2";
            }    
            $query = $query . " AND ((j.DATA_JOGO = CURRENT_DATE AND j.HORA_EXTRACAO > CURRENT_TIME) OR (j.DATA_JOGO > CURRENT_DATE) ) ";
            $data = mysqli_real_escape_string($con, $_POST['data']);
            $query = $query . " and j.data_jogo = '$data' ";
        break;
        default:
        break;
    }

    if ($tipoJogo != "") {
        $query = $query . " AND j.TIPO_JOGO = '$tipoJogo' ";
    }

    if (isset($_POST['dataAtual'])) {
        $query = $query . " AND j.DATA_JOGO >= CURRENT_DATE ";
    }    

    $query = $query . " ORDER BY j.HORA_EXTRACAO, j.DATA_JOGO, j.CONCURSO ";
    
} catch (Exception $e) {

}

try {
    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_jogo'] = $row['COD_JOGO'];
        $row_array['data_jogo'] = $row['DATA_JOGO'];
        $row_array['tp_status'] = $row['TP_STATUS'];
        $row_array['tipo_jogo'] = $row['TIPO_JOGO'];
        $row_array['descricao'] = $row['DESCRICAO'];
        $row_array['valor'] = $row['VALOR'];
        switch ($row['TIPO_JOGO']) {
            case "2":
                $row_array['numeros_sorteados'] = $row['NUMERO_1'];
            break;
            case "R":
                $row_array['numeros_sorteados'] = $row['NUMERO_1'];
            break;
            case "Q":
                $row_array['numeros_sorteados'] =  
                $row['NUMERO_1'] . ' - ' . $row['NUMERO_2'] . ' - ' . $row['NUMERO_3'] . ' - ' .
                $row['NUMERO_4'] . ' - ' . $row['NUMERO_5'];
            break;
            case "U":
            case "S":
                $row_array['numeros_sorteados'] =  
                $row['NUMERO_1'] . ' - ' . $row['NUMERO_2'] . ' - ' . $row['NUMERO_3'] . ' - ' .
                $row['NUMERO_4'] . ' - ' . $row['NUMERO_5'] . ' - ' . $row['NUMERO_6'];
            break;
            case "B":
                $row_array['numeros_sorteados'] = 
                $row['NUMERO_1'] . ' - ' . $row['NUMERO_2'] . ' - ' . $row['NUMERO_3'] . ' - ' .
                $row['NUMERO_4'] . ' - ' . $row['NUMERO_5'] . ' - ' . $row['NUMERO_6'] . ' - ' . 
                $row['NUMERO_7'] . ' - ' . $row['NUMERO_8'] . ' - ' . $row['NUMERO_9'] . ' - ' . 
                $row['NUMERO_10'];
            break;
            case "L":
                $row_array['numeros_sorteados'] = 
                $row['NUMERO_1'] . ' - ' . $row['NUMERO_2'] . ' - ' . $row['NUMERO_3'] . ' - ' .
                $row['NUMERO_4'] . ' - ' . $row['NUMERO_5'] . ' - ' . $row['NUMERO_6'] . ' - ' . 
                $row['NUMERO_7'] . ' - ' . $row['NUMERO_8'] . ' - ' . $row['NUMERO_9'] . ' - ' . 
                $row['NUMERO_10']. ' - ' . $row['NUMERO_11']. ' - ' . $row['NUMERO_12']. ' - ' .
                $row['NUMERO_13']. ' - ' . $row['NUMERO_14']. ' - ' . $row['NUMERO_15'];
            break;
        }

        $row_array['numero_1'] = $row['NUMERO_1'];
        $row_array['numero_2'] = $row['NUMERO_2'];
        $row_array['numero_3'] = $row['NUMERO_3'];
        $row_array['numero_4'] = $row['NUMERO_4'];
        $row_array['numero_5'] = $row['NUMERO_5'];
        $row_array['numero_6'] = $row['NUMERO_6'];
        $row_array['numero_7'] = $row['NUMERO_7'];
        $row_array['numero_8'] = $row['NUMERO_8'];
        $row_array['numero_9'] = $row['NUMERO_9'];
        $row_array['numero_10'] = $row['NUMERO_10'];
        $row_array['numero_11'] = $row['NUMERO_11'];
        $row_array['numero_12'] = $row['NUMERO_12'];
        $row_array['numero_13'] = $row['NUMERO_13'];
        $row_array['numero_14'] = $row['NUMERO_14'];
        $row_array['numero_15'] = $row['NUMERO_15'];
        $row_array['concurso'] = $row['CONCURSO'];
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
        $row_array['desc_hora'] = $row['DESC_HORA'];
        $row_array['automatizado'] = $row['AUTOMATIZADO'];
        $row_array['selecionado'] = false;
        $row_array['data_formatada'] = $row['DATA_FORMATADA'];
        
        if ($contador == 1) {
            $row_array['selecionado'] = true;
        }

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }    

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);

} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = "Ocorreu o seguinte erro: " . $e->getMessage();
    
    echo json_encode($response);
}

$con->close();