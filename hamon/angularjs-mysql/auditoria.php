<?php

include "auditoria_aux.php";

function inserir_auditoria($con, $cod_usuario, $cod_site, $cod_operacao, $txt_descricao)
{
    date_default_timezone_set('Etc/GMT+3');
    $ip = getUserIp();
    $data = date('Y-m-d H:i:s');
    $stmt = $con->prepare(
        "INSERT INTO auditoria
             (COD_USUARIO, COD_SITE, COD_OPERACAO, DATA_AUDITORIA, TXT_DESCRICAO, ENDERECO_IP)
             VALUES (?, ?, ?, ?, ?, INET6_ATON(?))"
    );
    $stmt->bind_param("iiisss", $cod_usuario, $cod_site, $cod_operacao, $data, $txt_descricao, $ip);
    $stmt->execute();
    $stmt->close();
};

function getUserIp() {
    $ip = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = removePort($_SERVER['HTTP_CLIENT_IP']);
    } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = removePort($_SERVER['HTTP_X_FORWARDED_FOR']);
    } else if (!empty($_SERVER['HTTP_X_FORWARDED'])) {
        $ip = removePort($_SERVER['HTTP_X_FORWARDED']);
    } else if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        $ip = removePort($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']);
    } else if (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (!empty($_SERVER['REMOTE_ADDR'])) {
        $ip = removePort($_SERVER['REMOTE_ADDR']);
    }
    return $ip;
}

function removePort($host) {
    $ip = explode(":", $host)[0];
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        return $ip;
    } else {
        return "";
    }
}

const AUD_BILHETE_CRIADO = 1;
const AUD_BILHETE_CANCELADO = 2;
const AUD_BILHETE_PAGO = 3;
const AUD_BILHETE_VIRTUAL_VALIDADO = 4;

const AUD_USUARIO_CRIADO = 10;
const AUD_USUARIO_EDITADO = 11;
const AUD_USUARIO_ATIVADO = 12;
const AUD_USUARIO_DESATIVADO = 13;
const AUD_USUARIO_RESET_SENHA = 14;
const AUD_USUARIO_SENHA_EDITADA = 15;
const AUD_USUARIO_PREST_CONTA = 16;
const AUD_ADMIN_PREST_CONTA = 17;

const AUD_LANCAMENTO_CRIADO = 20;
const AUD_LANCAMENTO_EXCLUIDO = 21;

const AUD_EXTRACAO_CRIADA = 30;
const AUD_EXTRACAO_EDITADA = 31;
const AUD_EXTRACAO_EXCLUIDA = 32;

const AUD_CONFIG_SENINHA_CRIADA = 41;
const AUD_CONFIG_QUININHA_CRIADA = 42;
const AUD_CONFIG_BICHO_CRIADA = 43;
const AUD_CONFIG_2PRA500_CRIADA = 44;
const AUD_CONFIG_SENINHA_EDITADA = 45;
const AUD_CONFIG_QUININHA_EDITADA = 46;
const AUD_CONFIG_BICHO_EDITADA = 47;
const AUD_CONFIG_2PRA500_EDITADA = 48;
const AUD_CONFIG_GERAL_CRIADA = 49;
const AUD_CONFIG_GERAL_EDITADA = 50;

const AUD_LIMITE_BICHO_CRIADO = 51;
const AUD_LIMITE_BICHO_EDITADO = 52;
const AUD_LIMITE_BICHO_EXCLUIDO = 53;
const AUD_CONFIG_LOTINHA_CRIADA = 54;
const AUD_CONFIG_LOTINHA_EDITADA = 55;
const AUD_CONFIG_RIFA_CRIADA = 56;
const AUD_CONFIG_RIFA_EDITADA = 57;

const AUD_JOGO_CRIADO = 61;
const AUD_JOGO_EDITADO = 62;
const AUD_JOGO_PROCESSADO = 63;
const AUD_JOGO_FINALIZADO = 64;
const AUD_JOGO_REABERTO = 65;

const AUD_AREA_CRIADA = 71;
const AUD_AREA_ATIVADA = 72;
const AUD_AREA_DESATIVADA = 73;
const AUD_AREA_EDITADA = 74;
const AUD_CONFIG_MULTIJOGOS_EDITADA = 75;
const AUD_CONFIG_MULTIJOGOS_CRIADA = 76;

const AUD_CONFIG_SUPERSENA_CRIADA = 77;
const AUD_CONFIG_SUPERSENA_EDITADA = 78;