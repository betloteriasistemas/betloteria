angular.module('main').controller('gerenciarJogosCtrl', function ($scope, user, $http, $route, $filter, funcoes, configGeral) {
    $scope.nome = user.getNome();
    $scope.comboTipoJogo = "";
    $scope.chkExibirFinalizados = false;
    $scope.tiposJogos = funcoes.getTiposJogos();
    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
    });

    $scope.listarJogos = function () {
        $http({
            method: "POST",
            url: "angularjs-mysql/jogos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + user.getName() + 
                  '&finalizados=' + $scope.chkExibirFinalizados + 
                  '&site=' + user.getSite() + 
                  '&operacao=&status=' +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;
        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.getDescSimNao = function (valor) {
        if (valor === "S") {
            return "SIM";
        } else {
            return "NÃO";
        }
    };

    $scope.getExtracoes = function () {
        if ($scope.comboTipoJogo == "S" || $scope.comboTipoJogo == "Q" || 
            $scope.comboTipoJogo == "L" || $scope.comboTipoJogo == "R") {
            return;
        }
        $http({
            method: "POST",
            url: "angularjs-mysql/extracoes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                  '&operacao=listar' + 
                  '&tipo_jogo=' + $scope.comboTipoJogo +
                  '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.extracoes = response.data;
        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status)
        });

    }

    $scope.getDescricaoStatus = function (status) {
        if (status == 'L') {
            return 'Liberado';
        } else if (status == 'B') {
            return 'Bloqueado';
        } else if (status == 'P') {
            return 'Processado';
        } else if (status == 'F') {
            return 'Finalizado';
        }
    }


    $scope.getDescricaoExtracao = function (jogo) {
        switch (jogo.tipo_jogo) {
            case 'B':
            case '2':
                return ' - ' + jogo.hora_extracao + ' - ' + jogo.desc_hora;
            case 'R':
                return ' - ' + jogo.descricao
            default:
                return '';
        }
    }

    $scope.getDescricaoConcurso = function(jogo){
        switch (jogo.tipo_jogo) {
            case 'B':
            case 2:
                return jogo.hora_extracao + " - " + jogo.desc_hora;
            case 'S':
            case 'Q':
            case 'L':
            case 'U':
                return jogo.concurso;
            case 'R':
                return jogo.descricao;
            default:
                return "";
        }
    }


    $scope.getDescricaoTipoJogo = function (tipo_jogo) {
        
        switch (tipo_jogo) {
            case 'S':
                return "Seninha";
            case 'Q':
                return "Quininha";
            case 'L':
                return "Lotinha";
            case 'B':
                return "Bicho";
            case 2:
                return $scope.desc_2pra500;
            case 'R':
                return "Rifa";
            case 'U':
                return "Super Sena";
            default:
                return "";
        }
    }

    $scope.showInputPremio = function (premio) {
        switch (String($scope.tipo_jogo)) {
            case '2':
                return premio == 1;
            case 'R':
                return premio == 1;
            case 'Q':
                return premio >= 1 && premio <= 5;
            case 'S':
            case 'U':
                return premio >= 1 && premio <= 6;
            case 'L':
                return premio >= 1 && premio <= 15;
            case 'B':
                if ($scope.extracao && $scope.extracao.qtd_premios) {
                    return premio <= $scope.extracao.qtd_premios;
                }
                return premio >= 1 && premio <= 10;
            default: return true;
        }
    }

    $scope.alterar = function (jogo) {
        $scope.cod_jogo = jogo.cod_jogo;
        $scope.edtData = new Date($filter('date')(jogo.data_jogo, 'MM/dd/yyyy'));
        $scope.edtConcursoEdit = jogo.concurso;
        $scope.tipo_jogo = jogo.tipo_jogo;
        $scope.edtDescricao = jogo.descricao;
        $scope.edtValor = jogo.valor;
        if ($scope.tipo_jogo == 'B') {
            $http({
                method: "POST",
                url: "angularjs-mysql/extracoes.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'operacao=obter' + 
                      '&site=' + user.getSite() + 
                      '&cod_extracao=' + jogo.cod_extracao +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.extracao = response.data;
            }, function (response) {
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
        $scope.edtNumero1Edit = jogo.numero_1;
        $scope.edtNumero2Edit = jogo.numero_2;
        $scope.edtNumero3Edit = jogo.numero_3;
        $scope.edtNumero4Edit = jogo.numero_4;
        $scope.edtNumero5Edit = jogo.numero_5;
        $scope.edtNumero6Edit = jogo.numero_6;
        $scope.edtNumero7Edit = jogo.numero_7;
        $scope.edtNumero8Edit = jogo.numero_8;
        $scope.edtNumero9Edit = jogo.numero_9;
        $scope.edtNumero10Edit = jogo.numero_10;
        $scope.edtNumero11Edit = jogo.numero_11;
        $scope.edtNumero12Edit = jogo.numero_12;
        $scope.edtNumero13Edit = jogo.numero_13;
        $scope.edtNumero14Edit = jogo.numero_14;
        $scope.edtNumero15Edit = jogo.numero_15;
    }

    $scope.orderFilter = function (campo) {
        if ($scope.campoOrdenacao == "") {
            $scope.campoOrdenacao = campo;
        }

        if ($scope.campoOrdenacao == campo) {
            $scope.ordenacao = !$scope.ordenacao;
        } else {
            $scope.campoOrdenacao = campo;
            $scope.ordenacao = false;
        }

        var sAux = "";
        if (!$scope.ordenacao) {
            sAux = "-";
        }

        $scope.records = $filter('orderBy')($scope.records, sAux + campo);
    };

    $scope.bloquearJogo = function (codigo, status) {
        var stString = '';
        if (status == 'B') {
            stString = 'desbloqueio';
        } else {
            stString = 'bloqueio';
        }
        $.confirm({
            title: '',
            content:"Confirma o " + stString + " do jogo?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/salvarJogo.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() + 
                              '&cod_site=' + user.getSite() +
                              '&codigo=' + codigo + 
                              '&status=' + status + 
                              '&operacao=bloquearJogo' +
                              '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    }

    $scope.finalizarConcurso = function (codigo) {
        $.confirm({
            title: '',
            content:"Confirma a finalização do concurso?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/salvarJogo.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() + 
                              '&cod_site=' + user.getSite() +
                              '&codigo=' + codigo + 
                              '&operacao=finalizarConcurso' +
                              '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $.alert('Concurso finalizado com sucesso!');
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }, function (response) {
                        $.alert('Erro no $HTTP: ' + response.status)
                    })
                }
            }
        });
    }

    $scope.reabrirConcurso = function (codigo) {
        $.confirm({
            title: '',
            content:"Confirma reabertura do concurso?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/salvarJogo.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() + 
                              '&cod_site=' + user.getSite() +
                              '&codigo=' + codigo + 
                              '&operacao=reabrirConcurso' +
                              '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $.alert('Concurso reaberto com sucesso!');
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }, function (response) {
                        $.alert('Erro no $HTTP: ' + response.status)
                    })
                }
            }
        });
    }

    $scope.reabrirJogo = function (codigo) {
        $.confirm({
            title: '',
            content:"Confirma reabertura do jogo?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $http({
                        url: 'angularjs-mysql/salvarJogo.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'cod_usuario=' + user.getCodigo() + 
                              '&cod_site=' + user.getSite() +
                              '&codigo=' + codigo + 
                              '&operacao=reabrirJogo' +
                              '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $.alert('Jogo reaberto com sucesso!');
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }, function (response) {
                        $.alert('Erro no $HTTP: ' + response.status)
                    })
                }
            }
        });
    }

    $scope.exibirFinalizados = function () {
        $scope.chkExibirFinalizados = !$scope.chkExibirFinalizados;
        $scope.records = [];
        $scope.listarJogos();
    }

    $scope.filterDate = function (item) {
        var data_jogo = $filter('date')($scope.edtData, "yyyy-MM-dd");
        return (typeof data_jogo === 'undefined' || data_jogo == null || data_jogo == item.data_jogo);
    }


    $scope.gerarPdf = function (codigo, cod_site) {
        window.open("angularjs-mysql/gerarPdf.php?codigo=" + codigo + "&cod_site=" + cod_site + "&schema=" + user.getSchema());

    }

    $scope.salvarProcessar = function (codigo) {
        var data_jogo = $filter('date')($scope.edtData, "yyyy-MM-dd");
        var concurso = $scope.edtConcurso;
        var descricao = $scope.edtDescricao;
        var valor = $scope.edtValor;

        var cod_usuario = user.getCodigo();
        var cod_site = user.getSite();

        var numero1 = $scope.edtNumero1Edit || null;
        var numero2 = $scope.edtNumero2Edit || null;
        var numero3 = $scope.edtNumero3Edit || null;
        var numero4 = $scope.edtNumero4Edit || null;
        var numero5 = $scope.edtNumero5Edit || null;
        var numero6 = $scope.edtNumero6Edit || null;
        var numero7 = $scope.edtNumero7Edit || null;
        var numero8 = $scope.edtNumero8Edit || null;
        var numero9 = $scope.edtNumero9Edit || null;
        var numero10 = $scope.edtNumero10Edit || null;
        var numero11 = $scope.edtNumero11Edit || null;
        var numero12 = $scope.edtNumero12Edit || null;
        var numero13 = $scope.edtNumero13Edit || null;
        var numero14 = $scope.edtNumero14Edit || null;
        var numero15 = $scope.edtNumero15Edit || null;
        $operacao = "salvar";
        if (codigo != 0 && numero1 != null) {
            $operacao = $operacao + "Processar";
        }

        var hora_extracao = $scope.comboExtracao;
        var desc_hora = "";


        if (typeof codigo == "undefined" || codigo == 0) {
            codigo = 0;
            var tipo_jogo = $scope.comboTipoJogo;

            if (tipo_jogo == "") {
                $.alert('Informe o tipo de jogo!');
                return;
            }
            if (tipo_jogo == "B" || tipo_jogo == "2") {
                if (typeof hora_extracao == "undefined" || hora_extracao == "") {
                    $.alert("Informe a Hora da Extração!");
                    return;
                }
                var extracao = hora_extracao.split("-");
                concurso = extracao[0];
                hora_extracao = extracao[1] + ":00";
                desc_hora = extracao[2];
                if (extracao.length > 3) {
                    desc_hora += '-' + extracao[3]
                }
                
            }
        } else {
            var tipo_jogo = $scope.tipo_jogo;
        }

        if (codigo != 0 && tipo_jogo == "B") {
            for (x = 1; x <= $scope.extracao.qtd_premios; x++) {
                var texto = "";
                if (x == 1 && numero1 != null) {
                    texto = numero1.toString();
                } else if (x == 2 && numero2 != null) {
                    texto = numero2.toString();
                } else if (x == 3 && numero3 != null) {
                    texto = numero3.toString();
                } else if (x == 4 && numero4 != null) {
                    texto = numero4.toString();
                } else if (x == 5 && numero5 != null) {
                    texto = numero5.toString();
                } else if (x == 6 && numero6 != null) {
                    texto = numero6.toString();
                } else if (x == 7 && numero7 != null) {
                    texto = numero7.toString();
                } else if (x == 8 && numero8 != null) {
                    texto = numero8.toString();
                } else if (x == 9 && numero9 != null) {
                    texto = numero9.toString();
                } else if (x == 10 && numero10 != null) {
                    texto = numero10.toString();
                }

                if (isNaN(texto)) {
                    $.alert("O número " + x + " não é um número válido!");
                    return;
                }

                if ((texto.length != 4) && (texto.length != 0) && (texto != '0') && (parseInt(texto) != 0)) {
                    $.alert("O número " + x + " deverá possuir 4 digitos ou em branco!");
                    return;
                }
            }
        } else if (codigo != 0 && (tipo_jogo == "S" || tipo_jogo == "U")) {
            if (((numero1 < 1 || numero1 > 60) ||
                (numero2 < 1 || numero2 > 60) ||
                (numero3 < 1 || numero3 > 60) ||
                (numero4 < 1 || numero4 > 60) ||
                (numero5 < 1 || numero5 > 60) ||
                (numero6 < 1 || numero6 > 60))) {
                $.alert('Os números permitidos são entre 1 e 60!');
                return;
            }
        } else if (codigo != 0 && tipo_jogo == "Q") {
            if (
                (numero1 < 1 || numero1 > 80) ||
                (numero2 < 1 || numero2 > 80) ||
                (numero3 < 1 || numero3 > 80) ||
                (numero4 < 1 || numero4 > 80) ||
                (numero5 < 1 || numero5 > 80)
            ) {
                $.alert('Os números permitidos são entre 1 e 80!');
                return;
            }
        } else if (codigo != 0 && tipo_jogo == "L") {
            if (
                (numero1 < 1 || numero1 > 25) ||
                (numero2 < 1 || numero2 > 25) ||
                (numero3 < 1 || numero3 > 25) ||
                (numero4 < 1 || numero4 > 25) ||
                (numero5 < 1 || numero5 > 25) ||
                (numero6 < 1 || numero6 > 25) ||
                (numero7 < 1 || numero7 > 25) ||
                (numero8 < 1 || numero8 > 25) ||
                (numero9 < 1 || numero9 > 25) ||
                (numero10 < 1 || numero10 > 25) ||
                (numero11 < 1 || numero11 > 25) ||
                (numero12 < 1 || numero12 > 25) ||
                (numero13 < 1 || numero13 > 25) ||
                (numero14 < 1 || numero14 > 25) ||
                (numero15 < 1 || numero15 > 25)
            ) {
                $.alert('Os números permitidos são entre 1 e 25!');
                return;
            }
        } else if (codigo != 0 && tipo_jogo == "R") {
            if (numero1 < 0 || numero1 > 9999) {
                $.alert('Os números permitidos são entre 0 e 9999!');
                return;
            }
        }

        if (codigo != 0 && tipo_jogo != "B" && tipo_jogo != "2" && tipo_jogo != "R") {
            vetorAux = [];
            vetorAux[0] = numero1;
            vetorAux[1] = numero2;
            vetorAux[2] = numero3;
            vetorAux[3] = numero4;
            vetorAux[4] = numero5;
            if (tipo_jogo == "S") {
                vetorAux[5] = numero6;
            }
            if (tipo_jogo == "L") {
                vetorAux[6] = numero7;
                vetorAux[7] = numero8;
                vetorAux[8] = numero9;
                vetorAux[9] = numero10;
                vetorAux[10] = numero11;
                vetorAux[11] = numero12;
                vetorAux[12] = numero13;
                vetorAux[13] = numero14;
                vetorAux[14] = numero15;
            }

            for (x = 0; x < getQtdNumerosPorModalidade(tipo_jogo); x++) {
                for (y = x + 1; y < getQtdNumerosPorModalidade(tipo_jogo); y++) {
                    if (vetorAux[x] == vetorAux[y]) {
                        $.alert('Não é permitido números repetidos no resultado!');
                        return;
                    }
                }
            }
        }

        if (typeof data_jogo === "undefined" && (codigo == 0 || tipo_jogo == 'R')) {
            $.alert('Informe a DATA!');
        } else if ((typeof concurso === "undefined" || concurso == "") && codigo == 0 && tipo_jogo != 'B' && tipo_jogo != '2' && tipo_jogo != 'R') {
            $.alert('Informe o CONCURSO!')
        } else if ((typeof descricao === "undefined" || descricao == "") && codigo == 0 && tipo_jogo == 'R') {
            $.alert('Informe a DESCRIÇÃO!')
        } else if ((typeof valor === "undefined" || valor == "") && codigo == 0 && tipo_jogo == 'R') {
            $.alert('Informe o VALOR DA RIFA!')
        }
        else {
            if (codigo != 0 && numero1 != null) {
                $.confirm({
                    title: '',
                    content:"Confirma o processamento do resultado?",
                    buttons: {
                        cancelar: function() {},
                        ok: function () {
                            salvarJogo(data_jogo, concurso, descricao, valor, codigo, cod_usuario, cod_site, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9, numero10, numero11, numero12, numero13, numero14, numero15, tipo_jogo, hora_extracao, desc_hora);
                        }
                    }
                })
            } else {
                salvarJogo(data_jogo, concurso, descricao, valor, codigo, cod_usuario, cod_site, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9, numero10, numero11, numero12, numero13, numero14, numero15, tipo_jogo, hora_extracao, desc_hora);
            }
        }
    }

    function salvarJogo(data_jogo, concurso, descricao, valor, codigo, cod_usuario, cod_site, numero1, numero2, numero3, numero4, numero5, numero6, numero7, numero8, numero9, numero10, numero11, numero12, numero13, numero14, numero15, tipo_jogo, hora_extracao, desc_hora) {
        $http({
            url: 'angularjs-mysql/salvarJogo.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'data=' + data_jogo +
                '&concurso=' + concurso +
                '&descricao=' + descricao +
                '&valor=' + valor +
                '&codigo=' + codigo +
                '&cod_usuario=' + cod_usuario +
                '&cod_site=' + cod_site +
                '&numero1=' + numero1 +
                '&numero2=' + numero2 +
                '&numero3=' + numero3 +
                '&numero4=' + numero4 +
                '&numero5=' + numero5 +
                '&numero6=' + numero6 +
                '&numero7=' + numero7 +
                '&numero8=' + numero8 +
                '&numero9=' + numero9 +
                '&numero10=' + numero10 +
                '&numero11=' + numero11 +
                '&numero12=' + numero12 +
                '&numero13=' + numero13 +
                '&numero14=' + numero14 +
                '&numero15=' + numero15 +
                '&operacao=' + $operacao + '&tipo_jogo=' + tipo_jogo +
                '&hora_extracao=' + hora_extracao + '&desc_hora=' + desc_hora +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                if (codigo != 0) {
                    if (numero1 != null) {
                        $.alert('Todas as apostas foram processadas com sucesso!');
                    } else {
                        $.alert('Jogo atualizado com sucesso!');
                    }
                    $(function () {
                        $('#modalJogos').modal('toggle');
                        $('#modalJogos').on('hidden.bs.modal', function (e) {
                            $scope.listarJogos();
                        });
                    });
                } else {
                    $scope.listarJogos();
                    $.alert('Jogo criado com sucesso!');
                }
            } else {
                $.alert(response.data.mensagem);
            }
        }, function (response) {
            $.alert('Erro no $HTTP: ' + response.status);
        });
    }
    

    function getQtdNumerosPorModalidade(tipo_jogo) {
        switch (tipo_jogo) {
            case 'S':
            case 'U':
                return 6;
            case 'Q':
                return 5;
            case 'L':
                return 15;
            default:
                throw new Exception('Tipo de jogo não encontrado!');
        }
    }
});

