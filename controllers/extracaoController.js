angular.module('main').controller('extracaoCtrl', function ($scope, $http, configGeral, 
    user, $route, notificacoes, $rootScope) {
    $rootScope.loading = false;
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.comboBloqueio = "0";
    $scope.edtHabilitaCentena = false;
    $scope.desc_2pra500 = '2 PRA 500';
    configGeral.get().then(function (data) {
        $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
    });

    notificacoes.get().then(function (data) {
        $scope.notificacoes = data;
    });

    $scope.tiposJogos = [];
    configGeral.get().then(function (data) {
        if (data.flg_bicho) {
            $scope.tiposJogos.push({nome: 'Bicho', codigo: 'B'});
        }
        if (data.flg_2pra500) {
            $scope.tiposJogos.push({nome: $scope.desc_2pra500, codigo: '2'});
        }
    });

    $scope.listarExtracoes = function () {
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/extracoes.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'site=' + user.getSite() + 
                '&operacao=listar' +
                '&filtra_automatizadas=true' +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            $scope.records = response.data;
            $rootScope.loading = false;
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status);
        });
    }

    $scope.getDescSimNao = function (federal) {
        if (federal === "S") {
            return "SIM";
        } else {
            return "NÃO";
        }
    };

    $scope.getDescSN = function (diaMarcado) {
        if (diaMarcado == true) {
            return "S";
        } 
        return "N";
    };

    $scope.getDescTipoJogo = function (tipo_jogo) {
        if (tipo_jogo == 'B') {
            return 'BICHO';
        } else if (tipo_jogo == '2') {
            return $scope.desc_2pra500;
        }
    }


    $scope.alterar = function (extracao) {
        $scope.cod_extracao = extracao.cod_extracao;
        $("#edtHora").val(extracao.hora_extracao);
        $scope.edtDescricao = extracao.descricao;
        $scope.edtCheckDomingo = extracao.domingo === "S";
        $scope.edtCheckSegunda = extracao.segunda === "S";
        $scope.edtCheckTerca = extracao.terca === "S";
        $scope.edtCheckQuarta = extracao.quarta === "S";
        $scope.edtCheckQuinta = extracao.quinta === "S";
        $scope.edtCheckSexta = extracao.sexta === "S";
        $scope.edtCheckSabado = extracao.sabado === "S"; 
        $scope.edtHabilitaCentena = extracao.habilita_centena === "S";       
        $scope.comboTipoJogo = String(extracao.tipo_jogo);
        $("#comboAreas").val(extracao.cod_area);
        $scope.comboPremios = extracao.qtd_premios;
        $scope.edtMilhar = parseValorMonetario(extracao.milhar);
        $scope.edtCentena = parseValorMonetario(extracao.centena);
        $scope.edtGrupo = parseValorMonetario(extracao.grupo);
        $scope.edtDuqueGrupo = parseValorMonetario(extracao.duque_grupo);
        $scope.edtTernoGrupo = parseValorMonetario(extracao.terno_grupo);
        $scope.edtQuinaGrupo = parseValorMonetario(extracao.quina_grupo);
        $scope.edtDezena = parseValorMonetario(extracao.dezena);
        $scope.edtDuqueDezena = parseValorMonetario(extracao.duque_dezena);
        $scope.edtTernoDezena = parseValorMonetario(extracao.terno_dezena);
        $scope.comboBloqueio = extracao.prazo_bloqueio + "";
        $('html, body').animate({ scrollTop: 0 }, 'slow'); //slow, medium, fast
    }

    $scope.salvar = function (codigo) {
        var hora = $("#edtHora").val();
        var descricao = $scope.edtDescricao;        
        var domingo = $scope.edtCheckDomingo;
        var segunda = $scope.edtCheckSegunda;
        var terca = $scope.edtCheckTerca;
        var quarta = $scope.edtCheckQuarta;
        var quinta = $scope.edtCheckQuinta;
        var sexta = $scope.edtCheckSexta;
        var sabado = $scope.edtCheckSabado;
        var tipo_jogo = $scope.comboTipoJogo;
        var qtdPremios = $scope.comboPremios;
        var habilita_centena = $scope.edtHabilitaCentena;
        var milhar = unparseValorMonetario($scope.edtMilhar);
        var centena = unparseValorMonetario($scope.edtCentena);
        var grupo = unparseValorMonetario($scope.edtGrupo);
        var duque_grupo = unparseValorMonetario($scope.edtDuqueGrupo);
        var terno_grupo = unparseValorMonetario($scope.edtTernoGrupo);
        var quina_grupo = unparseValorMonetario($scope.edtQuinaGrupo);
        var dezena = unparseValorMonetario($scope.edtDezena);
        var duque_dezena = unparseValorMonetario($scope.edtDuqueDezena);
        var terno_dezena = unparseValorMonetario($scope.edtTernoDezena);
        var prazo_bloqueio = $scope.comboBloqueio;

        if (tipo_jogo == undefined || tipo_jogo == '') {
            $.alert("Informe o tipo de jogo!");
            return;
        }

        if (typeof codigo == undefined) {
            codigo = 0;
        }

        if (typeof descricao == undefined) {
            descricao = "";
        }

        domingo = $scope.getDescSN(domingo);
        segunda = $scope.getDescSN(segunda);
        terca = $scope.getDescSN(terca);
        quarta = $scope.getDescSN(quarta);
        quinta = $scope.getDescSN(quinta);
        sexta = $scope.getDescSN(sexta);
        sabado = $scope.getDescSN(sabado);
        if (tipo_jogo == '2') {
            habilita_centena = false;
        }
        habilita_centena = $scope.getDescSN(habilita_centena);

        $rootScope.loading = true;
        $http({

            url: 'angularjs-mysql/extracoes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'codigo=' + codigo +
                '&operacao=salvar' +
                '&cod_usuario=' + user.getCodigo() +
                '&site=' + user.getSite() +
                '&hora=' + hora +
                '&descricao=' + descricao +
                '&domingo=' + domingo +
                '&segunda=' + segunda +
                '&terca=' + terca +
                '&quarta=' + quarta +
                '&quinta=' + quinta +
                '&sexta=' + sexta +
                '&sabado=' + sabado +
                '&tipo_jogo=' + tipo_jogo +
                '&qtd_premios=' + qtdPremios +
                '&milhar=' + milhar + 
                '&centena=' + centena + 
                '&grupo=' + grupo + 
                '&duque_grupo=' + duque_grupo +
                '&terno_grupo=' + terno_grupo + 
                '&quina_grupo=' + quina_grupo + 
                '&dezena=' + dezena + 
                '&duque_dezena=' + duque_dezena +
                '&terno_dezena=' + terno_dezena +
                '&prazo_bloqueio=' + prazo_bloqueio +
                '&habilita_centena=' + habilita_centena +
                '&schema=' + user.getSchema()
        }).then(function (response) {
            if (response.data.status == 'OK') {
                $.alert('Extração salva com sucesso!');
                $route.reload();
                notificacoes.registerUpdateCallback();
                $rootScope.loading = false;
            } else {
                $rootScope.loading = false;
                $.alert(response.data.mensagem);
            }
        })

    }

    $scope.excluir = function (codigo, hora_extracao) {
        $.confirm({
            title: '',
            content:"Confirma a exclusão da extracao: " + hora_extracao + "?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $rootScope.loading = true;
                    $http({
                        url: 'angularjs-mysql/extracoes.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 'codigo=' + codigo +
                            '&cod_usuario=' + user.getCodigo() +
                            '&site=' + user.getSite() +
                            '&operacao=excluir' + 
                            '&schema=' + user.getSchema()
                    }).then(function (response) {
                        if (response.data.status == 'OK') {
                            $route.reload();
                            $rootScope.loading = false;
                        } else {
                            $rootScope.loading = false;
                            $.alert(response.data.mensagem);
                        }
                    })
                }
            }
        });
    } 

    parseValorMonetario = function (valor) {
        return valor ? parseFloat(valor).toFixed(2).replace('.', ',').replace(/\d(?=(\d{3})+\,)/g, '$&.') : 0;
    }

    unparseValorMonetario = function (valor) {
        return valor ? valor.replace(/\./g, '').replace(',', '.') : 0;
    }
});