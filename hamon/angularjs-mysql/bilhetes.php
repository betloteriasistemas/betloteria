<?php

include "conexao.php";
include "funcoes_auxiliares.php";
include_once 'bitly.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);



if (isset($_POST['todosUsuarios'])) {
    $todosUsuarios = mysqli_real_escape_string($con, $_POST['todosUsuarios']);
} else {
    $todosUsuarios = null;
}

$contador = 0;

if ($operacao == "listar") {

    $dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
    $dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);
    $index = mysqli_real_escape_string($con, $_POST['index']);
    $qtdElementos = mysqli_real_escape_string($con, $_POST['qtdElementos']);
    $sort = mysqli_real_escape_string($con, $_POST['sort']);
    $sortType = filter_var(mysqli_real_escape_string($con, $_POST['sortType']), FILTER_VALIDATE_BOOLEAN);
    $buscarTotalizadores = mysqli_real_escape_string($con, $_POST['buscarTotalizadores']);
    $codigoJogo = mysqli_real_escape_string($con, $_POST['concurso']);
    $statusBilhete = mysqli_real_escape_string($con, $_POST['statusBilhete']);
    $tipoJogo = mysqli_real_escape_string($con, $_POST['tipoJogo']);
    $usuarioResponsavel = mysqli_real_escape_string($con, $_POST['usuarioResponsavel']);
    $nomeApostador = mysqli_real_escape_string($con, $_POST['nomeApostador']);


    $totalizadores = "";

    $query = "
        select v.cod_bilhete, v.txt_pule, v.txt_pule_pai, v.cambista, v.gerente, v.nome_apostador, v.telefone_apostador,
        v.data_bilhete, v.concurso, v.tipo_jogo, v.tipo_jogo_dinamico, v.data_hora_bilhete, v.status_bilhete,
        sum(v.qtd_bilhetes) qtd_bilhetes, sum(v.total) total, sum(v.comissao) comissao, sum(v.possivel_retorno) possivel_retorno, v.status_pagamento,
        v.apostas_premiadas
        from (
            select bi.cod_bilhete, bi.txt_pule, bi.txt_pule_pai, bi.status_pagamento, usu.nome cambista, geren.nome gerente, bi.nome_apostador, bi.telefone_apostador,
            DATE_FORMAT(bi.data_bilhete, '%d-%m-%Y') data_bilhete, cast(case
            when jogo.tipo_jogo IN('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
            when jogo.tipo_jogo = 'R' then jogo.DESCRICAO
            else jogo.concurso end as char) as concurso , jogo.tipo_jogo,
            (case
                  when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf2pra500.VALOR_APOSTA), ' PRA ', FLOOR(conf2pra500.VALOR_ACUMULADO))
                  else '' end) as tipo_jogo_dinamico,
            DATE_FORMAT(bi.data_bilhete, '%d/%m/%Y %H:%i:%S') data_hora_bilhete,
            count(apo.cod_aposta) qtd_bilhetes,  round(sum(apo.valor_aposta),2) total,
            sum(apo.comissao) as comissao, sum(case when apo.status = 'G' then 1 else 0 end) as apostas_premiadas,
            round(sum(apo.valor_ganho), 2) possivel_retorno,
            case
            when bi.status_bilhete = 'A' then 'Aberto'
            when bi.status_bilhete = 'C' then 'Cancelado'
            when bi.status_bilhete = 'F' then 'Finalizado'
            when bi.status_bilhete = 'P' then 'Processado' end status_bilhete
            from bilhete bi
            inner join aposta apo on (bi.cod_bilhete = apo.cod_bilhete)
            inner join usuario usu on (bi.cod_usuario = usu.cod_usuario)
            inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
            inner join jogo on (jogo.cod_jogo = apo.cod_jogo)
            left join configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
            left join configuracao_2pra500 conf2pra500 on (usu.COD_AREA = conf2pra500.COD_AREA and usu.COD_SITE = conf2pra500.COD_SITE)
            where bi.cod_site = '$site'
            and ( (usu.cod_usuario = '$cod_usuario' or usu.cod_gerente = '$cod_usuario')  or (geren.cod_usuario = '$cod_usuario' or geren.cod_gerente = '$cod_usuario') ) ";
            
    if ($tipoJogo) {
        $query = $query . " AND jogo.tipo_jogo = '$tipoJogo' ";
    }

    if ($codigoJogo) {
        $query = $query . " AND jogo.cod_jogo = '$codigoJogo' ";
    }

    if ($usuarioResponsavel) {
        $query = $query . " AND (geren.nome = '$usuarioResponsavel' OR usu.nome = '$usuarioResponsavel') ";
    }

    if ($nomeApostador) {
        $query = $query . " AND LOWER(bi.nome_apostador) LIKE LOWER('$nomeApostador%') ";
    }

    if ($statusBilhete) {
        if ($statusBilhete == "G") {
            $query = $query . " AND apo.status = 'G' ";
        } else {
            $query = $query . " AND bi.status_bilhete = '$statusBilhete' ";
        }
    }

    if ($dataDe != '0') {
        $query = $query . " AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d') >= DATE_FORMAT('$dataDe', '%Y%m%d')  ";
    }

    if ($dataAte != '0') {
        $query = $query . " AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d') <= DATE_FORMAT('$dataAte', '%Y%m%d')  ";
    }

    if ($dataDe == 0 && $dataAte == 0) {
        $query = $query . " AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d')  >= DATE_FORMAT(current_date - 7, '%Y%m%d')
                           AND DATE_FORMAT(bi.data_bilhete, '%Y%m%d')  <= DATE_FORMAT(current_date + 1, '%Y%m%d') ";
    }



    $query = $query . " group by bi.cod_bilhete, bi.txt_pule, usu.nome, geren.nome, bi.nome_apostador, bi.telefone_apostador, bi.data_bilhete, jogo.concurso, jogo.tipo_jogo, apo.cod_aposta
            ) v group by v.cod_bilhete, v.txt_pule, v.cambista, v.gerente, v.nome_apostador, v.telefone_apostador,
                v.data_bilhete, v.concurso, v.tipo_jogo, v.data_hora_bilhete, v.status_bilhete ";

    if ($buscarTotalizadores == 1) {
        $totalizadores = "SELECT SUM(total) as total, " .
            "SUM(comissao) as comissao, " .
            "COUNT(1) as 'total_registros'" .
            " FROM ( " . $query . " ) t";
    }

    $asc_desc = $sortType ? " ASC" : " DESC";
    switch ($sort) {
        case "CODIGO":
            $query = $query . "ORDER BY txt_pule";
            break;
        case "GERENTE":
            $query = $query . "ORDER BY gerente";
            break;
        case "CAMBISTA":
            $query = $query . "ORDER BY cambista";
            break;
        case "CONCURSO":
            $query = $query . "ORDER BY concurso";
            break;
        case "TIPO_JOGO":
            $query = $query . "ORDER BY tipo_jogo";
            break;
        case "DATA":
            $query = $query . "ORDER BY STR_TO_DATE(data_hora_bilhete, '%d/%m/%Y %H:%i:%s')";
            break;
        case "APOSTADOR":
            $query = $query . "ORDER BY nome_apostador";
            break;
        case "TELEFONE":
            $query = $query . "ORDER BY telefone_apostador";
            break;
        case "QTD_APOSTAS":
            $query = $query . "ORDER BY qtd_bilhetes";
            break;
        case "TOTAL":
            $query = $query . "ORDER BY total";
            break;
        case "COMISSAO":
            $query = $query . "ORDER BY comissao";
            break;
        case "RETORNO":
            $query = $query . "ORDER BY possivel_retorno";
            break;
        case "STATUS_BILHETE":
            $query = $query . "ORDER BY status_bilhete";
            break;
        case "STATUS_PGTO":
            $query = $query . "ORDER BY case 
                        when status_pagamento = 'N' and possivel_retorno > 0 then 2
                        when status_pagamento = 'S'  then 1
                        else 0 end";
            break;
        default:
            $query = $query . "ORDER BY data_hora_bilhete, '%d/%m/%Y %H:%i:%s')";
            break;
    }

    $query = $query . $asc_desc . " LIMIT $index,$qtdElementos ";
    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_bilhete'] = $row['cod_bilhete'];
        $row_array['txt_pule'] = $row['txt_pule'];
        $row_array['txt_pule_para_visualizar'] = $row['txt_pule'];
        if ($row['txt_pule'] == $row['cod_bilhete']) {
            $row_array['txt_pule_para_visualizar'] = $row['txt_pule_pai'];
        }        
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_hora_bilhete'] = $row['data_hora_bilhete'];

        $row_array['qtd_bilhetes'] = $row['qtd_bilhetes'];
        $row_array['total'] = $row['total'];
        $row_array['comissao'] = $row['comissao'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['concurso'] = $row['concurso'];
        $row_array['status_bilhete'] = $row['status_bilhete'];
        $row_array['gerente'] = $row['gerente'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        $row_array['tipo_jogo_dinamico'] = $row['tipo_jogo_dinamico'];
        $row_array['status_pagamento'] = $row['status_pagamento'];
        $row_array['bilhete_premiado'] = $row['apostas_premiadas'] > 0;

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };
    if ($buscarTotalizadores == 1) {
        $resultado_totalizador = mysqli_query($con, $totalizadores);
        $row = mysqli_fetch_array($resultado_totalizador);
        $totalizadoresJSON{
            'totalizadores'} = $row;

        array_push($return_arr, $totalizadoresJSON);
    }
    echo json_encode($return_arr, JSON_NUMERIC_CHECK);
} else if ($operacao == "getCambistas") {

    if ($todosUsuarios == 'A') {
        $query = " select nome, cod_usuario from usuario
               where cod_site = '$site'
                 and cod_usuario <>  '$cod_usuario'
                 and status = 'A'
                order by nome ";
    } else {
        $query = " select nome, cod_usuario from usuario
            where cod_gerente = '$cod_usuario'
            and cod_site = '$site'
            and status = 'A'
            order by nome ";
    }

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['nome'] = $row['nome'];
        $row_array['cod_usuario'] = $row['cod_usuario'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);
} else if ($operacao == "listarApostas") {
    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);

    $query = " select j.tipo_jogo, a.txt_aposta, a.qtd_numeros, a.valor_aposta, 
                      a.possivel_retorno, a.retorno5, a.retorno4, a.retorno3, a.flg_sorte, a.valor_ganho,
                case
                when a.status = 'A' then 'Aberta'
                when a.status = 'G' then 'Ganho'
                when a.status = 'C' then 'Cancelado'
                when a.status = 'P' then 'Perdido' end as status
                from aposta a
                inner join jogo j on (a.cod_jogo = j.cod_jogo)
                where a.cod_bilhete = '$cod_bilhete'
                and a.cod_site = '$site' ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        $row_array['txt_aposta'] = $row['txt_aposta'];
        $row_array['qtd_numeros'] = $row['qtd_numeros'];
        $row_array['valor_aposta'] = $row['valor_aposta'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['retorno5'] = $row['retorno5'];
        $row_array['retorno4'] = $row['retorno4'];
        $row_array['retorno3'] = $row['retorno3'];
        $row_array['flg_sorte'] = $row['flg_sorte'];
        $row_array['valor_ganho'] = $row['valor_ganho'];
        $row_array['status'] = $row['status'];
        $row_array['tipo_jogo'] = $_POST['tipo_jogo'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);
} else if ($operacao == "conferirBilhete") {

    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);
    if (isset($_POST['numero_bilhete'])) {
        $numero_bilhete = mysqli_real_escape_string($con, $_POST['numero_bilhete']);
    } else {
        $numero_bilhete = -1;
    }

    $query = "  SELECT DATE_FORMAT(bil.data_bilhete, '%d/%m/%Y %H:%i:%S') data_bilhete,
                       DATE_FORMAT(bil.data_bilhete, '%Y-%m-%dT%H:%i:%S') data_bilhete_js,
                site.NOME_BANCA NOME_BANCA,
                site.cod_site,
                usu.cod_area,
                usu.nome cambista,
                bil.nome_apostador,
                bil.telefone_apostador,
                bil.txt_pule,
                bil.txt_pule_pai,
                case
                    when bil.status_bilhete = 'A' then 'Aberto'
                    when bil.status_bilhete = 'P' then 'Processado'
                    when bil.status_bilhete = 'C' then 'Cancelado'
                    when bil.status_bilhete = 'F' then 'Finalizado'
                    when bil.status_bilhete = 'V' then 'Pendente de Validação' end as status_bilhete,
                apo.qtd_numeros,
                apo.txt_aposta,
                apo.valor_aposta,
                apo.possivel_retorno,
                apo.flg_sorte,
                apo.retorno5,
                apo.retorno4,
                apo.retorno3,
                apo.valor_ganho,
                apo.do_premio,
                apo.ao_premio,
                apo.tipo_jogo tipo_jogo_bicho,
                apo.inversoes,
                bil.status_pagamento,
                bil.numero_bilhete as numero_bilhete,
                case
                    when apo.status = 'A' then 'Aberto'
                    when apo.status = 'G' then 'Ganho'
                    when apo.status = 'C' then 'Cancelado'
                    when apo.status = 'P' then 'Perdido' end as status,
                jogo.data_jogo,
                jogo.cod_jogo,
                cast(case
                  when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
                  when jogo.tipo_jogo = 'R' then jogo.descricao
                  else jogo.concurso end as char) as concurso,
                case
                  when jogo.tipo_jogo = 'S' then 'SENINHA'
                  when jogo.tipo_jogo = 'U' then 'SUPER SENA'
                  when jogo.tipo_jogo = 'Q' then 'QUININHA'
                  when jogo.tipo_jogo = 'L' then 'LOTINHA'
                  when jogo.tipo_jogo = 'B' then 'BICHO'
                  when jogo.tipo_jogo = '2' then '2 PRA 500'
                  when jogo.tipo_jogo = 'R' then 'RIFA' 
                  else ''
                  end tipo_jogo,
                jogo.tipo_jogo as cod_tipo_jogo,
                (case
                  when jogo.tipo_jogo = '2' then CONCAT(FLOOR(conf.VALOR_APOSTA), ' PRA ', FLOOR(conf.VALOR_ACUMULADO))
                  else '' end) as tipo_jogo_dinamico,
                LPAD(jogo.numero_1,4,'0') numero_1,
                LPAD(jogo.numero_2,4,'0') numero_2,
                LPAD(jogo.numero_3,4,'0') numero_3,
                LPAD(jogo.numero_4,4,'0') numero_4,
                LPAD(jogo.numero_5,4,'0') numero_5,
                LPAD(jogo.numero_6,4,'0') numero_6,
                LPAD(jogo.numero_7,4,'0') numero_7,
                LPAD(jogo.numero_8,4,'0') numero_8,
                LPAD(jogo.numero_9,4,'0') numero_9,
                LPAD(jogo.numero_10,4,'0') numero_10,
                LPAD(jogo.numero_11,4,'0') numero_11,
                LPAD(jogo.numero_12,4,'0') numero_12,
                LPAD(jogo.numero_13,4,'0') numero_13,
                LPAD(jogo.numero_14,4,'0') numero_14,
                LPAD(jogo.numero_15,4,'0') numero_15
                FROM bilhete bil
                inner join site on (site.cod_site = bil.cod_site)
                inner join aposta apo on (bil.cod_bilhete = apo.cod_bilhete)
                inner join jogo on (apo.cod_jogo = jogo.cod_jogo)
                inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                inner join configuracao_2pra500 conf on (usu.COD_AREA = conf.COD_AREA and usu.COD_SITE = conf.COD_SITE)
                where (bil.txt_pule = '$cod_bilhete' or bil.txt_pule_pai = '$cod_bilhete' or bil.numero_bilhete = '$numero_bilhete')
                and (bil.cod_site = '$site' or bil.cod_site = 9999) ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;
    $linkZap = "";

    $user_access_token = '08c3557fb3ae656c9261e901986c77b25ce9ae10';

    $bilhete_anterior = "";

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    
    while ($contador < mysqli_num_rows($result)) {
        $bilhete_anterior = $row['txt_pule'];
        $row_array['linkZap'] = '';

        $row_array['cod_site'] = $row['cod_site'];
        $row_array['cod_area'] = $row['cod_area'];
        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_bilhete_js'] = $row['data_bilhete_js'];
        $row_array['nome_banca'] = getNomeBanca($row['NOME_BANCA'], $row['cod_site'], $row['cod_area']);
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['status_bilhete'] = $row['status_bilhete'];
        $row_array['txt_pule'] = $row['txt_pule'];
        $row_array['txt_pule_pai'] = $row['txt_pule_pai'];
        $row_array['numero_bilhete'] = $row['numero_bilhete'];
        $row_array['data_jogo'] = $row['data_jogo'];
        $row_array['cod_jogo'] = $row['cod_jogo'];
        $row_array['concurso'] = $row['concurso'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];
        $row_array['tipo_jogo_dinamico'] = $row['tipo_jogo_dinamico'];
        $row_array['cod_tipo_jogo'] = $row['cod_tipo_jogo'];
        $row_array['status_pagamento'] = $row['status_pagamento']; 
        $tipoJogo = $row['tipo_jogo'];
        if ($tipoJogo == '2 PRA 500') {
            $row_array['valor_bilhete'] = $row['valor_aposta'];
        }

        switch ($tipoJogo) {
            case "2 PRA 500":
                $row_array['numeros_sorteados'] = $row['numero_1'];
            break;
            case "QUININHA":
                $row_array['numeros_sorteados'] = 
                substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                substr($row['numero_5'], strlen($row['numero_5']) - 2, 2);
            break;
            case "SENINHA":
            case "SUPER SENA":
                $row_array['numeros_sorteados'] = 
                substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                substr($row['numero_5'], strlen($row['numero_5']) - 2, 2) . ' - ' .
                substr($row['numero_6'], strlen($row['numero_6']) - 2, 2);
            break;
            case "BICHO":
                $row_array['numeros_sorteados'] = 
                $row['numero_1'] . ' - ' . $row['numero_2'] . ' - ' .
                $row['numero_3'] . ' - ' . $row['numero_4'] . ' - ' .
                $row['numero_5'] . ' - ' . $row['numero_6'] . ' - ' .
                $row['numero_7'] . ' - ' . $row['numero_8'] . ' - ' .
                $row['numero_9'] . ' - ' . $row['numero_10'];
            break;
            case "LOTINHA":
                $row_array['numeros_sorteados'] = 
                substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
                substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
                substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
                substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
                substr($row['numero_5'], strlen($row['numero_5']) - 2, 2) . ' - ' .
                substr($row['numero_6'], strlen($row['numero_6']) - 2, 2) . ' - ' .
                substr($row['numero_7'], strlen($row['numero_7']) - 2, 2) . ' - ' .
                substr($row['numero_8'], strlen($row['numero_8']) - 2, 2) . ' - ' .
                substr($row['numero_9'], strlen($row['numero_9']) - 2, 2) . ' - ' .
                substr($row['numero_10'], strlen($row['numero_10']) - 2, 2) . ' - ' .
                substr($row['numero_11'], strlen($row['numero_11']) - 2, 2) . ' - ' .
                substr($row['numero_12'], strlen($row['numero_12']) - 2, 2) . ' - ' .
                substr($row['numero_13'], strlen($row['numero_13']) - 2, 2) . ' - ' .
                substr($row['numero_14'], strlen($row['numero_14']) - 2, 2) . ' - ' .
                substr($row['numero_15'], strlen($row['numero_15']) - 2, 2);
            break;
            case "RIFA":
                $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 3, 3);
            break;
        }

        $row_array['apostas'] = array();

        $valorBilhete = 0;
        do {
            $row_array_2['qtd_numeros'] = $row['qtd_numeros'];
            $row_array_2['txt_aposta'] = $row['txt_aposta'];
            $row_array_2['valor_aposta'] = $row['valor_aposta'];
            $row_array_2['possivel_retorno'] = $row['possivel_retorno'];
            $row_array_2['flg_sorte'] = $row['flg_sorte'];
            $row_array_2['retorno5'] = $row['retorno5'];
            $row_array_2['retorno4'] = $row['retorno4'];
            $row_array_2['retorno3'] = $row['retorno3'];
            $row_array_2['valor_ganho'] = $row['valor_ganho'];
            $row_array_2['status'] = $row['status'];
            $row_array_2['do_premio'] = $row['do_premio'];
            $row_array_2['ao_premio'] = $row['ao_premio'];
            $row_array_2['tipo_jogo_bicho'] = $row['tipo_jogo_bicho'];
            $row_array_2['inversoes'] = $row['inversoes'];
            $row_array_2['concurso'] = $row['concurso'];
            $row_array_2['cod_jogo'] = $row['cod_jogo'];
            $row_array_2['data_jogo'] = $row['data_jogo'];
            $valorBilhete = $valorBilhete + $row['valor_aposta'];

            array_push($row_array['apostas'], $row_array_2);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $contador = $contador + 1;

            if ($contador == mysqli_num_rows($result)) {
                break;
            }
        } while ($bilhete_anterior == $row['txt_pule']);

        if ($tipoJogo != '2 PRA 500') {
            $row_array['valor_bilhete'] = $valorBilhete;
        }

        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);
}

$con->close();
