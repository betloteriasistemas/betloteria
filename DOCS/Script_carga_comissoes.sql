SET SQL_SAFE_UPDATES = 0;

update rolamaximamaior.aposta a set comissao = (select 
    case
    when jogo.Tipo_Jogo = 'S' THEN sum(apo.valor_aposta) * usu.pct_comissao_seninha  / 100
    when jogo.Tipo_Jogo = 'Q' THEN sum(apo.valor_aposta) * usu.pct_comissao_quininha / 100
    when jogo.Tipo_Jogo = '2' THEN sum(apo.valor_aposta) * usu.pct_comissao_2PRA500  / 100
    when jogo.Tipo_Jogo = '6' THEN sum(apo.valor_aposta) * usu.pct_comissao_6DASORTE / 100
    WHEN jogo.Tipo_Jogo in ('B','G') THEN
		case 
		when apo.TIPO_JOGO = 'MC' and conf.COMISSAO_MILHAR_CENTENA is not null and conf.COMISSAO_MILHAR_CENTENA > 0 then sum(apo.valor_aposta) * conf.COMISSAO_MILHAR_CENTENA / 100
		when apo.TIPO_JOGO = 'MS' and conf.COMISSAO_MILHAR_SECA is not null and conf.COMISSAO_MILHAR_SECA > 0 then sum(apo.valor_aposta) * conf.COMISSAO_MILHAR_SECA / 100
		when (apo.TIPO_JOGO = 'MI' or apo.TIPO_JOGO = 'MCI') and conf.COMISSAO_MILHAR_INVERTIDA is not null and conf.COMISSAO_MILHAR_INVERTIDA > 0  then sum(apo.valor_aposta) * conf.COMISSAO_MILHAR_INVERTIDA / 100
		when apo.TIPO_JOGO = 'C' and conf.COMISSAO_CENTENA is not null and conf.COMISSAO_CENTENA > 0  then sum(apo.valor_aposta) * conf.COMISSAO_CENTENA / 100
		when apo.TIPO_JOGO = 'CI' and conf.COMISSAO_CENTENA_INVERTIDA is not null and conf.COMISSAO_CENTENA_INVERTIDA > 0  then sum(apo.valor_aposta) * conf.COMISSAO_CENTENA_INVERTIDA / 100
		when apo.TIPO_JOGO = 'G' and conf.COMISSAO_GRUPO is not null and conf.COMISSAO_GRUPO > 0  then sum(apo.valor_aposta) * conf.COMISSAO_GRUPO / 100
		when (apo.TIPO_JOGO = 'DG' or apo.TIPO_JOGO = 'DGC') and conf.COMISSAO_DUQUE_GRUPO	 is not null and conf.COMISSAO_DUQUE_GRUPO > 0  then sum(apo.valor_aposta) * conf.COMISSAO_DUQUE_GRUPO / 100
		when (apo.TIPO_JOGO = 'TG' or apo.TIPO_JOGO = 'TGC' or apo.TIPO_JOGO = 'TG10') and conf.COMISSAO_TERNO_GRUPO	is not null and conf.COMISSAO_TERNO_GRUPO > 0  then sum(apo.valor_aposta) * conf.COMISSAO_TERNO_GRUPO / 100
		when apo.TIPO_JOGO = 'DZ' and conf.COMISSAO_DEZENA is not null and conf.COMISSAO_DEZENA > 0  then sum(apo.valor_aposta) * conf.COMISSAO_DEZENA / 100
		when (apo.TIPO_JOGO = 'DDZ' or apo.TIPO_JOGO = 'DDZC') and conf.COMISSAO_DUQUE_DEZENA is not null and conf.COMISSAO_DUQUE_DEZENA > 0  then sum(apo.valor_aposta) * conf.COMISSAO_DUQUE_DEZENA / 100
		when (apo.TIPO_JOGO = 'TDZ' or apo.TIPO_JOGO = 'TDZC') and conf.COMISSAO_TERNO_DEZENA is not null and conf.COMISSAO_TERNO_DEZENA > 0  then sum(apo.valor_aposta) * conf.COMISSAO_TERNO_DEZENA / 100
		when (apo.TIPO_JOGO = 'PS' or apo.TIPO_JOGO = 'PS12') and conf.COMISSAO_PASSE_SECO is not null and conf.COMISSAO_PASSE_SECO > 0  then sum(apo.valor_aposta) * conf.COMISSAO_PASSE_SECO / 100
		when apo.TIPO_JOGO = 'PC' and conf.COMISSAO_PASSE_COMBINADO is not null and conf.COMISSAO_PASSE_COMBINADO > 0  then sum(apo.valor_aposta) * conf.COMISSAO_PASSE_COMBINADO / 100
		else sum(apo.valor_aposta) * usu.pct_comissao_bicho / 100 end 
        end as comissao
    from rolamaximamaior.jogo
    inner join rolamaximamaior.aposta apo on (apo.cod_jogo = jogo.cod_jogo and apo.cod_site = jogo.cod_site)
    inner join rolamaximamaior.bilhete bil on (bil.cod_bilhete = apo.cod_bilhete and apo.cod_site = bil.cod_site)
    inner join rolamaximamaior.usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
    left join rolamaximamaior.configuracao_comissao_bicho conf on usu.cod_usuario = conf.cod_usuario  
    where bil.status_bilhete not in ('C','F')
	  and apo.cod_aposta = a.cod_aposta)
where a.status not in ('C');

SET SQL_SAFE_UPDATES = 1;