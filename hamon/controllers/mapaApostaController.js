angular.module('main').controller('mapaApostaCtrl', function ($rootScope, $scope, $filter, $http, $location, user, $route) {
  $rootScope.loading = false;
  $scope.dataInicial = new Date();
  $scope.dataFinal = new Date();
  $scope.comboArea;
  $scope.comboExtracao = {
    extracoes: [],
    extracaoSelecionada: ''
  };
  $scope.comboModalidade;
  $scope.comboPremio;
  $scope.bichos;
  $scope.dataRank;
  $scope.totalizador;

  $scope.comboModalidade = {
    opcoesModalidade: [
      {descricao: 'Milhar', valor: 'MS'},
      {descricao: 'Milhar Invertida', valor: 'MI'},
      {descricao: 'Milhar com Centena', valor: 'MC'},
      {descricao: 'Milhar com Centena Invertida', valor: 'MCI'},
      {descricao: 'Centena', valor: 'C'},
      {descricao: 'Centena Invertida', valor: 'CI'},
      {descricao: 'Grupo', valor: 'G'},
      {descricao: 'Dezena', valor: 'DZ'},
    ],
    opcaoSelecionada: {descricao: 'Milhar', valor: 'MS'} //This sets the default value of the select in the ui
    };

  $scope.ajustaDataFimPeriodo = function () {
    if($scope.dataInicial > $scope.dataFinal) {
        $scope.dataFinal = $scope.dataInicial;
    }
  }

  $scope.pesquisar = function() {
    if($scope.dataInicial > $scope.dataFinal) {
      $.alert('A data final do período deve ser maior ou igual a data inicial.');
      return;
    }

    let dataInicial  = $filter('date')($scope.dataInicial, "yyyy-MM-dd");
    let dataFinal = $filter('date')($scope.dataFinal, "yyyy-MM-dd");
    let aoPremio = $scope.comboPremio;
    $scope.dataRank = [];
    // getAllBicho();
    if(!dataInicial) {
      $.alert('Campo data inicial inválido')
      return;
      dataInicial = 0;
    }
    if(!$scope.comboExtracao.extracaoSelecionada) {
      $.alert('O campo extrações não pode ser vazio');
      return;
    }
    if(!dataFinal) {
      $.alert('Campo data final inválido')
      return;
      dataFinal = 0;
    }
    if(!aoPremio) {
      aoPremio = 10;
    }
    $rootScope.loading = true;
    
    $http(
      {
        method: "GET",
        url: "angularjs-mysql/mapaAposta.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        params: {
              'site':  user.getSite(), 
              'dataInicial':  dataInicial,
              'dataFinal': dataFinal,
              'modalidade': $scope.comboModalidade.opcaoSelecionada.valor,
              'codigoJogo': '',
              'codigoExtracao': $scope.comboExtracao.extracaoSelecionada.cod_extracao,
              'aoPremio': aoPremio,
              'schema': user.getSchema(),

        }
 
      }
    ).then(function(response) {
      $rootScope.loading = false;
      $scope.dataRank = rank(response.data.apostas, response.data.bichos);
    });
  }

  function rank(dataApostas, dataBicho) {

    $scope.todosPalpites = [];
    $scope.dataBicho = dataBicho;    

    dataApostas.map(objeto => {
        const palpites = objeto.aposta.split('-');
        palpites.map(palpite => {
          $scope.todosPalpites.push({
            palpite,
            valorAposta: objeto.valor_aposta/palpites.length,
            valorPossivelRetorno: objeto.possivel_retorno
          });
        });
    });
    switch($scope.comboModalidade.opcaoSelecionada.valor) {
      case 'G':
        return rankGrupo();
      case 'DZ':
        return rankDezena();
      case 'MS': 
      case 'MC':
      case 'MI':
      case 'MCI':
        return rankMilhar();
      case 'C':
      case 'CI':
        return rankCentena();     
      default:
        return rankGrupo();
    }

  }

  function rankTodas() {
    let rankBicho = rankGrupo();
    rankBicho = rankBicho.concat(rankDezena());
    rankBicho = rankBicho.concat(rankMilhar());
    rankBicho = rankBicho.concat(rankCentena());
    rankBicho = rankBicho.sort(function ordenar(a, b) {
      return (b.totalPossivelRetorno - a.totalPossivelRetorno);
    });
    return rankBicho
          .filter(obj => obj.totalAposta > 0 && obj.totalPossivelRetorno > 0)
          .map((obj, index) => {
            obj.index = index + 1;
            return obj;
          });
  }

  function rankGrupo() {
    let rankBicho = [];
    $scope.dataBicho.map(bicho => {
      let quantidadeJogos = 0;
      let totalAposta = 0;
      let totalPossivelRetorno = 0;
      $scope.todosPalpites.map(objeto => {
        if(bicho.codigo == parseInt(objeto.palpite)) {
          quantidadeJogos++;
          totalAposta += parseFloat(objeto.valorAposta);
          totalPossivelRetorno += parseFloat(objeto.valorPossivelRetorno);
        }
      })
      rankBicho.push({
        numeroBicho: String(bicho.codigo).padStart(2, '0'),
        bicho: bicho.descricao,
        quantidadeJogos,
        totalAposta,
        totalPossivelRetorno,
        modalidade: 'Grupo',
      });
    });

    rankBicho = rankBicho.sort(function ordenar(a, b) {
      return (b.totalPossivelRetorno - a.totalPossivelRetorno);
    });
    return rankBicho
          .filter(obj => obj.totalAposta > 0 && obj.totalPossivelRetorno > 0)
          .map((obj, index) => {
            obj.index = index + 1;
            return obj;
          });
  }

  function rankDezena() {
    let rankBicho = [];
    $scope.dataBicho.map(bicho => {
      let dezenas = new Array(bicho.numero_1, bicho.numero_2, bicho.numero_3, bicho.numero_4);

      dezenas.map(dezena => {
        let quantidadeJogos = 0;
        let totalAposta = 0;
        let totalPossivelRetorno = 0;
        
        $scope.todosPalpites.map(objeto => {
          if(dezena == parseInt(objeto.palpite)) {
            quantidadeJogos++;
            totalAposta += parseFloat(objeto.valorAposta);
            totalPossivelRetorno += parseFloat(objeto.valorPossivelRetorno);

          }
        })
        rankBicho.push({
                      bicho: bicho.descricao,
                      numeroBicho: String(dezena).padStart(2, '0'),
                      quantidadeJogos,
                      totalAposta,
                      totalPossivelRetorno,
                      modalidade: 'Dezena',
                    })
      });
    });
    rankBicho = rankBicho.filter(obj => obj.quantidadeJogos > 0 );

    rankBicho = rankBicho.sort(function ordenar(a, b) {
      return (b.totalPossivelRetorno - a.totalPossivelRetorno);
    }).map((obj, index) => {
      obj.index = index + 1;
      return obj;
    });
    return rankBicho;
  }

  function rankMilhar() {
    let rankBicho = [];
    Array.from(new Set($scope.todosPalpites.map(data => data.palpite))).map(milhar => {
      let quantidadeJogos = 0;
      let totalAposta = 0;
      let totalPossivelRetorno = 0;  
      let descricao;
      $scope.dataBicho.map(bicho => {
        let dezenas = new Array(bicho.numero_1, bicho.numero_2, bicho.numero_3, bicho.numero_4);
        dezenas.map(dezena => {
          $scope.todosPalpites.map(objeto => {
            if(objeto.palpite == milhar && parseInt(milhar.substring(2,4)) == dezena) {
              quantidadeJogos++;
              totalAposta += parseFloat(objeto.valorAposta);
              totalPossivelRetorno += parseFloat(objeto.valorPossivelRetorno);
              descricao = bicho.descricao;
            }
          });
        });
      
      });

      rankBicho.push({
        bicho: descricao,
        numeroBicho: milhar,
        quantidadeJogos,
        totalAposta,
        totalPossivelRetorno,
        modalidade: 'Milhar',
      });
      
    });
    rankBicho = rankBicho.sort(function ordenar(a, b) {
      return (b.totalPossivelRetorno - a.totalPossivelRetorno);
    }).map((obj, index) => {
      obj.index = index + 1;
      return obj;
    });
    
    return rankBicho;
  }

  function rankCentena() {
    let rankBicho = [];
    Array.from(new Set($scope.todosPalpites.map(data => data.palpite))).map(centena => {
      let quantidadeJogos = 0;
      let totalAposta = 0;
      let totalPossivelRetorno = 0;  
      $scope.dataBicho.map(bicho => {
        let dezenas = new Array(bicho.numero_1, bicho.numero_2, bicho.numero_3, bicho.numero_4);
        dezenas.map(dezena => {
          $scope.todosPalpites.map(objeto => {
            if(objeto.palpite == centena && parseInt(centena.substring(1,3)) == dezena) {
              quantidadeJogos++;
              totalAposta += parseFloat(objeto.valorAposta);
              totalPossivelRetorno += parseFloat(objeto.valorPossivelRetorno);
              descricao = bicho.descricao;
            }
          });
        });
      
      });

      rankBicho.push({
        bicho: descricao,
        numeroBicho: centena,
        quantidadeJogos,
        totalAposta,
        totalPossivelRetorno,
        modalidade: 'Centena',
      });
      
    });
    rankBicho = rankBicho.sort(function ordenar(a, b) {
      return (b.valorPossivelRetorno - a.valorPossivelRetorno);
    }).map((obj, index) => {
      obj.index = index + 1;
      return obj;
    });
    return rankBicho;
  }
  
  
  $scope.listarExtracoes = function () {
    $http({
        method: "POST",
        url: "angularjs-mysql/extracoes.php",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: 'site=' + user.getSite() + 
              '&operacao=listar' +
              '&schema=' + user.getSchema()
    }).then(function (response) {
        $scope.extracoes = response.data;
        $scope.comboExtracao.extracoes = response.data.map(obj => {
          obj.descricao = `${obj.hora_extracao} - ${obj.descricao}`;
          return obj;
        });

        $scope.comboExtracao.extracaoSelecionada = response.data[0];
    }, function (response) {
        console.log(response);
        $.alert('Erro no $HTTP: ' + response.status)
    });
  }

  $scope.verificaSite = function () {
    if (user.getSite() == 3333 || user.getSite() == 4343 || user.getSite() == 2536 || user.getSite() == 2537 || user.getSite() == 2538 || user.getSite() == 9900 || user.getSite() == 1414
        || user.getSite() == 2233 || user.getSite() == 1015 || user.getSite() == 2906 || user.getSite() == 1122 || user.getSite() == 5507 || user.getSite() == 2306
        || user.getSite() == 1200) {
        return true;
    } else {
        return false;
    }
}
});