angular.module('main').controller('resultadosCtrl',
    function ($rootScope, $scope, $http, user, $filter, $location, funcoes, configGeral) {
        $rootScope.loading = false;
        $scope.nome = user.getNome();
        $scope.perfil = user.getPerfil();
        $scope.comboTipoJogo = '';
        $scope.tiposJogos = funcoes.getTiposJogos();
        $scope.edtDataDe = new Date();
        $scope.edtDataAte = new Date();
        $scope.desc_2pra500 = '2 PRA 500';
        configGeral.get().then(function (data) {
            $scope.desc_2pra500 = parseInt(data.valor_aposta) + ' pra ' + parseInt(data.valor_acumulado);
        });

        $scope.verificaBotaoImprimir = function (x) {
            if (x.concurso != '') {
                return true;
            }
            return false;
        }

        $scope.getLinkMobile = function (cod_jogo) {
            var strAux = "com.mitsoftwar.appprinter://" + $location.absUrl() 
                + "/angularjs-mysql/resultadoCodificado.php?id=" + cod_jogo 
                + "&site=" + user.getSite() 
                + "&area=" + user.getArea()
                + "&schema=" + user.getSchema();

            strAux = strAux.replace('#!/resultados/', '');
            return strAux;
        }

        $scope.imprimir = function (cod_jogo) {
            var linka = $scope.getLinkMobile(cod_jogo);
            window.open(linka);
        }

        $scope.getDescricaoTipoJogo = function (x) {
            if (x.concurso != '') {
                return funcoes.getDescricaoTipoJogo(x.tipo_jogo, $scope.desc_2pra500);
            } else {
                return "";
            }
        }

        $scope.ajustaDataFimPeriodo = function () {
            if ($scope.edtDataDe > $scope.edtDataAte) {
                $scope.edtDataAte = $scope.edtDataDe;
            }
        }

        $scope.pesquisar = function () {
            if ($scope.edtDataDe > $scope.edtDataAte) {
                $.alert('A data final do período deve ser maior ou igual a data inicial.');
                return;
            }
            $scope.mostrarFooterTabela = false;
            var tipoJogo = $scope.comboTipoJogo;
            var dataDe = $filter('date')($scope.edtDataDe, "yyyy-MM-dd");
            var dataAte = $filter('date')($scope.edtDataAte, "yyyy-MM-dd");

            if (typeof tipoJogo === 'undefined') {
                tipoJogo = '';
            }

            if (typeof dataDe === 'undefined' || dataDe == null) {
                dataDe = '0';
            }
            if (typeof dataAte === 'undefined' || dataAte == null) {
                dataAte = '0';
            }
            $rootScope.loading = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/resultados.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() +
                    '&dataDe=' + dataDe + 
                    '&dataAte=' + dataAte + 
                    '&tipoJogo=' + $scope.comboTipoJogo +
                    '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.records = response.data;
            }).catch(function (err) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status);
            }).finally(function () {
                $rootScope.loading = false;
                $scope.mostrarFooterTabela = true;
                if ($scope.records.length == 0) {
                    $.alert('Nenhum resultado encontrado para os filtros selecionados!');
                }
            });
        }
    });