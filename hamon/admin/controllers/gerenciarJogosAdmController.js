angular.module('main').controller('gerenciarJogosAdmCtrl', function ($scope, user, $http) {
    $scope.nome = user.getNome();
    $scope.perfil = user.getPerfil();
    $scope.resultado = [];
    $scope.edtNumero = "";
    $scope.dataHoje = new Date();
    $scope.dia = $scope.dataHoje.getDay();

    var from = '2018-09-12'.split("-");
    var teste = new Date(from[0], from[2], from[1]);
    var diaT = teste.getDay();
    
    console.log(diaT);
    console.log('Hoje: ' + $scope.dia);

    $scope.bloquear = function (atras) {
        $http({
            url: 'angularjs-mysql/resultados.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=bloquear&atras='+atras
        }).then(function (response) {                        
            alert('Todos os concursos até hoje foram bloqueados!')            
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        })
    }

    $scope.carregarResultados = function () {
        $http({
            url: 'angularjs-mysql/resultados.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=getResultados'
        }).then(function (response) {
            console.log("carregar resultados");
            console.log(response);
                        
            if (response.data.status == "OK"){
                if (response.data.mensagem != ""){
                    alert("Concursos que não foi possivel obter o resultado: " + response.data.mensagem);
                }
                alert('Os resultados dos concursos até hoje foram carregados com sucesso!')
            }else{
                alert(response.data.mensagem);
            }
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        })
    }

    $scope.processarCodJogo9999 = function(){
        $http({
            url: 'angularjs-mysql/resultados.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=processarJogos9999'
        }).then(function (response) {
            console.log("JOGOS 9999");
            //console.log(response);
                        
            if (response.data.status == "OK"){
                alert('PROCESSAR 9999 OK')
            }else{
                alert(response.data.mensagem);
            }
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        })
    }

    $scope.processarResultados = function () {
        $http({
            url: 'angularjs-mysql/processarResultados.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            console.log("Processando");
            //console.log(response);
                        
            if (response.data.status == "OK"){
                if (response.data.mensagem != ""){
                    alert("Concursos que não foi possivel processar o resultado: " + response.data.mensagem);
                }
                alert('Todos os concursos com o status "B" foram processados!')
            }else{
                alert(response.data.mensagem);
            }
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        })
    }

    $scope.criarJogos = function () {        
        $http({
            url: 'angularjs-mysql/resultados.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=criarJogos'
        }).then(function (response) {
            console.log("Criando jogos");
            //console.log(response);
                        
            if (response.data.status == "OK"){
                alert('Todos os concursos foram criados até o próximo sábado!')
            }else{
                alert(response.data.mensagem);
            }
        }, function (response) {
            console.log(response);
            alert('Erro no $HTTP: ' + response.status)
        })
    }
});