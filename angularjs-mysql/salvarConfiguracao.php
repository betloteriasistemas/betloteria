<?php

include "conexao.php";

require_once('funcoes_auxiliares.php');
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
	die();
}

$response = [];

$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$cod_area = mysqli_real_escape_string($con, $_POST['cod_area']);
$desc_area = mysqli_real_escape_string($con, $_POST['desc_area']);

if ($site != 500) {

	try {

		$query = "";
		$auditoria = "";
		$auditoriaOp;
		$queryOldConfig = "";
		$stmtOldConfig = null;
		$oldConfig = null;

		if ($cod_area == 'undefined') {
			$queryArea = "SELECT COD_AREA
			FROM areas
			WHERE cod_site = '$site' AND status = 'A'";

			$resultArea = mysqli_query($con, $queryArea);
			$rowArea = mysqli_fetch_array($resultArea, MYSQLI_ASSOC);
			$cod_area = $rowArea['COD_AREA'];
		}

		$query = "";

		switch ($operacao) {
			case "geral":
				$FLG_SENINHA = mysqli_real_escape_string($con, $_POST['flg_seninha']);
				$FLG_QUININHA = mysqli_real_escape_string($con, $_POST['flg_quininha']);
				$FLG_LOTINHA = mysqli_real_escape_string($con, $_POST['flg_lotinha']);
				$FLG_BICHO = mysqli_real_escape_string($con, $_POST['flg_bicho']);
				$FLG_2PRA500 = mysqli_real_escape_string($con, $_POST['flg_2pra500']);
				$FLG_RIFA = mysqli_real_escape_string($con, $_POST['flg_rifa']);
				$FLG_MULTIJOGOS = mysqli_real_escape_string($con, $_POST['flg_multijogos']);
				$FLG_SUPERSENA = mysqli_real_escape_string($con, $_POST['flg_supersena']);
				$NU_BILHETE_GRANDE = mysqli_real_escape_string($con, $_POST['nu_bilhete_grande']);
				$NU_BILHETE_NEGRITO = mysqli_real_escape_string($con, $_POST['nu_bilhete_negrito']);
				$NU_BILHETE_SUBLINHADO = mysqli_real_escape_string($con, $_POST['nu_bilhete_sublinhado']);
				$MINUTOS_CANCELAMENTO = mysqli_real_escape_string($con, $_POST['minutos_cancelamento']);
				$VALOR_MIN_APOSTA = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['valor_min_aposta']), null);
				$VALOR_MAX_APOSTA = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['valor_max_aposta']), null);
				$PRESTAR_CONTA_AUTOMATIZADA = mysqli_real_escape_string($con, $_POST['prestar_conta_automatizada']);
				$NU_WHATSAPP = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['nu_whatsapp']), null);
				$MODALIDADE_PADRAO_BV = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['modalidade_padrao_bv']), null);
			
				$query = "SELECT codigo
						  FROM configuracao_geral
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";
				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['codigo']) ? $row['codigo'] : null;
				
				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO configuracao_geral
						(cod_site, cod_area, flg_bicho, flg_quininha, flg_lotinha, flg_seninha, flg_2pra500, flg_rifa, 
						flg_multijogos, flg_super_sena, minutos_cancelamento, valor_min_aposta, valor_max_aposta, 
						prestar_conta_automatizada, nu_bilhete_grande, nu_bilhete_negrito, nu_bilhete_sublinhado,
						nu_whatsapp, modalidade_padrao_bv)
						VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param(
						"iisssssssiddssssss",
						$site,
						$cod_area,
						$FLG_BICHO,
						$FLG_QUININHA,
						$FLG_LOTINHA,
						$FLG_SENINHA,
						$FLG_2PRA500,
						$FLG_RIFA,
						$FLG_MULTIJOGOS,
						$FLG_SUPERSENA,
						$MINUTOS_CANCELAMENTO,
						$VALOR_MIN_APOSTA,
						$VALOR_MAX_APOSTA,
						$PRESTAR_CONTA_AUTOMATIZADA,
						$NU_BILHETE_GRANDE,
						$NU_BILHETE_NEGRITO,
						$NU_BILHETE_SUBLINHADO,
						$NU_WHATSAPP,
						$MODALIDADE_PADRAO_BV
					);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_geral
						SET 
						flg_bicho=?, 
						flg_quininha=?,
						flg_lotinha=?, 
						flg_seninha=?, 
						flg_2pra500=?,
						flg_rifa=?,
						flg_multijogos=?,
						flg_super_sena=?,
						minutos_cancelamento=?,
						valor_min_aposta = ?,
						valor_max_aposta = ?,
						prestar_conta_automatizada = ?,
						nu_bilhete_grande = ?,
						nu_bilhete_negrito = ?,
						nu_bilhete_sublinhado = ?,
						nu_whatsapp = ?, 
						modalidade_padrao_bv = ?
						WHERE codigo=$cod_config");
					$stmt->bind_param(
						"ssssssssiddssssss",
						$FLG_BICHO,
						$FLG_QUININHA,
						$FLG_LOTINHA,
						$FLG_SENINHA,
						$FLG_2PRA500,
						$FLG_RIFA,
						$FLG_MULTIJOGOS,
						$FLG_SUPERSENA,
						$MINUTOS_CANCELAMENTO,
						$VALOR_MIN_APOSTA,
						$VALOR_MAX_APOSTA,
						$PRESTAR_CONTA_AUTOMATIZADA,
						$NU_BILHETE_GRANDE,
						$NU_BILHETE_NEGRITO,
						$NU_BILHETE_SUBLINHADO,
						$NU_WHATSAPP,
						$MODALIDADE_PADRAO_BV
					);
					$queryOldConfig = "SELECT * FROM configuracao_geral WHERE codigo = '$cod_config'";
				}
			break;
			case "seninha":
				$PREMIO_MAXIMO = mysqli_real_escape_string($con, $_POST['premio_maximo']);
				$PREMIO_14 = mysqli_real_escape_string($con, $_POST['premio_14']);
				$PREMIO_15 = mysqli_real_escape_string($con, $_POST['premio_15']);
				$PREMIO_16 = mysqli_real_escape_string($con, $_POST['premio_16']);
				$PREMIO_17 = mysqli_real_escape_string($con, $_POST['premio_17']);
				$PREMIO_18 = mysqli_real_escape_string($con, $_POST['premio_18']);
				$PREMIO_19 = mysqli_real_escape_string($con, $_POST['premio_19']);
				$PREMIO_20 = mysqli_real_escape_string($con, $_POST['premio_20']);
				$PREMIO_25 = mysqli_real_escape_string($con, $_POST['premio_25']);
				$PREMIO_30 = mysqli_real_escape_string($con, $_POST['premio_30']);
				$PREMIO_35 = mysqli_real_escape_string($con, $_POST['premio_35']);
				$PREMIO_40 = mysqli_real_escape_string($con, $_POST['premio_40']);
				$QUADRA_SORTE = mysqli_real_escape_string($con, $_POST['quadra_sorte']);
				$QUINA_SORTE = mysqli_real_escape_string($con, $_POST['quina_sorte']);
				$SENA_SORTE = mysqli_real_escape_string($con, $_POST['sena_sorte']);
				$HABILITA_SORTE = mysqli_real_escape_string($con, $_POST['habilita_sorte']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);

				$query = "SELECT COD_CONFIGURACAO
						  FROM configuracao
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";
				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['COD_CONFIGURACAO']) ? $row['COD_CONFIGURACAO'] : null;

				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao`(`COD_SITE`,`PREMIO_MAXIMO`,`PREMIO_14`,`PREMIO_15`,
											`PREMIO_16`,`PREMIO_17`,`PREMIO_18`,`PREMIO_19`,`PREMIO_20`,`PREMIO_25`,`PREMIO_30`,`PREMIO_35`,`PREMIO_40`,
											`FLG_SORTE`,`MSG_BILHETE`, `SENA_SORTE`, `QUINA_SORTE`, `QUADRA_SORTE`, `COD_AREA`)
										VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
					$stmt->bind_param("iddddddddddddssdddi",
						$site,
						$PREMIO_MAXIMO,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$HABILITA_SORTE,
						$MSG_BILHETE,
						$SENA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$cod_area
					);
				} else {
					$stmt = $con->prepare("UPDATE configuracao
								SET
								PREMIO_MAXIMO = ?,
								PREMIO_14 = ?,
								PREMIO_15 = ?,
								PREMIO_16 = ?,
								PREMIO_17 = ?,
								PREMIO_18 = ?,
								PREMIO_19 = ?,
								PREMIO_20 = ?,
								PREMIO_25 = ?,
								PREMIO_30 = ?,
								PREMIO_35 = ?,
								PREMIO_40 = ?,
								FLG_SORTE = ?,
								SENA_SORTE = ?,
								QUINA_SORTE = ?,
								QUADRA_SORTE = ?,
								MSG_BILHETE = ?
								WHERE cod_configuracao = '$cod_config'");
					$stmt->bind_param("ddddddddddddsddds",
						$PREMIO_MAXIMO,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$HABILITA_SORTE,
						$SENA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$MSG_BILHETE
					);
					$queryOldConfig = "SELECT * FROM configuracao WHERE cod_configuracao = '$cod_config'";
				}
			break;
			case "supersena":
				$PREMIO_MAXIMO = mysqli_real_escape_string($con, $_POST['premio_maximo']);
				$PREMIO_SENA = mysqli_real_escape_string($con, $_POST['premio_sena']);
				$PREMIO_QUINA = mysqli_real_escape_string($con, $_POST['premio_quina']);
				$PREMIO_QUADRA = mysqli_real_escape_string($con, $_POST['premio_quadra']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);

				$query = "SELECT COD_CONFIGURACAO
						  FROM configuracao_super_sena
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";
				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['COD_CONFIGURACAO']) ? $row['COD_CONFIGURACAO'] : null;

				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_super_sena`(`COD_SITE`,`PREMIO_MAXIMO`,`PREMIO_SENA`,
											`PREMIO_QUINA`,`PREMIO_QUADRA`,`MSG_BILHETE`,`COD_AREA`)
										VALUES (?, ?, ?, ?, ?, ?, ?) ");
					$stmt->bind_param("iddddsi",
						$site,
						$PREMIO_MAXIMO,
						$PREMIO_SENA,
						$PREMIO_QUINA,
						$PREMIO_QUADRA,
						$MSG_BILHETE,
						$cod_area
					);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_super_sena
								SET
								PREMIO_MAXIMO = ?,
								PREMIO_SENA = ?,
								PREMIO_QUINA = ?,
								PREMIO_QUADRA = ?,
								MSG_BILHETE = ?
								WHERE cod_configuracao = '$cod_config'");
					$stmt->bind_param("dddds",
						$PREMIO_MAXIMO,
						$PREMIO_SENA,
						$PREMIO_QUINA,
						$PREMIO_QUADRA,
						$MSG_BILHETE
					);
					$queryOldConfig = "SELECT * FROM configuracao_super_sena WHERE cod_configuracao = '$cod_config'";
				}
			break;			
			case "quininha":
				$PREMIO_MAXIMO = mysqli_real_escape_string($con, $_POST['premio_maximo']);
				$PREMIO_13 = mysqli_real_escape_string($con, $_POST['premio_13']);
				$PREMIO_14 = mysqli_real_escape_string($con, $_POST['premio_14']);
				$PREMIO_15 = mysqli_real_escape_string($con, $_POST['premio_15']);
				$PREMIO_16 = mysqli_real_escape_string($con, $_POST['premio_16']);
				$PREMIO_17 = mysqli_real_escape_string($con, $_POST['premio_17']);
				$PREMIO_18 = mysqli_real_escape_string($con, $_POST['premio_18']);
				$PREMIO_19 = mysqli_real_escape_string($con, $_POST['premio_19']);
				$PREMIO_20 = mysqli_real_escape_string($con, $_POST['premio_20']);
				$PREMIO_25 = mysqli_real_escape_string($con, $_POST['premio_25']);
				$PREMIO_30 = mysqli_real_escape_string($con, $_POST['premio_30']);
				$PREMIO_35 = mysqli_real_escape_string($con, $_POST['premio_35']);
				$PREMIO_40 = mysqli_real_escape_string($con, $_POST['premio_40']);
				$PREMIO_45 = mysqli_real_escape_string($con, $_POST['premio_45']);
				$TERNO_SORTE = mysqli_real_escape_string($con, $_POST['terno_sorte']);
				$QUADRA_SORTE = mysqli_real_escape_string($con, $_POST['quadra_sorte']);
				$QUINA_SORTE = mysqli_real_escape_string($con, $_POST['quina_sorte']);
				$HABILITA_SORTE = mysqli_real_escape_string($con, $_POST['habilita_sorte']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);
	
				$query = "SELECT COD_CONFIGURACAO
						  FROM configuracao_quininha
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";
				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['COD_CONFIGURACAO']) ? $row['COD_CONFIGURACAO'] : null;
	
				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_quininha`(`COD_SITE`,`PREMIO_MAXIMO`,`PREMIO_13`,
							`PREMIO_14`,`PREMIO_15`,`PREMIO_16`,`PREMIO_17`,`PREMIO_18`,`PREMIO_19`,`PREMIO_20`,`PREMIO_25`,`PREMIO_30`,`PREMIO_35`,`PREMIO_40`, `PREMIO_45`, 
							`MSG_BILHETE`,`FLG_SORTE`, `QUINA_SORTE`, `QUADRA_SORTE`, `TERNO_SORTE`, `COD_AREA`)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param(
						"iddddddddddddddssdddi",
						$site,
						$PREMIO_MAXIMO,
						$PREMIO_13,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$PREMIO_45,
						$MSG_BILHETE,
						$HABILITA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$TERNO_SORTE,
						$cod_area
					);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_quininha
										SET
										PREMIO_MAXIMO = ?,
										PREMIO_13 = ?,
										PREMIO_14 = ?,
										PREMIO_15 = ?,
										PREMIO_16 = ?,
										PREMIO_17 = ?,
										PREMIO_18 = ?,
										PREMIO_19 = ?,
										PREMIO_20 = ?,
										PREMIO_25 = ?,
										PREMIO_30 = ?,
										PREMIO_35 = ?,
										PREMIO_40 = ?,
										PREMIO_45 = ?,
										MSG_BILHETE = ?,
										FLG_SORTE = ?,
										QUINA_SORTE = ?,
										QUADRA_SORTE = ?,
										TERNO_SORTE = ?
										WHERE COD_CONFIGURACAO = '$cod_config'");
					$stmt->bind_param(
						"ddddddddddddddssddd",
						$PREMIO_MAXIMO,
						$PREMIO_13,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$PREMIO_45,
						$MSG_BILHETE,
						$HABILITA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$TERNO_SORTE
					);
					$queryOldConfig = "SELECT * FROM configuracao_quininha WHERE COD_CONFIGURACAO = '$cod_config'";
				}	
			break;
			case "lotinha":
				$PREMIO_MAXIMO = mysqli_real_escape_string($con, $_POST['premio_maximo']);
				$PREMIO_16 = mysqli_real_escape_string($con, $_POST['premio_16']);
				$PREMIO_17 = mysqli_real_escape_string($con, $_POST['premio_17']);
				$PREMIO_18 = mysqli_real_escape_string($con, $_POST['premio_18']);
				$PREMIO_19 = mysqli_real_escape_string($con, $_POST['premio_19']);
				$PREMIO_20 = mysqli_real_escape_string($con, $_POST['premio_20']);
				$PREMIO_21 = mysqli_real_escape_string($con, $_POST['premio_21']);
				$PREMIO_22 = mysqli_real_escape_string($con, $_POST['premio_22']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);	
				$query = "SELECT CODIGO
						  FROM configuracao_lotinha
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";
				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['CODIGO']) ? $row['CODIGO'] : null;
	
				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_lotinha`(`COD_SITE`,`PREMIO_MAXIMO`,
							`PREMIO_16`,`PREMIO_17`,`PREMIO_18`,`PREMIO_19`,`PREMIO_20`,`PREMIO_21`,`PREMIO_22`, 
							`MSG_BILHETE`, `COD_AREA`)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param(
						"iddddddddsi",
						$site,
						$PREMIO_MAXIMO,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_21,
						$PREMIO_22,
						$MSG_BILHETE,
						$cod_area
					);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_lotinha
										SET
										PREMIO_MAXIMO = ?,
										PREMIO_16 = ?,
										PREMIO_17 = ?,
										PREMIO_18 = ?,
										PREMIO_19 = ?,
										PREMIO_20 = ?,
										PREMIO_21 = ?,
										PREMIO_22 = ?,
										MSG_BILHETE = ?
										WHERE CODIGO = '$cod_config'");
					$stmt->bind_param(
						"dddddddds",
						$PREMIO_MAXIMO,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_21,
						$PREMIO_22,
						$MSG_BILHETE
					);
					$queryOldConfig = "SELECT * FROM configuracao_lotinha WHERE CODIGO = '$cod_config'";
				}
			break;
			case "bicho":
				$PREMIO_MILHAR_CENTENA    = mysqli_real_escape_string($con, $_POST['premio_milhar_centena']);
				$PREMIO_MILHAR_SECA       = mysqli_real_escape_string($con, $_POST['premio_milhar_seca']);
				$PREMIO_MILHAR_INVERTIDA  = mysqli_real_escape_string($con, $_POST['premio_milhar_invertida']);
				$PREMIO_CENTENA           = mysqli_real_escape_string($con, $_POST['premio_centena']);
				$PREMIO_CENTENA_INVERTIDA = mysqli_real_escape_string($con, $_POST['premio_centena_invertida']);
				$PREMIO_GRUPO             = mysqli_real_escape_string($con, $_POST['premio_grupo']);
				$PREMIO_DUQUE_GRUPO       = mysqli_real_escape_string($con, $_POST['premio_duque_grupo']);
				$PREMIO_DUQUE_GRUPO6      = mysqli_real_escape_string($con, $_POST['premio_duque_grupo6']);
				$PREMIO_TERNO_GRUPO       = mysqli_real_escape_string($con, $_POST['premio_terno_grupo']);
				$PREMIO_TERNO_GRUPO10     = mysqli_real_escape_string($con, $_POST['premio_terno_grupo10']);
				$PREMIO_TERNO_GRUPO6      = mysqli_real_escape_string($con, $_POST['premio_terno_grupo6']);
				$PREMIO_TERNO_SEQUENCIA   = mysqli_real_escape_string($con, $_POST['premio_terno_sequencia']);
				$PREMIO_TERNO_SOMA        = mysqli_real_escape_string($con, $_POST['premio_terno_soma']);
				$PREMIO_TERNO_ESPECIAL    = mysqli_real_escape_string($con, $_POST['premio_terno_especial']);
				$PREMIO_QUINA_GRUPO       = mysqli_real_escape_string($con, $_POST['premio_quina_grupo']);
				$PREMIO_DEZENA            = mysqli_real_escape_string($con, $_POST['premio_dezena']);
				$PREMIO_DUQUE_DEZENA      = mysqli_real_escape_string($con, $_POST['premio_duque_dezena']);
				$PREMIO_DUQUE_DEZENA6      = mysqli_real_escape_string($con, $_POST['premio_duque_dezena6']);
				$PREMIO_TERNO_DEZENA      = mysqli_real_escape_string($con, $_POST['premio_terno_dezena']);
				$PREMIO_TERNO_DEZENA10    = mysqli_real_escape_string($con, $_POST['premio_terno_dezena10']);
				$PREMIO_TERNO_DEZENA6     = mysqli_real_escape_string($con, $_POST['premio_terno_dezena6']);
				$PREMIO_PASSE_SECO        = mysqli_real_escape_string($con, $_POST['premio_passe_seco']);
				$PREMIO_PASSE_COMBINADO   = mysqli_real_escape_string($con, $_POST['premio_passe_combinado']);
				$USA_MILHAR_BRINDE             = mysqli_real_escape_string($con, $_POST['usa_milhar_brinde']);
				$VALOR_LIBERAR_MILHAR_BRINDE   = mysqli_real_escape_string($con, $_POST['valor_liberar_milhar_brinde']);
				$VALOR_RETORNO_MILHAR_BRINDE   = mysqli_real_escape_string($con, $_POST['valor_retorno_milhar_brinde']);
				$TIPOS_JOGOS_BICHO 			   = json_decode($_POST['tipos_jogos_bicho']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);

				$query = "SELECT CODIGO
						  FROM configuracao_bicho
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";

				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['CODIGO']) ? $row['CODIGO'] : null;

				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_bicho`(
											cod_site, 
											premio_milhar_centena, 
											premio_milhar_seca, 
											premio_milhar_invertida, 
											premio_centena,
											premio_centena_invertida, 
											premio_grupo, 
											premio_duque_grupo, 
											premio_duque_grupo6, 
											premio_terno_grupo, 
											premio_terno_grupo10, 
											premio_terno_grupo6,
											premio_terno_sequencia, 
											premio_terno_soma,
											premio_terno_especial,
											premio_quina_grupo,
											premio_dezena, 
											premio_duque_dezena,
											premio_duque_dezena6, 
											premio_terno_dezena, 
											premio_terno_dezena10, 
											premio_terno_dezena6, 
											premio_passe_seco, 
											premio_passe_combinado, 
											MSG_BILHETE, 
											USA_MILHAR_BRINDE, 
											VALOR_LIBERAR_MILHAR_BRINDE, 
											VALOR_RETORNO_MILHAR_BRINDE, 
											COD_AREA)
						values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->bind_param(
						"idddddddddddddddddddddssddi",
						$site,
						$PREMIO_MILHAR_CENTENA,
						$PREMIO_MILHAR_SECA,
						$PREMIO_MILHAR_INVERTIDA,
						$PREMIO_CENTENA,
						$PREMIO_CENTENA_INVERTIDA,
						$PREMIO_GRUPO,
						$PREMIO_DUQUE_GRUPO,
						$PREMIO_DUQUE_GRUPO6,
						$PREMIO_TERNO_GRUPO,
						$PREMIO_TERNO_GRUPO10,
						$PREMIO_TERNO_GRUPO6,
						$PREMIO_TERNO_SEQUENCIA,
						$PREMIO_TERNO_SOMA,
						$PREMIO_TERNO_ESPECIAL,
						$PREMIO_QUINA_GRUPO,
						$PREMIO_DEZENA,
						$PREMIO_DUQUE_DEZENA,
						$PREMIO_DUQUE_DEZENA6,
						$PREMIO_TERNO_DEZENA,
						$PREMIO_TERNO_DEZENA10,
						$PREMIO_TERNO_DEZENA6,
						$PREMIO_PASSE_SECO,
						$PREMIO_PASSE_COMBINADO,
						$MSG_BILHETE,
						$USA_MILHAR_BRINDE,
						$VALOR_LIBERAR_MILHAR_BRINDE,
						$VALOR_RETORNO_MILHAR_BRINDE,
						$cod_area
					);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_bicho
										SET
										PREMIO_MILHAR_CENTENA = ?,
										PREMIO_MILHAR_SECA = ?,
										PREMIO_MILHAR_INVERTIDA = ?,
										PREMIO_CENTENA = ?,
										PREMIO_CENTENA_INVERTIDA = ?,
										PREMIO_GRUPO = ?,
										PREMIO_DUQUE_GRUPO = ?,
										PREMIO_DUQUE_GRUPO6 = ?,
										PREMIO_TERNO_GRUPO = ?,
										PREMIO_TERNO_GRUPO10 = ?,
										PREMIO_TERNO_GRUPO6 = ?,
										PREMIO_TERNO_SEQUENCIA = ?,
										PREMIO_TERNO_SOMA = ?,
										PREMIO_TERNO_ESPECIAL = ?,
										PREMIO_QUINA_GRUPO = ?,
										PREMIO_DEZENA = ?,
										PREMIO_DUQUE_DEZENA = ?,
										PREMIO_DUQUE_DEZENA6 = ?,
										PREMIO_TERNO_DEZENA = ?,
										PREMIO_TERNO_DEZENA10 = ?,
										PREMIO_TERNO_DEZENA6 = ?,
										PREMIO_PASSE_SECO = ?,
										PREMIO_PASSE_COMBINADO = ?,
										MSG_BILHETE = ?,
										USA_MILHAR_BRINDE = ?,
										VALOR_LIBERAR_MILHAR_BRINDE = ?,
										VALOR_RETORNO_MILHAR_BRINDE = ?
										WHERE CODIGO = '$cod_config'");
					$stmt->bind_param("dddddddddddddddddddddddssdd",
						$PREMIO_MILHAR_CENTENA,
						$PREMIO_MILHAR_SECA,
						$PREMIO_MILHAR_INVERTIDA,
						$PREMIO_CENTENA,
						$PREMIO_CENTENA_INVERTIDA,
						$PREMIO_GRUPO,
						$PREMIO_DUQUE_GRUPO,
						$PREMIO_DUQUE_GRUPO6,
						$PREMIO_TERNO_GRUPO,
						$PREMIO_TERNO_GRUPO10,
						$PREMIO_TERNO_GRUPO6,
						$PREMIO_TERNO_SEQUENCIA,
						$PREMIO_TERNO_SOMA,
						$PREMIO_TERNO_ESPECIAL,
						$PREMIO_QUINA_GRUPO,
						$PREMIO_DEZENA,
						$PREMIO_DUQUE_DEZENA,
						$PREMIO_DUQUE_DEZENA6,
						$PREMIO_TERNO_DEZENA,
						$PREMIO_TERNO_DEZENA10,
						$PREMIO_TERNO_DEZENA6,
						$PREMIO_PASSE_SECO,
						$PREMIO_PASSE_COMBINADO,
						$MSG_BILHETE,
						$USA_MILHAR_BRINDE,
						$VALOR_LIBERAR_MILHAR_BRINDE,
						$VALOR_RETORNO_MILHAR_BRINDE
					);
					$queryOldConfig = "SELECT * FROM configuracao_bicho WHERE CODIGO = '$cod_config'";
				}
			break;
			case "2pra500":
				$VALOR_ACUMULADO = mysqli_real_escape_string($con, $_POST['valor_acumulado']);
				$VALOR_APOSTA = mysqli_real_escape_string($con, $_POST['valor_aposta']);
				$QTD_NUMEROS = mysqli_real_escape_string($con, $_POST['qtd_numeros']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);

				$query = "SELECT CODIGO
						  FROM configuracao_2pra500
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";

				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['CODIGO']) ? $row['CODIGO'] : null;
				
				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_2pra500`
										(cod_site, valor_acumulado, qtd_numeros, MSG_BILHETE, VALOR_APOSTA, COD_AREA)
                      					values(?, ?, ?, ?, ?, ?) ");
					$stmt->bind_param("idisdi", $site, $VALOR_ACUMULADO, $QTD_NUMEROS, $MSG_BILHETE, $VALOR_APOSTA, $cod_area);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_2pra500
										SET VALOR_ACUMULADO = ?,
										MSG_BILHETE = ?,
										VALOR_APOSTA = ?,
										QTD_NUMEROS = ?
										WHERE CODIGO = '$cod_config'");
					$stmt->bind_param("dsdi", $VALOR_ACUMULADO, $MSG_BILHETE, $VALOR_APOSTA, $QTD_NUMEROS);
					$queryOldConfig = "SELECT * FROM configuracao_2pra500 WHERE CODIGO = '$cod_config'";
				}
			break;
			case "rifa":
				$QTD_RIFAS = mysqli_real_escape_string($con, $_POST['qtd_rifas']);
				$MSG_BILHETE = mysqli_real_escape_string($con, $_POST['msg_bilhete']);

				$query = "SELECT CODIGO
						  FROM configuracao_rifa
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";

				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['CODIGO']) ? $row['CODIGO'] : null;
				
				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_rifa`
										(COD_SITE, COD_AREA, QTD_RIFAS, MSG_BILHETE)
                      					values(?, ?, ?, ?) ");
					$stmt->bind_param("iiis", $site, $cod_area, $QTD_RIFAS, $MSG_BILHETE);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_rifa
										SET QTD_RIFAS = ?, MSG_BILHETE = ?
										WHERE CODIGO = '$cod_config'");
					$stmt->bind_param("is", $QTD_RIFAS, $MSG_BILHETE);
					$queryOldConfig = "SELECT * FROM configuracao_rifa WHERE CODIGO = '$cod_config'";
				}
			break;
			case "multijogos":
				$VALOR_CARTAO_1 = mysqli_real_escape_string($con, $_POST['valor_cartao_1']);
				$VALOR_CARTAO_2 = mysqli_real_escape_string($con, $_POST['valor_cartao_2']);
				$VALOR_CARTAO_3 = mysqli_real_escape_string($con, $_POST['valor_cartao_3']);
				$VALOR_CARTAO_4 = mysqli_real_escape_string($con, $_POST['valor_cartao_4']);
				$VALOR_CARTAO_5 = mysqli_real_escape_string($con, $_POST['valor_cartao_5']);
				$VALOR_MINIMO = mysqli_real_escape_string($con, $_POST['valor_minimo']);
				$QTD_NUMEROS_SENINHA = mysqli_real_escape_string($con, $_POST['qtd_numeros_seninha']);
				$QTD_NUMEROS_QUININHA = mysqli_real_escape_string($con, $_POST['qtd_numeros_quininha']);
				$QTD_NUMEROS_LOTINHA = mysqli_real_escape_string($con, $_POST['qtd_numeros_lotinha']);
				$TIPOS_JOGOS_BICHO = mysqli_real_escape_string($con, $_POST['tipos_jogos_bicho']);
				$CONCURSOS_MARCADOS = mysqli_real_escape_string($con, $_POST['concursos_marcados']);

				$query = "SELECT CODIGO
						  FROM configuracao_multijogos
						  WHERE cod_site = '$site' AND cod_area = '$cod_area'";

				$result = mysqli_query($con, $query);
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$cod_config = isset($row['CODIGO']) ? $row['CODIGO'] : null;
				
				if ($cod_config == null) {
					$stmt = $con->prepare("INSERT INTO `configuracao_multijogos`
										(COD_SITE, COD_AREA, VALOR_CARTAO_1, VALOR_CARTAO_2, VALOR_CARTAO_3,
										 VALOR_CARTAO_4, VALOR_CARTAO_5, QTD_NUMEROS_SENINHA, QTD_NUMEROS_QUININHA,
										 QTD_NUMEROS_LOTINHA, TIPOS_JOGOS_BICHO, VALOR_MINIMO_APOSTA, CONCURSOS_MARCADOS)
                      					values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
					$stmt->bind_param("iiiiiiissssds", $site, $cod_area, $VALOR_CARTAO_1, $VALOR_CARTAO_2,
										$VALOR_CARTAO_3, $VALOR_CARTAO_4, $VALOR_CARTAO_5, $QTD_NUMEROS_SENINHA,
										$QTD_NUMEROS_QUININHA, $QTD_NUMEROS_LOTINHA, $TIPOS_JOGOS_BICHO,
										$VALOR_MINIMO, $CONCURSOS_MARCADOS);
				} else {
					$stmt = $con->prepare("UPDATE configuracao_multijogos 
										SET VALOR_CARTAO_1 = ?,
											VALOR_CARTAO_2 = ?,
											VALOR_CARTAO_3 = ?,
											VALOR_CARTAO_4 = ?,
											VALOR_CARTAO_5 = ?,
											QTD_NUMEROS_SENINHA = ?,
											QTD_NUMEROS_QUININHA = ?,
											QTD_NUMEROS_LOTINHA = ?,
											TIPOS_JOGOS_BICHO = ?,
											VALOR_MINIMO_APOSTA = ?,
											CONCURSOS_MARCADOS = ?
										WHERE CODIGO = '$cod_config'");
					$stmt->bind_param("iiiiissssds", $VALOR_CARTAO_1, $VALOR_CARTAO_2,
										$VALOR_CARTAO_3, $VALOR_CARTAO_4, $VALOR_CARTAO_5, $QTD_NUMEROS_SENINHA,
										$QTD_NUMEROS_QUININHA, $QTD_NUMEROS_LOTINHA, $TIPOS_JOGOS_BICHO,
										$VALOR_MINIMO, $CONCURSOS_MARCADOS);
					$queryOldConfig = "SELECT * FROM configuracao_multijogos WHERE CODIGO = '$cod_config'";
				}
			break;			
			default:
			break;
		}

		if (!empty($queryOldConfig)) {
			$stmtOldConfig = $con->prepare($queryOldConfig);
			$stmtOldConfig->execute();
			$result = $stmtOldConfig->get_result();
			$oldConfig = $result->fetch_assoc();
			$stmtOldConfig->close();
		}

		switch ($operacao) {
			case "geral":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoGeral(
						$oldConfig,
						$desc_area,
						$FLG_SENINHA,
						$FLG_2PRA500,
						$FLG_QUININHA,
						$FLG_LOTINHA,
						$FLG_BICHO,
						$FLG_RIFA,
						$MINUTOS_CANCELAMENTO,
						$VALOR_MIN_APOSTA,
						$VALOR_MAX_APOSTA,
						$NU_BILHETE_GRANDE,
						$NU_BILHETE_NEGRITO,
						$NU_BILHETE_SUBLINHADO
					);
					$auditoriaOp = AUD_CONFIG_GERAL_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoGeral(
						$desc_area,
						$FLG_SENINHA,
						$FLG_2PRA500,
						$FLG_QUININHA,
						$FLG_LOTINHA,
						$FLG_BICHO,
						$FLG_RIFA,
						$MINUTOS_CANCELAMENTO,
						$VALOR_MIN_APOSTA,
						$VALOR_MAX_APOSTA,
						$NU_BILHETE_GRANDE,
						$NU_BILHETE_NEGRITO,
						$NU_BILHETE_SUBLINHADO
					);
					$auditoriaOp = AUD_CONFIG_GERAL_CRIADA;
				}
			break;
			case "seninha":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoSeninha(
						$oldConfig,
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$HABILITA_SORTE,
						$SENA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$MSG_BILHETE
					);
					$auditoriaOp = AUD_CONFIG_SENINHA_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoSeninha(
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$HABILITA_SORTE,
						$SENA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$MSG_BILHETE
					);
					$auditoriaOp = AUD_CONFIG_SENINHA_CRIADA;
				}
			break;
			case "supersena":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoSuperSena(
						$oldConfig,
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_SENA,
						$PREMIO_QUINA,
						$PREMIO_QUADRA,
						$MSG_BILHETE
					);
					$auditoriaOp = AUD_CONFIG_SUPERSENA_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoSuperSena(
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_SENA,
						$PREMIO_QUINA,
						$PREMIO_QUADRA,
						$MSG_BILHETE
					);
					$auditoriaOp = AUD_CONFIG_SUPERSENA_CRIADA;
				}
			break;			
			case "quininha":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoQuininha(
						$oldConfig,
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_13,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$PREMIO_45,
						$MSG_BILHETE,
						$HABILITA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$TERNO_SORTE
					);
					$auditoriaOp = AUD_CONFIG_QUININHA_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoQuininha(
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_13,
						$PREMIO_14,
						$PREMIO_15,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_25,
						$PREMIO_30,
						$PREMIO_35,
						$PREMIO_40,
						$PREMIO_45,
						$MSG_BILHETE,
						$HABILITA_SORTE,
						$QUINA_SORTE,
						$QUADRA_SORTE,
						$TERNO_SORTE
					);
					$auditoriaOp = AUD_CONFIG_QUININHA_CRIADA;
				}
			break;
			case "lotinha":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoLotinha(
						$oldConfig,
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_21,
						$PREMIO_22,
						$MSG_BILHETE
					);
					$auditoriaOp = AUD_CONFIG_LOTINHA_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoLotinha(
						$desc_area,
						$PREMIO_MAXIMO,
						$PREMIO_16,
						$PREMIO_17,
						$PREMIO_18,
						$PREMIO_19,
						$PREMIO_20,
						$PREMIO_21,
						$PREMIO_22,
						$MSG_BILHETE
					);
					$auditoriaOp = AUD_CONFIG_LOTINHA_CRIADA;
				}
			break;
			case "bicho":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoBicho(
						$oldConfig,
						$desc_area,
						$PREMIO_MILHAR_CENTENA,
						$PREMIO_MILHAR_SECA,
						$PREMIO_MILHAR_INVERTIDA,
						$PREMIO_CENTENA,
						$PREMIO_CENTENA_INVERTIDA,
						$PREMIO_GRUPO,
						$PREMIO_DUQUE_GRUPO,
						$PREMIO_DUQUE_GRUPO6,
						$PREMIO_TERNO_GRUPO,
						$PREMIO_TERNO_GRUPO10,
						$PREMIO_TERNO_GRUPO6,
						$PREMIO_TERNO_SEQUENCIA,
						$PREMIO_TERNO_SOMA,
						$PREMIO_TERNO_ESPECIAL,
						$PREMIO_QUINA_GRUPO,
						$PREMIO_DEZENA,
						$PREMIO_DUQUE_DEZENA,
						$PREMIO_DUQUE_DEZENA6,
						$PREMIO_TERNO_DEZENA,
						$PREMIO_TERNO_DEZENA10,
						$PREMIO_TERNO_DEZENA6,
						$PREMIO_PASSE_SECO,
						$PREMIO_PASSE_COMBINADO,
						$MSG_BILHETE,
						$USA_MILHAR_BRINDE,
						$VALOR_LIBERAR_MILHAR_BRINDE,
						$VALOR_RETORNO_MILHAR_BRINDE
					);
					$auditoriaOp = AUD_CONFIG_BICHO_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoBicho(
						$desc_area,
						$PREMIO_MILHAR_CENTENA,
						$PREMIO_MILHAR_SECA,
						$PREMIO_MILHAR_INVERTIDA,
						$PREMIO_CENTENA,
						$PREMIO_CENTENA_INVERTIDA,
						$PREMIO_GRUPO,
						$PREMIO_DUQUE_GRUPO,
						$PREMIO_DUQUE_GRUPO6,
						$PREMIO_TERNO_GRUPO,
						$PREMIO_TERNO_GRUPO6,
						$PREMIO_TERNO_GRUPO10,
						$PREMIO_TERNO_GRUPO6,
						$PREMIO_TERNO_SEQUENCIA,
						$PREMIO_TERNO_SOMA,
						$PREMIO_TERNO_ESPECIAL,
						$PREMIO_QUINA_GRUPO,
						$PREMIO_DEZENA,
						$PREMIO_DUQUE_DEZENA,
						$PREMIO_DUQUE_DEZENA6,
						$PREMIO_TERNO_DEZENA,
						$PREMIO_TERNO_DEZENA10,
						$PREMIO_TERNO_DEZENA6,
						$PREMIO_PASSE_SECO,
						$PREMIO_PASSE_COMBINADO,
						$MSG_BILHETE,
						$USA_MILHAR_BRINDE,
						$VALOR_LIBERAR_MILHAR_BRINDE,
						$VALOR_RETORNO_MILHAR_BRINDE
					);
					$auditoriaOp = AUD_CONFIG_BICHO_CRIADA;
				}
			break;
			case "2pra500":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracao2pra500($oldConfig, $desc_area, $VALOR_ACUMULADO, $MSG_BILHETE, $VALOR_APOSTA);
					$auditoriaOp = AUD_CONFIG_2PRA500_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracao2pra500($desc_area, $VALOR_ACUMULADO, $MSG_BILHETE, $VALOR_APOSTA);
					$auditoriaOp = AUD_CONFIG_2PRA500_CRIADA;
				}
			break;
			case "rifa":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoRifa($oldConfig, $desc_area, $QTD_RIFAS, $MSG_BILHETE);
					$auditoriaOp = AUD_CONFIG_RIFA_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoRifa($desc_area, $QTD_RIFAS, $MSG_BILHETE);
					$auditoriaOp = AUD_CONFIG_RIFA_CRIADA;
				}
			break;
			case "multijogos":
				if (!is_null($oldConfig)) {
					$auditoria = descreverEdicaoConfiguracaoMultijogos($oldConfig, $desc_area, 
																		$VALOR_CARTAO_1,
																		$VALOR_CARTAO_2,
																		$VALOR_CARTAO_3,
																		$VALOR_CARTAO_4,
																		$VALOR_CARTAO_5,
																		$VALOR_MINIMO,
																		$QTD_NUMEROS_SENINHA,
																		$QTD_NUMEROS_QUININHA,
																		$QTD_NUMEROS_LOTINHA,
																		$TIPOS_JOGOS_BICHO,
																		$CONCURSOS_MARCADOS);
					$auditoriaOp = AUD_CONFIG_MULTIJOGOS_EDITADA;
				} else {
					$auditoria = descreverCriacaoConfiguracaoMultijogos($desc_area, 
																		$VALOR_CARTAO_1,
																		$VALOR_CARTAO_2,
																		$VALOR_CARTAO_3,
																		$VALOR_CARTAO_4,
																		$VALOR_CARTAO_5,
																		$VALOR_MINIMO,
																		$QTD_NUMEROS_SENINHA,
																		$QTD_NUMEROS_QUININHA,
																		$QTD_NUMEROS_LOTINHA,
																		$TIPOS_JOGOS_BICHO,
																		$CONCURSOS_MARCADOS);
					$auditoriaOp = AUD_CONFIG_MULTIJOGOS_CRIADA;
				}
			break;			
			default:
			break;
		}

		$stmt->execute();

		if ($operacao == "bicho") {
			if ($cod_config == null) {
				$cod_config = $stmt->insert_id;
			}
			// INSERINDO / ATUALIZANDO TIPOS DE JOGOS DO BICHO
			$queryTiposJogosBicho = "SELECT COD_TIPO_JOGO_BICHO 
									 FROM configuracao_tipo_jogo_bicho 
									 WHERE COD_CONFIGURACAO = '$cod_config'";
			$result = mysqli_query($con, $queryTiposJogosBicho);
			$tiposJogosAntigos = [];
			while($cod_tipo_jogo = mysqli_fetch_array($result)) {
				$tiposJogosAntigos[] = intval($cod_tipo_jogo[0]);
			}
			$tiposJogosInserir = array_diff($TIPOS_JOGOS_BICHO, $tiposJogosAntigos);
			$tiposJogosRemover = array_diff($tiposJogosAntigos, $TIPOS_JOGOS_BICHO);
			foreach( $tiposJogosInserir as $codigoTipoJogo ) {
				$values[] = '("'. $cod_config . '", ' . $codigoTipoJogo . ')';
			}

			$tiposJogosInseridos = null;
			$tiposJogosRemovidos = null;
			if (!empty($tiposJogosInserir)) {
				$queryInserirPermissoes = "INSERT INTO configuracao_tipo_jogo_bicho(COD_CONFIGURACAO, COD_TIPO_JOGO_BICHO) 
				VALUES " . implode(',', $values);
				mysqli_query($con, $queryInserirPermissoes);    
				$queryTiposJogosInseridos = "SELECT group_concat(DESCRICAO) 
						 FROM tipo_jogo_bicho t 
						 WHERE t.COD_TIPO_JOGO_BICHO IN (" . implode(',', $tiposJogosInserir) . ")";
				$result = mysqli_query($con, $queryTiposJogosInseridos);
				$tiposJogosInseridos = mysqli_fetch_array($result)[0];
			}

			if (!empty($tiposJogosRemover)) {
				$queryTiposJogosRemover = "DELETE FROM configuracao_tipo_jogo_bicho 
					   					   WHERE COD_CONFIGURACAO = '$cod_config' 
					   					   AND COD_TIPO_JOGO_BICHO IN (" . implode(',', $tiposJogosRemover) . ")";
				mysqli_query($con, $queryTiposJogosRemover);
				$queryTiposJogosRemovidos = "SELECT group_concat(DESCRICAO) 
						 					 FROM tipo_jogo_bicho t 
						 					 WHERE t.COD_TIPO_JOGO_BICHO IN (" . implode(',', $tiposJogosRemover) . ")";
				$result = mysqli_query($con, $queryTiposJogosRemovidos);
				$tiposJogosRemovidos = mysqli_fetch_array($result)[0];
			}

			$auditoria = $auditoria . descreverTiposJogosBichos($tiposJogosInseridos, $tiposJogosRemovidos);
		}

		inserir_auditoria($con, $cod_usuario, $site, $auditoriaOp, $auditoria);
		$response['status'] = "OK";
	} catch (Exception $e) {
		$response['status'] = "ERROR";
		$response['mensagem'] = $e->errorMessage();
	}

	echo json_encode($response);

	$stmt->close();
	$con->close();
} else {
	$response['status'] = "OK";
	echo json_encode($response);
}
