angular.module('main').controller('bloqueiosAvulsosCtrl', function ($scope, $rootScope, $http, $route, $filter) {

    $scope.codigo = 0;
    $scope.data = new Date();
    $scope.tipoJogo = '';
    $scope.tiposJogos = [{ nome: 'Seninha/Super Sena', codigo: 'S' }, { nome: 'Quininha', codigo: 'Q' },
    { nome: 'Lotinha', codigo: 'L' }, { nome: 'Bicho', codigo: 'B' }, { nome: '2 pra 500', codigo: '2' }];
    $scope.concurso = null;
    $scope.extracao = null;


    $scope.listarBloqueios = function () {
        var data  = $filter('date')($scope.data, "yyyy-MM-dd");
        $rootScope.loading = true;
        $http({
            method: "POST",
            url: "angularjs-mysql/bloqueiosAvulsos.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'operacao=listar&data=' + data
        }).then(function (response) {
            $rootScope.loading = false;
            $scope.records = response.data;
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status)
        });
    }

    $scope.salvar = function () {
        var codigo = $scope.codigo;
        var data_jogo = $filter('date')($scope.data, "yyyy-MM-dd");
        var tipo_jogo = $scope.tipoJogo;
        var hora_bloqueio = $scope.edtHora;
        var hora_extracao = $scope.comboHorario;
        var descricao_extracao =  $('#nomeBanca :selected').text().replace(/(\r\n|\n|\r)/gm, "").trim();
        
        $operacao = "inserir";
        if (codigo != 0) {
            $operacao = "atualizar";
        }

        if (data_jogo == "" || data_jogo == null) {
            $.alert('Informe a data do resultado!');
            return;
        }

        if (tipo_jogo == "") {
            $.alert('Informe o tipo de jogo!');
            return;
        }

        if (hora_bloqueio == '') {
            $.alert('Informe o horário do bloqueio!');
            return;
        }

        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/bloqueiosAvulsos.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 
                'codigo=' + codigo +
                '&data=' + data_jogo +
                '&tipo=' + tipo_jogo +
                '&horaBloqueio=' + hora_bloqueio + 
                '&horaExtracao=' + hora_extracao + 
                '&descricaoExtracao=' + descricao_extracao +
                '&operacao=' + $operacao
        }).then(function (response) {
            $rootScope.loading = false;
            if (response.data.status == 'OK') {
                if (codigo != 0) {
                    $.alert('Resultado atualizado com sucesso!');
                } else {
                    $.alert('Resultado criado com sucesso!');
                }
                $route.reload();
            } else {
                $.alert(response.data.mensagem);
            }
        }, function (response) {
            $rootScope.loading = false;
            $.alert('Erro no $HTTP: ' + response.status);
        });
    }

    $scope.alterar = function(resultado) {
        $scope.codigo = resultado.codigo;
        $scope.data = new Date(resultado.data_jogo.replace(/-/g, '\/'));
        $scope.tipoJogo = resultado.tipo_jogo;
        $scope.edtHora = resultado.horario_bloqueio;
        $scope.comboHorario = resultado.hora_extracao;
    }

    $scope.excluir = function (codigo) {
        $.confirm({
            title: '',
            content:"Confirma a exclusão do resultado?",
            buttons: {
                cancelar: function() {},
                ok: function () {
                    $rootScope.loading = true;
                    $http({
                        url: 'angularjs-mysql/bloqueiosAvulsos.php',
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: 
                            'codigo=' + codigo +
                            '&operacao=excluir'
                    }).then(function (response) {
                        $rootScope.loading = false;
                        if (response.data.status == 'OK') {
                            $.alert('Resultado excluído com sucesso!');
                            $route.reload();
                        } else {
                            $.alert(response.data.mensagem);
                        }
                    }, function (response) {
                        $rootScope.loading = false;
                        $.alert('Erro no $HTTP: ' + response.status);
                    });
                }
            }
        });
    }

    $scope.getExtracoes = function() {
        $rootScope.loading = true;
        $http(
          {
            method: "POST",
            url: "angularjs-mysql/resultado.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "operacao=" + "listar_bancas"
          }
        ).then(function(response) {
          $scope.extracoes = response.data;
        }).catch(function (response) {            
          $.alert('Erro no $HTTP: ' + response.status)
        }).finally(function() {
          $rootScope.loading = false;
        });
    }

    $scope.getHorarios = function() {
        $rootScope.loading = true;
        $scope.comboHorario = "";
        var cod_extracao = $scope.comboBanca;
        $http(
          {
            method: "POST",
            url: "angularjs-mysql/resultado.php",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "operacao=listar_horarios_banca"
              + "&cod_extracao=" + cod_extracao
          }
        ).then(function(response) {
          $scope.horarios = response.data;
        }).catch(function (response) {            
          $.alert('Erro no $HTTP: ' + response.status)
        }).finally(function() {
          $rootScope.loading = false;
        });
    }

    $scope.getDescricaoTipoJogo = function (tipo_jogo) {
        switch (tipo_jogo) {
            case 'S':
                return "Seninha/Super Sena";
            case 'Q':
                return "Quininha";
            case 'L':
                return "Lotinha";
            case 'B':
                return "Bicho";
            case 2:
                return "2 pra 500";
            default:
                return "";
        }
    }

    $scope.getDescricaoStatus = function (status) {
        switch (status) {
            case 'A':
                return "Aberto";
            case 'P':
                return "Processado";
            default:
                return "";
        }
    }

})