<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$finalizados = mysqli_real_escape_string($con, $_POST['finalizados']);

$query = 'SET @@session.time_zone = "-03:00"';
$result = mysqli_query($con, $query);

$queryArea = "SELECT COD_AREA
    FROM areas
    WHERE cod_site = '$site' AND padrao = 'S' and status = 'A'";

$resultArea = mysqli_query($con, $queryArea);
$rowArea = mysqli_fetch_array($resultArea, MYSQLI_ASSOC);
$area = $rowArea['COD_AREA'];


$query = "SELECT j.COD_JOGO, 
                 TIME_FORMAT(j.HORA_EXTRACAO, '%H:%i') HORA_EXTRACAO, 
                 case when j.DESC_HORA is null then '' else j.DESC_HORA END DESC_HORA,
                 e.QTD_PREMIOS
            FROM jogo j JOIN extracao_bicho e ON (j.CONCURSO = e.COD_EXTRACAO
                                                    AND e.STATUS = 'A')
					JOIN area_extracao ae ON (e.COD_EXTRACAO = ae.COD_EXTRACAO 
                                                AND e.COD_SITE = ae.COD_SITE
                                                AND ae.STATUS = 'A')
            WHERE j.COD_SITE = '$site'
                AND ae.COD_AREA = '$area' ";

if ($finalizados == 'false') {
    $query = $query . " AND j.TP_STATUS <> 'F' ";
}

try {
    $status = mysqli_real_escape_string($con, $_POST['status']);  

    if ($status != "") {
        $query = $query . " AND j.TP_STATUS = '$status'";
    }
} catch (Exception $e) {

}

try {
    $operacao = mysqli_real_escape_string($con, $_POST['operacao']);  
    $data = mysqli_real_escape_string($con, $_POST['data']);          

    if ($operacao == "bicho"){
        $tipoJogo = "B";
    }else{
        $tipoJogo = "2";
    }

    $query = $query . " AND ((j.DATA_JOGO = CURRENT_DATE AND j.HORA_EXTRACAO > CURRENT_TIME + INTERVAL E.PRAZO_BLOQUEIO MINUTE) OR (j.DATA_JOGO > CURRENT_DATE) )
                        AND j.data_jogo = '$data' 
                        AND j.TIPO_JOGO = '$tipoJogo'  
                    ORDER BY j.HORA_EXTRACAO, j.CONCURSO DESC ";
    
} catch (Exception $e) {

}

try {
    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_jogo'] = $row['COD_JOGO'];
        $row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
        $row_array['desc_hora'] = $row['DESC_HORA'];   
        $row_array['qtd_premios'] = $row['QTD_PREMIOS'];     

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }    

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);

} catch (Exception $e) {
    $response['status'] = "ERROR";
    $response['mensagem'] = "Ocorreu o seguinte erro: " . $e->getMessage();
    
    echo json_encode($response);
}

$con->close();