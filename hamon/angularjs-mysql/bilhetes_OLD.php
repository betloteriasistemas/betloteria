<?php

include "conexao.php";

include_once 'bitly.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

session_start();

$response = [];

$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

if (isset($_POST['todosUsuarios'])) {
    $todosUsuarios = mysqli_real_escape_string($con, $_POST['todosUsuarios']);
} else {
    $todosUsuarios = null;
}

//$username = "will";
//$site = 1;

$contador = 0;

if ($operacao == "listar") {
    $query = "select bi.cod_bilhete, bi.txt_pule, usu.nome cambista, geren.nome gerente, bi.nome_apostador, bi.telefone_apostador,
            DATE_FORMAT(bi.data_bilhete, '%d-%m-%Y') data_bilhete, cast(case
            when jogo.tipo_jogo IN('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
            else jogo.concurso end as char) as concurso , jogo.tipo_jogo,
            DATE_FORMAT(bi.data_bilhete, '%d/%m/%Y %H:%i:%S') data_hora_bilhete,
            count(apo.cod_aposta) qtd_bilhetes,  round(sum(apo.valor_aposta),2) total,
            round(sum(apo.valor_aposta) * (CASE WHEN jogo.Tipo_Jogo = 'S' THEN usu.pct_comissao_seninha 
            WHEN jogo.Tipo_Jogo = 'Q' THEN  usu.pct_comissao_quininha 
            WHEN jogo.Tipo_Jogo = 'B' THEN  usu.pct_comissao_bicho 
            WHEN jogo.Tipo_Jogo = '2' THEN  usu.pct_comissao_2PRA500 
            WHEN jogo.Tipo_Jogo = '6' THEN  usu.pct_comissao_6DASORTE ELSE 0 END) / 100,2) as comissao,
            round(sum(case
            when apo.status = 'G' then apo.possivel_retorno
            else 0 end), 2) possivel_retorno,
            case
            when bi.status_bilhete = 'A' then 'Aberto'
            when bi.status_bilhete = 'C' then 'Cancelado'
            when bi.status_bilhete = 'P' then 'Processado' end status_bilhete
            from bilhete bi
            inner join aposta apo on (bi.cod_bilhete = apo.cod_bilhete)
            inner join usuario usu on (bi.cod_usuario = usu.cod_usuario)
            inner join usuario geren on (geren.cod_usuario = usu.cod_gerente)
            inner join jogo on (jogo.cod_jogo = apo.cod_jogo)
            where bi.cod_site = '$site'
            and ( (usu.cod_usuario = '$cod_usuario' or usu.cod_gerente = '$cod_usuario')  or (geren.cod_usuario = '$cod_usuario' or geren.cod_gerente = '$cod_usuario') )
            group by bi.cod_bilhete, bi.txt_pule, usu.nome, geren.nome, bi.nome_apostador, bi.telefone_apostador, bi.data_bilhete, jogo.concurso, jogo.tipo_jogo ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_bilhete'] = $row['cod_bilhete'];
        $row_array['txt_pule'] = $row['txt_pule'];
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_hora_bilhete'] = $row['data_hora_bilhete'];

        $row_array['qtd_bilhetes'] = $row['qtd_bilhetes'];
        $row_array['total'] = $row['total'];
        $row_array['comissao'] = $row['comissao'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['concurso'] = $row['concurso'];
        $row_array['status_bilhete'] = $row['status_bilhete'];
        $row_array['gerente'] = $row['gerente'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }
    ;

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);
} else if ($operacao == "getCambistas") {

    if ($todosUsuarios == 'A') {
        $query = " select nome, cod_usuario from usuario
               where cod_site = '$site'
                 and cod_usuario <>  '$cod_usuario'
                 and status = 'A'
                order by nome ";
    } else {
        $query = " select nome, cod_usuario from usuario
            where cod_gerente = '$cod_usuario'
            and cod_site = '$site'
            and status = 'A'
            order by nome ";
    }

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['nome'] = $row['nome'];
        $row_array['cod_usuario'] = $row['cod_usuario'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);
} else if ($operacao == "listarApostas") {
    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);

    $query = " select txt_aposta, qtd_numeros, valor_aposta, possivel_retorno,
    case
     when status = 'A' then 'Aberta'
     when status = 'G' then 'Ganho'
     when status = 'C' then 'Cancelado'
     when status = 'P' then 'Perdido' end as status
    from aposta
    where cod_bilhete = '$cod_bilhete'
      and cod_site = '$site' ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;

        $row_array['txt_aposta'] = $row['txt_aposta'];
        $row_array['qtd_numeros'] = $row['qtd_numeros'];
        $row_array['valor_aposta'] = $row['valor_aposta'];
        $row_array['possivel_retorno'] = $row['possivel_retorno'];
        $row_array['status'] = $row['status'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    }

    echo json_encode($return_arr);
} else if ($operacao == "conferirBilhete") {

    $cod_bilhete = mysqli_real_escape_string($con, $_POST['cod_bilhete']);
    if (isset($_POST['numero_bilhete'])) {
        $numero_bilhete = mysqli_real_escape_string($con, $_POST['numero_bilhete']);
    } else {
        $numero_bilhete = -1;
    }

    $query = "  SELECT DATE_FORMAT(bil.data_bilhete, '%d/%m/%Y %H:%i:%S') data_bilhete,
                       DATE_FORMAT(bil.data_bilhete, '%Y-%m-%dT%H:%i:%S') data_bilhete_js,
                site.NOME_BANCA NOME_BANCA,
                site.cod_site,
                usu.nome cambista,
                bil.nome_apostador,
                bil.telefone_apostador,
                bil.txt_pule,
                case
                    when bil.status_bilhete = 'A' then 'Aberto'
                    when bil.status_bilhete = 'P' then 'Processado'
                    when bil.status_bilhete = 'C' then 'Cancelado'
                    when bil.status_bilhete = 'V' then 'Pendente de Validação' end as status_bilhete,
                apo.qtd_numeros,
                apo.txt_aposta,
                apo.valor_aposta,
                apo.possivel_retorno,
                apo.do_premio,
                apo.ao_premio,
                apo.tipo_jogo tipo_jogo_bicho,
                apo.inversoes,
                bil.cod_bilhete as numero_bilhete,
                case
                    when apo.status = 'A' then 'Aberto'
                    when apo.status = 'G' then 'Ganho'
                    when apo.status = 'C' then 'Cancelado'
                    when apo.status = 'P' then 'Perdido' end as status,
                jogo.data_jogo,
                cast(case
                 when jogo.tipo_jogo IN ('B','2') then concat(DATE_FORMAT(jogo.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(jogo.hora_extracao, '%H:%i'), case when jogo.desc_hora is null then '' else concat('-',jogo.desc_hora) end)
                  else jogo.concurso end as char) as concurso,
                case
                  when jogo.tipo_jogo = 'S' then 'SENINHA'
                  when jogo.tipo_jogo = 'Q' then 'QUININHA'
                  when jogo.tipo_jogo = 'B' then 'BICHO'
                  when jogo.tipo_jogo = '2' then '2 PRA 500' else ''
                  end tipo_jogo,
                LPAD(jogo.numero_1,4,'0') numero_1,
                LPAD(jogo.numero_2,4,'0') numero_2,
                LPAD(jogo.numero_3,4,'0') numero_3,
                LPAD(jogo.numero_4,4,'0') numero_4,
                LPAD(jogo.numero_5,4,'0') numero_5,
                LPAD(jogo.numero_6,4,'0') numero_6,
                LPAD(jogo.numero_7,4,'0') numero_7,
                LPAD(jogo.numero_8,4,'0') numero_8,
                LPAD(jogo.numero_9,4,'0') numero_9,
                LPAD(jogo.numero_10,4,'0') numero_10
                FROM bilhete bil
                inner join site on (site.cod_site = bil.cod_site)
                inner join aposta apo on (bil.cod_bilhete = apo.cod_bilhete)
                inner join jogo on (apo.cod_jogo = jogo.cod_jogo)
                inner join usuario usu on (bil.cod_usuario = usu.cod_usuario and bil.cod_site = usu.cod_site)
                where (bil.txt_pule = '$cod_bilhete' or bil.txt_pule_pai = '$cod_bilhete' or bil.cod_bilhete = '$numero_bilhete')
                and (bil.cod_site = '$site' or bil.cod_site = 9999) ";

    $queryBilhete = "select sum(valor_aposta) valor_bilhete from ( " . $query . ") v ";

    $resultValorBilhete = mysqli_query($con, $queryBilhete);

    $rowValorBilhete = mysqli_fetch_array($resultValorBilhete, MYSQLI_ASSOC);

    $valorBilhete = $rowValorBilhete['valor_bilhete'];

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;
    $linkZap = "";

    // token willianae@gmail.com
    //$user_access_token = '8afa4e6c4ce593125a900430230be297e5b3e7f7';

    // token sabatina8@o2.pl
    // $user_access_token = '01099036338c09d46ce757703f983990b4a625e5';

    $user_access_token = '08c3557fb3ae656c9261e901986c77b25ce9ae10';

    $bilhete_anterior = "";

    //while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    while ($contador < mysqli_num_rows($result)){

        /*if ($contador == 1) {
        $params = array();
        $params['access_token'] = $user_access_token;
        $params['longUrl'] = 'http://www.betloteria.com/bilhete_externo/#!/' . $cod_bilhete;
        $params['domain'] = 'j.mp';
        $results = bitly_get('shorten', $params);

        $linkZap = '';

        //$linkZap = $results['data']['url'];
        }*/

        //$row_array['linkZap'] = $linkZap;
        $bilhete_anterior = $row['txt_pule'];
        $row_array['linkZap'] = '';

        $row_array['data_bilhete'] = $row['data_bilhete'];
        $row_array['data_bilhete_js'] = $row['data_bilhete_js'];
        $row_array['nome_banca'] = $row['NOME_BANCA'];
        $row_array['cambista'] = $row['cambista'];
        $row_array['nome_apostador'] = $row['nome_apostador'];
        $row_array['telefone_apostador'] = $row['telefone_apostador'];
        $row_array['status_bilhete'] = $row['status_bilhete'];
        $row_array['txt_pule'] = $row['txt_pule'];
        $row_array['numero_bilhete'] = $row['numero_bilhete'];
        $row_array['data_jogo'] = $row['data_jogo'];
        $row_array['concurso'] = $row['concurso'];
        $row_array['tipo_jogo'] = $row['tipo_jogo'];

        if ($row['tipo_jogo'] == '2 PRA 500'){
            $row_array['valor_bilhete'] = 2;
        }else{
            $row_array['valor_bilhete'] = $valorBilhete;
        }
        
        

        if ($row['tipo_jogo'] == "2 PRA 500") {
            $row_array['numeros_sorteados'] = $row['numero_1'];

        }else if ($row['tipo_jogo'] == "BICHO") {

            $row_array['numeros_sorteados'] = $row['numero_1'] . ' - ' . $row['numero_2'] . ' - ' .
                $row['numero_3'] . ' - ' . $row['numero_4'] . ' - ' .
                $row['numero_5'] . ' - ' . $row['numero_6'] . ' - ' .
                $row['numero_7'] . ' - ' . $row['numero_8'] . ' - ' .
                $row['numero_9'] . ' - ' . $row['numero_10'];
        } else {
            $row_array['numeros_sorteados'] = substr($row['numero_1'], strlen($row['numero_1']) - 2, 2) . ' - ' .
            substr($row['numero_2'], strlen($row['numero_2']) - 2, 2) . ' - ' .
            substr($row['numero_3'], strlen($row['numero_3']) - 2, 2) . ' - ' .
            substr($row['numero_4'], strlen($row['numero_4']) - 2, 2) . ' - ' .
            substr($row['numero_5'], strlen($row['numero_5']) - 2, 2);

            if ($row['tipo_jogo'] == 'SENINHA') {
                $row_array['numeros_sorteados'] = $row_array['numeros_sorteados'] . ' - ' .
                substr($row['numero_6'], strlen($row['numero_6']) - 2, 2);
            }

        }

        // carregamento das apostas
        $row_array['apostas'] = array();

        do {
            $row_array_2['qtd_numeros'] = $row['qtd_numeros'];
            $row_array_2['txt_aposta'] = $row['txt_aposta'];
            $row_array_2['valor_aposta'] = $row['valor_aposta'];
            $row_array_2['possivel_retorno'] = $row['possivel_retorno'];
            $row_array_2['status'] = $row['status'];
            $row_array_2['do_premio'] = $row['do_premio'];
            $row_array_2['ao_premio'] = $row['ao_premio'];
            $row_array_2['tipo_jogo_bicho'] = $row['tipo_jogo_bicho'];
            $row_array_2['inversoes'] = $row['inversoes'];            

            array_push($row_array['apostas'], $row_array_2);

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $contador = $contador + 1;

            if ($contador == mysqli_num_rows($result)) {
                break;
            }

        } while ($bilhete_anterior == $row['txt_pule']);

        array_push($return_arr, $row_array);

        /*if ($contador == mysqli_num_rows($result)) {
            break;
        }*/
    }

    echo json_encode($return_arr);

}
