<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$site = mysqli_real_escape_string($con, $_POST['site']);

$query = "";

if ($operacao == 'listar') {
    $query =
        " SELECT e.COD_EXTRACAO, e.COD_SITE, TIME_FORMAT(e.HORA_EXTRACAO, '%H:%i')  
                 AS HORA_EXTRACAO, e.DESCRICAO, e.DOMINGO, e.SEGUNDA, e.TERCA, e.QUARTA, 
                 e.QUINTA, e.SEXTA, e.SABADO, e.TIPO_JOGO, e.QTD_PREMIOS, e.HABILITA_CENTENA,
                 clb.MILHAR, clb.CENTENA, clb.GRUPO, clb.DUQUE_GRUPO, clb.TERNO_GRUPO, clb.QUINA_GRUPO,
                 clb.DEZENA, clb.DUQUE_DEZENA, TERNO_DEZENA, PRAZO_BLOQUEIO
          FROM extracao_bicho e 
          LEFT JOIN configuracao_limite_bicho clb ON (e.COD_EXTRACAO = clb.COD_EXTRACAO)
          WHERE e.STATUS = 'A' AND e.COD_SITE = '$site' ";

    if (isset($_POST['filtra_automatizadas'])) {
        $query = $query . " AND e.AUTOMATIZADA = 'N' ";
    }

    if (isset($_POST['tipo_jogo'])) {
        $tipo_jogo = $_POST['tipo_jogo'];
        $query = $query . " and tipo_jogo = '$tipo_jogo' ";
    }

    $query = $query . " ORDER BY HORA_EXTRACAO ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['cod_site'] = $row['COD_SITE'];
        $row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
        $row_array['descricao'] = $row['DESCRICAO'];
        $row_array['domingo'] = $row['DOMINGO'];
        $row_array['segunda'] = $row['SEGUNDA'];
        $row_array['terca'] = $row['TERCA'];
        $row_array['quarta'] = $row['QUARTA'];
        $row_array['quinta'] = $row['QUINTA'];
        $row_array['sexta'] = $row['SEXTA'];
        $row_array['sabado'] = $row['SABADO'];
        $row_array['tipo_jogo'] = $row['TIPO_JOGO'];
        $row_array['qtd_premios'] = $row['QTD_PREMIOS'];
        $row_array['milhar'] = $row['MILHAR'];
        $row_array['centena'] = $row['CENTENA'];
        $row_array['grupo'] = $row['GRUPO'];
        $row_array['duque_grupo'] = $row['DUQUE_GRUPO'];
        $row_array['terno_grupo'] = $row['TERNO_GRUPO'];
        $row_array['quina_grupo'] = $row['QUINA_GRUPO'];
        $row_array['dezena'] = $row['DEZENA'];
        $row_array['duque_dezena'] = $row['DUQUE_DEZENA'];
        $row_array['terno_dezena'] = $row['TERNO_DEZENA'];
        $row_array['prazo_bloqueio'] = $row['PRAZO_BLOQUEIO'];
        $row_array['habilita_centena'] = $row['HABILITA_CENTENA'];
        
        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

    echo json_encode($return_arr, JSON_NUMERIC_CHECK);
} else if ($operacao == 'salvar') {
    $queryOldExtracao = "SELECT eb.*, clb.MILHAR, clb.CENTENA, clb.GRUPO, clb.DUQUE_GRUPO,
                        clb.TERNO_GRUPO, clb.QUINA_GRUPO, clb.DEZENA, clb.DUQUE_DEZENA, clb.TERNO_DEZENA
                        FROM extracao_bicho eb
                        LEFT JOIN configuracao_limite_bicho clb ON (eb.cod_extracao = clb.cod_extracao)
                        WHERE eb.cod_extracao = ?";
    $stmtOldExtracao = null;
    $oldExtracao = null;

    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
    $hora = mysqli_real_escape_string($con, $_POST['hora']);
    $descricao = mysqli_real_escape_string($con, $_POST['descricao']);
    $domingo = mysqli_real_escape_string($con, $_POST['domingo']);
    $segunda = mysqli_real_escape_string($con, $_POST['segunda']);
    $terca = mysqli_real_escape_string($con, $_POST['terca']);
    $quarta = mysqli_real_escape_string($con, $_POST['quarta']);
    $quinta = mysqli_real_escape_string($con, $_POST['quinta']);
    $sexta = mysqli_real_escape_string($con, $_POST['sexta']);
    $sabado = mysqli_real_escape_string($con, $_POST['sabado']);
    $tipo_jogo = mysqli_real_escape_string($con, $_POST['tipo_jogo']);
    $qtd_premios = ifStringNullReturn(mysqli_real_escape_string($con, $_POST['qtd_premios']), null);
    $milhar = mysqli_real_escape_string($con, $_POST['milhar']);
    $centena = mysqli_real_escape_string($con, $_POST['centena']);
    $grupo = mysqli_real_escape_string($con, $_POST['grupo']);
    $duque_grupo = mysqli_real_escape_string($con, $_POST['duque_grupo']);
    $terno_grupo = mysqli_real_escape_string($con, $_POST['terno_grupo']);
    $quina_grupo = mysqli_real_escape_string($con, $_POST['quina_grupo']);
    $dezena = mysqli_real_escape_string($con, $_POST['dezena']);
    $duque_dezena = mysqli_real_escape_string($con, $_POST['duque_dezena']);
    $terno_dezena = mysqli_real_escape_string($con, $_POST['terno_dezena']);
    $prazo_bloqueio = mysqli_real_escape_string($con, $_POST['prazo_bloqueio']);
    $habilita_centena = mysqli_real_escape_string($con, $_POST['habilita_centena']);

    try {
        $codigo_extracao = $codigo;
        if ($codigo == 0) {
            $query = " SELECT cod_extracao FROM extracao_bicho
                        WHERE COD_SITE = '$site'
                        and hora_extracao = '$hora'
                        and descricao = '$descricao'
                        and tipo_jogo = '$tipo_jogo'
                        and automatizada != 'S' ";

            $result = mysqli_query($con, $query);
            if (mysqli_num_rows($result)) {
                $extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);

                $stmt = $con->prepare("UPDATE extracao_bicho
                                    set status = 'A',
                                    domingo = ?,
                                    segunda = ?,
                                    terca = ?,
                                    quarta = ?,
                                    quinta = ?,
                                    sexta = ?,
                                    sabado = ?,
                                    qtd_premios = ?,
                                    prazo_bloqueio = ?,
                                    habilita_centena = ?
                                    WHERE cod_extracao = ?");
                $stmt->bind_param("sssssssiisi", $domingo, $segunda, $terca, $quarta,
                    $quinta, $sexta, $sabado, $qtd_premios, $prazo_bloqueio, $habilita_centena,
                    $extracao['cod_extracao']);

                $stmtOldExtracao = $con->prepare($queryOldExtracao);
                $stmtOldExtracao->bind_param("i", $extracao['cod_extracao']);
                $stmtOldExtracao->execute();
                $result = $stmtOldExtracao->get_result();
                $oldExtracao = $result->fetch_assoc();
                $stmtOldExtracao->close();
                
                $stmt->execute();
            } else {
                $stmt = $con->prepare("INSERT INTO extracao_bicho(cod_site, hora_extracao, descricao, domingo,
                    segunda, terca, quarta, quinta, sexta, sabado, tipo_jogo, qtd_premios, prazo_bloqueio, habilita_centena)
                values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
                $stmt->bind_param("issssssssssiis", $site, $hora, $descricao, $domingo, $segunda, $terca, 
                    $quarta, $quinta, $sexta, $sabado, $tipo_jogo, $qtd_premios, $prazo_bloqueio, $habilita_centena);
                $stmt->execute();
                $codigo_extracao = $stmt->insert_id;

                $query = " SELECT cod_extracao FROM extracao_bicho
                WHERE COD_SITE = '$site'
                and hora_extracao = '$hora'
                and descricao = '$descricao'
                and tipo_jogo = '$tipo_jogo' 
                and automatizada != 'S' ";
                $result = mysqli_query($con, $query);
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $cod_extracao = $row['cod_extracao'];

                $query = " INSERT INTO area_extracao (cod_site, cod_area, cod_extracao, status)
                            SELECT a.cod_site, a.cod_area, '$cod_extracao', 'A'
                            FROM areas a  
                            WHERE a.cod_site = '$site' ";
                $con->query($query);
            }
        } else {
            $stmt = $con->prepare("UPDATE extracao_bicho
                set hora_extracao = ?,
                    descricao = ?,
                    domingo = ?,
                    segunda = ?,
                    terca = ?,
                    quarta = ?,
                    quinta = ?,
                    sexta = ?,
                    sabado = ?,
                    tipo_jogo = ?,
                    qtd_premios = ?,
                    prazo_bloqueio = ?,
                    habilita_centena = ?
                where cod_extracao = ? ");
            $stmt->bind_param("ssssssssssiisi", $hora, $descricao, $domingo, $segunda, $terca, $quarta, 
                $quinta, $sexta, $sabado, $tipo_jogo, $qtd_premios, $prazo_bloqueio, $habilita_centena, 
                $codigo);

            $stmtOldExtracao = $con->prepare($queryOldExtracao);
            $stmtOldExtracao->bind_param("i", $codigo);
            $stmtOldExtracao->execute();
            $result = $stmtOldExtracao->get_result();
            $oldExtracao = $result->fetch_assoc();
            $stmtOldExtracao->close();
            $stmt->execute();
        }

        if ($tipo_jogo == "B") {
            $queryLimiteBicho = " SELECT codigo FROM configuracao_limite_bicho
            WHERE COD_EXTRACAO = '$codigo_extracao'
              and cod_site = '$site' ";
            $result = mysqli_query($con, $queryLimiteBicho);
            if (mysqli_num_rows($result)) {
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $codigo_configuracao = $row['codigo'];
                $stmtLimiteBicho = $con->prepare("UPDATE configuracao_limite_bicho
						SET MILHAR = ?, CENTENA = ?, GRUPO = ?, DUQUE_GRUPO = ?,
                            TERNO_GRUPO = ?, QUINA_GRUPO = ?, DEZENA = ?, DUQUE_DEZENA = ?, TERNO_DEZENA = ?
						WHERE CODIGO = ? ");
                $stmtLimiteBicho->bind_param(
                    "dddddddddi", $milhar, $centena, $grupo, $duque_grupo,
                             $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena,
                             $codigo_configuracao
                );
            } else {
                $stmtLimiteBicho = $con->prepare("INSERT INTO configuracao_limite_bicho(COD_SITE, COD_EXTRACAO, MILHAR, CENTENA,
                                        GRUPO, DUQUE_GRUPO, TERNO_GRUPO, QUINA_GRUPO, DEZENA, DUQUE_DEZENA, TERNO_DEZENA)
                                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
                $stmtLimiteBicho->bind_param("iiddddddddd", $site, $codigo_extracao, $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena);  
            }
            $stmtLimiteBicho->execute();
            $stmtLimiteBicho->close();
        }

        if ($codigo_extracao != $codigo) {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $site,
                AUD_EXTRACAO_CRIADA,
                descreverExtracao($stmt->insert_id, $hora, $descricao, $domingo, $segunda, 
                    $terca, $quarta, $quinta, $sexta, $sabado, $tipo_jogo, $qtd_premios, $prazo_bloqueio, 
                    $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, 
                    $dezena, $duque_dezena, $terno_dezena)
            );
        } else {
            inserir_auditoria(
                $con,
                $cod_usuario,
                $site,
                AUD_EXTRACAO_EDITADA,
                descreverEdicaoExtracao($oldExtracao, $codigo_extracao, $hora . ":00", $descricao, 
                $domingo, $segunda, $terca, $quarta, $quinta, $sexta, $sabado, $tipo_jogo, $qtd_premios, 
                $prazo_bloqueio, false, $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, 
                $dezena, $duque_dezena, $terno_dezena)
            );
        }

        $response['status'] = "OK";
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    } finally {
        echo json_encode($response);
    }
} else if ($operacao == 'excluir') {
    try {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
        $stmt = $con->prepare("UPDATE extracao_bicho
									set STATUS = 'I'
									where cod_extracao = ? ");
        $stmt->bind_param("i", $codigo);
        $stmt->execute();
        inserir_auditoria(
            $con,
            $cod_usuario,
            $site,
            AUD_EXTRACAO_EXCLUIDA,
            descreverExclusaoExtracao($codigo)
        );

        $response['status'] = "OK";
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    } finally {
        echo json_encode($response);
    }
} else if ($operacao == 'obter') {
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);
    $stmt = $con->prepare(
                " SELECT `COD_EXTRACAO`, `COD_SITE`, `COD_AREA`,
                      `HORA_EXTRACAO`, `STATUS`, `DESCRICAO`, `FEDERAL`, `TIPO_JOGO`,
                       `AUTOMATIZADA`, `QTD_PREMIOS`, `PRAZO_BLOQUEIO`, `HABILITA_CENTENA`,
                       `DOMINGO`, `SEGUNDA`, `TERCA`, `QUARTA`, `QUINTA`, `SEXTA`, `SABADO` 
                FROM extracao_bicho eb 
                WHERE COD_EXTRACAO = ? AND COD_SITE = ?");
    $stmt->bind_param("ii", $cod_extracao, $site);
    $result = $stmt->execute();
    $res = $stmt->get_result();
    $row = $res->fetch_assoc();
    $retorno['cod_extracao'] = $row['COD_EXTRACAO'];
    $retorno['cod_site'] = $row['COD_SITE'];
    $retorno['cod_area'] = $row['COD_AREA'];
    $retorno['hora_extracao'] = $row['HORA_EXTRACAO'];
    $retorno['status'] = $row['STATUS'];
    $retorno['descricao'] = $row['DESCRICAO'];
    $retorno['federal'] = $row['FEDERAL'];
    $retorno['tipo_jogo'] = $row['TIPO_JOGO'];
    $retorno['automatizada'] = $row['AUTOMATIZADA'];
    $retorno['qtd_premios'] = $row['QTD_PREMIOS'];
    $retorno['prazo_bloqueio'] = $row['PRAZO_BLOQUEIO'];
    $retorno['habilita_centena'] = $row['HABILITA_CENTENA'];
    $retorno['domingo'] = $row['DOMINGO'];
    $retorno['segunda'] = $row['SEGUNDA'];
    $retorno['terca'] = $row['TERCA'];
    $retorno['quarta'] = $row['QUARTA'];
    $retorno['quinta'] = $row['QUINTA'];
    $retorno['sexta'] = $row['SEXTA'];
    $retorno['sabado'] = $row['SABADO'];
    echo json_encode($retorno, JSON_NUMERIC_CHECK);
}
$con->close();
