angular.module('main').controller('conferirBilheteCtrl',
    function (bilhetesAPI, $rootScope, $scope, user, $location, $route, $http, funcoes, configGeral) {
        $rootScope.loading = false;
        $scope.perfil = user.getPerfil();
        $scope.nome = user.getNome();
        $scope.listaApostas = [];
        $scope.mapModalidade = new Map();
        $scope.tabModalidadeSelected = -1;
        $scope.previousTabModalidade = null;
        $scope.tabConcursoSelected = -1;
        $scope.previousTabConcurso = null;
        $scope.tabApostas = [];
        $scope.iniciou = false;
        $scope.exibeBilhete = false;
        $scope.data_bilhete = null;
        $scope.data_bilhete_js = null;
        $scope.nome_banca = "";
        $scope.totalApostado = 0;
        $scope.totalGanho = 0;
        $scope.cambista = "";
        $scope.nome_apostador = "";
        $scope.telefone_apostador = "";
        $scope.concurso = "";
        $scope.txt_pule = "";
        $scope.txt_pule_pai = "";
        $scope.status = "";
        $scope.status_bilhete = "";
        $scope.data_jogo = null;
        $scope.externo = false;
        $scope.tipo_jogo_bilhete = '';

        $scope.qrcodeString = '';
        $scope.size = 100;
        $scope.correctionLevel = 'M';
        $scope.typeNumber = 0;
        $scope.inputMode = 'ALPHA_NUM';
        $scope.image = false;

        $scope.linkZap = "";

        $scope.minutos_cancelamento = 0;
        $scope.valor_min_aposta;
        $scope.valor_max_aposta;
        configGeral.get().then(function (data) {
            $scope.minutos_cancelamento = data.minutos_cancelamento;
            $scope.valor_min_aposta = data.valor_min_aposta;
            $scope.valor_max_aposta = data.valor_max_aposta;
        });

        $scope.getDescricaoTipoJogoBicho = function (tipo_jogo) {
            return funcoes.getDescricaoTipoJogoBicho(tipo_jogo);
        }

        $scope.carregaConfiguracao = function (tipo_jogo) {
            var operacao = 'seninha';
            switch (tipo_jogo) {
                case 'BICHO':
                    operacao = 'bicho';
                break;
                case 'QUININHA':
                    operacao = 'quininha';
                break;
                case 'LOTINHA':
                    operacao = 'lotinha';
                break;
                case '2 PRA 500':
                    operacao = '2pra500';
                break;
                case 'RIFA':
                    operacao = 'rifa';
                break;
                case 'SUPER SENA':
                    operacao = 'supersena';
                break;
            }
            
            $http({
                url: 'angularjs-mysql/configuracao.php',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'site=' + user.getSite() + 
                      '&cod_area=' + user.getArea() + 
                      '&operacao=' + operacao +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                $scope.listaCfg = response.data;
            });
        }

        $scope.validarBilhete = function () {
            for (aposta of $scope.listaApostas[0].apostas) {
                var valor_aposta = parseFloat(aposta.valor_aposta);
                if ($scope.valor_min_aposta && $scope.valor_min_aposta > valor_aposta
                    && valor_aposta != 0 && aposta.tipo_jogo_bicho == "MS") {
                    $.alert("O bilhete não pode ser validado! Valor de aposta R$ " + valor_aposta
                        + " menor do que o permitido R$ " + $scope.valor_min_aposta);
                    return;
                }

                if ($scope.valor_max_aposta && $scope.valor_max_aposta < valor_aposta) {
                    $.alert("O bilhete não pode ser validado! Valor de aposta R$ " + valor_aposta
                        + " maior do que o permitido R$ " + $scope.valor_max_aposta);
                    return;
                }
            }
            $txt_pule = $scope.listaApostas[0].txt_pule;
            if ($scope.listaApostas[0].numero_bilhete == $txt_pule) {
                $txt_pule = $scope.listaApostas[0].txt_pule_pai;
            }
            $rootScope.loading = true;
            $http({
                method: "POST",
                url: "angularjs-mysql/validarBilhete.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'txt_pule=' + $txt_pule + 
                      '&cod_site=' + user.getSite() + 
                      '&cod_usuario=' + user.getCodigo() +
                      '&schema=' + user.getSchema()
            }).then(function (response) {
                if (response.data.status == 'OK') {
                    $.alert('Bilhete validado com sucesso!');
                    user.atualizaSaldo(response.data.saldo);
                    user.setBilheteExterno($txt_pule);
                    $route.reload();
                } else {
                    $.alert(response.data.mensagem);
                }
                $rootScope.loading = false;
            }, function (response) {
                $.alert(response.data.mensagem);
                $rootScope.loading = false;
            });
        }

        $scope.selectTabModalidade = function(modalidade) {
            $scope.tabConcursoSelected = -1;
            $scope.previousTabConcurso = null;
            if ($scope.previousTabModalidade == $scope.tabModalidadeSelected) {
                $scope.tabModalidadeSelected = -1;
                $scope.previousTabModalidade = null;
            } else {
                $scope.previousTabModalidade = $scope.tabModalidadeSelected;
                $scope.carregaConfiguracao(modalidade);
                if ($scope.mapModalidade.get(modalidade).size == 1) {
                    let concurso = $scope.mapModalidade.get(modalidade).keys().next().value;
                    $scope.tabConcursoSelected = 0;
                    $scope.selectTabConcurso(concurso);
                }
            }
        }

        $scope.selectTabConcurso = function(concurso) {
            if ($scope.previousTabConcurso == $scope.tabConcursoSelected) {
                $scope.tabConcursoSelected = -1;
                $scope.previousTabConcurso = null;
            } else {
                $scope.previousTabConcurso = $scope.tabConcursoSelected;
                const modalidade = Array.from($scope.mapModalidade.keys())[$scope.tabModalidadeSelected];
                extrairInformacoesApostas(modalidade, concurso);
            }
        }

        $scope.deselectAll = function() {
            $scope.tabModalidadeSelected = -1;
            $scope.tabConcursoSelected = -1;
        }

        $scope.getQtdApostasCanceladasPorModalidade = function(concursos) {
            var canceladas = [];
            concursos.forEach((bilhetes, concurso) => {
                bilhetes.forEach(bilhete => {
                    canceladas = canceladas.concat(bilhete.apostas.filter(element => {
                        return element.status == "Cancelado";
                    }));
                });
            });
            return canceladas.length;
        }

        $scope.getQtdApostasCanceladasPorConcurso = function(bilhetes) {
            var canceladas = [];
            bilhetes.forEach(bilhete => {
                canceladas = canceladas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Cancelado";
                }));
            });
            return canceladas.length;
        }

        $scope.getQtdApostasAbertasPorModalidade = function(concursos) {
            var abertas = [];
            concursos.forEach((bilhetes, concurso) => {
                bilhetes.forEach(bilhete => {
                    abertas = abertas.concat(bilhete.apostas.filter(element => {
                        return element.status == "Aberto";
                    }));
                });
            });
            return abertas.length;
        }

        $scope.getQtdApostasAbertasPorConcurso = function(bilhetes) {
            var abertas = [];
            bilhetes.forEach(bilhete => {
                abertas = abertas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Aberto";
                }));
            });
            return abertas.length;
        }

        $scope.getQtdApostasPerdidasPorModalidade = function(concursos) {
            var perdidas = [];
            concursos.forEach((bilhetes, concurso) => {
                bilhetes.forEach(bilhete => {
                    perdidas = perdidas.concat(bilhete.apostas.filter(element => {
                        return element.status == "Perdido";
                    }));
                });
            });
            return perdidas.length;
        }

        $scope.getQtdApostasPerdidasPorConcurso = function(bilhetes) {
            var perdidas = [];
            bilhetes.forEach(bilhete => {
                perdidas = perdidas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Perdido";
                }));
            });
            return perdidas.length;
        }

        $scope.getQtdApostasGanhasPorModalidade = function(concursos) {
            var ganhas = [];
            concursos.forEach((bilhetes, concurso) => {
                bilhetes.forEach(bilhete => {
                    ganhas = ganhas.concat(bilhete.apostas.filter(element => {
                        return element.status == "Ganho";
                    }));
                });
            });
            if (ganhas.length > 0) {
                return ganhas.length + "(R$ " + ganhas.reduce(function (total, aposta) 
                { return total + parseFloat(aposta.valor_ganho); }, 0) + ")";
            } 
            return ganhas.length;
        }

        $scope.getQtdApostasGanhasPorConcurso = function(bilhetes) {
            var ganhas = [];
            bilhetes.forEach(bilhete => {
                ganhas = ganhas.concat(bilhete.apostas.filter(element => {
                    return element.status == "Ganho";
                }));
            })
            if (ganhas.length > 0) {
                return ganhas.length + "(R$ " + ganhas.reduce(function (total, aposta) 
                { return total + parseFloat(aposta.valor_ganho); }, 0) + ")";
            } 
            return ganhas.length;
        }

        $scope.getQtdApostasPorModalidade = function(concursos) {
            var qtd = 0;
            concursos.forEach((bilhetes, concurso) => {
                bilhetes.forEach(bilhete => {
                    qtd += bilhete.apostas.length;
                })
            });
            return qtd;
        }

        $scope.getQtdApostasPorConcurso = function(bilhetes) {
            var qtd = 0;
            bilhetes.forEach(bilhete => {
                qtd += bilhete.apostas.length;
            });
            return qtd;
        }

        $scope.getConcursos = function() {
            const modalidade = Array.from($scope.mapModalidade.keys())[$scope.tabModalidadeSelected];
            return $scope.mapModalidade.get(modalidade);
        }

        $scope.cancelarBilhete = function () {
            if (!$scope.verificaPodeCancelar($scope.status_bilhete)) {
                $.alert('Bilhete não pode ser cancelado, já se esgotou o tempo de cancelamento!');
            } else {
                $.confirm({
                    title: '',
                    content:"Se você deseja realmente cancelar esse bilhete, aperte em OK",
                    buttons: {
                        cancelar: function() {},
                        ok: function () {
                            var txt_pule = $scope.txt_pule;
                            if ($scope.txt_pule_pai) {
                                txt_pule = $scope.txt_pule_pai;
                            }
                            $rootScope.loading = true;
                            $http({
                                method: "POST",
                                url: "angularjs-mysql/cancelarBilhete.php",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: 'txt_pule=' + txt_pule + 
                                      '&cod_site=' + user.getSite() + 
                                      '&cod_usuario=' + user.getCodigo() +
                                      '&schema=' + user.getSchema()
                            }).then(function (response) {
                                if (response.data.status == 'OK') {
                                    $.alert('Bilhete cancelado com sucesso!');
                                    user.atualizaSaldo(response.data.saldo);
                                    $route.reload();    
                                } else {
                                    $.alert(response.data.mensagem);
                                    $rootScope.loading = false;
                                }
                            }, function (response) {
                                $.alert('Erro no $HTTP: ' + response.status);
                                $rootScope.loading = false;
                            });
                        }
                    }
                });
            }
        }

        $scope.verificaPodeValidar = function (status_bilhete) {
            if (user.getPerfil() != 'C' || status_bilhete != 'Pendente de Validação') {
                return false;
            } else {
                return true;
            }
        }

        $scope.verificaPodeRepetir = function (perfil, tipo_jogo, status_bilhete) {
            if (perfil != 'C' || tipo_jogo == '2 PRA 500' || tipo_jogo == 'RIFA') {
                return false;
            }            
            if (status_bilhete == 'Pendente de Validação') {
                return false;
            } else {
                return true;
            }
        }

        $scope.verificaPodeCancelar = function (status_bilhete) {
            if (user.getPerfil() == 'A') {
                return status_bilhete != 'Cancelado';
            }
            if (user.getPerfil() == 'C' && status_bilhete != 'Aberto') {
                return false;
            }

            if (status_bilhete == 'Pendente de Validação') {
                return false;
            }

            var data = new Date(),
                minutos = parseInt($scope.minutos_cancelamento);

            dataConv = new Date($scope.data_bilhete_js);
            dataConv.setMinutes(dataConv.getMinutes() + minutos);

            if (data > dataConv) {
                return false;
            } else {
                return true;
            }
        }

        $scope.conferirBilheteExterno = function () {
            var aux = user.getBilheteExterno();
            if (aux != "") {
                $scope.externo = true;
                $scope.edtCodBilhete = aux;
                $scope.qrcodeString = aux;
                $scope.procurarBilhete();
                user.setBilheteExterno("");
            }
        }

        $scope.getTelefoneSemCaracteres = function (telefone) {
            var sAux = telefone;
            sAux = sAux.replace("(", "");
            sAux = sAux.replace(")", "");
            sAux = sAux.replace("-", "");
            sAux = sAux.replace(" ", "");
            return sAux.trim();
        }

        $scope.getLinkMobile = function (txt_pule) {
            //return "com.mitsoftwar.appprinter://http://www.betloteria.com/angularjs-mysql/bilheteCodificado.php?id=" + txt_pule;
            var strAux = "com.mitsoftwar.appprinter://" + $location.absUrl() 
                          + "/angularjs-mysql/bilheteCodificado.php?id=" + txt_pule 
                          + "&site=" + user.getSite()
                          + "&cod_usuario=" + user.getCodigo()
                          + "&schema=" + user.getSchema();

            strAux = strAux.replace('#!/conferir_bilhete/', '');
            return strAux;
        }

        $scope.abrir = function (txt_pule) {
            var linka = $scope.getLinkMobile(txt_pule);
            window.open(linka);
        }

        $scope.abrirBilheteResumido = function(txt_pule) {
            var printer = "com.mitsoftwar.appprinter://" + $location.absUrl() + 
            "/angularjs-mysql/bilheteResumidoCodificado.php?id=" + txt_pule + '&schema=' + user.getSchema();
            printer = printer.replace('#!/conferir_bilhete/', '');
            window.open(printer);
        }

        $scope.repetirJogo = function (txt_pule_pai, txt_pule, tipo) {

            if ((tipo == 'BICHO' && user.getflg_bicho() == 'N') ||
                (tipo == 'SENINHA' && user.getflg_seninha() == 'N') ||
                (tipo == 'SUPER SENA' && user.getflg_supersena() == 'N') ||
                (tipo == 'QUININHA' && user.getflg_quininha() == 'N') ||
                (tipo == 'LOTINHA' && user.getflg_lotinha() == 'N')) {
                $.alert("O seu usuário não está habilitado para realizar este tipo de jogo!");
                return;
            }

            user.setRepetirBilhete(true);
            if (tipo == 'BICHO' && txt_pule_pai) {
                txt_pule = txt_pule_pai;
            }
            else if ($scope.numero_bilhete == txt_pule) {
                txt_pule = $scope.edtCodBilhete;
            }            
            user.setPuleRepeticao(txt_pule);

            switch(tipo) {
                case 'BICHO':
                    $location.path("/bicho");
                break;
                case 'SENINHA':
                    $location.path("/seninha");
                break;
                case 'SUPER SENA':
                    $location.path("/supersena");
                break;
                case 'QUININHA':
                    $location.path("/quininha");
                break;
                case 'LOTINHA':
                    $location.path("/lotinha");
                break;
                case 'MULTI':
                    $location.path("/multi_jogos");
                break;
            }
        }

        function limpaCampos() {
            $scope.exibeBilhete = false;
            $scope.data_bilhete = null;
            $scope.data_bilhete_js = null;
            $scope.nome_banca = "";
            $scope.cambista = "";
            $scope.nome_apostador = "";
            $scope.telefone_apostador = "";
            $scope.txt_pule = "";
            $scope.txt_pule_pai = "";
            $scope.qrcodeString = "";
            $scope.status = "";
            $scope.status_bilhete = "";
            $scope.data_jogo = null;
            $scope.totalApostado = 0;
            $scope.mapModalidade = new Map();
            $scope.tabModalidadeSelected = -1;
            $scope.tabConcursoSelected = -1;
        }

        $scope.clickZap = function (confirm) {

            var txt_pule = $scope.txt_pule
            if ($scope.listaApostas.length > 0 && $scope.listaApostas[0].numero_bilhete == txt_pule) {
                txt_pule = $scope.listaApostas[0].txt_pule_pai;
            }

            var linkWeb = 'Para conferir seu bilhete acesse:'
                + '%0ahttp://bilheteloteria.com/#!/' + txt_pule;
            var linkZap = 'Para conferir seu bilhete acesse:'
                + '%0ahttp://bilheteloteria.com/%23!/' + txt_pule;

            if (user.getDominio()) {
                linkWeb = linkWeb + '%0a%0aAposte em:%0a' + user.getDominio();
                linkZap = linkZap + '%0a%0aAposte em:%0a' + user.getDominio();
            }

            linkZap = linkZap + '%0a%0aBoa sorte 🍀!';
            linkWeb = linkWeb + '%0a%0aBoa sorte 🍀!';

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                if (confirm) {
                    if ($scope.telefone_apostador != '') {
                        window.open('https://wa.me/55' + $scope.telefone_apostador + '?text=' + linkZap);
                    } else {
                        window.open('https://wa.me/?text=' + linkZap);
                    }
                } else {
                    window.open('https://wa.me/?text=' + linkZap);
                }
            } else {
                if (confirm) {
                    if ($scope.telefone_apostador != '') {
                        window.open('https://web.whatsapp.com/send?phone=55' + $scope.telefone_apostador + '&text=' + linkWeb);
                    } else {
                        window.open('https://web.whatsapp.com/send?text=' + linkWeb);
                    }
                } else {
                    window.open('https://web.whatsapp.com/send?text=' + linkWeb);
                }
            }
        }

        $scope.confirmationDialog = function () {
            if ($scope.telefone_apostador != '') {
                $scope.confirmationDialogConfig = {
                    title: "",
                    message: "Deseja enviar o bilhete para o número: " + $scope.telefone_apostador + "?",
                    buttons: [{
                        label: "Sim",
                        action: "com-telefone"
                    },
                    {
                        label: "Outro Número",
                        action: "sem-telefone"
                    }]
                };
                $scope.showDialog(true);
            } else {
                $scope.clickZap(false);
            }
        }

        $scope.showDialog = function (flag) {
            jQuery("#confirmation-dialog").modal(flag ? 'show' : 'hide');
        }

        $scope.executeDialogAction = function (action) {
            switch (action) {
                case 'com-telefone':
                    $scope.clickZap(true);
                    break;
                case 'sem-telefone':
                    $scope.clickZap(false);
                    break;
            }
            $scope.showDialog(false);
        }

        $scope.procurarBilhete = function () {
            limpaCampos();
            $scope.iniciou = true;
            var promessa = bilhetesAPI.getBilhetes($scope.edtCodBilhete, $scope.edtNumeroBilhete);
            promessa.then(function (result) {
                $scope.listaApostas = result;
                $scope.exibeBilhete = $scope.listaApostas.length > 0;
                if (!$scope.exibeBilhete) {
                    limpaCampos();
                    $.alert('Bilhete não encontrado!');
                }
                for (i = 0; i < $scope.listaApostas.length; i++) {
                    const aposta = $scope.listaApostas[i];
                    const tipo_jogo = funcoes.getDescricaoTipoJogo(aposta.cod_tipo_jogo, 
                        aposta.tipo_jogo_dinamico).toUpperCase();
                    if ($scope.mapModalidade.get(tipo_jogo) == null) {
                        $scope.mapModalidade.set(tipo_jogo, new Map());
                    }
                    if ($scope.mapModalidade.get(tipo_jogo).get(aposta.concurso) == null) {
                        $scope.mapModalidade.get(tipo_jogo).set(aposta.concurso, []);
                    }
                    $scope.mapModalidade.get(tipo_jogo).get(aposta.concurso).push(aposta);
                    $scope.totalApostado += parseFloat(aposta.valor_bilhete);
                }
                
                if ($scope.exibeBilhete) {
                    if ($scope.mapModalidade.size > 1) {
                        $scope.tipo_jogo_bilhete = 'MULTI';
                    } else {
                        $scope.tipo_jogo_bilhete = $scope.listaApostas[0].tipo_jogo;
                        const modalidade = funcoes.getDescricaoTipoJogo(
                            $scope.listaApostas[0].cod_tipo_jogo, 
                            $scope.listaApostas[0].tipo_jogo_dinamico).toUpperCase();
                        $scope.tabModalidadeSelected = 0;
                        $scope.selectTabModalidade(modalidade);
                    }
                    extrairInformacoesBilhete();
                }
            });
        };

        extrairInformacoesBilhete = function () {
            const bilhete = $scope.listaApostas[0];
            $scope.nome_banca = bilhete.nome_banca;
            $scope.data_bilhete = bilhete.data_bilhete;
            $scope.data_bilhete_js = bilhete.data_bilhete_js;
            $scope.cambista = bilhete.cambista;
            $scope.nome_apostador = bilhete.nome_apostador;
            $scope.telefone_apostador = bilhete.telefone_apostador;
            $scope.txt_pule = bilhete.txt_pule;
            $scope.txt_pule_pai = bilhete.txt_pule_pai;
            $scope.status_bilhete = bilhete.status_bilhete;
        }

        extrairInformacoesApostas = function(modalidade, concurso) {
            const bilhetes = $scope.mapModalidade.get(modalidade).get(concurso);
            const jogo = bilhetes[0];
            $scope.concurso = jogo.concurso;
            $scope.data_jogo = jogo.data_jogo;
            $scope.numeros_sorteados = jogo.numeros_sorteados;
            $scope.tipo_jogo = jogo.tipo_jogo;
            $scope.tipo_jogo_dinamico = jogo.tipo_jogo_dinamico;
            $scope.txt_pule_aposta = jogo.numero_bilhete;
            $scope.status_aposta = jogo.status_bilhete;
            $scope.tabApostas = [];
            $scope.valor_bilhete = 0;
            bilhetes.forEach(bilhete => {
                $scope.tabApostas = $scope.tabApostas.concat(bilhete.apostas);
                $scope.valor_bilhete += parseFloat(bilhete.valor_bilhete);
            });
            $scope.totalGanho = 0;
            for (i = 0; i < $scope.tabApostas.length; i++) {
                var aposta = $scope.tabApostas[i];
                if (aposta.status == 'Ganho') {
                    $scope.totalGanho = $scope.totalGanho + parseFloat(aposta.valor_ganho, 0);
                }
            }
        }
    });
