<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


if (!isset($_POST)) die();

$response = [];

try {
	$cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
	$cod_site = mysqli_real_escape_string($con, $_POST['cod_site']);
	$codigo = mysqli_real_escape_string($con, $_POST['codigo']);
	$nome = mysqli_real_escape_string($con, $_POST['nome']);
	$valor = mysqli_real_escape_string($con, $_POST['valor']);

	$stmt = $con->prepare("UPDATE usuario SET saldo = limite WHERE COD_USUARIO = ?");
	$stmt->bind_param("i", $codigo);

	$stmt->execute();
	inserir_auditoria(
		$con, 
		$cod_usuario, 
		$cod_site, 
		AUD_USUARIO_RESET_SALDO,
		descreverResetSaldo($nome, $valor)
	);
	$response['status'] = "OK";
} catch (Exception $e) {
	$response['status'] = "ERROR";
	$response['mensagem'] = $e->getMessage();
}

echo json_encode($response);

$stmt->close();
$con->close();
