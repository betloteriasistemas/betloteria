<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
if ($operacao == "salvar") {
    $queryOldCliente = "SELECT c.* 
                        FROM cliente_cambista c
                        WHERE c.COD_CLIENTE_CAMBISTA = ?";
    $stmtOldCliente = null;
    $oldCliente = null;

    $con->begin_transaction();
    try {
        $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
        $nome = mysqli_real_escape_string($con, $_POST['nome']);
        $telefone = mysqli_real_escape_string($con, $_POST['telefone']);
        $codigo = mysqli_real_escape_string($con, $_POST['codigo']); 
        error_log($codigo);
        if ($codigo == 0) {
            verificarClienteExistente($con, $cod_usuario, $nome);
            $stmt = $con->prepare("INSERT INTO cliente_cambista (cod_usuario, nome, telefone) VALUES (?, ?, ?)");
            $stmt->bind_param("iss", $cod_usuario, $nome, $telefone);
            $stmt->execute();
        } else {
            $stmtOldCliente = $con->prepare($queryOldCliente);
            $stmtOldCliente->bind_param("i", $codigo);
            $stmtOldCliente->execute();
            $result = $stmtOldCliente->get_result();
            $oldCliente = $result->fetch_assoc();
            $stmtOldCliente->close();
            
            if ($nome != $oldCliente['NOME']) {
                verificarClienteExistente($con, $cod_usuario, $nome);
            }

            $stmt = $con->prepare("UPDATE cliente_cambista SET NOME=?, TELEFONE=? WHERE COD_CLIENTE_CAMBISTA = ?");
            $stmt->bind_param("ssi", $nome, $telefone, $codigo);
            $stmt->execute();
        }
        
        $con->commit();
        $response['status'] = "OK";
    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }
    echo json_encode($response);
} else if ($operacao == "excluir") {
    try {
        $con->begin_transaction();

        $cod_cliente = mysqli_real_escape_string($con, $_POST['cod_cliente']);
        $nome_cliente = mysqli_real_escape_string($con, $_POST['nome_cliente']);
        
        $stmt = $con->prepare("DELETE FROM cliente_cambista WHERE cod_cliente_cambista = ?");
        $stmt->bind_param("i", $cod_cliente);
        $stmt->execute();
        
        $con->commit();
        $stmt->close();
        $response['status'] = "OK";
    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }
    echo json_encode($response);
} else if ($operacao == "listar") {
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $query = "SELECT COD_CLIENTE_CAMBISTA, NOME, TELEFONE
              FROM cliente_cambista
              WHERE COD_USUARIO = $cod_usuario
              ORDER BY NOME";          
    $result = mysqli_query($con, $query);
    $return_arr = array();
    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['cod_cliente'] = $row['COD_CLIENTE_CAMBISTA'];
        $row_array['nome'] = $row['NOME'];
        $row_array['telefone'] = $row['TELEFONE'];
        array_push($return_arr,$row_array);
        if ($contador == mysqli_num_rows($result)){
		    break;
        }
    }
    echo json_encode($return_arr , JSON_NUMERIC_CHECK);
}

$con->close();

function verificarClienteExistente($con, $cod_usuario, $nome) {
    $query = "SELECT  nome
              FROM cliente_cambista
              WHERE cod_usuario = '$cod_usuario' and nome = '$nome'";
    $result = mysqli_query($con, $query);

    if (mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        throw new Exception('Já existe um cliente com este nome.');
    }
}