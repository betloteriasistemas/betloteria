angular.module('main').controller('trocarSenhaCtrl', function ($scope, $http, user, $location) {
    $scope.nome = user.getNome();

    $scope.trocarSenha = function () {
        var novaSenha = $scope.edtNovaSenha;
        var confirmarSenha = $scope.edtConfirmarSenha;

        if (typeof novaSenha === 'undefined'){
            $.alert('Informe a nova senha!')
        }else if (typeof confirmarSenha === 'undefined'){
            $.alert('Informe a confirmação da senha !')
        } else if (novaSenha.trim() == ""){
            $.alert('A senha não pode estar em branco!')
        } else if (novaSenha != confirmarSenha) {
            $.alert('A senhas não conferem!')
        } else {
            $http({
                method: "POST",                
                url: "angularjs-mysql/trocarSenha.php",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'nome_usuario=' + user.getNome() +
                '&cod_usuario=' + user.getCodigo() + 
                '&site=' + user.getSite() + 
                '&nova_senha=' + novaSenha +
                '&schema=' + user.getSchema()
            }).then(function (response) {
                $.alert('Senha alterada com sucesso!');
                $location.path('/dashboard');
                //$scope.records = JSON.stringify(response.data);			
            }, function (response) {
                console.log(response);
                $.alert('Erro no $HTTP: ' + response.status)
            });
        }
    }
});