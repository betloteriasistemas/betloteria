<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

if ($operacao == "listar") {

    $query = "SELECT EA.COD_EXTRACAO,
                    EA.NOME_BANCA,
                    EA.ENDPOINT,
                    HEA.COD_HORA_EXTRACAO,
                    HEA.HORA
                FROM EXTRACAO_AUTOMATIZADA EA
                    INNER JOIN DIA_EXTRACAO_AUTOMATIZADA DEA ON EA.COD_EXTRACAO = DEA.COD_EXTRACAO
                    INNER JOIN DIA_HORA_EXTRACAO DHE ON DEA.COD_DIA_EXTRACAO = DHE.COD_DIA_EXTRACAO
                    INNER JOIN HORA_EXTRACAO_AUTOMATIZADA HEA ON DHE.COD_HORA_EXTRACAO = HEA.COD_HORA_EXTRACAO
                GROUP BY EA.COD_EXTRACAO, HEA.COD_HORA_EXTRACAO
                ORDER BY EA.NOME_BANCA, HEA.HORA";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['nome_banca'] = $row['NOME_BANCA'];
        $row_array['endpoint'] = $row['ENDPOINT'];
        $row_array['cod_hora_extracao'] = $row['COD_HORA_EXTRACAO'];
        $row_array['hora'] = $row['HORA'];

        $cod_extracao = $row['COD_EXTRACAO'];
        $cod_hora_extracao = $row['COD_HORA_EXTRACAO'];
        $queryDias = "SELECT DEA.COD_DIA_EXTRACAO, DEA.DIA 
                    FROM DIA_EXTRACAO_AUTOMATIZADA DEA
                        INNER JOIN DIA_HORA_EXTRACAO DHE ON DEA.COD_DIA_EXTRACAO = DHE.COD_DIA_EXTRACAO
                    WHERE DHE.STATUS = 'A'
                        AND DEA.COD_EXTRACAO = '$cod_extracao'
                        AND DHE.COD_HORA_EXTRACAO = '$cod_hora_extracao'";
        
        $resultDias = mysqli_query($con, $queryDias); 

        $domingo = 'NÃO';
        $segunda = 'NÃO';
        $terca = 'NÃO';
        $quarta = 'NÃO';
        $quinta = 'NÃO';
        $sexta = 'NÃO';
        $sabado = 'NÃO';
        while ($rowDias = mysqli_fetch_array($resultDias, MYSQLI_ASSOC)) {     
            if ($rowDias['DIA'] == 'DOM') {
                $domingo = 'SIM';
            } else if ($rowDias['DIA'] == 'SEG') {
                $segunda = 'SIM';
            } else if ($rowDias['DIA'] == 'TER') {
                $terca = 'SIM';
            } else if ($rowDias['DIA'] == 'QUA') {
                $quarta = 'SIM';
            } else if ($rowDias['DIA'] == 'QUI') {
                $quinta = 'SIM';
            } else if ($rowDias['DIA'] == 'SEX') {
                $sexta = 'SIM';
            } else if ($rowDias['DIA'] == 'SAB') {
                $sabado = 'SIM';
            }        
        }                
        $row_array['domingo'] = $domingo;
        $row_array['segunda'] = $segunda;
        $row_array['terca'] = $terca;
        $row_array['quarta'] = $quarta;
        $row_array['quinta'] = $quinta;
        $row_array['sexta'] = $sexta;
        $row_array['sabado'] = $sabado;

        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);

} else if ($operacao == "listarBancas") {

    $query = "SELECT COD_EXTRACAO,
                     NOME_BANCA,
                     ENDPOINT
              FROM EXTRACAO_AUTOMATIZADA
              ORDER BY NOME_BANCA";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['nome_banca'] = $row['NOME_BANCA'];
        $row_array['endpoint'] = $row['ENDPOINT'];
        array_push($return_arr, $row_array);
    }
    echo json_encode($return_arr);

} else if ($operacao == "salvar") {
    
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);
    $edtHora = mysqli_real_escape_string($con, $_POST['edtHora']);
    $domingo = mysqli_real_escape_string($con, $_POST['domingo']);
    $segunda = mysqli_real_escape_string($con, $_POST['segunda']);
    $terca = mysqli_real_escape_string($con, $_POST['terca']);
    $quarta = mysqli_real_escape_string($con, $_POST['quarta']);
    $quinta = mysqli_real_escape_string($con, $_POST['quinta']);
    $sexta = mysqli_real_escape_string($con, $_POST['sexta']);
    $sabado = mysqli_real_escape_string($con, $_POST['sabado']);

    $dias_habilitar_arr = array();
    $dias_desabilitar_arr = array();
    if ($domingo == "S") {
        array_push($dias_habilitar_arr, 'DOM');
    } else {
        array_push($dias_desabilitar_arr, 'DOM');
    }

    if ($segunda == "S") {
        array_push($dias_habilitar_arr, 'SEG');
    } else {
        array_push($dias_desabilitar_arr, 'SEG');
    }

    if ($terca == "S") {
        array_push($dias_habilitar_arr, 'TER');
    } else {
        array_push($dias_desabilitar_arr, 'TER');
    }

    if ($quarta == "S") {
        array_push($dias_habilitar_arr, 'QUA');
    } else {
        array_push($dias_desabilitar_arr, 'QUA');
    }

    if ($quinta == "S") {
        array_push($dias_habilitar_arr, 'QUI');
    } else {
        array_push($dias_desabilitar_arr, 'QUI');
    }

    if ($sexta == "S") {
        array_push($dias_habilitar_arr, 'SEX');
    } else {
        array_push($dias_desabilitar_arr, 'SEX');
    }

    if ($sabado == "S") {
        array_push($dias_habilitar_arr, 'SAB');
    } else {
        array_push($dias_desabilitar_arr, 'SAB');
    }

    try {
        $query = "SHOW DATABASES";
        $resultSchema = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($resultSchema, MYSQLI_ASSOC)) {
            $schema = $row['Database'];        
            if (substr( $schema, 0, 1 ) == "b") {
                $con->begin_transaction();

                foreach ($dias_habilitar_arr as $dia) {
        
                    $query = " SELECT COD_DIA_EXTRACAO
                                FROM $schema.DIA_EXTRACAO_AUTOMATIZADA 
                                WHERE COD_EXTRACAO = '$cod_extracao'
                                    AND DIA = '$dia'";
        
                    $result = mysqli_query($con, $query);
        
                    $cod_dia_extracao = 0;
                    if (mysqli_num_rows($result)) {            
                        $dia_extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        $cod_dia_extracao = $dia_extracao['COD_DIA_EXTRACAO'];     
                    } else {
                        $stmt = $con->prepare("INSERT INTO $schema.DIA_EXTRACAO_AUTOMATIZADA
                                                (COD_EXTRACAO, DIA)
                                                VALUES(?, ?)");
                        $stmt->bind_param("is", $cod_extracao, $dia);
                        $stmt->execute();       
                        $cod_dia_extracao = $stmt->insert_id;
                    }
        
                    $query = " SELECT COD_HORA_EXTRACAO
                                FROM $schema.HORA_EXTRACAO_AUTOMATIZADA 
                                WHERE HORA = '$edtHora'";
        
                    $result = mysqli_query($con, $query);
                    if (mysqli_num_rows($result)) {
                        $hora_extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        $cod_hora_extracao = $hora_extracao['COD_HORA_EXTRACAO'];
                    } else {
                        $stmt = $con->prepare("INSERT INTO $schema.HORA_EXTRACAO_AUTOMATIZADA
                                                (HORA)
                                                VALUES(?)");
                        $stmt->bind_param("s", $edtHora);
                        $stmt->execute();       
                        $cod_hora_extracao = $stmt->insert_id;            
                    }
        
                    $query = " SELECT COD_DIA_HORA_EXTRACAO
                                FROM $schema.DIA_HORA_EXTRACAO 
                                WHERE COD_DIA_EXTRACAO = '$cod_dia_extracao'
                                    AND COD_HORA_EXTRACAO = '$cod_hora_extracao'";
        
                    $result = mysqli_query($con, $query);
                    if (mysqli_num_rows($result)) {
                        $dia_hora_extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        $cod_dia_hora_extracao = $dia_hora_extracao['COD_DIA_HORA_EXTRACAO'];
                        $stmt = $con->prepare("UPDATE $schema.DIA_HORA_EXTRACAO
                                                SET STATUS = 'A'
                                                WHERE COD_DIA_HORA_EXTRACAO = ?");
                        $stmt->bind_param("i", $cod_dia_hora_extracao);
                        $stmt->execute();
                    } else {
                        $stmt = $con->prepare("INSERT INTO $schema.DIA_HORA_EXTRACAO
                                                (COD_DIA_EXTRACAO, COD_HORA_EXTRACAO)
                                                VALUES(?, ?)");
                        $stmt->bind_param("ii", $cod_dia_extracao, $cod_hora_extracao);
                        $stmt->execute();
                    }
        
                }
        
                foreach ($dias_desabilitar_arr as $dia) {
        
                    $query = " SELECT COD_DIA_EXTRACAO
                                FROM $schema.DIA_EXTRACAO_AUTOMATIZADA 
                                WHERE COD_EXTRACAO = '$cod_extracao'
                                    AND DIA = '$dia'";
        
                    $result = mysqli_query($con, $query);
        
                    if (mysqli_num_rows($result)) {            
                        $dia_extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);
                        $cod_dia_extracao = $dia_extracao['COD_DIA_EXTRACAO'];     
                    
                        $query = " SELECT COD_HORA_EXTRACAO
                                    FROM $schema.HORA_EXTRACAO_AUTOMATIZADA 
                                    WHERE HORA = '$edtHora'";
        
                        $result = mysqli_query($con, $query);
                        if (mysqli_num_rows($result)) {
                            $hora_extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);
                            $cod_hora_extracao = $hora_extracao['COD_HORA_EXTRACAO'];       
                            
                            $query = " SELECT COD_DIA_HORA_EXTRACAO
                                        FROM $schema.DIA_HORA_EXTRACAO 
                                        WHERE COD_DIA_EXTRACAO = '$cod_dia_extracao'
                                            AND COD_HORA_EXTRACAO = '$cod_hora_extracao'";
        
                            $result = mysqli_query($con, $query);
                            if (mysqli_num_rows($result)) {
                                $dia_hora_extracao = mysqli_fetch_array($result, MYSQLI_ASSOC);
                                $cod_dia_hora_extracao = $dia_hora_extracao['COD_DIA_HORA_EXTRACAO'];
                                $stmt = $con->prepare("UPDATE $schema.DIA_HORA_EXTRACAO
                                                        SET STATUS = 'I'
                                                        WHERE COD_DIA_HORA_EXTRACAO = ?");
                                $stmt->bind_param("i", $cod_dia_hora_extracao);
                                $stmt->execute();
                            }                    
                        }
                    }
                }
        
                $con->commit();                
            }
        }

    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

} else if ($operacao == "excluir") {
    
    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);
    $edtHora = mysqli_real_escape_string($con, $_POST['edtHora']);

    try {
        $query = "SHOW DATABASES";
        $resultSchema = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($resultSchema, MYSQLI_ASSOC)) {
            $schema = $row['Database'];        
            if (substr( $schema, 0, 1 ) == "b") {
                $con->begin_transaction();

                $query = " SELECT DHE.COD_DIA_HORA_EXTRACAO
                            FROM $schema.DIA_EXTRACAO_AUTOMATIZADA DEA
                                INNER JOIN $schema.DIA_HORA_EXTRACAO DHE ON DEA.COD_DIA_EXTRACAO = DHE.COD_DIA_EXTRACAO
                                INNER JOIN $schema.HORA_EXTRACAO_AUTOMATIZADA HEA ON DHE.COD_HORA_EXTRACAO = HEA.COD_HORA_EXTRACAO
                            WHERE DEA.COD_EXTRACAO = '$cod_extracao'
                                AND HORA = '$edtHora'";
        
                $result = mysqli_query($con, $query);
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $cod_dia_hora_extracao = $row['COD_DIA_HORA_EXTRACAO'];
                    $stmt = $con->prepare("DELETE FROM $schema.DIA_HORA_EXTRACAO
                                            WHERE COD_DIA_HORA_EXTRACAO = ?");
                    $stmt->bind_param("i", $cod_dia_hora_extracao);
                    $stmt->execute();       
                }                
                $con->commit();        
            }
        }

    } catch (Exception $e) {
        $con->rollback();
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

}

$con->close();

