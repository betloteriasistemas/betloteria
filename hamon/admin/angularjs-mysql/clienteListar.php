<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$response = [];
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);
$return_arr = array();


if ($operacao == 'listar_cliente')
   {
    $dataDe = mysqli_real_escape_string($con, $_POST['dataDe']);
    $dataAte = mysqli_real_escape_string($con, $_POST['dataAte']);
    
    $query = "SHOW DATABASES";
	$resultSchema = mysqli_query($con, $query);
	while ($row = mysqli_fetch_array($resultSchema, MYSQLI_ASSOC)) {
		$schema = $row['Database'];        
        if (substr( $schema, 0, 1 ) == "b") {
            $query = "select
                        v.cod_site,
                        v.nome_banca,
                        v.mensalidade,
                        v.promocao,
                        v.vencimento,
                        SUM(v.total_entradas) as total_entradas,
                        SUM(v.total_saidas) as total_saidas,
                        SUM(v.comissao) as comissao,
                        SUM(v.qtd_apostas),
                        SUM(v.comissao_gerente) as comissao_gerente,
                        ROUND((SUM(v.total_entradas) - SUM(v.total_saidas) - SUM(v.comissao) - SUM(v.comissao_gerente)), 2) AS saldo
                    FROM
                        (SELECT 
                            apo.cod_site,
                                jog.data_jogo,
                                site.nome_banca,
                                site.mensalidade,
                                site.promocao,
                                site.vencimento,
                                SUM(apo.VALOR_APOSTA) total_entradas,
                                SUM(apo.VALOR_GANHO) total_saidas,
                                SUM(apo.COMISSAO) comissao,
                                COUNT(apo.cod_aposta) qtd_apostas,
                                CASE WHEN (sum(apo.VALOR_APOSTA) - sum(apo.VALOR_GANHO) - sum(apo.COMISSAO)) > 0 THEN
                                    round(((CASE
                                                WHEN jog.Tipo_Jogo = 'S' THEN geren.pct_comissao_seninha
                                                WHEN jog.Tipo_Jogo = 'Q' THEN geren.pct_comissao_quininha
                                                WHEN jog.Tipo_Jogo IN ('B' , 'G') THEN geren.pct_comissao_bicho
                                                WHEN jog.Tipo_Jogo = '2' THEN geren.pct_comissao_2PRA500
                                                ELSE 0
                                            END) * (SUM(apo.VALOR_APOSTA) - SUM(apo.VALOR_GANHO) - SUM(apo.COMISSAO)))/100, 2)
                                    ELSE 0 END AS comissao_gerente
                        FROM
                            $schema.aposta apo
                        INNER JOIN $schema.bilhete bil ON (bil.COD_BILHETE = apo.cod_bilhete
                            AND bil.cod_site = apo.cod_site)
                        INNER JOIN $schema.usuario usu ON (bil.COD_USUARIO = usu.cod_usuario
                            AND bil.cod_site = usu.cod_site)
                        INNER JOIN $schema.usuario geren ON (geren.cod_usuario = usu.cod_gerente)
                        INNER JOIN $schema.site site ON (bil.COD_SITE = site.cod_site)
                        INNER JOIN $schema.jogo jog ON (jog.COD_JOGO = apo.COD_JOGO)
                        WHERE
                            1 = 1 AND jog.data_jogo >= '$dataDe'
                                AND jog.data_jogo <= '$dataAte'
                                AND bil.STATUS_BILHETE <> 'C'
                                AND site.mensalidade >= 550
                        GROUP BY geren.cod_usuario) v   
                    GROUP BY cod_site
                    ORDER BY vencimento DESC ";
           $result = mysqli_query($con, $query);            
            
            $contador = 0;
            
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $contador = $contador + 1;
            
                $row_array['cod_site'] = $row['cod_site'];
                $row_array['nome_banca'] = $row['nome_banca'];
                $row_array['mensalidade'] = $row['mensalidade'];
                $row_array['promocao'] = $row['promocao'];
                $row_array['vencimento'] = $row['vencimento'];
                $row_array['total_entradas'] = $row['total_entradas'];
                $row_array['total_saidas'] = $row['total_saidas'];
                $row_array['comissao'] = $row['comissao'];
                $row_array['comissao_gerente'] = $row['comissao_gerente'];
                $row_array['saldo'] = $row['saldo'];
                
                $row_array['query'] = $query;
                
                array_push($return_arr, $row_array);
            
                if ($contador == mysqli_num_rows($result)) {
                    break;
                }
            };            

		}
	}

   }else if ($operacao == 'salvar'){
      $cliente = json_decode($_POST['cliente']);
    
    //   $query = "update site
    //             set mensalidade = '$cliente->mensalidade', promocao = '$cliente->promocao'
    //             where cod_site = '$cliente->cod_site' ";
    $stmt = $con->prepare("UPDATE site SET mensalidade = ?, promocao = ?
		                       WHERE cod_site = ? ");
        $stmt->bind_param("ssi", $cliente->mensalidade, $cliente->promocao, $cliente->cod_site);
        $stmt->execute();
    array_push($return_arr, "OK");
   }

   
echo json_encode($return_arr, JSON_NUMERIC_CHECK);
$con->close();