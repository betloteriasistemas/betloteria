<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
	die();
}

$query = " select ao.`COD_OPERACAO`, ao.`TXT_DESCRICAO` from auditoria_operacao ao";

$result = mysqli_query($con, $query);
$return_arr = array();
$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    $row_array['cod_operacao'] = $row['COD_OPERACAO'];
	$row_array['txt_descricao'] = $row['TXT_DESCRICAO'];
    array_push($return_arr, $row_array);
	if ($contador == mysqli_num_rows($result)) {
		break;
	}
}

$con->close();
echo json_encode($return_arr, JSON_NUMERIC_CHECK);
