<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');

$pule = mysqli_real_escape_string($con, $_GET['id']);
if ($pule == "") {
    print "Informe o Pule";
    exit;
}

$query_info_bilhete = "SELECT s.cod_site, nome_banca, NOME_APOSTADOR, TELEFONE_APOSTADOR, 
                              camb.NOME as CAMBISTA,
                              DATE_FORMAT(b.DATA_BILHETE, '%d/%m/%Y %H:%i:%S') DATA_BILHETE,
                              CONCAT(FLOOR(c.VALOR_APOSTA), ' PRA ', FLOOR(c.VALOR_ACUMULADO)) as JOGO_DINAMICO
                    FROM bilhete b 
                    INNER JOIN site s ON (s.cod_site = b.cod_site)
                    INNER JOIN usuario camb on (b.COD_USUARIO = camb.COD_USUARIO )
                    INNER JOIN configuracao_2pra500 c on (camb.COD_AREA = c.COD_AREA and camb.COD_SITE = c.COD_SITE)
                    WHERE (b.TXT_PULE = '$pule' or b.TXT_PULE_PAI = '$pule')
                    LIMIT 1";
                    
$query_info_apostas = "SELECT (case
                                when j.tipo_jogo = 'S' then 'SENINHA'
                                when j.tipo_jogo = 'Q' then 'QUININHA'
                                when j.tipo_jogo = 'L' then 'LOTINHA'
                                when j.tipo_jogo = 'B' then 'BICHO'
                                when j.tipo_jogo = '2' then '2 PRA 500'
                                when j.tipo_jogo = 'R' then 'RIFA' else ''
                                end
                                ) as tipo_jogo,
                            cast(
                                case
                                    when j.tipo_jogo IN ('B','2') then concat(
                                        DATE_FORMAT(j.data_jogo, '%d/%m/%Y'), '-', TIME_FORMAT(j.hora_extracao, '%H:%i'),
                                        case when j.desc_hora is null then '' else concat('-',j.desc_hora) 
                                    end)
                                    when j.tipo_jogo = 'R' THEN j.descricao
                                    else j.concurso end as char
                                ) as concurso,
                            (case
                                when a.status = 'A' then 'Aberto'
                                when a.status = 'G' then 'Ganho'
                                when a.status = 'C' then 'Cancelado'
                                when a.status = 'P' then 'Perdido' end
                            ) as status,
                            count(*) as qtd_apostas,
                            sum(a.VALOR_APOSTA) as valor_apostado,
                            sum(a.VALOR_GANHO) as valor_ganho
                    FROM bilhete b
                    inner join aposta a on (b.COD_BILHETE = a.COD_BILHETE)
                    inner join jogo j on (a.COD_JOGO = j.COD_JOGO)
                    where (b.txt_pule = '$pule' or b.txt_pule_pai = '$pule')
                    group by tipo_jogo, concurso, status";

$result = mysqli_query($con, $query_info_bilhete);
$info_bilhete = mysqli_fetch_assoc($result);
$cod_site = $info_bilhete['cod_site'];
print "!LF<br/>";
if ($cod_site == 3010){
    print "!AC~" . $info_bilhete['nome_banca'] . "~False~False~False<br/>";    
}else{
    print "!AC~" . $info_bilhete['nome_banca'] . "~True~False~True<br/>";    
}
print "!LP<br/>";
print "!AE~DATA: " . $info_bilhete['DATA_BILHETE'] . "~False~False~False<br/>";
print "!AE~COLABORADOR: " . $info_bilhete['CAMBISTA'] . "~False~False~False<br/>";
print "!AE~CLIENTE: " . $info_bilhete['NOME_APOSTADOR'] . " - " . $info_bilhete['TELEFONE_APOSTADOR'] . "~False~False~False<br/>";    

$result = mysqli_query($con, $query_info_apostas);

$modalidade = "";
$concurso = "";
$valor_total_bilhete = 0;

while($info_apostas = mysqli_fetch_assoc($result)) {
    if ($modalidade != $info_apostas['tipo_jogo']) {
        $modalidade = $info_apostas['tipo_jogo'];
        print "!LP<br/>";
        if ($modalidade == '2 PRA 500') {
            print "!AC~" . $info_bilhete['JOGO_DINAMICO'] . "~False~False~False<br/>";
        } else {
            print "!AC~" . $modalidade . "~False~False~False<br/>";
        }
        print "!LP<br/>";
    }

    if ($concurso != $info_apostas['concurso']) {
        $concurso = $info_apostas['concurso'];
        print "!AE~CONCURSO: " . $concurso . "~False~False~False<br/>";
    }
    print "!AE~Status: " . $info_apostas['status'] . 
          " Qtd. apostas: " . $info_apostas['qtd_apostas'] . 
          " Valor apostado: " . $info_apostas['valor_apostado'] .
          ($info_apostas['status'] == 'Ganho' ?  " Valor ganho: " . $info_apostas['valor_ganho'] : " ") . 
          "~False~False~False";
    print "<br/>";
    $valor_total_bilhete+= $info_apostas['valor_apostado'];
}

print "!LP<br/>";
print "!LF<br/>";
print "!AC~BILHETE:" . $pule . "~True~False~False<br/>";
print "!AC~VALOR BILHETE: R$ " . number_format($valor_total_bilhete, 2, ',', '.') . "~True~False~False<br/>";
print "!LF<br/>";

$con->close();