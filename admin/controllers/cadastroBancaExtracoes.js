angular.module('main').controller('cadastroBancaExtracoesCtrl', function ($scope, user, $http, $route, $rootScope) {

    $scope.listarBancas = function () {
        $rootScope.loading = true;
        $http({
            url: 'angularjs-mysql/cadastroBancaExtracoes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "&operacao=listar"
        }).then(function (response) {
            $scope.records = response.data;
        }, function (response) {
            alert('Erro no $HTTP: ' + response.status);
        }).finally(function() {
            $rootScope.loading = false;
        });
    }

    $scope.listarNomesBancas = function () {
        $http({
            url: 'angularjs-mysql/cadastroBancaExtracoes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: "&operacao=listarBancas"
        }).then(function (response) {
            $scope.nomesBancas = response.data;
        }, function (response) {
            alert('Erro no $HTTP: ' + response.status);
        }).finally(function() {
            $rootScope.loading = false;
        });
    }    

    $scope.getDescSN = function (diaMarcado) {
        if (diaMarcado == true) {
            return "S";
        } 
        return "N";
    };    

    $scope.alterar = function (banca) {
        $scope.comboBanca = banca.cod_extracao;
        $scope.edtHora = banca.hora;
        $scope.edtCheckDomingo = banca.domingo === "SIM";
        $scope.edtCheckSegunda = banca.segunda === "SIM";
        $scope.edtCheckTerca = banca.terca === "SIM";
        $scope.edtCheckQuarta = banca.quarta === "SIM";
        $scope.edtCheckQuinta = banca.quinta === "SIM";
        $scope.edtCheckSexta = banca.sexta === "SIM";
        $scope.edtCheckSabado = banca.sabado === "SIM";        
    }

    $scope.salvar = function () {
        $rootScope.loading = true;
        var cod_extracao = $scope.comboBanca; 
        var edtHora = $scope.edtHora;
        var domingo = $scope.edtCheckDomingo;
        var segunda = $scope.edtCheckSegunda;
        var terca = $scope.edtCheckTerca;
        var quarta = $scope.edtCheckQuarta;
        var quinta = $scope.edtCheckQuinta;
        var sexta = $scope.edtCheckSexta;
        var sabado = $scope.edtCheckSabado;

        if (comboBanca == undefined || comboBanca == '') {
            alert("Informe uma banca!");
            return;
        }

        if (edtHora == undefined || edtHora == '') {
            alert("Informe a hora de extração!");
            return;
        }        

        if (typeof cod_extracao == undefined) {
            cod_extracao = 0;
        }

        domingo = $scope.getDescSN(domingo);
        segunda = $scope.getDescSN(segunda);
        terca = $scope.getDescSN(terca);
        quarta = $scope.getDescSN(quarta);
        quinta = $scope.getDescSN(quinta);
        sexta = $scope.getDescSN(sexta);
        sabado = $scope.getDescSN(sabado);

        $http({

            url: 'angularjs-mysql/cadastroBancaExtracoes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_extracao=' + cod_extracao +
                '&operacao=salvar' +
                '&edtHora=' + edtHora +
                '&domingo=' + domingo +
                '&segunda=' + segunda +
                '&terca=' + terca +
                '&quarta=' + quarta +
                '&quinta=' + quinta +
                '&sexta=' + sexta +
                '&sabado=' + sabado
        }).then(function (response) {
            if (response.status == '200') {
                alert("Salvo com sucesso!");
                $route.reload();
            } else {
                alert(response.data.mensagem);
            }
        }).finally(function() {
            $rootScope.loading = false;
        });

    }

    $scope.excluir = function (cod_extracao, hora) {
        $rootScope.loading = true;
        $http({

            url: 'angularjs-mysql/cadastroBancaExtracoes.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'cod_extracao=' + cod_extracao +
                '&operacao=excluir' +
                '&edtHora=' + hora
        }).then(function (response) {
            if (response.status == '200') {
                alert("Excluído com sucesso!");
                $route.reload();
            } else {
                alert(response.data.mensagem);
            }
        }).finally(function() {
            $rootScope.loading = false;
        });        
    }

});