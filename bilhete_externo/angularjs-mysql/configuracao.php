<?php

include "conexao.php";

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

$queryArea = "SELECT COD_AREA
    FROM areas
    WHERE cod_site = '$site' AND padrao = 'S'";

$resultArea = mysqli_query($con, $queryArea);
$rowArea = mysqli_fetch_array($resultArea, MYSQLI_ASSOC);
$cod_area = $rowArea['COD_AREA'];

if (isset($_GET['schema']) && $_GET['schema'] != 'undefined') {
    $schema = $_GET['schema'];
} else if (isset($_POST['schema']) && $_POST['schema'] != 'undefined') {
    $schema = $_POST['schema'];
} else if (isset($_SESSION['schema']) && $_SESSION['schema'] != 'undefined') {
    $schema = $_SESSION['schema'];
} else {
    $schema = "betloteria";
}

$queryMaxPule = "SELECT AUTO_INCREMENT
                    FROM information_schema.tables
                WHERE table_name = 'bilhete'
                    AND table_schema = '$schema' ";

$resultMaxPule = mysqli_query($con, $queryMaxPule);
$rowMaxPule = mysqli_fetch_array($resultMaxPule, MYSQLI_ASSOC);
$valorMaxPule = $rowMaxPule['AUTO_INCREMENT'];

switch ($operacao) {
    case "geral":
        $query = "SELECT FLG_BICHO, FLG_QUININHA, FLG_LOTINHA, FLG_SENINHA, FLG_2PRA500, FLG_RIFA,
                        MINUTOS_CANCELAMENTO, VALOR_MIN_APOSTA, VALOR_MAX_APOSTA, FLG_SUPER_SENA
                FROM configuracao_geral
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "seninha":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_14, PREMIO_15, PREMIO_16, PREMIO_17, PREMIO_18,
                        PREMIO_19, PREMIO_20, PREMIO_25, PREMIO_30, PREMIO_35, PREMIO_40,
                        MSG_BILHETE, FLG_SORTE, SENA_SORTE, QUINA_SORTE, QUADRA_SORTE
                FROM configuracao
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "supersena":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_SENA, MSG_BILHETE, PREMIO_QUINA, PREMIO_QUADRA
                FROM configuracao_super_sena
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;     
    case "quininha":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_13, PREMIO_14, PREMIO_15, PREMIO_16, PREMIO_17,
                        PREMIO_18, PREMIO_19, PREMIO_20, PREMIO_25, PREMIO_30, PREMIO_35, PREMIO_40,
                        PREMIO_45, MSG_BILHETE, FLG_SORTE, QUINA_SORTE, QUADRA_SORTE, TERNO_SORTE
                FROM configuracao_quininha
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "lotinha":
        $query = "SELECT PREMIO_MAXIMO, PREMIO_16, PREMIO_17, PREMIO_18, PREMIO_19, PREMIO_20, 
                        PREMIO_21, PREMIO_22, MSG_BILHETE
                FROM configuracao_lotinha
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "bicho":
        $query = "SELECT PREMIO_MILHAR_CENTENA, PREMIO_MILHAR_SECA, PREMIO_MILHAR_INVERTIDA, 
                         PREMIO_CENTENA, PREMIO_CENTENA_INVERTIDA, 
                         PREMIO_GRUPO, PREMIO_DUQUE_GRUPO, PREMIO_DUQUE_GRUPO6, 
                         PREMIO_TERNO_GRUPO, PREMIO_TERNO_GRUPO6, PREMIO_TERNO_GRUPO10, 
                         PREMIO_TERNO_SEQUENCIA, PREMIO_TERNO_SOMA, 
                         PREMIO_TERNO_ESPECIAL, PREMIO_QUINA_GRUPO,
                         PREMIO_DEZENA, PREMIO_DUQUE_DEZENA, PREMIO_DUQUE_DEZENA6, PREMIO_TERNO_DEZENA, PREMIO_TERNO_DEZENA6, PREMIO_TERNO_DEZENA10,
                         PREMIO_PASSE_SECO, PREMIO_PASSE_COMBINADO, 
                         MSG_BILHETE, USA_MILHAR_BRINDE, VALOR_LIBERAR_MILHAR_BRINDE,
                        VALOR_RETORNO_MILHAR_BRINDE
                FROM configuracao_bicho
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
        $queryTiposJogosBicho = "SELECT tjb.*
                FROM tipo_jogo_bicho tjb
                INNER JOIN configuracao_tipo_jogo_bicho ctjb on (tjb.COD_TIPO_JOGO_BICHO = ctjb.COD_TIPO_JOGO_BICHO)
                INNER JOIN configuracao_bicho cb on (ctjb.COD_CONFIGURACAO = cb.CODIGO)
                WHERE cb.COD_SITE = '$site' and cb.COD_AREA = '$cod_area'
                ORDER BY tjb.ORDEM";
    break;
    case "2pra500":
        $query = " SELECT VALOR_ACUMULADO, VALOR_APOSTA, MSG_BILHETE
                FROM configuracao_2pra500
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
    case "rifa":
        $query = "SELECT MSG_BILHETE
                FROM configuracao_rifa
                WHERE COD_SITE = '$site' AND COD_AREA = '$cod_area'";
    break;
}


$result = mysqli_query($con, $query);    
$return_arr = array();
$contador = 0;

while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $contador = $contador + 1;
    switch ($operacao) {
        case "geral":
            $row_array['flg_bicho'] = $row['FLG_BICHO'] == 'S';
            $row_array['flg_seninha'] = $row['FLG_SENINHA'] == 'S';
            $row_array['flg_quininha'] = $row['FLG_QUININHA'] == 'S';
            $row_array['flg_lotinha'] = $row['FLG_LOTINHA'] == 'S';
            $row_array['flg_2pra500'] = $row['FLG_2PRA500'] == 'S';
            $row_array['flg_rifa'] = $row['FLG_RIFA'] == 'S';
            $row_array['flg_supersena'] = $row['FLG_SUPER_SENA'] == 'S';
            $row_array['minutos_cancelamento'] = $row['MINUTOS_CANCELAMENTO'];
            $row_array['valor_min_aposta'] = $row['VALOR_MIN_APOSTA'];
            $row_array['valor_max_aposta'] = $row['VALOR_MAX_APOSTA'];
        break;
        case "2pra500":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['valor_acumulado'] = $row['VALOR_ACUMULADO'];
            $row_array['valor_aposta'] = $row['VALOR_APOSTA'];
        break;
        case "seninha":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_14'] = $row['PREMIO_14'];
            $row_array['premio_15'] = $row['PREMIO_15'];
            $row_array['premio_16'] = $row['PREMIO_16'];
            $row_array['premio_17'] = $row['PREMIO_17'];
            $row_array['premio_18'] = $row['PREMIO_18'];
            $row_array['premio_19'] = $row['PREMIO_19'];
            $row_array['premio_20'] = $row['PREMIO_20'];
            $row_array['premio_25'] = $row['PREMIO_25'];
            $row_array['premio_30'] = $row['PREMIO_30'];
            $row_array['premio_35'] = $row['PREMIO_35'];
            $row_array['premio_40'] = $row['PREMIO_40'];
            $row_array['habilita_sorte'] = $row['FLG_SORTE'] == 'S';
            $row_array['quadra_sorte'] = $row['QUADRA_SORTE'];
            $row_array['quina_sorte'] = $row['QUINA_SORTE'];
            $row_array['sena_sorte'] = $row['SENA_SORTE'];
        break;
        case "supersena":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_sena'] = $row['PREMIO_SENA'];
            $row_array['premio_quina'] = $row['PREMIO_QUINA'];
            $row_array['premio_quadra'] = $row['PREMIO_QUADRA'];
        break;
        case "quininha":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_13'] = $row['PREMIO_13'];
            $row_array['premio_14'] = $row['PREMIO_14'];
            $row_array['premio_15'] = $row['PREMIO_15'];
            $row_array['premio_16'] = $row['PREMIO_16'];
            $row_array['premio_17'] = $row['PREMIO_17'];
            $row_array['premio_18'] = $row['PREMIO_18'];
            $row_array['premio_19'] = $row['PREMIO_19'];
            $row_array['premio_20'] = $row['PREMIO_20'];
            $row_array['premio_25'] = $row['PREMIO_25'];
            $row_array['premio_30'] = $row['PREMIO_30'];
            $row_array['premio_35'] = $row['PREMIO_35'];
            $row_array['premio_40'] = $row['PREMIO_40'];
            $row_array['premio_45'] = $row['PREMIO_45'];
            $row_array['habilita_sorte'] = $row['FLG_SORTE'] == 'S';
            $row_array['terno_sorte'] = $row['TERNO_SORTE'];
            $row_array['quadra_sorte'] = $row['QUADRA_SORTE'];
            $row_array['quina_sorte'] = $row['QUINA_SORTE'];
        break;
        case "lotinha":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_maximo'] = $row['PREMIO_MAXIMO'];
            $row_array['premio_16'] = $row['PREMIO_16'];
            $row_array['premio_17'] = $row['PREMIO_17'];
            $row_array['premio_18'] = $row['PREMIO_18'];
            $row_array['premio_19'] = $row['PREMIO_19'];
            $row_array['premio_20'] = $row['PREMIO_20'];
            $row_array['premio_21'] = $row['PREMIO_21'];
            $row_array['premio_22'] = $row['PREMIO_22'];
        break;
        case "bicho":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
            $row_array['premio_milhar_centena'] = $row['PREMIO_MILHAR_CENTENA'];
            $row_array['premio_milhar_seca'] = $row['PREMIO_MILHAR_SECA'];
            $row_array['premio_milhar_invertida'] = $row['PREMIO_MILHAR_INVERTIDA'];
            $row_array['premio_centena'] = $row['PREMIO_CENTENA'];
            $row_array['premio_centena_invertida'] = $row['PREMIO_CENTENA_INVERTIDA'];
            $row_array['premio_grupo'] = $row['PREMIO_GRUPO'];
            $row_array['premio_duque_grupo'] = $row['PREMIO_DUQUE_GRUPO'];
            $row_array['premio_duque_grupo6'] = $row['PREMIO_DUQUE_GRUPO6'];
            $row_array['premio_terno_grupo'] = $row['PREMIO_TERNO_GRUPO'];
            $row_array['premio_terno_grupo6'] = $row['PREMIO_TERNO_GRUPO6'];
            $row_array['premio_terno_grupo10'] = $row['PREMIO_TERNO_GRUPO10'];
            $row_array['premio_terno_sequencia'] = $row['PREMIO_TERNO_SEQUENCIA'];
            $row_array['premio_terno_soma'] = $row['PREMIO_TERNO_SOMA'];
            $row_array['premio_terno_especial'] = $row['PREMIO_TERNO_ESPECIAL'];
            $row_array['premio_quina_grupo'] = $row['PREMIO_QUINA_GRUPO'];
            $row_array['premio_dezena'] = $row['PREMIO_DEZENA'];
            $row_array['premio_duque_dezena'] = $row['PREMIO_DUQUE_DEZENA'];
            $row_array['premio_duque_dezena6'] = $row['PREMIO_DUQUE_DEZENA6'];
            $row_array['premio_terno_dezena'] = $row['PREMIO_TERNO_DEZENA'];
            $row_array['premio_terno_dezena6'] = $row['PREMIO_TERNO_DEZENA6'];
            $row_array['premio_terno_dezena10'] = $row['PREMIO_TERNO_DEZENA10'];
            $row_array['premio_passe_seco'] = $row['PREMIO_PASSE_SECO'];
            $row_array['premio_passe_combinado'] = $row['PREMIO_PASSE_COMBINADO'];
            $row_array['usa_milhar_brinde'] = $row['USA_MILHAR_BRINDE'] == "S";
            $row_array['valor_liberar_milhar_brinde'] = $row['VALOR_LIBERAR_MILHAR_BRINDE'];
            $row_array['valor_retorno_milhar_brinde'] = $row['VALOR_RETORNO_MILHAR_BRINDE'];
            $row_array['valorMaxPule'] = $valorMaxPule;    
            
            $result_tipos_jogos_bicho = mysqli_query($con, $queryTiposJogosBicho); 
            $tiposJogosBicho = array();
            while ($row_tipos_jogos_bicho = mysqli_fetch_array($result_tipos_jogos_bicho, MYSQLI_ASSOC)) {
                $row_tipo_jogos_bicho['codigo_tipo_jogo'] = $row_tipos_jogos_bicho['COD_TIPO_JOGO_BICHO'];
                $row_tipo_jogos_bicho['codigo'] = $row_tipos_jogos_bicho['CODIGO'];
                $row_tipo_jogos_bicho['descricao'] = $row_tipos_jogos_bicho['DESCRICAO'];
                array_push($tiposJogosBicho, $row_tipo_jogos_bicho);
            }
            $row_array['tipos_jogos_bicho'] = $tiposJogosBicho;
        break;
        case "rifa":
            $row_array['msg_bilhete'] = $row['MSG_BILHETE'];
        break;
    }

    array_push($return_arr, $row_array);

    if ($contador == mysqli_num_rows($result)) {
        break;
    }
}
$con->close();
echo json_encode($return_arr);
