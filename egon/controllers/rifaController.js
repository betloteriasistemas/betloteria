angular
  .module("main")
  .controller(
    "rifaCtrl",
    function ($rootScope, $scope, $q, user, $http, $location, configGeral) {
      $rootScope.loading = true;
      $scope.flg_rifa = true;
      $scope.vendidos = [];
      $scope.selecionados = [];
      $scope.apostados = [];
      $scope.jogos = [];
      $scope.apostas = [];
      $scope.chkRegistroCliente = false;
      $scope.clientes = [];
      $scope.edtRifa = null;
      $scope.paginacao = {
        totalItens: 0,
        paginaAtual: 1,
        itensPorPagina: 200,
      };

      configGeral.get().then(function (data) {
        $scope.flg_rifa = user.getflg_rifa() == "S" && data.flg_rifa;
        $scope.valor_min_aposta = data.valor_min_aposta;
        $scope.valor_max_aposta = data.valor_max_aposta;
      });

      $q.all([
        $http({
          url: "angularjs-mysql/configuracao.php",
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data:
            "site=" +
            user.getSite() +
            "&cod_area=" +
            user.getArea() +
            "&operacao=rifa" +
            "&schema=" +
            user.getSchema(),
        }),
        $http({
          method: "POST",
          url: "angularjs-mysql/clientes.php",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: "cod_usuario=" + user.getCodigo() + "&operacao=listar",
        }),
      ]).then(function (response) {
        $scope.listaCfg = response[0].data;
        $scope.clientes = response[1].data;
        $rootScope.loading = false;
        $scope.paginacao = {
          totalItens: $scope.listaCfg[0].qtd_rifas,
          paginaAtual: 1,
          itensPorPagina: 200,
        };
      });

      $scope.filtrarCliente = function (termo) {
        return $scope.clientes.filter(
          (cliente) => cliente.nome.toLowerCase().indexOf(termo) > -1
        );
      };

      $scope.onSelectCliente = function (cliente) {
        if (cliente) {
          $scope.edtTelefone = cliente.telefone;
          document.getElementById("chkRegistrarCliente").focus();
        } else {
          $scope.edtTelefone = null;
        }
      };

      $scope.getJogos = function () {
        $http({
          method: "POST",
          url: "angularjs-mysql/jogos.php",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data:
            "username=" +
            user.getName() +
            "&finalizados=false" +
            "&site=" +
            user.getSite() +
            "&status=L" +
            "&dataAtual=true" +
            "&operacao=rifa" +
            "&schema=" +
            user.getSchema(),
        }).then(
          function (response) {
            $scope.jogos = response.data;
          },
          function (response) {
            $.alert("Erro no $HTTP: " + response.status);
          }
        );
      };

      // Adiciona aposta
      $scope.adicionarAposta = function () {
        if (typeof $scope.comboConcurso === "undefined") {
          $.alert("Primeiro escolha a Rifa!");
          return;
        }
        var rifa = JSON.parse($scope.comboConcurso);
        // colocar o valor da rifa em questão
        $scope.edtValor = rifa.valor;
        var valorAux = parseFloat($scope.edtValor.toString().replace(",", "."));

        var sNumeros = "";
        var tamanhoArray, i;
        var booApostaDuplicada = false;
        tamanhoArray = $scope.apostas.length;
        for (k = 0; k < $scope.selecionados.length; k++) {
          var valorA = $scope.selecionados[k];
          for (i = 0; i < tamanhoArray; i++) {
            if (
              rifa.cod_jogo == $scope.apostas[i].cod_jogo &&
              $scope.apostas[i].numeros.includes(valorA)
            ) {
              booApostaDuplicada = true;
              return;
            }
          }
          sNumeros = sNumeros + $scope.getNumeroRifa(valorA) + "-";
        }
        if (booApostaDuplicada) {
          $.alert("Aposta duplicada!");
          return;
        }
        if (sNumeros == "") {
          $.alert("Escolha ao menos UMA RIFA!");
          return;
        }
        sNumeros = sNumeros.trim();
        sNumeros = sNumeros.substr(0, sNumeros.length - 1);
        sNumeros = sNumeros.replace(/\./g, "");
        var sTipo = "Rifa";
        var sValor = "R$ " + $scope.edtValor.toString();
        var iQtd = $scope.selecionados.length;
        var objeto = {
          numeros: sNumeros,
          valor: sValor,
          valorReal: valorAux * iQtd,
          qtd: iQtd,
          tipo: sTipo,
          cod_jogo: rifa.cod_jogo,
          possivelRetorno: rifa.descricao,
          possivelRetornoReal: 0,
        };
        $scope.apostas.push(objeto);
        $scope.getValorTotal();
        $scope.apostados.push(...$scope.selecionados);
        $scope.selecionados = [];
        $scope.aplicarClasses();
      };

      $scope.removerAposta = function (index) {
        var numerosRemovidos = $scope.apostas[index].numeros;
        $scope.apostas.splice(index, 1);
        $scope.getValorTotal();
        $scope.apostados = removeItems($scope.apostados, numerosRemovidos);
        $scope.aplicarClasses();
      };

      $scope.getValorTotal = function () {
        var dValorTotal = 0;
        for (i = 0; i < $scope.apostas.length; i++) {
          dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
        }

        var numero = parseFloat(dValorTotal).toFixed(2).split(".");
        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join(".");
        $scope.valorTotal = numero.join(",");
      };

      $scope.getValorTotalReal = function () {
        var dValorTotal = 0;
        for (i = 0; i < $scope.apostas.length; i++) {
          dValorTotal = dValorTotal + $scope.apostas[i].valorReal;
        }
        return dValorTotal;
      };

      $scope.gerarBilhete = function () {
        if (typeof $scope.comboConcurso === "undefined") {
          $.alert("Informe o CONCURSO!");
          return;
        }
        if (!$scope.edtNome) {
          $.alert("Informe o NOME!");
          return;
        }

        if (typeof $scope.edtTelefone === "undefined") {
          $.alert("Informe o TELEFONE!");
          return;
        }

        if ($scope.apostas.length == 0) {
          $.alert("Faça no mínimo uma aposta!");
          return;
        }

        if (
          parseFloat(user.getSaldoAtualizado()) < $scope.getValorTotalReal()
        ) {
          $.alert("Você não tem saldo para gerar o bilhete!");
          return;
        }

        $.confirm({
          title: "",
          content: "Confirma a geração do bilhete?",
          buttons: {
            cancelar: function () {},
            ok: function () {
              var rifa = JSON.parse($scope.comboConcurso);
              var sTelefone = $scope.edtTelefone;
              if (typeof sTelefone === "undefined") {
                sTelefone = "";
              }

              var data = new Date();

              var dia = data.getDate();
              if (dia.toString().length == 1) {
                dia = "0" + dia;
              }
              var mes = data.getMonth() + 1;
              if (mes.toString().length == 1) {
                mes = "0" + mes;
              }
              var ano = data.getFullYear();

              var horas = data.getHours();
              if (horas.toString().length == 1) {
                horas = "0" + horas;
              }
              var minutos = data.getMinutes();
              if (minutos.toString().length == 1) {
                minutos = "0" + minutos;
              }
              var segundos = data.getSeconds();
              if (segundos.toString().length == 1) {
                segundos = "0" + segundos;
              }

              var dataAtual =
                ano +
                "-" +
                mes +
                "-" +
                dia +
                " " +
                horas +
                ":" +
                minutos +
                ":" +
                segundos;
              $rootScope.loading = true;
              $http({
                url: "angularjs-mysql/salvarBilheteTeimosinha.php",
                method: "POST",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded",
                },
                data:
                  "cod_usuario=" +
                  user.getCodigo() +
                  "&cod_site=" +
                  user.getSite() +
                  "&nome_usuario=" +
                  user.getName() +
                  "&cod_jogo=" +
                  rifa.cod_jogo +
                  "&nome=" +
                  $scope.edtNome +
                  "&telefone=" +
                  sTelefone +
                  "&registro_cliente=" +
                  $scope.chkRegistroCliente +
                  "&apostas=" +
                  JSON.stringify($scope.apostas) +
                  "&data_atual=" +
                  dataAtual +
                  "&operacao=" +
                  "&schema=" +
                  user.getSchema(),
              }).then(
                function (response) {
                  if (response.data.status == "OK") {
                    user.atualizaSaldo(response.data.saldo);
                    user.setBilheteExterno(response.data.pule);
                    $location.path("/conferir_bilhete");
                  } else {
                    $rootScope.loading = false;
                    if (response.data.status == "INATIVO") {
                      $.alert(response.data.mensagem);
                      $location.path("/logout");
                    } else {
                      $.alert(response.data.mensagem);
                    }
                  }
                },
                function (response) {
                  $rootScope.loading = false;
                  $.alert("Erro no $HTTP: " + response.status);
                }
              );
            },
          },
        });
      };

      $scope.irParaRifaESelecionar = function (valor) {
        const rifa = parseInt(valor);
        if (isNaN(rifa) || rifa >= $scope.paginacao.totalItens) {
          $scope.edtRifa = '';
          $.alert(`Forneça um número entre 0 e ${$scope.paginacao.totalItens - 1}.`);
          return;
        }
        const pagina = Math.floor(rifa / $scope.paginacao.itensPorPagina) + 1;
        $scope.alterarPagina(pagina);
        if (
          $scope.selecionados.includes(rifa) ||
          $scope.vendidos.includes(rifa)
        ) {
          $.alert("Rifa já apostada.");
          return;
        } else {
          $scope.selecionados.push(rifa);
        }
      };

      $scope.getRifasVendidas = function () {
        var rifa = JSON.parse($scope.comboConcurso);
        $rootScope.loading = true;
        $http({
          method: "POST",
          url: "angularjs-mysql/rifa.php",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: "cod_jogo=" + rifa.cod_jogo + "&schema=" + user.getSchema(),
        }).then(
          function (response) {
            $scope.limpar();
            $scope.vendidos.push(...response.data);
            var apostasFeitas = $scope.apostas.filter(
              (a) => a.cod_jogo == rifa.cod_jogo
            );
            for (i = 0; i < apostasFeitas.length; i++) {
              var numeros = apostasFeitas[i].numeros.split("-");
              $scope.apostados.push(...numeros);
            }
            $rootScope.loading = false;
            $scope.aplicarClasses();
          },
          function (response) {
            $.alert("Erro no $HTTP: " + response.status);
          }
        );
      };

      // Limpa todos os números selecionados
      $scope.limpar = function () {
        $scope.vendidos = [];
        $scope.selecionados = [];
        $scope.apostados = [];
      };

      $scope.alterarPagina = function (newPage) {
        $scope.paginacao.paginaAtual = newPage;
        $(document)
          .unbind()
          .click(function () {
            $scope.aplicarClasses();
          });
      };

      $scope.selecionar = function (event) {
        var element = angular.element(event.target);
        const rifa = parseInt(element.text());
        if ($scope.selecionados.includes(rifa)) {
          const index = $scope.selecionados.findIndex((valor) => valor == rifa);
          $scope.selecionados.splice(index, 1);
        } else {
          $scope.selecionados.push(rifa);
        }
        $scope.aplicarClasses();
      };

      $scope.aplicarClasses = function () {
        for (
          var i =
            ($scope.paginacao.paginaAtual - 1) *
            $scope.paginacao.itensPorPagina;
          i <
          $scope.paginacao.paginaAtual * $scope.paginacao.itensPorPagina +
            ($scope.paginacao.itensPorPagina - 1);
          i++
        ) {
          const rifa = i;
          const id = "#btn" + $scope.getNumeroRifa(rifa);
          var element = $(id);
          if (
            $scope.vendidos.includes(rifa) ||
            $scope.apostados.includes(rifa)
          ) {
            element
              .removeClass("btn-bg-grid")
              .removeClass("btn-outline-grid")
              .removeClass("btn-bet-grid");
            element.addClass("btn-selled-grid").attr("disabled", true);
          } else if ($scope.selecionados.includes(rifa)) {
            element.removeClass("btn-outline-grid").addClass("btn-bg-grid");
          } else {
            element.removeClass("btn-selled-grid").attr("disabled", false);
            element.removeClass("btn-bg-grid").addClass("btn-outline-grid");
          }
        }
      };

      $scope.getNumeroRifa = function (rifa) {
        const maiorRifa = $scope.paginacao.totalItens - 1;
        const digitos = maiorRifa.toString().length;
        return ("0".repeat(digitos) + rifa).substr(-digitos);
      };

      const removeItems = (array, itemsToRemove) => {
        return array.filter((v) => {
          return !itemsToRemove.includes(v);
        });
      };
    }
  );
