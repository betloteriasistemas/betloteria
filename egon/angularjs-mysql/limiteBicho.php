<?php

include "conexao.php";
require_once('auditoria.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

if (!isset($_POST)) {
    die();
}

$response = [];

$site = mysqli_real_escape_string($con, $_POST['site']);
$operacao = mysqli_real_escape_string($con, $_POST['operacao']);

if ($operacao == 'listar' || $operacao == 'carregar') {
    $query = "";

    $query =
        " SELECT b.CODIGO, b.MILHAR, b.CENTENA, b.GRUPO, b.DUQUE_GRUPO, b.TERNO_GRUPO, b.QUINA_GRUPO, 
	             b.DEZENA, b.DUQUE_DEZENA, b.TERNO_DEZENA, 
                 ex.COD_EXTRACAO, TIME_FORMAT(ex.hora_extracao, '%H:%i') as HORA_EXTRACAO, ex.DESCRICAO
	FROM configuracao_limite_bicho b
	inner join extracao_bicho ex on (ex.cod_extracao = b.cod_extracao)
    where b.cod_site = '$site' ";

    if ($operacao == 'carregar') {
        $codigos = implode(",", explode(',', mysqli_real_escape_string($con, $_POST['codigo'])));
        $query = $query . " and TIME_FORMAT(ex.hora_extracao, '%H:%i') in (select TIME_FORMAT(hora_extracao, '%H:%i') from jogo where cod_jogo in (" . $codigos . 
        ")) and ex.descricao in (select desc_hora from jogo where cod_jogo in (" . $codigos . ")) ";
    }
    $query = $query . " order by hora_extracao ";

    $result = mysqli_query($con, $query);

    $return_arr = array();

    $contador = 0;

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $contador = $contador + 1;
        $row_array['codigo'] = $row['CODIGO'];
        $row_array['milhar'] = $row['MILHAR'];
        $row_array['centena'] = $row['CENTENA'];
        $row_array['grupo'] = $row['GRUPO'];
        $row_array['duque_grupo'] = $row['DUQUE_GRUPO'];
        $row_array['terno_grupo'] = $row['TERNO_GRUPO'];
        $row_array['quina_grupo'] = $row['QUINA_GRUPO'];
        $row_array['dezena'] = $row['DEZENA'];
        $row_array['duque_dezena'] = $row['DUQUE_DEZENA'];
        $row_array['terno_dezena'] = $row['TERNO_DEZENA'];
        $row_array['cod_extracao'] = $row['COD_EXTRACAO'];
        $row_array['hora_extracao'] = $row['HORA_EXTRACAO'];
        $row_array['descricao'] = $row['DESCRICAO'];

        array_push($return_arr, $row_array);

        if ($contador == mysqli_num_rows($result)) {
            break;
        }
    };

    echo json_encode($return_arr);
} else if ($operacao == 'excluir') {
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);
    $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
    try {
        $stmt = $con->prepare("delete from configuracao_limite_bicho where codigo = ?");
        $stmt->bind_param("i", $codigo);
        $stmt->execute();
        inserir_auditoria(
            $con,
            $cod_usuario,
            $site,
            AUD_LIMITE_BICHO_EXCLUIDO,
            descreverExclusaoLimiteBicho($codigo)
        );
        $response['status'] = "OK";
        $stmt->close();
        $con->close();
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

    echo json_encode($response);
} else if ($operacao == 'salvar') {

    $codigo = mysqli_real_escape_string($con, $_POST['codigo']);
    $cod_usuario = mysqli_real_escape_string($con, $_POST['cod_usuario']);

    $cod_extracao = mysqli_real_escape_string($con, $_POST['cod_extracao']);
    $desc_extracao = mysqli_real_escape_string($con, $_POST['desc_extracao']);
    $milhar = mysqli_real_escape_string($con, $_POST['milhar']);
    $centena = mysqli_real_escape_string($con, $_POST['centena']);
    $grupo = mysqli_real_escape_string($con, $_POST['grupo']);
    $duque_grupo = mysqli_real_escape_string($con, $_POST['duque_grupo']);
    $terno_grupo = mysqli_real_escape_string($con, $_POST['terno_grupo']);
    $quina_grupo = mysqli_real_escape_string($con, $_POST['quina_grupo']);
    $dezena = mysqli_real_escape_string($con, $_POST['dezena']);
    $duque_dezena = mysqli_real_escape_string($con, $_POST['duque_dezena']);
    $terno_dezena = mysqli_real_escape_string($con, $_POST['terno_dezena']);
    try {
        $queryOldLimite = "SELECT * FROM configuracao_limite_bicho WHERE CODIGO = ?";
        $stmtOldLimite = null;
        $oldLimite = null;
        if ($codigo != 0) {
            $stmt = $con->prepare("UPDATE configuracao_limite_bicho
								set MILHAR = ?,
								CENTENA = ?,
								GRUPO = ?,
								DUQUE_GRUPO = ?,
								TERNO_GRUPO = ?,
                                QUINA_GRUPO = ?,
								DEZENA = ?,
								DUQUE_DEZENA = ?,
								TERNO_DEZENA = ?
								WHERE CODIGO = ? ");
            $stmt->bind_param(
                "dddddddddi",
                $milhar,
                $centena,
                $grupo,
                $duque_grupo,
                $terno_grupo,
                $quina_grupo,
                $dezena,
                $duque_dezena,
                $terno_dezena,
                $codigo
            );
            $stmtOldLimite = $con->prepare($queryOldLimite);
            $stmtOldLimite->bind_param("i", $codigo);
            $stmtOldLimite->execute();
            $result = $stmtOldLimite->get_result();
            $oldLimite = $result->fetch_assoc();
            $stmtOldLimite->close();
        } else {
            $query = " SELECT count(*) qtd FROM configuracao_limite_bicho
            WHERE COD_EXTRACAO = '$cod_extracao'
              and cod_site = '$site' ";
            $result = mysqli_query($con, $query);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if ($row['qtd'] > 0) {
                throw new Exception('Extração já existe!');
            }

            $stmt = $con->prepare("INSERT INTO configuracao_limite_bicho(COD_SITE, COD_EXTRACAO, MILHAR, CENTENA,
		                                   GRUPO, DUQUE_GRUPO, TERNO_GRUPO, QUINA_GRUPO, DEZENA, DUQUE_DEZENA, TERNO_DEZENA)
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
            $stmt->bind_param("iiddddddddd", $site, $cod_extracao, $milhar, $centena, $grupo, $duque_grupo, $terno_grupo, $quina_grupo, $dezena, $duque_dezena, $terno_dezena);
        }

        $stmt->execute();

        inserir_auditoria(
            $con,
            $cod_usuario,
            $site,
            $codigo != 0 ? AUD_LIMITE_BICHO_EDITADO : AUD_LIMITE_BICHO_CRIADO,
            $codigo != 0 ?
                descreverEdicaoLimiteBicho(
                    $oldLimite,
                    $codigo,
                    $milhar,
                    $centena,
                    $grupo,
                    $duque_grupo,
                    $terno_grupo,
                    $quina_grupo,
                    $dezena,
                    $duque_dezena,
                    $terno_dezena
                ) :
                descreverLimiteBicho(
                    $stmt->insert_id,
                    $desc_extracao,
                    $milhar,
                    $centena,
                    $grupo,
                    $duque_grupo,
                    $terno_grupo,
                    $quina_grupo,
                    $dezena,
                    $duque_dezena,
                    $terno_dezena
                )
        );

        $response['status'] = "OK";
        $stmt->close();
        $con->close();
    } catch (Exception $e) {
        $response['status'] = "ERROR";
        $response['mensagem'] = $e->getMessage();
    }

    echo json_encode($response);
}
